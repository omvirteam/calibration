<?php
include("include/omConfig.php");

function ohms_trim($amount, $perfection = 0) {
	$sign = "&Omega;";
	$postFix = "";
	$pfList = array("", "K", "M", "G");
	$i = 0;
	while($amount >= 1000) {
		$amount /= 1000;
		$i++;
	}	
	$postFix = $pfList[$i] . $sign;
	return number_format($amount, $perfection) . " " . $postFix;
}

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}

ob_start();
?>

<style>
table {font-family: "Times New Roman"; font-size: 12px;}
tr {line-height: 19px;}
.main_table tr td {border: 1px solid black;}
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 1px solid black;
    
}
.main_table td, .main_table th {
    padding: 4px;
    
}
.main_table{
    padding-bottom: 3px;
    
}
</style>

        <?php
        $qry1 = "SELECT * FROM observation_data WHERE id = ".$_GET['obs_data_id']."  ";
	$res1 = mysql_query($qry1) or die("Error :: Cannot select observation_data details.<hr>".mysql_error());
        $grnobsmaster_data = array();
        while($row1 = mysql_fetch_assoc($res1)) { 
            array_push($grnobsmaster_data,$row1);
	}
        $uuc_json_data = json_decode($grnobsmaster_data[0]['uuc_json']);
        $uuc_data_d = json_decode(json_encode($uuc_json_data), true);
        $uuc_data = $uuc_data_d[$_GET['key_value']];
        
        $master_json_data = json_decode($grnobsmaster_data[0]['master_json']);
        $master_data_d = json_decode(json_encode($master_json_data), true);
        $master_data = $master_data_d[$_GET['key_value']];
        
        $ab_json_data = json_decode($grnobsmaster_data[0]['type_a_type_b_json']);
        $ab_data_d = json_decode(json_encode($ab_json_data), true);
        $ab_data = $ab_data_d[$_GET['key_value']];
//        echo "<pre>"; print_r($master_data); exit;
        ?>
        
            <table width="100%" align="center" class="main_table print-friendly"  style="font-size:13px;">
                <tr>
                    <th colspan="8" align="center" valign="middle">Uncertainty Budget</th>
                </tr>
                <tr>
                    <th>Source of uncertainty Xi</th>
                    <th>Estimation Xi </th>
                    <th>Probability <br/>Type A 0r<br/> B-factor</th>
                    <th>Diviser</th>
                    <th>Standard <br/>Uncertainty <br/>U(Xi) </th>
                    <th>Sensitivity<br/> Coefficient</th>
                    <th>Uncertainty <br/>Contribution<br/> u(yi)</th>
                    <th>Deg. of<br/> Freedom <br/>v eff</th>
                </tr>
                <tr>
                    <td>Uncertainty Pressure Gauge<br/> U1</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u1'], 4, '.', ''); ?></td>
                    <td>B normal</td>
                    <td align="center">2</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u1_val'], 4, '.', ''); ?></td>
                    <td align="center">1</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u1_val'], 4, '.', ''); ?></td>
                    <td>infinity</td>
                </tr>
                <tr>
                    <td>Accuracy of of D.P.G.<br/> U2</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u2'], 4, '.', ''); ?></td>
                    <td>B Rectangular</td>
                    <td>sqrt(3)</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u2_val'], 4, '.', ''); ?></td>
                    <td align="center">1</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u2_val'], 4, '.', ''); ?></td>
                    <td>infinity</td>
                </tr>
                <tr>
                    <td>Hysteresis (max)<br/> U3</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u3'], 4, '.', ''); ?></td>
                    <td>B Rectangular</td>
                    <td>sqrt(3)</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u3_val'], 4, '.', ''); ?></td>
                    <td align="center">1</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u3_val'], 4, '.', ''); ?></td>
                    <td>infinity</td>
                </tr>
                <tr>
                    <td>Repeatbility (max) <br/>U4</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u4'], 4, '.', ''); ?></td>
                    <td>B Rectangular</td>
                    <td>sqrt(3)</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u4_val'], 4, '.', ''); ?></td>
                    <td align="center">1</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u4_val'], 4, '.', ''); ?></td>
                    <td>infinity</td>
                </tr>
                <tr>
                    <td>Zero Deviation <br/>U5</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u5'], 4, '.', ''); ?></td>
                    <td>B Rectangular</td>
                    <td>sqrt(3)</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u5_val'], 4, '.', ''); ?></td>
                    <td align="center">1</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u5_val'], 4, '.', ''); ?></td>
                    <td>infinity</td>
                </tr>
                <tr>
                    <td>Resolution of D.P.G.<br/> U6<br/>50 % Contribution</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u6'], 4, '.', ''); ?></td>
                    <td>B Rectangular</td>
                    <td>sqrt(3)</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u6_val'], 4, '.', ''); ?></td>
                    <td align="center">1</td>
                    <td align="center"><?php echo number_format((float)$ab_data['u6_val'], 4, '.', ''); ?></td>
                    <td>infinity</td>
                </tr>
                <tr>
                    <td>Combined <br/>Uncertainty</td>
                    <td align="center">-</td>
                    <td>-</td>
                    <td>-</td>
                    <td align="center">-</td>
                    <td align="center">-</td>
                    <td align="center"><?php echo number_format((float)$ab_data['com_uncer'], 4, '.', ''); ?></td>
                    <td>infinity</td>
                </tr>
            </table>
<br/>


<?php
// html - end //
//exit;
$content = ob_get_clean();
require_once 'html2pdf/html2pdf.class.php';
try {
    $html2pdf = new HTML2PDF('P', 'A4', 'fr',true,'UTF-8',array(7, 7, 7, 7));
    $html2pdf->pdf->SetTitle('Budget');
    $html2pdf->shrink_tables_to_fit = 1;    // Default: false
    $html2pdf->setDefaultFont('Times');
    $html2pdf->AddFont('dejavusans');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output('certificate_print.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>