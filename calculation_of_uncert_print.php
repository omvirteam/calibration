<?php
include("include/omConfig.php");

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
	if($haystack == null){
		return false;
	}else{
 foreach($haystack as $item) {
 	      if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
}
    return false;
}
$view_uncertainity = 0;
if(isset($_GET['withuncer'])){
    $view_uncertainity = 1;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////

ob_start();
?>
<style>
table {font-family: "Times New Roman"; font-size: 11.5px;}
tr {line-height: 21px;}
.main_table tr td {border: 1px solid black;}
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 0.5px solid black;
    
}
.main_table td, .main_table th {
    padding-top: 0.7px;
    padding-bottom: 0.7px;
    padding-left: 5px;
    padding-right: 5px;
    line-height: 10px;
    
}
.main_table{
    padding-bottom: 8px;
    
}
</style>

<?php
    $qry1 = 'SELECT ob.*,grndetail.certificate_no,grnmaster.grn_no,DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate FROM observation_data as ob
            LEFT JOIN grnmaster ON grnmaster.grnId = ob.grn_id
            LEFT JOIN grndetail ON grndetail.grnDetailId = ob.grn_detail_id
            WHERE ob.grn_id = '.$_GET['grnId'].' ORDER BY grndetail.grnDetailId ASC ';
    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
    $grnobsmaster_data = array();
    while($row1 = mysql_fetch_assoc($res1)) {
            if(!empty($row1['certificate_no'])){
                $row1['certificate_no'] = substr($row1['certificate_no'], -2);
            }
            array_push($grnobsmaster_data,$row1);
    }
    $grnobsmaster_data = array_combine(range(1, count($grnobsmaster_data)), array_values($grnobsmaster_data));
    $grnobsmaster_data_all = $grnobsmaster_data;
//    echo "<pre>"; print_r($grnobsmaster_data); exit;
    $max_main_key = max(array_keys($grnobsmaster_data)); 
    foreach ($grnobsmaster_data as $main_key => $grn_data){
//        echo "<pre>"; print_r($grn_data); exit;
        $master_data = json_decode(json_encode(json_decode($grn_data['master_json'])), true);
        $uuc_data = json_decode(json_encode(json_decode($grn_data['uuc_json'])), true);
        $type_a_type_b_json = json_decode(json_encode(json_decode($grn_data['type_a_type_b_json'])), true);
        $reading_json = json_decode(json_encode(json_decode($grn_data['reading_json'])), true);
        unset($grn_data['master_json']);
        unset($grn_data['uuc_json']);
        unset($grn_data['type_a_type_b_json']);
        unset($grn_data['reading_json']);
//        echo "<pre>"; print_r($reading_json); exit;
        $parameter = '';
        $qry11 = 'SELECT parameter FROM mastermetersub WHERE masterMeterSubId = '.$master_data[0]['mParameterId'].' ';
        $res11 = mysql_query($qry11) or die("Error :: Cannot select parameter details.<hr>".mysql_error());
        $grnobsmaster_data = array();
        while($row1 = mysql_fetch_assoc($res11)) {
                $parameter = $row1['parameter'];
        }
        $total_readings = max(array_keys($master_data)) + 1; 
        
        $decimal_master = 0;
        $resolution_of_master = $master_data[0]['resolution_of_master'];

        if(!empty($resolution_of_master)){
            $exp_val = explode('.', $resolution_of_master);
            if(isset($exp_val[1])){
                $decimal_master = strlen($exp_val[1]);  
            } else {
              $decimal_master = 0;  
            }
        } else {
            $decimal_master = 0;
        }
        $decimal_least = 0;
        $resolution_of_least = $uuc_data[0]['leastCount'];
        if(!empty($resolution_of_least)){
            $exp_least = explode('.', $resolution_of_least);
            if(isset($exp_least[1])){
                $decimal_least = strlen($exp_least[1]);  
            } else {
              $decimal_least = 0;  
            }
        } else {
            $decimal_least = 0;
        }
//        echo "<pre>"; print_r($uuc_data); exit;
        ?>
        
        <?php if($grn_data['table_info_id'] == '12'){ ?>
            <?php include("calculation_of_unce_6B.php"); ?>
        <?php } else { ?>
        <table width="750" align="center" class="main_table print-friendly"  style="font-size:11.5px;">
            <tr>
                <td colspan="<?= $total_readings + 2; ?>" align="center"><strong>CALCULATION OF UNCERTAINTY</strong></td>
            </tr>
            <tr>
                <td width="" align="center" style="border-top: 1px solid black !important; border-right: none !important;"></td>
                <td width="" style="border-top: 1px solid black !important; border-right: none !important; border-left: none !important;"></td>
                <td colspan="<?= $total_readings; ?>" align="right" style="border-top: 1px solid black !important;"><?php echo $grn_data['certificate_no']; ?></td>
            </tr>
            <tr>
                <td width="" align="center" colspan="2" ><strong><?php echo $parameter; ?></strong></td>
                <td colspan="<?= $total_readings; ?>" align="left">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong>GRN NO.</strong></td>
                <td colspan="<?= $total_readings; ?>" align="left"><?php echo $grn_data['grn_no']; ?></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong>Date</strong></td>
                <td colspan="<?= $total_readings; ?>" align="left"><?php echo $grn_data['callibrationDate']; ?></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong>Degree of freedom</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($master_data[$x]['degree_of_freedom']) ? $master_data[$x]['degree_of_freedom'] : '0.00'; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong>Uncertainty calibration certificate</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['un_cali_certi']) ? number_format((float)$type_a_type_b_json[$x]['un_cali_certi'] , 3, '.', '') : '0.000'; ?></td>
                <?php } ?>
            </tr>
            <?php if($grn_data['table_info_id'] == '4'){ ?>
                <tr>
                    <td colspan="2" align="center"><strong>Uncertainty calibration certificate</strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($type_a_type_b_json[$x]['un_cali_certi_2']) ? number_format((float)$type_a_type_b_json[$x]['un_cali_certi_2'] , 3, '.', '') : '0.000'; ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="2" align="center"><strong>Accuracy </strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['r_phase_accuracy']) ? number_format((float)$type_a_type_b_json[$x]['r_phase_accuracy'] , 4, '.', '') : '0.0000'; ?></td>
                <?php } ?>
            </tr>
            <?php if($grn_data['table_info_id'] == '4'){ ?>
                <tr>
                    <td colspan="2" align="center"><strong>Accuracy </strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($type_a_type_b_json[$x]['r_phase_accuracy_2']) ? number_format((float)$type_a_type_b_json[$x]['r_phase_accuracy_2'] , 4, '.', '') : '0.0000'; ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            <?php if($grn_data['table_info_id'] == '11'){ ?>
                <tr>
                    <td colspan="2" align="center"><strong>Repeatibility %</strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($uuc_data[$x]['repeatibility']) ? $uuc_data[$x]['repeatibility'] : ''; ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td colspan="2" align="center"><strong>Sensor Installation Error %</strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($uuc_data[$x]['sensor']) ? $uuc_data[$x]['sensor'] : ''; ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="2" align="center"><strong>&nbsp;</strong></td>
                <td colspan="<?= $total_readings; ?>" align="center"><strong>Type A Uncertainty</strong></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong>&nbsp;</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><strong><?php echo $x+1; ?></strong></td>
                <?php } ?>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong>UUC Least Count</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo $uuc_data[$x]['leastCount']; ?></td>
                <?php } ?>
            </tr>
            <?php 
            $max_std_meter_row = 0;
            foreach ($reading_json as $reading){
                if((max(array_keys($reading['stdMeter'])) + 1) > $max_std_meter_row){
                    $max_std_meter_row = max(array_keys($reading['stdMeter'])) + 1;
                }
            }
            ?>
            <?php for($i = 0; $i < $max_std_meter_row; $i++){ ?>
                <tr>
                    <td colspan="2" align="center"><strong><?php echo $i + 1; ?></strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <?php $reg =  isset($reading_json[$x]['stdMeter'][$i]) ?  $reading_json[$x]['stdMeter'][$i] : ''; ?>
                        <td align="center"><?php echo number_format((float)$reg , $decimal_least, '.', ''); ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            <tr>
                <td align="center"><strong>Mean reading</strong></td>
                <td align="center"><strong>Avg.</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['stdmeter_avg_new']) ? number_format((float)$type_a_type_b_json[$x]['stdmeter_avg_new'] , 4, '.', '') : '0.0000'; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td align="center"><strong>Standard deviation</strong></td>
                <td align="center"><strong>&nbsp;</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['std_dev']) ? number_format((float)$type_a_type_b_json[$x]['std_dev'] , 4, '.', '') : '0.0000'; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td align="center"><strong>Standard Uncertainty</strong></td>
                <td align="center"><strong>Ur</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['std_uncer']) ? number_format((float)$type_a_type_b_json[$x]['std_uncer'] , 4, '.', '') : '0.0000'; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td align="center"><strong>Standard Uncertainty</strong></td>
                <td align="center"><strong>% Ur</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['std_uncer_per']) ? number_format((float)$type_a_type_b_json[$x]['std_uncer_per'] , 4, '.', '') : '0.0000'; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td colspan="2" align="center"><strong>&nbsp;</strong></td>
                <td colspan="<?= $total_readings; ?>" align="center"><strong>Type B Uncertainty</strong></td>
            </tr>
            <tr>
                <td align="center"><strong>Confidence level</strong></td>
                <td align="center">95%</td>
                <td colspan="<?= $total_readings; ?>" align="center"><strong>&nbsp;</strong></td>
            </tr>
            <tr>
                <td align="center"><strong>Coverage factor</strong></td>
                <td align="center">1.96</td>
                <td colspan="<?= $total_readings; ?>" align="center"><strong>&nbsp;</strong></td>
            </tr>
            <?php if($grn_data['table_info_id'] == '4'){ ?>
                <tr>
                    <td align="center"><strong>Uncertinity of master</strong></td>
                    <td align="center"><strong>UB1</strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($type_a_type_b_json[$x]['uncer_master']) ? number_format((float)$type_a_type_b_json[$x]['uncer_master'] , 4, '.', '') : ''; ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td align="center"><strong>Uncertinity of master</strong></td>
                    <td align="center"><strong>UB2</strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($type_a_type_b_json[$x]['uncer_master_2']) ? number_format((float)$type_a_type_b_json[$x]['uncer_master_2'] , 4, '.', '') : ''; ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td align="center"><strong>Accuracy of cali. Certi.</strong></td>
                    <td align="center"><strong>UB3</strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($type_a_type_b_json[$x]['acc_calibration']) ? number_format((float)$type_a_type_b_json[$x]['acc_calibration'] , 4, '.', '') : ''; ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td align="center"><strong>Accuracy of cali. Certi.</strong></td>
                    <td align="center"><strong>UB4</strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($type_a_type_b_json[$x]['acc_calibration_2']) ? number_format((float)$type_a_type_b_json[$x]['acc_calibration_2'] , 4, '.', '') : ''; ?></td>
                    <?php } ?>
                </tr>
            <?php }  else { ?>
                <tr>
                    <td align="center"><strong>Uncertinity of master</strong></td>
                    <td align="center"><strong>UB1</strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($type_a_type_b_json[$x]['uncer_master']) ? number_format((float)$type_a_type_b_json[$x]['uncer_master'] , 4, '.', '') : ''; ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td align="center"><strong>Accuracy of cali. Certi.</strong></td>
                    <td align="center"><strong>UB2</strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($type_a_type_b_json[$x]['acc_calibration']) ? number_format((float)$type_a_type_b_json[$x]['acc_calibration'] , 4, '.', '') : ''; ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td align="center"><strong>Resolution</strong></td>
                    <td align="center"><strong>UB3</strong></td>
                    <?php for($x = 0; $x < $total_readings; $x++){ ?>
                        <td align="center"><?php echo isset($type_a_type_b_json[$x]['resolution_new']) ? number_format((float)$type_a_type_b_json[$x]['resolution_new'] , 4, '.', '') : ''; ?></td>
                    <?php } ?>
                </tr>
                <?php if($grn_data['table_info_id'] == '11' || $grn_data['table_info_id'] == '2' || $grn_data['table_info_id'] == '16' || $grn_data['table_info_id'] == '18'){ ?>
                    <tr>
                        <td align="center"><strong>Uncertainty of Repeatibility</strong></td>
                        <td align="center"><strong>UB4</strong></td>
                        <?php for($x = 0; $x < $total_readings; $x++){ ?>
                            <td align="center"><?php echo isset($type_a_type_b_json[$x]['uncer_of_repeat']) ? number_format((float)$type_a_type_b_json[$x]['uncer_of_repeat'] , 4, '.', '') : ''; ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                <?php if($grn_data['table_info_id'] == '11'){ ?>
                    <tr>
                        <td align="center"><strong>Uncertainty of Installation Error</strong></td>
                        <td align="center"><strong>UB5</strong></td>
                        <?php for($x = 0; $x < $total_readings; $x++){ ?>
                            <td align="center"><?php echo isset($type_a_type_b_json[$x]['uncer_of_install_error']) ? number_format((float)$type_a_type_b_json[$x]['uncer_of_install_error'] , 4, '.', '') : ''; ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                <?php if($grn_data['table_info_id'] == '2' || $grn_data['table_info_id'] == '16'){ ?>
                    <tr>
                        <td align="center"><strong>Uncertainty due to Hysteresis</strong></td>
                        <td align="center"><strong>UB6</strong></td>
                        <?php for($x = 0; $x < $total_readings; $x++){ ?>
                            <td align="center"><?php echo isset($type_a_type_b_json[$x]['uncertainty_hysteresis']) ? number_format((float)$type_a_type_b_json[$x]['uncertainty_hysteresis'] , 4, '.', '') : ''; ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                <?php if($grn_data['table_info_id'] == '18'){ ?>
                    <tr>
                        <td align="center"><strong>Reference Std.Weight</strong></td>
                        <td align="center"><strong>UB7</strong></td>
                        <?php for($x = 0; $x < $total_readings; $x++){ ?>
                            <td align="center"><?php echo isset($type_a_type_b_json[$x]['reference_weight']) ? number_format((float)$type_a_type_b_json[$x]['reference_weight'] , 4, '.', '') : ''; ?></td>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td align="center"><strong>Eccentric Loading</strong></td>
                        <td align="center"><strong>UB8</strong></td>
                        <?php for($x = 0; $x < $total_readings; $x++){ ?>
                            <td align="center"><?php echo isset($type_a_type_b_json[$x]['eccentric_loading']) ? number_format((float)$type_a_type_b_json[$x]['eccentric_loading'] , 4, '.', '') : ''; ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            <?php } ?>
            <tr>
                <td align="center"><strong>Combined Uncertainty</strong></td>
                <td align="center"><strong>Uc</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['com_uncer']) ? number_format((float)$type_a_type_b_json[$x]['com_uncer'] , 4, '.', '') : '0.0000'; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td align="center"><strong>Expanded Uncertainty</strong></td>
                <td align="center"><strong>Ue</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['exp_uncer']) ? number_format((float)$type_a_type_b_json[$x]['exp_uncer'] , 2, '.', '') : '0.00'; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td align="center"><strong>Calculation</strong></td>
                <td align="center"><strong>%Ue</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['exp_uncer_per_ue_without_cmc']) ? number_format((float)$type_a_type_b_json[$x]['exp_uncer_per_ue_without_cmc'] , 2, '.', '') : '0.00'; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td align="center"><strong>CMC</strong></td>
                <td align="center"><strong></strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['cmc']) ? number_format((float)$type_a_type_b_json[$x]['cmc'] , 2, '.', '') : '0.00'; ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td align="center"><strong>Expanded Uncertainty</strong></td>
                <td align="center"><strong>%Ue</strong></td>
                <?php for($x = 0; $x < $total_readings; $x++){ ?>
                    <td align="center"><?php echo isset($type_a_type_b_json[$x]['exp_uncer_per_ue']) ? number_format((float)$type_a_type_b_json[$x]['exp_uncer_per_ue'] , 2, '.', '') : '0.00'; ?></td>
                <?php } ?>
            </tr>
        </table>
        <?php } ?>
        <?php $nextkey = $main_key + 1; ?>
        <?php if(isset($grnobsmaster_data_all[$nextkey])){ ?>
            <?php if ($main_key % 2 == 0) { ?>
                <div style="page-break-after:always; clear:both"></div>
            <?php } else { ?>
                
            <?php } ?>
        <?php } ?>
        
        <?php 
//        echo "<pre>"; print_r($master_data); exit;
    }
//    echo "<pre>"; print_r($total_table_array); exit;
//        echo "<pre>"; print_r($page_array); exit;
?>


<?php
//exit;
// html - end //
$content = ob_get_clean();

// convert in PDF
require_once 'html2pdf/html2pdf.class.php';
try {
    $html2pdf = new HTML2PDF('P', 'A4', 'fr',true,'UTF-8',array(7, 7, 7, 7));
    $html2pdf->pdf->SetTitle('Calculation of Uncertainty');
    $html2pdf->shrink_tables_to_fit = 1;    // Default: false
    $html2pdf->setDefaultFont('Times');
    $html2pdf->AddFont('dejavusans');
    $html2pdf->AddFont('dejavusansb');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output('certificate_print.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>