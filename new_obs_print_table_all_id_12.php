﻿<table width="605" align="left" class="main_table"  style="font-size:13px;">
                <?php
                $is_3_phase = 0;
                $table_title = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(!empty($grn_entry['table_title'])){
                        $table_title = $grn_entry['table_title'];
                        break;
                    }
                }
                $table_title_right = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                        $table_title_right = $grn_entry['table_title_right'];
                        break;
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                $t_pre_range = '';
                $t_pre_range_key = '';
                $t_pre_range_total = '';
//                echo "<pre>"; print_r($grn_data); exit; 
                ?>
                <?php 
//                    $master_unit_type1 = '';
//                    $master_range1 = '';
//                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
//                            WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
//                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
//                    while($row1 = mysql_fetch_assoc($res1)) { 
//                        $master_unit_type1 = $row1['range_unit'];
//                        $master_range1 = $row1['rangeValue'];
//                    }
//                    $uut_range1 = $grn_data[0]['uut_range'];
                    $ct_class_name = '';
                    $qryc1 = 'SELECT ct_class  FROM accuracy_class 
                    WHERE id = '.$grn_data[0]['ct_class'].' ';
                    $resc1 = mysql_query($qryc1) or die("Error :: Cannot select ct name details.<hr>".mysql_error());
                    while($rowc1 = mysql_fetch_assoc($resc1)) { 
                        $ct_class_name = $rowc1['ct_class'];
                    }
                    $pase_colspan = 2;
                    $is_empty_phase = 0;
                    if(!empty($grn_entry['testMeter_avg10']) || !empty($grn_entry['testMeter_avg9']) || !empty($grn_entry['testMeter_avg8']) || !empty($grn_entry['testMeter_avg7']) || !empty($grn_entry['testMeter_avg6'])){
                        $pase_colspan = 4;
                        $is_empty_phase = 1;
                    }
                ?>
                <tr>
                    <th align="center" valign="middle">Title:</th>
                    <th  colspan="<?= $pase_colspan; ?>" align="left" valign="middle" style="font-family:dejavusansb; font-size: 11px;">&nbsp; <?php echo $table_title_right; ?></th>
                    <th  colspan="2" align="left" valign="middle">C.T. Class : <?php echo $ct_class_name; ?></th>
                </tr>
                <tr>
                    <th valign="middle" align="center" >Sr. No.</th>
                    <th align="center" valign="middle" >Set Burden<br/>VA / %</th>
                    <th align="center" valign="middle">Load %</th>
                    <th align="center" valign="middle">Allowed Ratio<br/>Limits ± %</th>
                    <th align="center" valign="middle">Observed Ratio<br/>Error ± %</th>
                    <?php if(!empty($is_empty_phase)){ ?>
                        <th align="center" valign="middle">Allowed Phase<br/>Limits ± min.</th>
                        <th align="center" valign="middle">Observed Phase<br/>Error ± min.</th>
                    <?php } ?>
                </tr>
                <?php 
				$uncertPrint = "No";
                    foreach ($grn_data as $k => $grn_entry){
                    
                    // ct class data : start //
                    $ct_class_data = array();
                    $qryc = 'SELECT *  FROM accuracy_class 
                            WHERE id = '.$grn_entry['ct_class'].' ';
                    $resc = mysql_query($qryc) or die("Error :: Cannot select ct details.<hr>".mysql_error());
                    while($rowc = mysql_fetch_assoc($resc)) { 
                        $ct_class_data = $rowc;
                    }
                    // ct class data : end //
                    if($grn_entry['ct_class'] == '1' || $grn_entry['ct_class'] == '2' || $grn_entry['ct_class'] == '3' || $grn_entry['ct_class'] == '4'){
                        $grn_entry['row_span'] = '4';
                    } else {
                        $grn_entry['row_span'] = '5';
                    }
//                    echo "<pre>"; print_r($grn_entry); exit; 
                    
                    $i = $k + 1;
//                    $master_hz = '';
//                    $master_unit_type = '';
//                    $master_range = '';
//                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
//                            WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
//                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
//                    while($row1 = mysql_fetch_assoc($res1)) { 
//                        $master_hz = $row1['frequency'];
//                        $master_unit_type = $row1['range_unit'];
//                        $master_range = $row1['rangeValue'];
//                    }
                    
                    ?>
                        <?php 
                            $decimal_master = 0;
                            $resolution_of_master = $grn_entry['resolution_of_master'];
                           if(!empty($resolution_of_master)){
                               $exp_val = explode('.', $resolution_of_master);
                               if(isset($exp_val[1])){
                                   $decimal_master = strlen($exp_val[1]);  
                               } else {
                                 $decimal_master = 0;  
                               }
                           } else {
                               $decimal_master = 0;
                           }
                           $decimal_least = 0;
                           $resolution_of_least = $grn_entry['leastCount'];
                           if(!empty($resolution_of_least)){
                               $exp_least = explode('.', $resolution_of_least);
                               if(isset($exp_least[1])){
                                   $decimal_least = strlen($exp_least[1]);  
                               } else {
                                 $decimal_least = 0;  
                               }
                           } else {
                               $decimal_least = 0;
                           }
                        ?>
                    
                        <?php 
                            $x_val = ($grn_entry['row_span'] == '4') ? '4' : '5';
//                            echo $x_val; exit;
//                            echo "<pre>"; print_r($grn_entry); exit;
                            $inc_x = 1;
                            for($x = 1; $x <= $x_val; $x++) { ?>
                            <tr>
                            <?php if(!empty($grn_entry['testMeter_avg5']) || !empty($grn_entry['testMeter_avg10'])){ ?>
                                <?php if($x == 1){ ?>
                                    <td align="center" valign="middle"><?php echo $inc_x; ?></td>
                                    <td valign="middle" ><div style="word-break: break-all; width: 80px; text-align: center; font-family:dejavusans; font-size: 11px;" ><?php echo $grn_entry['burden_6b']; ?></div></td>
                                <?php $inc_x = $inc_x + 1; } ?>
                            <?php } ?>
                            <?php if(!empty($grn_entry['testMeter_avg4']) || !empty($grn_entry['testMeter_avg9'])){ ?>
                                <?php if($x == 2){ ?>
                                    <td align="center" valign="middle"><?php echo $inc_x; ?></td>
                                    <td valign="middle" ><div style="word-break: break-all; width: 80px; text-align: center; font-family:dejavusans; font-size: 11px;" ><?php echo $grn_entry['burden_6b']; ?></div></td>
                                <?php $inc_x = $inc_x + 1; } ?>
                            <?php } ?>
                            <?php if(!empty($grn_entry['testMeter_avg3']) || !empty($grn_entry['testMeter_avg8'])){ ?>
                                <?php if($x == 3){ ?>
                                    <td align="center" valign="middle"><?php echo $inc_x; ?></td>
                                    <td valign="middle" ><div style="word-break: break-all; width: 80px; text-align: center; font-family:dejavusans; font-size: 11px;" ><?php echo $grn_entry['burden_6b']; ?></div></td>
                                <?php $inc_x = $inc_x + 1; } ?>
                            <?php } ?>
                            <?php if(!empty($grn_entry['testMeter_avg2']) || !empty($grn_entry['testMeter_avg7'])){ ?>
                                <?php if($x == 4){ ?>
                                    <td align="center" valign="middle"><?php echo $inc_x; ?></td>
                                    <td valign="middle" ><div style="word-break: break-all; width: 80px; text-align: center; font-family:dejavusans; font-size: 11px;" ><?php echo $grn_entry['burden_6b']; ?></div></td>
                                <?php $inc_x = $inc_x + 1; } ?>
                            <?php } ?>
                            <?php if(!empty($grn_entry['testMeter_avg']) || !empty($grn_entry['testMeter_avg6'])){ ?>
                                <?php if($x == 5){ ?>
                                    <td align="center" valign="middle"><?php echo $inc_x; ?></td>
                                    <td valign="middle" ><div style="word-break: break-all; width: 80px; text-align: center; font-family:dejavusans; font-size: 11px;" ><?php echo $grn_entry['burden_6b']; ?></div></td>
                                <?php $inc_x = $inc_x + 1; } ?>
                            <?php } ?>
                            <?php if($x == 1){ ?>
                                <!--<td valign="middle" rowspan="<?= ($x == 1) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px; text-align: center;" ><?php echo $grn_entry['uut_range']; ?></div></td>-->
                            <?php } ?>
                            <?php if(!empty($grn_entry['testMeter_avg5']) || !empty($grn_entry['testMeter_avg10'])){ ?>
                                <?php if($x == 1){ ?>
                                    <td align="center" valign="middle">120%</td>
                                    <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['120_ratio_error'], $decimal_least, '.', ''); ?></td>
                                    <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg5'], $decimal_least, '.', ''); ?></td>
                                    <?php if(!empty($is_empty_phase)){ ?>
                                        <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['120_phase_error'], $decimal_least, '.', ''); ?></td>
                                        <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg10'], $decimal_least, '.', ''); ?></td>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <?php if(!empty($grn_entry['testMeter_avg4']) || !empty($grn_entry['testMeter_avg9'])){ ?>
                                <?php if($x == 2){ ?>
                                    <td align="center" valign="middle">100%</td>
                                    <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['100_ratio_error'], $decimal_least, '.', ''); ?></td>
                                    <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg4'], $decimal_least, '.', ''); ?></td>
                                    <?php if(!empty($is_empty_phase)){ ?>
                                        <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['100_phase_error'], $decimal_least, '.', ''); ?></td>
                                        <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg9'], $decimal_least, '.', ''); ?></td>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <?php if(!empty($grn_entry['testMeter_avg3']) || !empty($grn_entry['testMeter_avg8'])){ ?>
                                <?php if($x == 3){ ?>
                                    <td align="center" valign="middle">20%</td>
                                    <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['20_ratio_error'], $decimal_least, '.', ''); ?></td>
                                    <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg3'], $decimal_least, '.', ''); ?></td>
                                    <?php if(!empty($is_empty_phase)){ ?>
                                        <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['20_phase_error'], $decimal_least, '.', ''); ?></td>
                                        <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg8'], $decimal_least, '.', ''); ?></td>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <?php if(!empty($grn_entry['testMeter_avg2']) || !empty($grn_entry['testMeter_avg7'])){ ?>
                                <?php if($x == 4){ ?>
                                    <td align="center" valign="middle">5%</td>
                                    <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['5_ratio_error'], $decimal_least, '.', ''); ?></td>
                                    <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg2'], $decimal_least, '.', ''); ?></td>
                                    <?php if(!empty($is_empty_phase)){ ?>
                                        <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['5_phase_error'], $decimal_least, '.', ''); ?></td>
                                        <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg7'], $decimal_least, '.', ''); ?></td>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                           <?php if(!empty($grn_entry['testMeter_avg']) || !empty($grn_entry['testMeter_avg6'])){ ?>             
                                <?php if($x == 5){ ?>
                                    <td align="center" valign="middle">1%</td>
                                    <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['1_ratio_error'], $decimal_least, '.', ''); ?></td>
                                    <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg'], $decimal_least, '.', ''); ?></td>
                                    <?php if(!empty($is_empty_phase)){ ?>
                                        <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['1_phase_error'], $decimal_least, '.', ''); ?></td>
                                        <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg6'], $decimal_least, '.', ''); ?></td>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                                </tr>
                        <?php } 
						if($uncertPrint == "No")
						{
							$uncertPrint = "Yes";
						}
						else
						{
						?>
                <tr>
                    <td align="center" valign="middle" colspan="4">Max Expanded Uncert. in %</td>
                    <td align="center" valign="middle" colspan="1"><?php echo number_format((float)$max_ex_unce_p, 2, '.', ''); ?></td>
                    <?php if(!empty($is_empty_phase)){ ?>
                    <td align="center" valign="middle" colspan="1" ></td>
                    <td align="center" valign="middle" colspan="1" ></td>
                    <?php } ?>
                </tr>
                    
                <?php 
						}//else of if($uncertPrint == "No")
				}
                ?>
            </table>