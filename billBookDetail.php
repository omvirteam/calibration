<?php
require('./fpdf.php');
include("include/conn.php");
require('include/Smarty/libs/Smarty.class.php');
$billBookDetail = array();
$total          = array();
$partyId        = 0;
$totNetWeight   = 0;
$billBookNo     = 0;
$showPrev       = 0;
$partyName      = "";

$pdf = new FPDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();

$selectParty = "SELECT party.partyId,party.partyName,billbook.billBookId,billbook.billNo,
                       DATE_FORMAT(billbook.billBookDate,'%d-%m-%Y') AS billBookDate,billbook.showPrev
                  FROM party
                  JOIN billbook ON billbook.partyId = party.partyId
                 WHERE billbook.billBookId = ".$_GET['billBookId'];
$selectPartyRes = mysql_query($selectParty);
if($partyRow = mysql_fetch_assoc($selectPartyRes))
{
  $partyId      = $partyRow['partyId'];
  $billBookNo   = $partyRow['billNo'];
  $partyName    = $partyRow['partyName'];
  $billBookDate = $partyRow['billBookDate'];
  $showPrev     = $partyRow['showPrev'];
}

$selectBillBook = "SELECT billbookdetail.particulars,
                          billbookdetail.weight,billbookdetail.pari,billbookdetail.netWeight,billbookdetail.touch,billbookdetail.waste,billbookdetail.fine,
                          billbookdetail.qty,billbookdetail.labour,billbookdetail.amount 
                     FROM billbookdetail 
                    WHERE billbookdetail.billBookId = ".$_GET['billBookId']."
                    ORDER BY billbookdetail.billBookDetailId";
$selectBillBookRes = mysql_query($selectBillBook);
$a = 0;
$total['netWeight'] = 0;
$total['fine']      = 0;
$total['qty']       = 0;
$total['labour']    = 0;
$total['amount']    = 0;

while($dataRow = mysql_fetch_assoc($selectBillBookRes))
{
  $billBookDetail[$a]['billBookId']  = $_GET['billBookId'];
  $billBookDetail[$a]['particulars'] = $dataRow['particulars'];
  $billBookDetail[$a]['weight']      = $dataRow['weight'];
  $billBookDetail[$a]['pari']        = $dataRow['pari'];
  $billBookDetail[$a]['netWeight']   = $dataRow['netWeight'];
  $billBookDetail[$a]['touch']       = $dataRow['touch'];
  $billBookDetail[$a]['waste']       = $dataRow['waste'];
  $billBookDetail[$a]['fine']        = $dataRow['fine'];
  $billBookDetail[$a]['qty']         = $dataRow['qty'];
  $billBookDetail[$a]['labour']      = $dataRow['labour'];
  $billBookDetail[$a]['amount']      = $dataRow['amount'];
  
  $total['netWeight'] += $dataRow['netWeight'];
  $total['fine']      += $dataRow['fine'];
  $total['qty']       += $dataRow['qty'];
  $total['labour']    += $dataRow['labour'];
  $total['amount']    += $dataRow['amount'];
  $a++;
}


$prevFine = 0;
$prevAmount = 0;
$selectVeepariBook = "SELECT  vepaaribilldetail.fine, vepaaribilldetail.amount, vepaaribilldetail.CrDr,vepaaribillmaster.partyId,vepaaribillmaster.vepaariBookDate 
                        FROM  vepaaribilldetail
                        JOIN  vepaaribillmaster
                       WHERE vepaaribillmaster.partyId = ".$partyId."
                       ORDER BY vepaariBookDate, vepaariBookDetailId";
$selectVeepariBookRes = mysql_query($selectVeepariBook);
while($row = mysql_fetch_assoc($selectVeepariBookRes))
{
	if($row['CrDr'] == "Dr")
	{
		$prevFine   += $row['fine'];
    $prevAmount += $row['amount'];
	}
	else
	{
		$prevFine   -= $row['fine'];
    $prevAmount -= $row['amount'];
	}
}

$billBook = "SELECT totalFine,totalAmount FROM billbook
              WHERE partyId = ".$partyId."
              ORDER BY billBookId";
$billBookRes = mysql_query($billBook);
while($billBook = mysql_fetch_assoc($billBookRes))
{
	$prevFine   += $billBook['totalFine'];
  $prevAmount += $billBook['totalAmount'];
}

//$prevFine += $total['fine'];
//$prevAmount += $total['amount'];

$smarty = new Smarty;
require("bottom.php");
$smarty->assign("l_party",$l_party);
$smarty->assign("l_billBookDate",$l_billBookDate);
$smarty->assign("l_particulars",$l_particulars);
$smarty->assign("l_weight",$l_weight);
$smarty->assign("l_pari",$l_pari);
$smarty->assign("l_netWeight",$l_netWeight);
$smarty->assign("l_touch",$l_touch);
$smarty->assign("l_waste",$l_waste);
$smarty->assign("l_fine",$l_fine);
$smarty->assign("l_qty",$l_qty);
$smarty->assign("l_labour",$l_labour);
$smarty->assign("l_amount",$l_amount);
$smarty->assign("l_total",$l_total);
$smarty->assign("l_billBookDetailId",$l_billBookDetailId);

$smarty->assign("billBookDetail",$billBookDetail);
$smarty->assign("billBookNo",$billBookNo);
$smarty->assign("partyName",$partyName);
$smarty->assign("billBookDate",$billBookDate);
$smarty->assign("total",$total);
$smarty->assign("showPrev",$showPrev);
$smarty->assign("prevFine",$prevFine);
$smarty->assign("prevAmount",$prevAmount);

$smarty->display("billBookDetail.tpl");
?>