<?php
require('./fpdf.php');
include("include/omConfig.php");
class ConductPDF extends FPDF {
    function vcell($c_width, $c_height, $x_axis,$yPosition, $text,$lengthToSplit) {
        $w_w = $c_height / 3;
        $w_w_1 = $w_w + 2;
        $w_w1 = $w_w + $w_w + $w_w + 3;
        $len = strlen($text); // check the length of the cell and splits the text into 7 character each and saves in a array 

        if ($len > $lengthToSplit) {
            $w_text = str_split($text, $lengthToSplit);
            $this->SetXY($x_axis,$yPosition);
            $this->Cell($c_width, $w_w_1, $w_text[0], '', '', '');
            if (isset($w_text[1])) {
                $this->SetXY($x_axis,$yPosition);
                $this->Cell($c_width, $w_w1, $w_text[1], '', '', '');
            }
            $this->SetXY($x_axis,$yPosition);
            $this->Cell($c_width, $c_height, '', 'LTRB', 0, 'L', 0);
        } else {
            $this->SetXY($x_axis,$yPosition);
            $this->Cell($c_width, $c_height, $text, 'LTRB', 0, 'L', 0);
        }
    }

}

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = "";
  $grnDetailId         = "";
  $grnPrefix           = "";
  $grnNo               = "";
  $grnDate             = "";
  $poNo                = "";
  $poDate              = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();
  // Pdf Create :Start
//  $pdf = new FPDF('P','mm','A4');
  $pdf = new ConductPDF('P','mm','A4');
  $pdf->AliasNbPages();
  $pdf->AddPage();
    
  //SELECT OF GRN MASTER :START
  $selectGrnMaster = "SELECT grnPrefix,grnNo,grn_no,DATE_FORMAT(grnDate,'%d-%m-%Y') AS grnDate,poNo,DATE_FORMAT(poDate,'%d-%m-%Y') AS poDate,
                             custName,address,custCode,remarks,grnmaster.mrAndMrs,grnmaster.contPerson,phNo,userName
                        FROM grnmaster
                        JOIN customer
                       WHERE grnId = ".$_GET['grnId']."
                         AND grnmaster.customerId = customer.customerId";
  $selectGrnMasterRes = mysql_query($selectGrnMaster);
  if($selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes))
  {
   
  }
  //SELECT OF GRN MASTER :END
  if(strlen($selectGrnMasterRow['address']) > 90)
  {
//    $addPart1 = substr($selectGrnMasterRow['address'], 0, 85);
//    $addPart2 = substr($selectGrnMasterRow['address'], 85, 85);
    $line = $selectGrnMasterRow['address'];
if (preg_match('/^.{1,90}\b/s', $selectGrnMasterRow['address'], $match)){
    $addPart1 = $match[0];
    $addPart2 = substr($selectGrnMasterRow['address'], strlen($addPart1), 100);
}
//echo $line2; exit;
  }
  else
  {
    $addPart1 = $selectGrnMasterRow['address'];
	$addPart2 = "";
  }

  //SELECT OF GRN DETAIL :START
  for($z=0; $z < 2; $z++)
  {
    $a  = 0;
    $selectGrnEntry  = "SELECT grndetail.grnDetailId,grndetail.grnId,item.itemName,grndetail.itemCode,grndetail.rangeValue,grndetail.description,grndetail.challan,grndetail.received,
                               grndetail.grnCondition,DATE_FORMAT(custReqDate,'%d-%m-%Y') AS custReqDate,DATE_FORMAT(expDelivDate,'%d-%m-%Y') AS expDelivDate,
                               parameterentry.parameterName
                          FROM grndetail
                          JOIN item
                          JOIN parameterentry
                         WHERE grnId = ".$_GET['grnId']."
                           AND grndetail.itemId = item.itemId
                           AND grndetail.parameterId = parameterentry.parameterId
                         ORDER BY grndetail.grnDetailId";
    $selectGrnEntryRes   = mysql_query($selectGrnEntry);
//    echo $selectGrnEntry; exit;
    while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
    {
    	if(mysql_num_rows($selectGrnEntryRes) > 8)
    	{
        if(($a % 34) == 0)
        {
          $yPosition = 75;
          if($a > 0)
          {
            pageFooter1($lastYposition);
            $pdf->AddPage();
          }
          if($z == 0)
            pageHeader1('office');
          else
            pageHeader1('customer');
        }
        $lastYposition = grnDetailFunc1($yPosition,$a+1);
        $yPosition += 5;
    	}
    	else
    	{
        if(($a % $cfgRecPerGrnPage) == 0)
        {
          $yPosition = 75;
          $y2Position = 215;
          if($a > 0)
          $pdf->AddPage();
          pageHeader1('office');
          pageHeader2();
        }
        grnDetailFunc1($yPosition,$a+1);
        grnDetailFunc2($yPosition,$y2Position,$a+1);
        $yPosition += 5;
        $y2Position += 5;
        
        pageFooter1(105);
        pageFooter2();
      }
      $a++;
    }
    if(mysql_num_rows($selectGrnEntryRes) > 8)
      pageFooter1($lastYposition);
    
    if($z == 0 && mysql_num_rows($selectGrnEntryRes) > 8)
      $pdf->AddPage();
  }
  $pdf->output();
  include("./bottom.php");
}

function pageHeader1($head)
{
  global $pdf;
  global $selectGrnMasterRow,$addPart1,$addPart2;
  $pdf->Image('./images/logo_newaddr.jpg',122,1,70,28);
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(85,5);
  
  if($head == 'office')
    $pdf->Write(1,'Office Copy');
  else
    $pdf->Write(1,'Customer Copy');
    
  $pdf->SetFont('Arial','',13);
  $pdf->SetXY(12,30);
  $pdf->Write(1,'GOODS RECEIPT NOTE');
  $pdf->SetXY(175,30);
  $pdf->Write(1,'ST F 01');
  $pdf->SetXY(12,34);
  $pdf->SetFont('Arial','',10);
  $pdf->Write(5,'GrnNo : '.$selectGrnMasterRow['grn_no']);
  $pdf->SetXY(45,34);
  $pdf->Write(5,'Date  : '.($selectGrnMasterRow['grnDate']));
  $pdf->SetXY(100,34);
  $pdf->Write(5,'P.O/Letter No : '.($selectGrnMasterRow['poNo']));
  if($selectGrnMasterRow['poNo'] != 'No Letter')
  {
    $pdf->SetXY(163,34);
    $pdf->Write(5,'Date : '.($selectGrnMasterRow['poDate']));
  }
  else
  {
    $pdf->SetXY(163,34);
    $pdf->Write(5,'');
  }
  $pdf->SetXY(12,39);
  $pdf->Write(5,'Customer Name : '.($selectGrnMasterRow['custName']));
   $pdf->SetXY(12,44);
   $pdf->SetFont('Arial','',8);
  $pdf->Write(5,'Customer Address : '.(preg_replace('/\s\s+/', ' ',$addPart1)));
  $pdf->SetXY(38,47);
  $pdf->Write(5,(preg_replace('/\s\s+/', ' ',$addPart2)));
//  $pdf->vcell(150, 5, 12,44, 'Customer Address : '.(preg_replace('/\s\s+/', ' ',$selectGrnMasterRow['address'])),100);
 $pdf->SetFont('Arial','',10);
  
  $pdf->SetXY(162,39);
  $pdf->Write(5,'Code : '.($selectGrnMasterRow['custCode']));
  $pdf->SetXY(12,51);
  $pdf->Write(5,'Contact Person  : '.($selectGrnMasterRow['mrAndMrs']).'.  '.($selectGrnMasterRow['contPerson']));
  
  $pdf->SetXY(158,51);
  $pdf->Write(5,'Ph. No. : '.($selectGrnMasterRow['phNo']));
  $pdf->SetXY(13,60);
  $pdf->cell(180,05,' FOLLOWING MATERIALS RECEIVED FOR - CALIBRATION / VERIFICATEION:','1', '0', 'C');

  $pdf->SetFont('Arial','',7);
  $pdf->SetXY(13,70);
  $pdf->cell(11,05,'SR.NO.','1', '0', 'L');
  $pdf->SetXY(24,70);
  $pdf->cell(31,05,'ITEM CODE','1', '0', 'L');
  $pdf->SetXY(56,70);
  $pdf->cell(18,05,'DESCRIPTION','', '0', 'L');
  // Quantity part :Start
  $pdf->SetXY(134,65);
  $pdf->cell(59,05,'QUANTITY','1', '0', 'C');
  $pdf->SetXY(13,65);
  $pdf->cell(180,05,'','1', '0', 'L');
  //Quantity part :END
  $pdf->SetXY(134,70);
  $pdf->cell(16,05,'CHALLAN','1', '0', 'L');
  $pdf->SetXY(150,70);
  $pdf->cell(18,05,'RECEIVED','1', '0', 'L');
  $pdf->SetXY(168,70);
  $pdf->cell(25,05,'CONDITION','1', '0', 'L');
}
function pageFooter1($lastYposition)
{
  global $pdf;
  global $selectGrnMasterRow;
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(12,$lastYposition + 13);
  $pdf->Write(5,'Remarks : '.($selectGrnMasterRow['remarks']));
  $pdf->SetXY(12,$lastYposition + 20);
  $pdf->Write(5,'Received By : '.($selectGrnMasterRow['userName']));
  $pdf->SetXY(80,$lastYposition + 20);
  $pdf->Write(5,'Customers :(SIGN.&DATE) : '.('_____________'));
  $pdf->SetXY(12,$lastYposition + 30);
  $pdf->Write(1,'For delivery of materials this goods Receipt required,otherwise material will not be returned.');
}
function pageHeader2()
{
  global $pdf;
  global $selectGrnMasterRow,$addPart1,$addPart2;
  $pdf->Image('./images/logo_newaddr.jpg',122,142,70,30);
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(85,145);
  $pdf->Write(1,'Customer Copy');

  $pdf->SetXY(12,176);
  $pdf->Write(1,'GOODS RECEIPT NOTE');
  $pdf->SetXY(175,176);
  $pdf->Write(1,'ST F 01');
  $pdf->SetXY(12,180);
  $pdf->SetFont('Arial','',10);
  $pdf->Write(5,'GrnNo : '.$selectGrnMasterRow['grn_no']);
  $pdf->SetXY(45,180);
  $pdf->Write(5,'Date  : '.($selectGrnMasterRow['grnDate']));
  $pdf->SetXY(100,180);
  $pdf->Write(5,'P.O/Letter No : '.($selectGrnMasterRow['poNo']));
  if($selectGrnMasterRow['poNo'] != 'No Letter')
  {
    $pdf->SetXY(163,179);
    $pdf->Write(5,'Date : '.($selectGrnMasterRow['poDate']));
  }
  $pdf->SetXY(12,184);
  $pdf->Write(5,'Customer Name : '.($selectGrnMasterRow['custName']));
 
  $pdf->SetXY(162,184);
  $pdf->Write(5,'Code : '.($selectGrnMasterRow['custCode']));
  
  $pdf->SetFont('Arial','',8);
  $pdf->SetXY(12,188);
  $pdf->Write(5,'Customer Address : '.(preg_replace('/\s\s+/', ' ',$selectGrnMasterRow['address'])));
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(12,192);
  $pdf->Write(5,'Contact Person  : '.($selectGrnMasterRow['mrAndMrs']).'.  '.($selectGrnMasterRow['contPerson']));
 
  $pdf->SetXY(158,192);
  $pdf->Write(5,'Ph. No. : '.($selectGrnMasterRow['phNo']));
  
  $pdf->SetXY(13,200);
  $pdf->cell(180,05,' FOLLOWING MATERIALS RECEIVED FOR - CALIBRATION / VERIFICATEION:','1', '0', 'C');

  $pdf->SetFont('Arial','',7);
  $pdf->SetXY(13,210);
  $pdf->cell(11,05,'SR.NO.','1', '0', 'L');
 
  $pdf->SetXY(24,210);
  $pdf->cell(31,05,'ITEM CODE','1', '0', 'L');
  
  $pdf->SetXY(56,210);
  $pdf->cell(18,05,'DESCRIPTION','', '0', 'L');
  
  // Quantity part :Start
  $pdf->SetXY(134,205);
  $pdf->cell(59,05,'QUANTITY','1', '0', 'C');
  $pdf->SetXY(13,205);
  $pdf->cell(180,05,'','1', '0', 'L');
  //Quantity part :END
  
  $pdf->SetXY(134,210);
  $pdf->cell(16,05,'CHALLAN','1', '0', 'L');
  
  $pdf->SetXY(150,210);
  $pdf->cell(18,05,'RECEIVED','1', '0', 'L');

  $pdf->SetXY(168,210);
  $pdf->cell(25,05,'CONDITION','1', '0', 'L');
}
function pageFooter2()
{
  global $pdf;
  global $selectGrnMasterRow;
  
  $pdf->SetXY(10,260);
  $pdf->Write(5,'Remarks : '.($selectGrnMasterRow['remarks']));
  $pdf->SetXY(10,265);
  $pdf->Write(5,'Received By : '.($selectGrnMasterRow['userName']));
  $pdf->SetXY(80,265);
  $pdf->Write(5,'Customers :(SIGN.&DATE) : '.('_____________'));
  $pdf->SetXY(10,275);
  $pdf->Write(1,'For delivery of materials this goods Receipt required,otherwise material will not be returned.');
}

function grnDetailFunc1($yPosition,$srNo){
    global $pdf;
    global $selectGrnEntryResRow;
    $pdf->SetFont('Arial','',7);
    $pdf->vcell(11,05,13,$yPosition,$srNo,'4');// pass all values inside the cell 
    $pdf->vcell(31,05,24,$yPosition,$selectGrnEntryResRow['itemCode'],'20');
    $pdf->vcell(79,05,55,$yPosition,$selectGrnEntryResRow['itemName'],'45');
    $pdf->vcell(16,05,134,$yPosition,$selectGrnEntryResRow['challan'],'5');
    $pdf->vcell(18,05,150,$yPosition,$selectGrnEntryResRow['received'],'5');
    $pdf->vcell(25,05,168,$yPosition,$selectGrnEntryResRow['grnCondition'],'12');
    $pdf->Ln();
    return $yPosition;
}
function grnDetailFunc2($yPosition,$y2Position,$srNo){
    global $pdf;
    global $selectGrnEntryResRow;
    $pdf->vcell(11,05,13,$y2Position,$srNo,'4');// pass all values inside the cell 
    $pdf->vcell(31,05,24,$y2Position,$selectGrnEntryResRow['itemCode'],'20');
    $pdf->vcell(79,05,55,$y2Position,$selectGrnEntryResRow['itemName'],'45');
    $pdf->vcell(16,05,134,$y2Position,$selectGrnEntryResRow['challan'],'5');
    $pdf->vcell(18,05,150,$y2Position,$selectGrnEntryResRow['received'],'5');
    $pdf->vcell(25,05,168,$y2Position,$selectGrnEntryResRow['grnCondition'],'12');
    $pdf->Ln();
}
?>