<?php
include("include/omConfig.php");

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
	if($haystack == null){
		return false;
	}else{
 foreach($haystack as $item) {
 	      if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
}
    return false;
}
// general functions - start //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// variable list - start //
$certificateNo 			= '';
$certificate_no			= '';
$grnNo 					= '';
$grn_no 					= '';
$grnDate				= ''; 
$totalPages 			= 0;
$callibrationDate		= '';
$callibrationDateNext 	= '';
$certificateIssue		= '';
$certificateIssueDate	= '';
$customerName			= '';
$customerAddress		= '';
$customerCity		= '';
$customerRefNo			= '';
$ambientTemperature		= '';
$relativeHumidity		= '';
$remarks				= '';

$counter 				= 0;
$grnDetailIds 			= array();
$tempArr 				= array() ;

$instrumentDetails					= array();
$instrumentDetails['item_name']		= '';
$instrumentDetails['make']			= '';
$instrumentDetails['item_code']		= '';
$instrumentDetails['id_no']			= '';
$instrumentDetails['range_value']	= '';
$instrumentDetails['accuracy']		= '';
$instrumentDetails['grn_condition']	= '';
$instrumentDetails['certi_remarks']	= '';
$instrumentDetails['cali_location']	= '';
$instrumentDetails['uuc_location']	= '';
$instrumentDetails['ulr_no']	= '';
$instrumentDetails['userName']	= '';
$instrumentDetails['approved_by']	= '';
$instrumentDetails['designation']	= '';
$instrumentDetails['sr_no']	= '';

$masterMeters						= array();
$masterMeters['masterMeterName']			= array();
$masterMeters['masterMeterIdNo']				= array();
$masterMeters['masterMeterMake']			= array();
$masterMeters['masterMeterModelNo']				= array();
$masterMeters['masterMeterSerialNo']			= array();
$masterMeters['masterMeterCertificateNo']		= array();
$masterMeters['due_date']	= array();

$observations						= array();
// variable list - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// fetch data - start //
$qry = 'SELECT DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate,
	DATE_FORMAT(grndetail.nextYearDate,"%d/%m/%Y") AS nextYearDate,
	certificateIssue,DATE_FORMAT(grndetail.certificateIssueDate,"%d/%m/%Y") AS certificateIssueDate,
	grndetail.grnId,grndetail.certificate_no,grndetail.grnDetailId,
	grndetail.temperature,grndetail.humidity,itemCode,
	CONCAT("N-",grnmaster.grnNo) AS grnNo,grnmaster.grn_no,
	DATE_FORMAT(grnmaster.grnDate,"%d/%m/%Y") AS grnDate,
	customer.custName,customer.address,customer.city,
	customer.custCode AS customerRef 
	FROM grndetail 
	LEFT JOIN grnmaster ON grnmaster.grnId = grndetail.grnId 
	LEFT JOIN customer ON customer.customerId = grnmaster.customerId 
	WHERE grndetail.grnDetailId = '.$_GET['grnDetailId'];
$res = mysql_query($qry) or die("Error :: Cannot select basic details.<hr>".mysql_error());
if($row = mysql_fetch_assoc($res)) { 
	$grnNo 					= $row['grnNo'];
	$grn_no 					= $row['grn_no'];
	$certificate_no 					= $row['certificate_no'];
	$grnDate				= $row['grnDate'];
	$callibrationDate		= $row['callibrationDate'];
	$callibrationDateNext 	= $row['nextYearDate'];
	$certificateIssue		= $row['certificateIssue'];
	$certificateIssueDate	= $row['certificateIssueDate'];
	$customerName			= $row['custName'];
	$customerAddress		= $row['address'];
	$customerCity		= $row['city'];
        $ref_no = '';
        $words = preg_split("/\s+/", trim($row['custName']));
        $acronym = "";
        foreach ($words as $w) {
            if(strlen($acronym) == 2){
                break;
            } else {
                $acronym .= $w[0];
            }
        }
        $cust_sn = Strtoupper($acronym);
	$customerRefNo			= $cust_sn.'/Cali/'.$row['customerRef'];
	$ambientTemperature		= $row['temperature'];
	$relativeHumidity		= $row['humidity'];
	
	$tempArr = $instrumentDetails;
	// instrument details query //
	$qry1 = 'SELECT grndetail.grnDetailId,grndetail.makeModel AS make,grndetail.itemCode AS item_code,
		grndetail.instrumentId AS id_no,grndetail.rangeValue AS range_value,
		grndetail.accuracy,grndetail.grnCondition AS grn_condition,item.itemName AS item_name,
		grndetail.certiRemarks AS certi_remarks, grndetail.cali_location, grndetail.uuc_location,grndetail.ulr_no, staff.staffName as userName, grndetail.sr_no, approved_by.name as approved_by, approved_by.designation
		FROM grndetail 
		LEFT JOIN item ON item.itemId = grndetail.itemId 
		LEFT JOIN approved_by ON approved_by.id = grndetail.approved_by 
                LEFT JOIN staff ON staff.staffId = grndetail.user_id 
		WHERE grndetail.callibrationDate IS NOT NULL AND grndetail.grnId = '.$row['grnId'].' AND grndetail.grnDetailId = "'.$row['grnDetailId'].'"';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
	while($row1 = mysql_fetch_assoc($res1)) { 
		array_push($grnDetailIds,$row1['grnDetailId']);
		foreach($tempArr as $key=>$val) {
			if(!in_array_r($row1[$key],$tempArr[$key])) {
				$tempArr[$key][$counter] = $row1[$key];
				$instrumentDetails[$key] .= $row1[$key].' | ';
			}
		}
		$counter++;
	}
	
	foreach($instrumentDetails as $key=>$val) {
		if($val != '')
			$instrumentDetails[$key] = substr($val,0,-3);
	}
	mysql_free_result($res1);
	
	$tempArr = $masterMeters;
	// master meters query //
	 $all_least = '';
        $all_uut = '';
        $qry2 = 'SELECT * FROM observation_data as grn
		WHERE grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].' ORDER BY grn.id ASC  ';
	$res2 = mysql_query($qry2) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $master_data = array();
//        echo "<pre>"; print_r(mysql_fetch_assoc($res1)); exit;
        while($row2 = mysql_fetch_assoc($res2)) {
                array_push($master_data,$row2);
        }
//        echo "<pre>"; print_r($master_data); exit;
        $master_meter_ids = array();
        $max_count_page = count($master_data) + 1;
        $current_page_no = '';
        $page_no = 1;
        foreach ($master_data as $m_key => $ms_data){ 
            $page_no = $page_no + 1;
            if($ms_data['table_info_id'] == '16'){
                $current_page_no = $page_no;
                break;
            }
        }
        foreach ($master_data as $m_key => $ms_data){ 
           
            $m_data = json_decode(json_encode(json_decode($ms_data['master_json'])), true);
            $u_data = json_decode(json_encode(json_decode($ms_data['uuc_json'])), true);
//             echo "<pre>"; print_r($m_data); exit;
            $all_least = $u_data[0]['leastCount'];
            $all_uut = $u_data[0]['uut_range'];
            foreach ($m_data as $s_data){ 
                $master_meter_ids[] = $s_data['masterMeterId'];
            }
            
        }
//        echo "<pre>"; print_r($master_meter_ids); exit;
	$qry2 = 'SELECT masterMeterName as "0",masterMeterIdNo as "1",masterMeterMake as "2",masterMeterModelNo as "3",masterMeterSerialNo as "4",masterMeterCertificateNo as "5",
		 DATE_FORMAT(masterMeterExp,"%d/%m/%Y") AS "6"
                 FROM mastermeter 
		 WHERE masterMeterId IN ('.implode(",",$master_meter_ids).')'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res2 = mysql_query($qry2) or die("Error :: Cannot select master meter details.<hr>".mysql_error());
	$master_m_data = array();
        while($row2 = mysql_fetch_assoc($res2)) { 
            $master_m_data[] = $row2;
	}
//        echo "<pre>"; print_r($master_m_data); exit;
	$qry21 = 'SELECT GROUP_CONCAT(DISTINCT procedureText) as procedureText, GROUP_CONCAT(DISTINCT page_title) as page_title
                 FROM mastermeter 
		 WHERE masterMeterId IN ('.implode(",",$master_meter_ids).')'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res21 = mysql_query($qry21) or die("Error :: Cannot select master meter details.<hr>".mysql_error());
	$master_procedureText = '';
	$page_title = '';
        while($row21 = mysql_fetch_assoc($res21)) { 
            $master_procedureText = $row21['procedureText'];
            $page_title = $row21['page_title'];
	}
//print '<pre>'; print_r($masterMeters);
	mysql_free_result($res2);
}
mysql_free_result($res);
    $qry112 = 'SELECT * FROM setting ';
    $res112 = mysql_query($qry112) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
    $s_trac = '';
    $s_approve = '';
    $s_note = '';
    $s_all_note = '';
    $s_page_1_note = '';
    while($row112 = mysql_fetch_assoc($res112)) { 
        if($row112['setting_name'] == 'traceability_to'){
            $s_trac = $row112['setting_value'];
        }
        if($row112['setting_name'] == 'approved_by'){
            $s_approve = $row112['setting_value'];
        }
        if($row112['setting_name'] == 'footer_note_for_page_1'){
            $s_note = $row112['setting_value'];
        }
        if($row112['setting_name'] == 'footer_note_for_all'){
            $s_all_note = $row112['setting_value'];
        }
        if($row112['setting_name'] == 'page_1_remark'){
            $s_page_1_note = $row112['setting_value'];
        }
    }
// fetch data - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// html - start //
ob_start();
?>

<style>
table {font-family: "Times New Roman"; font-size: 13px;}
tr {line-height: 21px;}
</style>

<div style="text-align:right; padding-right: 25px;"><b><?php echo $page_title; ?></b></div>


<div style="page-break-after: always;"></div>
<style>
    tr {line-height: 21px;}
.main_table tr td {border: 1px solid black;}
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 1px solid black;
    
}
.main_table td, .main_table th {
    padding: 1px;
    
}
.main_table{
    padding-bottom: 2px;
    
}
</style>
<!--<div style="text-align:right; padding-right: 25px;"><b><?php echo $page_title; ?></b></div>-->
<div style="border: 1px solid black; border-radius: 10px; padding: 5px;">
<table width="605">
	<tr>
		<td width="150">CERTIFICATE NO.</td>
		<td width="145">: <?php echo $certificate_no; ?></td>
		<td width="5">&nbsp;</td>
		<td width="200">DATE OF CALIBRATION</td>
		<td width="95" align="right">: <?php echo $callibrationDate; ?></td>
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grn_no; ?></td>
		<td>&nbsp;</td>
		<td>NEXT RECOMMENDED DATE</td>
		<td align="right">: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td width="150">PAGE</td>
		<td width="145">: <?php echo $page_no; ?> OF <span class="total_page_no"><?php echo $max_count_page; ?></span></td>
		<td width="5">&nbsp;</td>
		<td width="200">CERTIFICATE ISSUE DATE</td>
		<td width="95" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
	</tr>
</table>
<hr />
	<?php
		if(isset($_GET['grnDetailId']) && $_GET['grnDetailId'] != '') {
	?>
        <?php
        $qry1 = 'SELECT *,ti.note as table_info_note FROM observation_data as grn
                LEFT JOIN table_info as ti ON ti.table_info_id = grn.table_info_id
		WHERE grn.table_info_id = "16" AND grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].'  ORDER BY grn.table_no ASC ';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $grnobsmaster_data = array();
//        echo "<pre>"; print_r(mysql_fetch_assoc($res1)); exit;
        while($row1 = mysql_fetch_assoc($res1)) {
                array_push($grnobsmaster_data,$row1);
        }
//        echo "<pre>"; print_r($grnobsmaster_data); exit;
        ?>
        
        <table width="605">
            <tr valign="top">
                <td width="10"><strong>10.</strong></td>
		<td width="235"><strong> Observation</strong></td>
		<td width="5"></td>
		<td width="355"></td>
            </tr>
        </table>
        <?php foreach ($grnobsmaster_data as $main_key => $grn_data){
            $master_data = json_decode(json_encode(json_decode($grn_data['master_json'])), true);
            $uuc_data = json_decode(json_encode(json_decode($grn_data['uuc_json'])), true);
            $type_a_type_b_json = json_decode(json_encode(json_decode($grn_data['type_a_type_b_json'])), true);
            $reading_json = json_decode(json_encode(json_decode($grn_data['reading_json'])), true);
            $stdmeter_data = $reading_json[0]['stdMeter'];
            $testmeter1_data = $reading_json[0]['testMeter'];
            $testmeter2_data = isset($reading_json[0]['testMeter2']) ? $reading_json[0]['testMeter2'] : array();
            $testmeter3_data = isset($reading_json[0]['testMeter3']) ? $reading_json[0]['testMeter3'] : array();
            $testmeter4_data = isset($reading_json[0]['testMeter4']) ? $reading_json[0]['testMeter4'] : array();
            $testmeter5_data = isset($reading_json[0]['testMeter5']) ? $reading_json[0]['testMeter5'] : array();
            $testmeter6_data = isset($reading_json[0]['testMeter6']) ? $reading_json[0]['testMeter6'] : array();
            $uuc_min_range = $uuc_data[0]['uut_range_min'];
            $uuc_max_range = $uuc_data[0]['uut_range'];
            $uuc_range = 0;
            if($uuc_min_range < 0){
                $uuc_range = abs($uuc_min_range) + abs($uuc_max_range);
            } else {
                if($uuc_min_range > $uuc_max_range){
                    $uuc_range = abs($uuc_min_range);
                } else {
                    $uuc_range = abs($uuc_max_range);
                }
            }
            unset($grn_data['master_json']);
            unset($grn_data['uuc_json']);
            unset($grn_data['type_a_type_b_json']);
            unset($grn_data['reading_json']);
    //        $grn_data[] = $type_a_type_b_json;
            $main_arr = array();
            foreach ($master_data as $key => $master_dat){
                $t_arr = array_merge($master_dat,$uuc_data[$key],$type_a_type_b_json[$key],$grn_data);
                $main_arr[] = $t_arr;
            }
            $grn_data = $main_arr;
//            echo "<pre>"; print_r($grn_data); exit;
            $table_title = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(!empty($grn_entry['table_title'])){
                        $table_title = $grn_entry['table_title'];
                        break;
                    }
                }
            $table_title_right = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                        $table_title_right = $grn_entry['table_title_right'];
                        break;
                    }
                }
//                echo "<pre>"; print_r($reading_json); exit;
                $decimal_master = 2;
                $resolution_of_master = $grn_data[0]['resolution_of_master'];
                if(!empty($resolution_of_master)){
                    $exp_val = explode('.', $resolution_of_master);
                    if(isset($exp_val[1])){
                        $decimal_master = strlen($exp_val[1]);  
                    } else {
                      $decimal_master = 0;  
                    }
                } else {
                    $decimal_master = 2;
                }
                $decimal_least = 2;
                $resolution_of_least = $grn_data[0]['leastCount'];
                if(!empty($resolution_of_least)){
                    $exp_least = explode('.', $resolution_of_least);
                    if(isset($exp_least[1])){
                        $decimal_least = strlen($exp_least[1]);  
                    } else {
                      $decimal_least = 0;  
                    }
                } else {
                    $decimal_least = 2;
                }
                $qry13 = 'SELECT range_unit,coverage_factor,cmc FROM mastermetersubsub
		WHERE masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                $res13 = mysql_query($qry13) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                $m_range_name = '';
                $m_coverage_factor = '';
                $m_cmc = '';
                while($row13 = mysql_fetch_assoc($res13)) {
                    $m_range_name = $row13['range_unit'];
                    $m_coverage_factor = $row13['coverage_factor'];
                    $m_cmc = $row13['cmc'];
                }
                $max_key1 = max(array_keys($testmeter1_data));
                $max_key2 = !empty($testmeter2_data) ? max(array_keys($testmeter2_data)) : '0';
                $max_key3 = !empty($testmeter3_data) ? max(array_keys($testmeter3_data)) : '0';
                $max_key4 = !empty($testmeter4_data) ? max(array_keys($testmeter4_data)) : '0';
                $max_key5 = !empty($testmeter5_data) ? max(array_keys($testmeter5_data)) : '0';
                $max_key6 = !empty($testmeter6_data) ? max(array_keys($testmeter6_data)) : '0';
                $max_key7 = max(array_keys($stdmeter_data));
                $max_key = 0; 
                if(($max_key1 >= $max_key2) && ($max_key1 >= $max_key3) && ($max_key1 >= $max_key4) && ($max_key1 >= $max_key5) && ($max_key1 >= $max_key6) && ($max_key1 >= $max_key7)){
                    $max_key = $max_key1;
                } else if(($max_key2 >= $max_key3) && ($max_key2 >= $max_key1) && ($max_key2 >= $max_key4) && ($max_key2 >= $max_key5) && ($max_key2 >= $max_key6) && ($max_key2 >= $max_key7)){
                    $max_key = $max_key2;
                } else if(($max_key3 >= $max_key1) && ($max_key3 >= $max_key2) && ($max_key3 >= $max_key4) && ($max_key3 >= $max_key5) && ($max_key3 >= $max_key6) && ($max_key3 >= $max_key7)){
                    $max_key = $max_key3;
                } else if(($max_key4 >= $max_key1) && ($max_key4 >= $max_key2) && ($max_key4 >= $max_key3) && ($max_key4 >= $max_key5) && ($max_key4 >= $max_key6) && ($max_key4 >= $max_key7)){
                    $max_key = $max_key4;
                } else if(($max_key5 >= $max_key1) && ($max_key5 >= $max_key2) && ($max_key5 >= $max_key3) && ($max_key5 >= $max_key4) && ($max_key5 >= $max_key6) && ($max_key5 >= $max_key7)){
                    $max_key = $max_key5;
                } else if(($max_key6 >= $max_key1) && ($max_key6 >= $max_key2) && ($max_key6 >= $max_key3) && ($max_key6 >= $max_key4) && ($max_key6 >= $max_key5) && ($max_key6 >= $max_key7)){
                    $max_key = $max_key6;
                } else if(($max_key7 >= $max_key1) && ($max_key7 >= $max_key2) && ($max_key7 >= $max_key3) && ($max_key7 >= $max_key4) && ($max_key7 >= $max_key5) && ($max_key7 >= $max_key6)){
                    $max_key = $max_key7;
                }
//                echo "<pre>"; print_r($max_key);
//                echo "<pre>"; print_r($testmeter2_data);
//                echo "<pre>"; print_r($testmeter3_data); exit;
            ?>
        <table width="605" align="left" class="main_table"  style="font-size:13px;">
                <tr>
                    <th align="center" valign="middle">Title:</th>
                    <th  colspan="9" align="left" valign="middle">&nbsp; <?php echo $table_title_right; ?></th>
                </tr>
                <tr>
                    <th valign="middle" align="center" rowspan="3">Step</th>
                    <th colspan="1" align="center" valign="middle">Master Reading</th>
                    <th align="center" valign="middle" colspan="6">UUC Reading</th>
                    <th align="center" colspan="1" valign="middle">Average Miw</th>
                </tr>
                <tr>
                    <th valign="middle" align="center">Pind</th>
                    <th valign="middle" align="center">M1</th>
                    <th valign="middle" align="center">M2</th>
                    <th valign="middle" align="center">M3</th>
                    <th valign="middle" align="center">M4</th>
                    <th valign="middle" align="center">M5</th>
                    <th valign="middle" align="center">M6</th>
                    <th valign="middle" align="center">((M1+M3)/2+</th>
                </tr>
                <tr>
                    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
                    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
                    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
                    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
                    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
                    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
                    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
                    <th valign="middle" align="center"> (M2+M4)2)/2</th>
                </tr>
                <?php
                $m1_total = 0;
                $m2_total = 0;
                $max_m2_m1_h = 0;
                $max_m1_m3_h = 0;
//                =((IMABS((M2-M1))))
//                Repetability = M1 - M3
                $hysteresis_val = '';
                $repetability_val = '';
                $one_avg = '';
                $max_avg = '';
                for($x = 0; $x <= $max_key; $x++) { ?>
                    <tr>
                        <td align="center" valign="middle"><?php echo $x + 1; ?></td>
                        <?php $std_mter = (isset($stdmeter_data[$x]) ? number_format((float)$stdmeter_data[$x], $decimal_master, '.', '') : '0'); ?>
                        <td align="center" valign="middle"><?= (isset($stdmeter_data[$x]) ? number_format((float)$stdmeter_data[$x], $decimal_master, '.', '') : ''); ?></td>
                        <?php $m1 = (isset($testmeter1_data[$x]) ? number_format((float)$testmeter1_data[$x], $decimal_least, '.', '') : '0'); ?>
                        <td align="center" valign="middle"><?= (isset($testmeter1_data[$x]) ? number_format((float)$testmeter1_data[$x], $decimal_least, '.', '') : '0'); ?></td>
                        <?php $m2 = (isset($testmeter2_data[$x]) ? number_format((float)$testmeter2_data[$x], $decimal_least, '.', '') : '0'); ?>
                        <td align="center" valign="middle"><?= (isset($testmeter2_data[$x]) ? number_format((float)$testmeter2_data[$x], $decimal_least, '.', '') : ''); ?></td>
                        <?php $m3 = (isset($testmeter3_data[$x]) ? number_format((float)$testmeter3_data[$x], $decimal_least, '.', '') : '0'); ?>
                        <td align="center" valign="middle"><?= (isset($testmeter3_data[$x]) ? number_format((float)$testmeter3_data[$x], $decimal_least, '.', '') : ''); ?></td>
                        <?php $m4 = (isset($testmeter4_data[$x]) ? number_format((float)$testmeter4_data[$x], $decimal_least, '.', '') : '0'); ?>
                        <td align="center" valign="middle"><?= (isset($testmeter4_data[$x]) ? number_format((float)$testmeter4_data[$x], $decimal_least, '.', '') : ''); ?></td>
                        <?php $m5 = (isset($testmeter5_data[$x]) ? number_format((float)$testmeter5_data[$x], $decimal_least, '.', '') : '0'); ?>
                        <td align="center" valign="middle"><?= (isset($testmeter5_data[$x]) ? number_format((float)$testmeter5_data[$x], $decimal_least, '.', '') : ''); ?></td>
                        <?php $m6 = (isset($testmeter6_data[$x]) ? number_format((float)$testmeter6_data[$x], $decimal_least, '.', '') : '0'); ?>
                        <td align="center" valign="middle"><?= (isset($testmeter6_data[$x]) ? number_format((float)$testmeter6_data[$x], $decimal_least, '.', '') : ''); ?></td>
                        <?php $avg = ((($m1+$m3+$m5)/3+($m2+$m4+$m6)/3)/2); ?>
                        <td align="center" valign="middle"><?= number_format((float)$avg, $decimal_master, '.', ''); ?></td>
                        <?php $dev = (($std_mter - $avg)/$uuc_range)*100; ?>
                    </tr>
                    <?php
                    if($x == 1){
                        $one_avg = $avg;
                        $max_avg = $avg;
                    }
                    $m1_total = $m1_total + $m1;
                    $m2_total = $m2_total + $m2;
                    $new_max_m2_m1_h = $m2 - $m1;
                    if($new_max_m2_m1_h > $max_m2_m1_h){
                        $max_m2_m1_h = $new_max_m2_m1_h;
                    }
                    $new_max_m1_m3_h = $m1 - $m3;
                    if($new_max_m1_m3_h > $max_m1_m3_h){
                        $max_m1_m3_h = $new_max_m1_m3_h;
                    }
                    if($avg > $max_avg){
                        $max_avg = $avg;
                    }
                    $max_m2_m1 = abs($m2 - $m1);
                    if($hysteresis_val == ''){
                        $hysteresis_val = $dev;
                    } else {
                        if($dev > $hysteresis_val){
                            $hysteresis_val = $dev;
                        }
                    }
                    $max_m1_m3 = $m1 - $m3;
                    if($repetability_val == ''){
                        $repetability_val = $max_m1_m3;
                    } else {
                        if($max_m1_m3 > $repetability_val){
                            $repetability_val = $max_m1_m3;
                        }
                    }
                } 
                // calculation for Uncertainty (max) : Start //
                $U5 = $grn_data[0]['uncertinty_from_tracebility'] / 2;
                $U7 = $grn_data[0]['accuracy_taking_account'] / sqrt(3);
                $U9 = ($new_max_m2_m1_h / 2) / sqrt(3);
                $U11 = ($max_m1_m3_h / 2) / sqrt(3);
                $U13 = ($one_avg / 2) / sqrt(3);
                $U15 = ($decimal_least / 2) / sqrt(3);
                $U17 = sqrt(pow($U5,2)+pow($U7,2)+pow($U9,2)+pow($U11,2)+pow($U13,2)+pow($U15,2)); // =SQRT(U5^2+U7^2+U9^2+U11^2+U13^2+U15^2)
                $U18 = $U17 * $m_coverage_factor; // =U17*M22
                $U19 = $U18 * 100 / $max_avg; // =U18*100/MAX(G29:G38)
//                if(strtolower($m_range_name) == 'bar'){
                    if($U18 > $m_cmc){
                        $uncertainty_max = $U18;
                    } else {
                        $uncertainty_max = $m_cmc;
                    }
//                } else {
//                    if($U19 > $m_cmc){
//                        $uncertainty_max = $U19;
//                    } else {
//                        $uncertainty_max = $m_cmc;
//                    }
//                }
                
                // calculation for Uncertainty (max) : End //
                ?>
                <tr>
                    <td colspan="3" align="center" valign="middle"><b>Hysteresis (max)</b></td>
                    <td colspan="1" align="center" valign="middle"><b><?php echo number_format((float)$hysteresis_val, $decimal_master, '.', ''); ?> <?php echo $m_range_name; ?></b></td>
                    <td colspan="2" align="center" valign="middle">&nbsp;</td>
                    <td colspan="1" align="center" valign="middle">&nbsp;</td>
                    <td colspan="1" align="center" valign="middle">&nbsp;</td>
                    <td colspan="1" align="center" valign="middle">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center" valign="middle"><b>Repetability</b></td>
                    <td colspan="1" align="center" valign="middle"><b><?php echo number_format((float)$repetability_val, $decimal_master, '.', ''); ?></b></td>
                    <td colspan="3" align="center" valign="middle"><b>Exp. Uncertainty(max)</b></td>
                    <td colspan="2" align="center" valign="middle"><b><?php echo number_format((float)$uncertainty_max, $decimal_master, '.', ''); ?> <?php echo $m_range_name; ?></b></td>
                </tr>
                <tr>
                    <td colspan="2" align="center" valign="middle">Remark</td>
                    <td colspan="7" align="left" valign="middle"><?php echo $instrumentDetails['certi_remarks']; ?></td>
                </tr>
            </table>
            <table width="605" align="left" border="0" class=""  style="font-size:11px;">
                <tr>
                    <td width="605" align="left" valign="middle"><b>Note : </b><?php echo nl2br($grn_data[0]['table_info_note']); ?></td>
                </tr>
            </table>
	<?php } ?>
	<?php
		}
	?>
<table width="605">
    <tr valign="top">
        <td width="5">&nbsp;</td>
        <td width="600">&nbsp;</td>
    </tr>
        <tr valign="top">
            <hr />
            <td width="5" style="height: 5px;"><strong></strong></td>
            <td  style="height: 5px;" width="600" style="font-size: 20px;"></td>
	</tr>
        <tr valign="bottom">
            <td width="5"><strong>&nbsp;</strong></td>
            <td width="600" style="font-size: 20px;"><img src="./images/for-krishna.png" /></td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">CALIBRATED BY: (<?php echo $instrumentDetails['userName']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; APPROVED BY: (<?php echo $instrumentDetails['approved_by'].', '.$instrumentDetails['designation']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; SEAL :</td>
	</tr>
</table>
</div>


<!--<page_footer>
    <table class="page_footer" style="width: 100%;">
        <tr>
            <td style="width: 100%;">
                <?php echo $s_all_note; ?>
            </td>
        </tr>
    </table>
</page_footer>-->
<?php

// html - end //
$content = ob_get_clean();

// convert in PDF
require_once 'html2pdf/html2pdf.class.php';
try {
//	$html2pdf = new HTML2PDF('P', 'A4', 'fr');
	$html2pdf = new HTML2PDF('P', 'A4', 'fr',true,'UTF-8',array(30, 43, 10, 14));
	// $html2pdf->setModeDebug();
	$html2pdf->setDefaultFont('Times');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output('example.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>