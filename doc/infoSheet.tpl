{include file="./headStart.tpl"}
{include file="./headEnd.tpl"}
<form action="infoSheet.php" name='infoSheet' method='post'>
<div>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
<tr>
  <td class="table1" align="center" colspan="25">! Customer Order Info Sheet !</td>
</tr>
</table>
  <table border="0" align="center" cellspacing="0" cellpadding="0" >
  <tr>
    <td align="center" colspan="6">GRN No :
      <select name="grnNo" onchange="document.infoSheet.action='infoSheet.php';document.infoSheet.submit();">
        <option value="0">Select GRN no</option>
        {html_options values=$grnNo output=$grnNo selected=$grnId}
      </select>
  </td>
  </tr>
   <tr>
    <td class="table1" align="right"> Date : </td>
    <td class="table2" align="left">{$grnDate}</td>
    <td class="table1" align="right"> Customer Name : </td>
    <td class="table2" align="left">{$custName}</td>
    <td class="table1" align="right"> Address : </td>
    <td class="table2" align="left">{$address}</td>
    </tr>
    <tr>
    <td class="table1" align="right"> Contact Person : </td>
    <td class="table2" align="left">{$contPerson}</td>
    <td class="table1" align="right"> Phone No : </td>
    <td class="table2" align="left">{$phone1}</td>
    <td class="table1" align="right"> E-Mail : </td>
    <td class="table2" align="left">{$email}</td>
    </tr>
    </table>
  <br /><br />
  </div>
  <table align="center" border="1" cellpadding="1" cellspacing="1">
      <tr><th colspan="13" align="center"><h2>! Following Instruments Received For - Calibration!</h2></th></tr>
  <tr>
    <td class="table1" align="center" rowspan="2">SR. NO. </td>
    <td class="table1" align="center" rowspan="2">Name Of Instrument </td>
    <td class="table1" align="center" rowspan="2">Instru. ID. NO. </td>
    <td class="table1" colspan="5" align="center">Parameters Of Calibrate </td>
    <td class="table1" align="center" rowspan="2">Cali. Due Date Req. By Cust.</td>
    <td class="table1" align="center" rowspan="2">Exp. Deliv. Date</td>
    <td class="table1" align="center" rowspan="2"> Condt. Of Instru.</td>
  </tr>
  <tr>
    <td class="table1" align="center"> i. </td>
    <td class="table1" align="center"> ii. </td>
    <td class="table1" align="center"> iii. </td>
    <td class="table1" align="center"> iv. </td>
    <td class="table1" align="center"> v. </td>
  </tr>
  {section name="sec" loop=$i}
  <tr>
    <td class="table2" align="right">
      <input type="hidden" name="grnDetailId" value="{$grnDetailId[sec]}">
     {$smarty.section.sec.rownum}
    </td>
	  <td class="table2" align="left">{$itemName[sec]}</td>
    <td class="table2" align="right">{$itemCode[sec]}</td>
    <td class="table2" align="left"><input type="text" size="5"></td>
	  <td class="table2" align="left"><input type="text" size="5"></td>
	  <td class="table2" align="left"><input type="text" size="5"></td>
	  <td class="table2" align="left"><input type="text" size="5"></td>
	  <td class="table2" align="left"><input type="text" size="5"></td>
    <td class="table2" nowrap>
      {html_select_date field_array="custReqDate[]" prefix="custReqDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
    </td>
    <td class="table2" nowrap>
      {html_select_date field_array="expDelivDate[]" prefix="expDelivDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
    </td>
    <td class="table2" align="left">{$grnCondition[sec]}</td>
  </tr>
  {/section}
  
</table>

    <br>

    <table align="center">
    <tr align="center">
    <td>
      <input type="submit"  value="Submit" align="right" name="submitBtn">
    </td>
    </tr>
    </table>
</form>
<script language="javascript">document.infoSheet.text1.focus();</script>
{include file="footer.tpl"}