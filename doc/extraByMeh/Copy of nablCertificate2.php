<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = "";
  $grnDetailId         = "";
  $grnPrefix           = "";
  $grnDate             = "";
  $poNo                = "";
  $poDate              = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $certificateNo       = 0;
  $yearWord            = "";
  $monthWord           = "";
  //$callibrationDate    = date("Y-m-d");
  $grnDetailArr        = array();
  //SELECT OF GRN MASTER Detail customer name to humidity :START
  $selectGrnMaster = "SELECT grnmaster.grnId,grnmaster.grnNo,grnmaster.infoSheetNo,grnmaster.customerId, DATE_FORMAT(grnmaster.grnDate,'%d/%m/%Y') AS grnDate,customer.customerId,customer.custName,
                             customer.address,customer.city,customer.custRefName,grnDetail.grnId,grnDetail.masterMeterId,DATE_FORMAT(grndetail.callibrationDate,'%d/%m/%Y') AS callibrationDate,
                             DATE_FORMAT(grndetail.nextYearDate,'%d/%m/%Y') AS nextYearDate,grnDetail.grnDetailId,mastermeter.masterMeterId,
                             mastermeter.masterMeterName,mastermeter.masterMeterIdNo,mastermeter.masterMeterMake,mastermeter.masterMeterModelNo,mastermeter.masterMeterCertificateNo,mastermeter.masterMeterExp,
                             mastermeter.masterMeterTraceabilityTo,mastermeter.procedureText,mastermeter.masterMeterSerialNo,mastermetersub.masterMeterSubId,
                             mastermetersub.masterMeterId,mastermetersubsub.masterMeterSubId,mastermetersubsub.range AS range1,mastermetersubsub.masterMeterSubSubId,
                             mastermetersubsub.certiAccuracy,grnDetail.accuracy,grnDetail.itemId,grnDetail.range AS range2,grndetail.makeModel,grndetail.instrumentId,grnDetail.leastCount,
                             grndetail.accuracy,grndetail.temperature,grndetail.humidity,custReqDate,item.itemId,item.itemName,customer.custCode, grnobservation.measurementUncertaintyIs
                        FROM grnmaster
                   LEFT JOIN customer ON grnmaster.customerId = customer.customerId
                   LEFT JOIN grndetail ON grnmaster.grnId = grndetail.grnId
                   LEFT JOIN grnobservation ON grnmaster.grnId = grnobservation.grnId
                   LEFT JOIN mastermeter ON grndetail.masterMeterId = mastermeter.masterMeterId
                   LEFT JOIN mastermetersub ON grndetail.masterParameterId = mastermetersub.masterMeterSubId
                   LEFT JOIN mastermetersubsub ON grndetail.masterRangeId = mastermetersubsub.masterMeterSubSubId
                   LEFT JOIN item ON grndetail.itemId = item.itemId
                       WHERE grnmaster.grnId = ".$_GET['grnId']."
                         AND grndetail.grnDetailId = ".$_GET['grnDetailId'];
  $selectGrnMasterRes    = mysql_query($selectGrnMaster);
  if($selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes))
  {
  	$grnDetailArr['masterMeterCertificateNo']  = $selectGrnMasterRow['masterMeterCertificateNo'];
  	$grnDetailArr['callibrationDate']          = $selectGrnMasterRow['callibrationDate'];
  	$grnDetailArr['nextYearDate']              = $selectGrnMasterRow['nextYearDate'];
  	$grnDetailArr['grnNo']                     = 'N'.($selectGrnMasterRow['grnNo']);
  	$grnDetailArr['custCode']                  = $selectGrnMasterRow['custCode'];
  	$grnDetailArr['custName']                  = $selectGrnMasterRow['custName'];
  	$grnDetailArr['address']                   = $selectGrnMasterRow['address'];
  	$grnDetailArr['city']                      = $selectGrnMasterRow['city'];
  	$grnDetailArr['custRefName']               = $selectGrnMasterRow['custRefName'];
  	$grnDetailArr['grnDate']                   = $selectGrnMasterRow['grnDate'];
  	$grnDetailArr['itemName']                  = $selectGrnMasterRow['itemName'];
  	$grnDetailArr['makeModel']                 = $selectGrnMasterRow['makeModel'];
  	$grnDetailArr['masterMeterSerialNo']       = $selectGrnMasterRow['masterMeterSerialNo'];
  	$grnDetailArr['instrumentId']              = $selectGrnMasterRow['instrumentId'];
  	$grnDetailArr['range1']                    = $selectGrnMasterRow['range1'];
  	$grnDetailArr['range2']                    = $selectGrnMasterRow['range2'];
  	$grnDetailArr['accuracy']                  = $selectGrnMasterRow['accuracy'];
  	$grnDetailArr['temperature']               = $selectGrnMasterRow['temperature'];
  	$grnDetailArr['humidity']                  = $selectGrnMasterRow['humidity'];
  	$grnDetailArr['masterMeterName']           = $selectGrnMasterRow['masterMeterName'];
  	$grnDetailArr['masterMeterIdNo']           = $selectGrnMasterRow['masterMeterIdNo'];
  	$grnDetailArr['masterMeterMake']           = $selectGrnMasterRow['masterMeterMake'];
  	$grnDetailArr['masterMeterModelNo']        = $selectGrnMasterRow['masterMeterModelNo'];
  	$grnDetailArr['masterMeterSerialNo']       = $selectGrnMasterRow['masterMeterSerialNo'];
  	$grnDetailArr['masterMeterCertificateNo']  = $selectGrnMasterRow['masterMeterCertificateNo'];
  	$grnDetailArr['masterMeterExp']            = $selectGrnMasterRow['masterMeterExp'];
  	$grnDetailArr['masterMeterTraceabilityTo'] = $selectGrnMasterRow['masterMeterTraceabilityTo'];
  	$grnDetailArr['procedureText']             = $selectGrnMasterRow['procedureText'];
  	$grnDetailArr['measurementUncertaintyIs']  = $selectGrnMasterRow['measurementUncertaintyIs'];
    if(isset($selectGrnMasterRow['custCode']))
    {
      $custCodeLength = strlen(($selectGrnMasterRow['custCode']));
      if($custCodeLength == "1")
        $custCodeLengthId = '00'.$selectGrnMasterRow['custCode'];
      else if($custCodeLength == "2")
        $custCodeLengthId = '0'.$selectGrnMasterRow['custCode'];
      else if($custCodeLength == "3")
        $custCodeLengthId = $selectGrnMasterRow['custCode'];
      else if($custCodeLength == "4")
        $custCodeLengthId = '000'; 
    }
    if(isset($selectGrnMasterRow['grnDetailId']))
    {
      $grnDetailLength = strlen(($selectGrnMasterRow['grnDetailId']));
      if($grnDetailLength == "1")
        $grnDetailLengthId = '00'.$selectGrnMasterRow['grnDetailId'];
      else if($grnDetailLength == "2")
        $grnDetailLengthId = '0'.$selectGrnMasterRow['grnDetailId'];
      else if($grnDetailLength == "3")
        $grnDetailLengthId = $selectGrnMasterRow['grnDetailId'];
      else if($grnDetailLength == "0")
        $grnDetailLengthId = '000'; 
    }
    if(isset($selectGrnMasterRow['callibrationDate']))
    {
      if(substr($selectGrnMasterRow['callibrationDate'],6,4));
      {
        $year = substr($selectGrnMasterRow['callibrationDate'],6,4);
        if($year == 2010)
          $yearWord = "A";
        else if ($year == 2011)
          $yearWord = "B";
        else if($year == 2012)
          $yearWord = "C";
        else if($year == 2013)
          $yearWord = "D";
        else if($year == 2014)
          $yearWord = "E";
      } 
      
      if(substr($selectGrnMasterRow['callibrationDate'],3,2))
      {
        $month = substr($selectGrnMasterRow['callibrationDate'],3,2);
        if($month == "01")
          $monthWord = "A";
        else if($month == "02")
          $monthWord = "B";
        else if($month == "03")
          $monthWord = "C";
        else if($month == "04")
          $monthWord = "D";
        else if($month == "05")
          $monthWord = "E";
         else if($month == "06")
          $monthWord = "F";
         else if($month == "07")
          $monthWord = "G";
         else if($month == "08")
          $monthWord = "H";
         else if($month == "09")
          $monthWord = "I";
         else if($month == "10")
          $monthWord = "J";
         else if($month == "11")
          $monthWord = "K";
         else if($month == "12")
          $monthWord = "L";
      } 
    }
    $certificateNo = 'KN'.$custCodeLengthId.''.$yearWord.''.$monthWord.''.$grnDetailLengthId;
  }  
  //SELECT OF GRN MASTER Detail customer name to humidity :End
  //For Master Meter Selection in nabl cerficate :Start
  $selectDetailMasterQuery = "SELECT masterMeterId,
                                     masterMeterName,masterMeterIdNo,masterMeterMake,masterMeterModelNo,masterMeterSerialNo,masterMeterCertificateNo,
                                     DATE_FORMAT(masterMeterExp,'%d/%m/%Y') as masterMeterExp,masterMeterTraceabilityTo,procedureText
                                FROM mastermeter
                               WHERE masterMeterId IN (SELECT masterMeterId FROM grndetail
                                                                    WHERE grnId = ".$_GET['grnId'].")
                               ORDER BY mastermeter.masterMeterId";
  $selectDetailMasterQueryRes = mysql_query($selectDetailMasterQuery);
  $masterMeterCount = 0;
  while($masterMeterDetailRow = mysql_fetch_array($selectDetailMasterQueryRes))
  {
    if($masterMeterCount % 2 == 0)
    {
      $leftMasterMeterId = $masterMeterDetailRow['masterMeterId'];
      //$masterMeterName = $masterMeterDetailRow['masterMeterName'];
    }

    $masterMeterCount++;
  }
  //For Master Meter Selection in nabl cerficate :End
  $selectGrnObservationId = "SELECT grnDetailId,mastermetersub.parameter
                               FROM grndetail
                               LEFT JOIN mastermetersub ON grndetail.masterParameterId = mastermetersub.masterMeterSubId
                              WHERE grnId = ".$_GET['grnId']."
                                AND grnDetailId = ".$_GET['grnDetailId'];
  $selectGrnObservationIdRes = mysql_Query($selectGrnObservationId);
  $b               = 0;
  $errorUnits      = 0;
  $a               = 0;
  $total           = 0;
  $detailArr       = array();
  $masterArr       = array();
  while($selectGrnDetail = mysql_fetch_array($selectGrnObservationIdRes))
  {
  	 $masterArr[$a]['grnDetailId'] = $selectGrnDetail['grnDetailId'];
  	 $masterArr[$a]['parameterName'] = $selectGrnDetail['parameter'];
     $selectGrnDetailId = "SELECT grnObservationId,testMeterAverage,stdMeterAverage,masterMeterReading,percentageRdg,expandedUncertinity,meanReading
                            FROM grnobservation
                           WHERE grnDetailId = ".$selectGrnDetail['grnDetailId'];
    $selectGrnDetailIdRes = mysql_Query($selectGrnDetailId);
    $b = 0;
    if(mysql_num_rows($selectGrnDetailIdRes) > 0)
    {
      while($selectGrnDetailRow = mysql_fetch_array($selectGrnDetailIdRes))
      {
      	$masterArr[$a]['dataPresent'] = 1;
      	$detailArr[$a][$b]['grnObservationId']   = $selectGrnDetailRow['grnObservationId'];
      	$detailArr[$a][$b]['masterMeterReading'] = $selectGrnDetailRow['masterMeterReading'];
      	$detailArr[$a][$b]['stdMeterAverage']    = $selectGrnDetailRow['stdMeterAverage'];
      	$detailArr[$a][$b]['testMeterAverage']   = $selectGrnDetailRow['testMeterAverage'];
      	$detailArr[$a][$b]['errorUnits']         = sprintf("%.2f",$selectGrnDetailRow['stdMeterAverage'] - $selectGrnDetailRow['testMeterAverage']);
        $total       = $selectGrnDetailRow['percentageRdg'];
        $errorUnits  = $selectGrnDetailRow['stdMeterAverage'] - $selectGrnDetailRow['testMeterAverage'];
        $detailArr[$a][$b]['errorRdg']    = sprintf("%.2f",($errorUnits/$selectGrnDetailRow['stdMeterAverage'])*100);
      	$detailArr[$a][$b]['expandedUncertinity'] = sprintf("%.3f",($selectGrnDetailRow['expandedUncertinity']*100)/$selectGrnDetailRow['meanReading']);
        $b++;
      }
    }
    else
    {
    	$detailArr[$a] = array();
    	$masterArr[$a]['dataPresent'] = 0;
    }
    $a++;
  }
  include("./bottom.php");
  $smarty->assign("certificateNo",$certificateNo);
  $smarty->assign("grnDetailArr",$grnDetailArr);
  $smarty->assign("total",$total);
  $smarty->assign("masterArr",$masterArr);
  $smarty->assign("detailArr",$detailArr);
  $smarty->display("nablCertificate.tpl");
}
?>