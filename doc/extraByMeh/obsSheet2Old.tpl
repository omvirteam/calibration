{include file="./headStart.tpl"}
<script type="text/javascript">
$(document).ready(function(){ 
  $(".add").live('click',function(){
    $("#one").clone(true).appendTo("#mainDiv").find('input[type="text"]').val('');
  });
  $(".delete").click(function(){
    $(this).parent().parent().remove();
  });
});
function getGrnData()
{
	var datastring = "grnId=" + $("#grnId").val();
	$.ajax({
	  url: "./obsSheetAj.php",
	  data: datastring,
	  success: function(data){ 
	    $('#showItem').html(data);
	    $('#custCode').val($('#custCodeHidden').val());
	    //
	    $("#grnDetailId").change(function(){
	      var datastring = "grnDetailId=" + $("#grnDetailId").val();
	      $.ajax({
	        url: "./obsSheetAjGrnDetailChange.php",
	        data: datastring,
	        success: function(data){
	          $('#showDetail').html(data);
	        }
	      });
	    });
	  }
	});
}
function getMasterMeterData()
{
	var dataString = "masterMeterId=" + $("#masterMeterId").val();
	$.ajax({
	  type: "GET",
	  url:  "masterMeterJq.php",
	  data: dataString,
	  success: function(data){ 
	    $("#masterpara").html(data);
	    // Ajax Call At Range :Start 
	    $("#masterMeterSubId").change(function(){ 
	      var dataString = "masterMeterSubId=" + $("#masterMeterSubId").val();
	      $.ajax({
	        type: "GET",
	        url:  "masterMeterJqSub.php",
	        data: dataString,
	        success: function(data){
	           $("#masterparasub").html(data);
	           
	          // Get accuracysub With Ajax  :Start 
	          $("#masterMeterSubSubId").change(function(){ 
	         	  var dataString = "masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
	       	    $.ajax({
	       	    type: "GET",
	       	    url:  "masterMeterJqSubAccuracy.php",
	       	    data: dataString,
	       	    success: function(data1){ 
	       	    	$("#rangedisplay").html(data1);
	              $('.accuracyTaken').each(function(){
	             	//On Changes Of Range Calculate Accuracy % taking in to account
//	                calculateAccuracy($(this));
//	                resolutionForTypeB($(this));
//	                stabilityForTypeB($(this));
//	                stabilityForTypeInPerc($(this));
//	                combinedUncertinity($(this));
//	                combinedUncertinityInPerc($(this));
//	                effectiveUncertinity($(this));
//	                effectiveUncertinityInPer($(this));
//	                effectiveDegreeOfFreed($(this));
//	                meanReadingCalculate($(this));
//	                masterMeterReading($(this));
//	                error($(this));
	                });
	       	      }
	       	    });
	          });
	        }	
	      });
	    });
	    // Get accuracysub With Ajax: End
	  }
	});
}
function meanReading(theStdObject)
{
  var row = $(theStdObject).parents('.tableRow');
  var stdMeterAverage2 = parseFloat($(theStdObject).val());
	valuesAvailable = 1;
	stdMeter1Val = row.find(".stdMeter1").val();
	stdMeter2Val = row.find(".stdMeter2").val();
	stdMeter3Val = row.find(".stdMeter3").val();
	stdMeter4Val = row.find(".stdMeter4").val();
	stdMeter5Val = row.find(".stdMeter5").val();
	stdMeterAverageObj = row.find(".stdMeterAverage").val();
  
	if(stdMeter2Val != "")  valuesAvailable++;  else  stdMeter2Val = 0;
	if(stdMeter3Val != "")  valuesAvailable++;  else  stdMeter3Val = 0;
	if(stdMeter4Val != "")  valuesAvailable++;  else  stdMeter4Val = 0;
	if(stdMeter5Val != "")  valuesAvailable++;  else  stdMeter5Val = 0;

  stdMeterAverageObj = (parseFloat(stdMeter1Val) + parseFloat(stdMeter2Val) + parseFloat(stdMeter3Val) + parseFloat(stdMeter4Val) 
                       + parseFloat(stdMeter5Val)) / valuesAvailable;
  row.find(".stdMeterAverage").val(stdMeterAverageObj);
  // 
  row.find(".accuracyTaken").val((((((stdMeterAverageObj*0.03)+(($("#masterMeterAccuracyRangeArray").val())*0.02))/100)*100)/stdMeterAverageObj).toFixed(4));
  //
  stdMeterAverageDev = stdMeterAverageObj;
  
  stdMeter1ValDeviation = (stdMeter1Val - stdMeterAverageDev) * (stdMeter1Val - stdMeterAverageDev);
  stdMeter2ValDeviation = (stdMeter2Val - stdMeterAverageDev) * (stdMeter2Val - stdMeterAverageDev);
  stdMeter3ValDeviation = (stdMeter3Val - stdMeterAverageDev) * (stdMeter3Val - stdMeterAverageDev);
  stdMeter4ValDeviation = (stdMeter4Val - stdMeterAverageDev) * (stdMeter4Val - stdMeterAverageDev);
  stdMeter5ValDeviation = (stdMeter5Val - stdMeterAverageDev) * (stdMeter5Val - stdMeterAverageDev);
  
  row.find(".standardDeviation").val((Math.sqrt((stdMeter1ValDeviation + stdMeter2ValDeviation 
                                    + stdMeter3ValDeviation + stdMeter4ValDeviation 
                                    + stdMeter5ValDeviation) / 5)).toFixed(4));

  standardUncertinity = Math.sqrt((stdMeter1ValDeviation + stdMeter2ValDeviation 
                                    + stdMeter3ValDeviation + stdMeter4ValDeviation 
                                    + stdMeter5ValDeviation) / 5)
  row.find(".standardUncertinity").val((standardUncertinity/Math.sqrt(5)).toFixed(4));
  row.find(".standardUncertinityperc").val((row.find(".standardUncertinity").val()*100/row.find(".stdMeterAverage").val()).toFixed(4));
  row.find(".degreeOfFreedom").val(0);
  row.find(".uncertinityForTypeB").val((($("#masterMeterUncertintyArray").val()/2)*row.find(".stdMeterAverage").val()*0.01).toFixed(4));
  row.find(".uncertinityInPercentage").val(0);
  row.find(".accuracyForTypeB").val((row.find(".accuracyTaken").val() * row.find(".stdMeterAverage").val() * 0.01 / Math.sqrt(3)).toFixed(4));
  row.find(".acuuracyForTypeBPerc").val(0);
  row.find(".resolutionTypeB").val((($("#masterMeterResolutionArray").val()/2)/Math.sqrt(3)).toFixed(4));
  row.find(".resolutionForTypeBPerc").val(0);
  row.find(".stabilityForTypeB").val(0);
  row.find(".stabilityForTypeBInPerc").val(0);
  row.find(".combinedUncertinityInPerc").val(0);
  row.find(".effectiveUncertinityInPer").val(0);
  row.find(".effectiveDegreeOfFreed").val(0);
  row.find(".masterMeterReading").val(0);
  row.find(".effectiveUncertinity").val(0);
  
  var  standardUncertinityComined = row.find(".standardUncertinity").val();
  var  accuracyForTypeBCombined = row.find(".accuracyForTypeB").val();
  var  uncertinityForTypeBcombined = row.find(".uncertinityForTypeB").val();
  var  resolutionTypeBCombined = row.find(".resolutionTypeB").val();
  
  row.find(".combinedUncertinity").val((Math.sqrt((standardUncertinityComined * standardUncertinityComined) + (accuracyForTypeBCombined * accuracyForTypeBCombined) +
                                           (uncertinityForTypeBcombined * uncertinityForTypeBcombined) + (resolutionTypeBCombined * resolutionTypeBCombined))).toFixed(5));
  row.find(".expandedUncertinity").val((2*row.find(".combinedUncertinity").val()).toFixed(4));
  row.find(".meanReading").val(row.find(".stdMeterAverage").val());
}
function testMeterAvg(theObj)
{
	var row = $(theObj).parents('.tableRow');
  
	valuesAvailable = 1;
	
	testMeter1Val = row.find(".testMeter1").val();
	testMeter2Val = row.find(".testMeter2").val();
	testMeter3Val = row.find(".testMeter3").val();
	testMeter4Val = row.find(".testMeter4").val();
	testMeter5Val = row.find(".testMeter5").val();
	
	if(testMeter2Val != "")  valuesAvailable++;  else  testMeter2Val = 0;
	if(testMeter3Val != "")  valuesAvailable++;  else  testMeter3Val = 0;
	if(testMeter4Val != "")  valuesAvailable++;  else  testMeter4Val = 0;
	if(testMeter5Val != "")  valuesAvailable++;  else  testMeter5Val = 0;

  testAverageObj = (parseFloat(testMeter1Val) + parseFloat(testMeter2Val) + parseFloat(testMeter3Val) + parseFloat(testMeter4Val) 
                       + parseFloat(testMeter5Val)) / valuesAvailable;
  row.find("#testMeterAverage").val(testAverageObj);
  //
	
}
</script>
{include file="./headEnd.tpl"}
<form action="{$smarty.server.PHP_SELF}" id="form1" name="form1" method='POST'>
<input type="hidden" name="grnDetailPassId" value={$grnDetailPassId} />
<center class="center"><h2>Observation Sheet</h2></center><br>
<table border='1' cellpadding='5' cellspacing='0' align='center'>
  <tr>
    <td>GRN NO.:-</font>
      <select name="grnId" id="grnId" onchange="getGrnData();">
        <option value="0">GRN</option>
        {html_options values=$grnId output=$grnPrefixNo}
      </select>
    <td>
      <font size="4">Cust Id No.:-</font>
      <input type="text" name="custCode" id="custCode" value="" size="2" />
    
    <td colspan="9"  NOWRAP><font size="4">Master Meter.:-</font>
     <select name="masterMeterId" id="masterMeterId" onchange="getMasterMeterData();">
  	 <option value="0">Select Meter</option>
       {html_options values=$meterEntryIdArray output=$meterEntryNameArray}  
    </select>
    <span id="masterpara"></span>         
    <span id="masterparasub"></span>  
     {html_select_date time="$callibrationDate" prefix="callibrationDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
    {html_select_date time="$nextYearDate" prefix="nextYearDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY"  day_value_format="%02d"}
    <br />
    Measurement uncertainty is : <input type="text" name="measurementUncertaintyIs" size="100" />
    </td>
  </tr>
  <tr>
    <td align="center" bgcolor="lightblue">INSTRUMENT.:-<span id="showItem"></span><div id="showDetail" required=required></div></td>
    <td align="center" bgcolor="lightblue">Make/Model :<input type="text" name="makeModel" id="makeModel" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Instrument I.D. No. : <input type="text" name="instrumentId" id="instrumentId" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Least Count : <input type="text" name="leastCount" id="leastCount" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Accuracy : <input type="text" name="accuracy" id="accuracy" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Temperature : <input type="text" name="temperature" id="temperature" value="(25�2.5)�C" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Relative Humidity : <input type="text" name="humidity" id="humidity" value="(35 to 65) %" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Date : </td>
    <td align="center" bgcolor="lightblue">User : <input type="hidden" name="" id="" value="" size="10" required=required/></td>
  </tr>    
</table>
<table align="center" border='1' cellpadding='5' cellspacing='0'  >
  <tbody id="mainDiv">
    <tr id="one" class="tableRow">
    	<td>
       <font size="4">UUT Value </font>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="text" name="stdMeter1[]" class="stdMeter1"  size="5" align="right" title="stdMeter1" required=required onchange="meanReading(this);">
          <input type="text" name="stdMeter2[]" class="stdMeter2"  size="5" align="right" title="stdMeter2" required=required onchange="meanReading(this);">
          <input type="text" name="stdMeter3[]" class="stdMeter3"  size="5" align="right" title="stdMeter3" required=required onchange="meanReading(this);">
          <input type="text" name="stdMeter4[]" class="stdMeter4"  size="5" align="right" title="stdMeter4" required=required onchange="meanReading(this);">
          <input type="text" name="stdMeter5[]" class="stdMeter5"  size="5" align="right" title="stdMeter5" required=required onchange="meanReading(this);">
          <input type="text" name="stdMeterAverage[]"           class="stdMeterAverage"           size="5" align="right" title="stdMeterAverage" value="0" READONLY>
          <input type="text" name="accuracyTaken[]"             class="accuracyTaken"             size="5" align="right" title="accuracyTaken" READONLY>
          <input type="text" name="standardDeviation[]"         class="standardDeviation"         size="5" align="right" title="standardDeviation" READONLY>
          <input type="text" name="standardUncertinity[]"       class="standardUncertinity"       size="5" align="right" title="standardUncertinity" READONLY>
          <input type="text" name="standardUncertinityperc[]"   class="standardUncertinityperc"   size="5" align="right" title="standardUncertinityperc" READONLY>
          <input type="text" name="degreeOfFreedom[]"           class="degreeOfFreedom"           size="5" align="right" title="degreeOfFreedom" READONLY>
          <input type="text" name="uncertinityForTypeB[]"       class="uncertinityForTypeB"       size="5" align="right" title="Uncertanity of Master" READONLY>
          <input type="text" name="uncertinityInPercentage[]"   class="uncertinityInPercentage"   size="5" align="right" title="uncertinityInPercentage" READONLY>
          <input type="text" name="accuracyForTypeB[]"          class="accuracyForTypeB"          size="5" align="right" title="accuracyForTypeB" READONLY>
          <input type="text" name="acuuracyForTypeBPerc[]"      class="acuuracyForTypeBPerc"      size="5" align="right" title="acuuracyForTypeBPerc" READONLY>
          <input type="text" name="resolutionTypeB[]"           class="resolutionTypeB"           size="5" align="right" title="resolutionTypeB" READONLY>
          <input type="text" name="resolutionForTypeBPerc[]"    class="resolutionForTypeBPerc"    size="5" align="right" title="resolutionForTypeBPerc" READONLY>
          <input type="text" name="stabilityForTypeB[]"         class="stabilityForTypeB"         size="5" align="right" title="stabilityForTypeB" READONLY>
          <input type="text" name="stabilityForTypeBInPerc[]"   class="stabilityForTypeBInPerc"   size="5" align="right" title="stabilityForTypeBInPerc" READONLY>
          <input type="text" name="combinedUncertinity[]"       class="combinedUncertinity"       size="5" align="right" title="combinedUncertinity" READONLY>
          <input type="text" name="combinedUncertinityInPerc[]" class="combinedUncertinityInPerc" size="5" align="right" title="combinedUncertinityInPerc" READONLY>
          <input type="text" name="effectiveUncertinity[]"      class="effectiveUncertinity"      size="5" align="right" title="effectiveUncertinity" READONLY>
          <input type="text" name="effectiveUncertinityInPer[]" class="effectiveUncertinityInPer" size="5" align="right" title="effectiveUncertinityInPer" READONLY>
          <input type="text" name="effectiveDegreeOfFreed[]"    class="effectiveDegreeOfFreed"    size="5" align="right" title="effectiveDegreeOfFreed" READONLY>
          <input type="text" name="meanReading[]"               class="meanReading"               size="5" align="right" title="MeanReading" READONLY>
          <input type="text" name="masterMeterReading[]"        class="masterMeterReading"        size="5" align="right" title="MeanReading" READONLY>
          <input type="text" name="error[]"                     class="error"                     size="5" align="right" title="Error" READONLY>
          <input type="text" name="expandedUncertinity[]"       class="expandedUncertinity"       size="5" align="right" title="ExpandedUncertinity" READONLY>
          <br>
          <font size="4">Master Meter Value</font>
          <input type="text" name="testMeter1[]" class="testMeter1" id="testMeter1Final1" size="5" align="right" title="testMeter1" required=required onchange="testMeterAvg(this);">
          <input type="text" name="testMeter2[]" class="testMeter2" id="testMeter1Final2" size="5" align="right" title="testMeter2" required=required onchange="testMeterAvg(this);">
          <input type="text" name="testMeter3[]" class="testMeter3" id="testMeter1Final3" size="5" align="right" title="testMeter3" required=required onchange="testMeterAvg(this);">
          <input type="text" name="testMeter4[]" class="testMeter4" id="testMeter1Final4" size="5" align="right" title="testMeter4" required=required onchange="testMeterAvg(this);">
          <input type="text" name="testMeter5[]" class="testMeter5" id="testMeter1Final5" size="5" align="right" title="testMeter5" required=required onchange="testMeterAvg(this);">
          <input type="text" name="testMeterAverage[]"  id="testMeterAverage"  size="5" align="right" title="testMeter5" READONLY>

          <span><input type="button" value="Add" class="add" /></span>
          <span><input type="button" value="Remove" class="delete" /></span>
        </td>
        </tr>
      </tbody>
        <tr>
  <td align="center" colspan="2"><input type="submit" name="insertBtn" value="Submit" class="button" /></td>
</tr>
      </table>
  <div id="rangedisplay"></div> 
</table>
</form>
{include file="footer.tpl"}