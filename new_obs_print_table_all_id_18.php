<?php
    $weight_unit = '';
    $qry1 = 'SELECT *  FROM weight_unit
            WHERE id = '.$grn_data[0]['weight_id'].' ';
    $res1 = mysql_query($qry1) or die("Error :: Cannot select weight_unit details.<hr>".mysql_error());
    while($row1 = mysql_fetch_assoc($res1)) { 
        $weight_unit = $row1['unit_name'];
    }
?>
<?php if($grn_data[0]['obs_type_id'] == 1){ ?>
    <table width="605" align="left" class="main_table"  style="font-size:13px;">
                    <?php
                    $table_title_right = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                            $table_title_right = $grn_entry['table_title_right'];
                            break;
                        }
                    }
                    $decimal_master = 0;
                    $resolution_of_master = $grn_data[0]['resolution_of_master'];

                    if(!empty($resolution_of_master)){
                        $exp_val = explode('.', $resolution_of_master);
                        if(isset($exp_val[1])){
                            $decimal_master = strlen($exp_val[1]);  
                        } else {
                          $decimal_master = 0;  
                        }
                    } else {
                        $decimal_master = 0;
                    }
                    $decimal_least = 0;
                    $resolution_of_least = $grn_data[0]['leastCount'];
                    if(!empty($resolution_of_least)){
                        $exp_least = explode('.', $resolution_of_least);
                        if(isset($exp_least[1])){
                            $decimal_least = strlen($exp_least[1]);  
                        } else {
                          $decimal_least = 0;  
                        }
                    } else {
                        $decimal_least = 0;
                    }
//                    echo "<pre>"; print_r($grn_data); exit; 
//                    echo "<pre>"; print_r($reading_json); exit; 
                    ?>
                    <tr>
                        <th align="center" valign="middle">Title:</th>
                        <th  colspan="4" align="left" valign="middle" style="font-family:dejavusansb; font-size: 11px;">&nbsp; <?php echo $table_title_right; ?></th>
                    </tr>
                    <tr>
                        <th valign="middle" align="center" >Sr. No.</th>
                        <th align="center" valign="middle">Std. reading <br/>of Load in (<?php echo $weight_unit; ?>)</th>
                        <th align="center" valign="middle">Certificate value <br/>of Load (<?php echo $weight_unit; ?>) (Std)</th>
                        <th align="center" valign="middle">Reading of UUC <br/>in (<?php echo $weight_unit; ?>) (Uo)</th>
                        <th align="center" valign="middle">Deviation in <br/>(<?php echo $weight_unit; ?>) (Uo-Std.)</th>
                    </tr>
                    <?php 
                    $stdmeter_data = $reading_json[0]['stdMeter'];
                    $testmeter1_data = $reading_json[0]['testMeter'];
                    $testmeter2_data = isset($reading_json[0]['testMeter2']) ? $reading_json[0]['testMeter2'] : array();
                        for ($x = 0; $x <= max(array_keys($stdmeter_data)); $x++) {
                        $i = $x + 1;
                        ?>
                        <tr>
                            <td align="center" valign="middle"><?php echo $i; ?></td>
                            <td valign="middle" align="center"><?php echo $stdmeter_data[$x]; ?></td>
                            <td valign="middle" align="center"><?php echo $testmeter1_data[$x]; ?></td>
                            <td valign="middle" align="center"><?php echo $testmeter2_data[$x]; ?></td>
                            <?php $dev = ($testmeter2_data[$x] - $testmeter1_data[$x]);  ?>
                            <td valign="middle" align="center"><?php echo number_format((float)$dev, 6, '.', ''); ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td align="left" colspan="5" valign="middle">Max Expanded Uncertainty(%) : &nbsp;<?php echo number_format((float)$grn_data[0]['exp_uncer_per_ue'], 6, '.', ''); ?></td>
                    </tr>
                </table>
<?php } ?>
<?php if($grn_data[0]['obs_type_id'] == 2){ ?>
    <table width="605" align="left" class="main_table"  style="font-size:13px;">
        <tr>
            <th align="center" colspan="2" valign="middle">Eccentricity Test: Load <?php echo $grn_data[0]['uut_range'] / 2; ?> <?php echo $weight_unit; ?></th>
            <th align="center" valign="middle">At 50% Loading</th>
        </tr>
        <tr>
            <th align="center" valign="middle"></th>
            <th align="center" colspan="2" valign="middle">Clockwise / Anti Clockwise</th>
        </tr>
        <tr>
            <td align="center" rowspan="6" valign="middle"><img width="100" src="./images/table_19_img.png" /></td>
            <td align="center" valign="middle">No.</td>
            <td align="center" valign="middle">Location</td>
        </tr>
        <tr>
            <td align="center" valign="middle">1</td>
            <td align="center" valign="middle">Centre</td>
        </tr>
        <tr>
            <td align="center" valign="middle">2</td>
            <td align="center" valign="middle">Back Left Side</td>
        </tr>
        <tr>
            <td align="center" valign="middle">3</td>
            <td align="center" valign="middle">Back Right Side</td>
        </tr>
        <tr>
            <td align="center" valign="middle">4</td>
            <td align="center" valign="middle">Front Right Side</td>
        </tr>
        <tr>
            <td align="center" valign="middle">5</td>
            <td align="center" valign="middle">Front Left Side</td>
        </tr>
    </table>
    <table width="605" align="left" class="main_table"  style="font-size:13px;">
                    <?php
                    $table_title_right = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                            $table_title_right = $grn_entry['table_title_right'];
                            break;
                        }
                    }
                    $decimal_master = 0;
                    $resolution_of_master = $grn_data[0]['resolution_of_master'];

                    if(!empty($resolution_of_master)){
                        $exp_val = explode('.', $resolution_of_master);
                        if(isset($exp_val[1])){
                            $decimal_master = strlen($exp_val[1]);  
                        } else {
                          $decimal_master = 0;  
                        }
                    } else {
                        $decimal_master = 0;
                    }
                    $decimal_least = 0;
                    $resolution_of_least = $grn_data[0]['leastCount'];
                    if(!empty($resolution_of_least)){
                        $exp_least = explode('.', $resolution_of_least);
                        if(isset($exp_least[1])){
                            $decimal_least = strlen($exp_least[1]);  
                        } else {
                          $decimal_least = 0;  
                        }
                    } else {
                        $decimal_least = 0;
                    }
//                    echo "<pre>"; print_r($grn_data); exit; 
//                    echo "<pre>"; print_r($reading_json); exit; 
                    ?>
                    <tr>
                        <th align="center" valign="middle">Title:</th>
                        <th  colspan="3" align="left" valign="middle">&nbsp; <?php echo $table_title_right; ?></th>
                    </tr>
                    <tr>
                        <th valign="middle" colspan="2" align="center" >Clockwise</th>
                        <th align="center" colspan="2" valign="middle">Anti Clockwise</th>
                    </tr>
                    <tr>
                        <th valign="middle" align="center" > Location </th>
                        <th align="center" valign="middle"> Balance Reading in (<?php echo $weight_unit; ?>) </th>
                        <th align="center" valign="middle"> Location </th>
                        <th align="center" valign="middle"> Balance Reading in (<?php echo $weight_unit; ?>) </th>
                    </tr>
                    <?php 
                    $stdmeter_data = $reading_json[0]['stdMeter'];
                    $testmeter1_data = $reading_json[0]['testMeter'];
                    $c_min = '';
                    $c_max = '';
                    $a_min = '';
                    $a_max = '';
                    $e_min = '';
                    $e_max = '';
                        for ($x = 0; $x <= max(array_keys($stdmeter_data)); $x++) {
                        $i = $x + 1;
                        if($c_min == ''){
                            $c_min = $stdmeter_data[$x];
                        } else {
                            if($stdmeter_data[$x] < $c_min){
                                $c_min = $stdmeter_data[$x];
                            }
                        }
                        if($c_max == ''){
                            $c_max = $stdmeter_data[$x];
                        } else {
                            if($stdmeter_data[$x] > $c_max){
                                $c_max = $stdmeter_data[$x];
                            }
                        }
                        if($a_min == ''){
                            $a_min = $testmeter1_data[$x];
                        } else {
                            if($testmeter1_data[$x] < $a_min){
                                $a_min = $testmeter1_data[$x];
                            }
                        }
                        if($a_max == ''){
                            $a_max = $testmeter1_data[$x];
                        } else {
                            if($testmeter1_data[$x] > $a_max){
                                $a_max = $testmeter1_data[$x];
                            }
                        }
                        ?>
                        <tr>
                            <td align="center" valign="middle"><?php echo $i; ?></td>
                            <td valign="middle" align="center"><?php echo $stdmeter_data[$x]; ?></td>
                            <td align="center" valign="middle"><?php echo $i; ?></td>
                            <td valign="middle" align="center"><?php echo $testmeter1_data[$x]; ?></td>
                            <?php // $dev = ($testmeter2_data[$x] - $testmeter1_data[$x]);  ?>
                        </tr>
                    <?php } ?>
                    <?php
                        if($c_min < $a_min){
                            $e_min = $c_min;
                        } else {
                            $e_min = $a_min;
                        }
                        if($c_max > $a_max){
                            $e_max = $c_max;
                        } else {
                            $e_max = $a_max;
                        }
                    ?>
                    <tr>
                        <td valign="middle" align="center">Min : <?php echo number_format((float)$c_min, 4, '.', ''); ?></td>
                        <td valign="middle" align="center">Max : <?php echo number_format((float)$c_max, 4, '.', ''); ?></td>
                        <td valign="middle" align="center">Min : <?php echo number_format((float)$a_min, 4, '.', ''); ?></td>
                        <td valign="middle" align="center">Max : <?php echo number_format((float)$a_max, 4, '.', ''); ?></td>
                    </tr>
                    <tr>
                        <td valign="middle" align="center">Eccentricity : </td>
                        <td valign="middle" align="center">Min : <?php echo number_format((float)$e_min, 4, '.', ''); ?></td>
                        <td valign="middle" align="center">Max : <?php echo number_format((float)$e_max, 4, '.', ''); ?></td>
                        <td valign="middle" align="center"></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="middle">Max Expanded Uncertainty(%) : &nbsp;<?php echo number_format((float)$grn_data[0]['exp_uncer_per_ue'], 6, '.', ''); ?></td>
                    </tr>
                </table>
                <table width="605" align="left" class="main_table"  style="font-size:13px;">
                    <tr>
                        <td rowspan="5" valign="middle"><div style="word-break: break-all; width: 380px; text-align: center; font-family:dejavusans; font-size: 11px;" >Where:  D  is  the  difference  between  maximum  and  minimum  values  from  the  eccentricity  test  performed according to OIML R 76-2; d1 is the estimated distance between the centers of the weights; and d2 is the distance from the center of the load receptor to one of the corners.</div></td>
                        <td align="center" valign="middle"> D= </td>
                        <td align="center" valign="middle"> <?php echo number_format((float)$e_max - $e_min, 4, '.', ''); ?>(<?php echo $weight_unit; ?>) </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle"> d1= </td>
                        <td align="center" valign="middle"> <?php echo $grn_data[0]['d1']; ?> </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle"> d2= </td>
                        <td align="center" valign="middle"> <?php echo $grn_data[0]['d2']; ?> </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle"> UE= </td>
                        <td align="center" valign="middle"> <?php echo number_format((float)(($grn_data[0]['d1']/$grn_data[0]['d2'])*($e_max - $e_min))/(2*sqrt(3)), 6, '.', ''); ?> </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" valign="middle" style="font-family: dejavusans;"> UE=[(d1/d2) *D]/ (2*√3) </td>
                    </tr>
                </table>
<?php } ?>
<?php if($grn_data[0]['obs_type_id'] == 3){ ?>
    <table width="605" align="left" class="main_table"  style="font-size:13px;">
        <?php
        $table_title_right = '';
        foreach ($grn_data as $k => $grn_entry){ 
            if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                $table_title_right = $grn_entry['table_title_right'];
                break;
            }
        }
        $decimal_master = 0;
        $resolution_of_master = $grn_data[0]['resolution_of_master'];

        if(!empty($resolution_of_master)){
            $exp_val = explode('.', $resolution_of_master);
            if(isset($exp_val[1])){
                $decimal_master = strlen($exp_val[1]);  
            } else {
              $decimal_master = 0;  
            }
        } else {
            $decimal_master = 0;
        }
        $decimal_least = 0;
        $resolution_of_least = $grn_data[0]['leastCount'];
        if(!empty($resolution_of_least)){
            $exp_least = explode('.', $resolution_of_least);
            if(isset($exp_least[1])){
                $decimal_least = strlen($exp_least[1]);  
            } else {
              $decimal_least = 0;  
            }
        } else {
            $decimal_least = 0;
        }
//                    echo "<pre>"; print_r($grn_data); exit; 
//                    echo "<pre>"; print_r($reading_json); exit; 
        ?>
        <tr>
            <th align="center" valign="middle">Title:</th>
            <th  colspan="3" align="left" valign="middle">&nbsp; <?php echo $table_title_right; ?></th>
        </tr>
        <tr>
            <th align="center" colspan="4" valign="middle">Repetability Test at 50% of Max. capacity of balance by using nearest single weight : <?php echo $grn_data[0]['uut_range']; ?> (<?php echo $weight_unit; ?>)</th>
        </tr>
        <tr>
            <th valign="middle" align="center" > Load in (<?php echo $weight_unit; ?>) </th>
            <th align="center" valign="middle"> Location </th>
            <th align="center" valign="middle"> Balance Reading in (<?php echo $weight_unit; ?>)</th>
            <th align="center" valign="middle"> Error in (<?php echo $weight_unit; ?>)</th>
        </tr>
        <?php
        $rep = '';
        $stdmeter_data = $reading_json[0]['stdMeter'];
        $testmeter1_data = $reading_json[0]['testMeter'];
        $testmeter2_data = isset($reading_json[0]['testMeter2']) ? $reading_json[0]['testMeter2'] : array();
            for ($x = 0; $x <= max(array_keys($stdmeter_data)); $x++) {
            $i = $x + 1;
            ?>
            <tr>
                <td valign="middle" align="center"><?php echo $stdmeter_data[$x]; ?></td>
                <td valign="middle" align="center"><?php echo $testmeter1_data[$x]; ?></td>
                <td valign="middle" align="center"><?php echo $testmeter2_data[$x]; ?></td>
                <?php $dev = ($stdmeter_data[$x] - $testmeter2_data[$x]);  
                if($rep == ''){
                    $rep = $dev;
                } else {
                    if($dev > $rep){
                        $rep = $dev;
                    }
                }
                ?>
                <td valign="middle" align="center"><?php echo number_format((float)$dev, 6, '.', ''); ?></td>
            </tr>
        <?php } ?>
        <tr>
            <td valign="middle" align="center">Average</td>
            <td valign="middle" align="center"><?php echo number_format((float)$grn_data[0]['testMeter2Average'], 6, '.', ''); ?></td>
            <td valign="middle" align="center">Repetability </td>
            <td valign="middle" align="center"><?php echo number_format((float)$rep, 6, '.', ''); ?></td>
        </tr>
        <tr>
            <td align="left" colspan="4" valign="middle">Max Expanded Uncertainty(%) : &nbsp;<?php echo number_format((float)$grn_data[0]['exp_uncer_per_ue'], 6, '.', ''); ?></td>
        </tr>
    </table>
<?php } ?>