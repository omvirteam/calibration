<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(STAFF_MODULE_ID,"view")) || (have_access_role(STAFF_MODULE_ID,"edit")) || (have_access_role(STAFF_MODULE_ID,"add")) ){
    $staffEntry = array();
    $staffId = isset($_REQUEST['staffId']) ? $_REQUEST['staffId'] : 0;
    $currentStaffName = "";
    $userName = "";
    $password = "";
    $userType = "";
    $address = "";
    $city = "";
    $state = "";
    $country = "";
    $fax = "";
    $phone1 = "";
    $phone2 = "";
    $email = "";
    $web = "";
    $contPerson = "";
    $msg = "";

    //Item Insert : Start
    if (isset($_POST['staffName']))   {
        if (isset($_POST['cancelBtn']))     {
            header("Location: index.php");

            exit();
        }


        if ($staffId > 0)     {
            $updateStaff = "UPDATE staff
                   SET staffName = '" . $_POST['staffName'] . "',userName = '" . $_POST['userName'] . "',password = MD5('" . $_POST['password'] . "'),address = '" . $_POST['address'] . "',
                  city = '" . $_POST['city'] . "',state = '" . $_POST['state'] . "',
                phone1 = '" . $_POST['phone1'] . "',phone2 = '" . $_POST['phone2'] . "',
                 email = '" . $_POST['email'] . "',web = '" . $_POST['web'] . "',
            contPerson = '" . $_POST['contPerson'] . "'
                 WHERE staffId = " . $_POST['staffId'];
            $updateStaffResult = mysql_query($updateStaff);
            $staffId = 0; //We don't want user to remain in Edit part after Update query done.
        }     else     {
            $insertStaffName = "INSERT INTO staff(staffName,userName,password,userType,address,city,state,phone1,phone2,email,web,contPerson)
                          VALUE ('" . $_POST['staffName'] . "','" . $_POST['userName'] . "',MD5('" . $_POST['password'] . "'),'" . $_POST['userType'] . "','" . $_POST['address'] . "','" . $_POST['city'] . "','" . $_POST['state'] . "',
                                 '" . $_POST['phone1'] . "','" . $_POST['phone2'] . "',
                                 '" . $_POST['email'] . "','" . $_POST['web'] . "','" . $_POST['contPerson'] . "')";

            $insertStaffNameResult = mysql_query($insertStaffName);
            if (!$insertStaffNameResult)
                die("Insert Query Not Inserted : " . mysql_error() . " : " . $insertStaffName);
            else
                header("Location:staffEntry.php");
        }
    }
    //Item Insert : End
      //Item Listing : Start
    $staffNameToDisplay = "SELECT staffId,staffName,userName,password,userType,address,city,state,phone1,phone2,email,web,contPerson
                     FROM staff
                    ORDER BY staffName";
    $selectStaffResult = mysql_query($staffNameToDisplay);
    $i = 0;

    while ($staffInRow = mysql_fetch_array($selectStaffResult))   {
        $staffEntry[$i]['staffId'] = $staffInRow['staffId'];
        $staffEntry[$i]['staffName'] = $staffInRow['staffName'];
        $staffEntry[$i]['userName'] = $staffInRow['userName'];
        $staffEntry[$i]['password'] = $staffInRow['password'];
        $staffEntry[$i]['userType'] = $staffInRow['userType'];
        $staffEntry[$i]['address'] = $staffInRow['address'];
        $staffEntry[$i]['city'] = $staffInRow['city'];
        $staffEntry[$i]['state'] = $staffInRow['state'];
        $staffEntry[$i]['phone1'] = $staffInRow['phone1'];
        $staffEntry[$i]['phone2'] = $staffInRow['phone2'];
        $staffEntry[$i]['email'] = $staffInRow['email'];
        $staffEntry[$i]['web'] = $staffInRow['web'];
        $staffEntry[$i]['contPerson'] = $staffInRow['contPerson'];


        if ($staffInRow['staffId'] == $staffId)     {
            $currentStaffName = $staffInRow['staffName'];
            $userName = $staffInRow['userName'];
            $password = $staffInRow['password'];
            $userType = $staffInRow['userType'];
            $address = $staffInRow['address'];
            $city = $staffInRow['city'];
            $state = $staffInRow['state'];
            $phone1 = $staffInRow['phone1'];
            $phone2 = $staffInRow['phone2'];
            $email = $staffInRow['email'];
            $web = $staffInRow['web'];
            $contPerson = $staffInRow['contPerson'];
        }
        $i++;
    }
    //Item Listing : End
    include("./bottom.php");
    $smarty->assign("staffEntry", $staffEntry);
    $smarty->assign("staffId", $staffId);
    $smarty->assign("currentStaffName", $currentStaffName);
    $smarty->assign("userName", $userName);
    $smarty->assign("password", $password);
    $smarty->assign("userType", $userType);
    $smarty->assign("address", $address);
    $smarty->assign("city", $city);
    $smarty->assign("state", $state);
    $smarty->assign("phone1", $phone1);
    $smarty->assign("phone2", $phone2);
    $smarty->assign("email", $email);
    $smarty->assign("web", $web);
    $smarty->assign("contPerson", $contPerson);
    $smarty->assign("msg", $msg);
    $smarty->assign("currentStaffName", $currentStaffName);
    $smarty->display("staffEntry.tpl");
} else {
  header("Location:index.php");
}  
}

?>