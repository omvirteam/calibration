<?php
    $stdmeter_data = isset($reading_json[0]['stdMeter']) ? $reading_json[0]['stdMeter'] : array();
    $testmeter1_data = isset($reading_json[0]['testMeter']) ? $reading_json[0]['testMeter'] : array();
    $testmeter2_data = isset($reading_json[0]['testMeter2']) ? $reading_json[0]['testMeter2'] : array();
    $testmeter3_data = isset($reading_json[0]['testMeter3']) ? $reading_json[0]['testMeter3'] : array();
    $uuc_min_range = $uuc_data[0]['uut_range_min'];
    $uuc_max_range = $uuc_data[0]['uut_range'];
    $uuc_range = 0;
    if($uuc_min_range < 0){
        $uuc_range = abs($uuc_min_range) + abs($uuc_max_range);
    } else {
        if($uuc_min_range > $uuc_max_range){
            $uuc_range = abs($uuc_min_range);
        } else {
            $uuc_range = abs($uuc_max_range);
        }
    }
    $table_title = '';
        foreach ($grn_data as $k => $grn_entry){ 
            if(!empty($grn_entry['table_title'])){
                $table_title = $grn_entry['table_title'];
                break;
            }
        }
    $table_title_right = '';
        foreach ($grn_data as $k => $grn_entry){ 
            if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                $table_title_right = $grn_entry['table_title_right'];
                break;
            }
        }
//                echo "<pre>"; print_r($grn_entry); exit;
        $decimal_master = 0;
        $resolution_of_master = $grn_data[0]['resolution_of_master'];
        if(!empty($resolution_of_master)){
            $exp_val = explode('.', $resolution_of_master);
            if(isset($exp_val[1])){
                $decimal_master = strlen($exp_val[1]);  
            } else {
              $decimal_master = 0;  
            }
        } else {
            $decimal_master = 0;
        }
        $decimal_least = 0;
        $resolution_of_least = $grn_data[0]['leastCount'];
        if(!empty($resolution_of_least)){
            $exp_least = explode('.', $resolution_of_least);
            if(isset($exp_least[1])){
                $decimal_least = strlen($exp_least[1]);  
            } else {
              $decimal_least = 0;  
            }
        } else {
            $decimal_least = 0;
        }
        $qry13 = 'SELECT range_unit,coverage_factor,cmc FROM mastermetersubsub
        WHERE masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
        $res13 = mysql_query($qry13) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $m_range_name = '';
        $m_coverage_factor = '';
        $m_cmc = '';
        while($row13 = mysql_fetch_assoc($res13)) {
            $m_range_name = $row13['range_unit'];
            $m_coverage_factor = $row13['coverage_factor'];
            $m_cmc = $row13['cmc'];
        }
        $max_key1 = !empty($testmeter1_data) ? max(array_keys($testmeter1_data)) : '0';
        $max_key2 = !empty($testmeter2_data) ? max(array_keys($testmeter2_data)) : '0';
        $max_key3 = !empty($testmeter3_data) ? max(array_keys($testmeter3_data)) : '0';
        $max_key4 = !empty($stdmeter_data) ? max(array_keys($stdmeter_data)) : '0';
        $max_key = 0; 
        if(($max_key1 >= $max_key2) && ($max_key1 >= $max_key3) && ($max_key1 >= $max_key4)){
            $max_key = $max_key1;
        } else if(($max_key2 >= $max_key3) && ($max_key2 >= $max_key1) && ($max_key2 >= $max_key4)){
            $max_key = $max_key2;
        } else if(($max_key3 >= $max_key1) && ($max_key3 >= $max_key2) && ($max_key3 >= $max_key4)){
            $max_key = $max_key3;
        } else if(($max_key4 >= $max_key1) && ($max_key4 >= $max_key2) && ($max_key4 >= $max_key3)){
            $max_key = $max_key4;
        }
        $col_span1 = '7';
        if(!empty($view_uncertainity)){
            $col_span1 = $col_span1 + 1;
        }
?>

<table width="605" align="left" class="main_table"  style="font-size:13px;">
                <tr>
                    <th align="center" valign="middle">Title:</th>
                    <th colspan="<?= $col_span1; ?>" align="left" valign="middle" style="font-family:dejavusansb; font-size: 11px;">&nbsp; <?php echo $table_title_right; ?></th>
                </tr>
                <tr>
                    <th valign="middle" align="center" rowspan="3">Step</th>
                    <th colspan="1" align="center" valign="middle">UUC Reading</th>
                    <th align="center" valign="middle" colspan="3">Master Reading</th>
                    <th align="center" colspan="1" valign="middle">Average</th>
                    <th align="center" colspan="1" valign="middle">Measurement</th>
                    <?php if(!empty($view_uncertainity)){ ?>
                        <th align="center" rowspan="3" valign="middle">Expanded <br/>Uncertainty(%)</th>
                    <?php } ?>
                </tr>
                <tr>
                    <th valign="middle" align="center">Pind</th>
                    <th valign="middle" align="center">M1</th>
                    <th valign="middle" align="center">M2</th>
                    <th valign="middle" align="center">M3</th>
                    <th valign="middle" align="center"> Miw</th>
                    <th valign="middle" align="center">Deviation</th>
                </tr>
                <tr>
                    <td valign="middle" align="center"><span style="font-family:dejavusansb;"><?php echo $m_range_name; ?></span></td>
                    <td valign="middle" align="center"><span style="font-family:dejavusansb;"><?php echo $m_range_name; ?></span></td>
                    <td valign="middle" align="center"><span style="font-family:dejavusansb;"><?php echo $m_range_name; ?></span></td>
                    <td valign="middle" align="center"><span style="font-family:dejavusansb;"><?php echo $m_range_name; ?></span></td>
                    <th valign="middle" align="center">((M1+M2)/2</th>
                    <th valign="middle" align="center">P % FS</th>
                </tr>
                <?php
                $m1_total = 0;
                $m2_total = 0;
                $max_m2_m1_h = 0;
                $max_m1_m3_h = 0;
                $hysteresis_val = '';
                $repetability_val = '';
                $one_avg = '';
                $max_avg = '';
                for($x = 0; $x <= $max_key; $x++) { ?>
                    <tr>
                        <td align="center" valign="middle"><?php echo $x + 1; ?></td>
                        <?php $std_mter = (isset($stdmeter_data[$x]) ? number_format((float)$stdmeter_data[$x], $decimal_least, '.', '') : '0'); ?>
                        <td valign="middle" align="center"><?= (isset($stdmeter_data[$x]) ? number_format((float)$stdmeter_data[$x], $decimal_least, '.', '') : ''); ?></td>
                        <?php $m1 = (isset($testmeter1_data[$x]) ? number_format((float)$testmeter1_data[$x], $decimal_master, '.', '') : '0'); ?>
                        <td valign="middle" align="center"><?= (isset($testmeter1_data[$x]) ? number_format((float)$testmeter1_data[$x], $decimal_master, '.', '') : '0'); ?></td>
                        <?php $m2 = (isset($testmeter2_data[$x]) ? number_format((float)$testmeter2_data[$x], $decimal_master, '.', '') : '0'); ?>
                        <td valign="middle" align="center"><?= (isset($testmeter2_data[$x]) ? number_format((float)$testmeter2_data[$x], $decimal_master, '.', '') : ''); ?></td>
                        <?php $m3 = (isset($testmeter3_data[$x]) ? number_format((float)$testmeter3_data[$x], $decimal_master, '.', '') : '0'); ?>
                        <td valign="middle" align="center"><?= (isset($testmeter3_data[$x]) ? number_format((float)$testmeter3_data[$x], $decimal_master, '.', '') : ''); ?></td>
                        <?php $avg = (($m1 + $m2)/2); ?>
                        <?php $dev = (($std_mter - $avg)/$uuc_range)*100; ?>
                        <td valign="middle" align="center"><?= number_format((float)$avg, $decimal_master, '.', ''); ?></td>
                        <td valign="middle" align="center"><?= number_format((float)$dev, 3, '.', ''); ?></td>
                        <?php if(!empty($view_uncertainity)){ ?>
                            <td valign="middle" align="center">
                                <?php 
                                //echo number_format((float)$max_ex_unce_p, 2, '.', ''); 
                                // echo isset($type_a_type_b_json[$k]['exp_uncer_per_ue_without_cmc']) ? sprintf("%.2f",$type_a_type_b_json[$k]['exp_uncer_per_ue_without_cmc']) : '';
                                echo isset($type_a_type_b_json[$k]['exp_uncer_per_ue']) ? sprintf("%.4f",$type_a_type_b_json[$k]['exp_uncer_per_ue']) : '';
                                ?>
                            </td>
                        <?php } ?>
                    </tr>
                    <?php
                    if($x == 0){
                        $one_avg = $avg;
                        $max_avg = $avg;
                    }
                    $m1_total = $m1_total + $m1;
                    $m2_total = $m2_total + $m2;
                    $new_max_m2_m1_h = number_format((float)abs($m2 - $m1), $decimal_master, '.', '');
                    if($new_max_m2_m1_h > $max_m2_m1_h){
                        $max_m2_m1_h = $new_max_m2_m1_h;
                    }
                    $new_max_m1_m3_h = number_format((float)$m1 - $m3, $decimal_master, '.', '');
                    if($new_max_m1_m3_h > $max_m1_m3_h){
                        $max_m1_m3_h = $new_max_m1_m3_h;
                    }
                    if($avg > $max_avg){
                        $max_avg = $avg;
                    }
                    $max_m2_m1 = number_format((float)abs($m2 - $m1), $decimal_master, '.', '');
                    if($hysteresis_val == ''){
                        $hysteresis_val = $max_m2_m1;
                    } else {
                        if($max_m2_m1 > $hysteresis_val){
                            $hysteresis_val = $max_m2_m1;
                        }
                    }
                    $max_m1_m3 = number_format((float)$m1 - $m3, $decimal_master, '.', '');
                    if($repetability_val == ''){
                        $repetability_val = $max_m1_m3;
                    } else {
                        if($max_m1_m3 > $repetability_val){
                            $repetability_val = $max_m1_m3;
                        }
                    }
                } 
                // calculation for Uncertainty (max) : Start //
                    $U5 = $grn_data[0]['uncertinty_from_tracebility'] / 2;
                    $U7 = $grn_data[0]['accuracy_taking_account'] / sqrt(3);
                    $U9 = ($max_m2_m1_h / 2) / sqrt(3);
                    $U11 = ($max_m1_m3_h / 2) / sqrt(3);
                    $U13 = ($one_avg / 2) / sqrt(3);
                    $U15 = ($decimal_least / 2) / sqrt(3);
                    $U17 = sqrt(pow($U5,2)+pow($U7,2)+pow($U9,2)+pow($U11,2)+pow($U13,2)+pow($U15,2)); // =SQRT(U5^2+U7^2+U9^2+U11^2+U13^2+U15^2)
                    $U18 = $U17 * $m_coverage_factor; // =U17*M22
                    if(!empty($max_avg)){
                        $U19 = $U18 * 100 / $max_avg; // =U18*100/MAX(G29:G38)
                    } else {
                        $U19 = 0;
                    }
                    if($U19 > $m_cmc){
                        $uncertainty_max = $U19;
                    } else {
                        $uncertainty_max = $m_cmc;
                    }
                // calculation for Uncertainty (max) : End //
                ?>
                <tr>
                    <td colspan="3" align="center" valign="middle">Hysteresis (max)</td>
                    <td colspan="1" valign="middle" align="center"><span style="font-family:dejavusans;"><?php echo number_format((float)$hysteresis_val, $decimal_master, '.', ''); ?> <?php echo $m_range_name; ?></span></td>
                    <td colspan="2" align="center" valign="middle">&nbsp;</td>
                    <td colspan="1" valign="middle" align="center">&nbsp;</td>
                    <?php if(!empty($view_uncertainity)){ ?>
                        <td valign="middle" align="center"></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td colspan="3" align="center" valign="middle">Repeatability</td>
                    <td colspan="1" valign="middle" align="center"><?php echo number_format((float)$repetability_val, $decimal_master, '.', ''); ?></td>
                    <?php if(!empty($view_uncertainity)){ ?>
                        <td valign="middle" colspan="4" align="center"></td>
                    <?php } else { ?>
                    <td colspan="2" align="center" valign="middle">Exp. Uncertainty(max)%</td>
                    <td colspan="1" valign="middle" align="center"><?php echo number_format((float)$max_ex_unce_p, 2, '.', ''); ?> <?php echo $m_range_name; ?></td>
                    <?php } ?>
                </tr>
                <tr>
                    <td colspan="2" align="center" valign="middle">Remark</td>
                    <?php if(!empty($view_uncertainity)){ ?>
                        <td colspan="6" align="left" valign="middle"><?php echo $instrumentDetails['certi_remarks']; ?></td>
                    <?php } else { ?>
                        <td colspan="5" align="left" valign="middle"><?php echo $instrumentDetails['certi_remarks']; ?></td>
                    <?php } ?>
                </tr>
            </table>