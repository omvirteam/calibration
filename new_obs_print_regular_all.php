<?php
include("include/omConfig.php");

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
	if($haystack == null){
		return false;
	}else{
 foreach($haystack as $item) {
 	      if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
}
    return false;
}
$view_uncertainity = 0;
if(isset($_GET['withuncer'])){
    $view_uncertainity = 1;
}
// general functions - start //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// variable list - start //
$certificateNo 			= '';
$certificate_no			= '';
$grnNo 					= '';
$grn_no 					= '';
$grnDate				= ''; 
$totalPages 			= 0;
$callibrationDate		= '';
$callibrationDateNext 	= '';
$certificateIssue		= '';
$certificateIssueDate	= '';
$customerName			= '';
$customerAddress		= '';
$customerCity		= '';
$customerRefNo			= '';
$ambientTemperature		= '';
$relativeHumidity		= '';
$remarks				= '';

$counter 				= 0;
$grnDetailIds 			= array();
$tempArr 				= array() ;

$instrumentDetails					= array();
$instrumentDetails['item_name']		= '';
$instrumentDetails['make']			= '';
$instrumentDetails['item_code']		= '';
$instrumentDetails['id_no']			= '';
$instrumentDetails['range_value']	= '';
$instrumentDetails['accuracy']		= '';
$instrumentDetails['grn_condition']	= '';
$instrumentDetails['certi_remarks']	= '';
$instrumentDetails['cali_location']	= '';
$instrumentDetails['uuc_location']	= '';
$instrumentDetails['ulr_no']	= '';
$instrumentDetails['userName']	= '';
$instrumentDetails['approved_by']	= '';
$instrumentDetails['designation']	= '';
$instrumentDetails['sr_no']	= '';

$masterMeters						= array();
$masterMeters['masterMeterName']			= array();
$masterMeters['masterMeterIdNo']				= array();
$masterMeters['masterMeterMake']			= array();
$masterMeters['masterMeterModelNo']				= array();
$masterMeters['masterMeterSerialNo']			= array();
$masterMeters['masterMeterCertificateNo']		= array();
$masterMeters['due_date']	= array();

$observations						= array();
/*$observations['parameters']			= array();
$observations['table_no']			= array();
$observations['range1']				= array();
$observations['reading1']			= array();
$observations['range2']				= array();
$observations['reading2']			= array();
$observations['units']				= array();
$observations['rdg']				= array();
$observations['expanded']			= array();*/
// variable list - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// fetch data - start //

$obs_type_ids_sql = '';
if(isset($_GET['type_1'])){
    $qry1 = 'SELECT *,ti.note as table_info_note FROM observation_data as grn
            LEFT JOIN table_info as ti ON ti.table_info_id = grn.table_info_id
            WHERE grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].' AND grn.table_info_id = "15"  ORDER BY grn.table_no ASC ';
    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
    $grnobsmaster_data = array();
    while($row1 = mysql_fetch_assoc($res1)) {
        array_push($grnobsmaster_data,$row1);
    }
    $obs_type_ids_arr = array();
    foreach ($grnobsmaster_data as $main_key => $grn_data){ 
        $uuc_data = json_decode(json_encode(json_decode($grn_data['uuc_json'])), true);
        unset($grn_data['uuc_json']);
        if(!empty($_GET['type_1'])){
            if($uuc_data[0]['obs_type_id'] == 1){
                $obs_type_ids_arr[] = $grn_data['id'];
            }
        }
        if(!empty($_GET['type_2'])){
            if($uuc_data[0]['obs_type_id'] == 2){
                $obs_type_ids_arr[] = $grn_data['id'];
            }
        }
        if(!empty($_GET['type_3'])){
            if($uuc_data[0]['obs_type_id'] == 3){
                $obs_type_ids_arr[] = $grn_data['id'];
            }
        }
    }
    if(!empty($obs_type_ids_arr)){
        $obs_type_ids_sql = ' AND grn.id IN ('.implode(",",$obs_type_ids_arr).') ';
    }
//    echo "<pre>"; print_r($obs_type_ids_sql); exit;
}

$qry = 'SELECT DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate,
	DATE_FORMAT(grndetail.nextYearDate,"%d/%m/%Y") AS nextYearDate,
	certificateIssue,DATE_FORMAT(grndetail.certificateIssueDate,"%d/%m/%Y") AS certificateIssueDate,
	grndetail.grnId,grndetail.certificate_no,grndetail.grnDetailId,
	grndetail.temperature,grndetail.humidity,itemCode,
	certiRemarks,
	CONCAT("N-",grnmaster.grnNo) AS grnNo,grnmaster.grn_no,
	DATE_FORMAT(grnmaster.grnDate,"%d/%m/%Y") AS grnDate,
	customer.custName,customer.address,customer.city,
	customer.custCode AS customerRef 
	FROM grndetail 
	LEFT JOIN grnmaster ON grnmaster.grnId = grndetail.grnId 
	LEFT JOIN customer ON customer.customerId = grnmaster.customerId 
	WHERE grndetail.grnDetailId = '.$_GET['grnDetailId'];
$res = mysql_query($qry) or die("Error :: Cannot select basic details.<hr>".mysql_error());
if($row = mysql_fetch_assoc($res)) {
	$grn_no 					= $row['grn_no'];
	$certificate_no 					= $row['certificate_no'];
	$grnDate				= $row['grnDate'];
	$callibrationDate		= $row['callibrationDate'];
	$callibrationDateNext 	= $row['nextYearDate'];
	$certificateIssue		= $row['certificateIssue'];
	$certificateIssueDate	= $row['certificateIssueDate'];
	$customerName			= $row['custName'];
	$customerAddress		= $row['address'];
	$customerCity		= $row['city'];
        $ref_no = '';
        $words = preg_split("/\s+/", trim($row['custName']));
//        echo "<pre>"; print_r($words); exit;
        $acronym = "";
        foreach ($words as $w) {
            if(strlen($acronym) == 2){
                break;
            } else {
                $acronym .= $w[0];
            }
        }
        $cust_sn = Strtoupper($acronym);
	$customerRefNo			= $cust_sn.'/Cali/'.$row['customerRef'];
	$ambientTemperature		= $row['temperature'];
	$relativeHumidity		= $row['humidity'];
	
	$tempArr = $instrumentDetails;
	// instrument details query //
	$qry1 = 'SELECT grndetail.grnDetailId,grndetail.makeModel AS make,grndetail.itemCode AS item_code,
		grndetail.instrumentId AS id_no,grndetail.rangeValue AS range_value,
		grndetail.accuracy,grndetail.grnCondition AS grn_condition,item.itemName AS item_name,
		grndetail.certiRemarks AS certi_remarks, grndetail.cali_location, grndetail.uuc_location,grndetail.ulr_no, staff.staffName as userName, grndetail.sr_no, approved_by.name as approved_by, approved_by.designation
		FROM grndetail 
		LEFT JOIN item ON item.itemId = grndetail.itemId 
		LEFT JOIN approved_by ON approved_by.id = grndetail.approved_by 
                LEFT JOIN staff ON staff.staffId = grndetail.user_id 
		WHERE grndetail.callibrationDate IS NOT NULL AND grndetail.grnId = '.$row['grnId'].' AND grndetail.grnDetailId = "'.$row['grnDetailId'].'"';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        while($row1 = mysql_fetch_assoc($res1)) { 
		array_push($grnDetailIds,$row1['grnDetailId']);
		foreach($tempArr as $key=>$val) {
			if(!in_array_r($row1[$key],$tempArr[$key])) {
				$tempArr[$key][$counter] = $row1[$key];
				$instrumentDetails[$key] .= $row1[$key].' | ';
			}
		}
		$counter++;
	}
	
	foreach($instrumentDetails as $key=>$val) {
		if($val != '')
			$instrumentDetails[$key] = substr($val,0,-3);
	}
	mysql_free_result($res1);
	
	$tempArr = $masterMeters;
	// master meters query //
        $all_least = '';
        $all_uut = '';
        $qry2 = 'SELECT master_json,uuc_json FROM observation_data as grn
		WHERE grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].' AND grn.table_info_id = "15" '.$obs_type_ids_sql.' ';
	$res2 = mysql_query($qry2) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $master_data = array();
//        echo "<pre>"; print_r(mysql_fetch_assoc($res1)); exit;
        while($row2 = mysql_fetch_assoc($res2)) {
                array_push($master_data,$row2);
        }
//        echo "<pre>"; print_r($master_data); exit;
        $master_meter_ids = array();
        $uuc_ranges = array();
        $all_least_arr = array();
        $max_count_page = count($master_data) + 1;
        foreach ($master_data as $m_key => $ms_data){ 
           
            $m_data = json_decode(json_encode(json_decode($ms_data['master_json'])), true);
            $u_data = json_decode(json_encode(json_decode($ms_data['uuc_json'])), true);
//             echo "<pre>"; print_r($m_data); exit;
//            $all_uut = $u_data[0]['uut_range'];
//            isset($uuc_data['uut_range_min']) ? $uuc_data['uut_range_min'] : '';
            foreach ($u_data as $uu_data){ 
                if($uu_data['uut_range_min'] != ''){
                    $uuu_name = $uu_data['uut_range_min'] ." to ". $uu_data['uut_range'];
                } else {
                    $uuu_name = $uu_data['uut_range'];
                }
                if(isset($uu_data['leastCount_print']) &&  ($uu_data['leastCount_print'] != '')){
                    $all_least_arr[] = $uu_data['leastCount_print'];
                }
                $uuc_ranges[] = $uuu_name;
            }
            foreach ($m_data as $s_data){ 
                $master_meter_ids[] = $s_data['masterMeterId'];
            }
            
        }
        $uni_uut = array_unique($uuc_ranges);
        $all_uut = implode(",",$uni_uut);
        $all_least_arr1 = array_unique($all_least_arr);
        $all_least = implode(",",$all_least_arr1);
	$qry2 = 'SELECT masterMeterName as "0",masterMeterIdNo as "1",masterMeterMake as "2",masterMeterModelNo as "3",masterMeterSerialNo as "4",masterMeterCertificateNo as "5",
		 DATE_FORMAT(masterMeterExp,"%d/%m/%Y") AS "6"
                 FROM mastermeter 
		 WHERE masterMeterId IN ('.implode(",",$master_meter_ids).')'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res2 = mysql_query($qry2) or die("Error :: Cannot select master meter details1.<hr>".mysql_error());
	$master_m_data = array();
        while($row2 = mysql_fetch_assoc($res2)) { 
            $master_m_data[] = $row2;
	}
//        echo "<pre>"; print_r($master_m_data); exit;
	$qry21 = 'SELECT GROUP_CONCAT(DISTINCT procedureText) as procedureText, GROUP_CONCAT(DISTINCT page_title) as page_title
                 FROM mastermeter 
		 WHERE masterMeterId IN ('.implode(",",$master_meter_ids).')'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res21 = mysql_query($qry21) or die("Error :: Cannot select master meter details2.<hr>".mysql_error());
	$master_procedureText = '';
	$page_title = '';
        while($row21 = mysql_fetch_assoc($res21)) { 
            $master_procedureText = $row21['procedureText'];
            $page_title = $row21['page_title'];
	}
//        echo "<pre>"; print_r($master_procedureText); exit;
//print '<pre>'; print_r($masterMeters);
	mysql_free_result($res2);
}
mysql_free_result($res);
// fetch data - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// html - start //
ob_start();
?>
<?php 
$qry112 = 'SELECT * FROM setting ';
$res112 = mysql_query($qry112) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
$s_trac = '';
$s_approve = '';
$s_note = '';
$s_all_note = '';
$s_page_1_note = '';
while($row112 = mysql_fetch_assoc($res112)) { 
    if($row112['setting_name'] == 'traceability_to'){
        $s_trac = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'approved_by'){
        $s_approve = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'footer_note_for_page_1'){
        $s_note = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'footer_note_for_all'){
        $s_all_note = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'page_1_remark'){
        $s_page_1_note = $row112['setting_value'];
    }
}
?>
<style>
table {font-family: "Times New Roman"; font-size: 13px;}
tr {line-height: 21px;}
</style>

<?php
    $one_page_lines = 33;
    $total_table_array = array();
    $qry1 = 'SELECT *,ti.note as table_info_note FROM observation_data as grn
            LEFT JOIN table_info as ti ON ti.table_info_id = grn.table_info_id
            WHERE grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].' AND grn.table_info_id = "15" '.$obs_type_ids_sql.'  ORDER BY grn.table_no ASC ';
    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
    $grnobsmaster_data = array();
    while($row1 = mysql_fetch_assoc($res1)) {
            array_push($grnobsmaster_data,$row1);
    }
//        echo "<pre>"; print_r($grnobsmaster_data); exit;
    foreach ($grnobsmaster_data as $main_key => $grn_data){ 
        $master_data = json_decode(json_encode(json_decode($grn_data['master_json'])), true);
        $uuc_data = json_decode(json_encode(json_decode($grn_data['uuc_json'])), true);
        $type_a_type_b_json = json_decode(json_encode(json_decode($grn_data['type_a_type_b_json'])), true);
        $reading_json = json_decode(json_encode(json_decode($grn_data['reading_json'])), true);
        unset($grn_data['master_json']);
        unset($grn_data['uuc_json']);
        unset($grn_data['type_a_type_b_json']);
        unset($grn_data['reading_json']);
        
        if($grn_data['table_info_id'] == 12){
        
            $selecttb = "SELECT * FROM accuracy_class WHERE id = ".$uuc_data[0]['ct_class']." ";
            $selecttbResult = mysql_query($selecttb);
            while ($selecttbRow = mysql_fetch_array($selecttbResult)) {
                $all_least = $selecttbRow['ct_class'];
            }
        }
    
        $main_arr = array();
        foreach ($master_data as $key => $master_dat){
            $t_arr = array_merge($master_dat,$uuc_data[$key],$type_a_type_b_json[$key],$grn_data);
            $main_arr[] = $t_arr;
        }
        $grn_data = $main_arr;
        $pre_range = '';
        $pre_range_key = '';
        $pre_range_total = '';
        foreach ($grn_data as $k => $grn_entry){ 
            if(empty($pre_range)){
                $pre_range = $grn_entry['mRangeId'];
                $pre_range_key = $k;
                $pre_range_total = 1;
                if(isset($grn_data[$k+1])){} else {
                    $grn_data[$k]['row_span'] = 1;
                }
            } else {
                if($pre_range == $grn_entry['mRangeId']){
                    $pre_range_total = $pre_range_total + 1;
                    if(isset($grn_data[$k+1])){} else {
                        $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                    }
                } else {
                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                    $pre_range = $grn_entry['mRangeId'];
                    $pre_range_key = $k;
                    $pre_range_total = 1;
                    if(isset($grn_data[$k+1])){} else {
                        $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                    }
                }
            }
        }
        $t_pre_range = '';
        $t_pre_range_key = '';
        $t_pre_range_total = '';
        foreach ($grn_data as $k => $grn_entry){ 
            if($k == 0){

                $t_pre_range = $grn_entry['uut_range'];
                $t_pre_range_key = $k;
                $t_pre_range_total = 1;
                if(isset($grn_data[$k+1])){} else {
                    $grn_data[$k]['row_span_test'] = 1;
                }
            } else {
                if($t_pre_range == $grn_entry['uut_range']){
                    $t_pre_range_total = $t_pre_range_total + 1;
                    if(isset($grn_data[$k+1])){} else {
                        $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                    }
                } else {
                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                    $t_pre_range = $grn_entry['uut_range'];
                    $t_pre_range_key = $k;
                    $t_pre_range_total = 1;
                    if(isset($grn_data[$k+1])){} else {
                        $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                    }
                }
            }
        }
        if($grn_data[0]['table_info_id'] == '6'){
            $count_row = 6;
        } else if($grn_data[0]['table_info_id'] == '7'){
            $count_row = 7;
        } else if($grn_data[0]['table_info_id'] == '12'){
            $count_row = 20;
        } else if($grn_data[0]['table_info_id'] == '8'){
            $count_row = 6;
        } else if($grn_data[0]['table_info_id'] == '17'){
            $count_row = 7;
        } else if($grn_data[0]['table_info_id'] == '13'){
            $count_row = 5;
        } else if($grn_data[0]['table_info_id'] == '14'){
            $count_row = 6;
        } else if($grn_data[0]['table_info_id'] == '11'){
            $count_row = 26;
        } else if($grn_data[0]['table_info_id'] == '4'){
            $count_row = 6;
        } else if($grn_data[0]['table_info_id'] == '18'){
            if($grn_data[0]['obs_type_id'] == 1){
                $stdmeter_data = $reading_json[0]['stdMeter'];
                $count_row = 4 + max(array_keys($stdmeter_data));
            }
            if($grn_data[0]['obs_type_id'] == 2){
                $stdmeter_data = $reading_json[0]['stdMeter'];
                $count_row = 18 + max(array_keys($stdmeter_data));
            }
            if($grn_data[0]['obs_type_id'] == 3){
                $stdmeter_data = $reading_json[0]['stdMeter'];
                $count_row = 4 + max(array_keys($stdmeter_data));
            }
            
        } else if($grn_data[0]['table_info_id'] == '2'){
            $stdmeter_data = $reading_json[0]['stdMeter'];
            $testmeter1_data = $reading_json[0]['testMeter'];
            $testmeter2_data = isset($reading_json[0]['testMeter2']) ? $reading_json[0]['testMeter2'] : array();
            $testmeter3_data = isset($reading_json[0]['testMeter3']) ? $reading_json[0]['testMeter3'] : array();
            $max_key1 = max(array_keys($testmeter1_data));
            $max_key2 = !empty($testmeter2_data) ? max(array_keys($testmeter2_data)) : '0';
            $max_key3 = !empty($testmeter3_data) ? max(array_keys($testmeter3_data)) : '0';
            $max_key4 = max(array_keys($stdmeter_data));
            $max_key = 0; 
            if(($max_key1 >= $max_key2) && ($max_key1 >= $max_key3) && ($max_key1 >= $max_key4)){
                $max_key = $max_key1;
            } else if(($max_key2 >= $max_key3) && ($max_key2 >= $max_key1) && ($max_key2 >= $max_key4)){
                $max_key = $max_key2;
            } else if(($max_key3 >= $max_key1) && ($max_key3 >= $max_key2) && ($max_key3 >= $max_key4)){
                $max_key = $max_key3;
            } else if(($max_key4 >= $max_key1) && ($max_key4 >= $max_key2) && ($max_key4 >= $max_key3)){
                $max_key = $max_key4;
            }
            $count_row = 8 + $max_key;
        } else if($grn_data[0]['table_info_id'] == '16'){
            $stdmeter_data = $reading_json[0]['stdMeter'];
            $testmeter1_data = $reading_json[0]['testMeter'];
            $testmeter2_data = isset($reading_json[0]['testMeter2']) ? $reading_json[0]['testMeter2'] : array();
            $testmeter3_data = isset($reading_json[0]['testMeter3']) ? $reading_json[0]['testMeter3'] : array();
            $testmeter4_data = isset($reading_json[0]['testMeter4']) ? $reading_json[0]['testMeter4'] : array();
            $testmeter5_data = isset($reading_json[0]['testMeter5']) ? $reading_json[0]['testMeter5'] : array();
            $testmeter6_data = isset($reading_json[0]['testMeter6']) ? $reading_json[0]['testMeter6'] : array();
            $max_key1 = max(array_keys($testmeter1_data));
            $max_key2 = !empty($testmeter2_data) ? max(array_keys($testmeter2_data)) : '0';
            $max_key3 = !empty($testmeter3_data) ? max(array_keys($testmeter3_data)) : '0';
            $max_key4 = !empty($testmeter4_data) ? max(array_keys($testmeter4_data)) : '0';
            $max_key5 = !empty($testmeter5_data) ? max(array_keys($testmeter5_data)) : '0';
            $max_key6 = !empty($testmeter6_data) ? max(array_keys($testmeter6_data)) : '0';
            $max_key7 = max(array_keys($stdmeter_data));
            $max_key = 0; 
            if(($max_key1 >= $max_key2) && ($max_key1 >= $max_key3) && ($max_key1 >= $max_key4) && ($max_key1 >= $max_key5) && ($max_key1 >= $max_key6) && ($max_key1 >= $max_key7)){
                $max_key = $max_key1;
            } else if(($max_key2 >= $max_key3) && ($max_key2 >= $max_key1) && ($max_key2 >= $max_key4) && ($max_key2 >= $max_key5) && ($max_key2 >= $max_key6) && ($max_key2 >= $max_key7)){
                $max_key = $max_key2;
            } else if(($max_key3 >= $max_key1) && ($max_key3 >= $max_key2) && ($max_key3 >= $max_key4) && ($max_key3 >= $max_key5) && ($max_key3 >= $max_key6) && ($max_key3 >= $max_key7)){
                $max_key = $max_key3;
            } else if(($max_key4 >= $max_key1) && ($max_key4 >= $max_key2) && ($max_key4 >= $max_key3) && ($max_key4 >= $max_key5) && ($max_key4 >= $max_key6) && ($max_key4 >= $max_key7)){
                $max_key = $max_key4;
            } else if(($max_key5 >= $max_key1) && ($max_key5 >= $max_key2) && ($max_key5 >= $max_key3) && ($max_key5 >= $max_key4) && ($max_key5 >= $max_key6) && ($max_key5 >= $max_key7)){
                $max_key = $max_key5;
            } else if(($max_key6 >= $max_key1) && ($max_key6 >= $max_key2) && ($max_key6 >= $max_key3) && ($max_key6 >= $max_key4) && ($max_key6 >= $max_key5) && ($max_key6 >= $max_key7)){
                $max_key = $max_key6;
            } else if(($max_key7 >= $max_key1) && ($max_key7 >= $max_key2) && ($max_key7 >= $max_key3) && ($max_key7 >= $max_key4) && ($max_key7 >= $max_key5) && ($max_key7 >= $max_key6)){
                $max_key = $max_key7;
            }
            $count_row = 8 + $max_key;
        } else {
            $count_row = 4;
        }
        $range1_row = 0;
        $range2_row = 0;
        foreach ($grn_data as $key => $dat){
            $master_hz = '';
            $master_unit_type = '';
            $master_range = '';
            $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                    WHERE grn.masterMeterSubSubId = '.$dat['mRangeId'].' ';
            $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
            while($row1 = mysql_fetch_assoc($res1)) { 
                $master_hz = $row1['frequency'];
                $master_unit_type = $row1['range_unit'];
                $master_range = $row1['rangeValue'];
            }
            if($dat['table_info_id'] == '1' || $dat['table_info_id'] == '10' || $dat['table_info_id'] == '14' || $dat['table_info_id'] == '17' || $dat['table_info_id'] == '3' || $dat['table_info_id'] == '4' || $dat['table_info_id'] == '5' || $dat['table_info_id'] == '6' || $dat['table_info_id'] == '7' || $dat['table_info_id'] == '8' || $dat['table_info_id'] == '9' || $dat['table_info_id'] == '15'){
                $range1 = $master_range.' '.$master_unit_type.' @ '.$master_hz;
            } else if($dat['table_info_id'] == '11'){
                $range1 = $master_range.' '.$master_unit_type;
            }
            
            if(!empty($range1)){
                $string = str_split($range1, 8);
                $no_of_string_row = max(array_keys($string)) + 1;
                if(isset($dat['row_span'])){
                    if($no_of_string_row > $dat['row_span']){
                        $range1_row = $range1_row + ($no_of_string_row - $dat['row_span']);
                    }
                }
            }
            if($dat['table_info_id'] == '1' || $dat['table_info_id'] == '3'){
                $range2 = $dat['uut_range'];
            } else if($dat['table_info_id'] == '10' || $dat['table_info_id'] == '11' || $dat['table_info_id'] == '17' || $dat['table_info_id'] == '5' || $dat['table_info_id'] == '6' || $dat['table_info_id'] == '7' || $dat['table_info_id'] == '8' || $dat['table_info_id'] == '9'){
                $range2 = $dat['uut_range']." ".$master_unit_type;
            } else if(($dat['table_info_id'] == '14') || ($dat['table_info_id'] == '15')){
                $range2 = $dat['grn_uuc_range'];
            } else if($dat['table_info_id'] == '4'){
                $range2 = $dat['uut_range'] ." ".$master_unit_type. "/ ".$dat['uut_range_sec'] ." ".$master_unit_type;
            }
            if(!empty($range2)){
                $string = str_split($range2, 8);
                $no_of_string_row = max(array_keys($string)) + 1;
                if(isset($dat['row_span_test'])){
                    if($no_of_string_row > $dat['row_span_test']){
                        $range2_row = $range2_row + ($no_of_string_row - $dat['row_span_test']);
                    }
                }
            }
            $count_row = $count_row + 1;
//            echo "<pre>"; print_r($no_of_string_row); exit;
        }
        if($range1_row > $range2_row){
            $count_row = $count_row + $range1_row;
        } else {
            $count_row = $count_row + $range2_row;
        }
        $total_table_array[$main_key] = $count_row;
//        echo "<pre>"; print_r($grn_data); exit;
    }
    $qry1 = ' SELECT * FROM table_notes WHERE note_id IN (SELECT note_id FROM table_notes_detail WHERE table_info_id IN (SELECT table_info_id FROM observation_data WHERE grn_id = '.$_GET['grnId'].' AND grn_detail_id = '.$_GET['grnDetailId'].' AND table_info_id = "15" ) ) GROUP BY note_id ';
    $res1 = mysql_query($qry1) or die("Error :: Cannot select notes details.<hr>".mysql_error());
    $note_data = array();
    while($row1 = mysql_fetch_assoc($res1)) {
            array_push($note_data,$row1);
    }
//    $s_all_note
//    echo "<pre>"; print_r(max(array_keys($note_data))); exit;
    if(!empty($note_data)){
//        $br_row = explode("<br/>",$note_data);
        $total_footer_row = max(array_keys($note_data)) + 3;
        $total_table_array[] = $total_footer_row;
    } else {
        $total_table_array[] = 2;
    }
//    echo "<pre>"; print_r($total_table_array); exit;
    $page_array = array();
    $page = 0;
    $row_total = 0;
    foreach ($total_table_array as $t_key => $total_table){
        if(($row_total + $total_table) > $one_page_lines){
            $page = $page + 1;
            $page_array[$page][] = array($t_key, $total_table);
            $row_total = $total_table;
        } else {
            $row_total = $row_total + $total_table;
            $page_array[$page][] = array($t_key, $total_table);
        } 
    }
    $max_page_no = max(array_keys($page_array)) + 2;
//        echo "<pre>"; print_r($page_array); exit;
?>

<?php include("new_obs_print_table_all_page_1.php"); ?>
<style>
    tr {line-height: 21px;}
.main_table tr td {border: 1px solid black;}
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 1px solid black;
    
}
.main_table td, .main_table th {
    padding: 1px;
    
}
.main_table{
    padding-bottom: 2px;
    
}
</style>
<?php $max_page_key = max(array_keys($page_array)); ?>
<?php foreach ($page_array as $page_key => $page_data){ ?>
<div style="border: 1px solid black; border-radius: 10px; padding: 5px;">
<table width="605">
    <tr>
        <td align="right" width="605"><strong><?php echo $page_title; ?></strong></td>
    </tr>
</table>
<table width="605">
	<tr>
		<td width="150">CERTIFICATE NO.</td>
		<td width="145">: <?php echo $certificate_no; ?></td>
		<td width="5">&nbsp;</td>
		<td width="200">DATE OF CALIBRATION</td>
		<td width="95" align="right">: <?php echo $callibrationDate; ?></td>
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grn_no; ?></td>
		<td>&nbsp;</td>
		<td>NEXT RECOMMENDED DATE</td>
		<td align="right">: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td width="150">PAGE</td>
		<td width="145">: <?php echo $page_key + 2; ?> OF <span class="total_page_no"><?php echo $max_page_no; ?></span></td>
		<td width="5">&nbsp;</td>
		<td width="200">CERTIFICATE ISSUE DATE</td>
		<td width="95" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
	</tr>
</table>
<hr />
        <table width="605">
            <tr valign="top">
                <td width="10"><strong>10.</strong></td>
		<td width="235"><strong> Observation</strong></td>
		<td width="5"></td>
		<td width="355"></td>
            </tr>
        </table>
<?php 
    $page_wise_total_row = 0;
    $max_inpage_key = max(array_keys($page_data)); 
    foreach ($page_data as $main_key => $grn_data_key){
        $table_wise_total_row = $grn_data_key[1];
        if(($page_key == $max_page_key) && ($main_key == $max_inpage_key)){
            include("new_obs_print_table_all_footer_note.php");
        } else {
            $grn_data = $grnobsmaster_data[$grn_data_key[0]];
            $master_data = json_decode(json_encode(json_decode($grn_data['master_json'])), true);
            $uuc_data = json_decode(json_encode(json_decode($grn_data['uuc_json'])), true);
            $type_a_type_b_json = json_decode(json_encode(json_decode($grn_data['type_a_type_b_json'])), true);
            $reading_json = json_decode(json_encode(json_decode($grn_data['reading_json'])), true);
            unset($grn_data['master_json']);
            unset($grn_data['uuc_json']);
            unset($grn_data['type_a_type_b_json']);
            unset($grn_data['reading_json']);
    //        $grn_data[] = $type_a_type_b_json;
            $main_arr = array();
            foreach ($master_data as $key => $master_dat){
                $t_arr = array_merge($master_dat,$uuc_data[$key],$type_a_type_b_json[$key],$grn_data);
                $main_arr[] = $t_arr;
            }
            $grn_data = $main_arr;
            $table_info_id = $grn_data[0]['table_info_id'];
            $qry13 = 'SELECT range_unit,coverage_factor,cmc FROM mastermetersubsub
                          WHERE masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
            $res13 = mysql_query($qry13) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
            $m_cmc = '';
            while($row13 = mysql_fetch_assoc($res13)) {
                $m_cmc = $row13['cmc'];
            }
            $ex_unce_arr = array();
            $max_ex_unce = '';
            foreach ($grn_data as $k => $grn_entry){ 
                $ex_unce = isset($grn_entry['exp_uncer_per_ue']) ? $grn_entry['exp_uncer_per_ue'] : $grn_entry['expandedUncertinity'];
                if($max_ex_unce == ''){
                    $max_ex_unce = $ex_unce;
                } else {
                    if($ex_unce >= $max_ex_unce){
                        $max_ex_unce = $ex_unce;
                    }
                }
            }
            $max_ex_unce_p = 0;
            if($m_cmc > $max_ex_unce){
                $max_ex_unce_p = $m_cmc;
            } else {
                $max_ex_unce_p = $max_ex_unce;
            }
            include("new_obs_print_regular_all_".$table_info_id.".php");
        }
//        echo "<pre>"; print_r($grn_data); exit;
        ?>
	<?php $page_wise_total_row = $page_wise_total_row + $table_wise_total_row;
        
                        } ?>
<?php 
        $blank_row = $one_page_lines - $page_wise_total_row;
        if(!empty($blank_row)){
            echo '<table width="605" align="left" border="0" class=""  style="font-size:13px;">';
            for($x = 1; $x <= $blank_row; $x++){
                echo '<tr>
                        <td width="605" align="left" valign="middle">&nbsp;</td>
                      </tr>';
                        
             }
             echo '</table><br/>';
        }
?>
<table width="605">
    <tr valign="top">
        <td width="5">&nbsp;</td>
        <td width="600">&nbsp;</td>
    </tr>
        <tr valign="top">
            <hr />
            <td width="5" style="height: 5px;"><strong></strong></td>
            <td  style="height: 5px;" width="600" style="font-size: 20px;"></td>
	</tr>
        <tr valign="bottom">
            <td width="5"><strong>&nbsp;</strong></td>
            <td width="600" style="font-size: 20px;"><img src="./images/for-krishna.png" /></td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">CALIBRATED BY: (<?php echo $instrumentDetails['userName']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; APPROVED BY: (<?php echo $instrumentDetails['approved_by'].', '.$instrumentDetails['designation']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; SEAL :</td>
	</tr>
</table>
</div>
<?php } ?>


<?php
//exit;
// html - end //
$content = ob_get_clean();

// convert in PDF
require_once 'html2pdf/html2pdf.class.php';
try {
    $html2pdf = new HTML2PDF('P', 'A4', 'fr',true,'UTF-8',array(30, 43, 10, 14));
    $html2pdf->pdf->SetTitle('Certificate Print');
    $html2pdf->shrink_tables_to_fit = 1;    // Default: false
    $html2pdf->setDefaultFont('Times');
    $html2pdf->AddFont('dejavusans');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output('certificate_print.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>