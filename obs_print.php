<?php
include("include/omConfig.php");

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
	if($haystack == null){
		return false;
	}else{
 foreach($haystack as $item) {
 	      if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
}
    return false;
}
// general functions - start //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// variable list - start //
$certificateNo 			= '';
$grnNo 					= '';
$certificate_no = '';
$grn_no = '';
$grnDate				= ''; 
$totalPages 			= 0;
$callibrationDate		= '';
$callibrationDateNext 	= '';
$certificateIssue		= '';
$certificateIssueDate	= '';
$customerName			= '';
$customerAddress		= '';
$customerRefNo			= '';
$ambientTemperature		= '';
$relativeHumidity		= '';
$remarks				= '';

$extraFields			= "";
$extraField1			= "";
$extraField2			= "";
$extraField3			= "";
$extraField4			= "";
$extraField5			= "";
$extraField6			= "";

$counter 				= 0;
$grnDetailIds 			= array();
$tempArr 				= array() ;

$instrumentDetails					= array();
$instrumentDetails['item_name']		= '';
$instrumentDetails['make']			= '';
$instrumentDetails['item_code']		= '';
$instrumentDetails['id_no']			= '';
$instrumentDetails['range_value']	= '';
$instrumentDetails['least_count']	= '';
$instrumentDetails['accuracy']		= '';
$instrumentDetails['grn_condition']	= '';
$instrumentDetails['certi_remarks']	= '';
$instrumentDetails['userName']	= '';
$instrumentDetails['approved_by']	= '';
$instrumentDetails['designation']	= '';

$masterMeters						= array();
$masterMeters['meter_name']			= array();
$masterMeters['sr_no']				= array();
$masterMeters['certi_no']			= array();
$masterMeters['id_no']				= array();
$masterMeters['due_date']			= array();
$masterMeters['tracebility']		= array();
$masterMeters['meter_procedure']	= array();
$masterMeters['masterMeterNote']	= array();

$observations						= array();
/*$observations['parameters']			= array();
$observations['table_no']			= array();
$observations['range1']				= array();
$observations['reading1']			= array();
$observations['range2']				= array();
$observations['reading2']			= array();
$observations['units']				= array();
$observations['rdg']				= array();
$observations['expanded']			= array();*/
// variable list - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// fetch data - start //
$qry = 'SELECT DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate,
	DATE_FORMAT(grndetail.nextYearDate,"%d/%m/%Y") AS nextYearDate,
	certificateIssue,DATE_FORMAT(grndetail.certificateIssueDate,"%d/%m/%Y") AS certificateIssueDate,
	grndetail.grnId,grndetail.certificate_no,
	grndetail.temperature,grndetail.humidity,itemCode,
	extraFields,extraField1,extraField2,extraField3,extraField4,extraField5,extraField6,certiRemarks,
	CONCAT("N-",grnmaster.grnNo) AS grnNo,grnmaster.grn_no,
	DATE_FORMAT(grnmaster.grnDate,"%d/%m/%Y") AS grnDate,
	grnobsmaster.selfCertiNo,
	customer.custName,customer.address,customer.city,
	CONCAT(IF(customer.custRefName IS NOT NULL, customer.custRefName,""),"/cali/",customer.custCode) AS customerRef 
	FROM grndetail 
	LEFT JOIN grnmaster ON grnmaster.grnId = grndetail.grnId 
	LEFT JOIN grnobsmaster ON grnobsmaster.grnDetailId = grndetail.grnDetailId 
	LEFT JOIN customer ON customer.customerId = grnmaster.customerId 
	WHERE grndetail.grnDetailId = '.$_GET['grnDetailId'];
$res = mysql_query($qry) or die("Error :: Cannot select basic details.<hr>".mysql_error());
if($row = mysql_fetch_assoc($res)) { 
	$certificateNo 			= $row['selfCertiNo'];
	$grnNo 					= $row['grnNo'];
	$grn_no 					= $row['grn_no'];
	$certificate_no 					= $row['certificate_no'];
	$grnDate				= $row['grnDate'];
	$callibrationDate		= $row['callibrationDate'];
	$callibrationDateNext 	= $row['nextYearDate'];
	$certificateIssue		= $row['certificateIssue'];
	$certificateIssueDate	= $row['certificateIssueDate'];
	$customerName			= $row['custName'];
	$customerAddress		= $row['address'].'<br />'.$row['city'];
	$customerRefNo			= $row['customerRef'];
	$ambientTemperature		= $row['temperature'];
	$relativeHumidity		= $row['humidity'];
	$extraFields			= $row['extraFields'];
	$extraField1			= $row['extraField1'];
	$extraField2			= $row['extraField2'];
	$extraField3			= $row['extraField3'];
	$extraField4			= $row['extraField4'];
	$extraField5			= $row['extraField5'];
	$extraField6			= $row['extraField6'];
	
	$tempArr = $instrumentDetails;
	// instrument details query //
	$qry1 = 'SELECT grndetail.grnDetailId,grndetail.makeModel AS make,grndetail.itemCode AS item_code,
		grndetail.instrumentId AS id_no,grndetail.rangeValue AS range_value,grndetail.leastCount AS least_count,
		grndetail.accuracy,grndetail.grnCondition AS grn_condition,item.itemName AS item_name,
		grndetail.certiRemarks AS certi_remarks, grndetail.userName, grndetail.sr_no, approved_by.name as approved_by, approved_by.designation 
		FROM grndetail 
		LEFT JOIN item ON item.itemId = grndetail.itemId 
                LEFT JOIN approved_by ON approved_by.id = grndetail.approved_by 
		WHERE grndetail.callibrationDate IS NOT NULL AND grndetail.grnId = '.$row['grnId'].' AND grndetail.itemCode = "'.$row['itemCode'].'"';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
	while($row1 = mysql_fetch_assoc($res1)) { 
		array_push($grnDetailIds,$row1['grnDetailId']);
		foreach($tempArr as $key=>$val) {
			if(!in_array_r($row1[$key],$tempArr[$key])) {
				$tempArr[$key][$counter] = $row1[$key];
				$instrumentDetails[$key] .= $row1[$key].' | ';
			}
		}
		$counter++;
	}
	
	foreach($instrumentDetails as $key=>$val) {
		if($val != '')
			$instrumentDetails[$key] = substr($val,0,-3);
	}
	mysql_free_result($res1);
	
	$tempArr = $masterMeters;
	// master meters query //
	$qry2 = 'SELECT masterMeterId,CONCAT(masterMeterName," (",masterMeterModelNo,")") AS meter_name,
		masterMeterSerialNo AS sr_no,masterMeterCertificateNo AS certi_no,
		masterMeterIdNo AS id_no,DATE_FORMAT(masterMeterExp,"%d/%m/%Y") AS due_date,
		masterMeterTraceabilityTo AS tracebility,procedureText AS meter_procedure,TRIM(masterUncertaintyText) AS masterMeterNote 
		FROM mastermeter 
		WHERE masterMeterId IN (SELECT masterMeterId 
			FROM grnobsmaster 
			WHERE grnDetailId IN ('.implode(",",$grnDetailIds).'))'; 
	// echo "DEBUG QUERY: " . $qry2;
	$res2 = mysql_query($qry2) or die("Error :: Cannot select master meter details1.<hr>".mysql_error());
	while($row2 = mysql_fetch_assoc($res2)) { 
		foreach($tempArr as $key=>$val) { 
			array_push($masterMeters[$key],$row2[$key]);
		}
	}
        $qry21 = 'SELECT GROUP_CONCAT(DISTINCT procedureText) as procedureText, GROUP_CONCAT(DISTINCT page_title) as page_title
                 FROM mastermeter 
		 WHERE masterMeterId IN (SELECT masterMeterId 
                    FROM grnobsmaster 
                    WHERE grnDetailId IN ('.implode(",",$grnDetailIds).'))'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res21 = mysql_query($qry21) or die("Error :: Cannot select master meter details2.<hr>".mysql_error());
	$master_procedureText = '';
	$page_title = '';
        while($row21 = mysql_fetch_assoc($res21)) { 
            $master_procedureText = $row21['procedureText'];
            $page_title = $row21['page_title'];
	}
//print '<pre>'; print_r($masterMeters);
	mysql_free_result($res2);
	$masterMeters = array_map("unserialize", array_unique(array_map("serialize", array_filter($masterMeters))));
}
mysql_free_result($res);
$qry112 = 'SELECT * FROM setting ';
$res112 = mysql_query($qry112) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
$s_trac = '';
$s_approve = '';
$s_note = '';
$s_all_note = '';
while($row112 = mysql_fetch_assoc($res112)) { 
    if($row112['setting_name'] == 'traceability_to'){
        $s_trac = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'approved_by'){
        $s_approve = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'footer_note_for_page_1'){
        $s_note = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'footer_note_for_all'){
        $s_all_note = $row112['setting_value'];
    }
}
// fetch data - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// html - start //
ob_start();
?>


<style>
table {font-family: "Times New Roman"; font-size: 14px;}
tr {line-height: 21px;}
.main_table tr td {border: 1px solid black;}
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 1px solid black;
    
}
.main_table td, .main_table th {
    padding: 3px;
    
}
</style>

<div style="text-align:right; padding-right: 25px;"><b><?php echo $page_title; ?></b></div>

<div style="border: 1px solid black; border-radius: 10px; padding: 5px;">
<table width="605">
	<tr>
		<td width="150">CERTIFICATE NO.</td>
		<td width="145">: <?php echo $certificate_no; ?></td>
		<td width="5">&nbsp;</td>
		<td width="200">DATE OF CALIBRATION</td>
		<td width="95" align="right">: <?php echo $callibrationDate; ?></td>
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grn_no; ?></td>
		<td>&nbsp;</td>
		<td>NEXT RECOMMENDED DATE</td>
		<td align="right">: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td width="150">PAGE</td>
		<td width="145">: 2 OF <span class="total_page_no">2</span></td>
		<td width="5">&nbsp;</td>
		<td width="200">CERTIFICATE ISSUE DATE</td>
		<td width="95" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
	</tr>
</table>
<hr />
	<?php
		if(isset($_GET['grnDetailId']) && $_GET['grnDetailId'] != '') {
	?>
        <br/>
        <?php
        $qry1 = 'SELECT * FROM grnobsmaster as grn
                LEFT JOIN grnobservation as ob ON ob.grnObsMasterId = grn.grnObsMasterId
		WHERE grn.grnId = '.$_GET['grnId'].' AND grn.grnDetailId = '.$_GET['grnDetailId'].'  ORDER BY ob.table_no,grn.mRangeId,grn.grnObsMasterId ASC ';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $grnobsmaster_data = array();
        while($row1 = mysql_fetch_assoc($res1)) { 
                if(!empty($grnobsmaster_data)){
                    $max_key = max(array_keys($grnobsmaster_data));
                    $is_insert = 0;
                    foreach ($grnobsmaster_data as $grnobsmaster_key => $grnobsmaster){
                        foreach ($grnobsmaster as $grn_line_key => $grn_line){
                            if($row1['table_no'] == $grn_line['table_no']){
                                array_push($grnobsmaster_data[$grnobsmaster_key],$row1);
                                $is_insert = 1;
                                break;
                            }
                        }
                    }
                    if($is_insert == 0){
                        array_push($grnobsmaster_data,array($row1));
                    }
                } else {
                    array_push($grnobsmaster_data,array($row1));
                }
		
        }
//        $qry12 = 'SELECT table_no, COUNT(mRangeId) as total_range FROM grnobsmaster as grn
//                LEFT JOIN grnobservation as ob ON ob.grnObsMasterId = grn.grnObsMasterId
//		WHERE grn.grnId = '.$_GET['grnId'].' AND grn.grnDetailId = '.$_GET['grnDetailId'].' GROUP BY grn.mRangeId,table_no ORDER BY grn.mRangeId ASC ';
//	$res12 = mysql_query($qry12) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
//        $range_data = array();
//        while($row12 = mysql_fetch_assoc($res12)) { 
//            $range_data[] = $row12;
//        }
//        echo "<pre>"; print_r($range_data); exit;
        ?>
        
        <table width="605">
            <tr valign="top">
                <td width="10"><strong>10.</strong></td>
		<td width="235"><strong> Observation</strong></td>
		<td width="5"></td>
		<td width="355"></td>
            </tr>
        </table>
        <?php foreach ($grnobsmaster_data as $main_key => $grn_data){ ?>
        <table width="605" align="left" class="main_table"  style="font-size:14px;">
                <?php
                $is_3_phase = 0;
                foreach ($grn_data as $k => $grn_entry){ 
                    if($grn_entry['phase_type_id'] == 2){
                        $is_3_phase = 1;
                        break;
                    }
                }
                $table_title = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(!empty($grn_entry['table_title'])){
                        $table_title = $grn_entry['table_title'];
                        break;
                    }
                }
                $pre_range = '';
                $pre_range_key = '';
                $pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(empty($pre_range)){
                        $pre_range = $grn_entry['mRangeId'];
                        $pre_range_key = $k;
                        $pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span'] = 1;
                        }
                    } else {
                        if($pre_range == $grn_entry['mRangeId']){
                            $pre_range_total = $pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        } else {
                            $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                $t_pre_range = '';
                $t_pre_range_key = '';
                $t_pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if($k == 0){
                        
                        $t_pre_range = $grn_entry['uut_range'];
                        $t_pre_range_key = $k;
                        $t_pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span_test'] = 1;
                        }
                    } else {
                        if($t_pre_range == $grn_entry['uut_range']){
                            $t_pre_range_total = $t_pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        } else {
                            $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            $t_pre_range = $grn_entry['uut_range'];
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                ?>
                <tr>
                    <th>Title:</th>
                    <th  <?= !empty($is_3_phase) ? 'colspan="7"' : 'colspan="6"'; ?> align="left">&nbsp; <?php echo $table_title; ?></th>
                </tr>
                <tr>
                    <th valign="middle" rowspan="2">Sr. No.</th>
                    <th colspan="2" align="center">Calibration Standard</th>
                    <th align="center" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                    <th align="center" colspan="2">Error</th>
                </tr>
                <?php 
                    $master_unit_type1 = '';
                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                            WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    while($row1 = mysql_fetch_assoc($res1)) { 
                        $master_unit_type1 = $row1['range_unit'];
                    }
                    
                    ?>
                <tr>
                    <th align="center" width="80px" valign="middle">Range</th>
                    <th align="center" valign="middle">Reading in <br/><?php echo $master_unit_type1; ?></th>
                    <th align="center" valign="middle" width="80px">Range</th>
                    <th align="center" valign="middle">Reading 1 <?php echo htmlentities('Ø') ?> <br/>in <?php echo $master_unit_type1; ?></th>
                    <?php if(!empty($is_3_phase)){ ?>
                    <th align="center" valign="middle">Reading 3 <?php echo htmlentities('Ø') ?> <br/>in <?php echo $master_unit_type1; ?></th>
                    <?php } ?>
                    <th align="center" valign="middle">Unit(+/-)</th>
                    <th align="center" valign="middle">% of Rdg.</th>
                </tr>
                <?php 
                    $total_test_meter_val = 0;
                    $total_std_meter_val = 0;
                    
                    foreach ($grn_data as $k => $grn_entry){ 
                    $i = $k + 1;
                    
                    $master_hz = '';
                    $master_unit_type = '';
                    $master_range = '';
                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                            WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    while($row1 = mysql_fetch_assoc($res1)) { 
                        $master_hz = $row1['frequency'];
                        $master_unit_type = $row1['range_unit'];
                        $master_range = $row1['rangeValue'];
                    }
                    
                    ?>
                    <tr>
                        <td align="center"><?php echo $i; ?></td>
                        <?php if(isset($grn_entry['row_span'])){ ?>
                        <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $master_range.' '.$master_unit_type.' @ '.$master_hz ?></div></td>
                        <?php } ?>
                        <?php 
                            $qry1 = 'SELECT SUM(testMeter) as testMeter, COUNT(testMeterId) as row_total  FROM grnobservation_testMeter as grn
                                   WHERE grn.grnObservationId = '.$grn_entry['grnObservationId'].' ';
                           $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                           $test_meter_val = 0;
                           while($row1 = mysql_fetch_assoc($res1)) { 
                               $test_meter_val = $row1['testMeter'] / $row1['row_total'];
                           }
                            $qry1 = 'SELECT SUM(stdMeter) as stdMeter, COUNT(stdMeterId) as row_total  FROM grnobservation_stdmeter as grn
                                   WHERE grn.grnObservationId = '.$grn_entry['grnObservationId'].' ';
                           $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                           $std_meter_val = 0;
                           while($row1 = mysql_fetch_assoc($res1)) { 
                               $std_meter_val = $row1['stdMeter'] / $row1['row_total'];
                           }
                           $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                           $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                           $decimal_master = 4;
                           $resolution_of_master = $grn_entry['resolution_of_master'];
                           if(!empty($resolution_of_master)){
                               $exp_val = explode('.', $resolution_of_master);
                               if(isset($exp_val[1])){
                                   $decimal_master = strlen($exp_val[1]);  
                               } else {
                                 $decimal_master = 0;  
                               }
                           } else {
                               $decimal_master = 4;
                           }
                           $decimal_least = 4;
                           $resolution_of_least = $grn_entry['leastCount'];
                           if(!empty($resolution_of_least)){
                               $exp_least = explode('.', $resolution_of_least);
                               if(isset($exp_least[1])){
                                   $decimal_least = strlen($exp_least[1]);  
                               } else {
                                 $decimal_least = 0;  
                               }
                           } else {
                               $decimal_least = 4;
                           }
                        ?>
                        <td align="right"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                        <?php if(isset($grn_entry['row_span_test'])){ ?>
                        <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $grn_entry['uut_range'] ?></div></td>
                        <?php } ?>
                        <td align="right"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                        <?php if(!empty($is_3_phase)){ ?>
                            <td align="right"><?php echo number_format((float)$std_meter_val * 1.732, $decimal_least, '.', ''); ?></td>
                        <?php } ?>
                        <td align="right"><?php echo number_format((float)$std_meter_val - $test_meter_val , $decimal_master, '.', ''); ?></td>
                        <?php $per = 100 * ($std_meter_val - $test_meter_val) / $test_meter_val;  ?>
                        <td align="right"><?php echo number_format((float)$per, $decimal_master, '.', ''); ?></td>
                    </tr>
                <?php } ?>
                    <tr>
                        <td><b>&nbsp;</b></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <?php if(!empty($is_3_phase)){ ?>
                            <td>&nbsp;</td>
                        <?php } ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
            </table>
            <br/>
            <br/>
	<?php } ?>
	<?php
		}
	?>
<table width="605">
    <tr valign="top">
        <td width="5">&nbsp;</td>
        <td width="600">&nbsp;</td>
    </tr>
        <tr valign="top">
            <hr />
            <td width="5" style="height: 5px;"><strong></strong></td>
            <td  style="height: 5px;" width="600" style="font-size: 20px;"></td>
	</tr>
        <tr valign="bottom">
            <td width="5"><strong>&nbsp;</strong></td>
            <td  width="600" style="font-size: 20px;"><strong>For, Krishna Instruments</strong></td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">CALIBRATED BY: (<?php echo $instrumentDetails['userName']; ?>) & APPROVED BY: (<?php echo $instrumentDetails['approved_by'].', '.$instrumentDetails['designation']; ?>) SEAL :</td>
	</tr>
        <tr valign="top">
		<td width="5"><strong>Note&nbsp;:</strong></td>
                <td>&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2" style="font-size: 12px;"><?php echo $s_all_note; ?></td>
	</tr>
</table>
</div>



<?php
// html - end //
//exit;
$content = ob_get_clean();

// convert in PDF
require_once 'html2pdf/html2pdf.class.php';
try {
	$html2pdf = new HTML2PDF('P', 'A4', 'fr');
	// $html2pdf->setModeDebug();
	$html2pdf->setDefaultFont('Times');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output('example.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>