<table width="605" align="left" class="main_table"  style="font-size:13px;">
                <?php
                $is_3_phase = 0;
                $table_title = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(!empty($grn_entry['table_title'])){
                        $table_title = $grn_entry['table_title'];
                        break;
                    }
                }
                $table_title_right = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                        $table_title_right = $grn_entry['table_title_right'];
                        break;
                    }
                }
                $ocillation = '';
                $frequency = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['ocillation']) && !empty($grn_entry['ocillation'])){
                        $ocillation = $grn_entry['ocillation'];
                        $frequency = $grn_entry['frequency'];
                        break;
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                $col_span1 = '4';
                if(!empty($view_uncertainity)){
                    $col_span1 = $col_span1 + 1;
                }
                ?>
                <tr>
                    <th align="center" valign="middle">Title:</th>
                    <th  colspan="<?= $col_span1; ?>" align="left" valign="middle" style="font-family:dejavusansb; font-size: 11px;">&nbsp; <?php echo $table_title_right; ?></th>
                    <!--<th  colspan="2" align="left" valign="middle">Ocillation :<?php echo $ocillation; ?></th>-->
                    <th  colspan="4" valign="middle" align="center">Frequency : <?php echo $frequency; ?></th>
                </tr>
                <tr>
                    <th valign="middle" align="center">Sr. No.</th>
                    <th align="center" valign="middle" >Time in Sec.<br/>@ <?php echo $ocillation; ?> Ocill.</th>
                    <th align="center" valign="middle" >Calculated <br/>Slip in %</th>
                    <th align="center" valign="middle" >UUT Reading<br/>in % Slip</th>
                    <th align="center" valign="middle">Error %</th>
                    <th align="center" valign="middle">Calculated <br/>RPM</th>
                    <th align="center" valign="middle">UUT Reading <br/>in RPM</th>
                    <th align="center" valign="middle">Error %</th>
                    <?php if(!empty($view_uncertainity)){ ?>
                        <th align="center" valign="middle">Expanded <br/>Uncertainty(%)</th>
                    <?php } ?>
                </tr>
                <?php 
                    foreach ($grn_data as $k => $grn_entry){ 
                    $i = $k + 1;
                    ?>
                    <tr>
                        <td align="center" valign="middle"><?php echo $i; ?></td>
                        <?php 
                            $std_meter_val = $grn_entry['stdMeterAverage'];
                            $decimal_master = 0;
                            $resolution_of_master = $grn_entry['resolution_of_master'];
                           
                           if(!empty($resolution_of_master)){
                               $exp_val = explode('.', $resolution_of_master);
                               if(isset($exp_val[1])){
                                   $decimal_master = strlen($exp_val[1]);  
                               } else {
                                 $decimal_master = 0;  
                               }
                           } else {
                               $decimal_master = 0;
                           }
                           $decimal_least = 0;
                           $resolution_of_least = $grn_entry['leastCount'];
                           if(!empty($resolution_of_least)){
                               $exp_least = explode('.', $resolution_of_least);
                               if(isset($exp_least[1])){
                                   $decimal_least = strlen($exp_least[1]);  
                               } else {
                                 $decimal_least = 0;  
                               }
                           } else {
                               $decimal_least = 0;
                           }
                           $least_count_for_rpm = 0;
                           $resolution_of_least_col = isset($grn_entry['least_count_for_rpm']) ? $grn_entry['least_count_for_rpm'] : '0';
                           if(!empty($resolution_of_least_col)){
                               $exp_least = explode('.', $resolution_of_least_col);
                               if(isset($exp_least[1])){
                                   $least_count_for_rpm = strlen($exp_least[1]);  
                               } else {
                                 $least_count_for_rpm = 0;  
                               }
                           } else {
                               $least_count_for_rpm = 0;
                           }
                        ?>
                        <td align="center" valign="middle"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                        <?php $slip = ($ocillation/($std_meter_val*$frequency)*100); ?>
                        <td align="center" valign="middle"><?php echo number_format((float)$slip, $decimal_least, '.', ''); ?></td>
                        <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['uuc_slip_reading'], $decimal_least, '.', ''); ?></td>
                        <?php $error1 = (($grn_entry['uuc_slip_reading'] - $slip)/$slip * 100); ?>
                        <td align="center" valign="middle"><?php echo number_format((float)$error1, $decimal_least, '.', ''); ?></td>
                        <?php $rpm = ($grn_entry['rpm']-(($slip/100)*$grn_entry['rpm'])); ?>
                        <td align="center" valign="middle"><?php echo number_format((float)$rpm, $least_count_for_rpm, '.', ''); ?></td>
                        <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['uuc_rpm_reading'], $least_count_for_rpm, '.', ''); ?></td>
                        <?php $error2 = (($grn_entry['uuc_rpm_reading']-$rpm)/$rpm*100); ?>
                        <td align="center" valign="middle"><?php echo number_format((float)$error2, 3, '.', ''); ?></td>
                        <?php if(!empty($view_uncertainity)){ ?>
                            <td valign="middle" align="center">
                                <?php 
                                //echo number_format((float)$max_ex_unce_p, 2, '.', ''); 
                                // echo isset($type_a_type_b_json[$k]['exp_uncer_per_ue_without_cmc']) ? sprintf("%.2f",$type_a_type_b_json[$k]['exp_uncer_per_ue_without_cmc']) : '';
                                echo isset($type_a_type_b_json[$k]['exp_uncer_per_ue']) ? sprintf("%.4f",$type_a_type_b_json[$k]['exp_uncer_per_ue']) : '';
                                ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                    <?php if(empty($view_uncertainity)){ ?>
                        <tr>
                            <td align="left" colspan="8">Result : Max Expanded Uncertainty Time (in Sec.) : <?php echo number_format((float)$max_ex_unce_p, 2, '.', ''); ?></td>
                        </tr>
                    <?php } ?>
                    
            </table>