<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(MASTER_GRN_PREFIX_MODULE_ID,"view"))){
  $msg             = "";
  $grnPrefixId     = (isset($_REQUEST['grnPrefixId'])) ? $_REQUEST['grnPrefixId'] : 0;
  $currentgrnPrefix = "";
  $grnPrefixArr         = array();

  //grnprefix Insert : Start
  if(isset($_POST['grnPrefix']))
  {
    if(isset($_POST['cancelBtn']))
    {
      header("Location: index.php"); 
      exit();
    }
    if($grnPrefixId > 0)
    {
      $updategrnPrefix = "UPDATE grnprefix
                             SET grnPrefix = '".$_POST['grnPrefix']."'
                           WHERE grnPrefixId = ".$_POST['grnPrefixId'];
      $updategrnPrefixResult = mysql_query($updategrnPrefix);
      $grnPrefixId = 0; //We don't want user to remain in Edit part after Update query done.
    }
    else
    {
      $insertgrnPrefix = "INSERT INTO grnprefix(grnPrefix)
                           VALUE('".$_POST['grnPrefix']."')";
      $insertgrnPrefixResult = mysql_query($insertgrnPrefix);

      if(!$insertgrnPrefixResult)
        die("Insert Query Not Inserted : ".mysql_error()." : ".$insertgrnPrefix);
      else
        header("Location:grnPrefix.php");
    }
  }
  //grnprefix Insert : End

  //grnprefix Listing : Start
  $grnPrefixToDisplay = "SELECT grnPrefixId,grnPrefix
                          FROM grnprefix
                         ORDER BY grnPrefix";
  $selectgrnPrefixResult = mysql_query($grnPrefixToDisplay);
  $i = 0;
  while($grnPrefixInRow = mysql_fetch_array($selectgrnPrefixResult))
  {
    $grnPrefixArr[$i]['grnPrefixId']   = $grnPrefixInRow['grnPrefixId'];
    $grnPrefixArr[$i]['grnPrefix']     = $grnPrefixInRow['grnPrefix'];

    if($grnPrefixInRow['grnPrefixId'] == $grnPrefixId)
       $currentgrnPrefix = $grnPrefixInRow['grnPrefix'];
    $i++;
  }
//grnprefix Listing : End
  include("./bottom.php");
  $smarty->assign("msg",$msg);
  $smarty->assign("grnPrefixId",$grnPrefixId);
  $smarty->assign("grnPrefixArr",$grnPrefixArr);
  $smarty->assign("currentgrnPrefix",$currentgrnPrefix);
  $smarty->display("grnPrefix.tpl");
} else {
  header("Location:index.php");
}
}
?>