<?php
include("include/omConfig.php");

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
	if($haystack == null){
		return false;
	}else{
 foreach($haystack as $item) {
 	      if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
}
    return false;
}
$view_uncertainity = 0;
if(isset($_GET['withuncer'])){
    $view_uncertainity = 1;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////

ob_start();
?>
<style>
table {font-family: "Times New Roman"; font-size: 12px;}
tr {line-height: 19px;}
.main_table tr td {border: 1px solid black;}
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 1px solid black;
    
}
.main_table td, .main_table th {
    padding: 0.5px;
    
}
.main_table{
    padding-bottom: 3px;
    
}
</style>

<?php
//SELECT * FROM `observation_data` WHERE `grn_id` = 228 ORDER BY `observation_data`.`grn_detail_id` ASC, `observation_data`.`table_info_id` ASC ,`observation_data`.`table_no` ASC

    $qry1 = 'SELECT customer.custCode,item.itemName,ob.*,grnmaster.grn_no,grnmaster.customerId,grndetail.*,DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate FROM observation_data as ob
            LEFT JOIN grnmaster ON grnmaster.grnId = ob.grn_id
            LEFT JOIN grndetail ON grndetail.grnDetailId = ob.grn_detail_id
            LEFT JOIN item ON item.itemId = grndetail.itemId
            LEFT JOIN customer ON customer.customerId = grnmaster.customerId
            WHERE ob.grn_id = '.$_GET['grnId'].' AND table_info_id IN (1,5,3,4,6,7,8,14,9,10,17,13) ORDER BY grn_detail_id ASC, table_info_id ASC, table_no ASC ';
    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
    $grnobsmaster_data = array();
    while($row1 = mysql_fetch_assoc($res1)) {
            array_push($grnobsmaster_data,$row1);
    }
//        echo "<pre>"; print_r($grnobsmaster_data); exit;
    $master_meter_arr = array();
    $max_main_key = max(array_keys($grnobsmaster_data)); 
    foreach ($grnobsmaster_data as $main_key => $grn_data){
        $master_data = json_decode(json_encode(json_decode($grn_data['master_json'])), true);
        $uuc_data = json_decode(json_encode(json_decode($grn_data['uuc_json'])), true);
        $reading_json = json_decode(json_encode(json_decode($grn_data['reading_json'])), true);
        unset($grnobsmaster_data[$main_key]['master_json']);
        unset($grnobsmaster_data[$main_key]['uuc_json']);
        unset($grnobsmaster_data[$main_key]['reading_json']);
        unset($grnobsmaster_data[$main_key]['type_a_type_b_json']);
        $grnobsmaster_data[$main_key]['master_data'] = $master_data;
//        echo "<pre>"; print_r($uuc_data); exit;
//        $grnobsmaster_data[$main_key]['uuc_data'] = $uuc_data;
        $grnobsmaster_data[$main_key]['reading_json'] = $reading_json;
        $grnobsmaster_data[$main_key]['leastCount_print'] = $uuc_data[0]['leastCount_print'];
        $grnobsmaster_data[$main_key]['leastCount'] = $uuc_data[0]['leastCount'];
        $master_meter_arr[$master_data[0]['masterMeterId']][] = $main_key;
    }
//    echo "<pre>"; print_r($grnobsmaster_data); exit;
//    echo "<pre>"; print_r($master_meter_arr); exit;
    foreach ($master_meter_arr as $master_meter_id => $obs_entry_arr){
        $qry2 = 'SELECT *,DATE_FORMAT(masterMeterExp,"%d/%m/%Y")
                 FROM mastermeter 
		 WHERE masterMeterId = '.$master_meter_id.' '; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res2 = mysql_query($qry2) or die("Error :: Cannot select master meter details1.<hr>".mysql_error());
	$master_m_data = array();
        while($row2 = mysql_fetch_assoc($res2)) { 
            $master_m_data[] = $row2;
	} 
        $grn_detail_id_wise_data = array();
        foreach ($obs_entry_arr as $obs_entry){
            if(isset($grn_detail_id_wise_data[$grnobsmaster_data[$obs_entry]['grn_detail_id']])){
                foreach($grnobsmaster_data[$obs_entry]['reading_json'] as $r_data){
                    $grn_detail_id_wise_data[$grnobsmaster_data[$obs_entry]['grn_detail_id']]['reading_json'][] = $r_data;
                }
            } else {
                $grn_detail_id_wise_data[$grnobsmaster_data[$obs_entry]['grn_detail_id']] = $grnobsmaster_data[$obs_entry];
            }
        }
        $grn_detail_id_wise_data = array_values($grn_detail_id_wise_data);
//        echo "<pre>"; print_r($grn_detail_id_wise_data); exit;
        ?>

        <table width="730" align="center" class="main_table print-friendly"  style="font-size:12px;">
        <tr>
            <td align="center" width="730"><strong>Krishna Instruments</strong></td>
        </tr>
        </table>
        <table width="730" align="center" class="main_table print-friendly"  style="font-size:12px;">
        <tr>
            <td align="center" width="168"><strong> Customer Id No. :</strong></td>
            <td align="center" width="188"><strong><?php echo $grnobsmaster_data[0]['custCode'] ?></strong></td>
            <td align="center" width="170"><strong>GRN No.:- </strong></td>
            <td align="center" width="189"><strong><?php echo $grnobsmaster_data[0]['grn_no'] ?></strong></td>
        </tr>
        </table>
        <table width="730" align="center" class="main_table print-friendly"  style="font-size:12px;">
        <tr>
            <td align="center" width="167"><strong> Master used	</strong></td>
            <td align="left" width="557"><?php echo $master_m_data[0]['masterMeterName'] ?></td>
        </tr>
        <tr>
            <td align="center" width="165"><strong> Make/Model </strong></td>
            <td align="left" width="547"><?php echo $master_m_data[0]['masterMeterMake'] .'/'. $master_m_data[0]['masterMeterModelNo'] ?></td>
        </tr>
        <tr>
            <td align="center" width="165"><strong> Sr. No.  </strong></td>
            <td align="left" width="547"><?php echo $master_m_data[0]['masterMeterSerialNo']; ?></td>
        </tr>
        <tr>
            <td align="center" width="165"><strong> Instrument I.D. No. </strong></td>
            <td align="left" width="547"><?php echo $master_m_data[0]['masterMeterIdNo']; ?></td>
        </tr>
        <tr>
            <td align="center" width="165"><strong> Work Instruction No. </strong></td>
            <td align="left" width="547"><?php // echo $master_m_data[0]['masterMeterSerialNo']; ?></td>
        </tr>
        </table>
        
        <table width="730" align="left" class="main_table print-friendly"  style="font-size:12px;">
            <tr>
                <td align="center" width="170"> Instrument Name </td>
                <td align="left" width="189"><?php echo isset($grn_detail_id_wise_data[0]) ? $grn_detail_id_wise_data[0]['itemName'] : ''; ?></td>
                <td align="center" width="166"> Instrument Name </td>
                <td align="left" width="189"><?php echo isset($grn_detail_id_wise_data[1]) ? $grn_detail_id_wise_data[1]['itemName'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Make/Model </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[0]) ? $grn_detail_id_wise_data[0]['makeModel'] : ''; ?></td>
                <td align="center" width="150"> Make/Model </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[1]) ? $grn_detail_id_wise_data[1]['makeModel'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Sr. No.	</td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[0]) ? $grn_detail_id_wise_data[0]['sr_no'] : ''; ?></td>
                <td align="center" width="150"> Sr. No.	</td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[1]) ? $grn_detail_id_wise_data[1]['sr_no'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Instrument I.D. No. </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[0]) ? $grn_detail_id_wise_data[0]['instrumentId'] : ''; ?></td>
                <td align="center" width="150"> Instrument I.D. No. </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[1]) ? $grn_detail_id_wise_data[1]['instrumentId'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Range </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[0]) ? $grn_detail_id_wise_data[0]['rangeValue'] : ''; ?></td>
                <td align="center" width="150"> Range </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[1]) ? $grn_detail_id_wise_data[1]['rangeValue'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Least Count </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[0]) ? $grn_detail_id_wise_data[0]['leastCount'] : ''; ?></td>
                <td align="center" width="150"> Least Count </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[1]) ? $grn_detail_id_wise_data[1]['leastCount'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Accuracy </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[0]) ? $grn_detail_id_wise_data[0]['accuracy'] : ''; ?></td>
                <td align="center" width="150"> Accuracy </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[1]) ? $grn_detail_id_wise_data[1]['accuracy'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> I. R. Tester Voltage </td>
                <td align="left" width="180"><?php echo ''; ?></td>
                <td align="center" width="150"> I. R. Tester Voltage </td>
                <td align="left" width="180"><?php echo ''; ?></td>
            </tr>
        </table>
        <table width="730" align="left" class="main_table print-friendly"  style="font-size:12px;">
            <tr>
                <td align="center" width="365"> <strong>OBSERVATION  </strong></td>
                <td align="center" width="360"> <strong>OBSERVATION  </strong></td>
            </tr>
        </table>
        <table width="730" align="left" class="main_table print-friendly"  style="font-size:12px;">
            <tr>
                <td align="center" width="85" > Std. Meter <br/> value </td>
                <td align="center" width="85">Test Meter <br/> value</td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="85"> Std. Meter <br/> value</td>
                <td align="center" width="85"> Test Meter <br/> value</td>
                <td align="center" width="83" > Std. Meter <br/> value</td>
                <td align="center" width="83">Test Meter <br/> value</td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="84"> Std. Meter <br/> value</td>
                <td align="center" width="84"> Test Meter <br/> value</td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][0]['testMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][0]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][0]['stdMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][0]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][1]['testMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][1]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][1]['stdMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][1]['stdMeter'][0] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][0]['testMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][0]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][0]['stdMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][0]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][1]['testMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][1]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][1]['stdMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][1]['stdMeter'][0] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][0]['testMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][0]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][0]['stdMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][0]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][1]['testMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][1]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][1]['stdMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][1]['stdMeter'][1] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][0]['testMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][0]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][0]['stdMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][0]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][1]['testMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][1]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][1]['stdMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][1]['stdMeter'][1] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][0]['testMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][0]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][0]['stdMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][0]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][1]['testMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][1]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][1]['stdMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][1]['stdMeter'][2] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][0]['testMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][0]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][0]['stdMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][0]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][1]['testMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][1]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][1]['stdMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][1]['stdMeter'][2] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][0]['testMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][0]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][0]['stdMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][0]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][1]['testMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][1]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][1]['stdMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][1]['stdMeter'][3] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][0]['testMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][0]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][0]['stdMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][0]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][1]['testMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][1]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][1]['stdMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][1]['stdMeter'][3] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][0]['testMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][0]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][0]['stdMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][0]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][1]['testMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][1]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][1]['stdMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][1]['stdMeter'][4] : ''; ?></td>
                
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][0]['testMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][0]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][0]['stdMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][0]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][1]['testMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][1]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][1]['stdMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][1]['stdMeter'][4] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][2]['testMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][2]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][2]['stdMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][2]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5" >&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][3]['testMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][3]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][3]['stdMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][3]['stdMeter'][0] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][2]['testMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][2]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][2]['stdMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][2]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][3]['testMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][3]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][3]['stdMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][3]['stdMeter'][0] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][2]['testMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][2]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][2]['stdMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][2]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][3]['testMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][3]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][3]['stdMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][3]['stdMeter'][1] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][2]['testMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][2]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][2]['stdMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][2]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][3]['testMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][3]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][3]['stdMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][3]['stdMeter'][1] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][2]['testMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][2]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][2]['stdMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][2]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][3]['testMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][3]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][3]['stdMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][3]['stdMeter'][2] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][2]['testMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][2]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][2]['stdMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][2]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][3]['testMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][3]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][3]['stdMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][3]['stdMeter'][2] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][2]['testMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][2]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][2]['stdMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][2]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][3]['testMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][3]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][3]['stdMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][3]['stdMeter'][3] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][2]['testMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][2]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][2]['stdMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][2]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][3]['testMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][3]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][3]['stdMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][3]['stdMeter'][3] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][2]['testMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][2]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][2]['stdMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][2]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][3]['testMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][3]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][3]['stdMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][3]['stdMeter'][4] : ''; ?></td>
                
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][2]['testMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][2]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][2]['stdMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][2]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][3]['testMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][3]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][3]['stdMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][3]['stdMeter'][4] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][4]['testMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][4]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][4]['stdMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][4]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][5]['testMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][5]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][5]['stdMeter'][0]) ? $grn_detail_id_wise_data[0]['reading_json'][5]['stdMeter'][0] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][4]['testMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][4]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][4]['stdMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][4]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][5]['testMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][5]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][5]['stdMeter'][0]) ? $grn_detail_id_wise_data[1]['reading_json'][5]['stdMeter'][0] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][4]['testMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][4]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][4]['stdMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][4]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][5]['testMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][5]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][5]['stdMeter'][1]) ? $grn_detail_id_wise_data[0]['reading_json'][5]['stdMeter'][1] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][4]['testMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][4]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][4]['stdMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][4]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][5]['testMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][5]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][5]['stdMeter'][1]) ? $grn_detail_id_wise_data[1]['reading_json'][5]['stdMeter'][1] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][4]['testMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][4]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][4]['stdMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][4]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][5]['testMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][5]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][5]['stdMeter'][2]) ? $grn_detail_id_wise_data[0]['reading_json'][5]['stdMeter'][2] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][4]['testMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][4]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][4]['stdMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][4]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][5]['testMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][5]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][5]['stdMeter'][2]) ? $grn_detail_id_wise_data[1]['reading_json'][5]['stdMeter'][2] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][4]['testMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][4]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][4]['stdMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][4]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][5]['testMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][5]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][5]['stdMeter'][3]) ? $grn_detail_id_wise_data[0]['reading_json'][5]['stdMeter'][3] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][4]['testMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][4]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][4]['stdMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][4]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][5]['testMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][5]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][5]['stdMeter'][3]) ? $grn_detail_id_wise_data[1]['reading_json'][5]['stdMeter'][3] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][4]['testMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][4]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][4]['stdMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][4]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][5]['testMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][5]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[0]['reading_json'][5]['stdMeter'][4]) ? $grn_detail_id_wise_data[0]['reading_json'][5]['stdMeter'][4] : ''; ?></td>
                
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][4]['testMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][4]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][4]['stdMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][4]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][5]['testMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][5]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[1]['reading_json'][5]['stdMeter'][4]) ? $grn_detail_id_wise_data[1]['reading_json'][5]['stdMeter'][4] : ''; ?></td>
            </tr>
        </table>
        <table width="730" align="left" class="main_table print-friendly"  style="font-size:12px;">
            <tr>
                <td align="center" width="170"> Instrument Name </td>
                <td align="left" width="189"><?php echo isset($grn_detail_id_wise_data[3]) ? $grn_detail_id_wise_data[3]['itemName'] : ''; ?></td>
                <td align="center" width="166"> Instrument Name </td>
                <td align="left" width="189"><?php echo isset($grn_detail_id_wise_data[4]) ? $grn_detail_id_wise_data[4]['itemName'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Make/Model </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[3]) ? $grn_detail_id_wise_data[3]['makeModel'] : ''; ?></td>
                <td align="center" width="150"> Make/Model </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[4]) ? $grn_detail_id_wise_data[4]['makeModel'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Sr. No.	</td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[3]) ? $grn_detail_id_wise_data[3]['sr_no'] : ''; ?></td>
                <td align="center" width="150"> Sr. No.	</td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[4]) ? $grn_detail_id_wise_data[4]['sr_no'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Instrument I.D. No. </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[3]) ? $grn_detail_id_wise_data[3]['instrumentId'] : ''; ?></td>
                <td align="center" width="150"> Instrument I.D. No. </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[4]) ? $grn_detail_id_wise_data[4]['instrumentId'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Range </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[3]) ? $grn_detail_id_wise_data[3]['rangeValue'] : ''; ?></td>
                <td align="center" width="150"> Range </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[4]) ? $grn_detail_id_wise_data[4]['rangeValue'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Least Count </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[3]) ? $grn_detail_id_wise_data[3]['leastCount'] : ''; ?></td>
                <td align="center" width="150"> Least Count </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[4]) ? $grn_detail_id_wise_data[4]['leastCount'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> Accuracy </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[3]) ? $grn_detail_id_wise_data[3]['accuracy'] : ''; ?></td>
                <td align="center" width="150"> Accuracy </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[4]) ? $grn_detail_id_wise_data[4]['accuracy'] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="165"> I. R. Tester Voltage </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[3]) ? $grn_detail_id_wise_data[3]['itemName'] : ''; ?></td>
                <td align="center" width="150"> I. R. Tester Voltage </td>
                <td align="left" width="180"><?php echo isset($grn_detail_id_wise_data[4]) ? $grn_detail_id_wise_data[4]['itemName'] : ''; ?></td>
            </tr>
        </table>
        <table width="730" align="left" class="main_table print-friendly"  style="font-size:12px;">
            <tr>
                <td align="center" width="365"> <strong>OBSERVATION  </strong></td>
                <td align="center" width="360"> <strong>OBSERVATION  </strong></td>
            </tr>
        </table>
        <table width="730" align="left" class="main_table print-friendly"  style="font-size:12px;">
            <tr>
                <td align="center" width="85" > Std. Meter <br/> value </td>
                <td align="center" width="85">Test Meter <br/> value</td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="85"> Std. Meter <br/> value</td>
                <td align="center" width="85"> Test Meter <br/> value</td>
                <td align="center" width="83" > Std. Meter <br/> value</td>
                <td align="center" width="83">Test Meter <br/> value</td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="84"> Std. Meter <br/> value</td>
                <td align="center" width="84"> Test Meter <br/> value</td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][0]['testMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][0]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][0]['stdMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][0]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][1]['testMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][1]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][1]['stdMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][1]['stdMeter'][0] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][0]['testMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][0]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][0]['stdMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][0]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][1]['testMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][1]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][1]['stdMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][1]['stdMeter'][0] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][0]['testMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][0]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][0]['stdMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][0]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][1]['testMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][1]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][1]['stdMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][1]['stdMeter'][1] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][0]['testMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][0]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][0]['stdMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][0]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][1]['testMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][1]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][1]['stdMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][1]['stdMeter'][1] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][0]['testMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][0]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][0]['stdMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][0]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][1]['testMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][1]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][1]['stdMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][1]['stdMeter'][2] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][0]['testMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][0]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][0]['stdMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][0]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][1]['testMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][1]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][1]['stdMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][1]['stdMeter'][2] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][0]['testMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][0]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][0]['stdMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][0]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][1]['testMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][1]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][1]['stdMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][1]['stdMeter'][3] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][0]['testMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][0]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][0]['stdMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][0]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][1]['testMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][1]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][1]['stdMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][1]['stdMeter'][3] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][0]['testMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][0]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][0]['stdMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][0]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][1]['testMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][1]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][1]['stdMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][1]['stdMeter'][4] : ''; ?></td>
                
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][0]['testMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][0]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][0]['stdMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][0]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][1]['testMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][1]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][1]['stdMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][1]['stdMeter'][4] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][2]['testMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][2]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][2]['stdMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][2]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5" >&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][3]['testMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][3]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][3]['stdMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][3]['stdMeter'][0] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][2]['testMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][2]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][2]['stdMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][2]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][3]['testMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][3]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][3]['stdMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][3]['stdMeter'][0] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][2]['testMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][2]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][2]['stdMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][2]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][3]['testMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][3]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][3]['stdMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][3]['stdMeter'][1] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][2]['testMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][2]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][2]['stdMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][2]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][3]['testMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][3]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][3]['stdMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][3]['stdMeter'][1] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][2]['testMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][2]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][2]['stdMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][2]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][3]['testMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][3]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][3]['stdMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][3]['stdMeter'][2] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][2]['testMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][2]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][2]['stdMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][2]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][3]['testMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][3]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][3]['stdMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][3]['stdMeter'][2] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][2]['testMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][2]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][2]['stdMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][2]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][3]['testMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][3]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][3]['stdMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][3]['stdMeter'][3] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][2]['testMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][2]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][2]['stdMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][2]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][3]['testMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][3]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][3]['stdMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][3]['stdMeter'][3] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][2]['testMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][2]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][2]['stdMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][2]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][3]['testMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][3]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][3]['stdMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][3]['stdMeter'][4] : ''; ?></td>
                
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][2]['testMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][2]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][2]['stdMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][2]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][3]['testMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][3]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][3]['stdMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][3]['stdMeter'][4] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][4]['testMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][4]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][4]['stdMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][4]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][5]['testMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][5]['testMeter'][0] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][5]['stdMeter'][0]) ? $grn_detail_id_wise_data[2]['reading_json'][5]['stdMeter'][0] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][4]['testMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][4]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][4]['stdMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][4]['stdMeter'][0] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][5]['testMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][5]['testMeter'][0] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][5]['stdMeter'][0]) ? $grn_detail_id_wise_data[3]['reading_json'][5]['stdMeter'][0] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][4]['testMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][4]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][4]['stdMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][4]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][5]['testMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][5]['testMeter'][1] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][5]['stdMeter'][1]) ? $grn_detail_id_wise_data[2]['reading_json'][5]['stdMeter'][1] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][4]['testMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][4]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][4]['stdMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][4]['stdMeter'][1] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][5]['testMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][5]['testMeter'][1] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][5]['stdMeter'][1]) ? $grn_detail_id_wise_data[3]['reading_json'][5]['stdMeter'][1] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][4]['testMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][4]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][4]['stdMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][4]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][5]['testMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][5]['testMeter'][2] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][5]['stdMeter'][2]) ? $grn_detail_id_wise_data[2]['reading_json'][5]['stdMeter'][2] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][4]['testMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][4]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][4]['stdMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][4]['stdMeter'][2] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][5]['testMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][5]['testMeter'][2] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][5]['stdMeter'][2]) ? $grn_detail_id_wise_data[3]['reading_json'][5]['stdMeter'][2] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][4]['testMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][4]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][4]['stdMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][4]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][5]['testMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][5]['testMeter'][3] : ''; ?></td>
                <td align="center" width="71"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][5]['stdMeter'][3]) ? $grn_detail_id_wise_data[2]['reading_json'][5]['stdMeter'][3] : ''; ?></td>
                
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][4]['testMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][4]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][4]['stdMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][4]['stdMeter'][3] : ''; ?></td>
                <td align="center" width="5">&nbsp;</td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][5]['testMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][5]['testMeter'][3] : ''; ?></td>
                <td align="center" width="67"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][5]['stdMeter'][3]) ? $grn_detail_id_wise_data[3]['reading_json'][5]['stdMeter'][3] : ''; ?></td>
            </tr>
            <tr>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][4]['testMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][4]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][4]['stdMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][4]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][5]['testMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][5]['testMeter'][4] : ''; ?></td>
                <td align="center" width="71" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[2]['reading_json'][5]['stdMeter'][4]) ? $grn_detail_id_wise_data[2]['reading_json'][5]['stdMeter'][4] : ''; ?></td>
                
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][4]['testMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][4]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][4]['stdMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][4]['stdMeter'][4] : ''; ?></td>
                <td align="center" width="5"  style="border-bottom: 3px solid black !important;">&nbsp;</td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][5]['testMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][5]['testMeter'][4] : ''; ?></td>
                <td align="center" width="67" style="border-bottom: 3px solid black !important;"><?php echo isset($grn_detail_id_wise_data[3]['reading_json'][5]['stdMeter'][4]) ? $grn_detail_id_wise_data[3]['reading_json'][5]['stdMeter'][4] : ''; ?></td>
            </tr>
        </table>
        <table width="730" align="left" class="main_table print-friendly"  style="font-size:12px;">
            <tr>
                <td align="left" width="320">Temperature:____________oC</td>
                <td align="center" width="90"> </td>
                <td align="right" width="310"> Calibration done By:_________________________</td>
            </tr>
            <tr>
                <td align="left" width="">Relative Humidity:_________%</td>
                <td align="center" width=""> </td>
                <td align="right" width=""> Sign & Date:_______________________________</td>
            </tr>
        </table>
        <?php
        
        
//        break;
    ?>
    <div style="page-break-after:always; clear:both"></div>  
        
    <?php } ?>
        
        
<!--<div style="width: 90%">
    <div style="width: 30%;">
        gggg
    </div>
    <div style="width: 30%;">
        yyyy
    </div>
</div>-->
        
        <?php 
//        echo "<pre>"; print_r($master_data); exit;
//    echo "<pre>"; print_r($total_table_array); exit;
//        echo "<pre>"; print_r($page_array); exit;
?>


<?php
//exit;
// html - end //
$content = ob_get_clean();

// convert in PDF
require_once 'html2pdf/html2pdf.class.php';
try {
    $html2pdf = new HTML2PDF('P', 'A4', 'fr',true,'UTF-8',array(7, 7, 7, 7));
    $html2pdf->pdf->SetTitle('Observation Sheet');
    $html2pdf->shrink_tables_to_fit = 1;    // Default: false
    $html2pdf->setDefaultFont('Times');
    $html2pdf->AddFont('dejavusans');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output('certificate_print.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>