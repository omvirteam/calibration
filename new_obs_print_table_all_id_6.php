<table width="605" align="left" class="main_table"  style="font-size:13px;">
                <?php
                $is_3_phase = 0;
                $table_title = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(!empty($grn_entry['table_title'])){
                        $table_title = $grn_entry['table_title'];
                        break;
                    }
                }
                $table_title_right = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                        $table_title_right = $grn_entry['table_title_right'];
                        break;
                    }
                }
                $pre_range = '';
                $pre_range_key = '';
                $pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(empty($pre_range)){
                        $pre_range = $grn_entry['mRangeId'];
                        $pre_range_key = $k;
                        $pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span'] = 1;
                        }
                    } else {
                        if($pre_range == $grn_entry['mRangeId']){
                            $pre_range_total = $pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        } else {
                            $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                $t_pre_range = '';
                $t_pre_range_key = '';
                $t_pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    $uuc_range_print_table = isset($grn_entry['uuc_range_print_table']) ? $grn_entry['uuc_range_print_table'] : '';
                    if($k == 0){
                        
                        $t_pre_range = $uuc_range_print_table;
                        $t_pre_range_key = $k;
                        $t_pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span_test'] = 1;
                        }
                    } else {
                        if($t_pre_range == $uuc_range_print_table){
                            $t_pre_range_total = $t_pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        } else {
                            $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            $t_pre_range = $uuc_range_print_table;
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                if(!empty($is_3_phase)){
                    $col_span1 = '9';
                } else {
                    $col_span1 = '8';
                }
                if(!empty($view_uncertainity)){
                    $col_span1 = $col_span1 + 1;
                }
                ?>
                <?php 
                    $master_unit_type1 = '';
                    $master_range1 = '';
                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                            WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    while($row1 = mysql_fetch_assoc($res1)) { 
                        $master_unit_type1 = $row1['range_unit'];
                        $master_range1 = $row1['range_disp'];
                    }
                    $uuc_range_print_table1 = isset($grn_data[0]['uuc_range_print_table']) ? $grn_data[0]['uuc_range_print_table'] : '';
                ?>
                <tr>
                    <th align="center" valign="middle">Title:</th>
                    <th  colspan="<?= $col_span1; ?>" align="left" valign="middle" style="font-family:dejavusansb; font-size: 11px;">&nbsp; <?php echo $table_title_right; ?></th>
                </tr>
                <tr>
                    <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                    <th colspan="4" align="center" valign="middle">Calibration Standard</th>
                    <th align="center" valign="middle" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                    <th align="center" colspan="2" valign="middle">Error</th>
                    <?php if(!empty($view_uncertainity)){ ?>
                        <th align="center" rowspan="2" valign="middle">Expanded <br/>Uncertainty(%)</th>
                    <?php } ?>
                </tr>
                <tr>
                    <th align="center" width="80px" valign="middle">Range</th>
                    <th align="center" valign="middle">Reading <br/>Primary</th>
                    <th align="center" valign="middle">Reading <br/>Secondary</th>
                    <th align="center" valign="middle">Calculated <br/>Ratio</th>
                    <th align="center" valign="middle" width="80px">Range</th>
                    <th align="center" valign="middle">Observed <br/>Ratio</th>
                    <?php if(!empty($is_3_phase)){ ?>
                    <th align="center" valign="middle">Reading 3 <?php echo htmlentities('Ø') ?></th>
                    <?php } ?>
                    <th align="center" valign="middle">Unit(+/-)</th>
                    <th align="center" valign="middle">% of Rdg.</th>
                </tr>
                <?php 
                    $total_test_meter_val = 0;
                    $total_std_meter_val = 0;
                    foreach ($grn_data as $k => $grn_entry){ 
                    $i = $k + 1;
                    
                    $master_hz = '';
                    $master_unit_type = '';
                    $master_range = '';
                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                            WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    while($row1 = mysql_fetch_assoc($res1)) { 
                        $master_hz = $row1['frequency'];
                        $master_unit_type = $row1['range_unit'];
                        $master_range = $row1['range_disp'];
                    }
                    
                    ?>
                    <tr>
                        <td align="center" valign="middle"><?php echo $i; ?></td>
                        <?php if(isset($grn_entry['row_span'])){ ?>
                        <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px; text-align: center; font-family:dejavusans; font-size: 11px;" ><?php echo $master_range.' '.$master_unit_type.' '.(strlen($master_hz) > 0 ? (' @ '.$master_hz) : '') ?></div></td>
                        <?php } ?>
                        <?php 
                            $test_meter_val = $grn_entry['testMeterAverage'];
                            $test_meter2_val = $grn_entry['testMeter2Average'];
                            $std_meter_val = $grn_entry['stdMeterAverage'];
                            $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                            $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                            $decimal_master = 0;
                            $resolution_of_master = $grn_entry['resolution_of_master'];
                           
                           if(!empty($resolution_of_master)){
                               $exp_val = explode('.', $resolution_of_master);
                               if(isset($exp_val[1])){
                                   $decimal_master = strlen($exp_val[1]);  
                               } else {
                                 $decimal_master = 0;  
                               }
                           } else {
                               $decimal_master = 0;
                           }
                           $decimal_least = 0;
                           $resolution_of_least = $grn_entry['leastCount'];
                           if(!empty($resolution_of_least)){
                               $exp_least = explode('.', $resolution_of_least);
                               if(isset($exp_least[1])){
                                   $decimal_least = strlen($exp_least[1]);  
                               } else {
                                 $decimal_least = 0;  
                               }
                           } else {
                               $decimal_least = 0;
                           }
                        ?>
                        <td valign="middle" align="center"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                        <td valign="middle" align="center"><?php echo number_format((float)$test_meter2_val, $decimal_master, '.', ''); ?></td>
                        <td valign="middle" align="center"><?php echo number_format((float)$test_meter_val / $test_meter2_val, $decimal_master, '.', ''); ?></td>
                        <?php if(isset($grn_entry['row_span_test'])){ ?>
                        <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px; text-align: center; font-family:dejavusans; font-size: 11px;" ><?php echo isset($grn_entry['uuc_range_print_table']) ? $grn_entry['uuc_range_print_table'] : ''; ?></div></td>
                        <?php } ?>
                        <td valign="middle" align="center"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                        <?php if(!empty($is_3_phase)){ ?>
                            <td valign="middle" align="center"><?php echo number_format((float)$std_meter_val * 1.732, $decimal_least, '.', ''); ?></td>
                        <?php } ?>
                        <?php $units = ($test_meter_val / $test_meter2_val) - $std_meter_val;  ?>
                        <td valign="middle" align="center"><?php echo number_format((float)$units, $decimal_master, '.', ''); ?></td>
                        <?php $per = 100 * $units / ($test_meter_val / $test_meter2_val);  ?>
                        <td valign="middle" align="center"><?php echo number_format((float)$per, 3, '.', ''); ?></td>
                        <?php if(!empty($view_uncertainity)){ ?>
                            <td valign="middle" align="center">
                                <?php 
                                //echo number_format((float)$max_ex_unce_p, 2, '.', ''); 
                                // echo isset($type_a_type_b_json[$k]['exp_uncer_per_ue_without_cmc']) ? sprintf("%.2f",$type_a_type_b_json[$k]['exp_uncer_per_ue_without_cmc']) : '';
                                echo isset($type_a_type_b_json[$k]['exp_uncer_per_ue']) ? sprintf("%.4f",$type_a_type_b_json[$k]['exp_uncer_per_ue']) : '';
                                ?>
                            </td>
                        <?php } ?>
                        
                    </tr>
                <?php } ?>
                    <?php if(empty($view_uncertainity)){ ?>
                        <tr>
                            <td valign="middle" align="center" colspan="3">Max Expanded Uncert. in %</td>
                            <td valign="middle" align="center"><?php echo number_format((float)$max_ex_unce_p, 2, '.', ''); ?></td>
                            <td>&nbsp;</td>
                            <?php if(!empty($is_3_phase)){ ?>
                                <td>&nbsp;</td>
                            <?php } ?>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php } ?>
            </table>