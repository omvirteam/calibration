<?php
include("include/omConfig.php");
error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
    foreach($haystack as $item) {
        if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}
// general functions - start //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// variable list - start //
$certificateNo 			= '';
$grnNo 					= '';
$grnDate				= ''; 
$totalPages 			= 0;
$callibrationDate		= '';
$callibrationDateNext 	= '';
$certificateIssue		= '';
$certificateIssueDate	= '';
$customerName			= '';
$customerAddress		= '';
$customerRefNo			= '';
$ambientTemperature		= '';
$relativeHumidity		= '';
$remarks				= '';

$extraFields			= "";
$extraField1			= "";
$extraField2			= "";
$extraField3			= "";
$extraField4			= "";
$extraField5			= "";
$extraField6			= "";

$counter 				= 0;
$grnDetailIds 			= array();
$tempArr 				= array() ;

$instrumentDetails					= array();
$instrumentDetails['item_name']		= '';
$instrumentDetails['make']			= '';
$instrumentDetails['item_code']		= '';
$instrumentDetails['id_no']			= '';
$instrumentDetails['range_value']	= '';
$instrumentDetails['least_count']	= '';
$instrumentDetails['accuracy']		= '';
$instrumentDetails['grn_condition']	= '';
$instrumentDetails['certi_remarks']	= '';

$masterMeters						= array();
$masterMeters['meter_name']			= array();
$masterMeters['sr_no']				= array();
$masterMeters['certi_no']			= array();
$masterMeters['id_no']				= array();
$masterMeters['due_date']			= array();
$masterMeters['tracebility']		= array();
$masterMeters['meter_procedure']	= array();
$masterMeters['masterMeterNote']	= array();

$observations						= array();
/*$observations['parameters']			= array();
$observations['table_no']			= array();
$observations['range1']				= array();
$observations['reading1']			= array();
$observations['range2']				= array();
$observations['reading2']			= array();
$observations['units']				= array();
$observations['rdg']				= array();
$observations['expanded']			= array();*/
// variable list - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// fetch data - start //
$qry = 'SELECT DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate,
	DATE_FORMAT(grndetail.nextYearDate,"%d/%m/%Y") AS nextYearDate,
	certificateIssue,DATE_FORMAT(grndetail.certificateIssueDate,"%d/%m/%Y") AS certificateIssueDate,
	grndetail.grnId,
	grndetail.temperature,grndetail.humidity,itemCode,
	extraFields,extraField1,extraField2,extraField3,extraField4,extraField5,extraField6,certiRemarks,
	CONCAT("N-",grnmaster.grnNo) AS grnNo,
	DATE_FORMAT(grnmaster.grnDate,"%d/%m/%Y") AS grnDate,
	grnobsmaster.selfCertiNo,
	customer.custName,customer.address,customer.city,
	CONCAT(IF(customer.custRefName IS NOT NULL, customer.custRefName,""),"/cali/",customer.custCode) AS customerRef 
	FROM grndetail 
	LEFT JOIN grnmaster ON grnmaster.grnId = grndetail.grnId 
	LEFT JOIN grnobsmaster ON grnobsmaster.grnDetailId = grndetail.grnDetailId 
	LEFT JOIN customer ON customer.customerId = grnmaster.customerId 
	WHERE grndetail.grnDetailId = '.$_GET['grnDetailId'];
$res = mysql_query($qry) or die("Error :: Cannot select basic details.<hr>".mysql_error());
if($row = mysql_fetch_assoc($res)) { 
	$certificateNo 			= $row['selfCertiNo'];
	$grnNo 					= $row['grnNo'];
	$grnDate				= $row['grnDate'];
	$callibrationDate		= $row['callibrationDate'];
	$callibrationDateNext 	= $row['nextYearDate'];;
	$certificateIssue		= $row['certificateIssue'];
	$certificateIssueDate	= $row['certificateIssueDate'];
	$customerName			= $row['custName'];
	$customerAddress		= $row['address'].'<br />'.$row['city'];
	$customerRefNo			= $row['customerRef'];
	$ambientTemperature		= $row['temperature'];
	$relativeHumidity		= $row['humidity'];
	$extraFields			= $row['extraFields'];
	$extraField1			= $row['extraField1'];
	$extraField2			= $row['extraField2'];
	$extraField3			= $row['extraField3'];
	$extraField4			= $row['extraField4'];
	$extraField5			= $row['extraField5'];
	$extraField6			= $row['extraField6'];
	
	$tempArr = $instrumentDetails;
	// instrument details query //
	$qry1 = 'SELECT grndetail.grnDetailId,grndetail.makeModel AS make,grndetail.itemCode AS item_code,
		grndetail.instrumentId AS id_no,grndetail.rangeValue AS range_value,grndetail.leastCount AS least_count,
		grndetail.accuracy,grndetail.grnCondition AS grn_condition,item.itemName AS item_name,
		grndetail.certiRemarks AS certi_remarks 
		FROM grndetail 
		LEFT JOIN item ON item.itemId = grndetail.itemId 
		WHERE grndetail.callibrationDate IS NOT NULL AND grndetail.grnId = '.$row['grnId'].' AND grndetail.itemCode = "'.$row['itemCode'].'"';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
	while($row1 = mysql_fetch_assoc($res1)) { 
		array_push($grnDetailIds,$row1['grnDetailId']);
		foreach($tempArr as $key=>$val) {
			if(!in_array_r($row1[$key],$tempArr[$key])) {
				$tempArr[$key][$counter] = $row1[$key];
				$instrumentDetails[$key] .= $row1[$key].' | ';
			}
		}
		$counter++;
	}
	
	foreach($instrumentDetails as $key=>$val) {
		if($val != '')
			$instrumentDetails[$key] = substr($val,0,-3);
	}
	mysql_free_result($res1);
	
	$tempArr = $masterMeters;
	// master meters query //
	$qry2 = 'SELECT masterMeterId,CONCAT(masterMeterName," (",masterMeterModelNo,")") AS meter_name,
		masterMeterSerialNo AS sr_no,masterMeterCertificateNo AS certi_no,
		masterMeterIdNo AS id_no,DATE_FORMAT(masterMeterExp,"%d/%m/%Y") AS due_date,
		masterMeterTraceabilityTo AS tracebility,procedureText AS meter_procedure,TRIM(masterUncertaintyText) AS masterMeterNote 
		FROM mastermeter 
		WHERE masterMeterId IN (SELECT masterMeterId 
			FROM grnobsmaster 
			WHERE grnDetailId IN ('.implode(",",$grnDetailIds).'))'; 
	// echo "DEBUG QUERY: " . $qry2;
	$res2 = mysql_query($qry2) or die("Error :: Cannot select master meter details.<hr>".mysql_error());
	while($row2 = mysql_fetch_assoc($res2)) { 
		foreach($tempArr as $key=>$val) { 
			array_push($masterMeters[$key],$row2[$key]);
		}
	}
//print '<pre>'; print_r($masterMeters);
	mysql_free_result($res2);
	$masterMeters = array_map("unserialize", array_unique(array_map("serialize", array_filter($masterMeters))));
}
mysql_free_result($res);
// fetch data - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// html - start //
ob_start();
?>

<script type="text/javascript" src="./js/jquery.min.js"></script>
<style>
div {margin-left: 25px;}
table {font-family: "Times New Roman"; font-size: 15px;}
tr {line-height: 21px;}
</style>

<div style="text-align:right"><b>QC F 05</b></div>

<div style="border: 1px solid; width: 700;">
<table width="700">
	<tr>
		<td width="195">CERTIFICATE NO.</td>
		<td width="150">: <?php echo $certificateNo; ?></td>
		<td width="5">&nbsp;</td>
		<td width="250">DATE OF CALIBRATION</td>
		<td width="100" align="right">: <?php echo $callibrationDate; ?></td>
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grnNo; ?></td>
		<td>&nbsp;</td>
		<td>NEXT RECOMMENDED DATE</td>
		<td align="right">: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td width="195">PAGE</td>
		<td width="150">: 1 OF <span class="total_page_no">2</span></td>
		<td width="5">&nbsp;</td>
		<td width="250">CERTIFICATE ISSUE DATE</td>
		<td width="100" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
	</tr>
</table>
<hr />
<table width="700">
	<tr valign="top">
		<td width="5"><strong>1.</strong></td>
		<td width="340"><strong>CUSTOMER NAME & ADDRESS</strong></td>
		<td width="5">:</td>
		<td width="350" style="font-family: Agency FB;font-size:18px; font-weight:bold;">M/s. <?php echo $customerName; ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="font-size:10px;">
			<?php echo $customerAddress; ?>
			<?php if($customerCity != "") { ?>
			<br /><b><?php echo $customerCity; ?></b>
			<?php } ?>
		</td>
	</tr>
	<tr valign="top">
		<td><strong>2.</strong></td>
		<td><strong>CUSTOMER REFERENCE NO.</strong></td>
		<td>:</td>
		<td><?php echo $customerRefNo; ?></td>
	</tr>
	<tr valign="top">
		<td><strong>3.</strong></td>
		<td><strong>INSTRUMENT RECEIVED ON</strong></td>
		<td>:</td>
		<td><?php echo $grnDate; ?></td>
	</tr>
	<tr valign="top">
		<td><strong>4.</strong></td>
		<td><strong>DESCRIPTION OF INSTRUMENT</strong></td>
		<td>:</td>
		<td>&nbsp;</td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Name</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['item_name']; ?><?php if($extraFields == 1) { ?><span style="float:right"><?php echo $extraField1; ?></span><?php } ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Make/Model</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['make']; ?><?php if($extraFields == 1) { ?><span style="float:right"><?php echo $extraField2; ?></span><?php } ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Sr. No.</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['item_code']; ?><?php if($extraFields == 1) { ?><span style="float:right"><?php echo $extraField3; ?></span><?php } ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Instrument ID No.</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['id_no']; ?><?php if($extraFields == 1) { ?><span style="float:right"><?php echo $extraField4; ?></span><?php } ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Range</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['range_value']; ?><?php if($extraFields == 1) { ?><span style="float:right"><?php echo $extraField5; ?></span><?php } ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Least Count</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['least_count']; ?><?php if($extraFields == 1) { ?><span style="float:right"><?php echo $extraField6; ?></span><?php } ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Accuracy</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['accuracy']; ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Condition on receipt</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['grn_condition']; ?></td>
	</tr>
	<tr valign="top">
		<td><strong>5.</strong></td>
		<td><strong>AMBIENT TEMPERATURE</strong></td>
		<td>:</td>
		<td><?php echo $ambientTemperature; ?> &nbsp;&nbsp;<strong>RELATIVE HUMIDITY</strong> : <?php echo $relativeHumidity; ?></td>
	</tr>
</table>	
<table width="700">	
	<tr valign="top">
		<td width="5"><strong>6.</strong></td>
		<td width="695"><strong>DETAILS OF REFERANCE STANDARD & MAJOR INSTRUMENT USED</strong></td>
	</tr>
	<tr valign="top">
		<td width="5">&nbsp;</td>
		<td width="695">
			<table width="680" cellpadding="0" cellspacing="0" border="1" style="font-size:10px;">
				<tr valign="top" style="font-weight:bold;">
					<td width="10">&nbsp;</td>
					<td width="175" align="center">Name & Make</td>
					<td width="85" align="center">Sr. No.</td>
					<td width="105" align="center">Certi. No.</td>
					<td width="85" align="center">ID No.</td>
					<td width="85" align="center">Due Date</td>
					<td width="135" align="center">Tracebility</td>
				</tr>
				<?php for($index = 0; $index < count($masterMeters['meter_name']); $index++) { ?>
				<tr valign="top">
					<td align="center"><?php echo ($index+1); ?></td>
					<td align="left"><?php echo $masterMeters['meter_name'][$index]; ?></td>
					<td align="center"><?php echo $masterMeters['sr_no'][$index]; ?></td>
					<td align="center"><?php echo $masterMeters['certi_no'][$index]; ?></td>
					<td align="center"><?php echo $masterMeters['id_no'][$index]; ?></td>
					<td align="center"><?php echo $masterMeters['due_date'][$index]; ?></td>
					<td align="center"><?php echo $masterMeters['tracebility'][$index]; ?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr valign="top">
		<td width="5">&nbsp;</td>
		<td width="695" colspan="3"><strong>Note:</strong><br />
		<?php 
		for($index = 0; $index < count($masterMeters['masterMeterNote']); $index++) { 
			if(strlen(trim($masterMeters['masterMeterNote'][$index])) > 0)
				echo '- '.$masterMeters['masterMeterNote'][$index].'<br>';
		}
		?>
		</td>
	</tr>
</table>
<table width="700">	
	<tr valign="top">
		<td width="5"><strong>7.</strong></td>
		<td width="345"><strong>PROCEDURE</strong></td>
		<td width="5">:</td>
		<td width="350"><?php
			$master_meters_procedure = "";
			foreach ($masterMeters['meter_procedure'] as $mProc) {
				# code...
				if($master_meters_procedure == "") {
					$master_meters_procedure = $mProc;
				} else {
					$arrX = explode("/", $mProc);
					$master_meters_procedure .= "/".$arrX[1];
				}
			}
		  echo $master_meters_procedure; ?></td>
	</tr>
	<tr valign="top">
		<td width="5">&nbsp;</td>
		<td width="695" colspan="3"><strong>Remarks:</strong><br />
		<?php 
		$remarks = explode(' | ', $instrumentDetails['certi_remarks']);
		for($index = 0; $index < count($remarks); $index++) { 
			if(strlen(trim($remarks[$index])) > 0)
				echo '- '.$remarks[$index].'<br>';
		}
		?>
		</td>
	</tr>
</table>
<hr />
<table width="700">	
	<tr valign="top">
		<td width="700">
			<table width="695" border="0">
				<tr>
					<td align="left" colspan="5" style="font-family: 'Exotc350 DmBd BT';font-size:20px; "><b>For Krishna Instruments</b></td>
				</tr>
				<tr>
					<td align="left" colspan="5" ><br /></td>
				</tr>
				<tr>
					<td width="225" align="left" style="font-size:x-small;">CALIBRATED BY : (Bhavesh Tank, CAL. ENG.)</td>
					<td width="70" align="left" >&nbsp;</td>
					<td width="225" align="left" style="font-size:x-small;">APPROVED BY : (D.R.SHAH, C.E.O.)</td>
					<td width="50" align="left" >&nbsp;</td>
					<td width="75" align="left" >SEAL:</td>
				</tr>
				<tr>
					<td align="left" colspan="5" style="font-size:x-small;">
      					1.This report pertains To Partuculars sample/Instrumenst submitted for test.<br>
				      	2.This certificate may not be reproduced except in full, without prior written permission of Krishna Instruments.<br>
				      	3.This Calibraiton Result Reported in the Certificate are valid at the time of measurments and under stated Conditions.
				      </td>
				</tr>

			</table>
		</td>
	</tr>
</table>
</div>
<div style="clear:both;"></div>
<br/><br/>
<style>
@media print
{
.pagebreak { page-break-after:always; }
}
</style>
<div class="pagebreak"></div>
<div style="border: 1px solid; width: 700;">
<table width="700">
	<tr>
		<td width="195">CERTIFICATE NO.</td>
		<td width="150">: <?php echo $certificateNo; ?></td>
		<td width="5">&nbsp;</td>
		<td width="250">DATE OF CALIBRATION</td>
		<td width="100" align="right">: <?php echo $callibrationDate; ?></td>
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grnNo; ?></td>
		<td>&nbsp;</td>
		<td>NEXT RECOMMENDED DATE</td>
		<td align="right">: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td width="195">PAGE</td>
		<td width="150">: 2 OF <span class="total_page_no">2</span></td>
		<td width="5">&nbsp;</td>
		<td width="250">CERTIFICATE ISSUE DATE</td>
		<td width="100" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
	</tr>
</table>
<hr />
<table width="700">	
	<tr valign="top">
		<td width="5"><strong>8.</strong></td>
		<td width="695"><strong>OBSERVATION</strong></td>
	</tr>	
	<tr>
		<td width="5">&nbsp;</td>	
		<td width="695">
		<?php
		$pageRow = 0;
		$pageNo = 3;
		$pageMod = 25;	

		$qry3 = 'SELECT grndetail.grnDetailId, grndetail.parameterId, grndetail.rangeValue, ratio1, ratio2,  
			parameterentry.parameterName, parameterentry.tableNo 
			FROM grndetail 
			LEFT JOIN parameterentry ON parameterentry.parameterId = grndetail.parameterId 
			WHERE grndetail.grnDetailId IN ('.implode(",",$grnDetailIds).')';
		$res3 = mysql_query($qry3) or die("Error :: Cannot select parameters in GRN details.<hr>".mysql_error());
		$recC = mysql_num_rows($res3);
		while($row3 = mysql_fetch_assoc($res3)) {
			$range2 = $row3['rangeValue'];
			
			if($row3['ratio2'] != 0)
				$mf	= ($row3['ratio1'] / $row3['ratio2']);
			else 
				$mf = 1;

			if($mf != 1) 
				$displayCalculated = 1;
			else
				$displayCalculated = 0;
				
			if(($row3['tableNo'] == 1 && $displayCalculated == 1) || $row3['tableNo'] == 2)
				$width = 135;
			else
				$width = 168.75;	
			
			$width1 = ($width / 2);	

			if($pageRow == $pageMod || ($pageRow+3) == $pageMod) {
				$pageRow = 0;
			?>

		</td>
	</tr>	
</table>
<table width="700">	
	<tr valign="top">
		<td width="700">
			<b>NOTE:</b><br />
			1. The reported expanded uncertainty in measurement is stated as the standard uncertainty 
			      in measurement multiplied by the coverage factor k=2, which for a normal distribution 
			      corresponds to a coverage probability of approximately 95%.
		</td>
	</tr>
</table>
<hr />
<table width="700">	
	<tr valign="top">
		<td width="700">
			<table width="695" border="0">
				<tr>
					<td align="left" colspan="5" style="font-family: 'Exotc350 DmBd BT';font-size:20px; "><b>For Krishna Instruments</b></td>
				</tr>
				<tr>
					<td align="left" colspan="5" ><br /></td>
				</tr>
				<tr>
					<td width="225" align="left" style="font-size:x-small;">CALIBRATED BY : (Bhavesh Tank, CAL. ENG.)</td>
					<td width="70" align="left" >&nbsp;</td>
					<td width="225" align="left" style="font-size:x-small;">APPROVED BY : (D.R.SHAH, C.E.O.)</td>
					<td width="50" align="left" >&nbsp;</td>
					<td width="75" align="left" >SEAL:</td>
				</tr>
			</table>
		</td>
	</tr>
</table>	
</div>
<div style="clear:both;"></div>
<br/><br/>
<style>
@media print
{
.pagebreak { page-break-after:always; }
}
</style>
<div class="pagebreak"></div>
<div style="border: 1px solid; width: 700;">
<table width="700">
	<tr>
		<td width="195">CERTIFICATE NO.</td>
		<td width="150">: <?php echo $certificateNo; ?></td>
		<td width="5">&nbsp;</td>
		<td width="250">DATE OF CALIBRATION</td>
		<td width="100" align="right">: <?php echo $callibrationDate; ?></td>
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grnNo; ?></td>
		<td>&nbsp;</td>
		<td>NEXT RECOMMENDED DATE</td>
		<td align="right">: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td width="195">PAGE</td>
		<td width="150">: <?php echo $pageNo; $pageNo++; ?> OF <span class="total_page_no">2</span></td>
		<td width="5">&nbsp;</td>
		<td width="250">CERTIFICATE ISSUE DATE</td>
		<td width="100" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
	</tr>
</table>
<hr />
<table width="700">	
	<tr>
		<td width="5">&nbsp;&nbsp;&nbsp;</td>	
		<td width="695">
<script>
$('.total_page_no').html((parseInt($('.total_page_no').html()) + 1));
</script>
			<?php
			}
			?>
			<table width="680">
				<tr>
					<td><?php echo $row3['parameterName']; $pageRow++; ?></td>
				</tr>
			</table>
			<table width="680" cellpadding="3" cellspacing="0" border="1" style="font-size:10px;">
				<tr valign="top" style="font-weight:bold;">
					<td width="5" rowspan="2" align="center">Sr. No.</td>
					<td width="<?php echo $width; ?>" colspan="<?php echo ($row3['tableNo'] == 6? '3': '2'); ?>" align="center">Callibration <?php if($row3['tableNo'] != 3){ ?>Standard<?php } ?></td>
					<?php if($row3['tableNo'] == 1 && $displayCalculated == 1) { ?>
					<td width="<?php echo $width; ?>" rowspan="2" align="center" valign="middle">Calculated Reading</td>
					<?php } else if($row3['tableNo'] == 2) { ?>
					<td width="<?php echo $width; ?>" rowspan="2" align="center" valign="middle">Calculated Reading in Volt</td>
					<?php } ?>
					<td width="<?php echo $width; ?>" colspan="2" align="center">Unit Under Test</td>
					<?php if($row3['tableNo'] == 3) { ?>
					<td width="<?php echo $width; ?>" rowspan="2" align="center" valign="middle">Secondary Calculated Reading in Amps.</td>
					<?php } ?>
					<td width="<?php echo ($row3['tableNo'] == 6? $width/2: $width); ?>" colspan="<?php echo ($row3['tableNo'] == 6? '1': '2'); ?>" align="center">Error</td>
					<td width="<?php echo ($row3['tableNo'] == 6? $width/2: $width); ?>" rowspan="2" align="center" valign="middle">Expanded Uncert. In %</td>
				</tr>
				<tr>
					<td align="center">Range</td>
					<td align="center">
					<?php 
					if($row3['tableNo'] == 2)
						echo 'Applied Voltage';
					else if($row3['tableNo'] == 3)
						echo 'Applied Ampere';	
					else if($row3['tableNo'] == 4)
						echo 'Primary Reading in Volt.';
					else if($row3['tableNo'] == 6)
						echo 'Applied <br/>Reading in &Omega;';
					else
						echo 'Reading'; 
					?>
					</td>	
					<?php if($row3['tableNo'] == 6) { ?>
					<td align="center">Values in &deg;C</td>
					<?php } ?>
					<td align="center">Range</td>
					<td align="center">
					<?php 
					if($row3['tableNo'] == 3)
						echo 'Secondary Reading in Amps.';
					else if($row3['tableNo'] == 4)
						echo 'Secondary Reading in Volt.';	
					else if($row3['tableNo'] == 6)
						echo '&deg;C';
					else
						echo 'Reading'; 
					?>
					</td>
					<td align="center">Units</td>
					<?php if($row3['tableNo'] != 6) { ?>
					<td align="center">% of Rdg.</td>
					<?php } ?>
				</tr>
				<?php 
				$pageRow = $pageRow + 2;
				$index = 1;
				$qry4 = 'SELECT grnObsMasterId, stdMeterAverage, testMeterAverage, expandedUncertinityInPre, resolutionTypeAAfterDecimal, stdMeter1AfterDecimal 
					FROM grnobservation WHERE grnDetailId = '.$row3['grnDetailId'];
				$res4 = mysql_query($qry4) or die("Error :: Cannot select grn overstation details.<hr>".mysql_error());	
				$reccnt4 = mysql_num_rows($res4);
				$counter4 = 0;
				$rangeSingleTime = null;
				$range2SingleTime = null;
				while($row4 = mysql_fetch_assoc($res4)) {
					$counter4++;
					$reading1 = number_format($row4['testMeterAverage'], $row4['stdMeter1AfterDecimal'], ".", ""); 
					//$reading2 = number_format($row4['stdMeterAverage'], $row4['resolutionTypeAAfterDecimal']);	
					$reading2 = $row4['stdMeterAverage'];	
					
					if($row3['tableNo'] == 1 && $displayCalculated == 1)
						$readingCalc = number_format(($row4['stdMeterAverage'] / $mf), $row4['stdMeter1AfterDecimal'], ".", "");
					
					if($row3['tableNo'] == 2) {
						$readingInVolt = number_format($row4['stdMeterAverage'], $row4['resolutionTypeAAfterDecimal'], ".", "");
						$reading2 = number_format(($row4['stdMeterAverage'] * $mf), $row4['resolutionTypeAAfterDecimal'], ".", "");	
					}
					
					if($row3['tableNo'] == 3) {
						$readingInAmp = number_format($reading1 / $mf);
						$reading2 = number_format($row4['stdMeterAverage'], $row4['resolutionTypeAAfterDecimal'], ".", "");
					}
					
					$expanded = abs(sprintf("%.3f", $row4['expandedUncertinityInPre']));
				
					$qry5 = "SELECT mastermetersubsub.rangeValue, leastCount  
                                FROM grnobsmaster
                                LEFT JOIN grndetail ON grnobsmaster.grnDetailId = grndetail.grnDetailId
                                LEFT JOIN parameterentry ON grndetail.parameterId = parameterentry.parameterId
                                LEFT JOIN mastermeter ON grnobsmaster.masterMeterId = mastermeter.masterMeterId
                                LEFT JOIN mastermetersub ON grnobsmaster.mParameterId = mastermetersub.masterMeterSubId
                                LEFT JOIN mastermetersubsub ON grnobsmaster.mRangeId = mastermetersubsub.masterMeterSubSubId
                               WHERE grnobsmaster.grnId = ".$_GET['grnId']."
                                 AND grnobsmaster.grnDetailId = ".$row3['grnDetailId'];		

					$res5 = mysql_query($qry5) or die("Error :: Cannot select range value.<hr>".mysql_error());
					$row5 = mysql_fetch_assoc($res5);
					
					$range1 = $row5['rangeValue'];
					//preg_match_all('!\d+\.*\d*!', $row5['leastCount'] ,$match);
					preg_match_all('!\d+\.*\d*!', $reading1 ,$match);
					$deci = explode('.', $match[0][0]);
					$leastCount = (count($deci) > 1) ? strlen($deci[1]) : 0;
					
					if($row3['tableNo'] != 2 && $row3['tableNo'] != 3) {
						//$reading2 = number_format($reading2, $leastCount, ".", "");
					}
					
					//$units	= ($reading2 / $mf - $reading1); 	
					if($row3['tableNo'] == 1 || $row3['tableNo'] == 5)
						$units = number_format(($row4['stdMeterAverage'] / $mf - $row4['testMeterAverage']), $leastCount, ".", "");
						
					if($row3['tableNo'] == 2 || $row3['tableNo'] == 6)
						$units = number_format(($row4['stdMeterAverage'] - $row4['testMeterAverage']), $row3['tableNo'] == 6 ? 1: 2, ".", "");
						
					if($row3['tableNo'] == 3 || $row3['tableNo'] == 4) 
						$units = number_format(($row4['stdMeterAverage'] - ($row4['testMeterAverage'] / $mf)), $row4['resolutionTypeAAfterDecimal'], ".", "");	
						
					if($row3['tableNo'] == 1)	
						$rdg	= number_format((($row4['stdMeterAverage'] / $mf - $row4['testMeterAverage']) / $row4['testMeterAverage'] * 100), $row4['stdMeter1AfterDecimal'], ".", "");
					else if($row3['tableNo'] == 3)
						$rdg	= number_format((($row4['stdMeterAverage'] - $row4['testMeterAverage'] / $mf) / $row4['testMeterAverage'] / $mf * 100), $row4['resolutionTypeAAfterDecimal'], ".", "");
					else
						// $rdg	= number_format((($row4['stdMeterAverage'] / $mf - $row4['testMeterAverage']) / $row4['testMeterAverage'] * 100), $row4['resolutionTypeAAfterDecimal'], ".", "");
				?>
				<tr valign="top">
					<td align="center" width="5"><?php echo $index; ?></td>
					<?php if($row3['tableNo'] == 6) { if($rangeSingleTime == null) { ?>
					<td align="center" rowspan="<?php echo $reccnt4; ?>" width="<?php echo $width1; ?>" style="vertical-align: middle;"><?php echo $range1; ?> &Omega;</td>
					<?php $rangeSingleTime = $range1; }
					} else { ?>
					<td align="center" width="<?php echo $width1; ?>"><?php echo $range1; ?></td>
					<?php } ?>
					<?php if($row3['tableNo'] == 6) { 
						$query_temp_ohms = "SELECT `ohms` FROM `tmp_ohm_relation` WHERE `temp` = " . $reading1;
						$query_result_temp_ohms = mysql_query($query_temp_ohms);
						$according_ohms_value = mysql_fetch_array($query_result_temp_ohms);
					?>
					<td align="center"><?php echo $according_ohms_value['ohms']; ?></td>
					<?php } ?>
					<td align="center" width="<?php echo $width1; ?>"><?php echo $reading1; ?></td>
					<?php if($row3['tableNo'] == 2) { ?>
					<td align="center" width="<?php echo $width1; ?>"><?php echo $readingInVolt; ?></td>
					<?php } ?>
					<?php if($row3['tableNo'] == 1 && $displayCalculated == 1) { ?>
					<td align="center" width="<?php echo $width; ?>"><?php echo $readingCalc; ?></td>
					<?php } ?>
					<?php if($row3['tableNo'] == 6) { if($range2SingleTime == null) { ?>
					<td align="center" rowspan="<?php echo $reccnt4; ?>" width="<?php echo $width1; ?>"  style="vertical-align: middle;"><?php echo $range2; ?></td>
					<?php $range2SingleTime = $range2; }
					} else { ?>
					<td align="center" width="<?php echo $width1; ?>"><?php echo $range2; ?></td>
					<?php } ?>
					<td align="center" width="<?php echo $width1; ?>"><?php echo $reading2; ?></td>
					<?php if($row3['tableNo'] == 3) { ?>
					<td align="center" width="<?php echo $width1; ?>"><?php echo $readingInAmp; ?></td>
					<?php } ?>
					<td align="center" width="<?php echo $width1; ?>"><?php echo $units; ?></td>
					<?php if($row3['tableNo'] != 6) { ?>
					<td align="center" width="<?php echo $width1; ?>"><?php echo $rdg; ?></td>
					<?php } ?>
					<td align="center" width="<?php echo ($row3['tableNo'] == 6? $width/2: $width); ?>"><?php echo $expanded; ?></td>
				</tr>
				<?php 
					$pageRow++;
					$index++;

					if($pageRow == $pageMod && $counter4 != $reccnt4) {
						$pageRow = 0;
				?>
			</table>
		</td>
	</tr>	
</table>
<table width="700">	
	<tr valign="top">
		<td width="700">
			<b>NOTE:</b><br />
			1. The reported expanded uncertainty in measurement is stated as the standard uncertainty 
			      in measurement multiplied by the coverage factor k=2, which for a normal distribution 
			      corresponds to a coverage probability of approximately 95%.
		</td>
	</tr>
</table>
<hr />
<table width="700">	
	<tr valign="top">
		<td width="700">
			<table width="695" border="0">
				<tr>
					<td align="left" colspan="5" style="font-family: 'Exotc350 DmBd BT';font-size:20px; "><b>For Krishna Instruments</b></td>
				</tr>
				<tr>
					<td align="left" colspan="5" ><br /></td>
				</tr>
				<tr>
					<td width="225" align="left" style="font-size:x-small;">CALIBRATED BY : (Bhavesh Tank, CAL. ENG.)</td>
					<td width="70" align="left" >&nbsp;</td>
					<td width="225" align="left" style="font-size:x-small;">APPROVED BY : (D.R.SHAH, C.E.O.)</td>
					<td width="50" align="left" >&nbsp;</td>
					<td width="75" align="left" >SEAL:</td>
				</tr>
			</table>
		</td>
	</tr>
</table>	
</div>
<div style="clear:both;"></div>
<br/><br/>
<style>
@media print
{
.pagebreak { page-break-after:always; }
}
</style>
<div class="pagebreak"></div>
<div style="border: 1px solid; width: 700;">
<table width="700">
	<tr>
		<td width="195">CERTIFICATE NO.</td>
		<td width="150">: <?php echo $certificateNo; ?></td>
		<td width="5">&nbsp;</td>
		<td width="250">DATE OF CALIBRATION</td>
		<td width="100" align="right">: <?php echo $callibrationDate; ?></td>
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grnNo; ?></td>
		<td>&nbsp;</td>
		<td>NEXT RECOMMENDED DATE</td>
		<td align="right">: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td width="195">PAGE</td>
		<td width="150">: <?php echo $pageNo; $pageNo++; ?> OF <span class="total_page_no">2</span></td>
		<td width="5">&nbsp;</td>
		<td width="250">CERTIFICATE ISSUE DATE</td>
		<td width="100" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
	</tr>
</table>
<hr />
<table width="700">	
	<tr>
		<td width="5">&nbsp;&nbsp;&nbsp;</td>	
		<td width="695">
			<table width="680">
				<tr>
					<td><?php echo $row3['parameterName']; $pageRow++; ?></td>
				</tr>
			</table>
			<table width="680" cellpadding="3" cellspacing="0" border="1" style="font-size:10px;">
				<tr valign="top" style="font-weight:bold;">
					<td width="5" rowspan="2" align="center"></td>
					<td width="<?php echo $width; ?>" colspan="2" align="center">Callibration <?php if($row3['tableNo'] != 3){ ?>Standard<?php } ?></td>
					<?php if($row3['tableNo'] == 1 && $displayCalculated == 1) { ?>
					<td width="<?php echo $width; ?>" rowspan="2" align="center" valign="middle">Calculated Reading</td>
					<?php } else if($row3['tableNo'] == 2) { ?>
					<td width="<?php echo $width; ?>" rowspan="2" align="center" valign="middle">Calculated Reading in Volt</td>
					<?php } ?>
					<td width="<?php echo $width; ?>" colspan="2" align="center">Unit Under Test</td>
					<?php if($row3['tableNo'] == 3) { ?>
					<td width="<?php echo $width; ?>" rowspan="2" align="center" valign="middle">Secondary Calculated Reading in Amps.</td>
					<?php } ?>
					<td width="<?php echo $width; ?>" colspan="2" align="center">Error</td>
					<td width="<?php echo $width; ?>" rowspan="2" align="center" valign="middle">Expanded Uncert. In %</td>
				</tr>
				<tr>
					<td align="center">Range</td>
					<td align="center">
					<?php 
					if($row3['tableNo'] == 2)
						echo 'Applied Voltage';
					else if($row3['tableNo'] == 3)
						echo 'Applied Ampere';	
					else if($row3['tableNo'] == 4)
						echo 'Primary Reading in Volt.';		
					else
						echo 'Reading'; 
					?>
					</td>
					<td align="center">Range</td>
					<td align="center">
					<?php 
					if($row3['tableNo'] == 3)
						echo 'Secondary Reading in Amps.';
					else if($row3['tableNo'] == 4)
						echo 'Secondary Reading in Volt.';	
					else
						echo 'Reading'; 
					?>
					</td>
					<td align="center">Units</td>
					<td align="center">$ of Rdg.</td>
				</tr>
<script>
$('.total_page_no').html((parseInt($('.total_page_no').html()) + 1));
</script>
				<?php
					}
				}
				mysql_free_result($res4); 
				?>
			</table>
		<?php
		}
		mysql_free_result($res3);
		?>
		</td>
	</tr>	
</table>
<table width="700">	
	<tr valign="top">
		<td width="700">
			<b>NOTE:</b><br />
			1. The reported expanded uncertainty in measurement is stated as the standard uncertainty 
			      in measurement multiplied by the coverage factor k=2, which for a normal distribution 
			      corresponds to a coverage probability of approximately 95%.
		</td>
	</tr>
</table>
<hr />
<table width="700">	
	<tr valign="top">
		<td width="700">
			<table width="695" border="0">
				<tr>
					<td align="left" colspan="5" style="font-family: 'Exotc350 DmBd BT';font-size:20px; "><b>For Krishna Instruments</b></td>
				</tr>
				<tr>
					<td align="left" colspan="5" ><br /></td>
				</tr>
				<tr>
					<td width="225" align="left" style="font-size:x-small;">CALIBRATED BY : (Bhavesh Tank, CAL. ENG.)</td>
					<td width="70" align="left" >&nbsp;</td>
					<td width="225" align="left" style="font-size:x-small;">APPROVED BY : (D.R.SHAH, C.E.O.)</td>
					<td width="50" align="left" >&nbsp;</td>
					<td width="75" align="left" >SEAL:</td>
				</tr>
			</table>
		</td>
	</tr>
</table>	
</div>
<?php
// html - end //
exit;
$content = ob_get_clean();

// convert in PDF
require_once 'html2pdf/html2pdf.class.php';
try {
	$html2pdf = new HTML2PDF('P', 'A4', 'fr');
	// $html2pdf->setModeDebug();
	$html2pdf->setDefaultFont('Times');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output('example.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>