<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(OBS_MODULE_ID,"certi. view"))){
	$selectedCustomerNew = "";
	$selectedStatusFired = "";
	$statusFired = "";
	$certiArray  = 0;
	if(isset($_POST['customerId']) && $_POST['customerId'] > 0)
	{
		$whereQry = "WHERE grnmaster.customerId = ".$_POST['customerId']."";
	}
	else
	{
		$whereQry = "WHERE 1 = 1";
	}
		
  if(isset($_POST['status']) && $_POST['status'] == "Done"){
		$statusQry = "JOIN grnobsmaster ON grndetail.grnDetailId = grnobsmaster.grnDetailId";
		$andQry = " 1 = 1";
	}elseif(isset($_POST['status']) && $_POST['status'] == "Pending"){
		$statusQry = "LEFT JOIN grnobsmaster ON grndetail.grnDetailId = grnobsmaster.grnDetailId";
		$andQry = " grndetail.grndetailId NOT IN ( SELECT grnDetailId FROM grnobsmaster) ";
	}else{
		$statusQry = "LEFT JOIN grnobsmaster ON grndetail.grnDetailId = grnobsmaster.grnDetailId";
		$andQry = " 1 = 1";
	}
	
  $statusFired = isset($_POST['status']) ? $_POST['status'] : '';
  $selectedStatusFired = isset($_POST['status']) ? $_POST['status'] : '';
  // additional change by girish - start
  /*$sel_certificate = "SELECT grnmaster.grnId, grnNo, item.itemName, grndetail.itemCode, grndetail.makeModel, grndetail.rangeValue, grndetail.accuracy,
                             grnobsmaster.userName, grnobsmaster.selfCertiNo
                        FROM grnmaster
                        JOIN grndetail ON grnmaster.grnId = grndetail.grnId
                        JOIN item ON grndetail.itemId = item.itemId
                        ".$statusQry."
                        ".$whereQry."
                         AND ".$andQry." ";*/
  $currPage = 0;
  if( isset($_GET['page']) ) {
  	$currPage = ( intval($_GET['page']) - 1 );
	if($currPage < 0) $currPage = 0;
  }
  $recordsPerPage = 1000000;
  						 
  $sel_certificate = "SELECT grnmaster.grnId, grnNo, item.itemName, grndetail.itemCode, grndetail.makeModel, grndetail.rangeValue, grndetail.accuracy,
                             grnobsmaster.userName, grnobsmaster.selfCertiNo, 
							 customer.custName 
                        FROM grnmaster
                        JOIN grndetail ON grnmaster.grnId = grndetail.grnId
                        JOIN item ON grndetail.itemId = item.itemId 
						JOIN customer ON customer.customerId  = grnmaster.customerId 
                        ".$statusQry."
                        ".$whereQry."
                         AND ".$andQry." ".
						 "LIMIT ".$currPage.", ".$recordsPerPage;						 
  // additional change by girish - end						 
  $res_certificate = mysql_query($sel_certificate);
  $certiArray = array();
  $count = 0;
  while($cRow = mysql_fetch_array($res_certificate))
  {
  	$certiArray[$count]['grnId']       = $cRow['grnId'];
  	$certiArray[$count]['grnNo']       = "N-".$cRow['grnNo'];
  	$certiArray[$count]['itemName']    = $cRow['itemName'];
  	$certiArray[$count]['itemCode']    = $cRow['itemCode'];
  	$certiArray[$count]['makeModel']   = $cRow['makeModel'];
  	$certiArray[$count]['range']       = $cRow['rangeValue'];
  	$certiArray[$count]['accuracy']    = $cRow['accuracy'];
  	$certiArray[$count]['userName']    = $cRow['userName'];
  	$certiArray[$count]['selfCertiNo'] = $cRow['selfCertiNo'];
	// additional change by girish - start
	$certiArray[$count]['custName'] = $cRow['custName'];
	// additional change by girish - end
  	$count++;
  }
	
  /////////////////  Customer Name:Start
  $selectName = "SELECT customerId,custName,custCode,address
                   FROM customer
                  ORDER BY custName";
  $selectNameResult = mysql_query($selectName);
  $a = 0;
  $selectedCustomerId = 0;
  $selectedCustomer = '';
  while($nameRow = mysql_fetch_array($selectNameResult))
  {
    $customerId[$a] = $nameRow['customerId'];
    $custName[$a]   = $nameRow['custName'];
    if(isset($_POST['getCertiData']) && $_POST['customerId'] == $nameRow['customerId']){
    	$selectedCustomerId = $nameRow['customerId'];
    	$selectedCustomer = $nameRow['custName'];
    }
    $a++;
  }
  /////////////////  Customer Name:Ends
  $selectedCustomerNew = isset($_POST['customerId']);
/*print '<pre>';
print_r($certiArray);
exit;*/
  $statusValue[0] = 'All';
  $statusName[0]  = 'All';
  $statusValue[1] = 'Pending';
  $statusName[1]  = 'Certi. Pending';
  $statusValue[2] = 'Done';
  $statusName[2]  = 'Certi. Done';
  
  include("./bottom.php");
  $smarty->assign("customerId",$customerId);
  $smarty->assign("custName",$custName);
  $smarty->assign("selectedCustomerId",$selectedCustomerId);
  $smarty->assign("statusFired",$statusFired);
  $smarty->assign("selectedCustomer",$selectedCustomer);
  $smarty->assign("certiArray",$certiArray);
  $smarty->assign("selectedCustomerNew",$selectedCustomerNew);
  $smarty->assign("selectedStatusFired",$selectedStatusFired);
  $smarty->assign("statusValue",$statusValue);
  $smarty->assign("statusName",$statusName);
  $smarty->display("listOfCertificate.tpl");
} else {
  header("Location:index.php");
}  
}

?>