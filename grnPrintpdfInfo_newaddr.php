<?php
include("include/omConfig.php");

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
?>

<?php 

  $grnId               = "";
  $grnDetailId         = "";
  $grnNo               = "";
  $grnDate             = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();
  $cfgRecPerGrnPage    = 8;
  // Pdf Create :Start
  
  //SELECT OF GRN MASTER :START
  $selectGrnMaster = "SELECT grnPrefix,grnNo,infoSheetNo,DATE_FORMAT(grnDate,'%d-%m-%Y') AS grnDate,poNo,DATE_FORMAT(poDate,'%d-%m-%Y') AS poDate,custName,custCode,remarks,grnmaster.contPerson,phNo,userName
                        FROM grnmaster
                        JOIN customer
                       WHERE grnId = ".$_GET['grnId']."
                         AND grnmaster.customerId = customer.customerId";
  $selectGrnMasterRes = mysql_query($selectGrnMaster);
  $selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes);
  //SELECT OF GRN MASTER :END
  //SELECT OF GRN DETAIL :START
  $selectGrnEntryResRow = array();
  $prevInstrument = 0;
  $a  = 0;
//  grnDetail.grnDetailId,
  $selectGrnEntry  = "SELECT grndetail.grnId,grndetail.itemId AS grnDetailItemId,item.itemName,grndetail.itemCode,grndetail.description,
                             grndetail.rangeValue,grndetail.description,grndetail.challan,grndetail.received,
                             grndetail.grnCondition,custReqDate,DATE_FORMAT(expDelivDate,'%d-%m-%y') AS expDelivDate,
                             parameterentry.parameterName
                        FROM grndetail
                        JOIN item
                        JOIN parameterentry
                       WHERE grnId = ".$_GET['grnId']."
                         AND grndetail.itemId = item.itemId
                         AND grndetail.parameterId = parameterentry.parameterId
                       ORDER BY grndetail.grnDetailId";
  $selectGrnEntryRes   = mysql_query($selectGrnEntry);
  while($selectGrnEntryResRow1 = mysql_fetch_assoc($selectGrnEntryRes)){
      $selectGrnEntryResRow[] = $selectGrnEntryResRow1;
  }
//  echo "<pre>"; print_r($selectGrnEntryResRow); exit;
  ?>

<?php
$html = '';
$html .= '<table width="730">
    <tr>
        <td align="right" width="450" ></td>
        <td align="right" width="280"><img style="width: 265px; height: 114px;" src="./images/logo_newaddr.jpg" /></td>
    </tr>
    <tr>
        <td align="left" width="450" style="font-size: 18px;"><strong>CUSTOMER ORDER INFO SHEET</strong></td>
        <td align="right" width="280" style="font-size: 16px;">SM F 01</td>
    </tr>
</table><br/>';
$html .= '<table width="696" style="font-size: 13px;">
    <tr>
        <td width="70" align="right">Sheet No.:</td>
        <td width="80" align="left">'.$selectGrnMasterRow['infoSheetNo'].'</td>
        <td width="10" >&nbsp;</td>
        <td width="60" align="right">GRN No.:</td>
        <td width="100" align="left">'.$selectGrnMasterRow['grnPrefix']."-".$selectGrnMasterRow['grnNo'].'</td>
        <td width="10" >&nbsp;</td>
        <td width="40" align="right">Date :</td>
        <td width="80" align="left">'.$selectGrnMasterRow['grnDate'].'</td>
        <td width="10" >&nbsp;</td>
        <td width="110" align="right">Customer Code :</td>
        <td width="70" align="left">'.$selectGrnMasterRow['custCode'].'</td>
    </tr>
    <br/>
</table>';
$html .= '<table width="100%" style="font-size: 13px; border: 1px solid black;" >
    <tr>
        <td width="100%" align="center"> FOLLOWING INSTRUMENTS RECEIVED FOR - CALIBRATION</td>
    </tr>
</table>';

$html .= '<table width="100%" style="font-size: 10px; border: 1px solid #000000; border-collapse: collapse;"  class="main_table" >';
$html .= '<tr>
        <td width="20" align="center" valign="middle"  style="font-size: 10px; border: 1px solid #000000; padding: 1px;" >SR. <br/>No.</td>
        <td width="120" align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;" >NAME OF INSTRUMENT</td>
        <td width="70" align="center" valign="middle"  style="font-size: 10px; border: 1px solid #000000; padding: 1px;" >Instrument ID No.</td>
        <td width="80" align="center" valign="middle"  style="font-size: 10px; border: 1px solid #000000; padding: 1px;" >PARAMETERS</td>
        <td width="100" align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;" >RANGE</td>
        <td width="70" align="center" valign="middle"  style="font-size: 10px; border: 1px solid #000000; padding: 1px;" >Notes</td>
        <td width="80" align="center" valign="middle"  style="font-size: 10px; border: 1px solid #000000; padding: 1px;" >Cali Due Dt Requested By Cust.</td>
        <td width="80" align="center" valign="middle"  style="font-size: 10px; border: 1px solid #000000; padding: 1px;" >Expected Delivery Date</td>
        <td width="60" align="center" valign="middle"  style="font-size: 10px; border: 1px solid #000000; padding: 1px;" >Condi. of Instr.</td>
    </tr>';
    $i = 1;
    foreach ($selectGrnEntryResRow as $selectGrnEntry){
    $html .= '<tr style="line-height: 21px;">
        <td width="20"  align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;">'.$i.'</td>
        <td width="120" align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;">'.$selectGrnEntry['itemName'].'</td>
        <td width="70"  align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;">'.$selectGrnEntry['itemCode'].'</td>
        <td width="80"  align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;">'.$selectGrnEntry['parameterName'].'</td>
        <td width="100" align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;">'.$selectGrnEntry['rangeValue'].'</td>
        <td width="70"  align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;">'.$selectGrnEntry['description'].'</td>
        <td width="80"  align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;">'.$selectGrnEntry['custReqDate'].'</td>
        <td width="80"  align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;">'.$selectGrnEntry['expDelivDate'].'</td>
        <td width="60"  align="center" valign="middle" style="font-size: 10px; border: 1px solid #000000; padding: 1px;">'.$selectGrnEntry['grnCondition'].'</td>
    </tr>';
    $i++;
    } 
$html .= '</table>';
$html .= '<table width="680" style="font-size: 13px;" >
        <tr>
            <td width="680" align="left">Note :</td>
        </tr>
        <tr>
            <td width="680" align="left" >(1) Callibration Standared method to be used.</td>
        </tr>
        <tr>
            <td width="680" align="left" >(2) Review of record (if any) to be done before sent to customer.</td>
        </tr>
        <tr>
            <td width="680" align="left" >(3) Is Customer has Provide Specification for Observation ? Yes/No</td>
        </tr>
        <tr>
            <td width="680" align="left" >(4) Is Reporting Statement of Conformity required for decision rule when deviation of results observed ? Yes/No</td>
        </tr>
    </table>';
$html .= '<table width="680" style="font-size: 13px;" >
        <tr>
            <td width="220" align="left">ANY OTHER INFO & REMARKS : </td>
            <td width="460" align="left">'.$selectGrnMasterRow['remarks'].'</td>
        </tr>
    </table><br/>';
$html .= '<table width="680" style="font-size: 13px;" >
        <tr>
            <td width="330" align="left">Sign (KI): ___________________ </td>
            <td width="350" align="left">Sign(Customer'."'".'s Rep.):</td>
        </tr>
    </table>';
$html .= '<table width="680" style="font-size: 13px;" >
        <tr>
            <td width="680" align="center">For delivery of instruments this receipt is required, otherwise material will not be returned.</td>
        </tr>
    </table>';

?>
<!--
<style>
table {font-family: "Times New Roman"; font-size: 13px;}
tr {
    line-height: 21px;
}
.main_table tr td {
    border: 1px solid black;
}
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 1px solid black;
}
.main_table2 td, .main_table2 th {
    padding: 3px;
}
.main_table td, .main_table th {
    padding: 1px;
}
.main_table{
    padding-bottom: 2px;
}
</style>-->
<?php
//exit;
// html - end //
//$html = ob_get_clean();
require_once('mpdfnew/random_compat/lib/random.php');
require_once('mpdfnew/vendor/autoload.php');

$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P','margin_left' => 10, 'margin_right' => 3, 'margin_top' => 3, 'margin_bottom' => 3]);
$mpdf->SetTitle('ORDER INFO SHEET');
$mpdf->WriteHTML($html);
$mpdf->Output('order_info_sheet.pdf', 'I'); 
exit;
//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>