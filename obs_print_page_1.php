<?php
include("include/omConfig.php");

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
	if($haystack == null){
		return false;
	}else{
 foreach($haystack as $item) {
 	      if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
}
    return false;
}
// general functions - start //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// variable list - start //
$certificateNo 			= '';
$certificate_no			= '';
$grnNo 					= '';
$grn_no 					= '';
$grnDate				= ''; 
$totalPages 			= 0;
$callibrationDate		= '';
$callibrationDateNext 	= '';
$certificateIssue		= '';
$certificateIssueDate	= '';
$customerName			= '';
$customerAddress		= '';
$customerCity		= '';
$customerRefNo			= '';
$ambientTemperature		= '';
$relativeHumidity		= '';
$remarks				= '';

$counter 				= 0;
$grnDetailIds 			= array();
$tempArr 				= array() ;

$instrumentDetails					= array();
$instrumentDetails['item_name']		= '';
$instrumentDetails['make']			= '';
$instrumentDetails['item_code']		= '';
$instrumentDetails['id_no']			= '';
$instrumentDetails['range_value']	= '';
$instrumentDetails['accuracy']		= '';
$instrumentDetails['grn_condition']	= '';
$instrumentDetails['certi_remarks']	= '';
$instrumentDetails['cali_location']	= '';
$instrumentDetails['uuc_location']	= '';
$instrumentDetails['ulr_no']	= '';
$instrumentDetails['userName']	= '';
$instrumentDetails['approved_by']	= '';
$instrumentDetails['designation']	= '';
$instrumentDetails['sr_no']	= '';

$masterMeters						= array();
$masterMeters['masterMeterName']			= array();
$masterMeters['masterMeterIdNo']				= array();
$masterMeters['masterMeterMake']			= array();
$masterMeters['masterMeterModelNo']				= array();
$masterMeters['masterMeterSerialNo']			= array();
$masterMeters['masterMeterCertificateNo']		= array();
$masterMeters['due_date']	= array();

$observations						= array();
/*$observations['parameters']			= array();
$observations['table_no']			= array();
$observations['range1']				= array();
$observations['reading1']			= array();
$observations['range2']				= array();
$observations['reading2']			= array();
$observations['units']				= array();
$observations['rdg']				= array();
$observations['expanded']			= array();*/
// variable list - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// fetch data - start //
$qry = 'SELECT DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate,
	DATE_FORMAT(grndetail.nextYearDate,"%d/%m/%Y") AS nextYearDate,
	certificateIssue,DATE_FORMAT(grndetail.certificateIssueDate,"%d/%m/%Y") AS certificateIssueDate,
	grndetail.grnId,grndetail.certificate_no,grndetail.grnDetailId,
	grndetail.temperature,grndetail.humidity,itemCode,
	certiRemarks,
	CONCAT("N-",grnmaster.grnNo) AS grnNo,grnmaster.grn_no,
	DATE_FORMAT(grnmaster.grnDate,"%d/%m/%Y") AS grnDate,
	customer.custName,customer.address,customer.city,
	customer.custCode AS customerRef 
	FROM grndetail 
	LEFT JOIN grnmaster ON grnmaster.grnId = grndetail.grnId 
	LEFT JOIN customer ON customer.customerId = grnmaster.customerId 
	WHERE grndetail.grnDetailId = '.$_GET['grnDetailId'];
$res = mysql_query($qry) or die("Error :: Cannot select basic details.<hr>".mysql_error());
if($row = mysql_fetch_assoc($res)) {
	$grn_no 					= $row['grn_no'];
	$certificate_no 					= $row['certificate_no'];
	$grnDate				= $row['grnDate'];
	$callibrationDate		= $row['callibrationDate'];
	$callibrationDateNext 	= $row['nextYearDate'];
	$certificateIssue		= $row['certificateIssue'];
	$certificateIssueDate	= $row['certificateIssueDate'];
	$customerName			= $row['custName'];
	$customerAddress		= $row['address'];
	$customerCity		= $row['city'];
        $ref_no = '';
        $words = preg_split("/\s+/", trim($row['custName']));
//        echo "<pre>"; print_r($words); exit;
        $acronym = "";
        foreach ($words as $w) {
            if(strlen($acronym) == 2){
                break;
            } else {
                $acronym .= $w[0];
            }
        }
        $cust_sn = Strtoupper($acronym);
	$customerRefNo			= $cust_sn.'/Cali/'.$row['customerRef'];
	$ambientTemperature		= $row['temperature'];
	$relativeHumidity		= $row['humidity'];
	
	$tempArr = $instrumentDetails;
	// instrument details query //
	$qry1 = 'SELECT grndetail.grnDetailId,grndetail.makeModel AS make,grndetail.itemCode AS item_code,
		grndetail.instrumentId AS id_no,grndetail.rangeValue AS range_value,
		grndetail.accuracy,grndetail.grnCondition AS grn_condition,item.itemName AS item_name,
		grndetail.certiRemarks AS certi_remarks, grndetail.cali_location, grndetail.uuc_location,grndetail.ulr_no, staff.staffName as userName, grndetail.sr_no, approved_by.name as approved_by, approved_by.designation
		FROM grndetail 
		LEFT JOIN item ON item.itemId = grndetail.itemId 
		LEFT JOIN approved_by ON approved_by.id = grndetail.approved_by 
                LEFT JOIN staff ON staff.staffId = grndetail.user_id 
		WHERE grndetail.callibrationDate IS NOT NULL AND grndetail.grnId = '.$row['grnId'].' AND grndetail.grnDetailId = "'.$row['grnDetailId'].'"';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        while($row1 = mysql_fetch_assoc($res1)) { 
		array_push($grnDetailIds,$row1['grnDetailId']);
		foreach($tempArr as $key=>$val) {
			if(!in_array_r($row1[$key],$tempArr[$key])) {
				$tempArr[$key][$counter] = $row1[$key];
				$instrumentDetails[$key] .= $row1[$key].' | ';
			}
		}
		$counter++;
	}
	
	foreach($instrumentDetails as $key=>$val) {
		if($val != '')
			$instrumentDetails[$key] = substr($val,0,-3);
	}
	mysql_free_result($res1);
	
	$tempArr = $masterMeters;
	// master meters query //
        $all_least = '';
        $all_uut = '';
        $qry2 = 'SELECT master_json,uuc_json FROM observation_data as grn
		WHERE grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].'  ';
	$res2 = mysql_query($qry2) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $master_data = array();
//        echo "<pre>"; print_r(mysql_fetch_assoc($res1)); exit;
        while($row2 = mysql_fetch_assoc($res2)) {
                array_push($master_data,$row2);
        }
//        echo "<pre>"; print_r($master_data); exit;
        $master_meter_ids = array();
        $uuc_ranges = array();
        $all_least_arr = array();
        $max_count_page = count($master_data) + 1;
        foreach ($master_data as $m_key => $ms_data){ 
           
            $m_data = json_decode(json_encode(json_decode($ms_data['master_json'])), true);
            $u_data = json_decode(json_encode(json_decode($ms_data['uuc_json'])), true);
//             echo "<pre>"; print_r($m_data); exit;
//            $all_uut = $u_data[0]['uut_range'];
//            isset($uuc_data['uut_range_min']) ? $uuc_data['uut_range_min'] : '';
            foreach ($u_data as $uu_data){ 
                if($uu_data['uut_range_min'] != ''){
                    $uuu_name = $uu_data['uut_range_min'] ." to ". $uu_data['uut_range'];
                } else {
                    $uuu_name = $uu_data['uut_range'];
                }
                if(isset($uu_data['leastCount_print']) &&  ($uu_data['leastCount_print'] != '')){
                    $all_least_arr[] = $uu_data['leastCount_print'];
                }
                $uuc_ranges[] = $uuu_name;
            }
            foreach ($m_data as $s_data){ 
                $master_meter_ids[] = $s_data['masterMeterId'];
            }
            
        }
        $uni_uut = array_unique($uuc_ranges);
        $all_uut = implode(",",$uni_uut);
        $all_least_arr1 = array_unique($all_least_arr);
        $all_least = implode(",",$all_least_arr1);
	$qry2 = 'SELECT masterMeterName as "0",masterMeterIdNo as "1",masterMeterMake as "2",masterMeterModelNo as "3",masterMeterSerialNo as "4",masterMeterCertificateNo as "5",
		 DATE_FORMAT(masterMeterExp,"%d/%m/%Y") AS "6"
                 FROM mastermeter 
		 WHERE masterMeterId IN ('.implode(",",$master_meter_ids).')'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res2 = mysql_query($qry2) or die("Error :: Cannot select master meter details1.<hr>".mysql_error());
	$master_m_data = array();
        while($row2 = mysql_fetch_assoc($res2)) { 
            $master_m_data[] = $row2;
	}
//        echo "<pre>"; print_r($master_m_data); exit;
	$qry21 = 'SELECT GROUP_CONCAT(DISTINCT procedureText) as procedureText, GROUP_CONCAT(DISTINCT page_title) as page_title
                 FROM mastermeter 
		 WHERE masterMeterId IN ('.implode(",",$master_meter_ids).')'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res21 = mysql_query($qry21) or die("Error :: Cannot select master meter details2.<hr>".mysql_error());
	$master_procedureText = '';
	$page_title = '';
        while($row21 = mysql_fetch_assoc($res21)) { 
            $master_procedureText = $row21['procedureText'];
            $page_title = $row21['page_title'];
	}
//        echo "<pre>"; print_r($master_procedureText); exit;
//print '<pre>'; print_r($masterMeters);
	mysql_free_result($res2);
}
mysql_free_result($res);
// fetch data - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// html - start //
ob_start();
?>
<?php 
$qry112 = 'SELECT * FROM setting ';
$res112 = mysql_query($qry112) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
$s_trac = '';
$s_approve = '';
$s_note = '';
$s_all_note = '';
$s_page_1_note = '';
while($row112 = mysql_fetch_assoc($res112)) { 
    if($row112['setting_name'] == 'traceability_to'){
        $s_trac = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'approved_by'){
        $s_approve = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'footer_note_for_page_1'){
        $s_note = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'footer_note_for_all'){
        $s_all_note = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'page_1_remark'){
        $s_page_1_note = $row112['setting_value'];
    }
}
?>
<style>
table {font-family: "Times New Roman"; font-size: 13px;}
tr {line-height: 21px;}
</style>

<div style="text-align:right; padding-right: 25px;"><b><?php echo $page_title; ?></b></div>
<div style="border: 1px solid black; border-radius: 10px; padding: 5px;">
<table width="605">
	<tr>
		<td width="150">CERTIFICATE NO.</td>
		<td width="145">: <?php echo $certificate_no; ?></td>
		<td width="5">&nbsp;</td>
		<td width="200">DATE OF CALIBRATION</td>
		<td width="95" align="right">: <?php echo $callibrationDate; ?></td>
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grn_no; ?></td>
		<td>&nbsp;</td>
		<td>NEXT RECOMMENDED DATE</td>
		<td align="right">: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td width="150">PAGE</td>
		<td width="145">: 1 OF <span class="total_page_no"><?php echo $max_count_page; ?></span></td>
		<td width="5">&nbsp;</td>
		<td width="200">CERTIFICATE ISSUE DATE</td>
		<td width="95" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
	</tr>
</table>
<hr />
<table width="605">
	<tr valign="top">
		<td width="5"><strong>1.</strong></td>
		<td width="200"><strong>CUSTOMER NAME & ADDRESS</strong></td>
		<td width="5">:</td>
		<td width="395" style="font-family: Agency FB;font-size:18px; font-weight:bold;"><?php echo $customerName; ?></td>
	</tr>
	<tr valign="top" >
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="font-size:14px; height:81px;" width="395">
                    <?php echo nl2br($customerAddress); ?>
                    <?php if(!empty($customerCity)) { ?>
                        <br/><b><?php echo $customerCity; ?></b>
                    <?php } ?>
		</td>
	</tr>
	<tr valign="top">
		<td><strong>2.</strong></td>
		<td><strong>CUSTOMER REFERENCE NO.</strong></td>
		<td>:</td>
		<td><?php echo $customerRefNo; ?></td>
	</tr>
	<tr valign="top">
		<td><strong>3.</strong></td>
		<td><strong>INSTRUMENT RECEIVED ON</strong></td>
		<td>:</td>
		<td><?php echo $grnDate; ?></td>
	</tr>
	<tr valign="top">
		<td><strong>4.</strong></td>
		<td><strong>ULR NO.</strong></td>
		<td>:</td>
		<td><?php echo $instrumentDetails['ulr_no']; ?></td>
	</tr>
	<tr valign="top">
		<td><strong>5.</strong></td>
		<td><strong>DESCRIPTION OF INSTRUMENT</strong></td>
		<td>:</td>
		<td>&nbsp;</td>
	</tr>
    </table>
    <table width="605">
	<tr valign="top">
		<td width="5">&nbsp;</td>
		<td width="100">Name</td>
		<td width="5">:</td>
		<td width="495"><?php echo $instrumentDetails['item_name']; ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Make/Model</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['make']; ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Sr. No.</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['item_code']; ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Instrument ID No.</td>
		<td>:</td>
                <td><?php echo !empty($instrumentDetails['id_no']) ? $instrumentDetails['id_no'] : '---'; ?></td>
	</tr>
    </table>
<table width="605" style="border-spacing:0; " cellspacing="0">
	<tr valign="top">
		<td width="5">&nbsp;</td>
		<td width="105">Range</td>
		<td width="5">:</td>
		<td width="185"><?php echo $instrumentDetails['range_value']; ?></td>
                <td width="115" style="border-left: 1px solod #000000;">&nbsp;Least Count</td>
		<td width="5">:</td>
		<td width="185"><?php echo $all_least; ?></td>
	</tr>
	<tr valign="top">
            <td width="5">&nbsp;</td>
            <td width="105">Accuracy</td>
            <td width="5">:</td>
            <td width="185"><?php echo $instrumentDetails['accuracy']; ?></td>
            <td width="115" style="border-left: 1px solod #000000;">&nbsp;Condition on receipt</td>
            <td width="5">:</td>
            <td width="185"><?php echo $instrumentDetails['grn_condition']; ?></td>
	</tr>
	<tr valign="top">
            <td width="5">&nbsp;</td>
            <td width="105">UUC Location</td>
            <td width="5">:</td>
            <td width="185"><?php echo $instrumentDetails['uuc_location']; ?></td>
            <td width="115" style="border-left: 1px solod #000000;">&nbsp;Remark</td>
            <td width="5">:</td>
            <td width="185"><?php echo $s_page_1_note; ?></td>
	</tr>
    </table>
    <table width="605">
	<tr valign="top">
		<td width="5"><strong>6.</strong></td>
		<td width="200"><strong>AMBIENT TEMPERATURE</strong></td>
		<td width="5">:</td>
                <td width="395" style="font-family:dejavusans; font-size: 12px;"><?php echo $ambientTemperature; ?></td>
	</tr>
	<tr valign="top">
		<td width="5"><strong></strong></td>
		<td width="200"><strong>RELATIVE HUMIDITY</strong></td>
		<td width="5">:</td>
		<td width="395"><?php echo $relativeHumidity; ?></td>
	</tr>
    </table>
<table >	
	<tr valign="top">
		<td width="5"><strong>7.</strong></td>
		<td width="600"><strong>DETAILS OF REFERENCE STANDARD & MAJOR INSTRUMENTS USED</strong></td>
	</tr>
        <br/>
	<tr valign="top">
            <td width="5">&nbsp;</td>
            <td width="600">
            <?php
                    $max_rows = count($master_m_data);
                    $name_arr = array();
                    $name_arr[] = 'Name';
                    $name_arr[] = 'I.D. No.';
                    $name_arr[] = 'Make';
                    $name_arr[] = 'Model No.';
                    $name_arr[] = 'Serial no.';
                    $name_arr[] = 'Certificate No.';
                    $name_arr[] = 'Calibration Valid Up to';
                    if($max_rows <= 2){
                        $font_s = 'font-size:14px;';
                    } else {
                        $font_s = 'font-size:10px;';
                    }
//                    echo "<pre>"; print_r($master_m_data); exit;
                ?>
            <table cellpadding="0" cellspacing="0" border="1" style="<?php echo $font_s; ?>" width="100%">
                <?php for($x = 0; $x < 7; $x++) { ?>
                <tr>
                    <td align="left" style="padding: 2px;" valign="center"><b><?php echo $name_arr[$x]; ?></b></td>
                    <?php foreach ($master_m_data as $master_m){ ?>
                        <td align="left" style="padding: 2px;" valign="center"><?php echo $master_m[$x]; ?></td>
                    <?php } ?>
                </tr>
                <?php } ?>
                <tr>
                    <td align="left" style="padding: 2px;" valign="center"><b>Traceability To </b></td>
                    <td align="left" style="padding: 2px;" valign="center" colspan="<?php echo $max_rows; ?>"><?php echo $s_trac; ?></td>
                </tr>
            </table>
	</td>
	</tr>
        
</table>
<table width="605">	
	<tr valign="top">
		<td width="5"><strong>8.</strong></td>
		<td width="200"><strong>PROCEDURE</strong></td>
		<td width="5">:</td>
		<td width="395"><?php echo $master_procedureText; ?></td>
	</tr>
	<tr valign="top">
		<td width="5"><strong>9.</strong></td>
		<td width="200"><strong>CALIBRATION LOCATION</strong></td>
		<td width="5">:</td>
		<td width="395"><?=  ($instrumentDetails['cali_location'] == '2') ? 'At Site' : 'At Lab';  ?></td>
	</tr>
        <tr valign="bottom">
            <hr />
            <td width="5" style="height: 5px;"><strong></strong></td>
            <td colspan="3" style="height: 5px;" width="600" style="font-size: 20px;"></td>
	</tr>
        <tr valign="bottom">
            <td width="5"><strong>&nbsp;</strong></td>
            <td colspan="3" width="600" style="font-size: 20px;"><img src="./images/for-krishna.png" /></td>
	</tr>
        <tr valign="top">
            <td colspan="4">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="4">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="4">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="4">CALIBRATED BY: (<?php echo $instrumentDetails['userName']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; APPROVED BY: (<?php echo $instrumentDetails['approved_by'].', '.$instrumentDetails['designation']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; SEAL :</td>
	</tr>
        <tr valign="top">
		<td width="5"><strong>Note&nbsp;:</strong></td>
                <td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="4" style="font-size: 12px;"><?php echo $s_note; ?></td>
	</tr>
</table>
</div>

<div style="page-break-after: always;"></div>
<?php

// html - end //
$content = ob_get_clean();

// convert in PDF
require_once 'html2pdf/html2pdf.class.php';
try {
//	$html2pdf = new HTML2PDF('P', 'A4', 'fr');
	$html2pdf = new HTML2PDF('P', 'A4', 'fr',true,'UTF-8',array(30, 43, 10, 14));
	// $html2pdf->setModeDebug();
	$html2pdf->setDefaultFont('Times');
        $html2pdf->AddFont('dejavusans');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output('example.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>