<?php
include("include/omConfig.php");
include ("include/functions.inc.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(GRN_MODULE_ID,"item view"))){
  $grnCount  = 0;
  $grnArray  = array();
  $msg       = "";
	$fromDate  = 0;
	$toDate    = 0;
        if(isset($_POST['smsBtn'])){
            if(isset($_POST['mylist']) && !empty($_POST['mylist'])){
                $masterQuery = "SELECT phone1 FROM customer WHERE customerId IN (". implode(',', $_POST['mylist']) .") AND phone1 IS NOT NULL GROUP BY customerId";
               $masterQueryResult = mysql_query($masterQuery);
               while($ListRow = mysql_fetch_array($masterQueryResult)){
                   if(!empty($ListRow['phone1']) && (strlen($ListRow['phone1']) >= 10)){
                       $sms = 'Dear Customer, Your instrument is getting due for calibration in this month, kindly reminded to calibrate on time. Team Krishna Instruments 0281-2381444';
                       send_sms($ListRow['phone1'], $sms);
                   }
               }
//                echo "<pre>"; print_r($_POST); exit;
            }
            
        }
	/////////////////  grnList date View:Start
	if(isset($_REQUEST['fromDateYear']))
		$fromDate  = $_REQUEST['fromDateYear']."-".$_REQUEST['fromDateMonth']."-".$_REQUEST['fromDateDay'];
	else
		$fromDate  = date("Y-m-d");
	
	if(isset($_REQUEST['toDateYear']))
		$toDate    = $_REQUEST['toDateYear']."-".$_REQUEST['toDateMonth']."-".$_REQUEST['toDateDay'];
	else
		$toDate    = date("Y-m-d");
		
	if(isset($_POST['dispatch']) && $_POST['dispatch'] != 0){
		if($_POST['dispatch'] == 1){
		  $dispatchQry = " AND grndetail.dispatch = 1 ";
		  $dispatchValue = 1;
		}else{
		  $dispatchQry = " AND grndetail.dispatch = 0 ";
		  $dispatchValue = 2;
		}
	}else{
		$dispatchQry = NULL;
		$dispatchValue = 0;
	}
        $prefix_qy = '';
        $prefix = '';
        if(empty($_REQUEST)){
            $prefix_qy = " AND grnmaster.grnPrefix = 'N' ";
            $prefix = 'N';
        } else {
            if(isset($_REQUEST['prefix']) && !empty($_REQUEST['prefix'])){
                if($_REQUEST['prefix'] == 'all'){
                    $prefix_qy = " ";
                    $prefix = 'all';
                } else {
                    $prefix = $_REQUEST['prefix'];
                    $prefix_qy = " AND grnmaster.grnPrefix = '".$_REQUEST['prefix']."' ";
                }
                
            }
        }
	/////////////////  grnList date View:Stop	
  $grnmasterQuery = "SELECT grnmaster.grnId,grnmaster.grnPrefix,grnmaster.grnNo,DATE_FORMAT(grnmaster.grnDate,'%d-%m-%y') AS grnDate,
                            grndetail.itemCode,grndetail.uuc_location,grndetail.sr_no,grndetail.itemId,DATE_FORMAT(grnmaster.grnDate,'%d-%m-%y') AS grnDueDate,item.itemId,item.itemName,
                            grnmaster.customerId,customer.customerId,grndetail.grnId,grndetail.dispatch,customer.custName,customer.custCode,grndetail.ulr_no,grndetail.ulr_inc_no,
                             (SELECT COUNT('x') FROM grndetail grndetail  WHERE grndetail.grnId = grnmaster.grnId) as count_ulr
                       FROM grnmaster
                       JOIN customer
                       JOIN grndetail
                       JOIN item
                      WHERE grnmaster.customerId = customer.customerId
                        AND grnmaster.grnId = grndetail.grnId
                        AND grndetail.itemId = item.itemId
                        AND grnmaster.grnDate >= '".$fromDate."'
                        AND grnmaster.grnDate <= '".$toDate."'
                        ".$prefix_qy."
                        ".$dispatchQry."
                      ORDER BY grnmaster.grnPrefix,grnmaster.grnNo,grndetail.ulr_no ASC";
  $grnmasterQueryResult = mysql_query($grnmasterQuery);
  while($grnListRow = mysql_fetch_array($grnmasterQueryResult))
  {
	  if(isset($grnArray[$grnCount - 1]['grnId']) 
		   && $grnArray[$grnCount - 1]['grnId']  == $grnListRow['grnId'])// && date prev and current both same
		{
		  $grnArray[$grnCount]['display'] = 0;
		}	 
		else
		{
			$grnArray[$grnCount]['display'] = 1;
		} 	    
    $grnArray[$grnCount]['grnId']      = $grnListRow['grnId'];
    $grnArray[$grnCount]['grnPrefix']  = $grnListRow['grnPrefix'];
    $grnArray[$grnCount]['grnNo']      = $grnListRow['grnNo'];
    $grnArray[$grnCount]['grnDate']    = $grnListRow['grnDate'];
    $grnArray[$grnCount]['custName']    = $grnListRow['custName'];
    $grnArray[$grnCount]['custCode']    = $grnListRow['custCode'];
    $grnArray[$grnCount]['ulr_no']    = $grnListRow['ulr_no'];
    $grnArray[$grnCount]['itemName']    = $grnListRow['itemName'];
    $grnArray[$grnCount]['uuc_location']    = $grnListRow['uuc_location'];
    $grnArray[$grnCount]['sr_no']    = $grnListRow['sr_no'];
    $grnArray[$grnCount]['itemCode']    = $grnListRow['itemCode'];
//    CC2381200 00000005 F
    $ulr_inc_no = '';
    if(!empty($grnListRow['ulr_no'])){
        $str1 = substr($grnListRow['ulr_no'], 9);
        $ulr_inc_no = substr($str1, 0, -1);
    }
    $grnArray[$grnCount]['ulr_inc_no']    = $ulr_inc_no;
    $grnArray[$grnCount]['count_ulr']    = $grnListRow['count_ulr'];
    $grnArray[$grnCount]['itemId']     = $grnListRow['itemName'];
    $sortTime = strtotime($grnListRow['grnDueDate'].'+ 1 days');
    $grnArray[$grnCount]['grnDueDate'] = date("y-m-d",$sortTime);
    $grnArray[$grnCount]['customerId'] = $grnListRow['customerId'];
    if($grnListRow['dispatch'] == 1){
      $grnArray[$grnCount]['dispatch'] = 'Yes';
    }else{
      $grnArray[$grnCount]['dispatch'] = 'No';
    }
    $grnCount++;
  }

  ///////////////// Item Name Combo : Starts
  $itemId   = "";
  $itemName = "";
  $d = 0;
//echo "<pre>"; print_r($grnArray); exit;
  $selectItem = "SELECT itemId,itemName
                   FROM item
                  ORDER BY itemName";
  $selectItemResult = mysql_query($selectItem);


  while($itemRow = mysql_fetch_array($selectItemResult))
  {
    $itemId [$d]   = $itemRow['itemId'];
    $itemName [$d] = $itemRow['itemName'];
    $d++;
  }
  $cust_names = array('Name' => 'aaa');
  ///////////////// Item Name Combo : Ends
//echo "<pre>"; print_r($grnArray); exit;
  include("./bottom.php");
  $smarty->assign("msg",$msg);
	$smarty->assign("fromDate",$fromDate);
	$smarty->assign("toDate",$toDate);  
  $smarty->assign("grnArray",$grnArray);
  $smarty->assign("grnCount",$grnCount);
  $smarty->assign("cust_names",$cust_names);
  $smarty->assign("itemId",$itemId);
  $smarty->assign("itemName",$itemName);
  $smarty->assign("dispatchValue",$dispatchValue);
  $smarty->assign("prefix",$prefix);
  $smarty->display("grnItemList.tpl");
} else {
  header("Location:index.php");
}  
}

?>