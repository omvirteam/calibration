<div style="border: 1px solid black; border-radius: 10px; padding: 5px;">
<table width="605">
    <tr>
        <td align="right" width="605"><strong><?php echo $page_title; ?></strong></td>
    </tr>
</table>
<table width="605">
	<tr>
		<td>CERTIFICATE NO.</td>
		<td >: <?php echo $certificate_no; ?></td>
		<td style="padding-left:50px;">DATE OF CALIBRATION</td>
		<td  >: <?php echo $callibrationDate; ?></td>
		
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grn_no; ?></td>
		<td style="padding-left:50px;">NEXT RECOMMENDED DATE</td>
		<td >: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td >PAGE</td>
		<td >: 1 OF <span class="total_page_no"><?php echo $max_page_no; ?></span></td>
		<td style="padding-left:50px;">CERTIFICATE ISSUE DATE</td>
		<td  >: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
		 <img height="50" width="50"  style="margin-top:-40px; margin-left:50px;"src="images/qrcode.png">
		
	</tr>
	
</table>
<hr />
<table width="605">
	<tr valign="top">
		<td width="5"><strong>1.</strong></td>
		<td width="200"><strong>CUSTOMER NAME & ADDRESS</strong></td>
		<td width="5">:</td>
		<td width="395" style="font-family: Agency FB;font-size:18px; font-weight:bold;"><?php echo $customerName; ?></td>
	</tr>
	<tr valign="top" >
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="font-size:14px; height:40px;" width="395">
                    <?php echo nl2br($customerAddress); ?>
                    <?php if(!empty($customerCity)) { ?>
                        <br/><b><?php echo $customerCity; ?></b>
                    <?php } ?>
		</td>
	</tr>
	<tr valign="top">
		<td><strong>2.</strong></td>
		<td><strong>CUSTOMER REFERENCE NO.</strong></td>
		<td>:</td>
		<td><?php echo $customerRefNo; ?></td>
	</tr>
	<tr valign="top">
		<td><strong>3.</strong></td>
		<td><strong>INSTRUMENT RECEIVED ON</strong></td>
		<td>:</td>
		<td><?php echo $grnDate; ?></td>
		<td  style="padding-left:-300px;"><strong>4.</strong></td>
		<td><strong>ULR NO.</strong></td>
		<td>:</td>
		<td><?php echo $instrumentDetails['ulr_no']; ?></td>
	</tr>
	<!-- <tr valign="top">
		<td><strong>4.</strong></td>
		<td><strong>ULR NO.</strong></td>
		<td>:</td>
		<td><?php echo $instrumentDetails['ulr_no']; ?></td>
	</tr> -->
	<tr valign="top">
		<td><strong>5.</strong></td>
		<td><strong>DESCRIPTION OF INSTRUMENT</strong></td>
		<td>:</td>
		<td>&nbsp;</td>
	</tr>
    </table>
    <table width="605">
	<tr valign="top">
		<td width="5">&nbsp;</td>
		<td width="100">Name</td>
		<td width="5">:</td>
		<td width="495"><?php echo $instrumentDetails['item_name']; ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Make/Model</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['make']; ?></td>
	</tr>
	<tr valign="top">
		<td>&nbsp;</td>
		<td>Sr. No.</td>
		<td>:</td>
		<td><?php echo $instrumentDetails['item_code']; ?></td>
		<td style="padding-left:-310px;">Instrument ID No.</td>
		<td>:</td>
                <td><?php echo !empty($instrumentDetails['id_no']) ? $instrumentDetails['id_no'] : '---'; ?></td>
	</tr>

    </table>
<table width="605" style="border-spacing:0; " cellspacing="0">
	<tr valign="top">
		<td width="5">&nbsp;</td>
		<td width="105">&nbsp;Range</td>
		<td width="5">:</td>
		<td width="185" style="font-family:dejavusans; font-size: 11px;"><?php echo $instrumentDetails['range_value']; ?></td>
                <td width="115" style="border-left: 1px solod #000000; ">&nbsp;Least Count</td>
		<td width="5">:</td>
		<td width="185" style="font-family:dejavusans; font-size: 11px;"><?php echo $all_least; ?></td>
	</tr>
	<tr valign="top">
            <td width="5">&nbsp;</td>
            <td width="105">&nbsp;Accuracy</td>
            <td width="5">:</td>
            <td width="185"><?php echo $instrumentDetails['accuracy']; ?></td>
            <td width="115" style="border-left: 1px solod #000000;">&nbsp;Condition on receipt</td>
            <td width="5">:</td>
            <td width="185"><?php echo $instrumentDetails['grn_condition']; ?></td>
	</tr>
        <tr valign="top">
            <td width="5">&nbsp;</td>
            <td width="105">&nbsp;UUC Location</td>
            <td width="5">:</td>
            <td width="185"><?php echo $instrumentDetails['uuc_location']; ?></td>
            <td width="115" style="border-left: 1px solod #000000;">&nbsp;Remark</td>
            <td width="5">:</td>
            <td width="185"><?php echo $s_page_1_note; ?></td>
	</tr>
        <?php if($grn_data[0]['table_info_id'] == '11'){ ?>
            <tr valign="top">
                <td width="5">&nbsp;</td>
                <td width="105">Flow Meter Size</td>
                <td width="5">:</td>
                <td width="185"><?php echo $flow_meter_size; ?></td>
                <td width="115" style="border-left: 1px solod #000000;">&nbsp;Line Size</td>
                <td width="5">:</td>
                <td width="185"><?php echo $line_size; ?></td>
            </tr>
        <?php } ?>
    </table>
    <?php if($grn_data[0]['table_info_id'] == '11'){ ?>
    <table width="605">
	<tr valign="top">
            <td width="5"><strong>&nbsp;</strong></td>
            <td width="160">Pipe Material/Pipe Thickness</td>
            <td width="5">:</td>
            <td width="435" style="font-family:dejavusans; font-size: 11px;"><?php echo $pipe_mate_thick; ?></td>
	</tr>
    </table>
    <?php } ?>
    <table width="605">
	<tr valign="top">
		<td width="5"><strong>6.</strong></td>
		<td width="200"><strong>AMBIENT TEMPERATURE</strong></td>
		<td width="5">:</td>
                <td width="395" style="font-family:dejavusans; font-size: 11px;"><?php echo htmlspecialchars($ambientTemperature); ?></td>
	</tr>
    </table>
    <?php if($grn_data[0]['table_info_id'] == '11'){ ?>
    <table width="605" style="border-spacing:0; " cellspacing="0">
	<tr valign="top">
            <td width="5">&nbsp;</td>
            <td width="150">RELATIVE HUMIDITY</td>
            <td width="5">:</td>
            <td width="155" style="font-family:dejavusans; font-size: 11px;"><?php echo htmlspecialchars($relativeHumidity); ?></td>
            <td width="140" style="border-left: 1px solod #000000;">&nbsp;CALIBRATION FLUID</td>
            <td width="5">:</td>
            <td width="145"><?php echo $cali_fluid; ?></td>
	</tr>
    </table>
    <?php } else { ?>
    <table width="605">
	<tr valign="top">
		<td width="5"><strong></strong></td>
		<td width="200"><strong>RELATIVE HUMIDITY</strong></td>
		<td width="5">:</td>
                <td width="395" style="font-family:dejavusans; font-size: 11px;"><?php echo htmlspecialchars($relativeHumidity); ?></td>
	</tr>
    </table>
    <?php } ?>
<table >	
	<tr valign="top">
		<td width="5"><strong>7.</strong></td>
		<td width="600"><strong>DETAILS OF REFERENCE STANDARD & MAJOR INSTRUMENTS USED</strong></td>
	</tr>
        <br/>
	<tr valign="top">
            <td width="5">&nbsp;</td>
            <td width="600">
            <?php
                    $max_rows = count($master_m_data);
                    $name_arr = array();
                    $name_arr[] = 'Name';
                    $name_arr[] = 'I.D. No.';
                    $name_arr[] = 'Make';
                    $name_arr[] = 'Model No.';
                    $name_arr[] = 'Serial no.';
                    $name_arr[] = 'Certificate No.';
                    $name_arr[] = 'Calibration Valid Up to';
                    if($max_rows <= 2){
                        $font_s = 'font-size:14px;';
                    } else {
                        $font_s = 'font-size:10px;';
                    }
//                    echo "<pre>"; print_r($master_m_data); exit;
                ?>
            <table cellpadding="0" cellspacing="0" border="1" style="<?php echo $font_s; ?>" width="100%">
                <?php for($x = 0; $x < 7; $x++) { ?>
                <tr>
                    <td align="left" style="padding: 2px;" valign="center"><b><?php echo $name_arr[$x]; ?></b></td>
                    <?php foreach ($master_m_data as $master_m){ ?>
                        <td align="left" style="padding: 2px;" valign="center"><?php echo $master_m[$x]; ?></td>
                    <?php } ?>
                </tr>
                <?php } ?>
                <tr>
                    <td align="left" style="padding: 2px;" valign="center"><b>Traceability To </b></td>
                    <td align="left" style="padding: 2px;" valign="center" colspan="<?php echo $max_rows; ?>"><?php echo $s_trac; ?></td>
                </tr>
            </table>
	</td>
	</tr>
        
</table>
<table width="605">	
	<tr valign="top">
		<td width="5"><strong>8.</strong></td>
		<td width="170"><strong>PROCEDURE</strong></td>
		<td width="5">:</td>
                <td width="425" style="font-size: 11px;"><?=  ($instrumentDetails['cali_location'] == '2') ? $master_procedureText : $master_procedureTextLab;  ?></td>
	</tr>
	<tr valign="top">
		<td width="5"><strong>9.</strong></td>
		<td width="170"><strong>CALIBRATION LOCATION</strong></td>
		<td width="5">:</td>
		<td width="425"><?=  ($instrumentDetails['cali_location'] == '2') ? 'At Site' : 'At Lab';  ?></td>
	</tr>
        <tr valign="bottom">
            <hr />
            <td width="5" style="height: 5px;"><strong></strong></td>
            <td colspan="3" style="height: 5px;" width="600" style="font-size: 20px;"></td>
	</tr>
</table>
<table width="605">
        
        <tr valign="bottom">
            <td width="5"><strong>&nbsp;</strong></td>
            <td  width="600" style="font-size: 20px;"><img src="./images/for-krishna.png" /></td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;CALIBRATED BY &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; || &nbsp;&nbsp; &nbsp; &nbsp; APPROVED BY  &nbsp;  &nbsp; &nbsp;&nbsp;  &nbsp; || &nbsp; &nbsp; &nbsp;  PRINTED BY  &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; SEAL </td>
	</tr>
	<tr valign="top">
            <td colspan="2">(<?php echo $instrumentDetails['userName']; ?>)   &nbsp; &nbsp;&nbsp;||&nbsp; &nbsp; &nbsp;  (<?php echo $instrumentDetails['approved_by'].', '.$instrumentDetails['designation']; ?>) &nbsp; &nbsp;&nbsp; || &nbsp; &nbsp;  (<?php echo $instrumentDetails['userName']; ?>)  &nbsp;  &nbsp;  &nbsp;   || </td>
	</tr>
        <tr valign="top">
		<td width="5"><strong>Note&nbsp;:</strong></td>
                <td>&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2" style="font-size: 12px;"><?php echo $s_note; ?></td>
	</tr>
</table>
</div>
