<?php
include("include/omConfig.php");

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
	if($haystack == null){
		return false;
	}else{
 foreach($haystack as $item) {
 	      if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
}
    return false;
}
// general functions - start //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// variable list - start //
$certificateNo 			= '';
$certificate_no			= '';
$grnNo 					= '';
$grn_no 					= '';
$grnDate				= ''; 
$totalPages 			= 0;
$callibrationDate		= '';
$callibrationDateNext 	= '';
$certificateIssue		= '';
$certificateIssueDate	= '';
$customerName			= '';
$customerAddress		= '';
$customerCity		= '';
$customerRefNo			= '';
$ambientTemperature		= '';
$relativeHumidity		= '';
$remarks				= '';

$counter 				= 0;
$grnDetailIds 			= array();
$tempArr 				= array() ;

$instrumentDetails					= array();
$instrumentDetails['item_name']		= '';
$instrumentDetails['make']			= '';
$instrumentDetails['item_code']		= '';
$instrumentDetails['id_no']			= '';
$instrumentDetails['range_value']	= '';
$instrumentDetails['accuracy']		= '';
$instrumentDetails['grn_condition']	= '';
$instrumentDetails['certi_remarks']	= '';
$instrumentDetails['cali_location']	= '';
$instrumentDetails['uuc_location']	= '';
$instrumentDetails['ulr_no']	= '';
$instrumentDetails['userName']	= '';
$instrumentDetails['approved_by']	= '';
$instrumentDetails['designation']	= '';
$instrumentDetails['sr_no']	= '';

$masterMeters						= array();
$masterMeters['masterMeterName']			= array();
$masterMeters['masterMeterIdNo']				= array();
$masterMeters['masterMeterMake']			= array();
$masterMeters['masterMeterModelNo']				= array();
$masterMeters['masterMeterSerialNo']			= array();
$masterMeters['masterMeterCertificateNo']		= array();
$masterMeters['due_date']	= array();

$observations						= array();
/*$observations['parameters']			= array();
$observations['table_no']			= array();
$observations['range1']				= array();
$observations['reading1']			= array();
$observations['range2']				= array();
$observations['reading2']			= array();
$observations['units']				= array();
$observations['rdg']				= array();
$observations['expanded']			= array();*/
// variable list - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// fetch data - start //
$qry = 'SELECT DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate,
	DATE_FORMAT(grndetail.nextYearDate,"%d/%m/%Y") AS nextYearDate,
	certificateIssue,DATE_FORMAT(grndetail.certificateIssueDate,"%d/%m/%Y") AS certificateIssueDate,
	grndetail.grnId,grndetail.certificate_no,grndetail.grnDetailId,
	grndetail.temperature,grndetail.humidity,itemCode,
	CONCAT("N-",grnmaster.grnNo) AS grnNo,grnmaster.grn_no,
	DATE_FORMAT(grnmaster.grnDate,"%d/%m/%Y") AS grnDate,
	customer.custName,customer.address,customer.city,
	customer.custCode AS customerRef 
	FROM grndetail 
	LEFT JOIN grnmaster ON grnmaster.grnId = grndetail.grnId 
	LEFT JOIN customer ON customer.customerId = grnmaster.customerId 
	WHERE grndetail.grnDetailId = '.$_GET['grnDetailId'];
$res = mysql_query($qry) or die("Error :: Cannot select basic details.<hr>".mysql_error());
if($row = mysql_fetch_assoc($res)) { 
	$grn_no 					= $row['grn_no'];
	$certificate_no 					= $row['certificate_no'];
	$grnDate				= $row['grnDate'];
	$callibrationDate		= $row['callibrationDate'];
	$callibrationDateNext 	= $row['nextYearDate'];
	$certificateIssue		= $row['certificateIssue'];
	$certificateIssueDate	= $row['certificateIssueDate'];
	$customerName			= $row['custName'];
	$customerAddress		= $row['address'];
	$customerCity		= $row['city'];
        $ref_no = '';
        $words = preg_split("/\s+/", trim($row['custName']));
//        echo "<pre>"; print_r($words); exit;
        $acronym = "";
        foreach ($words as $w) {
            if(strlen($acronym) == 2){
                break;
            } else {
                $acronym .= $w[0];
            }
        }
        $cust_sn = Strtoupper($acronym);
	$customerRefNo			= $cust_sn.'/Cali/'.$row['customerRef'];
	$ambientTemperature		= $row['temperature'];
	$relativeHumidity		= $row['humidity'];
	
	$tempArr = $instrumentDetails;
	// instrument details query //
	$qry1 = 'SELECT grndetail.grnDetailId,grndetail.makeModel AS make,grndetail.itemCode AS item_code,
		grndetail.instrumentId AS id_no,grndetail.rangeValue AS range_value,
		grndetail.accuracy,grndetail.grnCondition AS grn_condition,item.itemName AS item_name,
		grndetail.certiRemarks AS certi_remarks, grndetail.cali_location, grndetail.uuc_location,grndetail.ulr_no, staff.staffName as userName, grndetail.sr_no, approved_by.name as approved_by, approved_by.designation
		FROM grndetail 
		LEFT JOIN item ON item.itemId = grndetail.itemId 
		LEFT JOIN approved_by ON approved_by.id = grndetail.approved_by 
                LEFT JOIN staff ON staff.staffId = grndetail.user_id 
		WHERE grndetail.callibrationDate IS NOT NULL AND grndetail.grnId = '.$row['grnId'].' AND grndetail.grnDetailId = "'.$row['grnDetailId'].'"';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
	while($row1 = mysql_fetch_assoc($res1)) { 
		array_push($grnDetailIds,$row1['grnDetailId']);
		foreach($tempArr as $key=>$val) {
			if(!in_array_r($row1[$key],$tempArr[$key])) {
				$tempArr[$key][$counter] = $row1[$key];
				$instrumentDetails[$key] .= $row1[$key].' | ';
			}
		}
		$counter++;
	}
	
	foreach($instrumentDetails as $key=>$val) {
		if($val != '')
			$instrumentDetails[$key] = substr($val,0,-3);
	}
	mysql_free_result($res1);
	
	$tempArr = $masterMeters;
	// master meters query //
        $all_least = '';
        $all_uut = '';
        $qry2 = 'SELECT * FROM observation_data as grn
		WHERE grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].' ORDER BY grn.id ASC  ';
	$res2 = mysql_query($qry2) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $master_data = array();
//        echo "<pre>"; print_r(mysql_fetch_assoc($res1)); exit;
        while($row2 = mysql_fetch_assoc($res2)) {
                array_push($master_data,$row2);
        }
//        echo "<pre>"; print_r($master_data); exit;
        $master_meter_ids = array();
        $max_count_page = count($master_data) + 1;
        $current_page_no = '';
        $page_no = 1;
        foreach ($master_data as $m_key => $ms_data){ 
            $page_no = $page_no + 1;
            if($ms_data['table_info_id'] == '12'){
                $current_page_no = $page_no;
                break;
            }
        }
        foreach ($master_data as $m_key => $ms_data){ 
           
            $m_data = json_decode(json_encode(json_decode($ms_data['master_json'])), true);
            $u_data = json_decode(json_encode(json_decode($ms_data['uuc_json'])), true);
//             echo "<pre>"; print_r($m_data); exit;
            $all_least = $u_data[0]['leastCount'];
            $all_uut = $u_data[0]['uut_range'];
            foreach ($m_data as $s_data){ 
                $master_meter_ids[] = $s_data['masterMeterId'];
            }
            
        }
//        echo "<pre>"; print_r($master_meter_ids); exit;
	$qry2 = 'SELECT masterMeterName as "0",masterMeterIdNo as "1",masterMeterMake as "2",masterMeterModelNo as "3",masterMeterSerialNo as "4",masterMeterCertificateNo as "5",
		 DATE_FORMAT(masterMeterExp,"%d/%m/%Y") AS "6"
                 FROM mastermeter 
		 WHERE masterMeterId IN ('.implode(",",$master_meter_ids).')'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res2 = mysql_query($qry2) or die("Error :: Cannot select master meter details1.<hr>".mysql_error());
	$master_m_data = array();
        while($row2 = mysql_fetch_assoc($res2)) { 
            $master_m_data[] = $row2;
	}
//        echo "<pre>"; print_r($master_m_data); exit;
	$qry21 = 'SELECT GROUP_CONCAT(DISTINCT procedureText) as procedureText, GROUP_CONCAT(DISTINCT page_title) as page_title
                 FROM mastermeter 
		 WHERE masterMeterId IN ('.implode(",",$master_meter_ids).')'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res21 = mysql_query($qry21) or die("Error :: Cannot select master meter details2.<hr>".mysql_error());
	$master_procedureText = '';
	$page_title = '';
        while($row21 = mysql_fetch_assoc($res21)) { 
            $master_procedureText = $row21['procedureText'];
            $page_title = $row21['page_title'];
	}
//        echo "<pre>"; print_r($master_procedureText); exit;
//print '<pre>'; print_r($masterMeters);
	mysql_free_result($res2);
}
mysql_free_result($res);
// fetch data - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// html - start //
ob_start();
?>

<style>
table {font-family: "Times New Roman"; font-size: 13px;}
tr {line-height: 21px;}
</style>

<?php 
        
        $qry112 = 'SELECT * FROM setting ';
	$res112 = mysql_query($qry112) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $s_trac = '';
        $s_approve = '';
        $s_note = '';
        $s_all_note = '';
        $s_page_1_note = '';
        while($row112 = mysql_fetch_assoc($res112)) { 
            if($row112['setting_name'] == 'traceability_to'){
                $s_trac = $row112['setting_value'];
            }
            if($row112['setting_name'] == 'approved_by'){
                $s_approve = $row112['setting_value'];
            }
            if($row112['setting_name'] == 'footer_note_for_page_1'){
                $s_note = $row112['setting_value'];
            }
            if($row112['setting_name'] == 'footer_note_for_all'){
                $s_all_note = $row112['setting_value'];
            }
            if($row112['setting_name'] == 'page_1_remark'){
                $s_page_1_note = $row112['setting_value'];
            }
        }
        ?>
<style>
    tr {line-height: 21px;}
.main_table tr td {border: 1px solid black;}
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 1px solid black;
    
}
.main_table td, .main_table th {
    padding: 1px;
    
}
.main_table{
    padding-bottom: 2px;
    
}
</style>
<div style="text-align:right; padding-right: 25px;"><b><?php echo $page_title; ?></b></div>
<div style="border: 1px solid black; border-radius: 10px; padding: 5px;">
<table width="605">
	<tr>
		<td width="150">CERTIFICATE NO.</td>
		<td width="145">: <?php echo $certificate_no; ?></td>
		<td width="5">&nbsp;</td>
		<td width="200">DATE OF CALIBRATION</td>
		<td width="95" align="right">: <?php echo $callibrationDate; ?></td>
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grn_no; ?></td>
		<td>&nbsp;</td>
		<td>NEXT RECOMMENDED DATE</td>
		<td align="right">: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td width="150">PAGE</td>
		<td width="145">: <?php echo $page_no; ?> OF <span class="total_page_no"><?php echo $max_count_page; ?></span></td>
		<td width="5">&nbsp;</td>
		<td width="200">CERTIFICATE ISSUE DATE</td>
		<td width="95" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
	</tr>
</table>
<hr />
	<?php
		if(isset($_GET['grnDetailId']) && $_GET['grnDetailId'] != '') {
	?>
        <?php
        $qry1 = 'SELECT *,ti.note as table_info_note FROM observation_data as grn
                LEFT JOIN table_info as ti ON ti.table_info_id = grn.table_info_id
		WHERE grn.table_info_id = "12" AND grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].'  ORDER BY grn.table_no ASC ';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $grnobsmaster_data = array();
//        echo "<pre>"; print_r(mysql_fetch_assoc($res1)); exit;
        while($row1 = mysql_fetch_assoc($res1)) {
                array_push($grnobsmaster_data,$row1);
        }
//        echo "<pre>"; print_r($grnobsmaster_data); exit;
        ?>
        
        <table width="605">
            <tr valign="top">
                <td width="10"><strong>10.</strong></td>
		<td width="235"><strong> Observation</strong></td>
		<td width="5"></td>
		<td width="355"></td>
            </tr>
        </table>
        <?php foreach ($grnobsmaster_data as $main_key => $grn_data){ 
        $master_data = json_decode(json_encode(json_decode($grn_data['master_json'])), true);
        $uuc_data = json_decode(json_encode(json_decode($grn_data['uuc_json'])), true);
        $type_a_type_b_json = json_decode(json_encode(json_decode($grn_data['type_a_type_b_json'])), true);
        unset($grn_data['master_json']);
        unset($grn_data['uuc_json']);
        unset($grn_data['type_a_type_b_json']);
        unset($grn_data['reading_json']);
//        $grn_data[] = $type_a_type_b_json;
        $main_arr = array();
        foreach ($master_data as $key => $master_dat){
            $t_arr = array_merge($master_dat,$uuc_data[$key],$type_a_type_b_json[$key],$grn_data);
            $main_arr[] = $t_arr;
        }
        $grn_data = $main_arr;
//        echo "<pre>"; print_r($grn_data); exit;
        ?>
        <table width="605" align="left" class="main_table"  style="font-size:13px;">
                <?php
                $is_3_phase = 0;
                $table_title = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(!empty($grn_entry['table_title'])){
                        $table_title = $grn_entry['table_title'];
                        break;
                    }
                }
                $table_title_right = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                        $table_title_right = $grn_entry['table_title_right'];
                        break;
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                $t_pre_range = '';
                $t_pre_range_key = '';
                $t_pre_range_total = '';
//                echo "<pre>"; print_r($grn_data); exit; 
                ?>
                <?php 
//                    $master_unit_type1 = '';
//                    $master_range1 = '';
//                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
//                            WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
//                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
//                    while($row1 = mysql_fetch_assoc($res1)) { 
//                        $master_unit_type1 = $row1['range_unit'];
//                        $master_range1 = $row1['rangeValue'];
//                    }
//                    $uut_range1 = $grn_data[0]['uut_range'];
                    $ct_class_name = '';
                    $qryc1 = 'SELECT ct_class  FROM accuracy_class 
                    WHERE id = '.$grn_data[0]['ct_class'].' ';
                    $resc1 = mysql_query($qryc1) or die("Error :: Cannot select ct name details.<hr>".mysql_error());
                    while($rowc1 = mysql_fetch_assoc($resc1)) { 
                        $ct_class_name = $rowc1['ct_class'];
                    }
                ?>
                <tr>
                    <th align="center" valign="middle">Title:</th>
                    <th  colspan="4" align="left" valign="middle">&nbsp; <?php echo $table_title_right; ?></th>
                    <th  colspan="2" align="left" valign="middle">C.T. Class : <?php echo $ct_class_name; ?></th>
                </tr>
                <tr>
                    <th valign="middle" align="center" >Sr. No.</th>
                    <th align="center" valign="middle" >Set Burden<br/>VA / %</th>
                    <th align="center" valign="middle">Load %</th>
                    <th align="center" valign="middle">Allowed Ratio<br/>Limits ± %</th>
                    <th align="center" valign="middle">Observed Ratio<br/>Error ± %</th>
                    <th align="center" valign="middle">Allowed Phase<br/>Limits ± %</th>
                    <th align="center" valign="middle">Observed Phase<br/>Error ± %</th>
                </tr>
                <?php 
                    foreach ($grn_data as $k => $grn_entry){
                    
                    // ct class data : start //
                    $ct_class_data = array();
                    $qryc = 'SELECT *  FROM accuracy_class 
                            WHERE id = '.$grn_entry['ct_class'].' ';
                    $resc = mysql_query($qryc) or die("Error :: Cannot select ct details.<hr>".mysql_error());
                    while($rowc = mysql_fetch_assoc($resc)) { 
                        $ct_class_data = $rowc;
                    }
                    // ct class data : end //
                    if($grn_entry['ct_class'] == '1' || $grn_entry['ct_class'] == '2' || $grn_entry['ct_class'] == '3' || $grn_entry['ct_class'] == '4'){
                        $grn_entry['row_span'] = '4';
                    } else {
                        $grn_entry['row_span'] = '5';
                    }
//                    echo "<pre>"; print_r($grn_entry); exit; 
                    
                    $i = $k + 1;
//                    $master_hz = '';
//                    $master_unit_type = '';
//                    $master_range = '';
//                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
//                            WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
//                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
//                    while($row1 = mysql_fetch_assoc($res1)) { 
//                        $master_hz = $row1['frequency'];
//                        $master_unit_type = $row1['range_unit'];
//                        $master_range = $row1['rangeValue'];
//                    }
                    
                    ?>
                        <?php 
                            $decimal_master = 4;
                            $resolution_of_master = $grn_entry['resolution_of_master'];
                           if(!empty($resolution_of_master)){
                               $exp_val = explode('.', $resolution_of_master);
                               if(isset($exp_val[1])){
                                   $decimal_master = strlen($exp_val[1]);  
                               } else {
                                 $decimal_master = 0;  
                               }
                           } else {
                               $decimal_master = 4;
                           }
                           $decimal_least = 4;
                           $resolution_of_least = $grn_entry['leastCount'];
                           if(!empty($resolution_of_least)){
                               $exp_least = explode('.', $resolution_of_least);
                               if(isset($exp_least[1])){
                                   $decimal_least = strlen($exp_least[1]);  
                               } else {
                                 $decimal_least = 0;  
                               }
                           } else {
                               $decimal_least = 4;
                           }
                        ?>
                    
                        <?php 
                            $x_val = ($grn_entry['row_span'] == '4') ? '4' : '5';
//                            echo $x_val; exit;
                            for($x = 1; $x <= $x_val; $x++) { ?>
                            <tr>
                            <td align="center" valign="middle"><?php echo $x; ?></td>
                            <?php if($x == 1){ ?>
                                <td valign="middle" rowspan="<?= ($x == 1) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px; text-align: center;" ><?php echo $grn_entry['uut_range']; ?></div></td>
                            <?php } ?>
                            <?php if($x == 1){ ?>
                                <td align="center" valign="middle">120%</td>
                                <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['120_ratio_error'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg5'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['120_phase_error'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg10'], $decimal_least, '.', ''); ?></td>
                            <?php } ?>
                            <?php if($x == 2){ ?>
                                <td align="center" valign="middle">100%</td>
                                <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['100_ratio_error'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg4'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['100_phase_error'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg9'], $decimal_least, '.', ''); ?></td>
                            <?php } ?>
                            <?php if($x == 3){ ?>
                                <td align="center" valign="middle">20%</td>
                                <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['20_ratio_error'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg3'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['20_phase_error'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg8'], $decimal_least, '.', ''); ?></td>
                            <?php } ?>
                            <?php if($x == 4){ ?>
                                <td align="center" valign="middle">5%</td>
                                <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['5_ratio_error'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg2'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['5_phase_error'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg7'], $decimal_least, '.', ''); ?></td>
                            <?php } ?>
                            <?php if($x == 5){ ?>
                                <td align="center" valign="middle">1%</td>
                                <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['1_ratio_error'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$ct_class_data['1_phase_error'], $decimal_least, '.', ''); ?></td>
                                <td align="center" valign="middle"><?php echo number_format((float)$grn_entry['testMeter_avg6'], $decimal_least, '.', ''); ?></td>
                            <?php } ?>
                                </tr>
                        <?php } ?>
                    
                <?php }
                ?>
            </table>
            <br/>
            <table width="605" align="left" border="0" class=""  style="font-size:13px;">
                <tr>
                    <td width="605" align="left" valign="middle"><b>Result : </b>
                        <br/>
                        (1).The reported expanded uncertainty in measurement is stated as the standard uncertainty <br/>
                            in measurement multiplied by the coverage factor k=1.96, which for a normal distribution<br/>
                            corresponds to a coverage probability of approximately 95 %.<br/>
                        (2).For Current Transformer Phase Error and Ratio Error Calibration : (As per IS-2705 Part-2) <br/>
                        At Burrden Power Factor=0.8 Lag(Above 10 VA) <br/>
                        At Burrden Power Factor=UPF(Below 10 VA) <br/>
                        (3).Expanded Uncertainty Ratio Error (in %):  = 0.28<br/>
                    </td>
                </tr>
            </table>
	<?php } ?>
	<?php
		}
	?>
        <table width="605" align="left" border="0" class=""  style="font-size:13px;">
            <tr>
                <td width="605" align="left" valign="middle"><b>Note : </b><?php echo nl2br($grn_data[0]['table_info_note']); ?></td>
            </tr>
        </table>
<table width="605">
    <tr valign="top">
        <td width="5">&nbsp;</td>
        <td width="600">&nbsp;</td>
    </tr>
        <tr valign="top">
            <hr />
            <td width="5" style="height: 5px;"><strong></strong></td>
            <td  style="height: 5px;" width="600" style="font-size: 20px;"></td>
	</tr>
        <tr valign="bottom">
            <td width="5"><strong>&nbsp;</strong></td>
            <td width="600" style="font-size: 20px;"><img src="./images/for-krishna.png" /></td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">&nbsp;</td>
	</tr>
        <tr valign="top">
            <td colspan="2">CALIBRATED BY: (<?php echo $instrumentDetails['userName']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; APPROVED BY: (<?php echo $instrumentDetails['approved_by'].', '.$instrumentDetails['designation']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; SEAL :</td>
	</tr>
</table>
</div>


<!--<page_footer>
    <table class="page_footer" style="width: 100%;">
        <tr>
            <td style="width: 100%;">
                <?php echo $s_all_note; ?>
            </td>
        </tr>
    </table>
</page_footer>-->
<?php

// html - end //
$content = ob_get_clean();

// convert in PDF
require_once 'html2pdf/html2pdf.class.php';
try {
//	$html2pdf = new HTML2PDF('P', 'A4', 'fr');
	$html2pdf = new HTML2PDF('P', 'A4', 'fr',true,'UTF-8',array(30, 43, 10, 14));
	// $html2pdf->setModeDebug();
	$html2pdf->setDefaultFont('Times');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output('example.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>