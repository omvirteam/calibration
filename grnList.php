<?php
include("include/omConfig.php");
include ("include/functions.inc.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(GRN_MODULE_ID,"view"))){
  $grnCount  = 0;
  $grnArray  = array();
  $msg       = "";
	$fromDate  = 0;
	$toDate    = 0;
	
        if(isset($_POST['smsBtn_cali']) || isset($_POST['smsBtn_certi']) || isset($_POST['smsBtn_certi_sent']) || isset($_POST['smsBtn_ins_sent'])){
//            echo "<pre>"; print_r($_POST); exit;
            if(isset($_POST['mylist']) && !empty($_POST['mylist'])){
                $masterQuery = "SELECT phone1 FROM customer WHERE customerId IN (". implode(',', $_POST['mylist']) .") AND phone1 IS NOT NULL GROUP BY customerId";
                $masterQueryResult = mysql_query($masterQuery);
                while($ListRow = mysql_fetch_array($masterQueryResult)){
                   if(!empty($ListRow['phone1']) && (strlen($ListRow['phone1']) >= 10)){
                        if(isset($_POST['smsBtn_cali'])){
                            $sms = 'Dear customer, kindly note that the calibration of '.$_POST['all_or_partial'].' instruments has been completed, kindly arrange to collect the same. Team Krishna Instruments.';
                        }
                        if(isset($_POST['smsBtn_certi'])){
                            $sms = 'Dear customer, your certificates are ready, kindly arrange to collect them. Team Krishna Instruments';
                        }
                        if(isset($_POST['smsBtn_ins_sent'])){
//                            $cou_name1_ins = str_replace('Courier', '', $_POST['courier_name_ins']);
//                            $cou_name_ins = str_replace('courier', '', $cou_name1_ins);
                            $sms = 'Dear customer, your instrument has been sent through '.$_POST['courier_name_ins'].'. Thank you. Team Krishna Instruments';
                        }
                        if(isset($_POST['smsBtn_certi_sent'])){
//                            $cou_name1 = str_replace('Courier', '', $_POST['courier_name']);
//                            $cou_name = str_replace('courier', '', $cou_name1);
                            $sms = 'Dear customer, your certificates has been sent through '.$_POST['courier_name'].'. Thank you. Team Krishna Instruments https://www.facebook.com/krishnainstruments';
                        }
                       send_sms($ListRow['phone1'], $sms);
//                       echo $sms; exit;
                    }
                }
//                echo "<pre>"; print_r($_POST); exit;
            }
        }
        /////////////////  grnList date View:Start
	if(isset($_REQUEST['fromDateYear']))
		$fromDate  = $_REQUEST['fromDateYear']."-".$_REQUEST['fromDateMonth']."-".$_REQUEST['fromDateDay'];
	else
		$fromDate  = '2007-01-01';
	
	if(isset($_REQUEST['toDateYear']))
		$toDate    = $_REQUEST['toDateYear']."-".$_REQUEST['toDateMonth']."-".$_REQUEST['toDateDay'];
	else
		$toDate    = date("Y-m-d");
	/////////////////  grnList date View:Stop	
        $prefix_qy = '';
        $prefix = '';
        if(empty($_REQUEST)){
            $prefix_qy = " AND grnmaster.grnPrefix = 'N' ";
            $prefix = 'N';
        } else {
            if(isset($_REQUEST['prefix']) && !empty($_REQUEST['prefix'])){
                if($_REQUEST['prefix'] == 'all'){
                    $prefix_qy = " ";
                    $prefix = 'all';
                } else {
                    $prefix = $_REQUEST['prefix'];
                    $prefix_qy = " AND grnmaster.grnPrefix = '".$_REQUEST['prefix']."' ";
                }
                
            }
        }
//        echo "<pre>"; print_r($_REQUEST); exit;
 $grnmasterQuery = "SELECT grnId,grnPrefix,grnNo,grnmaster.grnDate AS gDate, DATE_FORMAT(grnmaster.grnDate,'%d-%m-%y') AS grnDate,poNo,
                            DATE_FORMAT(grnmaster.poDate,'%d-%m-%y') AS poDate,custName,custCode,grnmaster.contPerson,phNo,
                            grnmaster.customerId,customer.customerId,
                            (SELECT `short_name` FROM `item_type` WHERE `item_type`.`id` = `grnmaster`.`item_type_id`) AS `short_name`
                       FROM grnmaster
                       JOIN customer
                      WHERE grnmaster.customerId=customer.customerId
                        AND grnmaster.grnDate >= '".$fromDate."'
                        AND grnmaster.grnDate <= '".$toDate."'
                        ".$prefix_qy."
                      ORDER BY cast(substr(grnNo,1) AS UNSIGNED)"; //grnId DESC";
  $grnmasterQueryResult = mysql_query($grnmasterQuery);
  
  while($grnListRow = mysql_fetch_array($grnmasterQueryResult))
  {
    $grnArray[$grnCount]['grnId']      = $grnListRow['grnId'];
    $grnArray[$grnCount]['grnPrefix']  = $grnListRow['grnPrefix'];
    $grnArray[$grnCount]['grnNo']      = $grnListRow['grnNo'];
    $grnArray[$grnCount]['grnDate']    = $grnListRow['grnDate'];
    $grnArray[$grnCount]['poNo']       = $grnListRow['poNo'];
    $grnArray[$grnCount]['poDate']     = $grnListRow['poDate'];
    $grnArray[$grnCount]['custName']   = $grnListRow['custName'];
    $grnArray[$grnCount]['custCode']   = $grnListRow['custCode'];
    $grnArray[$grnCount]['contPerson'] = $grnListRow['contPerson'];
    $grnArray[$grnCount]['phNo']       = $grnListRow['phNo'];
    $grnArray[$grnCount]['customerId'] = $grnListRow['customerId'];
    $grnArray[$grnCount]['short_name'] = $grnListRow['short_name'];
    $grnCount++;
  }

  include("./bottom.php");
  $smarty->assign("msg",$msg);
	$smarty->assign("fromDate",'2007-01-01');
	$smarty->assign("toDate",$toDate);  
  $smarty->assign("grnArray",$grnArray);
  $smarty->assign("grnCount",$grnCount);
  $smarty->assign("prefix",$prefix);
  $smarty->display("grnList.tpl");
} else {
  header("Location:index.php");
}  
}

?>