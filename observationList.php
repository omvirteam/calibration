<?php
include("include/omConfig.php");
$grnId           = "";
$grnPrefixNo     = "";

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(OBS_MODULE_ID,"view"))){
  $selectGrn = "SELECT grn_no,grnPrefix,grnNo,grnId,grnDate
                  FROM grnmaster
                 WHERE grnId IN (SELECT grn_id FROM observation_data)
                 ORDER BY grnId * 1 DESC";
  $selectGrnResult = mysql_query($selectGrn);
  $i = 0;
  while($selectGrnRow = mysql_fetch_array($selectGrnResult))
  {
      $grnId[$i]       = $selectGrnRow['grnId'];
      $grnPrefixNo[$i] = $selectGrnRow['grn_no'];
      $i++;
  }
include("./bottom.php");

$smarty->assign("grnId",$grnId);
$smarty->assign("grnPrefixNo",$grnPrefixNo);
$smarty->display("observationList.tpl");
} else {
  header("Location:index.php");
}  
}

?>