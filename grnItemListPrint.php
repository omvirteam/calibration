<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnCount  = 0;
  $grnArray  = array();
  $msg       = "";
	$fromDate  = 0;
	$toDate    = 0;
	
	/////////////////  grnList date View:Start
	if(isset($_REQUEST['fromDateYear']))
		$fromDate  = $_REQUEST['fromDateYear']."-".$_REQUEST['fromDateMonth']."-".$_REQUEST['fromDateDay'];
	else
		$fromDate  = date("Y-m-d");
	
	if(isset($_REQUEST['toDateYear']))
		$toDate    = $_REQUEST['toDateYear']."-".$_REQUEST['toDateMonth']."-".$_REQUEST['toDateDay'];
	else
		$toDate    = date("Y-m-d");

	if(isset($_POST['dispatch']) && $_POST['dispatch'] != 0){
		if($_POST['dispatch'] == 1)
		  $dispatchQry = " AND grndetail.dispatch = 1 ";
		else
		  $dispatchQry = " AND grndetail.dispatch = 0 ";
	}else{
		$dispatchQry = NULL;
	}
	/////////////////  grnList date View:Stop	
  $grnmasterQuery = "SELECT grnmaster.grnId,grnmaster.grnPrefix,grnmaster.grnNo,DATE_FORMAT(grnmaster.grnDate,'%d-%m-%y') AS grnDate,
                            grndetail.itemId,DATE_FORMAT(grnmaster.grnDate,'%d-%m-%y') AS grnDueDate,item.itemId,item.itemName,
                            grnmaster.customerId,customer.customerId,grndetail.grnId,grndetail.dispatch
                       FROM grnmaster
                       JOIN customer
                       JOIN grndetail
                       JOIN item
                      WHERE grnmaster.customerId = customer.customerId
                        AND grnmaster.grnId = grndetail.grnId
                        AND grndetail.itemId = item.itemId
                        AND grnmaster.grnDate >= '".$fromDate."'
                        AND grnmaster.grnDate <= '".$toDate."'
                        ".$dispatchQry."
                      ORDER BY grnDate,grnmaster.grnId";
  $grnmasterQueryResult = mysql_query($grnmasterQuery);
  while($grnListRow = mysql_fetch_array($grnmasterQueryResult))
  {
	  if(isset($grnArray[$grnCount - 1]['grnId']) 
		   && $grnArray[$grnCount - 1]['grnId']  == $grnListRow['grnId'])// && date prev and current both same
		{
		  $grnArray[$grnCount]['display'] = 0;
		}	 
		else
		{
			$grnArray[$grnCount]['display'] = 1;
		} 	    
    $grnArray[$grnCount]['grnId']      = $grnListRow['grnId'];
    $grnArray[$grnCount]['grnPrefix']  = $grnListRow['grnPrefix'];
    $grnArray[$grnCount]['grnNo']      = $grnListRow['grnNo'];
    $grnArray[$grnCount]['grnDate']    = $grnListRow['grnDate'];
    $grnArray[$grnCount]['itemId']     = $grnListRow['itemName'];
    $sortTime = strtotime($grnListRow['grnDueDate'].'+ 1 days');
    $grnArray[$grnCount]['grnDueDate'] = date("y-m-d",$sortTime);
    $grnArray[$grnCount]['customerId'] = $grnListRow['customerId'];
    if($grnListRow['dispatch'] == 1){
      $grnArray[$grnCount]['dispatch'] = 'Yes';
    }else{
      $grnArray[$grnCount]['dispatch'] = 'No';
    }
    $grnCount++;
  }

  ///////////////// Item Name Combo : Starts
  $itemId   = "";
  $itemName = "";
  $d = 0;

  $selectItem = "SELECT itemId,itemName
                   FROM item
                  ORDER BY itemName";
  $selectItemResult = mysql_query($selectItem);


  while($itemRow = mysql_fetch_array($selectItemResult))
  {
    $itemId [$d]   = $itemRow['itemId'];
    $itemName [$d] = $itemRow['itemName'];
    $d++;
  }
  ///////////////// Item Name Combo : Ends

  include("./bottom.php");
  $smarty->assign("msg",$msg);
	$smarty->assign("fromDate",$fromDate);
	$smarty->assign("toDate",$toDate);  
  $smarty->assign("grnArray",$grnArray);
  $smarty->assign("grnCount",$grnCount);
  $smarty->assign("itemId",$itemId);
  $smarty->assign("itemName",$itemName);
  $smarty->display("grnItemListPrint.tpl");
}
?>