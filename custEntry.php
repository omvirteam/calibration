<?php
include("include/omConfig.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(MASTER_CUSTOMER_MODULE_ID,"add")) || (have_access_role(MASTER_CUSTOMER_MODULE_ID,"edit"))){
  $customerId         = "";
  $custName           = "";
  $custCode           = "";
  $address            = "";
  $city               = "";
  $state              = "";
  $country            = "";
  $fax                = "";
  $phone1             = "";
  $phone2             = "";
  $email              = "";
  $web                = "";
  $contPerson         = "";
  $custNameSelected   = "";
  $custCodeSelected   = "";
  $addressSelected    = "";
  $citySelected       = "";
  $stateSelected      = "";
  $countrySelected    = "";
  $faxSelected        = "";
  $phone1Selected     = "";
  $phone2Selected     = "";
  $emailSelected      = "";
  $webSelected        = "";
  $contPersonSelected = "";
  $custRefNameSelected = "";
  $customerIdSelected = isset($_REQUEST['customerIdSelected']) ? $_REQUEST['customerIdSelected'] : 0;
  
  if(isset($_POST['custName']))
  {
    if(isset($_POST['searchBtn']))
    {
      header("Location: custList.php");
      exit();
    }  
    else if(isset($_POST['cancelBtn']))
    {
      header("Location: index.php"); 
      exit();
    }
    if(($_POST['customerIdSelected']) > 0)
    {
      $updateCustDetail = "UPDATE customer
                              SET custName = '".$_POST['custName']."',custCode = '".$_POST['custCode']."',
                                  address = '".$_POST['address']."',city = '".$_POST['city']."',state = '".$_POST['state']."',
                                  country = '".$_POST['country']."',fax = '".$_POST['fax']."',phone1 = '".$_POST['phone1']."',
                                  phone2 = '".$_POST['phone2']."',email = '".$_POST['email']."',web = '".$_POST['web']."',
                                  contPerson = '".$_POST['contPerson']."',custRefName = '".$_POST['custRefName']."'
                            WHERE customerId = ".$_POST['customerIdSelected'];
      $updateCustDetailResult = mysql_query($updateCustDetail);
      if(!$updateCustDetailResult)
      die("Insert Query Not Inserted 1: ".mysql_error(). " : ".$updateCustDetail);   
    }
    else
    {
      $insertcustomerdetail = "INSERT INTO customer (custName,custCode,address,city,state,country,fax,phone1,phone2,email,web,contPerson,custRefName)
                                VALUES('".$_POST['custName']."','".$_POST['custCode']."','".$_POST['address']."','".$_POST['city']."','".$_POST['state']."',
                                      '".$_POST['country']."','".$_POST['fax']."','".$_POST['phone1']."','".$_POST['phone2']."','".$_POST['email']."',
                                      '".$_POST['web']."','".$_POST['contPerson']."','".$_POST['custRefName']."')";
      $insertcustomerdetailResult = mysql_query($insertcustomerdetail);
      if (!$insertcustomerdetailResult) {
        die("Insert Query Not Inserted : " . mysql_error());
      }
    }
  }
  /////// Edit of Customer List : Starts
  $customerId = "";
  $customerIdSelected = isset($_GET['customerId']) ? $_GET['customerId'] : 0;
  $msg = "";

  if(isset ($_GET['customerId']))
  {
    $selectCustList = "SELECT custName,custCode,address,city,state,country,fax,phone1,phone2,email,web,contPerson,custRefName
                         FROM customer
                        WHERE customerId = ".$_GET['customerId'];
    $selectCustListResult = mysql_query($selectCustList);
    $custlistrow = mysql_num_rows($selectCustListResult);
    if($custlistrow > 0)
    {
      while ($custlistrow = mysql_fetch_array($selectCustListResult))
      {
        $custNameSelected   = $custlistrow['custName'];
        $custCodeSelected   = $custlistrow['custCode'];
        $addressSelected    = $custlistrow['address'];
        $citySelected       = $custlistrow['city'];
        $stateSelected      = $custlistrow['state'];
        $countrySelected    = $custlistrow['country'];
        $faxSelected        = $custlistrow['fax'];
        $phone1Selected     = $custlistrow['phone1'];
        $phone2Selected     = $custlistrow['phone2'];
        $emailSelected      = $custlistrow['email'];
        $webSelected        = $custlistrow['web'];
        $contPersonSelected = $custlistrow['contPerson'];
        $custRefNameSelected = $custlistrow['custRefName'];
      }
    }
    else
    {
      $msg = '<tr><td align="center" colspan="22"> <h1><font color="red"><b>Record Not Found...!</b></h1></font></td></tr>';
    }
  }
/////// Edit of Customer List : Ends
  include("./bottom.php");
  $smarty->assign("custName",$custName);
  $smarty->assign("custCode",$custCode);
  $smarty->assign("address",$address);
  $smarty->assign("city",$city);
  $smarty->assign("state",$state);
  $smarty->assign("country",$country);
  $smarty->assign("fax",$fax);
  $smarty->assign("phone1",$phone1);
  $smarty->assign("phone2",$phone2);
  $smarty->assign("email",$email);
  $smarty->assign("web",$web);
  $smarty->assign("contPerson",$contPerson);
  $smarty->assign("customerId",$customerId);
  $smarty->assign("msg",$msg);
  $smarty->assign("customerIdSelected",$customerIdSelected);
  $smarty->assign("custNameSelected",$custNameSelected);
  $smarty->assign("custCodeSelected",$custCodeSelected);
  $smarty->assign("addressSelected",$addressSelected);
  $smarty->assign("citySelected",$citySelected);
  $smarty->assign("stateSelected",$stateSelected);
  $smarty->assign("countrySelected",$countrySelected);
  $smarty->assign("faxSelected",$faxSelected);
  $smarty->assign("phone1Selected",$phone1Selected);
  $smarty->assign("phone2Selected",$phone2Selected);
  $smarty->assign("emailSelected",$emailSelected);
  $smarty->assign("webSelected",$webSelected);
  $smarty->assign("contPersonSelected",$contPersonSelected);
  $smarty->assign("custRefNameSelected",$custRefNameSelected);
  $smarty->display("custEntry.tpl");
} else {
  header("Location:index.php");
}  
}

?>