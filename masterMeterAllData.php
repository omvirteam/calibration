<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
	$masterMeterSubId = isset($_POST['masterMeterSubId']) && $_POST['masterMeterSubId'] > 0 ? $_POST['masterMeterSubId'] : 0;
	if($masterMeterSubId > 0)
	{
		$addQuery = " AND masterMeterSubId = ".$masterMeterSubId." ";
	}
	else
	{
		$addQuery = NULL;
	}
	
	
	$meterSubSubData = array();
	$a = 0;
	$selSubData = "SELECT masterMeterSubId, parameter
	                    FROM mastermetersub
	                   WHERE masterMeterId = ".$_POST['masterMeterId']."
	                   $addQuery ";
  $selSubDataRes = mysql_query($selSubData);
  while($paraRow = mysql_fetch_array($selSubDataRes))
  {
    $selSubSubData = "SELECT *
  	                    FROM mastermetersubsub
  	                   WHERE masterMeterSubId = ".$paraRow['masterMeterSubId'];
  	
  	$selSubSubDataRes = mysql_query($selSubSubData);
  	while($subSubRow = mysql_fetch_array($selSubSubDataRes))
  	{
  		$meterSubSubData[$a]['parameterName']       = $paraRow['parameter'];
  		$meterSubSubData[$a]['masterMeterSubSubId'] = $subSubRow['masterMeterSubSubId'];
  		$meterSubSubData[$a]['range']               = $subSubRow['rangeValue'];
  		$meterSubSubData[$a]['range_disp']               = $subSubRow['range_disp'];
  		$meterSubSubData[$a]['accuracySubSubRdg']   = $subSubRow['accuracySubSub'];
  		$meterSubSubData[$a]['accuracySubSubFS']    = $subSubRow['accuracySubSubFS'];
  		$meterSubSubData[$a]['accuracy_main']       = $subSubRow['accuracy_main'];
  		$meterSubSubData[$a]['range_unit']          = $subSubRow['range_unit'];
  		$meterSubSubData[$a]['frequency']           = $subSubRow['frequency'];
  		$meterSubSubData[$a]['resolution']          = $subSubRow['resolution'];
  		$meterSubSubData[$a]['cmc']           = $subSubRow['cmc'];
  		$meterSubSubData[$a]['stability']           = $subSubRow['stability'];
  		$meterSubSubData[$a]['uncertinty']          = $subSubRow['uncertinty'];
  		$meterSubSubData[$a]['degreeOfFreedom']       = $subSubRow['degreeOfFreedom'];
  		$a++;
  	}
  }
	
  include("./bottom.php");
  $smarty->assign("meterSubSubData",$meterSubSubData);
  $smarty->display("masterMeterAllData.tpl");
}
?> 