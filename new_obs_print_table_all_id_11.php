<table width="605" align="left" class="main_table"  style="font-size:13px;">
                <?php
                $is_3_phase = 0;
                $table_title = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(!empty($grn_entry['table_title'])){
                        $table_title = $grn_entry['table_title'];
                        break;
                    }
                }
                $table_title_right = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                        $table_title_right = $grn_entry['table_title_right'];
                        break;
                    }
                }
                $start_time = '';
                $stop_time = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['start_time']) && !empty($grn_entry['start_time'])){
                        $start_time = $grn_entry['start_time'];
                        $stop_time = isset($grn_entry['stop_time']) ? $grn_entry['stop_time'] : '';
                        break;
                    }
                }
                $pre_range = '';
                $pre_range_key = '';
                $pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(empty($pre_range)){
                        $pre_range = $grn_entry['mRangeId'];
                        $pre_range_key = $k;
                        $pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span'] = 1;
                        }
                    } else {
                        if($pre_range == $grn_entry['mRangeId']){
                            $pre_range_total = $pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        } else {
                            $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                $t_pre_range = '';
                $t_pre_range_key = '';
                $t_pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    $uuc_range_print_table = isset($grn_entry['uuc_range_print_table']) ? $grn_entry['uuc_range_print_table'] : '';
                    if($k == 0){
                        
                        $t_pre_range = $uuc_range_print_table;
                        $t_pre_range_key = $k;
                        $t_pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span_test'] = 1;
                        }
                    } else {
                        if($t_pre_range == $uuc_range_print_table){
                            $t_pre_range_total = $t_pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        } else {
                            $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            $t_pre_range = $uuc_range_print_table;
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                if(!empty($is_3_phase)){
                    $col_span1 = '8';
                } else {
                    $col_span1 = '7';
                }
                if(!empty($view_uncertainity)){
                    $col_span1 = $col_span1 + 1;
                }
                ?>
                <?php 
                    $master_unit_type1 = '';
                    $master_range1 = '';
                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                            WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    while($row1 = mysql_fetch_assoc($res1)) { 
                        $master_unit_type1 = $row1['range_unit'];
                        $master_range1 = $row1['rangeValue'];
                    }
                    $uuc_range_print_table1 = isset($grn_data[0]['uuc_range_print_table']) ? $grn_data[0]['uuc_range_print_table'] : '';
                ?>
                <tr>
                    <th align="center" valign="middle">Title:</th>
                    <th  colspan="<?= $col_span1; ?>" align="left" valign="middle" style="font-family:dejavusansb; font-size: 11px;">&nbsp; <?php echo $table_title_right; ?></th>
                </tr>
                <tr>
                    <th  colspan="4" align="left" valign="middle">Started Time : <?php echo $start_time; ?></th>
                    <th  colspan="4" align="left" valign="middle">Stopped Time : <?php echo $stop_time; ?></th>
                    <?php if(!empty($view_uncertainity)){ ?>
                        <th align="center" valign="middle"></th>
                    <?php } ?>
                </tr>
                <tr>
                    <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                    <th align="center" valign="middle" rowspan="2">Temp. of <br/>Water in °C</th>
                    <th colspan="2" align="center" valign="middle">Calibration Standard</th>
                    <th align="center" valign="middle" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                    <th align="center" colspan="2" valign="middle">Error</th>
                    <?php if(!empty($view_uncertainity)){ ?>
                        <th align="center" rowspan="2" valign="middle">Expanded <br/>Uncertainty(%)</th>
                    <?php } ?>
                </tr>
                <tr>
                    <th align="center" valign="middle" width="80px">Sensor Range</th>
                    <th align="center" valign="middle">Flow Rate</th>
                    <th align="center" valign="middle" width="80px">Flow meter Range</th>
                    <th align="center" valign="middle">Flow Rate</th>
                    <th align="center" valign="middle">Unit(+/-)</th>
                    <th align="center" valign="middle">% Error</th>
                </tr>
                <?php 
                    $total_test_meter_val = 0;
                    $total_std_meter_val = 0;
                    $uut_y_vals = array();
                    $std_x_vals = array();
                    ?>
                    <tr>
                        <td align="center" valign="middle">1</td>
                        <td align="center" valign="middle">2</td>
                        <td align="center" valign="middle">3</td>
                        <td align="center" valign="middle">4</td>
                        <td align="center" valign="middle">5</td>
                        <td align="center" valign="middle">6</td>
                        <td align="center" valign="middle">7</td>
                        <td align="center" valign="middle">8</td>
                        <?php if(!empty($view_uncertainity)){ ?>
                            <td valign="middle" align="center">9</td>
                        <?php } ?>
                    </tr>
                        
                    <?php 
                    foreach ($grn_data as $k => $grn_entry){
                    
                        
                    $i = $k + 1;
                    
                    $master_hz = '';
                    $master_unit_type = '';
                    $master_range = '';
                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                            WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    while($row1 = mysql_fetch_assoc($res1)) { 
                        $master_hz = $row1['frequency'];
                        $master_unit_type = $row1['range_unit'];
                        $master_range = $row1['range_disp'];
                    }
                    
                    ?>
                    <tr>
                        <td align="center" valign="middle"><?php echo $i; ?></td>
                        <td align="center" valign="middle"  ><?php echo $grn_entry['temp_of_water'] ?></td>
                        <?php if(isset($grn_entry['row_span'])){ ?>
                        <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px; text-align: center; font-family:dejavusans; font-size: 11px;" ><?php echo $master_range.' '.$master_unit_type; ?></div></td>
                        <?php } ?>
                        
                        <?php 
                            $test_meter_val = $grn_entry['testMeterAverage'];
                            $std_meter_val = $grn_entry['stdMeterAverage'];
                            $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                            $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                            $decimal_master = 0;
                            $resolution_of_master = $grn_entry['resolution_of_master'];
                           if(!empty($resolution_of_master)){
                               $exp_val = explode('.', $resolution_of_master);
                               if(isset($exp_val[1])){
                                   $decimal_master = strlen($exp_val[1]);  
                               } else {
                                 $decimal_master = 0;  
                               }
                           } else {
                               $decimal_master = 0;
                           }
                           $decimal_least = 0;
                           $resolution_of_least = $grn_entry['leastCount'];
                           if(!empty($resolution_of_least)){
                               $exp_least = explode('.', $resolution_of_least);
                               if(isset($exp_least[1])){
                                   $decimal_least = strlen($exp_least[1]);  
                               } else {
                                 $decimal_least = 0;  
                               }
                           } else {
                               $decimal_least = 0;
                           }
                        ?>
                        <td valign="middle" align="center"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                        <?php $std_x_vals[] = number_format((float)$test_meter_val, $decimal_master, '.', ''); ?>
                        <?php if(isset($grn_entry['row_span_test'])){ ?>
                        <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px; text-align: center; font-family:dejavusans; font-size: 11px;" ><?php echo isset($grn_entry['uuc_range_print_table']) ? $grn_entry['uuc_range_print_table'] : ''; ?></div></td>
                        <?php } ?>
                        <td valign="middle" align="center"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                        <?php $uut_y_vals[] = number_format((float)$std_meter_val, $decimal_least, '.', ''); ?>
                        <?php $units = $test_meter_val - $std_meter_val;  ?>
                        <td valign="middle" align="center"><?php echo number_format((float)$units, $decimal_master, '.', ''); ?></td>
                        <?php $per = 100 * ($test_meter_val - $std_meter_val) / $test_meter_val;  ?>
                        <td valign="middle" align="center"><?php echo number_format((float)$per, 3, '.', ''); ?></td>
                        <?php if(!empty($view_uncertainity)){ ?>
                            <td valign="middle" align="center">
                                <?php 
                                //echo number_format((float)$max_ex_unce_p, 2, '.', ''); 
                                // echo isset($type_a_type_b_json[$k]['exp_uncer_per_ue_without_cmc']) ? sprintf("%.2f",$type_a_type_b_json[$k]['exp_uncer_per_ue_without_cmc']) : '';
                                echo isset($type_a_type_b_json[$k]['exp_uncer_per_ue']) ? sprintf("%.4f",$type_a_type_b_json[$k]['exp_uncer_per_ue']) : '';
                                ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php }
                ?>
                <?php if(empty($view_uncertainity)){ ?>
                    <tr>
                        <td valign="middle" align="center" colspan="3">Max Expanded Uncert. in %</td>
                        <td valign="middle" align="center"><?php echo number_format((float)$max_ex_unce_p, 2, '.', ''); ?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                <?php } ?>
            </table>
            <table width="605">
                <tr valign="bottom">
                     <?php 
//                    echo "<pre>"; print_r($uut_y_vals);
//                    echo "<pre>"; print_r($std_x_vals); exit;
                     
                    require_once ('./jpgraph-4.3.4/src/jpgraph.php');
                    require_once ('./jpgraph-4.3.4/src/jpgraph_scatter.php');
//                    https://jpgraph.net/download/manuals/classref/
                    $datax = $std_x_vals;
                    $datay = $uut_y_vals;
                    $graph = new Graph(500,230);
                    $graph->SetScale("linlin");
                    $graph->SetFrame(true,'black',0);
                    $graph->img->SetMargin(50,30,10,30); //left,right,top,bottom 
                    $graph->SetShadow(true,0);
                    $graph->title->Set("Chart");
                    $graph->title->SetAlign('center');
//                    $graph->title->SetMargin(10);

//                    $graph->title->SetTitleMargin(29);
                    $graph->title->SetFont(FF_FONT1,FS_BOLD);
                    
                    $graph->xaxis->SetTitleSide(SIDE_LEFT);
                    $graph->xaxis->SetTitle("Flow Rate Std. (".$master_unit_type.")","center");
                    $graph->xaxis->title->SetColor('black');
//                    $graph->xaxis->SetLabelAngle(45);
                    $graph->xaxis->SetTitleMargin(10);
                    $graph->xaxis->SetWeight(2);
                    $graph->xaxis->SetLabelMargin(5);
                    $graph->xaxis->SetColor('black');
                    $graph->xaxis->font_size = '7.2';
                    
                    $graph->xgrid->SetColor('#8C8C8C');
                    $graph->xgrid->SetLineStyle(true,'solid');
                    $graph->xgrid->SetFill(false);
                    $graph->xgrid->Show();
                    
                    $graph->yaxis->SetTitleSide(SIDE_LEFT);
                    $graph->yaxis->SetTitle("Flow Rate UUT (".$master_unit_type.")","center");
                    $graph->yaxis->title->SetColor('black');
                    $graph->yaxis->SetWeight(2);
                    $graph->yaxis->SetLabelMargin(2);
                    $graph->yaxis->SetColor('black');
                    $graph->yaxis->font_size = '7.2';
                    $graph->yaxis->SetTitleMargin(29);
                    
                    $graph->ygrid->SetColor('#8C8C8C');
                    $graph->ygrid->SetLineStyle(true,'solid');
                    $graph->ygrid->SetFill(false);
                    $graph->ygrid->Show();
                    
                    $sp1 = new ScatterPlot($datay,$datax);
                    $sp1->mark->SetWidth(4);
                    $sp1->link->Show();
                    $sp1->link->SetWeight(2);
                    $sp1->link->SetColor('black@0.1');
                    $graph->Add($sp1);
                    $graph->Stroke('./images/line_chart15.png');
                     
//                    include("./pChart.1.27d/pChart/pData.class");  
//                    include("./pChart.1.27d/pChart/pChart.class");  
//                    http://pchart.sourceforge.net/documentation.php?topic=pChart
                    // Dataset definition   
//                    $DataSet = new pData();  
//                    $DataSet->AddPoint($uut_y_vals,"Serie1");  
//                    $DataSet->AddPoint($std_x_vals,"Serie2");  
//                    $DataSet->AddSerie("Serie1");  
//                    $DataSet->SetAbsciseLabelSerie("Serie2");  
//                    $DataSet->SetSerieName("SourceForge Rank","Serie1");  
//                    // Initialise the graph  
//                    $Test = new pChart(660,230);  
//                    $Test->drawGraphAreaGradient(90,90,90,90,TARGET_BACKGROUND);  
//                    // Prepare the graph area  
//                    $Test->setFontProperties("./pChart.1.27d/Fonts/tahoma.ttf",8);  
//                    $Test->setGraphArea(60,40,595,190);  
//                    // Initialise graph area  
//                    $Test->setFontProperties("./pChart.1.27d/Fonts/tahoma.ttf",8);  
//                    // Draw the SourceForge Rank graph  
//                    $DataSet->SetYAxisName("Sourceforge Rank");  
//                    $Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_NORMAL,213,217,221,TRUE,0,0);  
//                    $Test->drawGraphAreaGradient(40,40,40,-50);  
//                    $Test->drawGrid(4,TRUE,230,230,230,10);  
//                    $Test->setShadowProperties(3,3,0,0,0,30,4);  
//                    $Test->drawCubicCurve($DataSet->GetData(),$DataSet->GetDataDescription());  
//                    $Test->clearShadow();  
//                    $Test->drawFilledCubicCurve($DataSet->GetData(),$DataSet->GetDataDescription(),.1,30);  
//                    $Test->drawPlotGraph($DataSet->GetData(),$DataSet->GetDataDescription(),3,2,255,255,255);  
//                    // Clear the scale  
//                    $Test->clearScale();  
//                    // Write the legend (box less)  
//                    $Test->setFontProperties("./pChart.1.27d/Fonts/tahoma.ttf",8);  
//                    $Test->drawLegend(530,5,$DataSet->GetDataDescription(),0,0,0,0,0,0,255,255,255,FALSE);  
//                    // Write the title  
//                    $Test->drawTitle(0,0,"SourceForge ranking summary",255,255,255,660,30,TRUE);  
//                    $Test->clearShadow();  

//                    // Compute the points  
//                    $DataSet->AddPoint($uut_y_vals,"Serie1");  
//                    $DataSet->AddPoint($std_x_vals,"Serie2");  
//                    $DataSet->SetSerieName("Chart","Serie1");  
//                    $DataSet->AddSerie("Serie1");  
//                    $DataSet->AddSerie("Serie2");  
//                    $DataSet->SetXAxisName("Flow Rate Std. (".$master_unit_type.")");  
//                    $DataSet->SetYAxisName("Flow Rate UUT (".$master_unit_type.")");  
//                    // Initialise the graph  
//                    $Test = new pChart(500,250); // For image Height and Width 
//                    $Test->drawGraphAreaGradient(255,255,255,-100,TARGET_BACKGROUND);  // For Image Full BG
//                    // Prepare the graph area  
//                    $Test->setFontProperties("./pChart.1.27d/Fonts/tahoma.ttf",8);  
//                    $Test->setGraphArea(55,30,380,200);  
////                    $Test->setFixedScale(200,800,6);
//                    $Test->drawXYScale($DataSet->GetData(),$DataSet->GetDataDescription(),"Serie1","Serie2",0,0,0,TRUE,45);  
//                    $Test->drawGraphArea(0,0,0,FALSE);  
//                    $Test->drawGraphAreaGradient(255,255,255,-50);  
//                    $Test->drawGrid(2,TRUE,150,150,150);  
////                    $Test->drawGrid(4,TRUE,230,230,230,10);  
//                    // Draw the chart  
//                    $Test->setShadowProperties(1,0,0,0,0,250,2);  
//                    $Test->drawXYGraph($DataSet->GetData(),$DataSet->GetDataDescription(),"Serie1","Serie2",0);  
//                    $Test->clearShadow();  
//                    // Draw the legend  
//                    $Test->setFontProperties("./pChart.1.27d/Fonts/pf_arma_five.ttf",6);  
//                    $DataSet->RemoveSerie("Serie2");  
//                    $Test->drawLegend(200,15,$DataSet->GetDataDescription(),0,0,0,0,0,0,0,0,0,FALSE);  
////                    $Test->drawScale(array("DrawSubTicks"=>TRUE,"DrawArrows"=>TRUE,"ArrowSize"=>6));
//                    $Test->Render("./images/line_chart15.png");  
                     
//                    include("./pChart2.1.4/class/pData.class.php");
//                    include("./pChart2.1.4/class/pDraw.class.php");
//                    include("./pChart2.1.4/class/pImage.class.php");
//                    $MyData = new pData(); 
//                    $MyData->addPoints($uut_y_vals,"Chart");
//                    //$MyData->setSerieWeight("",2);
//                    $MyData->setSerieWeight("Chart",1);
//                    $MyData->setAxisName(0,"Flow Rate UUT (".$master_unit_type.")");
//                    $MyData->addPoints($std_x_vals);
//                    $MyData->setAbscissa("Serie1");
//                    $MyData->setAbscissaName("Flow Rate Std. (".$master_unit_type.")");
//                    $serieSettings = array("R"=>0,"G"=>0,"B"=>0);
//                    $MyData->setPalette("Chart",$serieSettings);
//
//                    $myPicture = new pImage(700,230,$MyData);
//                    $myPicture->Antialias = TRUE;
//                    $myPicture->drawRectangle(0,0,699,229,array("R"=>0,"G"=>0,"B"=>0));
//                    $myPicture->setFontProperties(array("FontName"=>"./pChart2.1.4/fonts/verdana.ttf","FontSize"=>9,"R"=>0,"G"=>0,"B"=>0));
//                    $myPicture->setGraphArea(60,20,650,200);
//                    $scaleSettings = array("GridR"=>50,"GridG"=>110,"GridB"=>190,"DrawSubTicks"=>TRUE,"CycleBackground"=>TRUE);
//                    $myPicture->drawScale($scaleSettings);
//                    $myPicture->drawLegend(580,12,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL));
//                    $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
//                    $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));
//                    $settings = array("Gradient"=>TRUE,"GradientMode"=>GRADIENT_EFFECT_CAN,"DisplayPos"=>LABEL_POS_INSIDE,"DisplayValues"=>TRUE,"DisplayR"=>255,"DisplayG"=>255,"DisplayB"=>255,"DisplayShadow"=>TRUE,"Surrounding"=>10);
//                    $myPicture->drawLineChart();
//                    $myPicture->render("./images/line_chart15.png");
                    //$myPicture->autoOutput("./images/line_chart15.png");
                ?>
                    <td width="605"><img width="600" src="./images/line_chart15.png" /></td>
                </tr>
            </table>
            