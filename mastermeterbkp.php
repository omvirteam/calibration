<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $masterMeterId       = "";
  $masterMeterName     = "";
  $masterMeterExpDate  = "";
  $parameter           = "";
  $range               = "";
  $accuracy            = 0;
  $accuracySubSub      = 0;
  $uncertinity         = 0;
  $certiAccuracy       = 0;
  $accuracy            = 0;
  $resolution          = 0;
  $stability           = 0;
  $masterMeterExp      = "";
  $meterEntry          = array();
  $meterEntryNameArray = array();
  $meterEntryIdArray   = array();
  $meterEntryExpArray  = isset($_REQUEST['masterMeterId']) ? $_REQUEST['masterMeterId'] : 0;
  // Master Meter Name Entry : Starts
  if(isset($_REQUEST['insertBtn']))
  {
    $masterMeterExp           = ($_POST['masterMeterExpDateYear']."-".$_POST['masterMeterExpDateMonth']."-".$_POST['masterMeterExpDateDay']);
    $selectmasterMeterName    = "INSERT INTO mastermeter(masterMeterId,masterMeterName,masterMeterExp)
                                 VALUES('".$masterMeterId."','".$_POST['masterMeterName']."','".$masterMeterExp."')";
    $selectmasterMeterNameRes = mysql_query($selectmasterMeterName);
    $joinId		                = mysql_insert_id();

    if(!$selectmasterMeterNameRes)
      die("Insert Query Not Inserted : ".mysql_error());
  // Master Meter Name Entry : Ends

  // Parameter of Master Meter Entry : Starts
    $loopCount = 0;
    while($loopCount < count($_POST['parameter']))
    {
      $parameter     = ($_POST['parameter'][$loopCount] != '') ? $_POST['parameter'][$loopCount] : 0;
      $accuracy      = ($_POST['accuracy'][$loopCount] != '') ? $_POST['accuracy'][$loopCount] : 0;
      if($_POST['parameter'][$loopCount] != "" || $_POST['accuracy'][$loopCount] != "" )
      {
        $insertMeterPara       = "INSERT INTO mastermetersub(masterMeterId,parameter,accuracy)
                                  VALUES(".$joinId.",'".$parameter."','".$accuracy."')";
        $insertMeterParaResult = mysql_query($insertMeterPara);
        $masterMeterSubIdInserted = mysql_insert_id();
        
        
      }
			  $loopCount++;	
    }
  // Parameter of Master Meter Entry : Ends
    header("Location:masterMeter.php");
  }

  // Master Meter Name Listing : Starts
  $meterNameList          = "SELECT masterMeterId,masterMeterName,masterMeterExp
                               FROM mastermeter
                              ORDER BY masterMeterName";
  $meterNameListResult    = mysql_query($meterNameList);
  $meterListRow           = mysql_num_rows($meterNameListResult);
  $i = 0;
  while($meterListRow = mysql_fetch_array($meterNameListResult))
  {
    $meterEntryIdArray[$i]       = $meterListRow['masterMeterId'];
    $meterEntryNameArray[$i]     = $meterListRow['masterMeterName'];
    $i++;
  }
  
  include("./bottom.php");
  $smarty->assign("masterMeterExpDate",$masterMeterExpDate);
  $smarty->assign("parameter",$parameter);
  $smarty->assign("accuracy",$accuracy);
  $smarty->assign("range",$range);
  $smarty->assign("accuracySubSub",$accuracySubSub);
  $smarty->assign("resolution",$resolution);
  $smarty->assign("stability",$stability);
  $smarty->assign("uncertinity",$uncertinity);
  $smarty->assign("certiAccuracy",$certiAccuracy);
  $smarty->assign("masterMeterId",$masterMeterId);
  $smarty->assign("meterEntry",$meterEntry);
  $smarty->assign("meterEntryIdArray",$meterEntryIdArray);
  $smarty->assign("meterEntryNameArray",$meterEntryNameArray);
  $smarty->assign("meterEntryExpArray",$meterEntryExpArray);
  $smarty->display("masterMeterSub.tpl");
}
?>