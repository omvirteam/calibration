<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $certificateNo       = 0;
  $grnDetailArr        = array();
  $rangeArray          = array();
  $leastCountArray     = array();
  $accuracyArray       = array();
  $masterMeterArray    = array();
  $totalPageCount      = 1;
  
  //Total Pages and Observation Table Limit - AT
  $totalPages = 1;  
  $observationTableLimit = 4;
    	
  
  //SELECT OF GRN MASTER Detail customer name to humidity :START
  $selectGrnMaster = "SELECT grnId,grnNo,DATE_FORMAT(grnmaster.grnDate,'%d/%m/%Y') AS grnDate,customer.custName,customer.address,
                             customer.city,customer.custRefName,customer.custCode
                        FROM grnmaster
                        LEFT JOIN customer ON grnmaster.customerId = customer.customerId
                       WHERE grnmaster.grnId = ".$_GET['grnId'];
  $selectGrnMasterRes    = mysql_query($selectGrnMaster);
  while($selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes))
  {
  	$grnDetailArr['grnNo']       = 'N - '.($selectGrnMasterRow['grnNo']);
  	$grnDetailArr['custCode']    = $selectGrnMasterRow['custCode'];
  	$grnDetailArr['custName']    = $selectGrnMasterRow['custName'];
  	$grnDetailArr['address']     = $selectGrnMasterRow['address'];
  	$grnDetailArr['city']        = $selectGrnMasterRow['city'];
  	$grnDetailArr['custRefName'] = $selectGrnMasterRow['custRefName'];
  	$grnDetailArr['grnDate']     = $selectGrnMasterRow['grnDate'];
  	                                  
  	$grnDetailQuery = "SELECT DATE_FORMAT(grndetail.callibrationDate,'%d/%m/%Y') AS callibrationDate,
                              DATE_FORMAT(grndetail.nextYearDate,'%d/%m/%Y') AS nextYearDate,itemId,itemCode,
                              accuracy,makeModel,instrumentId,leastCount,temperature,humidity,extraFields,extraField1,extraField2,
                              extraField3,extraField4,extraField5,extraField6,certiRemarks,masterRangeId
  	                     FROM grndetail 
  	                    WHERE grnDetailId = ".$_GET['grnDetailId'];
    $grnDetailQueryRes = mysql_query($grnDetailQuery);
  	while($grnDetailQueryResRow = mysql_fetch_array($grnDetailQueryRes))
    {
   	
  	  $grnDetailArr['callibrationDate'] = $grnDetailQueryResRow['callibrationDate'];
  	  $grnDetailArr['nextYearDate']     = $grnDetailQueryResRow['nextYearDate'];
  	  $grnDetailArr['itemCode']         = $grnDetailQueryResRow['itemCode'];
  	  $grnDetailArr['makeModel']        = $grnDetailQueryResRow['makeModel'];
  	  $grnDetailArr['instrumentId']     = $grnDetailQueryResRow['instrumentId'];
  	  $grnDetailArr['temperature']      = $grnDetailQueryResRow['temperature'];
  	  $grnDetailArr['humidity']         = $grnDetailQueryResRow['humidity'];
  	  
  	  $grnDetailArr['extraFields']      = $grnDetailQueryResRow['extraFields'];
  	  $grnDetailArr['extraField1']      = $grnDetailQueryResRow['extraField1'];
  	  $grnDetailArr['extraField2']      = $grnDetailQueryResRow['extraField2'];
  	  $grnDetailArr['extraField3']      = $grnDetailQueryResRow['extraField3'];
  	  $grnDetailArr['extraField4']      = $grnDetailQueryResRow['extraField4'];
  	  $grnDetailArr['extraField5']      = $grnDetailQueryResRow['extraField5'];
  	  $grnDetailArr['extraField6']      = $grnDetailQueryResRow['extraField6'];
  	  
  	  //for same grn, item, itemCode... this will be different :Start
      $grnDetailArr['range2']          = "";
  	  $grnDetailArr['leastCount']      = "";
  	  $grnDetailArr['accuracy']        = "";
  	  $grnDetailArr['certiRemarks']    = "";
  	  $grnDetailArr['masterMeterIdIn'] = "";
  	  //for same grn, item, itemCode... this will be different :End
  	  //for different master meter... this will be different :Start
  	  $grnDetailArr['procedureText']   = "";
  	  //for different master meter... this will be different :End
  	  
    	$sameGrnDetailQuery = "SELECT grnDetailId, masterMeterId,DATE_FORMAT(grndetail.callibrationDate,'%d/%m/%Y') AS callibrationDate,
                                DATE_FORMAT(grndetail.nextYearDate,'%d/%m/%Y') AS nextYearDate,itemCode,
                                accuracy,rangeValue AS range2,makeModel,instrumentId,leastCount,temperature,humidity,extraFields,extraField1,extraField2,
                                extraField3,extraField4,extraField5,extraField6,certiRemarks,ratio1,ratio2
    	                     FROM grndetail 
    	                    WHERE grnId  = ".$selectGrnMasterRow['grnId']."
    	                      AND itemId = ".$grnDetailQueryResRow['itemId']."
    	                      AND itemCode  = '".$grnDetailQueryResRow['itemCode']."'
    	                      AND grnDetailId  = ".$_GET['grnDetailId'];
    	//When there is more than 1 range for same item, there will be same itemId and same itemCode... but different grnDetailId
    	//here same grnDetailId, from which we are selecting data... will come again
      $sameGrnDetailQueryRes = mysql_query($sameGrnDetailQuery) or print(mysql_error());
      
      $i = 0;
      $a = 0;
      $masterArr = array();
     	while($sameGrnDetailQueryResRow = mysql_fetch_array($sameGrnDetailQueryRes))
      {
      	if($i != 0)
      	{//if 1st time if 0 ... then we do not want comma
      		if(!in_array($sameGrnDetailQueryResRow['range2'], $rangeArray) )
      		{
      	    $grnDetailArr['range2']          .= ", ";
      	  }
      		if(!in_array($sameGrnDetailQueryResRow['leastCount'], $leastCountArray) )
      		{
      	    $grnDetailArr['leastCount']      .= ", ";
      	  }
      		if(!in_array($sameGrnDetailQueryResRow['accuracy'], $accuracyArray) )
      		{
      	    $grnDetailArr['accuracy']        .= ", ";
      	  }
      	  $grnDetailArr['certiRemarks']    .= ""; //we do not put comma here
      	  if($sameGrnDetailQueryResRow['masterMeterId'] != "")
  	        $grnDetailArr['masterMeterIdIn'] .= ", ";
      	}
     		if(!in_array($sameGrnDetailQueryResRow['range2'], $rangeArray) )
        {
      	  $grnDetailArr['range2']          .= $sameGrnDetailQueryResRow['range2'];
        }    	  
     		if(!in_array($sameGrnDetailQueryResRow['leastCount'], $leastCountArray) )
        {
          $grnDetailArr['leastCount']      .= $sameGrnDetailQueryResRow['leastCount'];
        }
    		if(!in_array($sameGrnDetailQueryResRow['accuracy'], $accuracyArray) )
    		{
    	    $grnDetailArr['accuracy']        .= $sameGrnDetailQueryResRow['accuracy'];
    	  }
    	  $grnDetailArr['certiRemarks']    .= $sameGrnDetailQueryResRow['certiRemarks'];
     	  if($sameGrnDetailQueryResRow['masterMeterId'] != "")
          $grnDetailArr['masterMeterIdIn'] .= $sameGrnDetailQueryResRow['masterMeterId'];
    	  
    	  ////////////////////////////////////////////////////////////////
        $selectMasterMeter = "SELECT grnObsMasterId, DATE_FORMAT(grnobsmaster .grnObsDate,'%d/%m/%Y') AS grnObsDate, 
                                     parameterentry.parameterName, parameterentry.tableNo, mastermetersub.parameter, 
                                     grndetail.rangeValue AS range2, mastermetersubsub.rangeValue, grnobsmaster.selfCertiNo
                                FROM grnobsmaster
                                LEFT JOIN grndetail ON grnobsmaster.grnDetailId = grndetail.grnDetailId
                                LEFT JOIN parameterentry ON grndetail.parameterId = parameterentry.parameterId
                                LEFT JOIN mastermeter ON grnobsmaster.masterMeterId = mastermeter.masterMeterId
                                LEFT JOIN mastermetersub ON grnobsmaster.mParameterId = mastermetersub.masterMeterSubId
                                LEFT JOIN mastermetersubsub ON grnobsmaster.mRangeId = mastermetersubsub.masterMeterSubSubId
                               WHERE grnobsmaster.grnId = ".$_GET['grnId']."
                                 AND grnobsmaster.grnDetailId = ".$sameGrnDetailQueryResRow['grnDetailId'];
        $selectMasterMeterRes = mysql_query($selectMasterMeter) or print(mysql_error());
        
        //Total Pages Calculation - AT
        $totalPages = $totalPages + ceil( mysql_num_rows($selectMasterMeterRes) / $observationTableLimit );
        
        while($mMeterRow = mysql_fetch_array($selectMasterMeterRes))
        {
        	$certificateNo = $mMeterRow['selfCertiNo'];
        	$masterArr[$a]['tableNo']    = $mMeterRow['tableNo'];
        	$masterArr[$a]['displayMf']     = 0;
          $masterArr[$a]['displayCalculated'] = 0;
        	$masterArr[$a]['grnObsDate']    = $mMeterRow['grnObsDate'];
        	$masterArr[$a]['parameterName'] = $mMeterRow['parameterName'];
        	$masterArr[$a]['range']         = $mMeterRow['rangeValue'];
        	$masterArr[$a]['range2']        = $mMeterRow['range2'];

          $selectGrnDetailId = "SELECT grnObservationId,testMeterAverage,resolutionTypeA,stdMeter1AfterDecimal,resolutionTypeAAfterDecimal,
                                       stdMeter1,stdMeterAverage,masterMeterReading,percentageRdg,expandedUncertinity,
                                       meanReading,expandedUncertinityInPre
                                  FROM grnobservation
                                 WHERE grnObsMasterId = ".$mMeterRow['grnObsMasterId'];
          $selectGrnDetailIdRes = mysql_Query($selectGrnDetailId);
          $b = 0;
          if(mysql_num_rows($selectGrnDetailIdRes) > 0)
          {
            while($selectGrnDetailRow = mysql_fetch_array($selectGrnDetailIdRes))
            {
            	$masterArr[$a]['dataPresent'] = 1;
            	
            	////////////////////////////////////////
            	if($sameGrnDetailQueryResRow['ratio2'] != 0)
            	{
            	  $detailArr[$a][$b]['mf'] = $sameGrnDetailQueryResRow['ratio1'] / $sameGrnDetailQueryResRow['ratio2'];
        	        $masterArr[$a]['displayMf']     = 1;
            	}
            	else
            	{
            	  $detailArr[$a][$b]['mf'] = 1;//we use this in multiplication, so 1 (not 0)
            	}
				
                  if($detailArr[$a][$b]['mf'] != 1)
                  {
                    $masterArr[$a]['displayCalculated'] = 1;
                  }
                  
            	$detailArr[$a][$b]['tableNo2reading']       = $selectGrnDetailRow['stdMeterAverage'] * $detailArr[$a][$b]['mf'];
            	$detailArr[$a][$b]['tableNo3calculated']    = $selectGrnDetailRow['testMeterAverage'] / $detailArr[$a][$b]['mf'];
            	$detailArr[$a][$b]['tableNo4calculated']    = $selectGrnDetailRow['testMeterAverage'] / $detailArr[$a][$b]['mf'];
            	////////////////////////////////////////    
            	$detailArr[$a][$b]['grnObservationId']      = $selectGrnDetailRow['grnObservationId'];
            	$detailArr[$a][$b]['masterMeterReading']    = $selectGrnDetailRow['masterMeterReading'];
            	$detailArr[$a][$b]['stdMeterAverage']       = $selectGrnDetailRow['stdMeterAverage'];
            	$detailArr[$a][$b]['stdMeterAverageAfterMf']= $selectGrnDetailRow['stdMeterAverage']/$detailArr[$a][$b]['mf'];
            	$detailArr[$a][$b]['testMeterAverage']      = $selectGrnDetailRow['testMeterAverage'];
            	$detailArr[$a][$b]['stdMeter1AfterDecimal'] = $selectGrnDetailRow['stdMeter1AfterDecimal'];
            	$detailArr[$a][$b]['resolutionTypeAAfterDecimal'] = $selectGrnDetailRow['resolutionTypeAAfterDecimal'];
            	
            	$detailArr[$a][$b]['errorUnits']         = $selectGrnDetailRow['stdMeterAverage']/$detailArr[$a][$b]['mf'] - $selectGrnDetailRow['testMeterAverage'];
            	$detailArr[$a][$b]['errorUnitsTableNo2'] = sprintf("%.2f",$selectGrnDetailRow['stdMeterAverage'] - $selectGrnDetailRow['testMeterAverage']);
            	
            	$detailArr[$a][$b]['errorUnitsTableNo3'] = $selectGrnDetailRow['stdMeterAverage'] - $detailArr[$a][$b]['tableNo3calculated'];
            	$detailArr[$a][$b]['errorUnitsTableNo4'] = $selectGrnDetailRow['stdMeterAverage'] - $detailArr[$a][$b]['tableNo4calculated'];
              
              $detailArr[$a][$b]['errorRdgTableNo3']   = sprintf("%.2f",($detailArr[$a][$b]['errorUnitsTableNo3']/$detailArr[$a][$b]['tableNo3calculated']*100));
              $detailArr[$a][$b]['errorRdgTableNo4']   = sprintf("%.2f",($detailArr[$a][$b]['errorUnitsTableNo4']/$detailArr[$a][$b]['tableNo4calculated']*100));
            	
              $total       = $selectGrnDetailRow['percentageRdg'];
              $errorUnits  = $selectGrnDetailRow['stdMeterAverage']/$detailArr[$a][$b]['mf'] - $selectGrnDetailRow['testMeterAverage'];
              $errorUnitsTableNo2 = $selectGrnDetailRow['stdMeterAverage'] - $selectGrnDetailRow['testMeterAverage'];

              $detailArr[$a][$b]['errorRdg']    = $errorUnits/$selectGrnDetailRow['testMeterAverage']*100;
              $detailArr[$a][$b]['errorRdgTableNo2']    = sprintf("%.2f",($errorUnitsTableNo2/$selectGrnDetailRow['testMeterAverage'])*100);

            	$detailArr[$a][$b]['expandedUncertinity'] = sprintf("%.3f",($selectGrnDetailRow['expandedUncertinity']*100)/$selectGrnDetailRow['meanReading']);
            	$detailArr[$a][$b]['expandedUncertinityInPre'] = abs(sprintf("%.3f",$selectGrnDetailRow['expandedUncertinityInPre']));
              $b++;
            }
          }
          else
          {
          	$detailArr[$a] = array();
          	$masterArr[$a]['dataPresent'] = 0;
          }
          $a++;
        }
        ////////////////////////////////////////////////////////////////
      	
      	$rangeArray[$i]      = $sameGrnDetailQueryResRow['range2'];
      	$leastCountArray[$i] = $sameGrnDetailQueryResRow['leastCount'];
      	$accuracyArray[$i]   = $sameGrnDetailQueryResRow['accuracy'];
      	$i++;
      }
      
      $mastermeterQuery = "SELECT masterMeterName,masterMeterIdNo,masterMeterMake,masterMeterCertificateNo,masterMeterModelNo,
                                  DATE_FORMAT(masterMeterExp,'%d/%m/%Y') AS masterMeterExp,masterMeterTraceabilityTo,
                                  procedureText,masterUncertaintyText,masterMeterSerialNo
    	                       FROM mastermeter 
  	                        WHERE masterMeterId IN (".$grnDetailArr['masterMeterIdIn'].")";
      $mastermeterQueryRes = mysql_query($mastermeterQuery) or print(mysql_error());
      $masterMeterCount = 0;
      $realMasterMeterCount = 0;
    	while($mastermeterQueryResRow = mysql_fetch_array($mastermeterQueryRes))
      {
      	if(!in_array($mastermeterQueryResRow['masterMeterCertificateNo'], $masterMeterArray))
      	{
  	      $masterMeterData[$realMasterMeterCount]['masterMeterName']           = $mastermeterQueryResRow['masterMeterName'];
  	      $masterMeterData[$realMasterMeterCount]['masterMeterIdNo']           = $mastermeterQueryResRow['masterMeterIdNo'];
  	      $masterMeterData[$realMasterMeterCount]['masterMeterMake']           = $mastermeterQueryResRow['masterMeterMake'];
  	      $masterMeterData[$realMasterMeterCount]['masterMeterModelNo']        = $mastermeterQueryResRow['masterMeterModelNo'];
      	  $masterMeterData[$realMasterMeterCount]['masterMeterCertificateNo']  = $mastermeterQueryResRow['masterMeterCertificateNo'];
      	  $masterMeterData[$realMasterMeterCount]['masterMeterExp']            = $mastermeterQueryResRow['masterMeterExp'];
      	  $masterMeterData[$realMasterMeterCount]['masterMeterTraceabilityTo'] = $mastermeterQueryResRow['masterMeterTraceabilityTo'];
      	  $masterMeterData[$realMasterMeterCount]['masterUncertaintyText']     = $mastermeterQueryResRow['masterUncertaintyText'];
      	  $masterMeterData[$realMasterMeterCount]['masterMeterSerialNo']       = $mastermeterQueryResRow['masterMeterSerialNo'];
          $realMasterMeterCount++;
        }
        
      	//For same meter, there may be different procedureText, so we have not put below in above if condition
      	if($masterMeterCount != 0)
      	{//if 1st time if 0 ... then we do not want comma / br tag
      	  $grnDetailArr['procedureText'] .= "<BR>";
      	}
        $grnDetailArr['procedureText'] .= $mastermeterQueryResRow['procedureText'];
        
      	$masterMeterArray[$masterMeterCount] = $mastermeterQueryResRow['masterMeterCertificateNo'];
        $masterMeterCount++;
      }

//    	$mastermetersubQuery = "SELECT rangeValue AS range1
//    	                          FROM mastermetersubsub 
//  	                           WHERE masterMeterSubSubId = ".$grnDetailQueryResRow['masterRangeId'];
//      $mastermetersubQueryRes = mysql_query($mastermetersubQuery);
//    	while($mastermetersubQueryResRow = mysql_fetch_array($mastermetersubQueryRes))
//      {
//  	    $grnDetailArr['range1'] = $mastermetersubQueryResRow['range1'];
//      }
//
    	$itemQuery = "SELECT itemName
                      FROM item 
                     WHERE itemId = ".$grnDetailQueryResRow['itemId'];
      $itemQueryRes = mysql_query($itemQuery);
    	while($itemQueryResRow = mysql_fetch_array($itemQueryRes))
      {
  	    $grnDetailArr['itemName']         = $itemQueryResRow['itemName'];
      }
    }
  }  
  //SELECT OF GRN MASTER Detail customer name to humidity :End
  
  if($realMasterMeterCount > 1)
  {
  	$totalPageCount++;
  }
  
  include("./bottom.php");
  $smarty->assign("certificateNo",$certificateNo);
  $smarty->assign("grnDetailArr",$grnDetailArr);
  $smarty->assign("masterMeterData",$masterMeterData);
  $smarty->assign("realMasterMeterCount",$realMasterMeterCount);
  $smarty->assign("total",$total);
  $smarty->assign("masterArr",$masterArr);
  $smarty->assign("detailArr",$detailArr);
  
  //Total Pages, Current Page Assign and Observation Table Limit - AT
  $smarty->assign("totalPages",$totalPages);
  $smarty->assign("currentpage",2);
  $smarty->assign("observationTableLimit",$observationTableLimit);
  
  $smarty->display("-nablCertificateTable.tpl");
}
?>
