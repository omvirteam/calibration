{include file="./headStart.tpl"}
<title>Master Meter</title>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
  $(document).ready(function(){ 
    $(".add").click(function(){
    $("#one").clone(true).appendTo("#mainDiv").find('input[type="text"]').val('').end().find('input[type="hidden"]').val(0);
    });
  });
</script>
<form name="form1" action="{$smarty.server.PHP_SELF}" id="form1" method="post">
{include file="./headEnd.tpl"}
{if (have_access_role($smarty.const.MASTER_METER_MODULE_ID,"add") && {!isset($smarty.get.masterMeterId)}) || (have_access_role($smarty.const.MASTER_METER_MODULE_ID,"edit") && {isset($smarty.get.masterMeterId)})}
<input type="hidden" name="masterMeterId" value="{$masterMeterId}" />
<table border="3" align="center" cellpadding="1" cellspacing="1">
<tr>
  <td align="center" class="table1" colspan="18">Master Meter Entry</td>
</tr>
<tr>
  <td align="right" class="table2">Meter Name</td>
  <td><input type="text" name="masterMeterName" id="masterMeterName" value="{$masterMeterName}"  required="required"></td>
  <td align="right" class="table2">I.D No.</td>
  <td><input type="text" name="masterMeterIdNo" value="{$masterMeterIdNo}" id="masterMeterIdNo"></td>
  <td align="right" class="table2">Make</td>
  <td><input type="text" name="masterMeterMake" value="{$masterMeterMake}" id="masterMeterMake"></td>
  <td align="right" class="table2">Model No.</td>
  <td><input type="text" name="masterMeterModelNo" value="{$masterMeterModelNo}" id="masterMeterModelNo"></td>
  <td align="right" class="table2">SERIAL No.</td>
  <td><input type="text" name="masterMeterSerialNo" value="{$masterMeterSerialNo}" id="masterMeterSerialNo"></td>
  <td align="right" class="table2">Certificate No.</td>
  <td><input type="text" name="masterMeterCertificateNo" value="{$masterMeterCertificateNo}" id="masterMeterCertificateNo"></td>
  <tr>
  <td align="right" class="table2">Calibration Expiry Date :</td>
  <td NOWRAP>
    {html_select_date prefix="masterMeterExpDate" start_year="-3" end_year="+10" month_format="%m" field_order="DMY" day_value_format="%02d" time=$masterMeterExp}
  </td>
  <td align="right" class="table2">Traceability To.</td>
  <td><input type="text" name="masterMeterTraceabilityTo" value="{$masterMeterTraceabilityTo}" id="masterMeterTraceabilityTo"></td>
  <td align="right" class="table2">Page Title</td>
  <td><input type="text" name="page_title" value="{$page_title}" class="page_title" /></td>
  <td colspan="6">
  <b> At A Site </b>
  Procedure  <input type="text" name="procedureText" value="{$procedureText}" class="procedure" />
  Master Temp   <input type="text" name="master_temp" value="{$master_temp}" class="master_temp" />
  Master Humidity   <input type="text" name="master_humidity" value="{$master_humidity}" class="master_humidity" />
  </td>
</tr>
<tr>
    <td align="right" class="table2">Master Uncertainty Text</td>
    <td colspan="4">
    <textarea name="masterUncertaintyText" rows="4" cols="78">
    {$masterUncertaintyText}
    </textarea>
  </td>
  <td style="border:none"></td>
    <td colspan="6">
  <b> At A LAB </b>
  Procedure  <input type="text" name="procedureTextLab" value="{$procedureTextLab}" class="procedure" />
  Master Temp   <input type="text" name="master_temp_lab" value="{$master_temp_lab}" class="master_temp" />
  Master Humidity   <input type="text" name="master_humidity_lab" value="{$master_humidity_lab}" class="master_humidity" />
  </td>
</tr>
</tr>
</table>
<input type="hidden" name="masterMeterId" value="{$masterMeterId}" />
<table border="3" id="dataTable" align="center" cellpadding="0" cellspacing="0">
  <tbody id="mainDiv">
  	{if $isEdit == 1}
  	  {section name=sec loop=$parameterArr}
  	  <tr id="one" class="itemRow">	
	      <td>
	      	Parameter
	        <input type="hidden" name="masterMeterSubId[]" class="masterMeterSubId" value="{$parameterArr[sec].masterMeterSubId}" />
	        <input type="text" name="parameter[]" class="parameter" value="{$parameterArr[sec].parameter}" />
	        <input type="button" name="add[]" value="Add Param" class="add"/>  
	      </td>
      </tr>
      {/section}
    {else}
      <tr id="one" class="itemRow">	
	      <td>
	      	parameter
	        <input type="text" name="parameter[]" class="parameter" />
	        <input type="button" name="add[]" value="Add Param" class="add"/>  
	      </td>
      </tr>    
  	{/if}
   </tbody>
</table>
<table border="2" align="center">
  <tr>
    <td><input type="submit" name="insertBtn" value="S  T  O  R  E" class="button" /></td>
  </tr>
</table>
{/if}
<table align='center' border='0' cellpadding='1' cellspacing='2'>
<center class="center"><h2>Master Metar Available...!</h2></center><br>
<tr>
  <td  class="table1" align='center'>&nbsp;</td>
  <td  class="table1" align='center'>&nbsp;</td>
  <!-- additional change by girish - start -->
  <td  class="table1" align='center'>#</td>
  <!-- additional change by girish - end -->  
  <td  class="table1" align='center'>Master Meter</td>
  <td  class="table1" align='center'>I.D No.</td>
  <td  class="table1" align='center'>Make</td>
  <td  class="table1" align='center'>Model No.</td>
  <td  class="table1" align='center'>SERIAL No.</td>
  <td  class="table1" align='center'>Certificate No.</td>
  <td  class="table1" align='center'>Expiry Date</td>
  <td  class="table1" align='center'>Traceability To.</td>
  <td  class="table1" align='center'>Procedure </td>
  <td  class="table1" align='center'>Master Uncertainty Text</td>
</tr>
{section name="sec" loop=$meterEntry}
<tr>
  <td width="2%">
        {if have_access_role($smarty.const.MASTER_METER_MODULE_ID,"edit")}
            <a href="masterMeter.php?masterMeterId={$meterEntry[sec].masterMeterId}" class="link">Edit</a>
        {/if}
  </td>
  <td width="2%">
        {if have_access_role($smarty.const.MASTER_METER_MODULE_ID,"delete")}
            <a href="masterMeterDelete.php?masterMeterId={$meterEntry[sec].masterMeterId}"  onclick="return deletechecked();" id="delete" class="link">Delete</a>
        {/if}
  </td>
  <!-- additional change by girish - start -->  
  <td class="table2" align="center" width="8%">{$smarty.section.sec.index+1}</td>
  <!-- additional change by girish - end -->  
  <td class="table2" align="center" width="8%">{$meterEntry[sec].masterMeterName}</td>
  <td class="table2" align="center" width="4%">{$meterEntry[sec].masterMeterIdNo}</td>
  <td class="table2" align="center">{$meterEntry[sec].masterMeterMake}</td>
  <td class="table2" align="center">{$meterEntry[sec].masterMeterModelNo}</td>
  <td class="table2" align="center">{$meterEntry[sec].masterMeterSerialNo}</td>
  <td class="table2" align="center" width="15%">{$meterEntry[sec].masterMeterCertificateNo}</td>
  <td class="table2" align="center" width="7%">{$meterEntry[sec].masterMeterExp}</td>
  <td class="table2" align="center">{$meterEntry[sec].masterMeterTraceabilityTo}</td>
  <td class="table2" align="center">{$meterEntry[sec].procedureText}</td>
  <td class="table2" align="center" width="10%">{$meterEntry[sec].masterUncertaintyText}</td>
</tr>
{/section}
</table>
</form>
<script type="text/javascript"> 
  var mutli_education = document.form1.elements["range[][]"].value;
  alert(mutli_education);
  mutli_education.setAttributeNodeNS(mutli_education.cloneNode(true));
</script>

{include file="footer.tpl"}