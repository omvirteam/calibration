<table align="left" cellpadding="2" cellspacing="0" border="1">
{if $recordNotFound == ""}
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

<tr>
    <th colspan="34">
        {if $stdMeter1[0].table_info == '19'}
            {if ( ((isset($stdMeter1[0]) && ($stdMeter1[0].table_no == '1')) || (isset($stdMeter1[0]) && ($stdMeter1[0].table_no == '2')) || (isset($stdMeter1[0]) && ($stdMeter1[0].table_no == '3'))) && ( (isset($stdMeter1[1]) && ($stdMeter1[1].table_no == '1')) || (isset($stdMeter1[1]) && ($stdMeter1[1].table_no == '2')) || (isset($stdMeter1[1]) && ($stdMeter1[1].table_no == '3'))) && ( (isset($stdMeter1[2]) && ($stdMeter1[2].table_no == '1')) || (isset($stdMeter1[2]) && ($stdMeter1[2].table_no == '2')) || (isset($stdMeter1[2]) && ($stdMeter1[2].table_no == '3'))))}
                <input type="checkbox" name="print_type_1" id="print_type_1" value="print_type_1"> <label for="print_type_1">Print Type 1</label> &nbsp; ||
                <input type="checkbox" name="print_type_2" id="print_type_2" value="print_type_2"> <label for="print_type_2">Print Type 2</label> &nbsp; ||
                <input type="checkbox" name="print_type_3" id="print_type_3" value="print_type_3"> <label for="print_type_3">Print Type 3</label> &nbsp; ||
{*                <a target="_blank" href="./new_obs_print_table_all.php?grnId={$grnId}&grnDetailId={$grnDetailId}" onclick="open_print()"> Table 19 Print </a> &nbsp; ||*}
                <a target="_blank" data-grnId="{$grnId}" data-grnDetailId="{$grnDetailId}"  id="table_19_print"> Table 19 Print </a> &nbsp; ||
            {else}
                || &nbsp; For Table 19 Print : All 3 obsevation type is mandatory &nbsp; ||
            {/if}
        {else}
		  {if $allowViewCust == 1}
            <a target="_blank" href="./new_obs_print_regular_all_15.php?grnId={$grnId}&grnDetailId={$grnDetailId}"> | Regular 1 Print </a> &nbsp; 
			|| &nbsp; <a target="_blank" href="./new_obs_print_table_all.php?grnId={$grnId}&grnDetailId={$grnDetailId}"> All Table Print </a> &nbsp; 
			|| &nbsp; <a target="_blank" href="./new_obs_print_table_all.php?grnId={$grnId}&grnDetailId={$grnDetailId}&withuncer=1"> All Table Print With Expanded Uncertainty Column</a> &nbsp; || 
            || &nbsp; <a target="_blank" href="./new_obs_print_table_all.php?grnId={$grnId}&grnDetailId={$grnDetailId}&withuncer=1&with_qr=true"> New</a> &nbsp; ||
		  {/if}
			&nbsp; &nbsp; &nbsp; <a target="_blank" href="./calculation_of_uncert_print.php?grnId={$grnId}&grnDetailId={$grnDetailId}"> Calculation of Uncertainty Print </a> &nbsp; &nbsp; 
		  {if $allowViewCust == 1}
			|| &nbsp; &nbsp;<a target="_blank" href="./reading_print.php?grnId={$grnId}"> Observation Reading Print </a>
		  {/if}
        {/if}
    </th>
</tr>
<tr>
    <th rowspan="3">Meter</th>
    <th colspan="5" rowspan="3">Meter Value</th>
    <th rowspan="3">Budget Print</th>
    <th rowspan="3">Table Info</th>
    <th rowspan="3">Table Index</th>
{*    <th rowspan="3">&nbsp;</th>*}
    <th rowspan="3">&nbsp;</th>
    <th rowspan="3">&nbsp;</th>
    <th rowspan="3">&nbsp;</th>
    <th colspan="4">Type A Uncertainty</th>
    <th colspan="18">Type B Uncertainty</th>
</tr>
<tr>
    <th>Avg.</th>
    <th>&nbsp;</th>
    <th>UA</th>
    <th>% U</th>
    <th>µ</th>
    <th>UB1</th>
    <th>UB2</th>
    <th>UB3</th>
    <th>UB4</th>
    <th>UB5</th>
    <th>UB6</th>
    <th>UB7</th>
    <th>UB8</th>
    <th>UB9</th>
    <th>UB10</th>
    <th>UB11</th>
    <th>UB12</th>
    <th>Uc</th>
    <th>Ue</th>
    <th>%Ue</th>
    <th>%Ue</th>
    <th>%Ue</th>
</tr>
<tr>
    <th>Mean reading</th>
    <th>Standard deviation</th>
    <th>Standard Uncertainty</th>
    <th>Standard Uncertainty</th>
    <th>Degree of freedom</th>
    
    <th>Uncertinty of Master</th>
    <th>Uncertinty of Master</th>
    <th>Uncertinty of Master</th>
    <th>Accuracy of Master</th>
    <th>Accuracy of Master</th>
    <th>Accuracy of Master</th>
    <th>Resolution of UUC</th>
    <th>Uncertainty of Repeatibility</th>
    <th>Uncertainty of Installation Error</th>
    <th>Uncertainty due to Hysteresis</th>
    <th>Reference Std.Weight</th>
    <th>Eccentric Loading</th>
    
    <th>Combined Uncertainty</th>
    <th>Expanded Uncertainty</th>
    <th>Expanded Uncertainty %(calculation)</th>
    <th>cmc</th>
    <th>Expanded Uncertainty %(with cmc)</th>
</tr>
  {section name="sec" loop=$stdMeter1}
{if $stdMeter1[sec].table_info == '6B'}
{if !empty($stdMeter1[sec].reading_json['testMeter5'])}
    <tr>
        <td class="" align='left'>120% Ratio</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter1[sec].reading_json['testMeter5']}
                    <td class="table2" align="right">{$stdMeter1[sec].reading_json['testMeter5'][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    <td class="">
      <a target="_blank" href="./budget_6B.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}&meter=5"> Budget Print</a>
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    <td class="" align="right">{if isset($stdMeter1[sec].testMeter_avg5)} {$stdMeter1[sec].testMeter_avg5|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_dev_5)} {$stdMeter1[sec].std_dev_5|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_5)} {$stdMeter1[sec].std_uncer_5|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per_5)} {$stdMeter1[sec].std_uncer_per_5|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_std_ct_5)} {$stdMeter1[sec].uncertinty_of_std_ct_5|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_burden_box_5)} {$stdMeter1[sec].uncertinty_of_burden_box_5|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_ct_test_5)} {$stdMeter1[sec].uncertinty_of_ct_test_5|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_std_ct_5)} {$stdMeter1[sec].accuracy_of_std_ct_5|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_ct_test_5)} {$stdMeter1[sec].accuracy_of_ct_test_5|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].com_uncer_5)} {$stdMeter1[sec].com_uncer_5|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_5)} {$stdMeter1[sec].exp_uncer_5|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc_5)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc_5|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_5)} {$stdMeter1[sec].exp_uncer_per_ue_5|string_format:"%.2f"}  {/if}</td>
    </tr>
{/if}
{if !empty($stdMeter1[sec].reading_json['testMeter4'])}
    <tr>
        <td class="" align='left'>100% Ratio</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter1[sec].reading_json['testMeter4']}
                    <td class="table2" align="right">{$stdMeter1[sec].reading_json['testMeter4'][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    <td class="">
      <a target="_blank" href="./budget_6B.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}&meter=4"> Budget Print</a>
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    <td class="" align="right">{if isset($stdMeter1[sec].testMeter_avg4)} {$stdMeter1[sec].testMeter_avg4|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_dev_4)} {$stdMeter1[sec].std_dev_4|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_4)} {$stdMeter1[sec].std_uncer_4|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per_4)} {$stdMeter1[sec].std_uncer_per_4|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_std_ct_4)} {$stdMeter1[sec].uncertinty_of_std_ct_4|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_burden_box_4)} {$stdMeter1[sec].uncertinty_of_burden_box_4|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_ct_test_4)} {$stdMeter1[sec].uncertinty_of_ct_test_4|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_std_ct_4)} {$stdMeter1[sec].accuracy_of_std_ct_4|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_ct_test_4)} {$stdMeter1[sec].accuracy_of_ct_test_4|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].com_uncer_4)} {$stdMeter1[sec].com_uncer_4|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_4)} {$stdMeter1[sec].exp_uncer_4|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc_4)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc_4|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_4)} {$stdMeter1[sec].exp_uncer_per_ue_4|string_format:"%.2f"}  {/if}</td>
    </tr>
{/if}
{if !empty($stdMeter1[sec].reading_json['testMeter3'])}
    <tr>
        <td class="" align='left'>20% Ratio</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter1[sec].reading_json['testMeter3']}
                    <td class="table2" align="right">{$stdMeter1[sec].reading_json['testMeter3'][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    <td class="">
      <a target="_blank" href="./budget_6B.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}&meter=3"> Budget Print</a>
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    <td class="" align="right">{if isset($stdMeter1[sec].testMeter_avg3)} {$stdMeter1[sec].testMeter_avg3|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_dev_3)} {$stdMeter1[sec].std_dev_3|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_3)} {$stdMeter1[sec].std_uncer_3|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per_3)} {$stdMeter1[sec].std_uncer_per_3|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_std_ct_3)} {$stdMeter1[sec].uncertinty_of_std_ct_3|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_burden_box_3)} {$stdMeter1[sec].uncertinty_of_burden_box_3|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_ct_test_3)} {$stdMeter1[sec].uncertinty_of_ct_test_3|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_std_ct_3)} {$stdMeter1[sec].accuracy_of_std_ct_3|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_ct_test_3)} {$stdMeter1[sec].accuracy_of_ct_test_3|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].com_uncer_3)} {$stdMeter1[sec].com_uncer_3|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_3)} {$stdMeter1[sec].exp_uncer_3|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc_3)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc_3|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_3)} {$stdMeter1[sec].exp_uncer_per_ue_3|string_format:"%.2f"}  {/if}</td>
    </tr>
{/if}
{if !empty($stdMeter1[sec].reading_json['testMeter2'])}
    <tr>
        <td class="" align='left'>5% Ratio</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter1[sec].reading_json['testMeter2']}
                    <td class="table2" align="right">{$stdMeter1[sec].reading_json['testMeter2'][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    <td class="">
      <a target="_blank" href="./budget_6B.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}&meter=2"> Budget Print</a>
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    <td class="" align="right">{if isset($stdMeter1[sec].testMeter_avg2)} {$stdMeter1[sec].testMeter_avg2|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_dev_2)} {$stdMeter1[sec].std_dev_2|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_2)} {$stdMeter1[sec].std_uncer_2|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per_2)} {$stdMeter1[sec].std_uncer_per_2|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_std_ct_2)} {$stdMeter1[sec].uncertinty_of_std_ct_2|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_burden_box_2)} {$stdMeter1[sec].uncertinty_of_burden_box_2|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_ct_test_2)} {$stdMeter1[sec].uncertinty_of_ct_test_2|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_std_ct_2)} {$stdMeter1[sec].accuracy_of_std_ct_2|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_ct_test_2)} {$stdMeter1[sec].accuracy_of_ct_test_2|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].com_uncer_2)} {$stdMeter1[sec].com_uncer_2|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_2)} {$stdMeter1[sec].exp_uncer_2|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc_2)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc_2|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_2)} {$stdMeter1[sec].exp_uncer_per_ue_2|string_format:"%.2f"}  {/if}</td>
    </tr>
{/if}
{if !empty($stdMeter1[sec].reading_json['testMeter'])}
    <tr>
        <td class="" align='left'>1% Ratio</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter1[sec].reading_json['testMeter']}
                    <td class="table2" align="right">{$stdMeter1[sec].reading_json['testMeter'][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    <td class="">
      <a target="_blank" href="./budget_6B.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}&meter=1"> Budget Print</a>
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    <td class="" align="right">{if isset($stdMeter1[sec].testMeter_avg)} {$stdMeter1[sec].testMeter_avg|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_dev)} {$stdMeter1[sec].std_dev|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer)} {$stdMeter1[sec].std_uncer|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per)} {$stdMeter1[sec].std_uncer_per|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_std_ct)} {$stdMeter1[sec].uncertinty_of_std_ct|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_burden_box)} {$stdMeter1[sec].uncertinty_of_burden_box|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_ct_test)} {$stdMeter1[sec].uncertinty_of_ct_test|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_std_ct)} {$stdMeter1[sec].accuracy_of_std_ct|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_ct_test)} {$stdMeter1[sec].accuracy_of_ct_test|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].com_uncer)} {$stdMeter1[sec].com_uncer|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer)} {$stdMeter1[sec].exp_uncer|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc_1)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc_1|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_1)} {$stdMeter1[sec].exp_uncer_per_ue_1|string_format:"%.2f"}  {/if}</td>
    </tr>
{/if}


{if !empty($stdMeter1[sec].reading_json['testMeter10'])}
    <tr>
        <td class="" align='left'>120% Phase</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter1[sec].reading_json['testMeter10']}
                    <td class="table2" align="right">{$stdMeter1[sec].reading_json['testMeter10'][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    <td class="">
      <a target="_blank" href="./budget_6B.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}&meter=10"> Budget Print</a>
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    <td class="" align="right">{if isset($stdMeter1[sec].testMeter_avg10)} {$stdMeter1[sec].testMeter_avg10|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_dev_10)} {$stdMeter1[sec].std_dev_10|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_10)} {$stdMeter1[sec].std_uncer_10|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per_10)} {$stdMeter1[sec].std_uncer_per_10|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_std_ct_10)} {$stdMeter1[sec].uncertinty_of_std_ct_10|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_burden_box_10)} {$stdMeter1[sec].uncertinty_of_burden_box_10|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_ct_test_10)} {$stdMeter1[sec].uncertinty_of_ct_test_10|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_std_ct_10)} {$stdMeter1[sec].accuracy_of_std_ct_10|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_ct_test_10)} {$stdMeter1[sec].accuracy_of_ct_test_10|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].com_uncer_10)} {$stdMeter1[sec].com_uncer_10|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_10)} {$stdMeter1[sec].exp_uncer_10|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc_10)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc_10|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_10)} {$stdMeter1[sec].exp_uncer_per_ue_10|string_format:"%.2f"}  {/if}</td>
    </tr>
{/if}
{if !empty($stdMeter1[sec].reading_json['testMeter9'])}
    <tr>
        <td class="" align='left'>100% Phase</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter1[sec].reading_json['testMeter9']}
                    <td class="table2" align="right">{$stdMeter1[sec].reading_json['testMeter9'][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    <td class="">
      <a target="_blank" href="./budget_6B.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}&meter=9"> Budget Print</a>
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    <td class="" align="right">{if isset($stdMeter1[sec].testMeter_avg9)} {$stdMeter1[sec].testMeter_avg9|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_dev_9)} {$stdMeter1[sec].std_dev_9|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_9)} {$stdMeter1[sec].std_uncer_9|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per_9)} {$stdMeter1[sec].std_uncer_per_9|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_std_ct_9)} {$stdMeter1[sec].uncertinty_of_std_ct_9|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_burden_box_9)} {$stdMeter1[sec].uncertinty_of_burden_box_9|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_ct_test_9)} {$stdMeter1[sec].uncertinty_of_ct_test_9|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_std_ct_9)} {$stdMeter1[sec].accuracy_of_std_ct_9|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_ct_test_9)} {$stdMeter1[sec].accuracy_of_ct_test_9|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].com_uncer_9)} {$stdMeter1[sec].com_uncer_9|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_9)} {$stdMeter1[sec].exp_uncer_9|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc_9)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc_9|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_9)} {$stdMeter1[sec].exp_uncer_per_ue_9|string_format:"%.2f"}  {/if}</td>
    </tr>
{/if}
{if !empty($stdMeter1[sec].reading_json['testMeter8'])}
    <tr>
        <td class="" align='left'>20% Phase</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter1[sec].reading_json['testMeter8']}
                    <td class="table2" align="right">{$stdMeter1[sec].reading_json['testMeter8'][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    <td class="">
      <a target="_blank" href="./budget_6B.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}&meter=8"> Budget Print</a>
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    <td class="" align="right">{if isset($stdMeter1[sec].testMeter_avg8)} {$stdMeter1[sec].testMeter_avg8|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_dev_8)} {$stdMeter1[sec].std_dev_8|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_8)} {$stdMeter1[sec].std_uncer_8|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per_8)} {$stdMeter1[sec].std_uncer_per_8|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_std_ct_8)} {$stdMeter1[sec].uncertinty_of_std_ct_8|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_burden_box_8)} {$stdMeter1[sec].uncertinty_of_burden_box_8|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_ct_test_8)} {$stdMeter1[sec].uncertinty_of_ct_test_8|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_std_ct_8)} {$stdMeter1[sec].accuracy_of_std_ct_8|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_ct_test_8)} {$stdMeter1[sec].accuracy_of_ct_test_8|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].com_uncer_8)} {$stdMeter1[sec].com_uncer_8|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_8)} {$stdMeter1[sec].exp_uncer_8|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc_8)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc_8|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_8)} {$stdMeter1[sec].exp_uncer_per_ue_8|string_format:"%.2f"}  {/if}</td>
    </tr>
{/if}
{if !empty($stdMeter1[sec].reading_json['testMeter7'])}
    <tr>
        <td class="" align='left'>5% Phase</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter1[sec].reading_json['testMeter7']}
                    <td class="table2" align="right">{$stdMeter1[sec].reading_json['testMeter7'][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    <td class="">
      <a target="_blank" href="./budget_6B.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}&meter=7"> Budget Print</a>
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    <td class="" align="right">{if isset($stdMeter1[sec].testMeter_avg7)} {$stdMeter1[sec].testMeter_avg7|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_dev_7)} {$stdMeter1[sec].std_dev_7|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_7)} {$stdMeter1[sec].std_uncer_7|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per_7)} {$stdMeter1[sec].std_uncer_per_7|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_std_ct_7)} {$stdMeter1[sec].uncertinty_of_std_ct_7|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_burden_box_7)} {$stdMeter1[sec].uncertinty_of_burden_box_7|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_ct_test_7)} {$stdMeter1[sec].uncertinty_of_ct_test_7|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_std_ct_7)} {$stdMeter1[sec].accuracy_of_std_ct_7|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_ct_test_7)} {$stdMeter1[sec].accuracy_of_ct_test_7|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].com_uncer_7)} {$stdMeter1[sec].com_uncer_7|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_7)} {$stdMeter1[sec].exp_uncer_7|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc_7)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc_7|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_7)} {$stdMeter1[sec].exp_uncer_per_ue_7|string_format:"%.2f"}  {/if}</td>
    </tr>
{/if}
{if !empty($stdMeter1[sec].reading_json['testMeter6'])}
    <tr>
        <td class="" align='left'>1% Phase</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter1[sec].reading_json['testMeter6']}
                    <td class="table2" align="right">{$stdMeter1[sec].reading_json['testMeter6'][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    <td class="">
      <a target="_blank" href="./budget_6B.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}&meter=6"> Budget Print</a>
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    <td class="" align="right">{if isset($stdMeter1[sec].testMeter_avg6)} {$stdMeter1[sec].testMeter_avg6|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_dev_6)} {$stdMeter1[sec].std_dev_6|string_format:"%.4f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_6)} {$stdMeter1[sec].std_uncer_6|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per_6)} {$stdMeter1[sec].std_uncer_per_6|string_format:"%.4f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_std_ct_6)} {$stdMeter1[sec].uncertinty_of_std_ct_6|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_burden_box_6)} {$stdMeter1[sec].uncertinty_of_burden_box_6|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].uncertinty_of_ct_test_6)} {$stdMeter1[sec].uncertinty_of_ct_test_6|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_std_ct_6)} {$stdMeter1[sec].accuracy_of_std_ct_6|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].accuracy_of_ct_test_6)} {$stdMeter1[sec].accuracy_of_ct_test_6|string_format:"%.5f"}  {/if}</td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right"></td>
    <td class="" align="right">{if isset($stdMeter1[sec].com_uncer_6)} {$stdMeter1[sec].com_uncer_6|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_6)} {$stdMeter1[sec].exp_uncer_6|string_format:"%.5f"} {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc_6)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc_6|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {/if}</td>
    <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_6)} {$stdMeter1[sec].exp_uncer_per_ue_6|string_format:"%.2f"}  {/if}</td>
    </tr>
{/if}





{else} 
    <tr>
    
    {if ($stdMeter1[sec].table_info == '16') || ($stdMeter1[sec].table_info == '17')}
        <td class="" align='left'>Test Meter Reading</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$stdMeter[sec]}
                    <td class="table2" align="right">{$stdMeter[sec][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    {else} 
        <td class="" align='left'>Std. Meter Reading</td>
        {if isset($testMeter[sec])}
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$testMeter[sec]}
                    <td class="table2" align="right">{$testMeter[sec][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
        {else}
            <td class="" colspan="5">
              <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
                <tr>
                        <td class="table2" align="right"></td>
                </tr>
              </table>
            </td>
        {/if}
    {/if}
    
    <td class="" align="right">
        {if $stdMeter1[sec].table_info == '16' || $stdMeter1[sec].table_info == '17'}
            <a target="_blank" href="./budget.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}"> Budget Print</a>
        {else}{/if}
        {if $stdMeter1[sec].table_info == '19'}
            <a target="_blank" href="./budget19.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}"> Budget Print</a>
        {else}{/if}
        {if $stdMeter1[sec].table_info == '1' || $stdMeter1[sec].table_info == '2' || $stdMeter1[sec].table_info == '3' || $stdMeter1[sec].table_info == '5_and_12' || $stdMeter1[sec].table_info == '6A' || $stdMeter1[sec].table_info == '7' || $stdMeter1[sec].table_info == '13_and_14' || $stdMeter1[sec].table_info == '8' || $stdMeter1[sec].table_info == '9' || $stdMeter1[sec].table_info == '10' || $stdMeter1[sec].table_info == '11'}
{*            1, 2, 3, 5_12, 6A,7,13_14, 8, 9, 10, 11  *}
            <a target="_blank" href="./budget_common.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}"> Budget Print</a>
        {else}{/if}
        {if $stdMeter1[sec].table_info == '15'}
{*            15  *}
            <a target="_blank" href="./budget_15.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}"> Budget Print</a>
        {else}{/if}
        {if $stdMeter1[sec].table_info == '4'}
{*            4  *}
            <a target="_blank" href="./budget_4.php?grnId={$grnId}&grnDetailId={$grnDetailId}&obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}"> Budget Print</a>
        {else}{/if}
    </td>
    <td class="" align="center">{$stdMeter1[sec].table_info}</td>
    <td class="" align="center">{$stdMeter1[sec].table_no}</td>
    <td class="" align="center">
        <a href="./observationSheet.php?obs_id={$stdMeter1[sec].obs_data_id}&obs_value={$stdMeter1[sec].key_value}">Add New</a>
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"delete"))}
            <input type="button" value="DELETE" onclick="deleteObs({$stdMeter1[sec].obs_data_id},{$stdMeter1[sec].key_value});" />
        {/if}
    </td>
    <td class="" align="right">
        {if (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
            <a href="./observationSheet.php?obs_data_id={$stdMeter1[sec].obs_data_id}&key_value={$stdMeter1[sec].key_value}">Edit</a>
        {/if}
    </td>
    {if $stdMeter1[sec].table_info == '19'}
        <td class="" align="right">{if isset($stdMeter1[sec].stdmeter_avg_new)} {$stdMeter1[sec].stdmeter_avg_new|string_format:"%.6f"}  {else} {$stdMeter1[sec].stdMeterAverage|string_format:"%.6f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].std_dev)} {$stdMeter1[sec].std_dev|string_format:"%.6f"}  {else} {$stdMeter1[sec].standardDeviation|string_format:"%.6f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].std_uncer)} {$stdMeter1[sec].std_uncer|string_format:"%.6f"}  {else} {$stdMeter1[sec].standardUncertinity|string_format:"%.6f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per)} {$stdMeter1[sec].std_uncer_per|string_format:"%.6f"}  {else} {$stdMeter1[sec].standardUncertinityperc|string_format:"%.6f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].degree_of_freedom)} {$stdMeter1[sec].degree_of_freedom|string_format:"%.6f"} {else}  {/if}</td>

        <td class="" align="right">{if isset($stdMeter1[sec].uncer_master)} {$stdMeter1[sec].uncer_master|string_format:"%.6f"}  {else} {$stdMeter1[sec].uncertinityForTypeB|string_format:"%.6f"} {/if}</td>
        <td class="" align="right"></td>
        <td class="" align="right"></td>
        <td class="" align="right">{if isset($stdMeter1[sec].acc_calibration)} {$stdMeter1[sec].acc_calibration|string_format:"%.6f"}  {else} {$stdMeter1[sec].accuracyForTypeB|string_format:"%.6f"} {/if}</td>
        <td class="" align="right"></td>
        <td class="" align="right"></td>
        <td class="" align="right">{if isset($stdMeter1[sec].resolution_new)} {$stdMeter1[sec].resolution_new|string_format:"%.6f"}  {else} {$stdMeter1[sec].resolutionTypeB|string_format:"%.6f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].uncer_of_repeat)} {$stdMeter1[sec].uncer_of_repeat|string_format:"%.6f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].uncer_of_install_error)} {$stdMeter1[sec].uncer_of_install_error|string_format:"%.6f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].uncertainty_hysteresis)} {$stdMeter1[sec].uncertainty_hysteresis|string_format:"%.6f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].reference_weight)} {$stdMeter1[sec].reference_weight|string_format:"%.6f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].eccentric_loading)} {$stdMeter1[sec].eccentric_loading|string_format:"%.6f"}  {/if}</td>

        <td class="" align="right">{if isset($stdMeter1[sec].com_uncer)} {$stdMeter1[sec].com_uncer|string_format:"%.6f"}  {else} {$stdMeter1[sec].combinedUncertinity|string_format:"%.6f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer)} {$stdMeter1[sec].exp_uncer|string_format:"%.6f"}  {else} {$stdMeter1[sec].expandedUncertinity|string_format:"%.6f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc|string_format:"%.6f"}  {else} {"0.000000"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.6f"}  {else} {"0.000000"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue)} {$stdMeter1[sec].exp_uncer_per_ue|string_format:"%.6f"}  {else} {$stdMeter1[sec].expandedUncertinityInPre|string_format:"%.6f"} {/if}</td>
    {else}
        <td class="" align="right">{if isset($stdMeter1[sec].stdmeter_avg_new)} {$stdMeter1[sec].stdmeter_avg_new|string_format:"%.4f"}  {else} {$stdMeter1[sec].stdMeterAverage|string_format:"%.4f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].std_dev)} {$stdMeter1[sec].std_dev|string_format:"%.4f"}  {else} {$stdMeter1[sec].standardDeviation|string_format:"%.4f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].std_uncer)} {$stdMeter1[sec].std_uncer|string_format:"%.4f"}  {else} {$stdMeter1[sec].standardUncertinity|string_format:"%.4f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].std_uncer_per)} {$stdMeter1[sec].std_uncer_per|string_format:"%.4f"}  {else} {$stdMeter1[sec].standardUncertinityperc|string_format:"%.4f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].degree_of_freedom)} {$stdMeter1[sec].degree_of_freedom|string_format:"%.4f"} {else}  {/if}</td>

        <td class="" align="right">{if isset($stdMeter1[sec].uncer_master)} {$stdMeter1[sec].uncer_master|string_format:"%.4f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].uncer_master_2)} {$stdMeter1[sec].uncer_master_2|string_format:"%.4f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].uncer_master_3)} {$stdMeter1[sec].uncer_master_3|string_format:"%.4f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].acc_calibration)} {$stdMeter1[sec].acc_calibration|string_format:"%.4f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].acc_calibration_2)} {$stdMeter1[sec].acc_calibration_2|string_format:"%.4f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].acc_calibration_3)} {$stdMeter1[sec].acc_calibration_3|string_format:"%.4f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].resolution_new)} {$stdMeter1[sec].resolution_new|string_format:"%.4f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].uncer_of_repeat)} {$stdMeter1[sec].uncer_of_repeat|string_format:"%.4f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].uncer_of_install_error)} {$stdMeter1[sec].uncer_of_install_error|string_format:"%.4f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].uncertainty_hysteresis)} {$stdMeter1[sec].uncertainty_hysteresis|string_format:"%.4f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].reference_weight)} {$stdMeter1[sec].reference_weight|string_format:"%.4f"}  {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].eccentric_loading)} {$stdMeter1[sec].eccentric_loading|string_format:"%.4f"}  {/if}</td>

        <td class="" align="right">{if isset($stdMeter1[sec].com_uncer)} {$stdMeter1[sec].com_uncer|string_format:"%.4f"}  {else} {$stdMeter1[sec].combinedUncertinity|string_format:"%.4f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer)} {$stdMeter1[sec].exp_uncer|string_format:"%.2f"}  {else} {$stdMeter1[sec].expandedUncertinity|string_format:"%.2f"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue_without_cmc)} {$stdMeter1[sec].exp_uncer_per_ue_without_cmc|string_format:"%.2f"}  {else} {"0.0000"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].cmc)} {$stdMeter1[sec].cmc|string_format:"%.2f"}  {else} {"0.0000"} {/if}</td>
        <td class="" align="right">{if isset($stdMeter1[sec].exp_uncer_per_ue)} {$stdMeter1[sec].exp_uncer_per_ue|string_format:"%.2f"}  {else} {$stdMeter1[sec].expandedUncertinityInPre|string_format:"%.2f"} {/if}</td>
    {/if}
    
  </tr>
  <tr>
    {if ($stdMeter1[sec].table_info == '16') || ($stdMeter1[sec].table_info == '17')}
        <td  class="" align='left'>M1</td>
        <td class="" colspan="5">
          <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
            <tr>
                {section name="sec1" loop=$testMeter[sec]}
                    <td class="table2" align="right">{$testMeter[sec][sec1]}</td>
                {/section}
            </tr>
          </table>
        </td>
    {else} 
        <td  class="" align='left'>Test Meter Reading</td>
        {if isset($stdMeter)}
            <td class="" colspan="5">
              <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
                <tr>
                    {if isset($stdMeter[sec])}
                    {section name="sec1" loop=$stdMeter[sec]}
                        <td class="table2" align="right">{$stdMeter[sec][sec1]}</td>
                    {/section}
                    {/if}
                </tr>
              </table>
            </td>
        {else}
            <td class="" colspan="5">
              <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
                <tr>
                        <td class="table2" align="right"></td>
                </tr>
              </table>
            </td>
        {/if}
    {/if}
  </tr>
      {if ($stdMeter1[sec].table_info == '16') || ($stdMeter1[sec].table_info == '17')}
        <tr>
            <td  class="" align='left'>M2</td>
            <td class="" colspan="5">
              <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
                <tr>
                    {section name="sec1" loop=$stdMeter2[sec]}
                        <td class="table2" align="right">{$stdMeter2[sec][sec1]}</td>
                    {/section}
                </tr>
              </table>
            </td>
        </tr>
    {/if} 
    {if ($stdMeter1[sec].table_info == '16') || ($stdMeter1[sec].table_info == '17')}
        <tr>
            <td  class="" align='left'>M3</td>
            <td class="" colspan="5">
              <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
                <tr>
                    {section name="sec1" loop=$stdMeter3[sec]}
                        <td class="table2" align="right">{$stdMeter3[sec][sec1]}</td>
                    {/section}
                </tr>
              </table>
            </td>
        </tr>
    {/if} 
    {if $stdMeter1[sec].table_info == '17'}
        <tr>
            <td  class="" align='left'>M4</td>
            <td class="" colspan="5">
              <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
                <tr>
                    {section name="sec1" loop=$stdMeter4[sec]}
                        <td class="table2" align="right">{$stdMeter4[sec][sec1]}</td>
                    {/section}
                </tr>
              </table>
            </td>
        </tr>
    {/if} 
    {if $stdMeter1[sec].table_info == '17'}
        <tr>
            <td  class="" align='left'>M5</td>
            <td class="" colspan="5">
              <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
                <tr>
                    {section name="sec1" loop=$stdMeter5[sec]}
                        <td class="table2" align="right">{$stdMeter5[sec][sec1]}</td>
                    {/section}
                </tr>
              </table>
            </td>
        </tr>
    {/if} 
    {if $stdMeter1[sec].table_info == '17'}
        <tr>
            <td  class="" align='left'>M6</td>
            <td class="" colspan="5">
              <table align='left' border='0' cellpadding='1' cellspacing='2' style='width:100%;'>
                <tr>
                    {section name="sec1" loop=$stdMeter6[sec]}
                        <td class="table2" align="right">{$stdMeter6[sec][sec1]}</td>
                    {/section}
                </tr>
              </table>
            </td>
        </tr>
    {/if} 
  
  <tr></tr><tr></tr><tr></tr>
  {/if}
  {/section}
  {else}
<h1 style="color:red;"><br/><br/><br/>{$recordNotFound}</h1><br>
{/if}
</table>
<script type="text/javascript" src="./js/jquerynew.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click',"#table_19_print",function() {
            var grnId = $('#table_19_print').attr('data-grnId');
            var grnDetailId  = $('#table_19_print').attr('data-grnDetailId');
            var type_1 = 0;
            if($("#print_type_1").prop('checked') == true){
                var type_1 = 1;
            }
            var type_2 = 0;
            if($("#print_type_2").prop('checked') == true){
                var type_2 = 1;
            }
            var type_3 = 0;
            if($("#print_type_3").prop('checked') == true){
                var type_3 = 1;
            }
            if((type_1 == 0) && (type_2 == 0) && (type_3 == 0)){
                alert('Select Print Type');
            } else {
                window.open("new_obs_print_table_all.php?grnId=" + grnId + "&grnDetailId=" + grnDetailId + "&type_1=" + type_1 + "&type_2=" + type_2 + "&type_3=" + type_3, '_blank');
            }
            
        });
    });
</script>