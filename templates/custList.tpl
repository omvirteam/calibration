{include file="./headStart.tpl"}
<title>Customer List</title>
<script type="text/javascript">
{literal}
function comboedit(comboValue,customerId)
{
  if(comboValue.value == 1)
    location.href ="custEntry.php?customerId="+customerId;
  if(comboValue.value == 2)
  {
    if(confirm("Are You Sure ?"))
      location.href ="custDelete.php?customerId="+customerId;
  }
}
{/literal}
</script>
{include file="./headEnd.tpl"}
<form action="{$smarty.server.PHP_SELF}" name='custList' method='POST'>
<center class="center"><h2>Customer List...!</h2></center><br>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
<tr>
  <th class="table1" align="center">&nbsp;</th>
  <th class="table1" align='center'>Customer Name</th>
  <th class="table1" align='center'>Customer Code</th>
  <th class="table1" align='center'>Address</th>
  <th class="table1" align='center'>City</th>
  <th class="table1" align='center'>State</th>
  <th class="table1" align='center'>Country</th>
  <th class="table1" align='center'>Fax</th>
  <th class="table1" align='center'>Phone 1</th>
  <th class="table1" align='center'>Phone 2</th>
  <th class="table1" align='center'>Email</th>
  <th class="table1" align='center'>Web</th>
  <th class="table1" align='center'>Contact Person</th>
</tr>
{section name="sec" loop=$custList}
<tr>
  <td class="table2" align="center">
	  <select name="customerId" id="customerId" onchange="comboedit(this, {$custList[sec].customerId});">
      <option value="0">Select Action</option>
      {if have_access_role($smarty.const.MASTER_CUSTOMER_MODULE_ID,"edit")}
        <option value="1">Edit</option>
      {/if} 
      {if have_access_role($smarty.const.MASTER_CUSTOMER_MODULE_ID,"delete")}
        <option value="2">Delete</option>
      {/if} 
    </select>
  </td>
  
	<td class="table2" align="left">{$custList[sec].custName}</td>
	<td class="table2" align="left">{$custList[sec].custCode}</td>
	<td class="table2" align="left">{$custList[sec].address}</td>
	<td class="table2" align="left">{$custList[sec].city}</td>
	<td class="table2" align="left">{$custList[sec].state}</td>
	<td class="table2" align="left">{$custList[sec].country}</td>
	<td class="table2" align="left">{$custList[sec].fax}</td>
	<td class="table2" align="left">{$custList[sec].phone1}</td>
	<td class="table2" align="left">{$custList[sec].phone2}</td>
	<td class="table2" align="left">{$custList[sec].email}</td>
	<td class="table2" align="right">{$custList[sec].web}</td>
	<td class="table2" align="right">{$custList[sec].contPerson}</td>
</tr>
{sectionelse}
<tr><td align="center" colspan="22"> <h1><font color="red"><b>Record Not Found...!</b></font></h1></td></tr>
{/section}
</table>
</form>
{include file="footer.tpl"}