{include file="./headStart.tpl"}
<script type="text/javascript">
  $(document).ready(function(){ 
    $(".add").live('click',function(){
      $("#one").clone(true).appendTo("#mainDiv").find('input[type="text"]').val('');
    });
    $(".delete").click(function(){
      $(this).parent().parent().parent().remove();
    });

     
 /////// When Data Not Enter Then Alert Coming  : Start    
     $("#masterMeterId").change(function(){ 
    	 var dataString = "masterMeterId=" + $("#masterMeterId").val();
       $.ajax({
       	 type: "GET",
         url:  "masterMeterJq.php",
         data: dataString,
         success: function(data){ 
         	$("#masterpara").html(data);
         	
            // Ajax Call At Range :Start 
            $("#masterMeterSubId").change(function(){ 
    	        var dataString = "masterMeterSubId=" + $("#masterMeterSubId").val();
                $.ajax({
       	          type: "GET",
                  url:  "masterMeterJqSub.php",
                  data: dataString,
                  success: function(data){ 
         	          $("#masterparasub").html(data);
         	          
                    // Get accuracysub With Ajax  :Start 
                    $("#masterMeterSubSubId").change(function(){ 
    	             	  var dataString = "masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
                   	    $.ajax({
       	           	    type: "GET",
                   	    url:  "masterMeterJqSubAccuracy.php",
                   	    data: dataString,
                   	    success: function(data1){ 
                   	    	$("#rangedisplay").html(data1);
                          	$('.accuracyTaken').each(function()
                           {
                           	//On Changes Of Range Calculate Accuracy % taking in to account
                            });
                   	    }	
                   	  });
                   	});
                  }	
                });
            });
                  // Get accuracysub With Ajax: End
         }	
       });
     });
       
     
   
     $(".stdMeterAverage").change(function(){ 
    	 calculateError($(this));
    	 calculateRdg($(this));
     });
      $("#testMeterAverage").change(function(){ 
    	 calculateError($(this).prev());
    	 calculateRdg($(this).prev());
     });
     
     // Resistance Value Calculate :Start
      $(".stdMeterAverageRes").change(function(){ 
    	 calculateErrorRes($(this));
    	 calculateRdgRes($(this));
     });
      $("#testMeterAverageRes").change(function(){ 
    	 calculateErrorRes($(this).prev());
    	 calculateRdgRes($(this).prev());
     });
     // Resistance Value Calculate :End
  });

{literal}
$(document).ready( function () { 
  $("#grnId").change(function(){ 
    var datastring = "grnId=" + $("#grnId").val();
    $.ajax({
      url: "./obsSheetAj.php",
      data: datastring,
      success: function(data){ 
        $('#showItem').html(data);
        $('#custCode').val($('#custCodeHidden').val());
        //
        $("#grnDetailId").change(function(){
          var datastring = "grnDetailId=" + $("#grnDetailId").val();
          $.ajax({
            url: "./obsSheetAjGrnDetailChange.php",
            data: datastring,
            success: function(data){
              $('#showDetail').html(data);
            }
          });
        });
      }
    });
  });
});
{/literal}

function calculateError(stdMeterAverage)
{
	stdMeterAverageVal =  stdMeterAverage.val();
	testMeterAverageVal = stdMeterAverage.next().val();
	stdMeterAverageObj =  stdMeterAverage.next().next();
	stdMeterAverageObj.val(testMeterAverageVal - stdMeterAverageVal);
	calculateRdg(stdMeterAverageObj);
}
function calculateRdg(stdMeterAverageObj)
{
  testMeterAverageVal = stdMeterAverageObj.next().val();
  errorVal            = stdMeterAverageObj.next().next().val();
  calculateRdgObj     = stdMeterAverageObj.next().next().next();
  calculateRdgObj.val((100*errorVal)/testMeterAverageVal);
}

function calculateErrorRes(stdMeterAverageRes)
{
	stdMeterAverageResVal  =  stdMeterAverageRes.val();
	testMeterAverageResVal =  stdMeterAverageRes.next().val();
	stdMeterAverageResObj  =  stdMeterAverageRes.next().next();
	stdMeterAverageResObj.val(testMeterAverageResVal - stdMeterAverageResVal);
	calculateRdgRes(stdMeterAverageResObj);
}
function calculateRdgRes(stdMeterAverageResObj)
{
  testMeterAverageResVal = stdMeterAverageResObj.next().val();
  errorResVal            = stdMeterAverageResObj.next().next().val();
  calculateRdgResObj     = stdMeterAverageResObj.next().next().next();
  calculateRdgResObj.val((100*errorResVal)/testMeterAverageResVal);
}
function payNow()
{
    var sameEmpty = false;
    $("select").each(function() {
        if (this.value == "")
        {
            sameEmpty = true;
            break;
        }
    });

    if(sameEmpty) {
        alert("Please select color for FREE product");
        return false;
    }

    return true;
}
</script>
{include file="./headEnd.tpl"}
<form action="{$smarty.server.PHP_SELF}" id="form1" name="form1" method='POST'>
<input type="hidden" name="grnDetailPassId" value={$grnDetailPassId} />
<center class="center"><h2>Observation Sheet</h2></center><br>
<table border='1' cellpadding='5' cellspacing='0' align='center'>
  <tr>
    <td>GRN NO.:-</font>
      <select name="grnId" id="grnId">
        <option value="0">GRN</option>
        {html_options values=$grnId output=$grnPrefixNo}
      </select>
    <td>
      <font size="4">Cust Id No.:-</font>
      <input type="text" name="custCode" id="custCode" value="" size="2" />
    
    <td colspan="9"  NOWRAP><font size="4">Master Meter.:-</font>
     <!-- <select name="masterMeterId" id="masterMeterId">
        <option value="0">Master Meter</option>
        {html_options values=$masterMeterId output=$masterMeterName}
      </select>
     --> 
     <select name="masterMeterId" id="masterMeterId">
  	 <option value="0">Select Meter</option>
       {html_options values=$meterEntryIdArray output=$meterEntryNameArray}  
    </select>
    <span id="masterpara"></span>         
    <span id="masterparasub"></span>  
    {html_select_date time="$callibrationDate" prefix="callibrationDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
    {html_select_date time="$nextYearDate" prefix="nextYearDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY"  day_value_format="%02d"}
    </td>

  </tr>
  <tr>
    <td align="center" bgcolor="lightblue">INSTRUMENT.:-<span id="showItem"></span><div id="showDetail"></div></td>
    <td align="center" bgcolor="lightblue">Make/Model :<input type="text" name="makeModel" id="makeModel" title="Make Model"  value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Instrument I.D. No. : <input type="text" name="instrumentId" id="instrumentId" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Least Count : <input type="text" name="leastCount" id="leastCount" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Accuracy : <input type="text" name="accuracy" id="accuracy" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Temperature : <input type="text" name="temperature" id="temperature" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Relative Humidity : <input type="text" name="humidity" id="humidity" value="" size="10" required=required/></td>
  </tr>    
</table>
<table align="center" border='1' cellpadding='5' cellspacing='0'  >
  <tbody id="mainDiv">
    <tr id="one" class="tableRow">
    	<td>
       <font size="4">AmpereValue</font>
          <input type="text" name="stdMeterAverage[]" class="stdMeterAverage" size="7" align="right" placeholder="STD METER"  title="stdMeterAverage" required=required>
          <input type="text" name="testMeterAverage[]"  id="testMeterAverage"  size="5" align="right" title="testMeter5" required=required>
          <input type="hidden" name="error[]"  class="error"  size="5" align="right" title="Error" readonly>
          <input type="text" name="percentageRdg[]"  class="percentageRdg"  size="5" align="right" title="percentageRdg" readonly>

          <span><input type="button" value="Add" class="add" /></span>
          <span><input type="button" value="Remove" class="delete" /></span>
        </td>
        </tr>
      </tbody>
        <tr>
  <td align="center" colspan="2"><input type="submit" name="insertBtn" value="Submit" class="button" />&nbsp;<a href="./observationSheetRegular.php">Test Another Meter</a></td>
  
</tr>
      </table>
  <div id="rangedisplay"></div> 
</table>
</form>
{include file="footer.tpl"}