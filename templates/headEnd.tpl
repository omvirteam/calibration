{if strlen($s_activId) > 0}
<script type="text/javascript">
//SuckerTree Horizontal Menu (Sept 14th, 06)
//By Dynamic Drive: http://www.dynamicdrive.com/style/
var menuids=["treemenu1"] //Enter id(s) of SuckerTree UL menus, separated by commas

function buildsubmenus_horizontal()
{
  for (var i=0; i<menuids.length; i++)
  {
    var ultags=document.getElementById(menuids[i]).getElementsByTagName("ul")
    for (var t=0; t<ultags.length; t++)
    {
  		if(ultags[t].parentNode.parentNode.id==menuids[i])
  		{ //if this is a first level submenu
  			ultags[t].style.top=ultags[t].parentNode.offsetHeight+"px" //dynamically position first level submenus to be height of main menu item
  			ultags[t].parentNode.getElementsByTagName("a")[0].className="mainfoldericon"
		  }
		  else
		  { //else if this is a sub level menu (ul)
  		  ultags[t].style.left=ultags[t-1].getElementsByTagName("a")[0].offsetWidth+"px" //position menu to the right of menu item that activated it
      	ultags[t].parentNode.getElementsByTagName("a")[0].className="subfoldericon"
		  }
      ultags[t].parentNode.onmouseover=function()
      {
        this.getElementsByTagName("ul")[0].style.visibility="visible"
      }
      ultags[t].parentNode.onmouseout=function()
      {
        this.getElementsByTagName("ul")[0].style.visibility="hidden"
      }
    }
  }
}

if (window.addEventListener)
window.addEventListener("load", buildsubmenus_horizontal, false)
else if (window.attachEvent)
window.attachEvent("onload", buildsubmenus_horizontal)

</script>
{/if}
<script>
        $(document).ajaxSend(function() {
          $("#overlay").fadeIn(300);　
        });
        $(document).ajaxComplete(function(){
          setTimeout(function(){
            $("#overlay").fadeOut(300);
          },500);
        });
     
</script>
</head>
<body>

<div id="overlay">
  <div class="cv-spinner">
    <span class="spinner"></span>
  </div>
</div>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="content">
    {if strlen($s_activId) > 0}
      <a href="logout.php" class="link">Logout</a> |
      <b>User Name : {$s_activId}</b>
      <span style="float:right"><a href="backup.php" class="link">Backup</a> </span>
    {else}
      <a href="checkLogin.php" class="link">Login</a>
    {/if}
    <br /><br />
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        {if strlen($s_activId) > 0}
        <tr>
          <td class="bluebg1">
            <div class="suckertreemenu">
            <br />
            <ul id="treemenu1">
                {if have_access_role($smarty.const.MASTER_MODULE_ID,"view")}
                    <li><a href="#">Master</a>
                      <ul>
                        {if have_access_role($smarty.const.MASTER_GRN_PREFIX_MODULE_ID,"view")}
                            <li><a href="grnPrefix.php" class="link">GRN Prefix</a></li>
                        {/if}
                        {if have_access_role($smarty.const.MASTER_ITEM_MODULE_ID,"view")}
                            <li><a href="item.php" class="link">Item</a></li>
                        {/if}
                        {if have_access_role($smarty.const.MASTER_PARAMETER_MODULE_ID,"view")}
                            <li><a href="parameterEntry.php" class="link">Parameter</a></li>
                        {/if}
                        {if have_access_role($smarty.const.MASTER_METER_MODULE_ID,"view")}
                            <li><a href="masterMeter.php" class="link">Master Meter</a></li>
                        {/if}
                        {if have_access_role($smarty.const.MASTER_METER_SUB_MODULE_ID,"view")}
                            <li><a href="masterMeterSub.php" class="link">Master Meter Sub</a></li>
                        {/if}
                        {if (have_access_role($smarty.const.MASTER_CUSTOMER_MODULE_ID,"view")) || (have_access_role($smarty.const.MASTER_CUSTOMER_MODULE_ID,"add")) || (have_access_role($smarty.const.MASTER_CUSTOMER_MODULE_ID,"edit"))}
                            <li><a href="#" class="link">Customer</a>
                              <ul style="padding-left: 0px;">
                                {if (have_access_role($smarty.const.MASTER_CUSTOMER_MODULE_ID,"add")) || (have_access_role($smarty.const.MASTER_CUSTOMER_MODULE_ID,"edit"))}
                                    <li><a href="custEntry.php" class="link">Entry</a></li>
                                {/if}
                                {if have_access_role($smarty.const.MASTER_CUSTOMER_MODULE_ID,"view")}
                                    <li><a href="custList.php" class="link">List</a></li>
                                {/if}
                              </ul>
                            </li>
                        {/if}
                        {if have_access_role($smarty.const.MASTER_SYMBOL_MODULE_ID,"view")}
                        <li><a href="symbols.php" class="link" >SYMBOLS</a></li>
                        {/if} 
                        {if have_access_role($smarty.const.MASTER_RIGHTS_MODULE_ID,"view")}
                        <li><a href="user_rights.php" class="link" >User Rights</a></li>
                        <li><a href="Userrights_log.php" class="link" >User Rights Log</a></li>
                        {/if} 
                      </ul>
                    </li>
              {/if}
              {if (have_access_role($smarty.const.GRN_MODULE_ID,"view")) || (have_access_role($smarty.const.GRN_MODULE_ID,"add")) || (have_access_role($smarty.const.GRN_MODULE_ID,"edit")) || (have_access_role($smarty.const.GRN_MODULE_ID,"item view"))}
              <li><a href="#" class="link">G.R.N.</a>
                <ul>
                {if have_access_role($smarty.const.GRN_MODULE_ID,"add")}
                  <li><a href="grnEntry.php" class="link">Entry</a></li>
                {/if} 
                {if have_access_role($smarty.const.GRN_MODULE_ID,"view")}
                  <li><a href="grnList.php" class="link">List</a></li>
                {/if}
                {if have_access_role($smarty.const.GRN_MODULE_ID,"item view")}
                  <li><a href="grnItemList.php" class="link">Item List</a></li>
                {/if} 
                </ul>
              </li>
              {/if} 
              {if (have_access_role($smarty.const.STAFF_MODULE_ID,"view")) || (have_access_role($smarty.const.STAFF_MODULE_ID,"add")) || (have_access_role($smarty.const.STAFF_MODULE_ID,"edit"))}
                <li><a href="staffEntry.php" class="link">Staff Entry</a></li>
              {/if}
              {if (have_access_role($smarty.const.OBS_MODULE_ID,"view")) || (have_access_role($smarty.const.OBS_MODULE_ID,"add")) || (have_access_role($smarty.const.OBS_MODULE_ID,"edit")) || (have_access_role($smarty.const.OBS_MODULE_ID,"cal uncer. view")) || (have_access_role($smarty.const.OBS_MODULE_ID,"inst. dues")) || (have_access_role($smarty.const.OBS_MODULE_ID,"certi. view"))}
              <li><a href="#" class="link">Observation Sheet</a>
                <ul>
                {if (have_access_role($smarty.const.OBS_MODULE_ID,"add")) || (have_access_role($smarty.const.OBS_MODULE_ID,"edit"))}
                  <li><a href="observationSheet.php" class="link">NABL ENTRY</a></li>
                {/if}
                  <!--li><a href="observationSheetRegular.php" class="link">REGULAR ENTRY</a></li-->
                  {if (have_access_role($smarty.const.OBS_MODULE_ID,"view"))}
                    <li><a href="observationList.php" class="link">List</a></li>
                  {/if}
                  <li><a href="observationPrint.php" class="link">Print</a></li>
                  {if (have_access_role($smarty.const.OBS_MODULE_ID,"cal uncer. view"))}
                    <li><a href="calUncertainty.php" class="link">Calculation of Uncertainty</a></li>
                  {/if}
                  {if (have_access_role($smarty.const.OBS_MODULE_ID,"certi. view"))}
                    <li><a href="listOfCertificate.php" class="link">List of Certificate</a></li>
                  {/if}
                  {if (have_access_role($smarty.const.OBS_MODULE_ID,"inst. dues"))}
                    <li><a href="instrumentsDues.php" class="link">Instruments Dues</a></li>
                  {/if}
                  {*{if (have_access_role($smarty.const.OBS_MODULE_ID,"view"))}
                    <li><a href="table_note.php" class="link">Table Info Notes</a></li>
                  {/if}*}
                  {if (have_access_role($smarty.const.OBS_MODULE_ID,"view"))}
                    <li><a href="table_note_new.php" class="link">Table Info Notes New</a></li>
                  {/if}
                  {if (have_access_role($smarty.const.OBS_MODULE_ID,"view"))}
                    <li><a href="table_note_new_set.php" class="link">Set Table Info Notes New</a></li>
                  {/if}
                  {if (have_access_role($smarty.const.OBS_MODULE_ID,"view"))}
                    <li><a href="setting_table_6b.php" class="link">Table 6B C.T.Class Setting</a></li>
                  {/if}
                  {if (have_access_role($smarty.const.OBS_MODULE_ID,"view"))}
                    <li><a href="setting.php" class="link">Setting</a></li>
                  {/if}
                </ul>
              </li>
              {/if}
              <li><a href="deliveryChallan.php" class="link">Delivery Challan</a></li>
            </ul>
            <br /><br /><br />
            </div>
          </td>
        </tr>
        {/if}
        <tr>
          <td class="content">
<br /><br />
	