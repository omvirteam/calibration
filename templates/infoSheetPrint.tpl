{include file="./headStartPrint.tpl"}
{include file="./headEndPrint.tpl"}
<form name="PrintForm" id="printForm" method="POST" action="{$smarty.server.PHP_SELF}">
<table border="0" cellSpacing="0" cellpadding="2">
  <tr>
    <th align="right" colspan="2">
        <a href="grnList.php"><img src="./images/logo.jpg" border="0" width="300" height="130" /></a>
    </th>
  </tr>
  <tr>
    <th align="left">
      <font size="4"><u>CUSTOMER ORDER INFO SHEET</u></font><br />
    </th>
    <th align="right">
      <font size="5">ST F 01</font><br />
    </th>
  </tr>
  <tr>
    <td align="center" colspan="2">
      <table border="0" cellSpacing="0" cellpadding="2">
        <tr>
          <td> GRN No. :</td>
          <td class="table2" align="left" colspan="2">{$grnPrefix}-{$grnNo}</td>
          <td>&nbsp;&nbsp;&nbsp;</td>
          <td align="right"> Date :</td>
          <td class="table2" align="right">{$grnDate}</td>
          <td>&nbsp;&nbsp;&nbsp;</td>
          <td> Customer Code :</td>
          <td class="table2" align="left">{$custCode}</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="center" colspan="2">
    <table border="1" cellSpacing="0" cellpadding="2">
      <tr><th colspan="16">FOLLOWING INSTRUMENTS RECEIVED FOR - <B>CALIBRATION</B></th> </tr>
      <tr>
      <th>SR NO.</th>
      <th>NAME OF INSTRUMENT</th>
      <th>INTRUMENT ID No.</th>
      <th>PARAMETERS</th>
      <th>RANGE</th>
      <th>Cali Due Date Requested By Cust.</th>
      <th>Expected Delivery Date</th>
      <th>Condition of Instrument</th>
      </tr>
     
      {section name="sec" loop=$grnDetail}
       <tr>
         <td class="table2" align="left">{$smarty.section.sec.rownum}</td>
         <td class="table2" align="left">{$grnDetail[sec].itemName}</td>
         <td class="table2" align="left">{$grnDetail[sec].itemCode}</td>
         <td class="table2" align="center">{$grnDetail[sec].parameterName}</td>
         <td class="table2" align="center">{$grnDetail[sec].range}</td>
         <td class="table2" align="center">{$grnDetail[sec].custReqDate}</td>
         <td class="table2" align="center">{$grnDetail[sec].expDelivDate}</td>
         <td class="table2" align="center">{$grnDetail[sec].grnCondition}</td>
       <tr>
       {sectionelse}
       {/section}
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="2">ANY OTHER INFO & REMARKS:<u>{$remarks}</u></td>
  </tr><tr>
    <td>Sign(Customer's Rep.):<u> {$userName}</u></td>
    <td align="right">Sign (KI):_______________________________</td>
  </tr>
  <tr>
    <td colspan="2" align="center">For delivery of instruments this receipt is required, otherwise material will not be returned.</td>
  </tr>
</table>
</form>
{include file="./footerPrint.tpl"}