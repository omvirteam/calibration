{include file="./headStart.tpl"}
<title>Table Note</title>
<script type="text/javascript">
$(document).ready(function() {
    $('#note').focus();
});

function confirmDelete()
{
	var answer = confirm("Delete Selected Recored ?")
	if (answer)
	{
	  document.messages.submit();
	}
	return false;
}
</script>
{include file="./headEnd.tpl"}
<form name="noteForm" method="post" action="{$smarty.server.PHP_SELF}" id="noteForm">
<input type="hidden" name="note_id" value="{$note_id}" />
{if (have_access_role($smarty.const.OBS_MODULE_ID,"add") && {!isset($smarty.get.note_id)}) || (have_access_role($smarty.const.OBS_MODULE_ID,"edit") && {isset($smarty.get.note_id)})}
    <table border="1" align="center" cellpadding="1" cellspacing="1">
    <tr>
      <td align="center" class="table1" colspan="2">Table Note Entry</td>
    </tr>
    <tr>
        <td><textarea style="width: 710px; height:100px;" name="note" id="note"  required="required" >{$current_note}</textarea></td>
    </tr>
    <tr>
      <td align="center"><input type="submit" name="insertBtn" value="Save" class="button" /></td>
    </tr>
    </table>
{/if}
</form>
<br /><br />
<table align="center" border="0" cellpadding="1" cellspacing="1">
{section name="sec" loop=$note_arr}
<tr>
	<td class="table2" align="center">
            {if have_access_role($smarty.const.OBS_MODULE_ID,"edit")}
                <a href="table_note_new.php?note_id={$note_arr[sec].note_id}" class="link">Edit</a>
            {/if}
	</td>
	{* <td class="table2" align="center">
            {if have_access_role($smarty.const.OBS_MODULE_ID,"delete")}
                <a href="delete_note.php?note_id={$note_arr[sec].note_id}" onclick="return confirmDelete();" id="delete{$note_arr[sec].note_id}" class="link">Delete</a>
            {/if}
	</td>*}
	<td class="table2" align="left">{$note_arr[sec].note}</td>
</tr>
{sectionelse}
<tr>
  <td class="table2" align="center" colspan="3">No Records Found.....!!!!!!!!</td>
</tr>
{/section}
</table>
{include file="footer.tpl"}