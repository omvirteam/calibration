	    </td>
		</tr>
		<tr>
			<td colspan="9">
			  <table border="0" cellspacing="0" style="border:0px solid black;margin-left:50px;font-size:x-small;font:12px arial,sans-serif;">
			  <tr><td colspan="2"><b>NOTE :</b></td></tr>
			  <tr>
			    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			    <td style="font-size:x-small;">
			      1. The reported expanded uncertainty in measurement is stated as the standard uncertainty <br/>
			      in measurement multiplied by the coverage factor k=2, which for a normal distribution <br/>
			      corresponds to a coverage probability of approximately 95%.
			    </td>
			  </tr>
			  </table>
			</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
			<td align="left" colspan="1">&nbsp;</td>
      <td align="left" colspan="10" style="font-family: 'Exotc350 DmBd BT';font-size:20px; "><b>For Krishna Instruments</b></td>
		</tr>
		<tr><td align="left" colspan="11">&nbsp;</td></tr>
 		<tr>
			<td align="left" colspan="1">&nbsp;</td>
      <td align="left" colspan="4">CALIBRATED BY : (Bhavesh Tank, CAL. ENG.)</td>
      <td align="left">&nbsp;</td>
      <td align="left" colspan="3">APPROVED BY : (D.R.SHAH, C.E.O.)</td>
			<td align="left" colspan="2">SEAL:</td>
		</tr>
  </table>
</div>
<div style="clear:both;">
</div>
</div>
<style>
@media print
{
.pagebreak { page-break-after:always; }
}
</style>
<div class="pagebreak"></div>
<div style="width:100%">
<div style="display:block;border:1px solid black;float:left;margin:240px 0 0 55px;padding:5px;">
	<table border=0 cellpadding=0 cellspacing=4 width:"100%" style="font:10px arial,sans-serif;">
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left">CERTIFICATE NO.</td>
			<td align="left">:</td>
			<td align="left">{$certificateNo}</td>
			<td align="left">&nbsp;</td>
			<td align="left">DATE OF CALLIBRATION</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.callibrationDate}</td>
			<td align="left" colspan="2">&nbsp;</td>
		</tr> 
		<tr>
      <td align="left" colspan="2">&nbsp;</td>
      <td align="left">NO OF SHEETS.</td>
			<td align="left">:</td>
			<td align="left">{counter}{$currentpage} Of {$totalPages}</td>
			<td align="left">&nbsp;&nbsp;&nbsp;</td>
			<td align="left">Next Recommended Calibration date</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.nextYearDate}</td>
		  <td align="left" colspan="2">&nbsp;</td>
    </tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left">GRN No.</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.grnNo}</td>
			<td align="left" colspan="6">&nbsp;</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="9"><b>8.OBSERVATION</b></td>
		</tr>
		<tr>
		  <td colspan="11">
