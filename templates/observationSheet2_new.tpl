{include file="./headStart.tpl"}
<title>Observation Entry</title>
<script type="text/javascript" src="./js/jquerynew.min.js"></script>
<script type="text/javascript" src="./js/save_obs_entry.js"></script>
<script type="text/javascript">
    

$(document).ready(function(){
    $(document).on('change',"#consider_ue",function() {
        if($(this).prop('checked') == true){
            $('.ue_unit_field').show();
        } else {
            $('.ue_unit_field').hide();
        }
    });
    
    $(document).on('change',"#table_info_id",function() {
        $('.ue_unit_field').hide();
        $('#table_data_div').html('');
        $('#obsDetail').html('');
        if($(this).val() != ''){
            if($(this).val() == '18'){
                $('.table_19_field_obs_type').show();
                $('.table_19_field').show();
                $('#obs_type_id').attr('required', 'required');
                $('#weight_id').attr('required', 'required');
                $('#uuc_range').attr('required', 'required');
            } else {
                $('.table_19_field_obs_type').hide();
                $('.table_19_field').hide();
                $('#obs_type_id').removeAttr('required', 'required');
                $('#weight_id').removeAttr('required', 'required');
                $('#uuc_range').removeAttr('required', 'required');
                
                getDetails();
                get_range();
                $.ajax({
                    url: "./obs_table_data.php",
                    type: "POST",
                    data: "table_info_no="+$("#table_info_id option:selected").html()+"&edit_reading_json_data="+{$edit_reading_json_data|json_encode}+"&edit_uuc_json_data="+{$edit_uuc_json_data|json_encode},
                    success: function(response){
                      $('#table_data_div').html(response);
                      $("input").attr("autocomplete", "off");
                      if($("#table_info_id").val() == 13 || $("#table_info_id").val() == 11 || $("#table_info_id").val() == 17){
                          $('.table_11_15_10_field').show();
                          $('#uncertainty_calibration').attr('required', 'required');
                      } else {
                          $('.table_11_15_10_field').hide();
                          $('#uncertainty_calibration').removeAttr('required', 'required');
                      }
                      if($("#table_info_id").val() == 2 || $("#table_info_id").val() == 16){
                        $('.table_16_field').show();
                      } else {
                          $('.table_16_field').hide();
                      }
                      if($("#table_info_id").val() == 5){
                        $('.table_2_field').show();
                        if($('#consider_ue').prop('checked') == true){
                            $('.ue_unit_field').show();
                        } else {
                            $('.ue_unit_field').hide();
                        }
                      } else {
                          $('.table_2_field').hide();
                      }
                      if($("#table_info_id").val() == 4){
                        $('.table_4_field').show();
                        $('.table_4_and_6B_field').show();
                        $('#uuc_range_range').attr('required', 'required');
                        $('#masterMeterId_2').attr('required', 'required');
                        $('#masterMeterSubId_2').attr('required', 'required');
                        $('#masterMeterSubSubId_2').attr('required', 'required');
                        $("#accuracy_of_rdg_2").attr('required', 'required');
                        $("#accuracy_of_fs_2").attr('required', 'required');
                        $("#master_stability_2").attr('required', 'required');
                        $("#degree_of_freedom_2").attr('required', 'required');
                        $("#uncertinty_from_tracebility_2").attr('required', 'required');
                        $("#resolution_of_master_2").attr('required', 'required');
                        $("#accuracy_taking_account_2").attr('required', 'required');
                      } else {
                          $('.table_4_field').hide();
                          $('.table_4_and_6B_field').hide();
                        $('#uuc_range_range').removeAttr('required', 'required');
                        $('#masterMeterId_2').removeAttr('required', 'required');
                        $('#masterMeterSubId_2').removeAttr('required', 'required');
                        $('#masterMeterSubSubId_2').removeAttr('required', 'required');
                        $("#accuracy_of_rdg_2").removeAttr('required', 'required');
                        $("#accuracy_of_fs_2").removeAttr('required', 'required');
                        $("#master_stability_2").removeAttr('required', 'required');
                        $("#degree_of_freedom_2").removeAttr('required', 'required');
                        $("#uncertinty_from_tracebility_2").removeAttr('required', 'required');
                        $("#resolution_of_master_2").removeAttr('required', 'required');
                        $("#accuracy_taking_account_2").removeAttr('required', 'required');
                      }
                      if($("#table_info_id").val() == 8){
                        $('.table_7_field').show();
                        $('#ratio_bottom_part').attr('required', 'required');
                        $('#ratio_bottom_part_range').attr('required', 'required');
                        $('#uuc_range_range').attr('required', 'required');
                      } else {
                          $('.table_7_field').hide();
                          $('#ratio_bottom_part').removeAttr('required', 'required');
                          $('#ratio_bottom_part_range').removeAttr('required', 'required');
                          $('#uuc_range_range').removeAttr('required', 'required');
                      }
                      if($("#table_info_id").val() == 10){
                        $('.table_9_field').show();
                      } else {
                          $('.table_9_field').hide();
                      }
                      if($("#table_info_id").val() == 11){
                        $('.table_15_field').show();
                        $('#repeatibility').attr('required', 'required');
                        $('#sensor').attr('required', 'required');
                        $('#start_time').attr('required', 'required');
                        $('#stop_time').attr('required', 'required');
                        $('#flow_meter_size').attr('required', 'required');
                        $('#line_size').attr('required', 'required');
                        $('#pipe_mate_thick').attr('required', 'required');
                        $('#cali_fluid').attr('required', 'required');
                      } else {
                          $('.table_15_field').hide();
                          $('#repeatibility').removeAttr('required', 'required');
                          $('#sensor').removeAttr('required', 'required');
                          $('#start_time').removeAttr('required', 'required');
                          $('#stop_time').removeAttr('required', 'required');
                          $('#flow_meter_size').removeAttr('required', 'required');
                          $('#line_size').removeAttr('required', 'required');
                          $('#pipe_mate_thick').removeAttr('required', 'required');
                          $('#cali_fluid').removeAttr('required', 'required');
                      }
                      if($("#table_info_id").val() == 12){
                        $('.table_4_and_6B_field').show();
                        $('.table_6B_field').show();
                        $('.table_6B_field_new').show();
                        $('#ct_class').attr('required', 'required');
                        $('#masterMeterId_3').attr('required', 'required');
                        $('#masterMeterSubId_3').attr('required', 'required');
                        $('#masterMeterSubSubId_3').attr('required', 'required');
                        $("#accuracy_of_rdg_3").attr('required', 'required');
                        $("#accuracy_of_fs_3").attr('required', 'required');
                        $("#master_stability_3").attr('required', 'required');
                        $("#degree_of_freedom_3").attr('required', 'required');
                        $("#uncertinty_from_tracebility_3").attr('required', 'required');
                        $("#resolution_of_master_3").attr('required', 'required');
                        $("#accuracy_taking_account_3").attr('required', 'required');
                      } else {
                          $('.table_4_and_6B_field').hide();
                          $('.table_6B_field').hide();
                          $('.table_6B_field_new').hide();
                          $('#ct_class').removeAttr('required', 'required');
                          $('#masterMeterId_3').removeAttr('required', 'required');
                            $('#masterMeterSubId_3').removeAttr('required', 'required');
                            $('#masterMeterSubSubId_3').removeAttr('required', 'required');
                            $("#accuracy_of_rdg_3").removeAttr('required', 'required');
                            $("#accuracy_of_fs_3").removeAttr('required', 'required');
                            $("#master_stability_3").removeAttr('required', 'required');
                            $("#degree_of_freedom_3").removeAttr('required', 'required');
                            $("#uncertinty_from_tracebility_3").removeAttr('required', 'required');
                            $("#resolution_of_master_3").removeAttr('required', 'required');
                            $("#accuracy_taking_account_3").removeAttr('required', 'required');
                      }
                      if($("#table_info_id").val() == 7){
                        $('.table_6A_field').show();
                        $('#cal_reading_range_1').attr('required', 'required');
                        $('#cal_reading_range_2').attr('required', 'required');
                      } else {
                          $('.table_6A_field').hide();
                          $('#cal_reading_range_1').removeAttr('required', 'required');
                          $('#cal_reading_range_2').removeAttr('required', 'required');
                      }
                      if($("#table_info_id").val() == 14){
                        $('.range_class').html("UUC");
                        $('.table_14_field').show();
                        $('#uuc_range_2').attr('required', 'required');
                      } else {
                          $('.range_class').html("UUC Range Max ");
                          $('.table_14_field').hide();
                          $('#uuc_range_2').removeAttr('required', 'required');
                      }
                      if($("#table_info_id").val() == 13){
                        $('.table_11_field').show();
                        $('#ocillation').attr('required', 'required');
                        $('#frequency').attr('required', 'required');
                        $('#rpm').attr('required', 'required');
                        $('#least_count_for_rpm').attr('required', 'required');
                      } else {
                          $('.table_11_field').hide();
                          $('#ocillation').removeAttr('required', 'required');
                          $('#frequency').removeAttr('required', 'required');
                          $('#rpm').removeAttr('required', 'required');
                          $('#least_count_for_rpm').removeAttr('required', 'required');
                      }
                      {*if($("#table_info_id").val() == 15){
                        $('.table_regular_1_field').show();
                        $('#ctr').attr('required', 'required');
                        $('#class').attr('required', 'required');
                        $('#cos').attr('required', 'required');
                      } else {
                          $('.table_regular_1_field').hide();
                          $('#ctr').removeAttr('required', 'required');
                          $('#class').removeAttr('required', 'required');
                          $('#cos').removeAttr('required', 'required');
                      }*}
                      if($("#table_info_id").val() == 17){
                        $('.table_10_field').show();
                        $('#span_in').attr('required', 'required');
                        $('#col_head_1').attr('required', 'required');
                        $('#col_head_1_least_count').attr('required', 'required');
                      } else {
                          $('.table_10_field').hide();
                          $('#span_in').removeAttr('required', 'required');
                          $('#col_head_1').removeAttr('required', 'required');
                          $('#col_head_1_least_count').removeAttr('required', 'required');
                      }
                      if($("#table_info_id").val() == 19){
                        $('.table_18_field').show();
                        $('#ac_voltage').attr('required', 'required');
                        $('#ac_current').attr('required', 'required');
                        $('#power_factor').attr('required', 'required');
                      } else {
                          $('.table_18_field').hide();
                          $('#ac_voltage').removeAttr('required', 'required');
                          $('#ac_current').removeAttr('required', 'required');
                          $('#power_factor').removeAttr('required', 'required');
                      }
                      if(($("#table_info_id").val() == 4) || ($("#table_info_id").val() == 12)){
                        $('.table_4_and_6B_field').show();
                        $('#masterMeterId_2').attr('required', 'required');
                        $('#masterMeterSubId_2').attr('required', 'required');
                        $('#masterMeterSubSubId_2').attr('required', 'required');
                        $("#accuracy_of_rdg_2").attr('required', 'required');
                        $("#accuracy_of_fs_2").attr('required', 'required');
                        $("#master_stability_2").attr('required', 'required');
                        $("#degree_of_freedom_2").attr('required', 'required');
                        $("#uncertinty_from_tracebility_2").attr('required', 'required');
                        $("#resolution_of_master_2").attr('required', 'required');
                        $("#accuracy_taking_account_2").attr('required', 'required');
                      } else {
                        $('.table_4_and_6B_field').hide();
                        $('#uuc_range_range').removeAttr('required', 'required');
                        $('#masterMeterId_2').removeAttr('required', 'required');
                        $('#masterMeterSubId_2').removeAttr('required', 'required');
                        $('#masterMeterSubSubId_2').removeAttr('required', 'required');
                        $("#accuracy_of_rdg_2").removeAttr('required', 'required');
                        $("#accuracy_of_fs_2").removeAttr('required', 'required');
                        $("#master_stability_2").removeAttr('required', 'required');
                        $("#degree_of_freedom_2").removeAttr('required', 'required');
                        $("#uncertinty_from_tracebility_2").removeAttr('required', 'required');
                        $("#resolution_of_master_2").removeAttr('required', 'required');
                        $("#accuracy_taking_account_2").removeAttr('required', 'required');
                      }
                      {*if($('#edit_grn_id').val() != '/'){
                          $('#stdMeter_1').val('99');
                      }*}
                      var image_url = $("#table_info_id").find(':selected').data('image_url');
    {*                  console.log(image_url);*}
                        if(typeof(image_url) !== 'undefined'){
                            $('#image_span').html('<img style="float: right" width="400px"  src=".'+image_url+'" />');
                        } else {
                            $('#image_span').html('');
                        }
                    }
                  });
              }
        } else {
           $('#table_data_div').html('');
        }
    });
    
    $(document).on('change',"#obs_type_id",function() {
        $('#table_data_div').html('');
        $('#obsDetail').html('');
        if($(this).val() != ''){
                if($(this).val() == '2'){
                        $('.table_19_field2').show();
                        $('#d1').attr('required', 'required');
                        $('#d2').attr('required', 'required');
                    } else {
                        $('.table_19_field2').hide();
                        $('#d1').removeAttr('required', 'required');
                        $('#d2').removeAttr('required', 'required');
                    }
                getDetails();
                get_range();
                $.ajax({
                    url: "./obs_table_data.php",
                    type: "POST",
                    data: "obs_type_id="+$(this).val()+"&table_info_no="+$("#table_info_id option:selected").html()+"&edit_reading_json_data="+{$edit_reading_json_data|json_encode}+"&edit_uuc_json_data="+{$edit_uuc_json_data|json_encode},
                    success: function(response){
                      $('#table_data_div').html(response);
                      $("input").attr("autocomplete", "off");
                      var image_url = $("#table_info_id").find(':selected').data('image_url');
    {*                  console.log(image_url);*}
                        if(typeof(image_url) !== 'undefined'){
                            $('#image_span').html('<img style="float: right" width="400px"  src=".'+image_url+'" />');
                        } else {
                            $('#image_span').html('');
                        }
                    }
                  });
        } else {
           $('#table_data_div').html('');
        }
    });
        
    $(document).on('input', ".num_only", function () {
        this.value = this.value.replace(/[^\d\.\-]/g, '');
    });

    getMasterMeterData();
    {if !empty($edit_master_meter_id_2)}
        getMasterMeterData_2();
    {/if}
    {if !empty($edit_master_meter_id_3)}
        getMasterMeterData_3();
    {/if}
    if($('#edit_grn_id').val() != '/'){
        $('#grnId').change();
    }

    $("#form1").submit(function(e){
        e.preventDefault();
        if($("#table_info_id").val() == '1' || $("#table_info_id").val() == '3' || $("#table_info_id").val() == '4' || $("#table_info_id").val() == '5' || $("#table_info_id").val() == '7' || $("#table_info_id").val() == '8' ||  $("#table_info_id").val() == '9' || $("#table_info_id").val() == '10' || $("#table_info_id").val() == '11' || $("#table_info_id").val() == '14' || $("#table_info_id").val() == '17'  || $("#table_info_id").val() == '19') {
            save_table_1();
        }
        if($("#table_info_id").val() == '2'){
            save_table_16();
        }
        if($("#table_info_id").val() == '6'){
            save_table_5();
        }
        if($("#table_info_id").val() == '18'){
            save_table_19();
        }
        if($("#table_info_id").val() == '12'){
            save_table_6b();
        }
        if($("#table_info_id").val() == '13'){
            save_table_11();
        }
        if($("#table_info_id").val() == '15'){
            save_table_1();
        }
        if($("#table_info_id").val() == '16'){
            save_table_17();
        }

    });

    $(document).on('click',"#save_and_addnew",function() {
        var new_entry = true;
        if($("#table_info_id").val() == '1' || $("#table_info_id").val() == '3' || $("#table_info_id").val() == '4' || $("#table_info_id").val() == '5' || $("#table_info_id").val() == '7' || $("#table_info_id").val() == '8' ||  $("#table_info_id").val() == '9' || $("#table_info_id").val() == '10' || $("#table_info_id").val() == '11' || $("#table_info_id").val() == '14' || $("#table_info_id").val() == '17'  || $("#table_info_id").val() == '19') {
            save_table_1(new_entry);
        }
        if($("#table_info_id").val() == '2'){
            save_table_16(new_entry);
        }
        if($("#table_info_id").val() == '6'){
            save_table_5(new_entry);
        }
        if($("#table_info_id").val() == '18'){
            save_table_19(new_entry);
        }
        if($("#table_info_id").val() == '12'){
            save_table_6b(new_entry);
        }
        if($("#table_info_id").val() == '13'){
            save_table_11(new_entry);
        }
        if($("#table_info_id").val() == '15'){
            save_table_1(new_entry);
        }
        if($("#table_info_id").val() == '16'){
            save_table_17(new_entry);
        }
    });
});

function htmlEntities(str) {
    return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function getDateByPara(){
}
function getDetails()
{
    var datastring = "grnId=" + $('#grnId').val() + "&grnDetailId=" + $('#grnDetailId').val() + "&table_info_id=" + $('#table_info_id').val();
{*    console.log(datastring);*}
    $.ajax({
      url: "./obsDetailAj.php",
      data: datastring,
      success: function(response){
        $('#obsDetail').html(response);
      }
    });
}

function get_range(){
    {if empty($obs_data_id) && empty($obs_id)}
        var table_selected_val2 = $('#table_no').val() || 0;
        var datastring = "&grn_detail_id=" + $('#grnDetailId').val() + "&table_info_id=" + $('#table_info_id').val()+ "&table_no=" + table_selected_val2;
        $.ajax({
          url: "./obsSheetAjGrnDetailChange.php",
          data: datastring,
          datatype:'json',
          success: function(response){
    {*          console.log(response);*}
            var json = $.parseJSON(response);
            $('#uuc_range_min').val(json['min_range']);
            $('#uuc_range').val(json['max_range']);
            if($('#table_info_id').val() == '11'){
                if(json['table_title_right'] == ''){
                    $('#table_title_right').val('Flow');
                } else {
                    $('#table_title_right').val(json['table_title_right']);
                }
            } else {
                $('#table_title_right').val(json['table_title_right']);
            }
          }
        });
    {/if}
}



function deleteObs(obs_data_id, key_value)
{
    var answer = confirm("Are You Sure Delete Your Recored ?")
    if (answer == true)
    {
      var datastring = "grnObservationId=" + obs_data_id +"&key_value=" + key_value;
        $.ajax({
          url: "./obsDetailDelete.php?grnObservationId=" + obs_data_id +"&key_value=" + key_value,
          data: datastring,
          method: 'POST',
          success: function(response){
            getDetails();
          }
        });
  }
    return false;
}

function getGrnData()
{
    var datastring = "grnId=" + $("#grnId").val();
    $.ajax({
        url: "./obsSheetAj.php",
        data: datastring,
        success: function(data){ 
            $('#showItem').html(data);
            $('#custCode').val($('#custCodeHidden').val());
            if($('#edit_grn_detail_id').val() != '/') {
                $('#grnDetailId').val($('#edit_grn_detail_id').val());
                var datastring = "grnDetailId=" + $("#grnDetailId").val();
                if($('#table_info_id').val() != ''){
                    getDetails();
                }
                $.ajax({
                    url: "./obsSheetAjGrnDetailChange.php",
                    data: datastring,
                    success: function(data) {
                        $('#showDetail').html(data);
                        $('#grn_uuc_range').val('');
                        $('#grn_uuc_range').val($('#uuc_range_hid').val());
                        $('#instrumentId').val('');
                        $('#instrumentId').val($('#instrumentId_hid').val());
                        $('#makeModel').val('');
                        $('#makeModel').val($('#make_modal_hid').val());
                        {*$('#temperature').val('');
                        $('#temperature').val($('#temp_hid').val());
                        $('#humidity').val('');
                        $('#humidity').val($('#humidity_hid').val());*}
                        $('#uuc_location').val('');
                        $('#uuc_location').val($('#uuc_location_hid').val());
                        $('#approved_by').val($('#approved_by_hid').val());
                        $('#phase_type_id').val('{$edit_phase_type_id}');
                        $.ajax({
                            url: "./obsSheetAjGrnDetailChange.php",
                            data: "grnDetailId=" + $("#grnDetailId").val() +"&is_table=1"+"&edit_table_no="+{$edit_table_no},
                            success: function(data){
                               $('#showDetail_table').html(data);
                            }
                        });
                        {if empty($obs_data_id) && empty($obs_id)}
                        $.ajax({
                            url: "./obsSheetGetTemp.php",
                            data: "grnDetailId=" + $("#grnDetailId").val() +"&masterMeterId=" + $("#masterMeterId").val() +"",
                            datatype:'json',
                            success: function(data){
                                var json = $.parseJSON(data);
                                $('#temperature').val('');
                                $('#humidity').val('');
                                $('#temperature').val(json['master_temp']);
                                $('#humidity').val(json['master_humidity']);
                            }
                        });
                        {/if}
                        // additional change by girish - start
                        $.ajax({
                            url: "obsSheetAjCertiNo.php", 
                            data: datastring, 
                            success: function(data) {
                                $('#selfCertiNo').val(data);
                                $('#cali_location').trigger('click');
                            }
                        });
                        // additional change by girish - end
                    }
                });
                $('#uuc_range_print_table').val('{$edit_uuc_range_print_table}');
                $('#leastCount').val('{$edit_leastCount}');
                $('#accuracy').val('{$edit_accuracy}');
                $('#certiRemarks').val('{$edit_certiRemarks}');
                $('#makeModel').val('{$edit_makeModel}');
                $('#instrumentId').val('{$edit_instrumentId}');
                $('#uuc_range').val('{$edit_uut_range}');
                $('#leastCount_print').val('{$edit_leastCount_print}');
                $('#uuc_location').val('{$edit_uuc_location}');
                $('#table_info_id').val('{$edit_table_info_id}');
                $('#uuc_range_min').val('{$edit_uut_range_min}');
                $('#table_title_right').val('{$edit_table_title_right}');
                $('#cos').val('{$edit_cos}');
                $('#class').val('{$edit_class}');
                $('#ctr').val('{$edit_ctr}');
                $('#start_time').val('{$edit_start_time}');
                $('#stop_time').val('{$edit_stop_time}');
                $('#flow_meter_size').val({$edit_flow_meter_size|@json_encode});
{*                $('#flow_meter_size').val('{$edit_flow_meter_size}');*}
                $('#line_size').val({$edit_line_size|@json_encode});
                $('#pipe_mate_thick').val({$edit_pipe_mate_thick|@json_encode});
                $('#cali_fluid').val('{$edit_cali_fluid}');
                $('#cal_reading_range_1').val('{$edit_cal_reading_range_1}');
                $('#cal_reading_range_2').val('{$edit_cal_reading_range_2}');
                $('#ac_voltage').val('{$edit_ac_voltage}');
                $('#ac_current').val('{$edit_ac_current}');
                $('#power_factor').val('{$edit_power_factor}');
                $('#start_reading_of_kwh').val('{$edit_start_reading_of_kwh}');
                $('#end_reading_of_kwh').val('{$edit_end_reading_of_kwh}');
                $('#uuc_range_sec').val('{$edit_uut_range_sec}');
                $('#ratio_bottom_part').val('{$edit_ratio_bottom_part}');
                $('#ratio_bottom_part_range').val('{$edit_ratio_bottom_part_range}');
                $('#uuc_range_range').val('{$edit_uuc_range_range}');
                $('#burden_6b').val('{$edit_burden_6b}');
                $('#ratio_6b').val('{$edit_ratio_6b}');
                $('#col_head_1').val('{$edit_col_head_1}');
                $('#span_in').val('{$edit_span_in}');
                $('#uuc_range_2').val('{$edit_uuc_range_2}');
                $('#voltage').val('{$edit_voltage}');
                $('#ocillation').val('{$edit_ocillation}');
                $('#col_head_1_least_count').val('{$edit_col_head_1_least_count}');
                $('#least_count_for_rpm').val('{$edit_least_count_for_rpm}');
                $('#uncertainty_calibration').val('{$edit_uncertainty_calibration}');
                $('#frequency').val('{$edit_frequency}');
                $('#rpm').val('{$edit_rpm}');
                $('#sensor').val('{$edit_sensor}');
                $('#repeatibility').val('{$edit_repeatibility}');
                $('#temp_of_water').val('{$edit_temp_of_water}');
                $('#humidity').val('{$edit_humidity}');
                $('#obs_type_id').val('{$edit_obs_type_id}').change();
                $('#weight_id').val('{$edit_weight_id}');
                $('#d1').val('{$edit_d1}');
                $('#d2').val('{$edit_d2}');
                $('#temperature').val('{$edit_temperature}');
                $('#confidence_level').val('{$edit_confidence_level}');
                $('#covarage_factor').val('{$edit_covarage_factor}');
                $('#table_no').val('{$edit_table_no}');
                $('#ue_unit').val('{$edit_ue_unit}');
                var consider_ue = '{$edit_consider_ue}';
                if(consider_ue == 1){
                    $('#consider_ue').attr('checked',true);
                }

                {if !empty($obs_data_id) || !empty($obs_id)}
                    $('#accuracy_taking_account').val('{$edit_master['accuracy_taking_account']}');
                    $('#accuracy_taking_account_2').val('{if isset($edit_master['accuracy_taking_account_2'])}{$edit_master['accuracy_taking_account_2']}{/if}');
                    $('#accuracy_taking_account_3').val('{if isset($edit_master['accuracy_taking_account_3'])}{$edit_master['accuracy_taking_account_3']}{/if}');
                    $('#accuracy_of_rdg').val('{if isset($edit_master['accuracy_of_rdg'])}{$edit_master['accuracy_of_rdg']}{/if}');
                    $('#accuracy_of_rdg_2').val('{if isset($edit_master['accuracy_of_rdg_2'])}{$edit_master['accuracy_of_rdg_2']}{/if}');
                    $('#accuracy_of_rdg_3').val('{if isset($edit_master['accuracy_of_rdg_3'])}{$edit_master['accuracy_of_rdg_3']}{/if}');
                    $('#accuracy_of_fs').val('{if isset($edit_master['accuracy_of_fs'])}{$edit_master['accuracy_of_fs']}{/if}');
                    $('#accuracy_of_fs_2').val('{if isset($edit_master['accuracy_of_fs_2'])}{$edit_master['accuracy_of_fs_2']}{/if}');
                    $('#accuracy_of_fs_3').val('{if isset($edit_master['accuracy_of_fs_3'])}{$edit_master['accuracy_of_fs_3']}{/if}');
                    $('#master_stability').val('{if isset($edit_master['master_stability'])}{$edit_master['master_stability']}{/if}');
                    $('#master_stability_2').val('{if isset($edit_master['master_stability_2'])}{$edit_master['master_stability_2']}{/if}');
                    $('#master_stability_3').val('{if isset($edit_master['master_stability_3'])}{$edit_master['master_stability_3']}{/if}');
                    $('#resolution_of_master').val('{if isset($edit_master['resolution_of_master'])}{$edit_master['resolution_of_master']}{/if}');
                    $('#resolution_of_master_2').val('{if isset($edit_master['resolution_of_master_2'])}{$edit_master['resolution_of_master_2']}{/if}');
                    $('#resolution_of_master_3').val('{if isset($edit_master['resolution_of_master_3'])}{$edit_master['resolution_of_master_3']}{/if}');
                    $('#degree_of_freedom').val('{if isset($edit_master['degree_of_freedom'])}{$edit_master['degree_of_freedom']}{/if}');
                    $('#degree_of_freedom_2').val('{if isset($edit_master['degree_of_freedom_2'])}{$edit_master['degree_of_freedom_2']}{/if}');
                    $('#degree_of_freedom_3').val('{if isset($edit_master['degree_of_freedom_3'])}{$edit_master['degree_of_freedom_3']}{/if}');
                    $('#uncertinty_from_tracebility').val('{if isset($edit_master['uncertinty_from_tracebility'])}{$edit_master['uncertinty_from_tracebility']}{/if}');
                    $('#uncertinty_from_tracebility_2').val('{if isset($edit_master['uncertinty_from_tracebility_2'])}{$edit_master['uncertinty_from_tracebility_2']}{/if}');
                    $('#uncertinty_from_tracebility_3').val('{if isset($edit_master['uncertinty_from_tracebility_3'])}{$edit_master['uncertinty_from_tracebility_3']}{/if}');
                {/if}
                $('#table_info_id').change();
                {* console.log('{$edit_reading_json_data|json_encode}');*}
            }
            $("#grnDetailId").change(function() {
                var datastring = "grnDetailId=" + $("#grnDetailId").val();
                if($('#table_info_id').val() != ''){
                    getDetails();
                }
                $.ajax({
                    url: "./obsSheetAjGrnDetailChange.php",
                    data: datastring,
                    success: function(data){
                        $('#showDetail').html(data);
                        $('#grn_uuc_range').val('');
                        $('#grn_uuc_range').val($('#uuc_range_hid').val());
                        $('#instrumentId').val('');
                        $('#instrumentId').val($('#instrumentId_hid').val());
                        $('#makeModel').val('');
                        $('#makeModel').val($('#make_modal_hid').val());
                        {*$('#temperature').val('');
                        $('#temperature').val($('#temp_hid').val());
                        $('#humidity').val('');
                        $('#humidity').val($('#humidity_hid').val());*}
                        $('#uuc_location').val('');
                        $('#uuc_location').val($('#uuc_location_hid').val());
                        $('#approved_by').val($('#approved_by_hid').val());
                        $.ajax({
                            url: "./obsSheetAjGrnDetailChange.php",
                            data: "grnDetailId=" + $("#grnDetailId").val() +"&is_table=1",
                            success: function(data){
                                $('#showDetail_table').html(data);
                            }
                        });
                        $.ajax({
                            url: "./obsSheetGetTemp.php",
                            data: "grnDetailId=" + $("#grnDetailId").val() +"&masterMeterId=" + $("#masterMeterId").val() +"",
                            datatype:'json',
                            success: function(data){
                                var json = $.parseJSON(data);
                                $('#temperature').val('');
                                $('#humidity').val('');
                                $('#temperature').val(json['master_temp']);
                                $('#humidity').val(json['master_humidity']);
                            }
                        });
                        // additional change by girish - start
                        $.ajax({
                            url: "obsSheetAjCertiNo.php", 
                            data: datastring, 
                            success: function(data) { 
                                $('#selfCertiNo').val(data);
                                $('#cali_location').trigger('click');
                            }
                        });
                        // additional change by girish - end
                    }
                });
            });
        }
    });
}

function get_table_no(){
    var table_selected_val = $('#table_no').val() || 0;
    $.ajax({
        url: "./obsSheetAjGrnDetailChange.php",
        data: "grnDetailId=" + $("#grnDetailId").val() +"&is_table=1"+"&is_table_selected_val="+table_selected_val,
        success: function(data){
          $('#showDetail_table').html(data);
        }
    });
}

function get_table_title(){
    var table_selected_val1 = $('#table_no').val() || 0;
    var datastring = "grnDetailId=" + $("#grnDetailId").val() + "&table_no=" + table_selected_val1;
    $.ajax({
        url: "./obsSheetAjGrnDetailChange.php",
        data: datastring,
        datatype:'json',
        success: function(data){
            var json = $.parseJSON(data);
            $('#table_title').val('');
{*            console.log('sss'.json);*}
            $('#table_title').val(json);
        }
    });
}
function getMasterMeterData()
{
    if($("#masterMeterId").val() != 0) {
        var dataString = "masterMeterId=" + $("#masterMeterId").val();
        $.ajax({
            type: "GET",
            url:  "masterMeterJq.php",
            data: dataString,
            success: function(data) { 
                $("#masterpara").html(data);
                $("#masterMeterSubId").focus();
                // Ajax Call At Range :Start 
                $("#masterMeterSubId").change(function() { 
                    var dataString = "masterMeterSubId=" + $("#masterMeterSubId").val();
                    $.ajax({
                        type: "GET",
                        url:  "masterMeterJqSub.php",
                        data: dataString,
                        success: function(data) {
                            $("#masterparasub").html(data);
                            $("#masterMeterSubSubId").focus();
                            if($('#edit_master_meter_sub_sub_id') != '/') {
                                $("#masterMeterSubSubId").val($('#edit_master_meter_sub_sub_id').val());
                                var dataString = "masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
                                var dataString2 = "masterMeterSubSubId_2=" + $("#masterMeterSubSubId").val();
                                $.ajax({
                                    type: "GET",
                                    url:  "masterMeterJqSubAccuracy.php",
                                    data: dataString,
                                    success: function(data1) { 
                                        $("#rangedisplay").html(data1);
                                    }
                                });
                                {if (empty($obs_data_id)) && (empty($obs_id))}
                                $.ajax({
                                    type: "GET",
                                    url:  "masterMeterJqSubAccuracy.php",
                                    data: dataString2,
                                    success: function(data1) { 
                                        var json = $.parseJSON(data1);
                                        $("#accuracy_of_rdg").val(json.accuracySubSub);
                                        $("#accuracy_of_fs").val(json.accuracySubSubFS);
                                        $("#master_stability").val(json.stability);
                                        $("#degree_of_freedom").val(json.degreeOfFreedom);
                                        $("#uncertinty_from_tracebility").val(json.uncertinty);
                                        $("#resolution_of_master").val(json.resolution);
                                        $("#accuracy_taking_account").val(json.accuracy_main);
                                    }
                                });
                                {/if}
                            }
                            // Get accuracysub With Ajax  :Start 
                            $("#masterMeterSubSubId").change(function() { 
                                var dataString = "masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
                                var dataString2 = "masterMeterSubSubId_2=" + $("#masterMeterSubSubId").val();

                                $.ajax({
                                    type: "GET",
                                    url:  "masterMeterJqSubAccuracy.php",
                                    data: dataString,
                                    success: function(data1){ 
                                        $("#rangedisplay").html(data1);
                                    }
                                });
                                {assign var="obs_id" value=""}
                                {if empty($obs_data_id) && empty($obs_id)}
                                $.ajax({
                                    type: "GET",
                                    url:  "masterMeterJqSubAccuracy.php",
                                    data: dataString2,
                                    success: function(data1) { 
                                        var json = $.parseJSON(data1);
                                        $("#accuracy_of_rdg").val(json.accuracySubSub);
                                        $("#accuracy_of_fs").val(json.accuracySubSubFS);
                                        $("#master_stability").val(json.stability);
                                        $("#degree_of_freedom").val(json.degreeOfFreedom);
                                        $("#uncertinty_from_tracebility").val(json.uncertinty);
                                        $("#resolution_of_master").val(json.resolution);
                                        $("#accuracy_taking_account").val(json.accuracy_main);
                                    }
                                });
                                {/if}
                                    
                            });
                        }
                    });
                });
                // Get accuracysub With Ajax: End
                if($('#edit_master_meter_sub_id') != '/') {
                    $("#masterMeterSubId").val($('#edit_master_meter_sub_id').val());
                    $("#masterMeterSubId").change();
                }
            }
        });
    }
}

function getMasterMeterData_2()
{
    if($("#masterMeterId_2").val() != 0) {
        var dataString = "masterMeterId=" + $("#masterMeterId_2").val();
        $.ajax({
            type: "GET",
            url:  "masterMeterJq_2.php",
            data: dataString,
            success: function(data) { 
                $("#masterpara_2").html(data);
                $("#masterMeterSubId_2").focus();
                // Ajax Call At Range :Start 
                $("#masterMeterSubId_2").change(function() {
                    var dataString = "masterMeterSubId=" + $("#masterMeterSubId_2").val();
                    $.ajax({
                        type: "GET",
                        url:  "masterMeterJqSub_2.php",
                        data: dataString,
                        success: function(data) {
                            $("#masterparasub_2").html(data);
                            $("#masterMeterSubSubId_2").focus();
                            if($('#edit_master_meter_sub_sub_id_2') != '/') {
                                $("#masterMeterSubSubId_2").val($('#edit_master_meter_sub_sub_id_2').val());
                            }
                            $("#masterMeterSubSubId_2").change(function() { 
                                var dataString2 = "masterMeterSubSubId_2=" + $("#masterMeterSubSubId_2").val();
                                {assign var="obs_id" value=""}
                                {if empty($obs_data_id) && empty($obs_id)}
                                $.ajax({
                                    type: "GET",
                                    url:  "masterMeterJqSubAccuracy.php",
                                    data: dataString2,
                                    success: function(data1) { 
                                        var json = $.parseJSON(data1);
                                        $("#accuracy_of_rdg_2").val(json.accuracySubSub);
                                        $("#accuracy_of_fs_2").val(json.accuracySubSubFS);
                                        $("#master_stability_2").val(json.stability);
                                        $("#degree_of_freedom_2").val(json.degreeOfFreedom);
                                        $("#uncertinty_from_tracebility_2").val(json.uncertinty);
                                        $("#resolution_of_master_2").val(json.resolution);
                                        $("#accuracy_taking_account_2").val(json.accuracy_main);
                                    }
                                });
                                {/if}
                                    
                            });
                        }
                    });
                });
                if($('#edit_master_meter_sub_id_2') != '/') {
                    $("#masterMeterSubId_2").val($('#edit_master_meter_sub_id_2').val());
                    $("#masterMeterSubId_2").change();
                }
            }
        });
    }
}

function getMasterMeterData_3()
{
    if($("#masterMeterId_3").val() != 0) {
        var dataString = "masterMeterId=" + $("#masterMeterId_3").val();
        $.ajax({
            type: "GET",
            url:  "masterMeterJq_3.php",
            data: dataString,
            success: function(data) { 
                $("#masterpara_3").html(data);
                $("#masterMeterSubId_3").focus();
                // Ajax Call At Range :Start 
                $("#masterMeterSubId_3").change(function() {
                    var dataString = "masterMeterSubId=" + $("#masterMeterSubId_3").val();
                    $.ajax({
                        type: "GET",
                        url:  "masterMeterJqSub_3.php",
                        data: dataString,
                        success: function(data) {
                            $("#masterparasub_3").html(data);
                            $("#masterMeterSubSubId_3").focus();
                            if($('#edit_master_meter_sub_sub_id_3') != '/') {
                                $("#masterMeterSubSubId_3").val($('#edit_master_meter_sub_sub_id_3').val());
                            }
                            $("#masterMeterSubSubId_3").change(function() { 
                                var dataString3 = "masterMeterSubSubId_2=" + $("#masterMeterSubSubId_3").val();
                                {assign var="obs_id" value=""}
                                {if empty($obs_data_id) && empty($obs_id)}
                                $.ajax({
                                    type: "GET",
                                    url:  "masterMeterJqSubAccuracy.php",
                                    data: dataString3,
                                    success: function(data1) { 
                                        var json = $.parseJSON(data1);
                                        $("#accuracy_of_rdg_3").val(json.accuracySubSub);
                                        $("#accuracy_of_fs_3").val(json.accuracySubSubFS);
                                        $("#master_stability_3").val(json.stability);
                                        $("#degree_of_freedom_3").val(json.degreeOfFreedom);
                                        $("#uncertinty_from_tracebility_3").val(json.uncertinty);
                                        $("#resolution_of_master_3").val(json.resolution);
                                        $("#accuracy_taking_account_3").val(json.accuracy_main);
                                    }
                                });
                                {/if}
                                    
                            });
                        }
                    });
                });
                if($('#edit_master_meter_sub_id_3') != '/') {
                    $("#masterMeterSubId_3").val($('#edit_master_meter_sub_id_3').val());
                    $("#masterMeterSubId_3").change();
                }
            }
        });
    }
}

function getDataOfMaster(){
    var masterMeterId = $('#masterMeterId').val();
    var calibration_location = $('#cali_location').val();
    if(masterMeterId != '' && calibration_location != ''){
    var dataString = "masterMeterId=" + masterMeterId + "&calibration_location=" +calibration_location;
        $.ajax({
            url:"masterMeterData.php",
            type:"GET",
            dataType:'json',
            data : dataString,
            success:function(response){
                $('#temperature').val('');
                $('#humidity').val('');
                if(response.status){
                    if(calibration_location == 1){
                        $('#temperature').val(response.data.master_temp_lab ?? '' );
                        $('#humidity').val(response.data.master_humidity_lab ?? '' );
                    }else{
                        $('#temperature').val(response.data.master_temp ?? '' );
                        $('#humidity').val(response.data.master_humidity ?? '' );
                    }
                    
                }
            }
        });
    }
}

$(document).on('change click','#masterMeterId,#cali_location',function(){
        console.log(78);
        getDataOfMaster();
});

</script>
{include file="./headEnd.tpl"}

<input type="hidden" name="" id="edit_master_meter_sub_id" value={$edit_master_meter_sub_id} />
<input type="hidden" name="" id="edit_master_meter_sub_sub_id" value={$edit_master_meter_sub_sub_id} />
<input type="hidden" name="" id="edit_master_meter_sub_id_2" value={$edit_master_meter_sub_id_2} />
<input type="hidden" name="" id="edit_master_meter_sub_sub_id_2" value={$edit_master_meter_sub_sub_id_2} />
<input type="hidden" name="" id="edit_master_meter_sub_id_3" value={$edit_master_meter_sub_id_3} />
<input type="hidden" name="" id="edit_master_meter_sub_sub_id_3" value={$edit_master_meter_sub_sub_id_3} />
<input type="hidden" name="" id="edit_grn_id" value={$edit_grn_id} />
<input type="hidden" name="" id="edit_grn_detail_id" value={$edit_grn_detail_id} />
<form action="{$smarty.server.PHP_SELF}" id="form1" name="form1" method='POST'>
{if !empty($obs_data_id)}
    <input type="hidden" name="obs_data_id" id="" value={$obs_data_id} />
    <input type="hidden" name="obs_data_key" id="" value={$obs_data_key} />
{/if}
{if !empty($obs_id2)}
    <input type="hidden" name="obs_id" id="" value={$obs_id2} />
    <input type="hidden" name="obs_key" id="" value={$obs_key} />
{/if}
<input type="hidden" name="grnDetailPassId" value={$grnDetailPassId} />
<span class="center"><h2>Observation Entry</h2></span><br>
<table border='1' cellpadding='5' cellspacing='0' align='left'>
  <tr>
    
      <td colspan="9"  NOWRAP style="padding-left: 0px;"><span style="background-color: aquamarine; padding-top: 11px; padding-bottom: 8px;"><font size="4">Master Meter.:-</font>
     <select name="masterMeterId" id="masterMeterId" onchange="getMasterMeterData();">
    {if empty($obs_data_id)}
     <option value="0">Select Meter</option>
     {/if}
       {html_options values=$meterEntryIdArray output=$meterEntryNameArray selected=$edit_master_meter_id}  
    </select>
    <span id="masterpara"></span>         
    <span id="masterparasub" style="background-color: #0A5BC4; padding: 11px 3px 8px 3px; color: #ffffff;"></span>  
    </span>
    Date Of Cali.
     {html_select_date time="$callibrationDate" prefix="callibrationDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
      Next Recom. Cali. date
    {html_select_date time="$nextYearDate" prefix="nextYearDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY"  day_value_format="%02d"}
    Certi. Issue Date 
     {if $certificateIssue == "Y"}
     <input type="checkbox" name="certificateIssue" value="1" CHECKED />
     {else}
     <input type="checkbox" name="certificateIssue" value="1" />
     {/if}
     {html_select_date time="$certificateIssueDate" prefix="certificateIssueDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY"  day_value_format="%02d"}
     
    </td>
  </tr>
  <tr>
    <td align="left" colspan="1" bgcolor="aquamarine" style="white-space: nowrap;">
        Accuracy % of Rdg.: <input type="text" size="6" name="accuracy_of_rdg" id="accuracy_of_rdg"  />

         Accuracy % of FS: <input type="text" size="6" name="accuracy_of_fs" id="accuracy_of_fs"  />
    </td>
    <td align="left" colspan="1" bgcolor="aquamarine" style="white-space: nowrap;">
        Stability: <input type="text" size="6" name="master_stability" id="master_stability" />
        Resolution of master: <input type="text" size="6" name="resolution_of_master" id="resolution_of_master" />
    </td>
    <td align="left" colspan="1" bgcolor="aquamarine">
        Degree of freedom: <input type="text" size="6" name="degree_of_freedom" id="degree_of_freedom" />
    </td>
    <td align="left" colspan="1" bgcolor="aquamarine">
        Uncertinty of master from Tracebility: <input type="text" size="6" name="uncertinty_from_tracebility" id="uncertinty_from_tracebility" />
    </td>
    <td align="left" colspan="1" bgcolor="aquamarine">
        Accuracy : <input type="text" size="6" name="accuracy_taking_account" id="accuracy_taking_account"  />
    </td>
 </tr>
  <tr class="table_4_and_6B_field" style="display: none;" >
    <td colspan="9"  NOWRAP style="padding-left: 0px;">
        <span style="background-color: lightgray; padding-top: 11px; padding-bottom: 8px;"><font size="4">Master Meter 2.:-</font>
            <select name="masterMeterId_2" id="masterMeterId_2" onchange="getMasterMeterData_2();">
            {if empty($obs_data_id)}
                <option value="0">Select Meter</option>
            {/if}
                {html_options values=$meterEntryIdArray output=$meterEntryNameArray selected=$edit_master_meter_id_2}  
            </select>
            <span id="masterpara_2"></span>         
            <span id="masterparasub_2" style="background-color: #0A5BC4; padding: 11px 3px 8px 3px; color: #ffffff;"></span>  
        </span>
    </td>
  </tr>
  <tr class="table_4_and_6B_field" style="display: none;" >
    <td align="left" colspan="1" bgcolor="lightgray" style="white-space: nowrap;">
        Accuracy % of Rdg.: <input type="text" size="6" name="accuracy_of_rdg_2" id="accuracy_of_rdg_2"  />

         Accuracy % of FS: <input type="text" size="6" name="accuracy_of_fs_2" id="accuracy_of_fs_2"  />
    </td>
    <td align="left" colspan="1" bgcolor="lightgray" style="white-space: nowrap;">
        Stability: <input type="text" size="6" name="master_stability_2" id="master_stability_2" />
        Resolution of master: <input type="text" size="6" name="resolution_of_master_2" id="resolution_of_master_2" />
    </td>
    <td align="left" colspan="1" bgcolor="lightgray">
        Degree of freedom: <input type="text" size="6" name="degree_of_freedom_2" id="degree_of_freedom_2" />
    </td>
    <td align="left" colspan="1" bgcolor="lightgray">
        Uncertinty of master from Tracebility: <input type="text" size="6" name="uncertinty_from_tracebility_2" id="uncertinty_from_tracebility_2" />
    </td>
    <td align="left" colspan="1" bgcolor="lightgray">
        Accuracy : <input type="text" size="6" name="accuracy_taking_account_2" id="accuracy_taking_account_2"  />
    </td>
 </tr>
  <tr class="table_6B_field" style="display: none;" >
    <td colspan="9"  NOWRAP style="padding-left: 0px;" >
        <span style="background-color: lightsteelblue; padding-top: 11px; padding-bottom: 8px;"><font size="4">Master Meter 3.:-</font>
            <select name="masterMeterId_3" id="masterMeterId_3" onchange="getMasterMeterData_3();">
            {if empty($obs_data_id)}
                <option value="0">Select Meter</option>
            {/if}
                {html_options values=$meterEntryIdArray output=$meterEntryNameArray selected=$edit_master_meter_id_3}  
            </select>
            <span id="masterpara_3"></span>         
            <span id="masterparasub_3" style="background-color: #0A5BC4; padding: 11px 3px 8px 3px; color: #ffffff;"></span>  
        </span>
    </td>
  </tr>
  <tr class="table_6B_field" style="display: none;" >
    <td align="left" colspan="1" bgcolor="lightsteelblue" style="white-space: nowrap;">
        Accuracy % of Rdg.: <input type="text" size="6" name="accuracy_of_rdg_3" id="accuracy_of_rdg_3"  />

         Accuracy % of FS: <input type="text" size="6" name="accuracy_of_fs_3" id="accuracy_of_fs_3"  />
    </td>
    <td align="left" colspan="1" bgcolor="lightsteelblue" style="white-space: nowrap;">
        Stability: <input type="text" size="6" name="master_stability_3" id="master_stability_3" />
        Resolution of master: <input type="text" size="6" name="resolution_of_master_3" id="resolution_of_master_3" />
    </td>
    <td align="left" colspan="1" bgcolor="lightsteelblue">
        Degree of freedom: <input type="text" size="6" name="degree_of_freedom_3" id="degree_of_freedom_3" />
    </td>
    <td align="left" colspan="1" bgcolor="lightsteelblue">
        Uncertinty of master from Tracebility: <input type="text" size="6" name="uncertinty_from_tracebility_3" id="uncertinty_from_tracebility_3" />
    </td>
    <td align="left" colspan="1" bgcolor="lightsteelblue">
        Accuracy : <input type="text" size="6" name="accuracy_taking_account_3" id="accuracy_taking_account_3"  />
    </td>
 </tr>  
  <tr>
    <td>GRN NO.: <b>N-<b/></font>
      <select name="grnId" id="grnId" onchange="getGrnData();" autofocus=autofocus required="required">
        {if empty($obs_data_id)}
            <option value="">GRN</option>
        {/if}
            {html_options values=$grnId output=$grnPrefixNo selected=$edit_grn_id}
      </select>
      <font size="3">Cust Id No.:-</font>
      <input type="text" name="custCode" id="custCode" value="" size="2" />
    </td>
    <td align="center" bgcolor="lightblue">
        INSTRUMENT.:-<span id="showItem"></span>
        <div style="white-space: nowrap;" id="showDetail" required=required></div>
    </td>
      <td align="center" bgcolor="lightblue">Certificate No: <input type="text" name="selfCertiNo" id="selfCertiNo" required=required/></td>
      <td align="center" bgcolor="aquamarine">Temperature: <input type="text" name="temperature" id="temperature" value="" size="10" required=required/></td>
      <td align="center" bgcolor="aquamarine">Relative Humidity: <input type="text" name="humidity" id="humidity" value="" size="10" required=required/></td>
      
  </tr>
  <tr>
  </tr>    
  <tr>
          
    <td align="left" colspan="1" bgcolor="lightblue" style="white-space: nowrap;">
        <input type="hidden" name="" /><input type="hidden" size="6" name="ratio1" /><input type="hidden" name="ratio2" />
        Confidence level: <input type="text" size="6" name="confidence_level" id="confidence_level" value="95" />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        Covarage factor: <input type="text" size="6" name="covarage_factor" id="covarage_factor" value="1.96" />
    </td>
    <td align="left" bgcolor="lightblue" colspan="2" style="white-space: nowrap;">
        Least Count: <input type="text" name="leastCount" id="leastCount" value="" size="7" required=required/>
        Least Count for Print: <input type="text" name="leastCount_print" id="leastCount_print" value="" size="7" required=required/>
        Accuracy: <input type="text" name="accuracy" id="accuracy" value="" size="7" required=required/>
        Remarks: <input type="text" id="certiRemarks" name="certiRemarks" size="30" />
    </td>
    <td align="center" bgcolor="lightblue">
        Make/Model: <input type="text" name="makeModel" id="makeModel" value="" size="10" required=required/>
    </td>
    <td align="center" bgcolor="lightblue">
        Instrument I.D. No.: <input type="text" name="instrumentId" id="instrumentId" value="" size="15" /></td>
 </tr>    
  <tr>
      <td align="left" colspan="5" bgcolor="lightblue">
          Table Info:
            <select name="table_info_id" id="table_info_id" required="">
                <option value="">-- Select --</option>
                {foreach $tableArray as $item}
                <option value="{$item.table_info_id}" data-image_url="{$item.image_url}">
                    {$item.table_name}
                </option>
                {/foreach}
            </select>
            <span class="table_19_field_obs_type" style="display: none;">
            Observation Type:
              <select name="obs_type_id" id="obs_type_id">
                  <option value="">-- Select --</option>
                  <option value="1">Type 1</option>
                  <option value="2">Type 2</option>
                  <option value="3">Type 3</option>
              </select>
            </span>
            <span style="white-space: nowrap;" id="showDetail_table" required=required></span>
            Table Title: <input type="text" id="table_title_right" name="table_title_right" size="20" value="" />&nbsp;&nbsp;
            <span class="table_19_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">
                <select name="weight_id" id="weight_id">
                    <option value="">-- Select --</option>
                    {foreach $weightArray as $item}
                    <option value="{$item.id}">
                        {$item.unit_name}
                    </option>
                    {/foreach}
                </select>
            </span>
            <span class="table_19_field2" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">D1 =: <input type="text" id="d1" size="5" name="d1" value="" /></span>
            <span class="table_19_field2" style="display: none; background-color: #9E64CC; padding: 5px 1px 5px 1px; color: #ffffff;">D2 =: <input type="text" id="d2" size="5" name="d2" value="" /></span>
            <span class="table_10_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">Column Heading 1 : <input type="text" id="col_head_1" size="5" name="col_head_1" value="" /></span>
            <span class="table_10_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">Column Heading 1 Least Count : <input type="text" id="col_head_1_least_count" size="5" name="col_head_1_least_count" value="" class="num_only" /></span>
            <span class="table_10_field" style="display: none; background-color: #9E64CC; padding: 5px 1px 5px 1px; color: #ffffff;">Span In : <input type="text" id="span_in" size="5" name="span_in" value="" /></span>
            <span class="table_regular_1_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">CTR : <input type="text" id="ctr" size="5" name="ctr" value="" /></span>
            <span class="table_regular_1_field" style="display: none; background-color: #9E64CC; padding: 5px 1px 5px 1px; color: #ffffff;">Class : <input type="text" id="class" size="5" name="class" value="" /></span>
            <span class="table_regular_1_field" style="display: none; background-color: #A0522D; padding: 5px 1px 5px 1px; color: #ffffff;">Cos : <input type="text" id="cos" size="5" name="cos" value="" /></span>
            <span class="table_11_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">Ocillation : <input type="text" id="ocillation" size="5" name="ocillation" value="" /></span>
            <span class="table_11_field" style="display: none; background-color: #9E64CC; padding: 5px 1px 5px 1px; color: #ffffff;">Frequency : <input type="text" id="frequency" size="5" name="frequency" value="" /></span>
            <span class="table_11_field" style="display: none; background-color: #A0522D; padding: 5px 1px 5px 1px; color: #ffffff;">RPM : <input type="text" id="rpm" size="5" name="rpm" value="" /></span>
            <span class="table_15_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">Temperature of water in °C: <input type="text" id="temp_of_water" size="5" name="temp_of_water" value="" /></span>
            <span class="table_15_field" style="display: none; background-color: #9E64CC; padding: 5px 1px 5px 1px; color: #ffffff;">Repeatibility % : <input type="text" id="repeatibility" size="5" name="repeatibility" value="" /></span>
            <span class="table_15_field" style="display: none; background-color: #A0522D; padding: 5px 1px 5px 1px; color: #ffffff;">Sensor Installation Error % : <input type="text" id="sensor" size="5" name="sensor" value="" /></span>
            <span class="table_11_15_10_field" style="display: none; background-color: #DE413C; padding: 5px 1px 5px 1px; color: #ffffff;">Uncertainty calibration certificate : <input type="text" id="uncertainty_calibration" size="5" name="uncertainty_calibration" value="" /></span>
            <span class="table_15_field" style="display: none; background-color: #6C1E68; padding: 5px 1px 5px 1px; color: #ffffff;"><br/><br/>started Time: <input type="text" id="start_time" size="10" name="start_time" value="" /></span>
            <span class="table_15_field" style="display: none; background-color: #8C2417; padding: 5px 1px 5px 1px; color: #ffffff;">Stopped Time: <input type="text" id="stop_time" size="10" name="stop_time" value="" /></span>
                                                                                                                    
            <span class="table_15_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">Flow Meter Size : <input type="text" id="flow_meter_size" size="10" name="flow_meter_size" value="" /></span>
            <span class="table_15_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">Line Size : <input type="text" id="line_size" size="10" name="line_size" value="" /></span>
            <span class="table_15_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">Pipe Material/Pipe Thickness : <input type="text" id="pipe_mate_thick" size="10" name="pipe_mate_thick" value="" /></span>
            <span class="table_15_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">Calibration Fluid : <input type="text" id="cali_fluid" size="10" name="cali_fluid" value="" /></span>
            
            <span class="table_18_field" style="display: none; background-color: #8C2417; padding: 5px 1px 5px 1px; color: #ffffff;">AC Voltage: <input type="text" id="ac_voltage" size="10" name="ac_voltage" value="" /></span>
            <span class="table_18_field" style="display: none; background-color: #A0522D; padding: 5px 1px 5px 1px; color: #ffffff;">AC Current: <input type="text" id="ac_current" size="10" name="ac_current" value="" /></span>
            <span class="table_18_field" style="display: none; background-color: #6C1E68; padding: 5px 1px 5px 1px; color: #ffffff;">Power Factor: <input type="text" id="power_factor" size="10" name="power_factor" value="" /></span>
            <span class="table_18_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">Start reading of kwh: <input type="text" id="start_reading_of_kwh" size="10" name="start_reading_of_kwh" value="" /></span>
            <span class="table_18_field" style="display: none; background-color: #2F4F4F; padding: 5px 1px 5px 1px; color: #ffffff;">End reading of kwh: <input type="text" id="end_reading_of_kwh" size="10" name="end_reading_of_kwh" value="" /></span>
            
            <span class="table_6A_field" style="display: none; background-color: #8C2417; padding: 5px 1px 5px 1px; color: #ffffff;">Calculated reading range 1: <input type="text" id="cal_reading_range_1" size="10" class="num_only" name="cal_reading_range_1" value="" /></span>
            <span class="table_6A_field" style="display: none; background-color: #8C2417; padding: 5px 1px 5px 1px; color: #ffffff;">Calculated reading range 2: <input type="text" id="cal_reading_range_2" size="10" class="num_only" name="cal_reading_range_2" value="" /></span>
            <span class="table_6B_field" style="display: none; background-color: #6C1E68; padding: 5px 1px 5px 1px; color: #ffffff;">
                C.T. Class : 
                <select name="ct_class" id="ct_class" >
                    <option value="">-- Select --</option>
                    {foreach $ctArray as $item}
                    <option value="{$item.id}" {if $edit_ct_class == $item.id}selected="selected"{else}{/if}>
                        {$item.ct_class}
                    </option>
                    {/foreach}
                </select>
            </span>
            <br /><br />
            UUC Range and Unit From GRN: <input name="grn_uuc_range" type="text" id="grn_uuc_range"  size="10" value="" />
          <span class="table_16_field" style="display: none;">UUC Range Min: <input type="text" id="uuc_range_min" name="range_min" size="5" value="" /></span>
          <span style="background-color: #00A600; padding: 5px 1px 5px 1px; color: #ffffff;"><span class="range_class">UUC Range Max:</span> <input type="text" id="uuc_range" name="range" class="num_only" size="5" value="" /> <span class="table_7_field" style="display: none; "> <input type="text" id="uuc_range_range" name="uuc_range_range" size="5" value="" /></span> </span>
          &nbsp; &nbsp; UUC Range for print in table: <input name="uuc_range_print_table" type="text" id="uuc_range_print_table"  size="10" value="" />
          <span class="table_11_field" style="display: none; background-color: #A0522D; padding: 5px 1px 5px 1px; color: #ffffff;">Least Count for RPM : <input type="text" id="least_count_for_rpm" size="5" name="least_count_for_rpm" value="" class="num_only" /></span>
          <span class="table_14_field" style="display: none; background-color: #A0522D; padding: 5px 1px 5px 1px; color: #ffffff;"> Master : <input type="text" id="uuc_range_2" name="uuc_range_2" size="5" value="" /></span>
            <span class="table_4_field" style="display: none; background-color: #A700AE; padding: 5px 1px 5px 1px; color: #ffffff;"> UUC Range Secondary:<input type="text" id="uuc_range_sec" size="5" name="range_sec" value="" /></span>
            <span class="table_7_field" style="display: none; background-color: #A0522D; padding: 5px 1px 5px 1px; color: #ffffff;"> Ratio Bottom Part: <input type="text" id="ratio_bottom_part" size="5" name="ratio_bottom_part" class="num_only" value="" /> <input type="text" id="ratio_bottom_part_range" size="5" name="ratio_bottom_part_range" value="" /></span>
            <span class="table_9_field" style="display: none;">Voltage: <input type="text" id="voltage" size="5" name="voltage" value="" /></span>
            <span class="table_6B_field" style="display: none; background-color: #8C2417; padding: 5px 1px 5px 1px; color: #ffffff;">Burden: <input type="text" id="burden_6b" size="10" name="burden_6b" value="" /></span>
            <span class="table_6B_field" style="display: none; background-color: #A0522D; padding: 5px 1px 5px 1px; color: #ffffff;">Ratio: <input type="text" id="ratio_6b" size="10" name="ratio_6b" value="" /></span> &nbsp;
            <span class="table_2_field" style="display: none; background-color: #2F4F4F; padding: 14px 1px 8px 1px; color: #ffffff;"> Consider Ue for Exp. Unce.:<input type="checkbox" style="height:17px; width:17px;" id="consider_ue" size="5" name="consider_ue" value="" /></span>
            <span class="ue_unit_field" style="display: none; background-color: #A0522D; padding: 5px 1px 5px 1px; color: #ffffff;">UE Unit : <input type="text" id="ue_unit" size="10" name="ue_unit" value="" /></span> &nbsp;
            <br/> <br/>&nbsp; Calibration Location:
            <select name="cali_location" id="cali_location" style="width: 80px;">
                <option value="1" {if $edit_cali_location == '1'}selected="selected"{else}{/if}>At Lab</option>
                <option value="2" {if $edit_cali_location == '2'}selected="selected"{else}{/if}>At Site</option>
            </select>

            UUC Location: <input type="text" id="uuc_location" size="5" name="uuc_location" value="" />
            Calibrated By:
            <select name="user_id" id="user_by" required="">
              {html_options values=$userArray output=$userNameArray selected=$edit_user_id}
            </select>
            Approved By:
            <select name="approved_by" id="approved_by">
              {html_options values=$approveArray output=$approveNameArray}  
            </select>
            
            

        </td>
    </tr>
</table>
<div id="table_data_div">
    
</div>
<br/>
<br/>
<br/>
<br/>
{if (have_access_role($smarty.const.OBS_MODULE_ID,"view"))}
    <div id="obsDetail"></div>
{/if}
</form>


{include file="footer.tpl"}