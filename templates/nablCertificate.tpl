<span style="float:right;margin:127px 0 0 0;"><b>QC F 05</b></span>
<div style="display:block;border:1px solid black;float:left;margin:0 0 0 55px;padding:5px;">
	<table border=0 cellpadding=0 cellspacing=4 width:"100%" style="font:12px arial,sans-serif;">
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left">CERTIFICATE NO.</td>
			<td align="left">:</td>
			<td align="left">{$certificateNo}</td>
			<td align="left">&nbsp;</td>
			<td align="left">DATE OF CALLIBRATION</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.callibrationDate}</td>
			<td align="left" colspan="2">&nbsp;</td>
		</tr> 
		<tr>
      <td align="left" colspan="2">&nbsp;</td>
      <td align="left">NO OF SHEETS.</td>
			<td align="left">:</td>
			<td align="left">1 Of 2</td>
			<td align="left">&nbsp;&nbsp;&nbsp;</td>
			<td align="left">Next Recommended Calibration date</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.nextYearDate}</td>
		  <td align="left" colspan="2">&nbsp;</td>
    </tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left">GRN No.</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.grnNo}</td>
			<td align="left" colspan="6">&nbsp;</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>1.CUSTOMER NAME & ADDRESS</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4"><b>{$grnDetailArr.custName}</b></td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="6">&nbsp;</td>
			<td align="left" colspan="5" style="font-size:x-small;">
			  {$grnDetailArr.address}
			  <br />
			  <b style="font-weight:bold;font-size:12px;">{$grnDetailArr.city}</b></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>2.CUSTOMERS REFERANCE NO.</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.custRefName}/cali/{$grnDetailArr.custCode}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>3.INSTRUMENT RECEIVED ON</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.grnDate}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="4"><b>4.DESCRIPTION OF INSTRUMENT</b></td>
			<td align="left" colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Name</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.itemName}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Make/Model.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.makeModel}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Sr No.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.itemCode}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Instrument.ID.No.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.instrumentId}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Range.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.range2}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Least Count.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.leastCount}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Accuracy.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.accuracy}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Condition on receipt.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">Good</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>5.AMBIENT TEMPERATURE</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.temperature}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;<b>RELATIVE HUMIDITY</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.humidity}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="9"><b>6.DETAILS OF REFERANCE STANDARD & MAJOR INSTRUMENT USED.</b></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Name</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterName}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;I.D No</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterIdNo}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Make</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterMake}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Model No.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterModelNo}</td>
			<td align="left">&nbsp;</td>
		</tr>
 		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Serial No.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterSerialNo}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Certificate No</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterCertificateNo}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Calibration Valid Up To</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterExp}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Traceability To</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterTraceabilityTo}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="8" style="font-size:x-small;">
        &nbsp;&nbsp;&nbsp;Measurement uncertainty  {$grnDetailArr.measurementUncertaintyIs}
      </td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>7.PROCEDURE</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.procedureText}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="9" style="font-family: 'Exotc350 DmBd BT';font-size:20px;">&nbsp;&nbsp;&nbsp;<b>For Krishna Instruments</b></td>
		</tr>
		<tr><td align="left" colspan="11">&nbsp;</td></tr>
 		<tr>
			<td align="left" colspan="1">&nbsp;</td>
      <td align="left" colspan="4">CALIBRATED BY : (Bhavesh Tank, CAL. ENG.)</td>
      <td align="left">&nbsp;</td>
      <td align="left" colspan="3">APPROVED BY : (D.R.SHAH, C.E.O.)</td>
			<td align="left" colspan="2">SEAL:</td>
		</tr>
 		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="8" style="font-size:x-small;">
      	1.This report pertains To Partuculars sample/Instrumenst submitted for test.<br>
      	2.This certificate may not be reproduced except in full, without prior written permission of Krishna Instruments.<br>
      	3.This Calibraiton Result Reported in the Certificate are valid at the time of measurments and under started Conditions.
      </td>
			<td align="left">&nbsp;</td>
		</tr>
  </table>
</div>
<div style="clear:both;">
</div>
<br/>
<div style="display:block;border:1px solid black;float:left;margin:200px 0 0 55px;padding:5px;">
	<table border=0 cellpadding=0 cellspacing=4 width:"100%" style="font:12px arial,sans-serif;">
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left">CERTIFICATE NO.</td>
			<td align="left">:</td>
			<td align="left">{$certificateNo}</td>
			<td align="left">&nbsp;</td>
			<td align="left">DATE OF CALLIBRATION</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.callibrationDate}</td>
			<td align="left" colspan="2">&nbsp;</td>
		</tr> 
		<tr>
      <td align="left" colspan="2">&nbsp;</td>
      <td align="left">NO OF SHEETS.</td>
			<td align="left">:</td>
			<td align="left">2 Of 2</td>
			<td align="left">&nbsp;&nbsp;&nbsp;</td>
			<td align="left">Next Recommended Calibration date</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.nextYearDate}</td>
		  <td align="left" colspan="2">&nbsp;</td>
    </tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left">GRN No.</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.grnNo}</td>
			<td align="left" colspan="6">&nbsp;</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="9"><b>8.OBSERVATION</b></td>
		</tr>
		<tr>
		  <td colspan="11">
				{section name=sec loop=$masterArr}
				{if $masterArr[sec].dataPresent == 1}
				<table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				<tr>
					<div style="margin:10px;"><b>{$masterArr[sec].parameterName}</b></div>
			  </tr>
				<tr>
				  <td align="left" colspan="2" rowspan="2">SR No</td>
	        <td align="center" colspan="6">Callibration Standard</td>
	        <td align="center" colspan="6">Unit Under Test</td>
	        <td align="center" colspan="6">Error</td>
	        <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	      </tr>
	      <tr>
	        <td align="center" colspan="3">Range</td>
	        <td align="center" colspan="3">Reading</td>
	        <td align="center" colspan="3">Range</td>
	        <td align="center" colspan="3">Reading</td>
	        <td align="center" colspan="3">Units</td>
	        <td align="center" colspan="3">% of Rdg.</td>
	      </tr>
	      {section name=sec1 loop=$detailArr[sec]}
	      <tr>
	        <td align="center" colspan="2">{$smarty.section.sec1.rownum}</td>
	        <td align="center" colspan="3">{$masterArr[sec].range}</td>
	        <td align="center" colspan="3">{$detailArr[sec][sec1].testMeterAverage}</td>
	        <td align="center" colspan="3">{$masterArr[sec].range2}</td>
	        <td align="center" colspan="3">{$detailArr[sec][sec1].stdMeterAverage}</td>
	        <td align="center" colspan="3">{$detailArr[sec][sec1].errorUnits}</td>
	        <td align="center" colspan="3">{$detailArr[sec][sec1].errorRdg}</td>
	        <td align="center" colspan="2">{$detailArr[sec][sec1].expandedUncertinityInPre}</td>
	      </tr>
	      {/section}
	      </table>
	      {/if}
	      {/section}
	    </td>
		</tr>
		<tr>
			<td colspan="9">
			  <table border="0" cellspacing="0" style="border:0px solid black;margin-left:50px;font-size:x-small;font:12px arial,sans-serif;">
			  <tr><td colspan="2"><b>NOTE :</b></td></tr>
			  <tr>
			    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			    <td style="font-size:x-small;">
			      1. The reported expanded uncertainty in measurement is stated as the standard uncertainty <br/>
			      in measurement multiplied by the coverage factor k=2, which for a normal distribution <br/>
			      corresponds to a coverage probability of approximately 95%.
			    </td>
			  </tr>
			  </table>
			</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="9" style="font-family: 'Exotc350 DmBd BT';font-size:20px;">&nbsp;&nbsp;&nbsp;<b>For Krishna Instruments</b></td>
		</tr>
		<tr><td align="left" colspan="11">&nbsp;</td></tr>
 		<tr>
			<td align="left" colspan="1">&nbsp;</td>
      <td align="left" colspan="4">CALIBRATED BY : (Bhavesh Tank, CAL. ENG.)</td>
      <td align="left">&nbsp;</td>
      <td align="left" colspan="3">APPROVED BY : (D.R.SHAH, C.E.O.)</td>
			<td align="left" colspan="2">SEAL:</td>
		</tr>
  </table>
</div>
<div style="clear:both;">