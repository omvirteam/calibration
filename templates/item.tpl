{include file="./headStart.tpl"}
<head>
<title>Item</title>
<script type="text/javascript">
$(document).ready(function() {
  $('#itemName').focus();
});

function confirmDelete()
{
	var answer = confirm("Delete Selected Recored ?")
	if(answer)
	  document.messages.submit();

	return false;
}
</script>
</head>
{include file="./headEnd.tpl"}
<form name="itemForm" method="post" action="{$smarty.server.PHP_SELF}" id="itemForm">
<input type="hidden" name="itemId" value="{$itemId}" />

{if (have_access_role($smarty.const.MASTER_ITEM_MODULE_ID,"add") && {!isset($smarty.get.itemId)}) || (have_access_role($smarty.const.MASTER_ITEM_MODULE_ID,"edit") && {isset($smarty.get.itemId)})}
    <table border = "1" align="center" cellpadding="1" cellspacing="1">
        <tr>
          <td align="center" class="table1" colspan="2">Item Entry</td>
        </tr>
        <tr>
          <td><input type="text" name="itemName" id="itemName" value="{$currentItemName}"  required="required" title="ItemName"/></td>
        </tr>
        <tr>
          <td align="center"><input type="submit" name="insertBtn" value="Store !" class="button" /> <input type="submit" value="CANCEL" name="cancelBtn" class="button" /></td>
        </tr>
    </table>
{/if}

</form>
<br /><br />
{if have_access_role($smarty.const.MASTER_ITEM_MODULE_ID,"view") || have_access_role($smarty.const.MASTER_ITEM_MODULE_ID,"edit")}
<table align="center" border="0" cellpadding="1" cellspacing="1">
{section name="sec" loop=$itemArr}
<tr>
	<td class="table2" align="center">
        {if have_access_role($smarty.const.MASTER_ITEM_MODULE_ID,"edit")}
	  <a href="item.php?itemId={$itemArr[sec].itemId}" class="link">Edit</a>
        {/if}
	</td>
	<td class="table2" align="center">
        {if have_access_role($smarty.const.MASTER_ITEM_MODULE_ID,"delete")}
	  <a href="deleteItem.php?itemId={$itemArr[sec].itemId}" onclick="return confirmDelete();" id="delete{$itemArr[sec].itemId}" class="link">Delete</a>
        {/if}
	</td>
	<td class="table2" align="left">{$itemArr[sec].itemName}</td>
</tr>
{sectionelse}
<tr>
  <td class="table2" align="center" colspan="3">No Records Found.....!!!!!!!!</td>
</tr>
{/section}
</table>
{/if}
{include file="footer.tpl"}