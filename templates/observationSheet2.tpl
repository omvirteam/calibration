{include file="./headStart.tpl"}
<script type="text/javascript" src="./js/jquerynew.min.js"></script>
<script type="text/javascript">
function sameRecord()
{
    

    

}
$(document).ready(function(){
        $(document).on('click',"#view_calculation",function(){
            testMeterAvg();
            meanReading();
            $('#calculation_table').show();
        });
        $(document).on('click',"#hide_calculation",function(){
            $('#calculation_table').hide();
        });
        $(document).on('change',"#table_info_id",function(){
            var image_url = $(this).find(':selected').data('image_url');
            if(typeof(image_url) !== 'undefined'){
                $('#image_span').html('<img style="float: right" width="400px"  src=".'+image_url+'" />');
            } else {
                $('#image_span').html('');
            }
        });
	$("#form1").submit(function(e){
        e.preventDefault();
        testMeterAvg();
        meanReading();
        
        var is_range = true;
        is_value_entered_uut = 0;
        is_value_entered_master = 0;
        var rangeIdSame1 = $("#masterMeterAccuracyRangeArray").val();
        var range_extra = parseFloat(rangeIdSame1) * 10 / 100;
        var rangeIdSame2 = parseFloat(rangeIdSame1) + parseFloat(range_extra);
        var rangeIdSame = parseFloat(rangeIdSame2).toFixed(4);
        
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.stdMeter').each(function(ind,ele){
                $(this).attr('name','stdMeter['+row_ind+'][]');
            });
        });
        
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.testMeter2').each(function(ind,ele){
                $(this).attr('name','testMeter2['+row_ind+'][]');
            });
        });
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.testMeter3').each(function(ind,ele){
                $(this).attr('name','testMeter3['+row_ind+'][]');
            });
        });
        
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            var row = $(this);
            row.find('.stdMeter').each(function(ind,ele){
                if($(this).val() != '') {
                    is_value_entered_uut = 1;
                }
            });
            
            $("tr.tableRow:visible").each(function(row_ind,row_ele){
                $(this).find('.testMeter').each(function(ind,ele){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                });
            });
            row.find('.testMeter').each(function(ind,ele){
                if($(this).val() != '') {
                    is_value_entered_master = 1;
                }
            });

        });
        
        if(is_value_entered_uut == 0) {
            alert('Enter UUC Reading');
            $('#stdMeter_1').focus();
            return false;
        }
        if(is_value_entered_master == 0) {
            alert('Enter Master Reading');
            $('#testMeter_1').focus();
            return false;
        }
        
        
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            var row = $(this);
            row.find('.stdMeter').each(function(ind,ele){
                if($(this).val() != '') {
                    is_value_entered_uut = 1;
                    var tmp_val = parseFloat($(this).val()) || 0;
                    if(tmp_val > parseFloat(rangeIdSame)) {
                        is_range = false;
                        return false;
                    }
                }
            });
            if(is_range == false) {
                return false;
            }
            $("tr.tableRow:visible").each(function(row_ind,row_ele){
                $(this).find('.testMeter').each(function(ind,ele){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                });
            });
            row.find('.testMeter').each(function(ind,ele){
                if($(this).val() != '') {
                    is_value_entered_master = 1;
                    var tmp_val = parseFloat($(this).val()) || 0;
                    if(tmp_val > parseFloat(rangeIdSame)) {
                        is_range = false;
                        return false;
                    }
                }
            });
            if(is_range == false) {
                return false;
            }
        });

        if(is_range == true) {
            var form_obj = $(this);
            $.ajax({
                type: "POST",
                url: form_obj.attr('action'),
                data: form_obj.serialize(), //serializes the form's elements.
                dataType:'json',
                success: function(data)
                {
                    $("#mainDiv input[type='text']").val('');
                     var std_values = '   <span class="main_std main_std1">   '  + 
                     '                   <input type="button" value=" - " class="std_remove"><input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" id="stdMeter_1" title="stdMeter"  >  '  + 
                     '               </span>  '  + 
                     '               <span class="main_std">   '  + 
                     '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                     '               </span>  '  + 
                     '               <span class="main_std">   '  + 
                     '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                     '               </span>  '  + 
                     '               <span class="main_std">   '  + 
                     '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                     '               </span>  '  + 
                     '               <span class="main_std">   '  + 
                     '                           <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                     '              </span>  ' ; 
                    $(".std_values").html(std_values);
                    var testMeter_values =  '   <span class="main_testMeter1 main_testMeter">   '  + 
                         '                   <input type="button" value=" - " class="testMeter_remove"><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                         '                  </span>  ' ; 
                    $(".testMeter_values").html(testMeter_values);
                    var testMeter2_values =  '   <span class="main_testMeter21 main_testMeter2">   '  + 
                         '                   <input type="button" value=" - " class="testMeter2_remove"><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only" size="4" align="right" title="testMeter"   >  '  + 
                         '                  </span>  ' ; 
                     testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                                '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                     testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                                '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                     testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                                '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                     testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                                '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                     testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                                '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                     testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                                '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                     testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                                '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                     testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                                '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                     testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                                '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                    $(".testMeter2_values").html(testMeter2_values);
                    var testMeter3_values =  '   <span class="main_testMeter31 main_testMeter3">   '  + 
                         '                   <input type="button" value=" - " class="testMeter3_remove"><input type="text" name="testMeter3[][]" id="testMeter3_1" class="testMeter3 num_only" size="4" align="right" title="testMeter"   >  '  + 
                         '                  </span>  ' ; 
                    testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                            '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                    testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                            '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                    testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                            '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                    testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                            '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                    testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                            '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                    testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                            '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                    testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                            '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                    testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                            '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                    testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                            '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                            '</span>  ' ; 
                    $(".testMeter3_values").html(testMeter3_values);
                    uut_box_total_count();
                    master_box_total_count();
                    master_box_total_count2();
                    master_box_total_count3();
                    getDetails();
                    get_table_no();
                    $('#stdMeter_1').focus();
                }
            });
        } else {
            alert('Not a Range');
            return false;
        }
	}); 

    $(document).on('click',".add",function(){
            var tr_obj = $(this).closest('.tableRow');
            var row_template = $("#row_template").clone(true)
            row_template.find('input[type="text"]').val('');
            row_template.removeAttr("id");
            row_template.removeAttr("style");
            row_template.insertAfter(tr_obj);
    });
        
        $(document).on('click',".delete",function(){
            $(this).parent().parent().parent().remove();
            return false;
	});
        
	$(document).on('click',".std_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_std:last').hasClass("main_std1")) {
            } else {
                td_obj.find('.main_std:last').remove();
            }
            uut_box_total_count();
	});
  
  	$(".std_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_std_html = '<span class="main_std"> <input type="text" name="stdMeter[][]" class="stdMeter" size="4" align="right" title="stdMeter"  ></span>';
            td_obj.find(".std_values").append(main_std_html);
            uut_box_total_count();
  	});
       
  	$(document).on('keydown',".stdMeter",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.stdMeter:last').val() != '') {
                            var last_main_std = td_obj.find('.main_std:last');
                            var main_std_html = '<span class="main_std">&nbsp;<input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  ></span>';
                            $(main_std_html).insertAfter(last_main_std);
                    }
                }
            }
            uut_box_total_count();
  	});
        
         
        $(document).on('click',".testMeter_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter:last').hasClass("main_testMeter1")) {
            } else {
                td_obj.find('.main_testMeter:last').remove();
            }
            master_box_total_count();
	});
        
  	$(".testMeter_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html = '<span class="main_testMeter"> <input type="text" name="testMeter[][]" class="testMeter" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter_values").append(main_testMeter_html);
            master_box_total_count();
  	});
        
  	$(document).on('keydown',".testMeter",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter:last').val() != '') {
                            var last_main_testMeter = td_obj.find('.main_testMeter:last');
                            var main_testMeter_html = '<span class="main_testMeter">&nbsp;<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   ></span>';
                            console.log(last_main_testMeter);
                            $(main_testMeter_html).insertAfter(last_main_testMeter);
                    }
                }
            }
            master_box_total_count();
  	});
        
        $(document).on('click',".testMeter2_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter2:last').hasClass("main_testMeter21")) {
            } else {
                td_obj.find('.main_testMeter2:last').remove();
            }
            master_box_total_count2();
	});
        
  	$(".testMeter2_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html2 = '<span class="main_testMeter2"> <input type="text" name="testMeter2[][]" class="testMeter2" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter2_values").append(main_testMeter_html2);
            master_box_total_count2();
  	});
        
  	$(document).on('keydown',".testMeter2",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter2:last').val() != '') {
                            var last_main_testMeter2 = td_obj.find('.main_testMeter2:last');
                            var main_testMeter_html2 = '<span class="main_testMeter2">&nbsp;<input type="text" name="testMeter2[][]" class="testMeter2 num_only" size="4" align="right" title="testMeter"   ></span>';
                            console.log(last_main_testMeter2);
                            $(main_testMeter_html2).insertAfter(last_main_testMeter2);
                    }
                }
            }
            master_box_total_count2();
  	});
        
        $(document).on('click',".testMeter3_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter3:last').hasClass("main_testMeter31")) {
            } else {
                td_obj.find('.main_testMeter3:last').remove();
            }
            master_box_total_count3();
	});
        
  	$(".testMeter3_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html3 = '<span class="main_testMeter3"> <input type="text" name="testMeter3[][]" class="testMeter3" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter3_values").append(main_testMeter_html3);
            master_box_total_count3();
  	});
        
  	$(document).on('keydown',".testMeter3",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter3:last').val() != '') {
                            var last_main_testMeter3 = td_obj.find('.main_testMeter3:last');
                            var main_testMeter_html3 = '<span class="main_testMeter3">&nbsp;<input type="text" name="testMeter3[][]" class="testMeter3 num_only" size="4" align="right" title="testMeter"   ></span>';
                            console.log(last_main_testMeter3);
                            $(main_testMeter_html3).insertAfter(last_main_testMeter3);
                    }
                }
            }
            master_box_total_count3();
  	});

  	$(document).on('input', ".num_only", function () {
        this.value = this.value.replace(/[^\d\.\-]/g, '');
    });
    uut_box_total_count();
    master_box_total_count();
    master_box_total_count2();
    master_box_total_count3();
});

function uut_box_total_count(){
    var box_count = 0;
    var box_count_filled = 0;
    $('tr.tableRow:visible').find('.stdMeter').each(function(ind,ele){
        if($(this).val() != '') {
            box_count_filled++;
        }
        box_count++;
    });
    $('#uut_box_total').html(box_count);
    $('#box_count_filled').html(box_count_filled);
}

function master_box_total_count(){
    var test_box_count = 0;
    var test_box_count_filled = 0;
    $('tr.tableRow:visible').find('.testMeter').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled++;
        }
        test_box_count++;
    });
    $('#test_box_total').html(test_box_count);
    $('#test_box_count_filled').html(test_box_count_filled);
}

function master_box_total_count2(){
    var test_box_count2 = 0;
    var test_box_count_filled2 = 0;
    $('tr.tableRow:visible').find('.testMeter2').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled2++;
        }
        test_box_count2++;
    });
    $('#test_box_total2').html(test_box_count2);
    $('#test_box_count_filled2').html(test_box_count_filled2);
}

function master_box_total_count3(){
    var test_box_count3 = 0;
    var test_box_count_filled3 = 0;
    $('tr.tableRow:visible').find('.testMeter3').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled3++;
        }
        test_box_count3++;
    });
    $('#test_box_total3').html(test_box_count3);
    $('#test_box_count_filled3').html(test_box_count_filled3);
}

function getDetails()
{
  var datastring = "grnId=" + $('#grnId').val() + "&grnDetailId=" + $('#grnDetailId').val();
  console.log(datastring);
	$.ajax({
	  url: "./obsDetailAj.php",
	  data: datastring,
	  success: function(response){
	    $('#obsDetail').html(response);
	  }
	});
}

function deleteObs(grnObsId)
{
	var answer = confirm("Are You Sure Delete Your Recored ?")
	if (answer == true)
	{
	  var datastring = "grnObservationId=" + grnObsId;
		$.ajax({
		  url: "./obsDetailDelete.php",
		  data: datastring,
		  method: 'POST',
		  success: function(response){
		    getDetails();
		  }
		});
  }
	return false;
}

function getGrnData()
{
	var datastring = "grnId=" + $("#grnId").val();
	$.ajax({
	  url: "./obsSheetAj.php",
	  data: datastring,
	  success: function(data){ 
	    $('#showItem').html(data);
	    $('#custCode').val($('#custCodeHidden').val());
	    //
	    $("#grnDetailId").change(function(){
	      var datastring = "grnDetailId=" + $("#grnDetailId").val();
              getDetails();
	      $.ajax({
	        url: "./obsSheetAjGrnDetailChange.php",
	        data: datastring,
	        success: function(data){
	          $('#showDetail').html(data);
                  $('#uuc_range').val('');
                  $('#uuc_range').val($('#uuc_range_hid').val());
                  {*$('#temperature').val('');
                  $('#temperature').val($('#temp_hid').val());
                  $('#humidity').val('');
                  $('#humidity').val($('#humidity_hid').val());*}
                  $('#uuc_location').val('');
                  $('#uuc_location').val($('#uuc_location_hid').val());
                  $('#approved_by').val($('#approved_by_hid').val());
                  
                    $.ajax({
                        url: "./obsSheetAjGrnDetailChange.php",
                        data: "grnDetailId=" + $("#grnDetailId").val() +"&is_table=1",
                        success: function(data){
                          $('#showDetail_table').html(data);
                        }
                    });
                    $.ajax({
                        url: "./obsSheetGetTemp.php",
                        data: "grnDetailId=" + $("#grnDetailId").val() +"&masterMeterId=" + $("#masterMeterId").val() +"",
                        datatype:'json',
                        success: function(data){
                            var json = $.parseJSON(data);
                            $('#temperature').val('');
                            $('#humidity').val('');
                            $('#temperature').val(json['master_temp']);
                            $('#humidity').val(json['master_humidity']);
                        }
                    });
			  	// additional change by girish - start
				$.ajax({
					url: "obsSheetAjCertiNo.php", 
					data: datastring, 
					success: function(data) { 
						$('#selfCertiNo').val(data);
					}
				});
				// additional change by girish - end
	        }
	      });
	    });
	  }
	});
}

function get_table_no(){
    $.ajax({
        url: "./obsSheetAjGrnDetailChange.php",
        data: "grnDetailId=" + $("#grnDetailId").val() +"&is_table=1",
        success: function(data){
          $('#showDetail_table').html(data);
        }
    });
}

function get_table_title(){
    var datastring = "grnDetailId=" + $("#grnDetailId").val() + "&table_no=" + $("#table_no").val();
    $.ajax({
        url: "./obsSheetAjGrnDetailChange.php",
        data: datastring,
        datatype:'json',
        success: function(data){
            var json = $.parseJSON(data);
            $('#table_title').val('');
            console.log('sss'.json);
            $('#table_title').val(json);
        }
    });
}
function getMasterMeterData()
{
	var dataString = "masterMeterId=" + $("#masterMeterId").val();
	$.ajax({
	  type: "GET",
	  url:  "masterMeterJq.php",
	  data: dataString,
	  success: function(data){ 
	    $("#masterpara").html(data);
	    $("#masterMeterSubId").focus();
	    
	    // Ajax Call At Range :Start 
	    $("#masterMeterSubId").change(function(){ 
	      var dataString = "masterMeterSubId=" + $("#masterMeterSubId").val();
	      $.ajax({
	        type: "GET",
	        url:  "masterMeterJqSub.php",
	        data: dataString,
	        success: function(data){
	           $("#masterparasub").html(data);
	           $("#masterMeterSubSubId").focus();
	          	// Get accuracysub With Ajax  :Start 
	          	$("#masterMeterSubSubId").change(function(){ 
	         	  var dataString = "masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
	         	  var dataString2 = "masterMeterSubSubId_2=" + $("#masterMeterSubSubId").val();
	       	    $.ajax({
		       	    type: "GET",
		       	    url:  "masterMeterJqSubAccuracy.php",
		       	    data: dataString,
		       	    success: function(data1){ 
		       	    	$("#rangedisplay").html(data1);
	       	      	}
	       	    });
	       	    $.ajax({
		       	    type: "GET",
		       	    url:  "masterMeterJqSubAccuracy.php",
		       	    data: dataString2,
		       	    success: function(data1){ 
                                var json = $.parseJSON(data1);
                                $("#accuracy_of_rdg").val(json.accuracySubSub);
                                $("#accuracy_of_fs").val(json.accuracySubSubFS);
                                $("#master_stability").val(json.stability);
                                $("#degree_of_freedom").val(json.degreeOfFreedom);
                                $("#uncertinty_from_tracebility").val(json.uncertinty);
                                $("#resolution_of_master").val(json.resolution);
                                $("#accuracy_taking_account").val(json.accuracy_main);
	       	      	}
	       	    });
	          });
	        }	
	      });
	    });
	    // Get accuracysub With Ajax: End
	  }
	});
        
}

function get_stability_of_source(){
    var master_stability_val_s = $('#master_stability').val();
    var stdMeterAverage_val_s = $('.stdMeterAverage').val();
    if(master_stability_val_s == ''){
        master_stability_val_s = 0;
    }
    var master_stability_val_ss = parseFloat(master_stability_val_s).toFixed(4);

    if(stdMeterAverage_val_s == ''){
        stdMeterAverage_val_s = 0;
    }
    var stdMeterAverage_val_ss = parseFloat(stdMeterAverage_val_s).toFixed(4);

    var stability_of_source = (master_stability_val_ss * stdMeterAverage_val_ss)*0.01/Math.sqrt(3);
    if(isNaN(stability_of_source)){
        stability_of_source = 0;
    }
    var stability_of_source_s = parseFloat(stability_of_source).toFixed(4);
    $('.stability_of_source').val(stability_of_source_s);
}

function getUnInPer(theObjInPer)
{
  // var row = $(theObjInPer).closest('tr.tableRow');
  // var ucInPer1 = $(theObjInPer).val() != '' ? $(theObjInPer).val() : 0;
  // var ucInPer = parseFloat(ucInPer1).toFixed(4);
  // var stdMeterAverage1 = $(".stdMeterAverage").val() != '' ? $(".stdMeterAverage").val() : 0;
  // var stdMeterAverage = parseFloat(stdMeterAverage1).toFixed(4);
  // var uncertaintyCalibration = (parseFloat(ucInPer) / 2 * parseFloat(stdMeterAverage)) / 100;
    // if(isNaN(uncertaintyCalibration)) {
       //  uncertaintyCalibration = 0;
    // }
    // uncertaintyCalibration_val = parseFloat(uncertaintyCalibration).toFixed(4);
    // row.find(".uncertaintyCalibration").val(uncertaintyCalibration_val);
}

function getUnInPerSecond(theObjInPerSecond)
{
  // var row = $(theObjInPerSecond).closest('tr.tableRow');
  // var ucInPerSecond = $(theObjInPerSecond).val() != '' ? $(theObjInPerSecond).val() : 0;
  // var stdMeterAverage = $(".stdMeterAverage").val() != '' ? $(".stdMeterAverage").val() : 0;
  
  // additional change by girish - start
  //var uncertaintyCalibrationInPer = (parseFloat(ucInPerSecond) * 100 / parseFloat(stdMeterAverage));
  //row.find(".uncertaintyCalibrationInPer").val(uncertaintyCalibrationInPer.toFixed(4));
  // additional change by girish - end
}

function meanReading(){
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
	var valuesAvailable = 0;
	
	var stdMeterValTotal = 0;
	var stdMeterValArr = [];
	row.find('.stdMeter').each(function(ind,ele){
		if($(this).val() != '') {
			var tmp_val1 = parseFloat($(this).val()) || 0;
			var tmp_val = parseFloat(tmp_val1).toFixed(4);
			stdMeterValTotal = parseFloat(stdMeterValTotal) + parseFloat(tmp_val);
			valuesAvailable++;
			stdMeterValArr.push(tmp_val);
		}
	});

	var stdMeterValTotal1 = parseFloat(stdMeterValTotal).toFixed(4);
	stdMeterAverageObj = stdMeterValTotal1 / valuesAvailable;
	if(isNaN(stdMeterAverageObj)){
            stdMeterAverageObj = 0;
        }
	row.find(".stdMeterAverage").val(stdMeterAverageObj.toFixed(4));
        var accuracyTaken_val = ((stdMeterAverageObj*($("#masterMeterAccuracyArray").val())+($("#masterMeterAccuracyRangeArray").val())*($("#masterMeterAccuracyFSArray").val()))/stdMeterAverageObj);
	if(isNaN(accuracyTaken_val)) {
            accuracyTaken_val = 0;
        }
        accuracyTaken_val = parseFloat(accuracyTaken_val).toFixed(4);
        row.find(".accuracyTaken").val(accuracyTaken_val);
	stdMeterAverageDev = stdMeterAverageObj;
	
	var stdMeterValDeviationTotal = 0;
	$.each(stdMeterValArr,function(ind,tmp_value){
		var tmpDeviation = (parseFloat(tmp_value) - parseFloat(stdMeterAverageDev)) * (parseFloat(tmp_value) - parseFloat(stdMeterAverageDev));
		stdMeterValDeviationTotal = parseFloat(stdMeterValDeviationTotal) + parseFloat(tmpDeviation);
	});
        var standardDeviation_val = (Math.sqrt(stdMeterValDeviationTotal / (valuesAvailable - 1)));
	if(isNaN(standardDeviation_val)) {
            standardDeviation_val = 0;
        }
        standardDeviation_val = parseFloat(standardDeviation_val).toFixed(4);
	row.find(".standardDeviation").val(standardDeviation_val);
	
	standardUncertinity = Math.sqrt(stdMeterValDeviationTotal / (valuesAvailable));
        var valuesAvailable = 0;
	row.find('.stdMeter').each(function(ind,ele){
		if($(this).val() != '') {
			valuesAvailable++;
		}
	});
        var standardUncertinity_val = (standardDeviation_val/Math.sqrt(valuesAvailable));
        if(isNaN(standardUncertinity_val)) {
            standardUncertinity_val = 0;
        }
        standardUncertinity_val = parseFloat(standardUncertinity_val).toFixed(4);
	row.find(".standardUncertinity").val(standardUncertinity_val);
        var standardUncertinityperc_val = (row.find(".standardUncertinity").val()*100/row.find(".stdMeterAverage").val());
        if(isNaN(standardUncertinityperc_val)) {
            standardUncertinityperc_val = 0;
        }
        standardUncertinityperc_val = parseFloat(standardUncertinityperc_val).toFixed(4);
	row.find(".standardUncertinityperc").val(standardUncertinityperc_val);
	row.find(".degreeOfFreedom").val(0);
        var uncertinty_from_tracebility = $('#uncertinty_from_tracebility').val();
        if(uncertinty_from_tracebility == ''){
            uncertinty_from_tracebility = 0;
        }
        var uncertinty_from_tracebility_val = parseFloat(uncertinty_from_tracebility).toFixed(4);
        var uncertinityForTypeB_val = ((uncertinty_from_tracebility_val / 2)*row.find(".stdMeterAverage").val()*0.01);
        if(isNaN(uncertinityForTypeB_val)) {
            uncertinityForTypeB_val = 0;
        }
        uncertinityForTypeB_val = parseFloat(uncertinityForTypeB_val).toFixed(4);
	row.find(".uncertinityForTypeB").val(uncertinityForTypeB_val);
	row.find(".uncertinityInPercentage").val(0);
        get_accuracy_taking_account();
        var accuracyForTypeB_val = ($("#accuracy_taking_account").val() * stdMeterAverageObj * 0.01 / Math.sqrt(3));
        if(isNaN(accuracyForTypeB_val)) {
            accuracyForTypeB_val = 0;
        }
        accuracyForTypeB_val = parseFloat(accuracyForTypeB_val).toFixed(4);
	row.find(".accuracyForTypeB").val(accuracyForTypeB_val);
	row.find(".acuuracyForTypeBPerc").val(0);
        var resolution_of_master = $("#resolution_of_master").val();
        if(resolution_of_master == ''){
            resolution_of_master = 0;
        }
        var resolution_of_master_val = parseFloat(resolution_of_master).toFixed(4);
        var resolutionTypeB_val  = ((resolution_of_master_val / 2)/Math.sqrt(3));
        if(isNaN(resolutionTypeB_val)) {
            resolutionTypeB_val = 0;
        }
        resolutionTypeB_val  = parseFloat(resolutionTypeB_val).toFixed(4);
	row.find(".resolutionTypeB").val(resolutionTypeB_val);
	
	row.find(".resolutionForTypeBPerc").val(0);
	row.find(".stabilityForTypeB").val(0);
	row.find(".stabilityForTypeBInPerc").val(0);
	row.find(".combinedUncertinityInPerc").val(0);
	row.find(".effectiveUncertinityInPer").val(0);
	row.find(".effectiveDegreeOfFreed").val(0);
	row.find(".masterMeterReading").val(0);
	row.find(".effectiveUncertinity").val(0);
	get_stability_of_source();
	var  standardUncertinityComined = row.find(".standardUncertinity").val();
	var  accuracyForTypeBCombined = row.find(".accuracyForTypeB").val();
	var  uncertinityForTypeBcombined = row.find(".uncertinityForTypeB").val();
	var  resolutionTypeBCombined = row.find(".resolutionTypeB").val();
	var  stability_of_source_mas = row.find(".stability_of_source").val();
	var combinedUncertinity_val = (Math.sqrt((standardUncertinityComined * standardUncertinityComined) + (accuracyForTypeBCombined * accuracyForTypeBCombined) + (uncertinityForTypeBcombined * uncertinityForTypeBcombined) + (resolutionTypeBCombined * resolutionTypeBCombined) + (stability_of_source_mas * stability_of_source_mas)));
        if(isNaN(combinedUncertinity_val)) {
            combinedUncertinity_val = 0;
        }
        combinedUncertinity_val = parseFloat(combinedUncertinity_val).toFixed(5);
	row.find(".combinedUncertinity").val(combinedUncertinity_val);
        var expandedUncertinity_val = 2*row.find(".combinedUncertinity").val();
        if(isNaN(expandedUncertinity_val)) {
            expandedUncertinity_val = 0;
        }
        expandedUncertinity_val = parseFloat(expandedUncertinity_val).toFixed(4);
	row.find(".expandedUncertinity").val(expandedUncertinity_val);
        var expandedUncertinityInPre_val = (2*row.find(".combinedUncertinity").val()).toFixed(4) * 100 / stdMeterAverageObj;
        if(isNaN(expandedUncertinityInPre_val)) {
            var expandedUncertinityInPre_val = 0;
        }
	row.find(".expandedUncertinityInPre").val(parseFloat(expandedUncertinityInPre_val).toFixed(4));
        var meanReading_val = row.find(".stdMeterAverage").val();
        if(isNaN(meanReading_val)) {
            meanReading_val = 0;
        }
        meanReading_val = parseFloat(meanReading_val).toFixed(4);
	row.find(".meanReading").val(meanReading_val);
        });
        
	// additional change by girish - partial end end
}

    function testMeterAvg(){
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            var row = $(this);
            var valuesAvailable = 0;

            var testMeterValTotal = 0;
            var testMeterValArr = [];
            row.find('.testMeter').each(function(ind,ele){
                    if($(this).val() != '') {
                            var tmp_val = parseFloat($(this).val()) || 0;
                            testMeterValTotal = testMeterValTotal + tmp_val;
                            valuesAvailable++;
                            testMeterValArr.push(tmp_val);
                    }
            });
            testAverageObj = testMeterValTotal / valuesAvailable;
            if(isNaN(testAverageObj)) {
                testAverageObj = 0;
            }
            testAverageObj = parseFloat(testAverageObj).toFixed(4);
            row.find(".testMeterAverage").val(testAverageObj);
        });
    }
    
    function get_accuracy_taking_account(){
        var accuracy_taking_account_val1 = $('#accuracy_taking_account').val();
        var accuracy_taking_account_val =  parseFloat(accuracy_taking_account_val1).toFixed(4);
        if(isNaN(accuracy_taking_account_val)) {
            accuracy_taking_account_val = 0;
        }
        if(accuracy_taking_account_val <= 0){
            var s_range1 = $('#masterMeterSubSubId').find(':selected').data('range');
            var s_range = parseFloat(s_range1);
            var accuracy_taking_account_val_new = (((($('.stdMeterAverage').val()*$('#accuracy_of_rdg').val())+(s_range*$('#accuracy_of_fs').val()))/100)*100)/$('.stdMeterAverage').val();
            var accuracy_taking_account_val_new1 =  parseFloat(accuracy_taking_account_val_new).toFixed(4);
            if(accuracy_taking_account_val_new == 'Infinity'){
                accuracy_taking_account_val_new1 = 0;
            }
            if(isNaN(accuracy_taking_account_val_new1)) {
                accuracy_taking_account_val_new1 = 0;
            }
            $('#accuracy_taking_account').val(accuracy_taking_account_val_new1);
            
            var accuracyForTypeB_val = ($("#accuracy_taking_account").val() * $('.stdMeterAverage').val() * 0.01 / Math.sqrt(3));
            if(isNaN(accuracyForTypeB_val)) {
                accuracyForTypeB_val = 0;
            }
            accuracyForTypeB_val = parseFloat(accuracyForTypeB_val).toFixed(4);
            $(".accuracyForTypeB").val(accuracyForTypeB_val);
        } 
    }
</script>
{include file="./headEnd.tpl"}
<form action="{$smarty.server.PHP_SELF}" id="form1" name="form1" method='POST'>
<input type="hidden" name="grnDetailPassId" value={$grnDetailPassId} />
<span class="center"><h2>Observation Sheet</h2></span><br>
<table border='1' cellpadding='5' cellspacing='0' align='left'>
  <tr>
    
      <td colspan="9"  NOWRAP><span style="background-color: aquamarine; padding-top: 11px; padding-bottom: 8px;"><font size="4">Master Meter.:-</font>
     <select name="masterMeterId" id="masterMeterId" onchange="getMasterMeterData();">
  	 <option value="0">Select Meter</option>
       {html_options values=$meterEntryIdArray output=$meterEntryNameArray}  
    </select>
    <span id="masterpara"></span>         
    <span id="masterparasub"></span>  
    </span>
    Date Of Cali.
     {html_select_date time="$callibrationDate" prefix="callibrationDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
      Next Recom. Cali. date
    {html_select_date time="$nextYearDate" prefix="nextYearDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY"  day_value_format="%02d"}
    Certi. Issue Date 
	 {if $certificateIssue == "Y"}
	 <input type="checkbox" name="certificateIssue" value="1" CHECKED />
	 {else}
	 <input type="checkbox" name="certificateIssue" value="1" />
	 {/if}
	 {html_select_date time="$certificateIssueDate" prefix="certificateIssueDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY"  day_value_format="%02d"}
	 
    </td>
  </tr>
  <tr>
  	<td align="left" colspan="1" bgcolor="aquamarine" style="white-space: nowrap;">
            Accuracy % of Rdg. : <input type="text" size="6" name="accuracy_of_rdg" id="accuracy_of_rdg" />
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
             Accuracy % of FS : <input type="text" size="6" name="accuracy_of_fs" id="accuracy_of_fs" />
  	</td>
        <td align="left" colspan="1" bgcolor="aquamarine" style="white-space: nowrap;">
            Stability : <input type="text" size="6" name="master_stability" id="master_stability" />
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Resolution of master : <input type="text" size="6" name="resolution_of_master" id="resolution_of_master" />
  	</td>
  	<td align="left" colspan="1" bgcolor="aquamarine">
            Degree of freedom : <input type="text" size="6" name="degree_of_freedom" id="degree_of_freedom" />
  	</td>
  	<td align="left" colspan="1" bgcolor="aquamarine">
            Uncertinty of master from Tracebility : <input type="text" size="6" name="uncertinty_from_tracebility" id="uncertinty_from_tracebility" />
  	</td>
  	<td align="left" colspan="1" bgcolor="aquamarine">
            Accuracy % taking in to account : <input type="text" size="6" name="accuracy_taking_account" id="accuracy_taking_account" />
  	</td>
 </tr>  
  <tr>
      <td>GRN NO.: <b>N-<b/></font>
      <select name="grnId" id="grnId" onchange="getGrnData();" autofocus=autofocus required="required">
        <option value="">GRN</option>
        {html_options values=$grnId output=$grnPrefixNo}
      </select>
      <font size="3">&nbsp; &nbsp; &nbsp; &nbsp; Cust Id No.:-</font>
      <input type="text" name="custCode" id="custCode" value="" size="2" />
    </td>
    <td align="center" bgcolor="lightblue">
            INSTRUMENT.:-<span id="showItem"></span>
            <div style="white-space: nowrap;" id="showDetail" required=required></div>
            <div style="white-space: nowrap;" id="showDetail_table" required=required></div>
            
      </td>
      <td align="center" bgcolor="lightblue">Certificate No :<input type="text" name="selfCertiNo" id="selfCertiNo" required=required/></td>
      <td align="center" bgcolor="aquamarine">Temperature : <input type="text" name="temperature" id="temperature" value="" size="10" required=required/></td>
      <td align="center" bgcolor="aquamarine">Relative Humidity : <input type="text" name="humidity" id="humidity" value="" size="10" required=required/></td>
      
  </tr>
  
  <tr>
    
    
    
    
  </tr>    
  <tr>
          <input type="hidden" name="" /><input type="hidden" size="6" name="ratio1" /><input type="hidden" name="ratio2" />
          <td align="left" colspan="1" bgcolor="lightblue" style="white-space: nowrap;">
            Confidence level : <input type="text" size="6" name="confidence_level" id="confidence_level" value="95" />
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            Covarage factor : <input type="text" size="6" name="covarage_factor" id="covarage_factor" value="1.96" />
  	</td>
         <td align="left" bgcolor="lightblue" colspan="2" style="white-space: nowrap;">
             Least Count : <input type="text" name="leastCount" id="leastCount" value="" size="7" required=required/>
    Accuracy : <input type="text" name="accuracy" id="accuracy" value="" size="7" required=required/>
        Remarks : <input type="text" name="certiRemarks" size="30" />
    </td>
    <td align="center" bgcolor="lightblue">Make/Model :<input type="text" name="makeModel" id="makeModel" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Instrument I.D. No. : <input type="text" name="instrumentId" id="instrumentId" value="" size="10" required=required/></td>
 </tr>    
  <tr>
      <td align="left" colspan="5" bgcolor="lightblue">
          UUC Range : <input type="text" id="uuc_range" name="range" value="" /> &nbsp; &nbsp; &nbsp; 
          Calibration Location : <select name="cali_location" style="width: 80px;"><option value="1">At Lab</option><option value="2">At Site</option></select>&nbsp; &nbsp; &nbsp; 
          UUC Location : <input type="text" id="uuc_location" name="uuc_location" value="" /> &nbsp; &nbsp; &nbsp; 
          Approved By : 
          <select name="approved_by" id="approved_by">
              {html_options values=$approveArray output=$approveNameArray}  
           </select>
          &nbsp; &nbsp; &nbsp; 
          Table Info : 
          <select name="table_info_id" id="table_info_id" required="">
                <option value="">-- Select --</option>
                {foreach $tableArray as $item}
                    <option value="{$item.table_info_id}" data-image_url="{$item.image_url}">{$item.table_name}</option>
                {/foreach}
          </select>
          &nbsp; &nbsp; &nbsp; 
      </td>
 </tr>    
    
</table>
<table align="left" border='1' cellpadding='5' cellspacing='0'>
  <tbody id="mainDiv">
  <tr class="tableRow">
      <td valign="bottom" align="left" style="white-space: nowrap;"><br/><b>UUC Reading (<span id="box_count_filled"> </span>/<span id="uut_box_total"> </span>)&nbsp; &nbsp;</b>
      	<input type="button" value=" + " class="std_add" />
  		<span class="std_values">
  			<span class="main_std main_std1"> 
                            <input type="button" value=" - " class="std_remove" /><input type="text" name="stdMeter[][]" class="stdMeter num_only" id="stdMeter_1"  size="4" align="right" title="stdMeter"  >
	  		</span>
  			<span class="main_std"> 
	      		<input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter"  >
	  		</span>
  			<span class="main_std"> 
	      		<input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter"  >
	  		</span>
  			<span class="main_std"> 
	      		<input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter"  >
	  		</span>
  			<span class="main_std"> 
                        <input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter"  >
	  		</span>
  		</span>
            <br/>
            <br/>
            <b>Master 1 Reading (<span id="test_box_count_filled"> </span>/<span id="test_box_total"> </span>)</b>
            <input type="button" value=" + " class="testMeter_add" />
            <span class="testMeter_values">
                <span class="main_testMeter1 main_testMeter"> 
                <input type="button" value=" - " class="testMeter_remove" /><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only"  size="4" align="right" title="testMeter"   >
                </span>
            </span>
            <br/>
            <br/>
            <b>Master 2 Reading (<span id="test_box_count_filled2"> </span>/<span id="test_box_total2"> </span>)</b>
            <input type="button" value=" + " class="testMeter2_add" />
            <span class="testMeter2_values">
                <span class="main_testMeter21 main_testMeter2"> 
                <input type="button" value=" - " class="testMeter2_remove" /><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only"  size="4" align="right" title="testMeter"   >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  >
                </span>
            </span>
            <br/>
            <br/>
            <b>Master 3 Reading (<span id="test_box_count_filled3"> </span>/<span id="test_box_total3"> </span>)</b>
            <input type="button" value=" + " class="testMeter3_add" />
            <span class="testMeter3_values">
                <span class="main_testMeter31 main_testMeter3"> 
                <input type="button" value=" - " class="testMeter3_remove" /><input type="text" name="testMeter3[][]" id="testMeter3_1" class="testMeter3 num_only"  size="4" align="right" title="testMeter"   >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  >
                </span>
            </span>
             <br/>
             <br/>
            <input type="button" id="view_calculation" value="View / Refresh Calculation" class="button" />
            <input type="button" id="hide_calculation" value="Hide Calculation" class="button" />
            <br/>
            <br/>
            <table align="left" border='1' cellpadding='5' cellspacing='0' id="calculation_table" style="display: none;">
            <tr>
                <td colspan="8"><b>Type A </b></td>
            </tr>
            <tr>
                <td>Mean Avg. </td><td><input type="text" name="stdMeterAverage[]"     class="stdMeterAverage"     size="8" align="right" title="stdMeterAverage" value="0" READONLY></td>
                <td>Std Devia. </td><td><input type="text" name="standardDeviation[]"   class="standardDeviation"   size="8" align="right" title="standardDeviation" READONLY></td>
                <td>Std Uncert.	</td><td><input type="text" name="standardUncertinity[]" class="standardUncertinity" size="8" align="right" title="standardUncertinity" READONLY></td>
                <td>Std Uncert. % </td><td><input type="text" name="standardUncertinityperc[]" class="standardUncertinityperc" size="8" align="right" title="standardUncertinityperc" READONLY></td>
                {*<input type="hidden" name="stdMeterAverage[]"     class="stdMeterAverage"     size="8" align="right" title="stdMeterAverage" value="0" READONLY>
                <input type="hidden" name="standardDeviation[]"   class="standardDeviation"   size="8" align="right" title="standardDeviation" READONLY>
                <input type="hidden" name="standardUncertinity[]" class="standardUncertinity" size="8" align="right" title="standardUncertinity" READONLY>
                <input type="hidden" name="standardUncertinityperc[]" class="standardUncertinityperc" size="8" align="right" title="standardUncertinityperc" READONLY>*}
                <input type="hidden" name="degreeOfFreedom[]"       class="degreeOfFreedom"         size="3" align="right" title="degreeOfFreedom" READONLY>
            </tr>
            <tr>
                <input type="hidden" name="uncertaintyCalibrationInPer[]" class="uncertaintyCalibrationInPer num_only"  title="uncertaintyCalibrationInPer" onblur="getUnInPer(this);" >
                <input type="hidden" name="uncertaintyCalibration[]" class="uncertaintyCalibration num_only"  title="uncertaintyCalibration" onblur="getUnInPerSecond(this);" >
                <input type="hidden" name="resolutionTypeA[]" class="resolutionTypeA num_only"  title="Resolution"  >
                <input type="hidden" name="accuracyTaken[]"       class="accuracyTaken num_only"  title="accuracyTaken" READONLY >
            </tr>
            <tr>
                <td colspan="8"><b>Type B </b></td>
            </tr>
            <tr>
                <td>Degree of freedom </td><td><input type="text" name="degree_of_freedom_add[]"       class="degree_of_freedom_add"     size="8" align="right" title="" READONLY></td>
                <td>Uncert. B </td><td><input type="text" name="uncertinityForTypeB[]"       class="uncertinityForTypeB"     size="8" align="right" title="Uncertanity of Master" READONLY ></td>
                <input type="hidden" name="uncertinityInPercentage[]" class="uncertinityInPercentage" size="3" align="right" title="uncertinityInPercentage" READONLY >
                <td>Accu. of Cali.	</td><td><input type="text" name="accuracyForTypeB[]"       class="accuracyForTypeB"     size="8" align="right" title="accuracyForTypeB" READONLY ></td>
                <input type="hidden" name="acuuracyForTypeBPerc[]" class="acuuracyForTypeBPerc" size="8" align="right" title="acuuracyForTypeBPerc" READONLY >
                <td>Resol. </td><td><input type="text"   name="resolutionTypeB[]"         class="resolutionTypeB"         size="8" align="right" title="resolutionTypeB" READONLY ></td>
                <input type="hidden" name="resolutionForTypeBPerc[]"  class="resolutionForTypeBPerc"  size="3" align="right" title="resolutionForTypeBPerc" READONLY >
                <input type="hidden" name="stabilityForTypeB[]"       class="stabilityForTypeB"       size="3" align="right" title="stabilityForTypeB" READONLY >
                <input type="hidden" name="stabilityForTypeBInPerc[]" class="stabilityForTypeBInPerc" size="3" align="right" title="stabilityForTypeBInPerc" READONLY >
                
                <input type="hidden" name="combinedUncertinityInPerc[]" class="combinedUncertinityInPerc" size="3" align="right" title="combinedUncertinityInPerc" READONLY >
                <input type="hidden" name="effectiveUncertinity[]"      class="effectiveUncertinity"      size="3" align="right" title="effectiveUncertinity" READONLY >
                <input type="hidden" name="effectiveUncertinityInPer[]" class="effectiveUncertinityInPer" size="3" align="right" title="effectiveUncertinityInPer" READONLY >
                <input type="hidden" name="effectiveDegreeOfFreed[]"    class="effectiveDegreeOfFreed"    size="3" align="right" title="effectiveDegreeOfFreed" READONLY >
                <input type="hidden" name="meanReading[]"               class="meanReading"               size="3" align="right" title="MeanReading" READONLY >
                <input type="hidden" name="masterMeterReading[]"        class="masterMeterReading"        size="3" align="right" title="MeanReading" READONLY >
                <input type="hidden" name="error[]"                     class="error"                     size="3" align="right" title="Error" READONLY >
            </tr>
            <tr>
                <td>Stability of Source</td><td><input type="text"   name="stability_of_source[]"       class="stability_of_source num_only"       size="8" align="right" title="stability_of_source" READONLY></td>
                <td>Comb. Uncer. </td><td><input type="text"   name="combinedUncertinity[]"       class="combinedUncertinity"       size="8" align="right" title="combinedUncertinity" READONLY ></td>
                <td>Expan. Uncer. </td><td><input type="text" name="expandedUncertinity[]"       class="expandedUncertinity"       size="8" align="right" title="ExpandedUncertinity" READONLY ></td>
                <td>Expan. Uncer. In % </td><td><input type="text" name="expandedUncertinityInPre[]"  class="expandedUncertinityInPre"  size="8" align="right" title="ExpandedUncertinityInPer" READONLY ></td>
                <input type="hidden" size="8" style="background-color:#000000;" READONLY />
                <input type="hidden" name="testMeterAverage[]" class="testMeterAverage" title="testMeter5" READONLY >
                </tr>
        </table>
    </td>
    {*<td valign="bottom"><span><input type="button" value="Add" class="add" /></span></td>*}
  </tr>
  </tbody>
  <tr>
    <td align="center" colspan="28"><input type="submit" name="insertBtn" value="Submit" class="button" /></td>
  </tr>
</table>
  <span id="image_span"></span>
<div id="rangedisplay"></div> 
</table>
{if (have_access_role($smarty.const.OBS_MODULE_ID,"view"))}
<div id="obsDetail"></div>
{/if}
</form>
<tr id="row_template" class="tableRow" style="display:none;">
	<td valign="bottom" align="left" style="white-space: nowrap;">
		<input type="button" value=" + " class="std_add">
		<span class="std_values">
			<span class="main_std">
				<input type="button" value=" - " class="std_remove"><input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter" required="required"  >
			</span>
			<span class="main_std">
				<input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter" required="required"  >
			</span>
			<span class="main_std">
				<input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter" required="required"  >
			</span>
			<span class="main_std">
				<input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter" required="required"  >
			</span>
			<span class="main_std">
				<input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter" required="required"  >
			</span>
		</span>
	</td>
	<td valign="bottom"><input type="text" name="stdMeterAverage[]" class="stdMeterAverage" size="3" align="right" title="stdMeterAverage" value="0" readonly=""></td>
	<td valign="bottom"><input type="text" name="standardDeviation[]" class="standardDeviation" size="3" align="right" title="standardDeviation" readonly=""></td>
	<td valign="bottom"><input type="text" name="standardUncertinity[]" class="standardUncertinity" size="3" align="right" title="standardUncertinity" readonly=""></td>
	<td valign="bottom">
	<input type="text" name="standardUncertinityperc[]" class="standardUncertinityperc" size="3" align="right" title="standardUncertinityperc" readonly="">
	<input type="hidden" name="degreeOfFreedom[]" class="degreeOfFreedom" size="3" align="right" title="degreeOfFreedom" readonly="">
	</td>

        <input type="hidden" name="uncertaintyCalibrationInPer[]" class="uncertaintyCalibrationInPer"  title="uncertaintyCalibrationInPer" onblur="getUnInPer(this);">
	<!-- additional change by girish - start -->
	<!-- <td valign="bottom" align="center"><input type="text" name="uncertaintyCalibration[]" class="uncertaintyCalibration"  size="5" align="right" title="uncertaintyCalibration" required=required onblur="meanReading(this); getUnInPerSecond(this);" ></td> -->
        <input type="hidden" name="uncertaintyCalibration[]" class="uncertaintyCalibration"  title="uncertaintyCalibration" onblur="getUnInPerSecond(this);">
	<!-- additional change by girish - end -->
        <input type="hidden" name="resolutionTypeA[]" class="resolutionTypeA" title="Resolution"  >
	<td valign="bottom"><input type="text" name="accuracyTaken[]" class="accuracyTaken" size="3" align="right" title="accuracyTaken" readonly=""></td>
	<td valign="bottom">
	<input type="text" name="uncertinityForTypeB[]" class="uncertinityForTypeB" size="3" align="right" title="Uncertanity of Master" readonly="">
	<input type="hidden" name="uncertinityInPercentage[]" class="uncertinityInPercentage" size="3" align="right" title="uncertinityInPercentage" readonly="">
	</td>
	<td valign="bottom">
	<input type="text" name="accuracyForTypeB[]" class="accuracyForTypeB" size="3" align="right" title="accuracyForTypeB" readonly="">
	<input type="hidden" name="acuuracyForTypeBPerc[]" class="acuuracyForTypeBPerc" size="3" align="right" title="acuuracyForTypeBPerc" readonly="">
	</td>
	<td valign="bottom">
	<input type="text" name="resolutionTypeB[]" class="resolutionTypeB" size="3" align="right" title="resolutionTypeB" readonly="">
	<input type="hidden" name="resolutionForTypeBPerc[]" class="resolutionForTypeBPerc" size="3" align="right" title="resolutionForTypeBPerc" readonly="">
	<input type="hidden" name="stabilityForTypeB[]" class="stabilityForTypeB" size="3" align="right" title="stabilityForTypeB" readonly="">
	<input type="hidden" name="stabilityForTypeBInPerc[]" class="stabilityForTypeBInPerc" size="3" align="right" title="stabilityForTypeBInPerc" readonly="">
	</td>
	<td valign="bottom">
	<input type="text" name="combinedUncertinity[]" class="combinedUncertinity" size="3" align="right" title="combinedUncertinity" readonly="">
	<input type="hidden" name="combinedUncertinityInPerc[]" class="combinedUncertinityInPerc" size="3" align="right" title="combinedUncertinityInPerc" readonly="">
	<input type="hidden" name="effectiveUncertinity[]" class="effectiveUncertinity" size="3" align="right" title="effectiveUncertinity" readonly="">
	<input type="hidden" name="effectiveUncertinityInPer[]" class="effectiveUncertinityInPer" size="3" align="right" title="effectiveUncertinityInPer" readonly="">
	<input type="hidden" name="effectiveDegreeOfFreed[]" class="effectiveDegreeOfFreed" size="3" align="right" title="effectiveDegreeOfFreed" readonly="">
	<input type="hidden" name="meanReading[]" class="meanReading" size="3" align="right" title="MeanReading" readonly="">
	<input type="hidden" name="masterMeterReading[]" class="masterMeterReading" size="3" align="right" title="MeanReading" readonly="">
	<input type="hidden" name="error[]" class="error" size="3" align="right" title="Error" readonly="">
	</td>
	<td valign="bottom"><input type="text" name="expandedUncertinity[]" class="expandedUncertinity" size="5" align="right" title="ExpandedUncertinity" readonly=""></td>
	<td valign="bottom"><input type="text" name="expandedUncertinityInPre[]" class="expandedUncertinityInPre" size="5" align="right" title="ExpandedUncertinityInPer" readonly=""></td>
	<td><input type="text" size="4" style="background-color:#000000;" readonly=""></td>
	<td valign="bottom"><input type="text" name="testMeter1[]" class="testMeter1" size="3" align="right" title="testMeter1" required="required" ></td>
	<td valign="bottom"><input type="text" name="testMeter2[]" class="testMeter2" size="3" align="right" title="testMeter2" required="required" ></td>
	<td valign="bottom"><input type="text" name="testMeter3[]" class="testMeter3" size="3" align="right" title="testMeter3" required="required" ></td>
	<td valign="bottom"><input type="text" name="testMeter4[]" class="testMeter4" size="3" align="right" title="testMeter4" required="required" ></td>
	<td valign="bottom"><input type="text" name="testMeter5[]" class="testMeter5" size="3" align="right" title="testMeter5" required="required" ></td>
	<td valign="bottom"><input type="text" name="testMeterAverage[]" class="testMeterAverage" size="5" align="right" title="testMeter5" readonly=""></td>
	<td valign="bottom"><span><input type="button" value="Add" class="add"></span></td>
	<td valign="bottom"><span><input type="button" value="Remove" class="delete"></span></td>
</tr>
{include file="footer.tpl"}