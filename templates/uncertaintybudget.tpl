{include file="./headStart.tpl"}
{include file="./headEnd.tpl"}
<form action="{$smarty.server.PHP_SELF}" id="form1" name="form1" method='POST'>
<center class="center"><h2>Uncertinity Budget</h2></center><br>
<table align='center' width=80% border='1' cellpadding='1' cellspacing='2'>
  <tr>
     <td class="table1" align='center'></td>
     <td class="table1" align='center'></td>
     <td class="table1" align='center'></td>
     <td class="table1" align='center'>Voltage</td>
     <td class="table1" align='center'></td>
     <td class="table1" align='center'></td>
     <td class="table1" align='center'></td>
     <td class="table1" align='center'></td>
     <td class="table1" align='center'></td>
  </tr>
   <tr>
     <td class="table1" align='center'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
     <td class="table1" align='center'>Source of Uncertainty Xi</td>
     <td class="table1" align='center' >Estimate xi</td>
     <td class="table1" align='center'>Voltage Limits %</td>
     <td class="table1" align='center'>Probability Distribution Type A or B Factor  </td>
     <td class="table1" align='center'> Standard Uncertainty %</td>
     <td class="table1" align='center'>Sensitivity Coefficient Ci </td>
     <td class="table1" align='center'>Uncertainty contribution Ui(y) %</td>
     <td class="table1" align='center'>Degree of freedom Vi</td>
  </tr>
  <tr>
    <td class="table1" align='center'>Uncertinty of master</td>
    <td class="table1" align='center'>U1</td>
    <td class="table1" align='center'></td>
    <td class="table1" align='center'>{$uncertinty}</td>
    <td class="table1" align='center'>Normal type &nbsp;B-1.96</td>
    <td class="table1" align='center'>{$uncertinityInPercentage}</td>
    <td class="table1" align='center'>1</td>
    <td class="table1" align='center'>{$uncertinityInPercentage}</td>
    <td class="table1" align='center'>Infinity</td>
  </tr>
  <tr>
    <td class="table1" align='center'>Master Accuracy %</td>
    <td class="table1" align='center'>U2</td>
    <td class="table1" align='center'></td>
    <td class="table1" align='center'>{$accuracyTaken}</td>
    <td class="table1" align='center'>RectangularType Bsqrt(3)</td>
    <td class="table1" align='center'>{$acuuracyForTypeBPerc}</td>
    <td class="table1" align='center'>1</td>
    <td class="table1" align='center'>{$acuuracyForTypeBPerc}</td>
    <td class="table1" align='center'>Infinity</td>
  </tr>
  <tr>
    <td class="table1" align='center'>Resolution of UUT</td>
    <td class="table1" align='center'>U3</td>       
    <td class="table1" align='center'></td>                       
    <td class="table1" align='center'>{$resolution}</td>                       
    <td class="table1" align='center'>RectangularType Bsqrt(3)</td>                       
    <td class="table1" align='center'>{$resolutionForTypeBPerc|string_format:"%.3f"}</td>
    <td class="table1" align='center'>1</td>
    <td class="table1" align='center'>{$resolutionForTypeBPerc|string_format:"%.3f"}</td>
    <td class="table1" align='center'>Infinity</td>
  </tr>
  <tr>
    <td class="table1" align='center'>Stability</td>
    <td class="table1" align='center'>U4</td>
    <td class="table1" align='center'></td>                       
    <td class="table1" align='center'>{$stability}</td>                       
    <td class="table1" align='center'>RectangularType Bsqrt(3)</td>                       
    <td class="table1" align='center'>{$stabilityForTypeBInPerc}</td>
    <td class="table1" align='center'>1</td>
    <td class="table1" align='center'>{$stabilityForTypeBInPerc}</td>
    <td class="table1" align='center'>Infinity</td>
  </tr>
  <tr>
    <td class="table1" align='center'></td>
    <td class="table1" align='center'>Repeatability(Ur)</td>
    <td class="table1" align='center'>{$stdMeterAverage}</td>                       
    <td class="table1" align='center'></td>                       
    <td class="table1" align='center'>NormalType A</td>                       
    <td class="table1" align='center'>{$standardUncertinityperc}</td>
    <td class="table1" align='center'>1</td>
    <td class="table1" align='center'></td>
    <td class="table1" align='center'>{$degreeOfFreedom}</td>
  </tr>
  <tr>
    <td class="table1" align='center'></td>
    <td class="table1" align='center'>Uc</td>
    <td class="table1" align='center'></td>                       
    <td class="table1" align='center'></td>                       
    <td class="table1" align='center'></td>                       
    <td class="table1" align='center'></td>
    <td class="table1" align='center'></td>
    <td class="table1" align='center'>0.184</td>
    <td class="table1" align='center'>Infinity</td>
  </tr>
  <tr>
    <td class="table1" align='center'>Master Accuracy %</td>
    <td class="table1" align='center'>Expanded Uncertainty (U)</td>
    <td class="table1" align='center'></td>
    <td class="table1" align='center'></td>
    <td class="table1" align='center'>K=2</td>
    <td class="table1" align='center'></td>
    <td class="table1" align='center'></td>
    <td class="table1" align='center'>0.368</td>
    <td class="table1" align='center'>Infinity</td>
  </tr>
</table>
  <div id="rangedisplay"></div> 
</form>
{include file="footer.tpl"}