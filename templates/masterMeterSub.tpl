{include file="./headStart.tpl"}
<title>Sub Master Meter</title>
<script type="text/javascript" src="./js/jquerynew.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  if( {$isEdit} > 0 )
  {
    getParameter();
    getDateByPara()
  }
  $(".add").click(function(){
    $("#one").clone(true).appendTo("#mainDiv").find('.hidden_val').val('');
  });
  $(".delete").click(function(){ 
  	$(this).parent().remove();
  });
  
  $("#masterMeterId").change(function(){
    getParameter();
  });
  
  $("#form1").submit(function(e){
        e.preventDefault();
            var form_obj = $(this);
            $.ajax({
                type: "POST",
                url: form_obj.attr('action'),
                data: form_obj.serialize(), //serializes the form's elements.
                dataType:'json',
                success: function(data){
                    console.log(data.status);
                    if(data.status == 'reload'){
                        window.location.href = "masterMeterSub.php";
                    } else {
                        getDateByPara();
                    }
                    
                }
            });
	});
        
    $(document).on('change', '.accuracy_class', function(){
        var accuracySubSub = $.trim($(".accuracySubSub").val());
        var accuracySubSubFS = $.trim($(".accuracySubSubFS").val());
        if (accuracySubSub !== '') {
            $(".accuracy_main").val('0');
            $(".accuracy_main").attr('readonly','readonly');
        }
        if (accuracySubSubFS !== '') {
            $(".accuracy_main").val('0');
            $(".accuracy_main").attr('readonly','readonly');
        }
        if ((accuracySubSub == '') && (accuracySubSubFS == '')) {
            $(".accuracy_main").removeAttr('readonly','readonly');
        }
    });
    $(".accuracySubSub").change();
});
function getParameter()
{
  var dataString = "masterMeterId=" + $("#masterMeterId").val() + "&masterMeterSubId=" + $(".masterMeterSubId").val();
	$.ajax({
	 type: "GET",
	 url:  "masterMeterJq.php",
	 data: dataString,
	 success: function(data){ 
	 	$("#masterpara").html(data);
	 }
	});
}

function getAllDate()
{
	 var dataString = "masterMeterId=" + $("#masterMeterId").val();
   $.ajax({
   	 type: "POST",
     url:  "masterMeterAllData.php",
     data: dataString,
     success: function(data){ 
     	$(".allData").html(data);
     }
   });
}
function getDateByPara()
{
	 var dataString = "masterMeterId=" + $('#masterMeterId').val() + "&masterMeterSubId=" + $('#masterMeterSubId').val();
   $.ajax({
   	 type: "POST",
     url:  "masterMeterAllData.php",
     data: dataString,
     success: function(data){ 
     	$(".allData").html(data);
     }
   });
   var dataString2 = "masterMeterId=" + $('#masterMeterId').val() + "&masterMeterSubId=" + $('#masterMeterSubId').val() + "&masterMeterSubIdEdit=" + $('#masterMeterSubIdEdit').val();
        $.ajax({
   	 type: "POST",
     url:  "masterMeter_last_data.php",
     data: dataString2,
     success: function(data1){ 
        var json = $.parseJSON(data1);
        $(".range").val('');
        $(".range_dis").val('');
        $(".range_unit").val(json.range_unit);
        $(".frequency").val(json.frequency);
        $(".accuracySubSub").val(json.accuracySubSub);
        $(".accuracySubSubFS").val(json.accuracySubSubFS);
        $(".accuracy_main").val(json.accuracy_main);
        $(".resolution").val(json.resolution);
        $(".coverage_factor").val(json.coverage_factor);
        $(".cmc").val(json.cmc);
        $(".stability").val(json.stability);
        $(".uncertinty").val(json.uncertinty);
        $(".degreeOfFreedom").val(json.degreeOfFreedom);
     }
   });
}
</script>
<form action="{$smarty.server.PHP_SELF}" id="form1" name="form1" method='POST'>
{include file="./headEnd.tpl"}
<table border = "5" align="center" cellpadding="1" cellspacing="1">
<tr>
  <td align="center" class="table1" colspan="4">Master Meter Entry</td>
</tr>
<tr>
  <td align="right" class="table2">Meter Selection</td>
  <td>
      <select name="masterMeterId" id="masterMeterId" required="" onchange="getAllDate();">
  	 <option value="">Select Meter</option>
       {html_options values=$meterEntryIdArray output=$meterEntryNameArray selected=$editArray.masterMeterId}
    </select>
 	</td>
 	<td><div id="masterpara"></div></td>
	</tr>
</table>
{if (have_access_role($smarty.const.MASTER_METER_SUB_MODULE_ID,"add") && {!isset($smarty.get.masterMeterSubSubId)}) || (have_access_role($smarty.const.MASTER_METER_SUB_MODULE_ID,"edit") && {isset($smarty.get.masterMeterSubSubId)})}
<center><h3>* Keep Accuracy 0 to use formula</h3></center>
<table border="2" width="100%" id="dataTable" align="center" left="10"cellpadding="0" cellspacing="0">
  <tbody id="mainDiv">
    <tr id="one" class="itemRow">	
      <td>
        <input type="hidden" name="masterMeterSubSubId" class="masterMeterSubSubId hidden_val" value="{$editArray.masterMeterSubSubId}" />
        <input type="hidden" name="masterMeterSubIdEdit" id="masterMeterSubIdEdit" class="masterMeterSubId hidden_val" value="{$editArray.masterMeterSubId}" />
      	Range Calc.
        <input type="text" name="range[]" class="range" size="4" required="" value="{$editArray.range}" />
      	Range Disp.
        <input type="text" name="range_disp[]" class="range_disp" size="4" required="" value="{$editArray.range_disp}" />
        Range unit
          <input type="text" name="range_unit[]" class="range_unit" size="4" value="{$editArray.range_unit}" />
        Frequency
          <input type="text" name="frequency[]" class="frequency" size="4" value="{$editArray.frequency}" />
        Accu. % of Rdg.
          <input type="text" name="accuracySubSub[]" class="accuracySubSub accuracy_class" size="4" value="{$editArray.accuracySubSub}" />
        Accu. % of FS
          <input type="text" name="accuracySubSubFS[]" class="accuracySubSubFS accuracy_class " size="3" value="{$editArray.accuracySubSubFS}" />
        Accuracy *
          <input type="text" name="accuracy_main[]" class="accuracy_main" size="5" value="{$editArray.accuracy_main}" />
        Resolution
          <input type="text" name="resolution[]" class="resolution"  size="4" value="{$editArray.resolution}"/>
        Coverage Factor
          <input type="text" name="coverage_factor[]" class="coverage_factor"  size="4" value="{$editArray.coverage_factor}"/>
        CMC
          <input type="text" name="cmc[]" class="cmc"  size="4" value="{$editArray.cmc}"/>
        Stability
          <input type="text" name="stability[]" class="stability" size="4" value="{$editArray.stability}" />
        Uncertinity
          <input type="text" name="uncertinty[]" class="uncertinty"  size="4" value="{$editArray.uncertinty}"/>
        Degree of Freedom
          <input type="text" name="degreeOfFreedom[]" class="degreeOfFreedom" size="3" value="{$editArray.degreeOfFreedom}" />
        {if !isset($smarty.get.masterMeterSubSubId)}
        {*<input type="button" name="add[]" value="Add Param" class="add" />
        <input type="button" name="delete[]" value="Delete" class="delete"/>*}
        {/if}
      </td>
    </tr>
   </tbody>
</table>
<table border="2" align="center">
  <tr>
    <td><input type="submit" name="insertBtn" value="S  T  O  R  E" class="button" /></td>
  </tr>
</table>
{/if}        
</form>
<br><br>
<div class="allData"></div>

<script type="text/javascript"> 
  var mutli_education = document.form1.elements["range[]"].value;
  mutli_education.setAttributeNodeNS(mutli_education.cloneNode(true));
</script>

{include file="footer.tpl"}