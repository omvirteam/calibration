{include file="./headStart.tpl"}
<title>Delivery Challan</title>
<!-- code for user security-->

<link type="text/css" href="./css/maincombobox.css" />
<script type="text/javascript">
  $(document).ready(function(){
    $('#grnPrefix').focus();
    customerIdFunc();
    $('#custName').attr("disabled", true);
    $('#custCode').attr("disabled", true);
    $('#address').attr("disabled", true);
    $(document).keydown(function(e) {
    	var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      if(code == 13){
        window.event.keyCode = 9;
      }
    });
    $('#grnPrefix').change(function(){
      var grnPrefix = $("#grnPrefix").val();
      var datastring = 'grnPrefix='+ grnPrefix;
      $.ajax({
        url: "grnEntryJq.php",
        data: datastring,
        success: function(data){
          $("#grnNo").val(data);
        }
      });
    });

	  $("#customerId").change(function ()
	  {
	    if($("#customerId").val() > 0)
	    {
	      $('#custName').val("");
	      $('#custName').attr("disabled", true);
	      $('#custCode').attr("disabled", true);
	      $('#address').attr("disabled", true);
	    }
	    else
	    {
	      $('#custName').removeAttr("disabled");
	      $('#custCode').removeAttr("disabled");
	      $('#address').removeAttr("disabled");
	    }
	  });
  });

  function customerIdFunc(){
    var customerId = $("#customerId").val();
    var datastring = 'customerId=' + customerId;
    $.ajax({
          url: "customerCodeJq.php",
          data: datastring,
          success: function(data){
            $("#custCode").val(data);
          }
    });
  }

</script>
<script>
  $('#parameterId').val(function() {
    return val + (!val ? '' : ', ') + 'parameterId';
      alert("val")
  });
</script>
{include file="./headEnd.tpl"}
<form name="form1" id="form1" method="post">
<div>
<input type="hidden" name="paramhidden" id="paramhidden">        
<input type="hidden" name="gatePassId" size="5" value="{$gatePassId}">     
<table border="0" align="center">
  <tr>
    <td>GRN No:
      <select name="grnPrefix" id="grnPrefix" >
        {html_options values=$grnPrefix output=$grnPrefix selected=$grnPrefixSelected}
      </select>
    </td>
    <input type="hidden" name="grnId" id="grnId" value="{$grnIdSelected}">
    <td><input type="text" name="grnNo" id="grnNo" value="{$grnNoSelected}"></td>
    <td>Info Sheet No.:
      <input type="text" name="infoSheetNo" value={$infoSheetNo} id="srNoClass" size="3" />
    </td>
    <td>Date: </td>
    <td NOWRAP>
      {html_select_date prefix="grnDate" time=$grnDateSelected start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
    </td>
    <td>P.O. / Letter no: </td><td> <input value="{$poNoSelected}" type="text" name="poNo" id="poNo"></td>
    <td> PO Date :</td>
    <td NOWRAP>
      {html_select_date time=$poDateSelected prefix="poDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
    </td>
  </tr>
  <tr>
  <tr>
    <td>Customer Name :</td>
    <td colspan="3">
      <select name="customerId" id="customerId" onchange="customerIdFunc();">
        <option value='0'>Select Name</option>
        {html_options values=$customerId output=$custName selected=$customerIdSelected}
      </select><br />
      <input type="text" name="custName" id="custName" size=55>
    </td>
    <td>Code :</td><td><input type="text" value="{$custCodeSelected}" name="custCode" id="custCode"></td>
  </tr>
  <tr>
    <td>Address :</td>
    <td><textarea rows="3" cols="25" name="address" id="address">{$addressSelected}</textarea>
    </td>
  </tr>
  <tr>
    <td>Contact Person :</td>
    <td colspan="3"><input type="text" name="contPerson" value="{$contPersonSelected}" id="contPerson" size=55></td>
    <td>Ph No :</td>
    <td><input type="text" name="phNo" value="{$phNoSelected}" id="phNo"></td>
  </tr>
  <tr>
    <td>Remarks :</td>
    <td colspan="14"><input type="text" name="remarks" value="{$remarksSelected}" id="remarks" size=155>
  </tr>
</table>
</div>

<center class="center"><h2>Following Materials Received For - Calibration/Verification</h2></center><br>
<table align="center" border="1"  cellpadding="1" cellspacing="0">

<tr>
<td colspan="13">
<table align="center" border="0" id="dataTable" width="100%" cellpadding="1" cellspacing="0">
 <tr>
    <th align="center" rowspan="2" width="100%" style="width :135px"> SR. NO. </th>
    <th align="center" rowspan="2" style="width :150px"> ITEM. Description </th>
    <th align="center" rowspan="2" style="width :135px"> ID. CODE. </th>
    <th align="center" rowspan="2" style="width :147px"> Paramter </th>
    <th align="center" rowspan="2"> Range </th>
    <th align="center" rowspan="2" style="width :150px"> Cali. Due Date Requested By Cust.</th>
    <th align="center" rowspan="2" style="width :125px">Expected Delivery Date</th>
    <th colspan="3" align="center" > Q U A N T I T Y</th>
    <th align="center" rowspan="2" style="width :120px"> Notes </th>
    <th align="center" rowspan="2" style="width :120px"> Kcallibration Report.No &nbsp;</th>
  </tr>
  <tr>
    <th align="center" style="width :130px"> Challan </th>
    <th align="center" style="width :130px"> Recd. </th>
    <th align="center" style="width :130px"> Condition </th>
  </tr>
  {section name="sec" loop=$grnDetail}
  <tr>
    <td align="center" NOWRAP>
      <input type="checkbox" name="dispatch[{$grnDetail[sec].grnDetailId}]" value="1" {$grnDetail[sec].grnDetailChecked} />
      <input type="text" name="grnDetailId[{$grnDetail[sec].grnDetailId}]" value="{$grnDetail[sec].grnDetailId}"/>
      <input type="hidden" name="grnDetailId[{$grnDetail[sec].grnDetailId}]" value="{$grnDetail[sec].grnDetailId}"/>
      <input type="text" name="srNo[]" value="{$smarty.section.sec.rownum}" class="srNoClass" size="3" DISABLED /></td>
    <td align="center">
      <select name="itemId[]" style="width: 148px;" DISABLED>
        {html_options values=$itemId output=$itemName selected=$grnDetail[sec].itemId}
      </select>
    </td>
    <td align="center" ><input type="text" name="itemCode[]" value="{$grnDetail[sec].itemCode}" DISABLED /></td>
    <td>
      <select name="parameterId[]" id="parameterId" DISABLED>
        {html_options values="$parameterId" output="$parameterName" selected=$grnDetail[sec].parameterId}
      </select><br />
    </td>
    <td>
      <input type="text" name="range[]" value="{$grnDetail[sec].range}" DISABLED />
    </td>
    <td class="table2" nowrap>
      <input type="text" name="custReqDate1[]" id="custReqDate1[]" value="{$grnDetail[sec].custReqDate}" DISABLED>
    </td>
    <td class="table2" nowrap>
      {html_select_date field_array="expDelivDate[]" time=$grnDetail[sec].expDelivDate prefix="" start_year="-3" end_year="+1" month_format="%m" 
       month_extra='disabled=disabled' day_extra='disabled=disabled' year_extra='disabled=disabled' field_order="DMY" day_value_format="%02d"}
    </td>
    <td align="center" ><input type="text" name="challang[]" size="5" value="{$grnDetail[sec].challan}" DISABLED /></td>
    <td align="center" ><input type="text" name="receivedg[]" size="5" value="{$grnDetail[sec].received}" class="recdClass" DISABLED /></td>
    <td>
      <select name="condition[]" style="width :135px" value=$grnDetail[sec].grnCondition onchange="document.infoSheet.submit();" DISABLED>
        {html_options values="$grnCondition" output="$grnCondition" selected=$grnDetail[sec].grnCondition}
      </select>
    </td>
    <td align="center" ><input type="text" name="description[]" value="{$grnDetail[sec].description}" DISABLED /></td>
    <td align="center" ><input type="text" name="reportNo[{$grnDetail[sec].grnDetailId}]" value="{$grnDetail[sec].reportNo}" /></td>
  </tr>
  {/section}
  <tr>
    <td align="right" colspan="2">Mode of Delivery : </td>
    <td align="center" ><input type="text" name="mode" value="{$grnDetailMode}" required="required" title="Delivery Mode" /></td>
  </tr>
</tr>
</table>
<table align="center">
  <tr>
    <td colspan="9">
      <input type="submit" value="SUBMIT" name="submitBtn" />
      <input type="submit" value="GRN Print" name="grnPrintBtn" />
      <input type="submit" value="INFO Print" name="infoPrintBtn" />
      <input type="submit" value="SEARCH" name="searchBtn" />
      <input type="submit" value="CANCEL" name="cancelBtn" />
    </td>
  </tr>
</table>
</form>
{include file="footer.tpl"}