{include file="./headStart.tpl"}
<title>Delivery Challan</title>
{include file="./headEnd.tpl"}
<script type="text/javascript" src="./js/jquery.colorbox.js"></script>
<script type="text/javascript" src="./js/Modal.js"></script>
<script type="text/javascript">
function deleteGetpass()
{
	var answer = confirm("Are you sure?")
  if (answer)
    return true;
  else
    return false;  
}
</script>
<center class="table1">Delivery Challan's</center><br>
<form name="deliveryChallan" action="{$smarty.server.PHP_SELF}" id='deliveryChallan' method='POST'>
  Customer :
  <select name="customerId" id="customerId">
    <option value='0'>Select Name</option>
    {html_options values=$customerId output=$custName selected=$selectedCustomerId}
  </select>
  <input type="submit" name="getChallanData" value=" Search " />
</form>
<table border="1" align="center" cellpadding="1" cellspacing="0">
  <tr>
    <td class="table1" align="center">Edit</td>
    <td class="table1" align="center">Delete</td>
    <td class="table1" align="center">GRN NO</td>
    <td class="table1" align="center">Date Of Dispatch</td>
    <td class="table1" align="center">Callibration Report No</td>
    <td class="table1" align="center">Callibration Date</td>
    <td class="table1" align="center">Customer ID</td>
    <td class="table1" align="center">Customer Name</td>
    <td class="table1" align="center">Deliverd By(Sign)</td>
    <td class="table1" align="center">Mode OF Delivery </td>
    <td class="table1" align="center">Certificate Received By</td>
    <td class="table1" align="center">Bill No</td>
    <td class="table1" align="center">Remarks</td>
    <td class="table1" align="center">Party</td>
  </tr>
  {section name="sec" loop=$gatePassRowArray|@count}
  <tr>
    <td class="table1"><a href ="deliveryChallanEdit.php?gatePassId={$gatePassRowArray[sec].gatePassId}">Edit</a></td>
    {if $gatePassRowArray[sec].userName == $username || $userType == "Admin"}
      <td class="table1"><a href ="deliveryChallanDelete.php?gatePassId={$gatePassRowArray[sec].gatePassId}" onclick="return deleteGetpass();">Delete</a></td>
    {else}
      <td class="table1"><a href ="index.php">Delete</a></td>
    {/if}
    <td class="table1">{$gatePassRowArray[sec].grnNo}</td>
    <td class="table1" NOWRAP>{$gatePassRowArray[sec].gatePassDate}</td>
    <td class="table1">
  	{section name="sec2" loop=$grnDetailArray[sec]}
  	  {$grnDetailArray[sec][sec2]}<br />
  	{/section}
  	</td>    	
    <td class="table1" NOWRAP>{$gatePassRowArray[sec].grnDate}</td>
    <td class="table1">{$gatePassRowArray[sec].customerId}</td>
    <td class="table1">{$gatePassRowArray[sec].custName}</td>
    <td class="table1">{$gatePassRowArray[sec].userName}</td>
    <td class="table1">{$gatePassRowArray[sec].deliveryMode}</td>
    <td class="table1">{$gatePassRowArray[sec].contPerson}</td>
    <td class="table1">&nbsp;</td>
    <td class="table1">{$gatePassRowArray[sec].remarks}</td>
    <td class="table1" align="center"><a href="deliveryChallanPdf.php?gatePassId={$gatePassRowArray[sec].gatePassId}">{$gatePassRowArray[sec].grnPrefix}</a></td>
  </tr>
  {/section}
</table>
</body>
{include file="footer.tpl"}