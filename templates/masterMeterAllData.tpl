<table border="2" width="100%" id="dataTable" align="center" left="10"cellpadding="0" cellspacing="0">
<tr>
  <td align="center" class="table1">&nbsp;</td>
  <td align="center" class="table1">Parameter</td>
  <td align="center" class="table1">Range Calc.</td>
  <td align="center" class="table1">Range Disp.</td>
  <td align="center" class="table1">Range Unit</td>
  <td align="center" class="table1">Frequency</td>
  <td align="center" class="table1">Accuracy % of Rdg</td>
  <td align="center" class="table1">Accuracy % of FS</td>
  <td align="center" class="table1">Accuracy</td>
  <td align="center" class="table1">Resolution</td>
  <td align="center" class="table1">Cmc</td>
  <td align="center" class="table1">Stability</td>
  <td align="center" class="table1">Uncertinity</td>
  <td align="center" class="table1">Degree Of Freedom</td>
  <td align="center" class="table1">&nbsp;</td>
</tr>
{section name=sec loop=$meterSubSubData}
<tr>
  <td align="center" class="table2">
    {if have_access_role($smarty.const.MASTER_METER_SUB_MODULE_ID,"edit")}
        <a href="masterMeterSub.php?masterMeterSubSubId={$meterSubSubData[sec].masterMeterSubSubId}" class="link">Edit</a>
    {/if}
  </td>
  <td align="center" class="table2">{$meterSubSubData[sec].parameterName}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].range}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].range_disp}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].range_unit}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].frequency}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].accuracySubSubRdg}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].accuracySubSubFS}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].accuracy_main}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].resolution}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].cmc}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].stability}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].uncertinty}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].degreeOfFreedom}</td>
  <td align="center" class="table2">
    {if have_access_role($smarty.const.MASTER_METER_SUB_MODULE_ID,"delete")}
        <a href="deleteMMSubSubDelete.php?masterMeterSubSubId={$meterSubSubData[sec].masterMeterSubSubId}" class="link">Delete</a>
    {/if}
  </td>
</tr>
{/section}
</table>