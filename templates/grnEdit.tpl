{include file="./headStart.tpl"}
<title>GRN Edit</title>
<link type="text/css" href="./css/maincombobox.css" />
<script type="text/javascript">
$(document).ready(function(){
  $('#grnPrefix').focus();
  customerIdFunc();
  $('#custName').attr("disabled", true);
  $('#custCode').attr("disabled", true);
  $('#address').attr("disabled", true);
  $(document).keydown(function(e) {
  	var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    if(code == 13){
      window.event.keyCode = 9;
    }
  });
  $('#grnPrefix').change(function(){
    var grnPrefix = $("#grnPrefix").val();
    var datastring = 'grnPrefix='+ grnPrefix;
    $.ajax({
      url: "grnEntryJq.php",
      data: datastring,
      success: function(data){
        $("#grnNo").val(data);
      }
    });
  });

  $("#customerId").change(function ()
  {
    if($("#customerId").val() > 0)
    {
      $('#custName').val("");
      $('#custName').attr("disabled", true);
      $('#custCode').attr("disabled", true);
      $('#address').attr("disabled", true);
    }
    else
    {
      $('#custName').removeAttr("disabled");
      $('#custCode').removeAttr("disabled");
      $('#address').removeAttr("disabled");
    }
  });
  
  $(".add").click(function(){
    $("#one").clone(true).appendTo("#mainDiv")
    .find('input[type="text"]').val('').end()
    .find('select.parameterId').val(0).end()
    .find('select.itemId').val(0).end()
    .find('.grnDetailId').val(0).end()
    .find('.custReqDate1').val('1 Year');
  });
  
  $("#grnNo").blur(function() {
		var gr_no = $("#grnNo").val();
                var grnPrefix = $("#grnPrefix").val();
{*		var item_type_id = $("#item_type_id").val();*}
		var item_type_id = '';
		var grnId = $("#grnId").val();
		$("#srNoClass").val(gr_no);
		$.ajax({ 
			type: "POST", 
			url: "check.php", 
			data: "gr_no="+ gr_no+"&grnPrefix="+ grnPrefix+"&item_type_id="+ item_type_id+"&grnId="+ grnId, 
			success: function(msg){ 
    				if(msg == "Duplicate No Found")
					{
						$("#grnNo").val("");
						$("#grnNo").focus();
						$("#status").html(msg);
						msg = "";
					}
					else
					{
						$("#status").html(msg);
					}
			}
		});
		});
  
});

//  function addRow(tableID)
//  {
//    var table = document.getElementById(tableID);
//
//    var rowCount = table.rows.length;
//    var row = table.insertRow(rowCount);
//
//    var colCount = table.rows[2].cells.length;
//
//    for(var i=0; i<colCount; i++)
//    {
//      var newcell = row.insertCell(i);
//      newcell.innerHTML = table.rows[2].cells[i].innerHTML;
//      //alert(newcell.childNodes);
//      switch(newcell.childNodes[0].type)
//      {
//        case "text":
//          if(i==0)
//            newcell.childNodes[0].value = rowCount-1;
//          else
//          {
//            if(newcell.childNodes[0].className == "recdClass")
//              newcell.childNodes[0].value = 1;
//            else
//              newcell.childNodes[0].value = "";
//          }
//          break;
//      }
//    }
//  }
//
//  function deleteRow() {
//     var countSr = 1;
//     $('.delete').click(function(){
//        $(this).parent().parent().remove();
//        $('.srNoClass').each(function(){
//          this.value = countSr++;
//        });
//        return false;
//     });
//  }

function customerIdFunc()
{
  var customerId = $("#customerId").val();
  var datastring = 'customerId=' + customerId;
  $.ajax({
    url: "customerCodeJq.php",
    data: datastring,
    dataType:'json',
    success: function(data){
      $("#custCode").val(data[0]);
      $("#address").val(data[1]);
      $("#custName").val(data[2]);
    }
  });
}

function deletechecked()
{
	
}

function grnDeleteRow(rowObj)
{
	var answer = confirm("Are You Sure Delete Your Recored ?")
	if (answer == true)
	{
    $(rowObj).parents("#one").remove();  
		var row = $(rowObj).parents('.tableRow');
		grnDetailIdRow = row.find(".grnDetailIdRow").val();
	  var datastring = 'grnDetailId=' + grnDetailIdRow;
	  $.ajax({
	    url: "grnDeleteRow.php",
	    data: datastring,
	    success: function(data){
	    	//alert('Sucssess');
	    }
	  });
	}
	return false;
}

</script>
<script>
  $('#parameterId').val(function() {
    return val + (!val ? '' : ', ') + 'parameterId';
      alert("val")
  });
</script>
{include file="./headEnd.tpl"}
<form name="form1" id="form1" action="{$smarty.server.PHP_SELF}" method="post">
    <input type="hidden" name="grnId" id="grnId" value="{$grnIdSelected}" />
<div>
<span class="center"><h2>GRN Edit</h2></span><br>
<input type="hidden" name="paramhidden" id="paramhidden">
<table border="0" align="center">
  <tr>
    <td>GRN No:
      <select name="grnPrefix" id="grnPrefix" >
        {html_options values=$grnPrefix output=$grnPrefix selected=$grnPrefixSelected}
      </select>
      {*<select name="item_type_id" id="item_type_id" style="width :100px">
        {html_options values=$item_type_id_array output=$item_type_name_array selected=$item_type_id_selected}
      </select>*}
    </td>
    <td><input type="text" name="grnNo" id="grnNo" value="{$grnNoSelected}" required="required"><div id="status" style="color: red; font-size: 20px;"></div></td>
    <td>Info Sheet No.:
      <input type="text" name="infoSheetNo" value={$infoSheetNo} id="srNoClass" size="3" required="required" />
    </td>
    <td>Date: </td>
    <td NOWRAP>
      {html_select_date time=$grnDateSelected prefix="grnDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
    </td>
    <td>P.O. / Letter no: </td><td> <input value="{$poNoSelected}" type="text" name="poNo" id="poNo" required="required"></td>
    <td> PO Date :</td>
     <td NOWRAP>
         {if empty($poDateSelected)}
            {html_select_date prefix="poDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d" year_empty="Year" month_empty="Month" day_empty="Day" time="--"}
       {else}
            {html_select_date time=$poDateSelected prefix="poDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
     {/if}
    </td>
  </tr>
  <tr>
  <tr>
    <td>Customer Name :</td>
    <td colspan="3">
      <select name="customerId" id="customerId" onchange="customerIdFunc();">
        <option value='0'>Select Name</option>
        {html_options values=$customerId output=$custName selected=$customerIdSelected}
      </select><br />
      <input type="text" name="custName" id="custName" size=55>
    </td>
    <td>Code :</td><td><input type="text" value="{$custCodeSelected}" name="custCode" id="custCode"></td>
  </tr>
  <tr>
    <td>Address :</td>
    <td><textarea rows="3" cols="25" name="address" id="address">{$addressSelected}</textarea>
    </td>
  </tr>
  <tr>
    <td>Contact Person :</td>
    <td colspan="3"><input type="text" name="contPerson" value="{$contPersonSelected}" id="contPerson" size=55 required="required"></td>
    <td>Ph No :</td>
    <td><input type="text" name="phNo" value="{$phNoSelected}" id="phNo" required="required"></td>
  </tr>
  <tr>
    <td>Remarks :</td>
    <td colspan="14"><input type="text" name="remarks" value="{$remarksSelected}" id="remarks" size=155 required="required">
  </tr>
  <tr>
    <td>Send SMS :</td>
    <td colspan="14"><input style="width:20px; height: 20px;" type="checkbox" name="send_sms" value=""></td>
  </tr>
</table>
</div>

<center class="center"><h2>Following Materials Received For - Calibration/Verification</h2></center><br>
<table align="center" border="1"  cellpadding="1" cellspacing="0">

<tr>
<td colspan="13">
<table align="center" border="0" id="dataTable" width="100%" cellpadding="1" cellspacing="0">
<tr>
  <th align="center" rowspan="2" width="100%" style="width :135px"> SR. NO. </th>
  <th align="center" rowspan="2" style="width :150px"> ITEM. Description </th>
  <th align="center" rowspan="2" style="width :135px"> ID. CODE. </th>
  <th align="center" rowspan="2" style="width :147px"> Paramter </th>
  <th align="center" rowspan="2"> UUC Range and Unit </th>
  <th align="center" rowspan="2" style="width :150px"> Cali. Due Date Requested By Cust.</th>
  <th align="center" rowspan="2" style="width :125px">Expected Delivery Date</th>
  <th colspan="3" align="center" > Q U A N T I T Y</th>
  <th align="center" rowspan="2" style="width :120px"> Notes </th>
  <th align="center"  style="width :70px"> UUC Location </th>
  <th align="center" rowspan="2" style="width :30px">Add Row</th>
  <th align="center" rowspan="2" style="width :30px">Delete Row</th>
</tr>
<tr>
  <th align="center" style="width :130px"> Challan </th>
  <th align="center" style="width :130px"> Recd. </th>
  <th align="center" style="width :130px"> Condition </th>
</tr>
<tbody id="mainDiv">
{section name="sec" loop=$grnDetail}
<tr id="one" class="tableRow">
  <input type="hidden" name="grnDetailId[]" class="grnDetailId" value="{$grnDetail[sec].grnDetailId}" />
  <input type="hidden" name="refGrnDetailId[]" value="{$grnDetail[sec].refGrnDetailId}" />
  <td align="center"><input type="text" name="srNo[]" value="{$smarty.section.sec.rownum}" class="srNoClass" size="3" readonly="" /></td>
  <td align="center">
      <select name="itemId[]" class="itemId" style="width: 148px;" required="">
      <option value="">-- Select --</option>
      {html_options values=$itemId output=$itemName selected=$grnDetail[sec].itemId}
    </select>
  </td>
  <td align="center" ><input type="text" name="itemCode[]" value="{$grnDetail[sec].itemCode}" /></td>
  <td>
      <select name="parameterId[]" class="parameterId" id="parameterId" required="">
      {html_options values="$parameterId" output="$parameterName" selected=$grnDetail[sec].parameterId}
    </select><br />
  </td>
  <td>
      <input type="text" name="range[]" value="{$grnDetail[sec].range}" required=""/>
  </td>
  <td class="table2" nowrap>
    <input type="text" name="custReqDate1[]" id="custReqDate1[]" class="custReqDate1" value="{$grnDetail[sec].custReqDate}" >
  </td>
  <td class="table2" nowrap>
    {html_select_date field_array="expDelivDate[]" time=$grnDetail[sec].expDelivDate prefix="" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
  </td>
  <td align="center" ><input type="text" name="challang[]" size="5" value="{$grnDetail[sec].challan}" required="" /></td>
  <td align="center" ><input type="text" name="receivedg[]" size="5" value="{$grnDetail[sec].received}" class="recdClass" /></td>
  <td>
    <select name="condition[]" style="width :135px" value=$grnDetail[sec].grnCondition onchange="document.infoSheet.submit();">
      {html_options values="$grnCondition" output="$grnCondition" selected=$grnDetail[sec].grnCondition}
    </select>
  </td>
  <td align="center" ><input type="text" name="description[]" value="{$grnDetail[sec].description}" /></td>
  <td align="center" ><input type="text" name="uuc_location[]" value="{$grnDetail[sec].uuc_location}" /></td>
  <td><input type="button" name="add" class="add" value="Add" /></td>
  <td>
  	<input type="hidden" class="grnDetailIdRow" value="{$grnDetail[sec].grnDetailId}" />
  	<a href="#" class="delete deleteRow" onclick="grnDeleteRow(this);" />Delete</a>
  </td>
  </tr>
{/section}
</tbody>
</table>
<table align="center">
  <tr>
    <td colspan="9">
      <input type="submit" value="SUBMIT" name="submitBtn" />
      <input type="submit" value="GRN Print" name="grnPrintBtn" />
      <input type="submit" value="INFO Print" name="infoPrintBtn" />
      <input type="submit" value="SEARCH" name="searchBtn" />
      <input type="submit" value="CANCEL" name="cancelBtn" />
    </td>
  </tr>
</table>
</form>
{include file="footer.tpl"}