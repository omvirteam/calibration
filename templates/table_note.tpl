{include file="./headStart.tpl"}
<script type="text/javascript">
$(document).ready(function() {
  $('#table_info_id').focus();
  $("#table_info_id").change(function(){
    var datastring = "get_table_info_id=" + $('#table_info_id').val();
    $.ajax({
      url: "./table_note.php",
      data: datastring,
      success: function(data){
        console.log(data);
        var json = $.parseJSON(data);
        $('#note').val(json);

      }
    });
  });
});
</script>
{include file="./headEnd.tpl"}
<form name="noteData" id="noteData" method="post">

<table border='0' cellpadding='1' cellspacing='2' align='center' width="650px">
<tr width="650px">
  <td class="table1" width="650px" align="center">Table Info Note for print</td>
</tr>
<tr width="650px">
  <td width="650px">
      <select name="table_info_id" id="table_info_id" style="width: 650px;" required="">
      <option value="">Select</option>
      {html_options values=$table_info_id output=$table_name}
    </select>
  </td>
</tr>

<tr>
  <td>
    <br/>
  </td>
</tr>
<tr>
  <td>
    Note :
  </td>
</tr>
<tr>
  <td>
    <textarea name="note" id="note" style="width: 650px; height: 200px;"></textarea>
  </td>
</tr>
<tr>
  <td>
    <input type="submit" name="insertBtn" value="Submit" class="button" />
  </td>
</tr>
</table>
<br/>
</form>