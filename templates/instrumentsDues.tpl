{include file="./headStart.tpl"}
<title>Instruments Dues</title>
{include file="./headEnd.tpl"}
<form action="{$smarty.server.PHP_SELF}" name='grnList' method='POST'>
	
<center class="center"><h2>Instruments Dues...!</h2></center><br>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
	<tr>
		<td colspan="6" align="center">
			Select From : {html_select_date prefix="fromDate" start_year="-5" end_year="+2" field_order="DMY" time=$fromDate day_value_format="%02d"} 
			To : {html_select_date prefix="toDate" start_year="-2" end_year="+1" field_order="DMY" time=$toDate day_value_format="%02d"} 
			<input type="submit" name="submitDateBtn" value="GO..." class="button"/>
		</td>
	</tr>
	<tr>
		<td colspan="6">
			<hr />
		</td>
	</tr>
	<tr>
		<th class="table1" align="left">#</th>
		<th class="table1" align='left'>GRN</th>
		<th class="table1" align='left'>Customer</th>
		<th class="table1" align='left'>Instrument</th>
		<th class="table1" align='center'>Expire On</th>
		<th class="table1" align='center'>Remaing Days</th>
	</tr>
	{section name="sec" loop=$grnList}
	{if $grnList[sec].remainingDays lt 0}
	<tr class="expire">
	{elseif $grnList[sec].remainingDays gt -1 AND $grnList[sec].remainingDays lt 15}
	<tr class="nearbydue">
	{else}
	<tr>
	{/if}
  		<td class="table2" align="left">{$smarty.section.sec.index+1}</td>
		<td class="table2" align="left">{$grnList[sec].grnPrefix}-{$grnList[sec].grnNo}</td>
		<td class="table2" align="left">{$grnList[sec].custName}</td>
		<td class="table2" align="left">{$grnList[sec].itemName} => {$grnList[sec].parameterName} => {$grnList[sec].itemCode}</td>
		<td class="table2" align="center">{$grnList[sec].expDate}</td>
		<td class="table2" align="center">{$grnList[sec].remainingDays}</td>
	</tr>
	{sectionelse}
<tr><td align="center" colspan="22"> <h1><font color="red"><b>No Record Found...!</b></font></h1></td></tr>
{/section}
</table>
</form>
{include file="footer.tpl"}