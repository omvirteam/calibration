{include file="./headStartPrint.tpl"}
{include file="./headEndPrint.tpl"}
<form name="PrintForm" id="printForm" method="POST" action="{$smarty.server.PHP_SELF}">
<table border="1" cellSpacing="0" cellpadding="2">
  <tr>
    <th align="right" colspan="2">
        <a href="grnList.php"><img src="./images/logo.jpg" border="0" width="250" height="115" /></a>
    </th>
  </tr>
  <tr>
    <th align="left">
      <font size="5"><u>GOODS RECEIPT NOTE</u></font><br />
    </th>
    <th align="right" NOWRAP>
      <font size="5" >ST F 01</font><br />
    </th>
  </tr>
  <tr>
    <td align="right" colspan="2">
      <table border="1" cellSpacing="0" cellpadding="2">
      <tr>
        <td align="left"> GRN No. :</td>
        <td class="table2" align="left">{$grnPrefix}&minus;{$grnNo} </td>
        <td align="right">Date :</td>
        <td class="table2" align="left">{$grnDate}</td>
        <td>&nbsp;&nbsp;&nbsp; P.O /Letter No :</td>
        <td class="table2" align="left">{$poNo}</td>
        <td>&nbsp;&nbsp;&nbsp; Date :</td>
        <td class="table2" align="left">{$poDate}</td>
      </tr>
      <tr>
        <td colspan="1" align="left"> Customer Name:</td>
        <td class="table2" align="left" colspan="3">{$custName}</td>
        <td>CODE :</td>
        <td class="table2" align="left">{$custCode}</td>
      </tr>
       <tr>
        <td> Contact Person :</td>
        <td class="table2" align="left" colspan="3">{$mrAndMrs}.  {$contPerson}</td>
        <td colspan="2"align="right"> PH. No. :</td>
        <td class="table2" align="left">{$phNo}</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td align="center" colspan="2">
    <table border="1" cellSpacing="0" cellpadding="2">
      <tr><th colspan="8">FOLLOWING MATERIALS RECEIVED FOR - CALIBRATION /VERIFICATION</th> </tr>
      <tr>
      <th rowspan="2">SR NO.</th>
      <th rowspan="2">ITEM ID Code</th>
      <th rowspan="2">DESCRIPTION</th>
      <th colspan="3" align="center">Q U A N T I T Y</th>
      </tr>
      <tr>
      <th>CHALLAN</th>
      <th>RECEIVED</th>
      <th>CONDITION</th>
      </tr>
      {section name="sec" loop=$grnDetail}
       <tr>
         <td class="table2" align="left">{$smarty.section.sec.rownum}</td>
         <td class="table2" align="left">{$grnDetail[sec].itemCode}</td>
         <td class="table2" align="left">{$grnDetail[sec].itemName}</td>
         <td class="table2" align="center">{$grnDetail[sec].challan}</td>
         <td class="table2" align="center">{$grnDetail[sec].received}</td>
         <td class="table2" align="center">{$grnDetail[sec].grnCondition}</td>
         <tr class="table2" align="left">
       {sectionelse}
       {/section}
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="2"> Remarks : {$remarks}</td>
  </tr><tr>
    <td> Received By (sign.&amp;date) :{$userName}</td>
    <td> Store HOD : (SIGN.&amp;DATE) :</td>
  </tr>
  <tr><td colspan="2"> For delivery of materials this goods Receipt required,otherwise material will not be returned.</td></tr>
</table>

</form>
{include file="./footerPrint.tpl"}