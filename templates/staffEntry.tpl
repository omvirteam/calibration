{include file="./headStart.tpl"}
<title>Staff</title>
<script type="text/javascript">
$(document).ready(function() {
  $('#staffName').focus();
  $('input:visible:enabled').bind('keypress', function(e) {
	    if(e.keyCode==13){
	      $(this).next().focus();
	    }
	  });
  });
function deletechecked()
{
	var answer = confirm("Delete Selected Recored ?")
	if (answer)
	{
	  document.messages.submit();
	}
	return false;
}
</script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="./js/jquery.validate.js"></script>
<script type="text/javascript">
{literal}
  $(document).ready(function() {
    $("#form1").validate({
      rules: {
        staffName :"required",
        userName  :"required",
        password: { 
          required: true, 
          minlength: 5
        }, 
        c_password: { 
          required: true, 
          equalTo : "#password", 
          minlength: 5
        }
      }
    });
  });
{/literal}
</script>
{include file="./headEnd.tpl"}
<form name="form1" method="post" action="{$smarty.server.PHP_SELF}" id="form1">
<input type="hidden" name="staffId" value="{$staffId}" />
{if (have_access_role($smarty.const.STAFF_MODULE_ID,"add") && {!isset($smarty.get.staffId)}) || (have_access_role($smarty.const.STAFF_MODULE_ID,"edit") && {isset($smarty.get.staffId)})}
<table border="1" align="center" cellpadding="1" cellspacing="1">
<tr>
  <td align="center" class="table1" colspan="2">Staff Entry</td>
</tr>
<tr>
  <td align="right">Member Name</td>
  <td width="400px"><input type="text" name="staffName" id="staffName" value="{$currentStaffName}"></td>
</tr><tr>
  <td align="right">User Name</td>
  <td><input type="text" name="userName" value="{$userName}"></td>
</tr>
<tr>
   <td align="right">password</td>
   <td><input type="password" name="password" id="password" value="{$password}"></td>
</tr>
<tr>
  <td align="right">Re-Type New password : </td>
  <td align="left"><input type="password" name="c_password" id="c_password"/></td>
</tr>
<tr>
  <td align="right">User Type</td>
  <td align="left">
    <select name="userType" id="userType">
      <option value="Admin">Admin</option>
      <option value="User">User</option>
    </select>
  </td>
</tr>
<tr>
   <td align="right">Address</td>
   <td><input type="text" name="address" value="{$address}"></td>
</tr>
<tr>
   <td align="right">City</td>
   <td><input type="text" name="city" value="{$city}"></td>
</tr>
<tr>
  <td align="right">State</td>
  <td><input type="text" name="state" value="{$state}"></td>
</tr>
<tr>
  <td align="right">Phone1</td>
  <td><input type="t ext" name="phone1" value="{$phone1}"></td>
</tr>
<tr>
  <td align="right">Phone2</td>
  <td><input type="text" name="phone2" value="{$phone2}"></td>
</tr>
<tr>
  <td align="right">Email</td>
  <td><input type="text" name="email" value="{$email}"></td>
</tr>
<tr>
  <td align="right">Web</td>
  <td><input type="text" name="web" value="{$web}"></td>
</tr>
<tr>
  <td align="right">Cont Person</td>
  <td><input type="text" name="contPerson" value="{$contPerson}"></td>
<tr>
  <td align="center" colspan="2">
    <input type="submit" name="insertBtn" value="Submit" class="button" /> 
    <input type="submit" value="CANCEL" name="cancelBtn" class="button cancel"  />
  </td>
</tr>
</table>
{/if}
</form>
{if have_access_role($smarty.const.STAFF_MODULE_ID,"view")}
<table align='center' border='0' cellpadding='1' cellspacing='2'>
<center class="center"><h2>Staff Available...!</h2></center><br>
<tr>
  <td  class="table1" align='center'>&nbsp;</td>
  <td  class="table1" align='center'>&nbsp;</td>
  <td  class="table1" align='center'>Staff Name</td>
  <td  class="table1" align='center'>Username</td>
  <td  class="table1" align='center'>Usertype</td>
  <td  class="table1" align='center'>Address</td>
  <td  class="table1" align='center'>City</td>
  <td  class="table1" align='center'>State</td>
  <td  class="table1" align='center'>phone1</td>
  <td  class="table1" align='center'>phone2</td>
  <td  class="table1" align='center'>email</td>
  <td  class="table1" align='center'>web</td>
  <td  class="table1" align='center'>ContPerson</td>
</tr>

{section name="sec" loop=$staffEntry}
<tr>
  <td class="table2" align="center">
      {if have_access_role($smarty.const.STAFF_MODULE_ID,"edit")}
	  <a href="staffEntry.php?staffId={$staffEntry[sec].staffId}" class="link">Edit</a>
        {/if}
	</td>                                 
	<td class="table2" align="center">
        {if have_access_role($smarty.const.STAFF_MODULE_ID,"delete")}
            <a href="staffDelete.php?staffId={$staffEntry[sec].staffId}"  onclick="return deletechecked();" id="delete" class="link">Delete</a>
        {/if}
	</td>
  <td class="table2" align="center">{$staffEntry[sec].staffName}</td>
  <td class="table2" align="center">{$staffEntry[sec].userName}</td>
  <td class="table2" align="center">{$staffEntry[sec].userType}</td>
  <td class="table2" align="center">{$staffEntry[sec].address}</td>
  <td class="table2" align="center">{$staffEntry[sec].city}</td>
  <td class="table2" align="center">{$staffEntry[sec].state}</td>
  <td class="table2" align="center">{$staffEntry[sec].phone1}</td>
  <td class="table2" align="center">{$staffEntry[sec].phone2}</td>
  <td class="table2" align="cenetr">{$staffEntry[sec].email}</td>
  <td class="table2" align="center">{$staffEntry[sec].web}</td>
  <td class="table2" align="center">{$staffEntry[sec].contPerson}</td>
  </tr>
{sectionelse}
<tr>
  <td class="table2" align="center" colspan="3">No Records Found.....!!!!!!!!</td>
</tr>
{/section}
</table>
{/if}
{include file="footer.tpl"}