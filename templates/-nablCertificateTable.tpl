<span style="float:right;margin:125px 0 0 0;"><b>QC F 05</b></span>
<div style="display:block;border:1px solid black;float:left;margin:0 0 0 55px;padding:5px;">
	<table border=0 cellpadding=0 cellspacing=3 width="100%" style="font:12px arial,sans-serif;">
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left">CERTIFICATE NO.</td>
			<td align="left">:</td>
			<td align="left">{$certificateNo}</td>
			<td align="left">&nbsp;</td>
			<td align="left">DATE OF CALLIBRATION</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.callibrationDate}</td>
			<td align="left" colspan="2">&nbsp;</td>
		</tr> 
		<tr>
      <td align="left" colspan="2">&nbsp;</td>
      <td align="left">NO OF SHEETS.</td>
			<td align="left">:</td>
			<td align="left">1 Of {$totalPages}</td>
			<td align="left">&nbsp;&nbsp;&nbsp;</td>
			<td align="left">Next Recommended Calibration date</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.nextYearDate}</td>
		  <td align="left" colspan="2">&nbsp;</td>
    </tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left">GRN No.</td>
			<td align="left">:</td>
			<td align="left">{$grnDetailArr.grnNo}</td>
			<td align="left" colspan="6">&nbsp;</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>1.CUSTOMER NAME & ADDRESS</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4" style="font-family: Agency FB;font-size:18px;"><b>M/s. {$grnDetailArr.custName}</b></td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="6">&nbsp;</td>
			<td align="left" colspan="5" style="font-size:x-small;">
			  {$grnDetailArr.address}
			  <br />
			  <b style="font-weight:bold;font-size:12px;">{$grnDetailArr.city}</b></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>2.CUSTOMERS REFERANCE NO.</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.custRefName}/cali/{$grnDetailArr.custCode}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>3.INSTRUMENT RECEIVED ON</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.grnDate}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="4"><b>4.DESCRIPTION OF INSTRUMENT</b></td>
			<td align="left" colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Name</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.itemName}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Make/Model.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.makeModel}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Sr No.</td>
      <td align="left">:</td>
      <td align="left">{$grnDetailArr.itemCode}</td>
			<td align="left" colspan="4">{if $grnDetailArr.extraFields == 1}{$grnDetailArr.extraField1}{else}&nbsp;{/if}</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Instrument.ID.No.</td>
      <td align="left">:</td>
      <td align="left">{$grnDetailArr.instrumentId}</td>
			<td align="left" colspan="4">{if $grnDetailArr.extraFields == 1}{$grnDetailArr.extraField2}{else}&nbsp;{/if}</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Range.</td>
      <td align="left">:</td>
      <td align="left">{$grnDetailArr.range2}</td>
			<td align="left" colspan="4">{if $grnDetailArr.extraFields == 1}{$grnDetailArr.extraField3}{else}&nbsp;{/if}</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Least Count.</td>
      <td align="left">:</td>
      <td align="left">{$grnDetailArr.leastCount}</td>
			<td align="left" colspan="4">{if $grnDetailArr.extraFields == 1}{$grnDetailArr.extraField4}{else}&nbsp;{/if}</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Accuracy.</td>
      <td align="left">:</td>
      <td align="left">{$grnDetailArr.accuracy}</td>
			<td align="left" colspan="4">{if $grnDetailArr.extraFields == 1}{$grnDetailArr.extraField5}{else}&nbsp;{/if}</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Condition on receipt.</td>
      <td align="left">:</td>
      <td align="left">Good</td>
			<td align="left" colspan="4">{if $grnDetailArr.extraFields == 1}{$grnDetailArr.extraField6}{else}&nbsp;{/if}</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left" colspan="8">
			  <table width="98%" border=0 cellpadding=1 cellspacing=0 style="font:12px arial,sans-serif;">
			  	<tr>
			  		<td align="left"><b>5.AMBIENT TEMPERATURE</b></td>
			  		<td align="left">:</td>
			  		<td align="left">&nbsp;{$grnDetailArr.temperature}</td>
						<td align="left">&nbsp;</td>
			      <td align="left"><b>RELATIVE HUMIDITY</b></td>
			      <td align="left">:</td>
			      <td align="left">{$grnDetailArr.humidity}</td>
			  </table>
			</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="9"><b>6.DETAILS OF REFERANCE STANDARD & MAJOR INSTRUMENT USED.</b></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left" colspan="8">
			  <table width="98%" border=1 cellpadding=2 cellspacing=0 style="font-size:11px">
			    <tr>
			      <td align="center" style="white-space: nowrap"><b>Sr. No.</b></td>
			      <td align="center" style="white-space: nowrap"><b>Name & Make</b></td>
				  <!-- additional change by girish - start -->
			      <td align="center" style="white-space: nowrap"><b>Serial No.</b></td>
				  <!-- additional change by girish - end -->
			      <td align="center" style="white-space: nowrap"><b>Certi. No.</b></td>
			      <td align="center" style="white-space: nowrap"><b>I.D. No.</b></td>
			      <td align="center" style="white-space: nowrap"><b>Callibration Due Date</b></td>
			      <td align="center" style="white-space: nowrap"><b>Tracebility</b></td>
			    </tr>
	      {section name=masterMeterSec loop=$masterMeterData}
			    <tr>
			      <td align="center" width="20px">{$smarty.section.masterMeterSec.rownum}</td>
			      <td align="center" width="40px" style="white-space: nowrap">{$masterMeterData[masterMeterSec].masterMeterName} ({$masterMeterData[masterMeterSec].masterMeterModelNo})</td>
				  <!-- additional change by girish - start -->
			      <td align="center" width="20px" style="white-space: nowrap">{$masterMeterData[masterMeterSec].masterMeterSerialNo}</td>
				  <!-- additional change by girish - end -->
			      <td align="center" width="20px" style="white-space: nowrap">{$masterMeterData[masterMeterSec].masterMeterCertificateNo}</td>
			      <td align="center" width="10px" style="white-space: nowrap">{$masterMeterData[masterMeterSec].masterMeterIdNo}</td>
			      <td align="center" width="20px" style="white-space: nowrap">{$masterMeterData[masterMeterSec].masterMeterExp}</td>
			      <td align="center" width="20px" style="white-space: nowrap">{$masterMeterData[masterMeterSec].masterMeterTraceabilityTo}</td>
			    </tr>
			  {/section}
			  </table> 
			</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left" colspan="9">
				<table width="98%" border=0 cellpadding=2 cellspacing=0 style="font-size:11px">
					<tr>
						<td align="left" width="4%"><b>Note: </b></td>
						<td align="left" width="">{$masterMeterData[0].masterUncertaintyText}</td>
					</tr>
				</table>
			</td>
		</tr>
<!--			
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Name</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterName}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;I.D No</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterIdNo}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Make</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterMake}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Model No.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterModelNo}</td>
			<td align="left">&nbsp;</td>
		</tr>
 		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Serial No.</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterSerialNo}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Certificate No</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterCertificateNo}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Calibration Valid Up To</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterExp}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Traceability To</td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.masterMeterTraceabilityTo}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="8" style="font-size:x-small;">
        &nbsp;&nbsp;&nbsp;Measurement uncertainty  {$grnDetailArr.masterUncertaintyText}
      </td>
			<td align="left">&nbsp;</td>
		</tr>
-->	
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>7.PROCEDURE</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4">{$grnDetailArr.procedureText}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="8"><b>Remarks</b> : {$grnDetailArr.certiRemarks}</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
		<td colspan="2">&nbsp;</td>
      <td align="left" colspan="10" style="font-family: 'Exotc350 DmBd BT';font-size:20px; "><b>For Krishna Instruments</b></td>
		</tr>
		<tr><td align="left" colspan="11">&nbsp;</td></tr>
 		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="4" style="font-size:x-small;">CALIBRATED BY : (Bhavesh Tank, CAL. ENG.)</td>
      
      <td align="center" colspan="3">APPROVED BY : (D.R.SHAH, C.E.O.)</td>
			<td align="left" colspan="2">SEAL:</td>
		</tr>
 		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="8" style="font-size:x-small;">
      	1.This report pertains To Partuculars sample/Instrumenst submitted for test.<br>
      	2.This certificate may not be reproduced except in full, without prior written permission of Krishna Instruments.<br>
      	3.This Calibraiton Result Reported in the Certificate are valid at the time of measurments and under stated Conditions.
      </td>
			<td align="left">&nbsp;</td>
		</tr>
  </table>
</div>
<div style="clear:both;">
</div>
<br/><br/>
<style>
@media print
{
.pagebreak { page-break-after:always; }
}
</style>
<div class="pagebreak"></div>
{include file='nablCertificateNewPageTop.tpl'}

		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="9"><b>8.OBSERVATION</b></td>
		</tr>
		<tr>
		  <td colspan="11">
		  	{counter assign=currentpage start=2 print=false}
				{section name=sec loop=$masterArr}
				  {if $smarty.section.sec.rownum % $observationTableLimit == 0}
				    {include file='nablCertificateNewPageInclude.tpl'}
				  {/if}
				  {if $masterArr[sec].tableNo == 1}
				    {if $masterArr[sec].dataPresent == 1}
				      <table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				      <tr>
				      	<div style="margin:10px;"><b>{$masterArr[sec].parameterName}</b></div>
			        </tr>
				      <tr>
				        <td align="left" colspan="2" rowspan="2">SR No</td>
	              <td align="center" colspan="6">Callibration Standard</td>
                    {if $masterArr[sec].displayCalculated == 1}
                      <td rowspan="2" align="center">Calculated<br /> Reading</td>
                    {/if}
	              <td align="center" colspan="6">Unit Under Test</td>
	              <td align="center" colspan="6">Error</td>
	              <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	            </tr>
	            <tr>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Reading</td>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Reading</td>
	              <td align="center" colspan="3">Units</td>
	              <td align="center" colspan="3">% of Rdg.</td>
	            </tr>
	            {section name=sec1 loop=$detailArr[sec]}
	              <tr>
	                <td align="center" colspan="2">{$smarty.section.sec1.rownum}</td>
	                <td align="center" colspan="3">{$masterArr[sec].range}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].testMeterAverage|number_format:$detailArr[sec][sec1].stdMeter1AfterDecimal:".":""}</td>
                      {if $masterArr[sec].displayCalculated == 1}
                        <td align="center">{$detailArr[sec][sec1].stdMeterAverageAfterMf|number_format:$detailArr[sec][sec1].stdMeter1AfterDecimal:".":""}</td>
                      {/if}
	                <td align="center" colspan="3">{$masterArr[sec].range2}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].stdMeterAverage|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].errorUnits|number_format:$detailArr[sec][sec1].stdMeter1AfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].errorRdg|number_format:$detailArr[sec][sec1].stdMeter1AfterDecimal:".":""}</td>
	                <td align="center" colspan="2">{$detailArr[sec][sec1].expandedUncertinityInPre}</td>
	              </tr>
	            {/section}
	            </table>
	          {/if}
	        {/if}
				  {if $masterArr[sec].tableNo == 2}
				    {if $masterArr[sec].dataPresent == 1}
				      <table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				      <tr>
				      	<div style="margin:10px;"><b>{$masterArr[sec].parameterName}</b></div>
			        </tr>
				      <tr>
				        <td align="left" colspan="2" rowspan="2">SR No</td>
	              <td align="center" colspan="6">Callibration Standard</td>
	              <td align="center" rowspan="2">Calculated <br />Reading in <br />Volt</td>
	              <td align="center" colspan="6">Unit Under Test</td>
	              <td align="center" colspan="6">Error</td>
	              <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	            </tr>
	            <tr>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Applied <br />Voltage</td>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Reading</td>
	              <td align="center" colspan="3">Units</td>
	              <td align="center" colspan="3">% of Rdg.</td>
	            </tr>
	            {section name=sec1 loop=$detailArr[sec]}
	              <tr>
	                <td align="center" colspan="2">{$smarty.section.sec1.rownum}</td>
	                <td align="center" colspan="3">{$masterArr[sec].range}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].testMeterAverage|number_format:$detailArr[sec][sec1].stdMeter1AfterDecimal:".":""}</td>
	                <td align="center">{$detailArr[sec][sec1].stdMeterAverage|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$masterArr[sec].range2}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].tableNo2reading|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].errorUnitsTableNo2}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].errorRdgTableNo2|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="2">{$detailArr[sec][sec1].expandedUncertinityInPre}</td>
	              </tr>
	            {/section}
	            </table>
	          {/if}
	        {/if}
				  {if $masterArr[sec].tableNo == 3}
				    {if $masterArr[sec].dataPresent == 1}
				      <table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				      <tr>
				      	<div style="margin:10px;"><b>{$masterArr[sec].parameterName}</b></div>
			        </tr>
				      <tr>
				        <td align="left" colspan="2" rowspan="2">SR No</td>
	              <td align="center" colspan="6">Callibration</td>
	              <td align="center" colspan="6">Unit Under Test</td>
	              <td align="center" rowspan="2">Secondary<br /> Calculated<br /> Reading in<br /> Amps.</td>
	              <td align="center" colspan="6">Error</td>
	              <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	            </tr>
	            <tr>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Applied <br />Ampere</td>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Secondary <br />Reading<br />in Amps.</td>
	              <td align="center" colspan="3">Units</td>
	              <td align="center" colspan="3">% of Rdg.</td>
	            </tr>
	            {section name=sec1 loop=$detailArr[sec]}
	              <tr>
	                <td align="center" colspan="2">{$smarty.section.sec1.rownum}</td>
	                <td align="center" colspan="3">{$masterArr[sec].range}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].testMeterAverage|number_format:$detailArr[sec][sec1].stdMeter1AfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$masterArr[sec].range2}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].stdMeterAverage|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center">{$detailArr[sec][sec1].tableNo3calculated|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].errorUnitsTableNo3|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].errorRdgTableNo3|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="2">{$detailArr[sec][sec1].expandedUncertinityInPre}</td>
	              </tr>
	            {/section}
	            </table>
	          {/if}
	        {/if}
				  {if $masterArr[sec].tableNo == 4}
				    {if $masterArr[sec].dataPresent == 1}
				      <table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				      <tr>
				      	<div style="margin:10px;"><b>{$masterArr[sec].parameterName}</b></div>
			        </tr>
				      <tr>
				        <td align="left" colspan="2" rowspan="2">SR No</td>
	              <td align="center" colspan="6">Callibration Standard</td>
	              <td align="center" colspan="8">Unit Under Test</td>
	              <td align="center" colspan="6">Error</td>
	              <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	            </tr>
	            <tr>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Primary<br />Reading<br />in Volt.</td>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Secondary<br />Reading in<br />Volt.</td>
	              <td align="center">M.F.</td>
	              <td align="center">Calculated<br />Reading in<br />Volt.</td>
	              <td align="center" colspan="3">Units</td>
	              <td align="center" colspan="3">% of Rdg.</td>
	            </tr>
	            {section name=sec1 loop=$detailArr[sec]}
	              <tr>
	                <td align="center" colspan="2">{$smarty.section.sec1.rownum}</td>
	                <td align="center" colspan="3">{$masterArr[sec].range}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].testMeterAverage|number_format:$detailArr[sec][sec1].stdMeter1AfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$masterArr[sec].range2}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].stdMeterAverage|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center">{$detailArr[sec][sec1].mf}</td>
	                <td align="center">{$detailArr[sec][sec1].tableNo4calculated}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].errorUnitsTableNo4|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].errorRdgTableNo4|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="2">{$detailArr[sec][sec1].expandedUncertinityInPre}</td>
	              </tr>
	            {/section}
	            </table>
	          {/if}
	        {/if}
				  {if $masterArr[sec].tableNo == 5}
				    {if $masterArr[sec].dataPresent == 1}
				      <table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				      <tr>
				      	<div style="margin:10px;"><b>{$masterArr[sec].parameterName}</b></div>
			        </tr>
				      <tr>
				        <td align="left" colspan="2" rowspan="2">SR No</td>
	              <td align="center" colspan="6">Callibration Standard</td>
	              <td align="center" colspan="6">Unit Under Test</td>
	              <td align="center" colspan="6">Error</td>
	              <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	            </tr>
	            <tr>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Reading</td>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Reading</td>
	              <td align="center" colspan="3">Units</td>
	              <td align="center" colspan="3">% of Rdg.</td>
	            </tr>
	            {section name=sec1 loop=$detailArr[sec]}
	              <tr>
	                <td align="center" colspan="2">{$smarty.section.sec1.rownum}</td>
	                <td align="center" colspan="3">{$masterArr[sec].range}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].testMeterAverage|number_format:$detailArr[sec][sec1].stdMeter1AfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$masterArr[sec].range2}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].stdMeterAverage|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].errorUnits|number_format:$detailArr[sec][sec1].stdMeter1AfterDecimal:".":""}</td>
	                <td align="center" colspan="3">{$detailArr[sec][sec1].errorRdg|number_format:$detailArr[sec][sec1].resolutionTypeAAfterDecimal:".":""}</td>
	                <td align="center" colspan="2">{$detailArr[sec][sec1].expandedUncertinityInPre}</td>
	              </tr>
	            {/section}
	            </table>
	          {/if}
	        {/if}
	      {/section}
	    </td>
		</tr>

{include file='nablCertificateNewPageBottom.tpl'}
