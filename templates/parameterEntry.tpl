{include file="./headStart.tpl"}
<title>Parameter</title>
<script type="text/javascript">
$(document).ready(function() {
    $('#parameterName').focus();
});

function confirmDelete()
{
	var answer = confirm("Delete Selected Recored ?")
	if (answer)
	{
	  document.messages.submit();
	}
	return false;
}
</script>
{include file="./headEnd.tpl"}
<form name="parameterForm" method="post" action="{$smarty.server.PHP_SELF}" id="parameterForm">
<input type="hidden" name="parameterId" value="{$parameterId}" />
{if (have_access_role($smarty.const.MASTER_PARAMETER_MODULE_ID,"add") && {!isset($smarty.get.parameterId)}) || (have_access_role($smarty.const.MASTER_PARAMETER_MODULE_ID,"edit") && {isset($smarty.get.parameterId)})}
    <table border="1" align="center" cellpadding="1" cellspacing="1">
    <tr>
      <td align="center" class="table1" colspan="2">Parameter Entry</td>
    </tr>
    <tr>
      <td><input type="text" name="parameterName" id="parameterName" value="{$currentparameterName}"  required="required" title="ParameterName" /></td>
    </tr>
    <tr>
      <td align="center"><input type="submit" name="insertBtn" value="Store !" class="button" /> <input type="submit" value="CANCEL" name="cancelBtn" class="button" /></td>
    </tr>
    </table>
{/if}
</form>
<br /><br />
<table align="center" border="0" cellpadding="1" cellspacing="1">
{section name="sec" loop=$parameterArr}
<tr>
	<td class="table2" align="center">
            {if have_access_role($smarty.const.MASTER_PARAMETER_MODULE_ID,"edit")}
                <a href="parameterEntry.php?parameterId={$parameterArr[sec].parameterId}" class="link">Edit</a>
            {/if}
	</td>
	<td class="table2" align="center">
            {if have_access_role($smarty.const.MASTER_PARAMETER_MODULE_ID,"delete")}
                <a href="deleteParameter.php?parameterId={$parameterArr[sec].parameterId}" onclick="return confirmDelete();" id="delete{$parameterArr[sec].parameterId}" class="link">Delete</a>
            {/if}
	</td>
	<td class="table2" align="left">{$parameterArr[sec].parameterName}</td>
</tr>
{sectionelse}
<tr>
  <td class="table2" align="center" colspan="3">No Records Found.....!!!!!!!!</td>
</tr>
{/section}
</table>
{include file="footer.tpl"}