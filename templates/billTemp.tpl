{include file="./headStart.tpl"}
<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".add").click(function(){ 
     $("#one").clone(true).appendTo("#mainDiv").find('input[type="text"]').val('');               
  });

// To take labour : Starts
  $('.classForLabour').change(function(){
    	var dataString = "partyId=" + $('#partyIdcombo').val() + "&itemId=" + $('#itemId').val() + "&partyTypeId=9";
    	$.ajax({
    		type: "GET",
    		url: "./labourAjax.php",
    		data: dataString,
    		success:function(data){
    			$('#labour').val(data);
        }
      });
  });    
// To take labour : Ends	
	
// {*show client : Start's*}
  $("#partyIdcombo").change(function(){ 
  var datastring = "partyIdcombo=" + $('#partyIdcombo').val();
    $.ajax({
      url: "./billAj.php",
     data: datastring,
      success: function(data){
       $('#showBill').html(data);
      }
    });
  });
 
 	$(".noOfPair").change(function(){
  	var noOfPair    = $(this).val();
  	var bagWeight   = $(this).next().val();
  	var weight      = $(this).next().next().val();
  	changeNetWeight(noOfPair, bagWeight, weight, $(this).next().next().next().next());
  	//$(this).next().next().next().next().val((noOfPair * bagWeight) - weight);
  });

  $(".bagWeight").change(function(){
  	var noOfPair  = $(this).prev().val();
  	var bagWeight = $(this).val();
    var weight    = $(this).next().val();           
  	changeNetWeight(noOfPair, bagWeight, weight, $(this).next().next().next());
  });

  $(".weight").change(function(){
  	var noOfPair     = $(this).prev().prev().val();
  	var bagWeight    = $(this).prev().val();
    var weight       = $(this).val();           
    var netWeightObj = $(this).next().next();
    var netWeight    = $(this).next().next().val();
  	changeNetWeight(noOfPair, bagWeight, weight,netWeightObj);
  	
  	var touch       = $(this).next().next().next().val();
  	var waste       = $(this).next().next().next().next().val();
  	var fineObj     = $(this).next().next().next().next().next();
  });

  $(".touch").change(function(){
  	var weight    = $(this).prev().prev().prev().val();
  	var touch     = $(this).val();
    var waste     = $(this).next().val();           
    var netWeight = $(this).prev().val();
  	changeFine(netWeight, touch, waste, $(this).next().next());
  });
  
  $(".waste").change(function(){
  	var weight    = $(this).prev().prev().prev().prev().val();
  	var touch     = $(this).prev().val();
    var waste     = $(this).val();           
    var netWeight = $(this).prev().prev().val();
  	changeFine(netWeight, touch, waste, $(this).next());
  });
  
  $(".netWeight").change(function(){
  	var netWeight = $(this).val();
  	var labour    = $(this).next().next().next().next().val();
  	var fine	    = $(this).next().next().next().val();
    var netWeight = $(this).val();
  	changeFine(netWeight,labour,fine);	
  	
  });
  
  $(".labour").change(function(){ 
  	var netWeight  = $(this).prev().prev().prev().prev().val();
  	var labour     = $(this).val();
  	changeAmount(netWeight, labour, $(this).next());
  	
  });
});

function changeNetWeight(noOfPair,bagWeight,weight,netWeight)
{
	netWeight.val(weight - (noOfPair * bagWeight));
	setAmount();
}

function changeAmount(netWeight,labour,amount)
{
	amount.val((netWeight * labour));
	setAmount();
}
function changeFine(netWeight,touch,waste,fine) 
{
	fine.val(((netWeight) * (parseFloat(touch) + parseFloat(waste)))/100);
	setAmount();
}

function deleteRow() {
   var countSr = 1;
   $('.delete').click(function(){
      $(this).parent().remove();
      return false;
   });
}

function setAmount()
{
  var totalWeight = 0;
  var totalFine   = 0;
  var totalAmount = 0;
  
  $('.weight').each(function(i)
  {
    price = $(this).val().replace("$","");
    if(!isNaN(price)) totalWeight+= Number(price);
  });
  
  $('.fine').each(function(i)
  {
    price = $(this).val().replace("$","");
    if(!isNaN(price)) totalFine += Number(price);
  });
  
  $('.amount').each(function(i)
  {
   price = $(this).val().replace("$","");
    if(!isNaN(price)) totalAmount+= Number(price);
  });

  $('#totWeight').val(totalWeight);
  $('#totFine').val(totalFine);
  $('#totAmount').val(totalAmount);
}

</script>
{include file="./headEnd.tpl"}
<form name="form1" action="" method="post">
<input type="hidden" name="isEdit" value="{$isEdit}" />
<input type="hidden" name="billmasterId" value="{$billmasterId}" />
<p align="center" class="table1" colspan="8">******* Bill ********</p>	
{if $msg != ''}<center><span style="color:#FF0000"><b>{$msg}</b></span></center>{/if}
<div class="contentDiv">
  <table border="0" align="center" width="100%">
  <tr>
    <td  class="table1" NOWRAP>
  		Party :
      <select name="party" id="partyIdcombo" class="classForLabour">
        <option value="0">Select Party</option>
        {html_options values=$partyIdcombo output=$partyName}
      </select>
      <input type="text" name="newParty" id="newParty" size="8" />
    </td>
    <td colspan="6" class="table1">
      Date :
      {html_select_date prefix="curDate" start_year="-2" end_year="+1" field_order="DMY" day_value_format="%02d" time=$supplierDate}
    </td>
    <td class="table1" align="center" >
      <select name="drcr">
        <option value="Dr">Debit</option>
        <option value="Cr">Credit</option>
      </select> : Debit / Credit 
    </td>    
  </tr>
  </table>
<table id="billDetailTable" border="0" align="center" width="100%">
<tr class="table1">
  <td>
    <td class="table1">Marko</td>
    <td class="table1">No.Of Pair</td>
    <td class="table1">Bag Weight</td>
    <td class="table1">Weight</td>
    <td class="table1">Item Name</td>
    <td class="table1">Net Weight</td>
    <td class="table1">Touch</td>
    <td class="table1">Waste</td>
    <td class="table1">Fine</td>
    <td class="table1">Labour</td>
    <td class="table1">Amount</td>
  </td>
  </tr>
</table>
  <table id="billDetailTable" border="0" align="center" width="100%">     
    <tbody id="main Div">
      <tr id="one" class="itemRow">
    	  <td>
    	    <input size="7" type="text" name="marko[]"     class="marko" /> 
    	    <input size="7" type="text" name="noOfPair[]"  class="noOfPair"  onChange="setAmount(this)"/> 
    	    <input size="7" type="text" name="bagWeight[]" class="bagWeight" /> 
          <input size="5" type="text" name="weight[]"    class="weight"    onChange="setAmount(this)"/> 
          <select name="itemId[]" id="itemId" class="classForLabour">
            <option value="0">Select Item</option>
              {html_options values=$itemId output=$itemName}	
            </select> 
          <input size="7" type="text" name="netWeight[]" class="netWeight"  /> 
          <input size="5" type="text" name="touch[]"     class="touch"    onChange="setAmount(this)" /> 
          <input size="5" type="text" name="waste[]"     class="waste"    onChange="setAmount(this)" /> 
          <input size="5" type="text" name="fine[]"      class="fine"      readonly="readonly" onChange="setAmount(this)" /> 
          <input size="5" type="text" name="labour[]"    class="labour" id="labour" value="" onChange="setAmount(this)" /> 
          <input size="5" type="text" name="amount[]"    class="amount"    onChange="setAmount(this)"  /> 
          <input type="button" name="add" class="add" value="Add" /> 
          <input type="button" name="deleteBtn" class="delete" value="Delete"  onclick="deleteRow()"/>
        </td> 
      </tr>
    </tbody>
  </table>
  <table width="100%">
    <tr class="itemRowTot"><td>
    <b>Total</b>  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
    <input size="5" type="text" name="totWeight" id="totWeight"/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input size="5" type="text" name="totFine"   id="totFine"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
    <input size="5" type="text" name="totAmount" id="totAmount"/>
    </td>
    </tr>
  </table>
  <div align="left">
    {if $isEdit == 1}
    <input type="submit" name="billBookAddBtn" value="save"/>
  {else}
    <input type="submit" name="billBookAddBtn" value="Store"/>
     <a href="./billPrint.php">Print</a>	
  {/if}
 </div>
</div>
<br/><br/><br/>
<div id="showBill">
</div>
</form>
<script type="text/javascript">
{if $isEdit == 1}
  setAmount();
{/if}
</script>
{include file="footer.tpl"}