{if $is_table == '1'}
    Table Index:
    {if $selected_table_no != ''}
        <select name="table_no" id="table_no" style="width: 80px;" onchange="get_range()" required="">
        {foreach from=$table_arr key=k item=det}
            <option {if $selected_table_no == $det}selected="selected"{else}{/if} value="{$det}"> {$det}</option>
        {/foreach}
    </select>
    {else}
        <select name="table_no" id="table_no" style="width: 80px;" onchange="get_range()" required="">
            {foreach from=$table_arr key=k item=det}
                <option selected="selected" value="{$det}"> {$det}</option>
            {/foreach}
        </select>
    {/if}
    <!-- Table Title: -->
    <input type="hidden" name="table_title" id="table_title" value="{$selected_table_title}" size="5" />
{else}
    <br/><span style="float:left; ">Code :<input type="text" style="width: 100px;" name="itemCode" value="{$itemCode}"  /></span>
    <input type="hidden" id="uuc_range_hid" value="{$range}" />
    <input type="hidden" id="instrumentId_hid" value="{$instrumentId}" />
    <input type="hidden" id="make_modal_hid" value="{$make_modal}" />
    <input type="hidden" id="temp_hid" value="{$temperature}" />
    <input type="hidden" id="humidity_hid" value="{$humidity}" />
    <input type="hidden" id="uuc_location_hid" value="{$uuc_location}" />
    <input type="hidden" id="approved_by_hid" value="{$approved_by}" />

    Phase Type :
    <select name="phase_type_id" id="phase_type_id">
        <option value="0">Select Phase</option>
        <option value="1">Phase - 1</option>
        <option value="2">Phase - 3</option>
    </select>
    <br/>
    <br/>
{/if}
