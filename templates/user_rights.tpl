{include file="./headStart.tpl"}
<head>
<script type="text/javascript" src="./js/jquerynew.min.js"></script>
</head>
<title>User Rights</title>
{include file="./headEnd.tpl"}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Rights
        </h1>
    </section>
    <div class="clearfix">
        <div class="row">
            <div style="margin: 15px;">
                <div class="col-md-12">
                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="{$smarty.server.PHP_SELF}" id="form1" class="main-frm" name="form1" method='POST'>
                                        <table width="95%" align="center" cellpadding="2" cellspacing="0" border="1" style="font-size:15px;">
                                            <tr>
                                            <th width="20%">
                                                User <span class="required-sign">*</span>
                                            </th>
                                            <th>
                                                <select class="form-control select2" id="user_type" name="user_type" onchange="window.location='user_rights.php?user_type='+$(this).val();" required="" style="width: 90%;">
                                                    <option value="">- Select User - </option>
                                                    {foreach from=$data['users'] key=k item=det}
                                                        <option {if $user_type_id == $det.staffId}selected="selected"{else}{/if} value="{$det.staffId}"> {$det.staffName}</option>
                                                    {/foreach}
                                                </select>
                                            </th>
                                                
                                            </tr>
                                        </table>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="panel panel-default">
                                                <table width="95%" align="center" cellpadding="2" cellspacing="0" border="1" style="font-size:15px;">
                                                    <thead>
                                                        <tr>
                                                            <th width="20%" >Module</th>
                                                            <th width="80%" align="left">
                                                                Roles
                                                                <input type="button" value="Un Select ALL" class="un-chk-all" style="margin-right: 5px; float: right;">
                                                                <input type="button" value="Select ALL" class="chk-all" style="margin-right: 5px; float: right;">
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {foreach from=$data['modules_roles'] key=rk item=row}
                                                        <tr>
                                                            <td>{$row.title}</td>
                                                            <td>
                                                                {foreach from=$row['roles'] key=rok item=role}
                                                                <label class="col-sm-2">
                                                                    <input type="checkbox" style="height:17px; width: 17px;" {if in_array($role.module_role_id, $data['user_roles'])}checked="checked"{else}{/if} class="chkids {$row.main_module}" value="{$role.module_role_id}" name="roles[{$role.module_role_id}_{$role.website_module_id}]" /> {str_replace("_", " ", ucwords($role.title))}
                                                                </label>
                                                                {/foreach}
                                                            </td>
                                                        </tr>
                                                        {/foreach}
                                                    </tbody>
                                                    <tr>
                                                        <td></td>
                                                        <td align="center"><input type="submit" name="insertBtn" value="Update" class="button btn-update-roles" /></td>
                                                    </tr>
                                                </table>
                                        </div>
                                                    
                                    </form>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".chk-all").click(function(){
            $(".chkids").prop("checked",true);
        });

        $(".un-chk-all").click(function(){
            $(".chkids").prop("checked",false);
        });
        

        $(".btn-update-roles").click(function(){
            var form_obj = $(this);
            $.ajax({
                type: "POST",
                url: form_obj.attr('action'),
                data: form_obj.serialize(), //serializes the form's elements.
                dataType:'json',
                success: function(data){
                    alert('Updated Successfullly');
                }
            });
        });
    });
    
</script>

{include file="footer.tpl"}