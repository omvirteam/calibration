{include file="./headStart.tpl"}
<script type="text/javascript">
  $(document).ready(function(){ 
    $(".add").live('click',function(){
      $("#one").clone(true).appendTo("#mainDiv").find('input[type="text"]').val('');
    });
    $(".delete").click(function(){
      $(this).parent().parent().remove();
    });

     $("#masterMeterId").change(function(){ 
    	 var dataString = "masterMeterId=" + $("#masterMeterId").val();
       $.ajax({
       	 type: "GET",
         url:  "masterMeterJq.php",
         data: dataString,
         success: function(data){ 
         	$("#masterpara").html(data);
         	
            // Ajax Call At Range :Start 
            $("#masterMeterSubId").change(function(){ 
    	        var dataString = "masterMeterSubId=" + $("#masterMeterSubId").val();
                $.ajax({
       	          type: "GET",
                  url:  "masterMeterJqSub.php",
                  data: dataString,
                  success: function(data){
         	          $("#masterparasub").html(data);
         	          
                    // Get accuracysub With Ajax  :Start 
                    $("#masterMeterSubSubId").change(function(){ 
    	             	  var dataString = "masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
                   	    $.ajax({
       	           	    type: "GET",
                   	    url:  "masterMeterJqSubAccuracy.php",
                   	    data: dataString,
                   	    success: function(data1){ 
                   	    	$("#rangedisplay").html(data1);
                          	$('.accuracyTaken').each(function()
                           {
                           	//On Changes Of Range Calculate Accuracy % taking in to account
                              calculateAccuracy($(this));
                              resolutionForTypeB($(this));
                              stabilityForTypeB($(this));
                              stabilityForTypeInPerc($(this));
                              combinedUncertinity($(this));
                              combinedUncertinityInPerc($(this));
                              effectiveUncertinity($(this));
                              effectiveUncertinityInPer($(this));
                              effectiveDegreeOfFreed($(this));
                              meanReadingCalculate($(this));
                              masterMeterReading($(this));
                              error($(this));
                            });
                   	    }
                   	  });
                   	});
                  }	
                });
            });
                  // Get accuracysub With Ajax: End
         }
       });
     });
       
     
    ///////////////
    $(".stdMeter1").change(function(){
    	calculateAverage($(this));
    });
    $(".stdMeter2").change(function(){
    	calculateAverage($(this).prev());
    });
    $(".stdMeter3").change(function(){
    	calculateAverage($(this).prev().prev());
    });
    $(".stdMeter4").change(function(){
    	calculateAverage($(this).prev().prev().prev());
    });
    $(".stdMeter5").change(function(){
    	calculateAverage($(this).prev().prev().prev().prev());
    });
    ///////////////
    $(".testMeter1").change(function(){
    	calculateTestAverage($(this));
    });
    $(".testMeter2").change(function(){
    		calculateTestAverage($(this).prev());
    });
    $(".testMeter3").change(function(){
    	calculateTestAverage($(this).prev().prev());
    });
    $(".testMeter4").change(function(){
    	calculateTestAverage($(this).prev().prev().prev());
    });
    $(".testMeter5").change(function(){
    	calculateTestAverage($(this).prev().prev().prev().prev());
    });
    $(".accuracyTaken").change(function(){
    	  calculateAccuracy($(this).next());
    });
    $('.stdMeterAverage').live('change', function() { 
	    	stdMeterAverageFinalVal  = ($(this).prev().val()); 
    	calculateAccuracy(stdMeterAverageFinalVal);
    });
//     $(".stdMeterAverage").change(function(){ 
//    	calculateAccuracy($(this));
//    	alert($(this));
//     // stdMeterAverageFinalVal  = $(".stdMeterAverage").val();
//    });
  });

{literal}
$(document).ready( function () { 	
  $("#grnId").change(function(){ 
    var datastring = "grnId=" + $("#grnId").val();
    $.ajax({
      url: "./obsSheetAj.php",
      data: datastring,
      success: function(data){ 
        $('#showItem').html(data);
        $('#custCode').val($('#custCodeHidden').val());
        //
        $("#grnDetailId").change(function(){
          var datastring = "grnDetailId=" + $("#grnDetailId").val();
          $.ajax({
            url: "./obsSheetAjGrnDetailChange.php",
            data: datastring,
            success: function(data){
              $('#showDetail').html(data);
            }
          });
        });
      }
    });
  });
});
{/literal}

function calculateAverage(stdMeter1)
{
	valuesAvailable = 1;
	stdMeter1Val = stdMeter1.val();
	stdMeter2Val = stdMeter1.next().val();
	stdMeter3Val = stdMeter1.next().next().val();
	stdMeter4Val = stdMeter1.next().next().next().val();
	stdMeter5Val = stdMeter1.next().next().next().next().val();
	stdMeterAverageObj = stdMeter1.next().next().next().next().next();
  
	if(stdMeter2Val != "")  valuesAvailable++;  else  stdMeter2Val = 0;
	if(stdMeter3Val != "")  valuesAvailable++;  else  stdMeter3Val = 0;
	if(stdMeter4Val != "")  valuesAvailable++;  else  stdMeter4Val = 0;
	if(stdMeter5Val != "")  valuesAvailable++;  else  stdMeter5Val = 0;

  stdMeterAverageObj.val((parseFloat(stdMeter1Val) + parseFloat(stdMeter2Val) + parseFloat(stdMeter3Val) + parseFloat(stdMeter4Val) 
                       + parseFloat(stdMeter5Val)) / valuesAvailable);
                    
  calculateAccuracy(stdMeterAverageObj);
}

function calculateTestAverage(testMeter1)
{
	valuesAvailable = 1;
	testMeter1Val = testMeter1.val();
	testMeter2Val = testMeter1.next().val();
	testMeter3Val = testMeter1.next().next().val();
	testMeter4Val = testMeter1.next().next().next().val();
	testMeter5Val = testMeter1.next().next().next().next().val();
	testMeterAverageObj = testMeter1.next().next().next().next().next();

	if(testMeter2Val != "")  valuesAvailable++;  else  testMeter2Val = 0;
	if(testMeter3Val != "")  valuesAvailable++;  else  testMeter3Val = 0;
	if(testMeter4Val != "")  valuesAvailable++;  else  testMeter4Val = 0;
	if(testMeter5Val != "")  valuesAvailable++;  else  testMeter5Val = 0;

  testMeterAverageObj.val((parseFloat(testMeter1Val) + parseFloat(testMeter2Val) + parseFloat(testMeter3Val) + parseFloat(testMeter4Val) 
                         + parseFloat(testMeter5Val)) / valuesAvailable);
  calculateAccuracy(testMeterAverageObj);
}
// Find The Accuracy :Start 
function calculateAccuracy(stdMeterAverageObj)
{ 
	rangeValue = $("#masterMeterAccuracyRangeArray").val(); 
	accuracy   = $("#masterMeterAccuracyArray").val();
	accuracyFs = $("#masterMeterAccuracyFSArray").val();
	accuracyTakenTotalObj = stdMeterAverageObj.next();
  accuracyTakenTotalObj.val(((parseFloat(stdMeterAverageObj.val()) * parseFloat(accuracy)) + (parseFloat(rangeValue) * parseFloat(accuracyFs))) / 100);
  standardDeviation(accuracyTakenTotalObj);
}
// Find The Accuracy :End
// Find The Standard Daviation :Start 
function standardDeviation(accuracyTakenTotalObj)
{
	stdMeter1Val = parseFloat(accuracyTakenTotalObj.prev().prev().prev().prev().prev().prev().val());
	stdMeter2Val = parseFloat(accuracyTakenTotalObj.prev().prev().prev().prev().prev().val());
	stdMeter3Val = parseFloat(accuracyTakenTotalObj.prev().prev().prev().prev().val());
	stdMeter4Val = parseFloat(accuracyTakenTotalObj.prev().prev().prev().val());
	stdMeter5Val = parseFloat(accuracyTakenTotalObj.prev().prev().val());
  stdMeterAverageDev = parseFloat(accuracyTakenTotalObj.prev().val());
  
  standardDeviationObj  = accuracyTakenTotalObj.next();
  stdMeter1ValDeviation = (stdMeter1Val - stdMeterAverageDev) * (stdMeter1Val - stdMeterAverageDev);
  stdMeter2ValDeviation = (stdMeter2Val - stdMeterAverageDev) * (stdMeter2Val - stdMeterAverageDev);
  stdMeter3ValDeviation = (stdMeter3Val - stdMeterAverageDev) * (stdMeter3Val - stdMeterAverageDev);
  stdMeter4ValDeviation = (stdMeter4Val - stdMeterAverageDev) * (stdMeter4Val - stdMeterAverageDev);
  stdMeter5ValDeviation = (stdMeter5Val - stdMeterAverageDev) * (stdMeter5Val - stdMeterAverageDev);
  standardDeviationObj.val(Math.sqrt((stdMeter1ValDeviation + stdMeter2ValDeviation 
                                    + stdMeter3ValDeviation + stdMeter4ValDeviation 
                                    + stdMeter5ValDeviation) / 5));
  standardUncertinity(standardDeviationObj);     
}
// Find The Standard Daviation :End

// Find The Standard Uncertinity :Start 
 function standardUncertinity(standardDeviationObj)
  {
  	standardDeviationMain = standardDeviationObj.val();
  	standardUncertinityObj = standardDeviationObj.next();
  	standardUncertinityObj.val((parseFloat(standardDeviationMain))/(Math.sqrt(5)));
  	
    standardUncertinityPerc(standardUncertinityObj);
  }
// Find The Standard Uncertinity :End

// Find The Standard Uncertinity :Start 
  function standardUncertinityPerc(standardUncertinityObj)
  {
  	standardUncertinityMain = parseFloat(standardUncertinityObj.val());
  	stdMeterAverageVal = parseFloat(standardUncertinityObj.prev().prev().prev().val());
  	standardUncertinityObjPerc = standardUncertinityObj.next();
  	standardUncertinityObjPerc.val((standardUncertinityMain*100)/stdMeterAverageVal);
    
  	degreeOfFreedom(standardUncertinityObjPerc);
  }
// Find The Standard Uncertinity :End

// Find The Degree Of FreeDom :Start
  function degreeOfFreedom(standardUncertinityObjPerc)
  {
  	valuesAvailable = 1;
  	stdMeter1Val = standardUncertinityObjPerc.prev().prev().prev().prev().prev().prev().prev().prev().prev().val();
  	stdMeter2Val = standardUncertinityObjPerc.prev().prev().prev().prev().prev().prev().prev().prev().val();
  	stdMeter3Val = standardUncertinityObjPerc.prev().prev().prev().prev().prev().prev().prev().val();
  	stdMeter4Val = standardUncertinityObjPerc.prev().prev().prev().prev().prev().prev().val();
  	stdMeter5Val = standardUncertinityObjPerc.prev().prev().prev().prev().prev().val();
    if(stdMeter2Val != "")  valuesAvailable++;  else  stdMeter2Val = 0;
	  if(stdMeter3Val != "")  valuesAvailable++;  else  stdMeter3Val = 0;
	  if(stdMeter4Val != "")  valuesAvailable++;  else  stdMeter4Val = 0;
	  if(stdMeter5Val != "")  valuesAvailable++;  else  stdMeter5Val = 0;     
	  	
    degreeOfFreedomObj = standardUncertinityObjPerc.next();
    degreeOfFreedomObj.val(valuesAvailable-1);  
    uncertinityForTypeB(degreeOfFreedomObj);	
  }   
// Find The Degree Of FreeDom :End  
 // Type B Uncertinity Master :Start
    function uncertinityForTypeB(degreeOfFreedomObj)
    {
    	uncertinity = $("#masterMeterUncertintyArray").val();
    	stdMeterAverageVal = parseFloat(degreeOfFreedomObj.prev().prev().prev().prev().prev().prev().val());
    	uncertinityObj = degreeOfFreedomObj.next();
    	uncertinityObj.val((parseFloat(uncertinity/2)) * parseFloat(stdMeterAverageVal) * 0.01);
    	uncertinityInPercentage(uncertinityObj)
    }
    function uncertinityInPercentage(uncertinityObj)
    {
      uncertinity = $("#masterMeterUncertintyArray").val();
      uncertinityInPerObj = uncertinityObj .next();
      uncertinityInPerObj.val(parseFloat(uncertinity/2));  	
      accuracyForTypeB(uncertinityInPerObj)
    }
    function accuracyForTypeB(uncertinityInPerObj)
    {
      accuracy   = uncertinityInPerObj.prev().prev().prev().prev().prev().prev().val();
      stdMeterAverageDev = parseFloat(uncertinityInPerObj.prev().prev().prev().prev().prev().prev().prev().val());
      accuracyForTypeBObj = uncertinityInPerObj.next();
      accuracyForTypeBObj.val((((accuracy)*stdMeterAverageDev*0.01)/(Math.sqrt(3))));
      acuuracyForTypeBPerc(accuracyForTypeBObj)
    }
    function acuuracyForTypeBPerc(accuracyForTypeBObj)
    {
      accuracy   = accuracyForTypeBObj.prev().prev().prev().prev().prev().prev().prev().val();
      stdMeterAverageDev = parseFloat(accuracyForTypeBObj.prev().prev().prev().prev().prev().prev().prev().prev().val());
      accuracyForTypeBObj = accuracyForTypeBObj.next();
      accuracyForTypeBObj.val((((accuracy)*stdMeterAverageDev*0.01)));
      resolutionForTypeB(accuracyForTypeBObj);
    }
    function resolutionForTypeB(accuracyForTypeBObj)
    {
      resolutionUUT = $("#masterMeterResolutionArray").val();
      resolutionForTypeBObj = accuracyForTypeBObj.next();
      resolutionForTypeBObj.val((resolutionUUT/2)/(Math.sqrt(3)));
      resolutionForTypeBInPerc(resolutionForTypeBObj);
    }
    function resolutionForTypeBInPerc(resolutionForTypeBObj)
    {
      resolutionUUT = $("#masterMeterResolutionArray").val();
      stdMeterAverageDev = parseFloat(resolutionForTypeBObj.prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().val());
      resolutionForTypeBInPercObj = resolutionForTypeBObj.next();
      resolutionForTypeBInPercObj.val(((resolutionUUT/2)/(Math.sqrt(3))*100)/parseFloat(stdMeterAverageDev));   	
      stabilityForTypeB(resolutionForTypeBInPercObj)
    }
    function stabilityForTypeB(resolutionForTypeBInPercObj)
    {
    	stability = $("#masterMeterStabilityArray").val();
    	stdMeterAverageDev = parseFloat(resolutionForTypeBInPercObj.prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().val());
    	stabilityForTypeBObj = resolutionForTypeBInPercObj.next();
    	stabilityForTypeBObj.val( ((stability * stdMeterAverageDev)*0.01) / (Math.sqrt(3)) );
    	stabilityForTypeInPerc(stabilityForTypeBObj);
    }
    function stabilityForTypeInPerc(stabilityForTypeBObj)
    {
    	stability = $("#masterMeterStabilityArray").val();
    	stdMeterAverageDev = parseFloat(stabilityForTypeBObj.prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().val());
    	stabilityForTypeInPercObj = stabilityForTypeBObj.next();
    	stabilityForTypeInPercObj.val(((stability * stdMeterAverageDev)*0.01));
    	combinedUncertinity(stabilityForTypeInPercObj);
    }
    function combinedUncertinity(stabilityForTypeInPercObj)
    {
      standardUncertinityComined = parseFloat(stabilityForTypeInPercObj.prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().val());
      accuracyForTypeBCombined = parseFloat(stabilityForTypeInPercObj.prev().prev().prev().prev().prev().val());
      uncertinityForTypeBcombined = parseFloat(stabilityForTypeInPercObj.prev().prev().prev().prev().prev().prev().prev().val());
      resolutionTypeBCombined = parseFloat(stabilityForTypeInPercObj.prev().prev().prev().val());
      combinedUncertinityObj = stabilityForTypeInPercObj.next();
      combinedUncertinityObj.val(Math.sqrt((standardUncertinityComined * standardUncertinityComined) + (accuracyForTypeBCombined * accuracyForTypeBCombined) +
                                           (uncertinityForTypeBcombined * uncertinityForTypeBcombined) + (resolutionTypeBCombined * resolutionTypeBCombined) ));
     combinedUncertinityInPerc(combinedUncertinityObj);
    }
    function combinedUncertinityInPerc(combinedUncertinityObj)
    {
      combinedUncertinityInPercVal = parseFloat(combinedUncertinityObj.val());
      stdMeterAverageDevFinal = parseFloat(combinedUncertinityObj.prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().val());
      combinedUncertinityInPercObj = combinedUncertinityObj.next();
      combinedUncertinityInPercObj.val((combinedUncertinityInPercVal * 100) / stdMeterAverageDevFinal);
      effectiveUncertinity(combinedUncertinityInPercObj);
    }
    function effectiveUncertinity(combinedUncertinityInPercObj)
    {
    	combinedUncertinityForTypeB = parseFloat(combinedUncertinityInPercObj.prev().val());
    	effectiveUncertinityObj = combinedUncertinityInPercObj.next();
    	effectiveUncertinityObj.val(2 * parseFloat(combinedUncertinityForTypeB));
    	effectiveUncertinityInPer(effectiveUncertinityObj);
    }
    function effectiveUncertinityInPer(effectiveUncertinityObj)
    {
    	effectiveUncertinityForTypeB = parseFloat(effectiveUncertinityObj.val());
    	stdMeterAverageDevFinalForTypeB = parseFloat(combinedUncertinityObj.prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().val());
    	effectiveUncertinityInPerObj  = effectiveUncertinityObj.next();
    	effectiveUncertinityInPerObj.val((parseFloat(effectiveUncertinityForTypeB) * 100)/parseFloat(stdMeterAverageDevFinalForTypeB));
    	effectiveDegreeOfFreed(effectiveUncertinityInPerObj);
    }
    function effectiveDegreeOfFreed(effectiveUncertinityInPerObj)
    {
    	standardUncertinityInPerc = effectiveUncertinityInPerObj.prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().val();
    	combinedUncertinityInPercFinal = effectiveUncertinityInPerObj.prev().prev().val();
    	effectiveDegreeOfFreedObj = effectiveUncertinityInPerObj.next();
    	if(standardUncertinityInPerc == 0)
    	{
        effectiveDegreeOfFreedObj.val(0);
    	}
    	else
      {
      	effectiveDegreeOfFreedObj.val((parseFloat(combinedUncertinityInPercFinal) * parseFloat(combinedUncertinityInPercFinal) * parseFloat(combinedUncertinityInPercFinal) * parseFloat(combinedUncertinityInPercFinal)) / 
      	                              (parseFloat(standardUncertinityInPerc) * parseFloat(standardUncertinityInPerc) * 	parseFloat(standardUncertinityInPerc) * parseFloat(standardUncertinityInPerc)) / 4);
      }
      meanReadingCalculate(effectiveDegreeOfFreedObj)
    }
    function meanReadingCalculate(effectiveDegreeOfFreedObj)
    {
      meanReading = effectiveDegreeOfFreedObj.prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().prev().val();
      meanReadingObj = effectiveDegreeOfFreedObj.next();
      meanReadingObj.val(parseFloat(meanReading));
      masterMeterReading(meanReadingObj);
    }
    function masterMeterReading(meanReadingObj)
    {
      rangeValue = $("#masterMeterAccuracyRangeArray").val(); 
      masterMeterReadingObj = meanReadingObj.next();
      masterMeterReadingObj.val(parseFloat(rangeValue));
      error(masterMeterReadingObj);
    }
    function error(masterMeterReadingObj)
    {
      meanReadingForError = masterMeterReadingObj.prev().val();
      masterMeterReadingForError = masterMeterReadingObj.val();
      errorObj = masterMeterReadingObj.next();
      errorObj.val(parseFloat(masterMeterReadingForError)-parseFloat(meanReadingForError));
      expandedUncertinity(errorObj);
    }
    function expandedUncertinity(errorObj)
    {
      expandedUncertinityFinal = errorObj.prev().prev().prev().prev().val();
      expandedUncertinityFinalObj = errorObj.next();
      expandedUncertinityFinalObj.val(expandedUncertinityFinal);
    }
 // Type B Uncertinity Master :End
</script>
{include file="./headEnd.tpl"}
<form action="{$smarty.server.PHP_SELF}" id="form1" name="form1" method='POST'>
<input type="hidden" name="grnDetailPassId" value={$grnDetailPassId} />
<center class="center"><h2>Observation Sheet</h2></center><br>
<table border='1' cellpadding='5' cellspacing='0' align='center'>
  <tr>
    <td>GRN NO.:-</font>
      <select name="grnId" id="grnId">
        <option value="0">GRN</option>
        {html_options values=$grnId output=$grnPrefixNo}
      </select>
    <td>
      <font size="4">Cust Id No.:-</font>
      <input type="text" name="custCode" id="custCode" value="" size="2" />
    
    <td colspan="9"  NOWRAP><font size="4">Master Meter.:-</font>
     <!-- <select name="masterMeterId" id="masterMeterId">
        <option value="0">Master Meter</option>
        {html_options values=$masterMeterId output=$masterMeterName}
      </select>
     --> 
     <select name="masterMeterId" id="masterMeterId">
  	 <option value="0">Select Meter</option>
       {html_options values=$meterEntryIdArray output=$meterEntryNameArray}  
    </select>
    <span id="masterpara"></span>         
    <span id="masterparasub"></span>  
     {html_select_date time="$callibrationDate" prefix="callibrationDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
    {html_select_date time="$nextYearDate" prefix="nextYearDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY"  day_value_format="%02d"}
    </td>

  </tr>
  <tr>
    <td align="center" bgcolor="lightblue">INSTRUMENT.:-<span id="showItem"></span><div id="showDetail" required=required></div></td>
    <td align="center" bgcolor="lightblue">Make/Model :<input type="text" name="makeModel" id="makeModel" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Instrument I.D. No. : <input type="text" name="instrumentId" id="instrumentId" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Least Count : <input type="text" name="leastCount" id="leastCount" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Accuracy : <input type="text" name="accuracy" id="accuracy" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Temperature : <input type="text" name="temperature" id="temperature" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Relative Humidity : <input type="text" name="humidity" id="humidity" value="" size="10" required=required/></td>
    <td align="center" bgcolor="lightblue">Date : </td>
    <td align="center" bgcolor="lightblue">User : <input type="hidden" name="" id="" value="" size="10" required=required/></td>
  </tr>    
</table>
<table align="center" border='1' cellpadding='5' cellspacing='0'  >
  <tbody id="mainDiv">
    <tr id="one" class="tableRow">
    	<td>
       <font size="4">UUT Value </font>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="text" name="stdMeter1[]" class="stdMeter1"  size="5" align="right" title="stdMeter1" required=required>
          <input type="text" name="stdMeter2[]" class="stdMeter2"  size="5" align="right" title="stdMeter2" required=required>
          <input type="text" name="stdMeter3[]" class="stdMeter3"  size="5" align="right" title="stdMeter3" required=required>
          <input type="text" name="stdMeter4[]" class="stdMeter4"  size="5" align="right" title="stdMeter4" required=required>
          <input type="text" name="stdMeter5[]" class="stdMeter5"  size="5" align="right" title="stdMeter5" required=required>
          <input type="text" name="stdMeterAverage[]"           class="stdMeterAverage"           size="5" align="right" title="stdMeterAverage" READONLY>
          <input type="text" name="accuracyTaken[]"             class="accuracyTaken"             size="5" align="right" title="accuracyTaken" READONLY>
          <input type="text" name="standardDeviation[]"         class="standardDeviation"         size="5" align="right" title="standardDeviation" READONLY>
          <input type="text" name="standardUncertinity[]"       class="standardUncertinity"       size="5" align="right" title="standardUncertinity" READONLY>
          <input type="text" name="standardUncertinityperc[]"   class="standardUncertinityperc"   size="5" align="right" title="standardUncertinityperc" READONLY>
          <input type="text" name="degreeOfFreedom[]"           class="degreeOfFreedom"           size="5" align="right" title="degreeOfFreedom" READONLY>
          <input type="text" name="uncertinityForTypeB[]"       class="uncertinityForTypeB"       size="5" align="right" title="Uncertanity of Master" READONLY>
          <input type="text" name="uncertinityInPercentage[]"   class="uncertinityInPercentage"   size="5" align="right" title="uncertinityInPercentage" READONLY>
          <input type="text" name="accuracyForTypeB[]"          class="accuracyForTypeB"          size="5" align="right" title="accuracyForTypeB" READONLY>
          <input type="text" name="acuuracyForTypeBPerc[]"      class="acuuracyForTypeBPerc"      size="5" align="right" title="acuuracyForTypeBPerc" READONLY>
          <input type="text" name="resolutionTypeB[]"           class="resolutionTypeB"           size="5" align="right" title="resolutionTypeB" READONLY>
          <input type="text" name="resolutionForTypeBPerc[]"    class="resolutionForTypeBPerc"    size="5" align="right" title="resolutionForTypeBPerc" READONLY>
          <input type="text" name="stabilityForTypeB[]"         class="stabilityForTypeB"         size="5" align="right" title="stabilityForTypeB" READONLY>
          <input type="text" name="stabilityForTypeBInPerc[]"   class="stabilityForTypeBInPerc"   size="5" align="right" title="stabilityForTypeBInPerc" READONLY>
          <input type="text" name="combinedUncertinity[]"       class="combinedUncertinity"       size="5" align="right" title="combinedUncertinity" READONLY>
          <input type="text" name="combinedUncertinityInPerc[]" class="combinedUncertinityInPerc" size="5" align="right" title="combinedUncertinityInPerc" READONLY>
          <input type="text" name="effectiveUncertinity[]"      class="effectiveUncertinity"      size="5" align="right" title="effectiveUncertinity" READONLY>
          <input type="text" name="effectiveUncertinityInPer[]" class="effectiveUncertinityInPer" size="5" align="right" title="effectiveUncertinityInPer" READONLY>
          <input type="text" name="effectiveDegreeOfFreed[]"    class="effectiveDegreeOfFreed"    size="5" align="right" title="effectiveDegreeOfFreed" READONLY>
          <input type="text" name="meanReading[]"               class="meanReading"               size="5" align="right" title="MeanReading" READONLY>
          <input type="text" name="masterMeterReading[]"        class="masterMeterReading"        size="5" align="right" title="MeanReading" READONLY>
          <input type="text" name="error[]"                     class="error"                     size="5" align="right" title="Error" READONLY>
          <input type="text" name="expandedUncertinity[]"       class="expandedUncertinity"       size="5" align="right" title="ExpandedUncertinity" READONLY>
          <br>
          <font size="4">Master Meter Value</font>
          <input type="text" name="testMeter1[]" class="testMeter1" id="testMeter1Final1" size="5" align="right" title="testMeter1" required=required>
          <input type="text" name="testMeter2[]" class="testMeter2" id="testMeter1Final2" size="5" align="right" title="testMeter2" required=required>
          <input type="text" name="testMeter3[]" class="testMeter3" id="testMeter1Final3" size="5" align="right" title="testMeter3" required=required>
          <input type="text" name="testMeter4[]" class="testMeter4" id="testMeter1Final4" size="5" align="right" title="testMeter4" required=required>
          <input type="text" name="testMeter5[]" class="testMeter5" id="testMeter1Final5" size="5" align="right" title="testMeter5" required=required>
          <input type="text" name="testMeterAverage[]"  id="testMeterAverage"  size="5" align="right" title="testMeter5">

          <span><input type="button" value="Add" class="add" /></span>
          <span><input type="button" value="Remove" class="delete" /></span>
        </td>
        </tr>
      </tbody>
        <tr>
  <td align="center" colspan="2"><input type="submit" name="insertBtn" value="Submit" class="button" /></td>
</tr>
      </table>
  <div id="rangedisplay"></div> 
</table>
</form>
{include file="footer.tpl"}