<script type="text/javascript">
$(document).ready(function(){
        $(document).on('click',"#view_calculation",function(){
            testMeterAvg();
            meanReading();
            $('#calculation_table').show();
        });
        $(document).on('click',"#hide_calculation",function(){
            $('#calculation_table').hide();
        });
        $(document).on('change',"#table_info_id",function(){
            var image_url = $(this).find(':selected').data('image_url');
            if(typeof(image_url) !== 'undefined'){
                $('#image_span').html('<img style="float: right" width="400px"  src=".'+image_url+'" />');
            } else {
                $('#image_span').html('');
            }
        });
	

    $(document).on('click',".add",function(){
            var tr_obj = $(this).closest('.tableRow');
            var row_template = $("#row_template").clone(true)
            row_template.find('input[type="text"]').val('');
            row_template.removeAttr("id");
            row_template.removeAttr("style");
            row_template.insertAfter(tr_obj);
    });
        
        $(document).on('click',".delete",function(){
            $(this).parent().parent().parent().remove();
            return false;
	});
        
	$(document).on('click',".std_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_std:last').hasClass("main_std1")) {
            } else {
                td_obj.find('.main_std:last').remove();
            }
            uut_box_total_count();
	});
  
  	$(".std_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_std_html = '<span class="main_std"> <input type="text" name="stdMeter[][]" class="stdMeter" size="4" align="right" title="stdMeter"  ></span>';
            td_obj.find(".std_values").append(main_std_html);
            uut_box_total_count();
  	});
       
  	$(document).on('keydown',".stdMeter",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.stdMeter:last').val() != '') {
                            var last_main_std = td_obj.find('.main_std:last');
                            var main_std_html = '<span class="main_std">&nbsp;<input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  ></span>';
                            $(main_std_html).insertAfter(last_main_std);
                    }
                }
            }
            uut_box_total_count();
  	});
        
         
        $(document).on('click',".testMeter_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter:last').hasClass("main_testMeter1")) {
            } else {
                td_obj.find('.main_testMeter:last').remove();
            }
            master_box_total_count();
	});
        
  	$(".testMeter_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html = '<span class="main_testMeter"> <input type="text" name="testMeter[][]" class="testMeter" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter_values").append(main_testMeter_html);
            master_box_total_count();
  	});
        
  	$(document).on('keydown',".testMeter",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter:last').val() != '') {
                            var last_main_testMeter = td_obj.find('.main_testMeter:last');
                            var main_testMeter_html = '<span class="main_testMeter">&nbsp;<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   ></span>';
                            console.log(last_main_testMeter);
                            $(main_testMeter_html).insertAfter(last_main_testMeter);
                    }
                }
            }
            master_box_total_count();
  	});
        
        $(document).on('click',".testMeter2_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter2:last').hasClass("main_testMeter21")) {
            } else {
                td_obj.find('.main_testMeter2:last').remove();
            }
            master_box_total_count2();
	});
        
  	$(".testMeter2_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html2 = '<span class="main_testMeter2"> <input type="text" name="testMeter2[][]" class="testMeter2" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter2_values").append(main_testMeter_html2);
            master_box_total_count2();
  	});
        
  	$(document).on('keydown',".testMeter2",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter2:last').val() != '') {
                            var last_main_testMeter2 = td_obj.find('.main_testMeter2:last');
                            var main_testMeter_html2 = '<span class="main_testMeter2">&nbsp;<input type="text" name="testMeter2[][]" class="testMeter2 num_only" size="4" align="right" title="testMeter"   ></span>';
                            console.log(last_main_testMeter2);
                            $(main_testMeter_html2).insertAfter(last_main_testMeter2);
                    }
                }
            }
            master_box_total_count2();
  	});
        

  	$(document).on('input', ".num_only", function () {
        this.value = this.value.replace(/[^\d\.\-]/g, '');
    });
    uut_box_total_count();
    master_box_total_count();
    master_box_total_count2();
});

function uut_box_total_count(){
    var box_count = 0;
    var box_count_filled = 0;
    $('tr.tableRow:visible').find('.stdMeter').each(function(ind,ele){
        if($(this).val() != '') {
            box_count_filled++;
        }
        box_count++;
    });
    $('#uut_box_total').html(box_count);
    $('#box_count_filled').html(box_count_filled);
}

function master_box_total_count(){
    var test_box_count = 0;
    var test_box_count_filled = 0;
    $('tr.tableRow:visible').find('.testMeter').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled++;
        }
        test_box_count++;
    });
    $('#test_box_total').html(test_box_count);
    $('#test_box_count_filled').html(test_box_count_filled);
}

function master_box_total_count2(){
    var test_box_count2 = 0;
    var test_box_count_filled2 = 0;
    $('tr.tableRow:visible').find('.testMeter2').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled2++;
        }
        test_box_count2++;
    });
    $('#test_box_total2').html(test_box_count2);
    $('#test_box_count_filled2').html(test_box_count_filled2);
}

function get_table_title(){
    var datastring = "grnDetailId=" + $("#grnDetailId").val() + "&table_no=" + $("#table_no").val();
    $.ajax({
        url: "./obsSheetAjGrnDetailChange.php",
        data: datastring,
        datatype:'json',
        success: function(data){
            var json = $.parseJSON(data);
            $('#table_title').val('');
            console.log('sss'.json);
            $('#table_title').val(json);
        }
    });
}
function getMasterMeterData()
{
	var dataString = "masterMeterId=" + $("#masterMeterId").val();
	$.ajax({
	  type: "GET",
	  url:  "masterMeterJq.php",
	  data: dataString,
	  success: function(data){ 
	    $("#masterpara").html(data);
	    $("#masterMeterSubId").focus();
	    
	    // Ajax Call At Range :Start 
	    $("#masterMeterSubId").change(function(){ 
	      var dataString = "masterMeterSubId=" + $("#masterMeterSubId").val();
	      $.ajax({
	        type: "GET",
	        url:  "masterMeterJqSub.php",
	        data: dataString,
	        success: function(data){
	           $("#masterparasub").html(data);
	           $("#masterMeterSubSubId").focus();
	          	// Get accuracysub With Ajax  :Start 
	          	$("#masterMeterSubSubId").change(function(){ 
	         	  var dataString = "masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
	         	  var dataString2 = "masterMeterSubSubId_2=" + $("#masterMeterSubSubId").val();
	       	    $.ajax({
		       	    type: "GET",
		       	    url:  "masterMeterJqSubAccuracy.php",
		       	    data: dataString,
		       	    success: function(data1){ 
		       	    	$("#rangedisplay").html(data1);
	       	      	}
	       	    });
	       	    $.ajax({
		       	    type: "GET",
		       	    url:  "masterMeterJqSubAccuracy.php",
		       	    data: dataString2,
		       	    success: function(data1){ 
                                var json = $.parseJSON(data1);
                                $("#accuracy_of_rdg").val(json.accuracySubSub);
                                $("#accuracy_of_fs").val(json.accuracySubSubFS);
                                $("#master_stability").val(json.stability);
                                $("#degree_of_freedom").val(json.degreeOfFreedom);
                                $("#uncertinty_from_tracebility").val(json.uncertinty);
                                $("#resolution_of_master").val(json.resolution);
                                $("#accuracy_taking_account").val(json.accuracy_main);
	       	      	}
	       	    });
	          });
	        }	
	      });
	    });
	    // Get accuracysub With Ajax: End
	  }
	});
        
}

function get_stability_of_source(){
    var master_stability_val_s = $('#master_stability').val();
    var stdMeterAverage_val_s = $('.stdMeterAverage').val();
    if(master_stability_val_s == ''){
        master_stability_val_s = 0;
    }
    var master_stability_val_ss = parseFloat(master_stability_val_s).toFixed(4);

    if(stdMeterAverage_val_s == ''){
        stdMeterAverage_val_s = 0;
    }
    var stdMeterAverage_val_ss = parseFloat(stdMeterAverage_val_s).toFixed(4);

    var stability_of_source = (master_stability_val_ss * stdMeterAverage_val_ss)*0.01/Math.sqrt(3);
    if(isNaN(stability_of_source)){
        stability_of_source = 0;
    }
    var stability_of_source_s = parseFloat(stability_of_source).toFixed(4);
    $('.stability_of_source').val(stability_of_source_s);
}

function getUnInPer(theObjInPer)
{
  // var row = $(theObjInPer).closest('tr.tableRow');
  // var ucInPer1 = $(theObjInPer).val() != '' ? $(theObjInPer).val() : 0;
  // var ucInPer = parseFloat(ucInPer1).toFixed(4);
  // var stdMeterAverage1 = $(".stdMeterAverage").val() != '' ? $(".stdMeterAverage").val() : 0;
  // var stdMeterAverage = parseFloat(stdMeterAverage1).toFixed(4);
  // var uncertaintyCalibration = (parseFloat(ucInPer) / 2 * parseFloat(stdMeterAverage)) / 100;
    // if(isNaN(uncertaintyCalibration)) {
       //  uncertaintyCalibration = 0;
    // }
    // uncertaintyCalibration_val = parseFloat(uncertaintyCalibration).toFixed(4);
    // row.find(".uncertaintyCalibration").val(uncertaintyCalibration_val);
}

function getUnInPerSecond(theObjInPerSecond)
{
  // var row = $(theObjInPerSecond).closest('tr.tableRow');
  // var ucInPerSecond = $(theObjInPerSecond).val() != '' ? $(theObjInPerSecond).val() : 0;
  // var stdMeterAverage = $(".stdMeterAverage").val() != '' ? $(".stdMeterAverage").val() : 0;
  
  // additional change by girish - start
  //var uncertaintyCalibrationInPer = (parseFloat(ucInPerSecond) * 100 / parseFloat(stdMeterAverage));
  //row.find(".uncertaintyCalibrationInPer").val(uncertaintyCalibrationInPer.toFixed(4));
  // additional change by girish - end
}

function meanReading(){
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
	var valuesAvailable = 0;
	
	var stdMeterValTotal = 0;
	var stdMeterValArr = [];
	row.find('.stdMeter').each(function(ind,ele){
		if($(this).val() != '') {
			var tmp_val1 = parseFloat($(this).val()) || 0;
			var tmp_val = parseFloat(tmp_val1).toFixed(4);
			stdMeterValTotal = parseFloat(stdMeterValTotal) + parseFloat(tmp_val);
			valuesAvailable++;
			stdMeterValArr.push(tmp_val);
		}
	});

	var stdMeterValTotal1 = parseFloat(stdMeterValTotal).toFixed(4);
	stdMeterAverageObj = stdMeterValTotal1 / valuesAvailable;
	if(isNaN(stdMeterAverageObj)){
            stdMeterAverageObj = 0;
        }
	row.find(".stdMeterAverage").val(stdMeterAverageObj.toFixed(4));
        var accuracyTaken_val = ((stdMeterAverageObj*($("#masterMeterAccuracyArray").val())+($("#masterMeterAccuracyRangeArray").val())*($("#masterMeterAccuracyFSArray").val()))/stdMeterAverageObj);
	if(isNaN(accuracyTaken_val)) {
            accuracyTaken_val = 0;
        }
        accuracyTaken_val = parseFloat(accuracyTaken_val).toFixed(4);
        row.find(".accuracyTaken").val(accuracyTaken_val);
	stdMeterAverageDev = stdMeterAverageObj;
	
	var stdMeterValDeviationTotal = 0;
	$.each(stdMeterValArr,function(ind,tmp_value){
		var tmpDeviation = (parseFloat(tmp_value) - parseFloat(stdMeterAverageDev)) * (parseFloat(tmp_value) - parseFloat(stdMeterAverageDev));
		stdMeterValDeviationTotal = parseFloat(stdMeterValDeviationTotal) + parseFloat(tmpDeviation);
	});
        var standardDeviation_val = (Math.sqrt(stdMeterValDeviationTotal / (valuesAvailable - 1)));
	if(isNaN(standardDeviation_val)) {
            standardDeviation_val = 0;
        }
        standardDeviation_val = parseFloat(standardDeviation_val).toFixed(4);
	row.find(".standardDeviation").val(standardDeviation_val);
	
	standardUncertinity = Math.sqrt(stdMeterValDeviationTotal / (valuesAvailable));
        var valuesAvailable = 0;
	row.find('.stdMeter').each(function(ind,ele){
		if($(this).val() != '') {
			valuesAvailable++;
		}
	});
        var standardUncertinity_val = (standardDeviation_val/Math.sqrt(valuesAvailable));
        if(isNaN(standardUncertinity_val)) {
            standardUncertinity_val = 0;
        }
        standardUncertinity_val = parseFloat(standardUncertinity_val).toFixed(4);
	row.find(".standardUncertinity").val(standardUncertinity_val);
        var standardUncertinityperc_val = (row.find(".standardUncertinity").val()*100/row.find(".stdMeterAverage").val());
        if(isNaN(standardUncertinityperc_val)) {
            standardUncertinityperc_val = 0;
        }
        standardUncertinityperc_val = parseFloat(standardUncertinityperc_val).toFixed(4);
	row.find(".standardUncertinityperc").val(standardUncertinityperc_val);
	row.find(".degreeOfFreedom").val(0);
        var uncertinty_from_tracebility = $('#uncertinty_from_tracebility').val();
        if(uncertinty_from_tracebility == ''){
            uncertinty_from_tracebility = 0;
        }
        var uncertinty_from_tracebility_val = parseFloat(uncertinty_from_tracebility).toFixed(4);
        var uncertinityForTypeB_val = ((uncertinty_from_tracebility_val / 2)*row.find(".stdMeterAverage").val()*0.01);
        if(isNaN(uncertinityForTypeB_val)) {
            uncertinityForTypeB_val = 0;
        }
        uncertinityForTypeB_val = parseFloat(uncertinityForTypeB_val).toFixed(4);
	row.find(".uncertinityForTypeB").val(uncertinityForTypeB_val);
	row.find(".uncertinityInPercentage").val(0);
        get_accuracy_taking_account();
        var accuracyForTypeB_val = ($("#accuracy_taking_account").val() * stdMeterAverageObj * 0.01 / Math.sqrt(3));
        if(isNaN(accuracyForTypeB_val)) {
            accuracyForTypeB_val = 0;
        }
        accuracyForTypeB_val = parseFloat(accuracyForTypeB_val).toFixed(4);
	row.find(".accuracyForTypeB").val(accuracyForTypeB_val);
	row.find(".acuuracyForTypeBPerc").val(0);
        var resolution_of_master = $("#resolution_of_master").val();
        if(resolution_of_master == ''){
            resolution_of_master = 0;
        }
        var resolution_of_master_val = parseFloat(resolution_of_master).toFixed(4);
        var resolutionTypeB_val  = ((resolution_of_master_val / 2)/Math.sqrt(3));
        if(isNaN(resolutionTypeB_val)) {
            resolutionTypeB_val = 0;
        }
        resolutionTypeB_val  = parseFloat(resolutionTypeB_val).toFixed(4);
	row.find(".resolutionTypeB").val(resolutionTypeB_val);
	
	row.find(".resolutionForTypeBPerc").val(0);
	row.find(".stabilityForTypeB").val(0);
	row.find(".stabilityForTypeBInPerc").val(0);
	row.find(".combinedUncertinityInPerc").val(0);
	row.find(".effectiveUncertinityInPer").val(0);
	row.find(".effectiveDegreeOfFreed").val(0);
	row.find(".masterMeterReading").val(0);
	row.find(".effectiveUncertinity").val(0);
	get_stability_of_source();
	var  standardUncertinityComined = row.find(".standardUncertinity").val();
	var  accuracyForTypeBCombined = row.find(".accuracyForTypeB").val();
	var  uncertinityForTypeBcombined = row.find(".uncertinityForTypeB").val();
	var  resolutionTypeBCombined = row.find(".resolutionTypeB").val();
	var  stability_of_source_mas = row.find(".stability_of_source").val();
	var combinedUncertinity_val = (Math.sqrt((standardUncertinityComined * standardUncertinityComined) + (accuracyForTypeBCombined * accuracyForTypeBCombined) + (uncertinityForTypeBcombined * uncertinityForTypeBcombined) + (resolutionTypeBCombined * resolutionTypeBCombined) + (stability_of_source_mas * stability_of_source_mas)));
        if(isNaN(combinedUncertinity_val)) {
            combinedUncertinity_val = 0;
        }
        combinedUncertinity_val = parseFloat(combinedUncertinity_val).toFixed(5);
	row.find(".combinedUncertinity").val(combinedUncertinity_val);
        var expandedUncertinity_val = 2*row.find(".combinedUncertinity").val();
        if(isNaN(expandedUncertinity_val)) {
            expandedUncertinity_val = 0;
        }
        expandedUncertinity_val = parseFloat(expandedUncertinity_val).toFixed(4);
	row.find(".expandedUncertinity").val(expandedUncertinity_val);
        var expandedUncertinityInPre_val = (2*row.find(".combinedUncertinity").val()).toFixed(4) * 100 / stdMeterAverageObj;
        if(isNaN(expandedUncertinityInPre_val)) {
            var expandedUncertinityInPre_val = 0;
        }
	row.find(".expandedUncertinityInPre").val(parseFloat(expandedUncertinityInPre_val).toFixed(4));
        var meanReading_val = row.find(".stdMeterAverage").val();
        if(isNaN(meanReading_val)) {
            meanReading_val = 0;
        }
        meanReading_val = parseFloat(meanReading_val).toFixed(4);
	row.find(".meanReading").val(meanReading_val);
        });
        
	// additional change by girish - partial end end
}

    function testMeterAvg(){
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            var row = $(this);
            var valuesAvailable = 0;

            var testMeterValTotal = 0;
            var testMeterValArr = [];
            row.find('.testMeter').each(function(ind,ele){
                    if($(this).val() != '') {
                            var tmp_val = parseFloat($(this).val()) || 0;
                            testMeterValTotal = testMeterValTotal + tmp_val;
                            valuesAvailable++;
                            testMeterValArr.push(tmp_val);
                    }
            });
            testAverageObj = testMeterValTotal / valuesAvailable;
            if(isNaN(testAverageObj)) {
                testAverageObj = 0;
            }
            testAverageObj = parseFloat(testAverageObj).toFixed(4);
            row.find(".testMeterAverage").val(testAverageObj);
        });
    }
    
    function get_accuracy_taking_account(){
        var accuracy_taking_account_val1 = $('#accuracy_taking_account').val();
        var accuracy_taking_account_val =  parseFloat(accuracy_taking_account_val1).toFixed(4);
        if(isNaN(accuracy_taking_account_val)) {
            accuracy_taking_account_val = 0;
        }
        if(accuracy_taking_account_val <= 0){
            var s_range1 = $('#masterMeterSubSubId').find(':selected').data('range');
            var s_range = parseFloat(s_range1);
            var accuracy_taking_account_val_new = (((($('.stdMeterAverage').val()*$('#accuracy_of_rdg').val())+(s_range*$('#accuracy_of_fs').val()))/100)*100)/$('.stdMeterAverage').val();
            var accuracy_taking_account_val_new1 =  parseFloat(accuracy_taking_account_val_new).toFixed(4);
            if(accuracy_taking_account_val_new == 'Infinity'){
                accuracy_taking_account_val_new1 = 0;
            }
            if(isNaN(accuracy_taking_account_val_new1)) {
                accuracy_taking_account_val_new1 = 0;
            }
{*            $('#accuracy_taking_account').val(accuracy_taking_account_val_new1);*}
            
            var accuracyForTypeB_val = (accuracy_taking_account_val_new1 * $('.stdMeterAverage').val() * 0.01 / Math.sqrt(3));
            if(isNaN(accuracyForTypeB_val)) {
                accuracyForTypeB_val = 0;
            }
            accuracyForTypeB_val = parseFloat(accuracyForTypeB_val).toFixed(4);
            $(".accuracyForTypeB").val(accuracyForTypeB_val);
        } 
    }
</script>
<table align="left" border='1' cellpadding='5' cellspacing='0'>
  <tbody id="mainDiv">
  <tr class="tableRow">
      <td valign="bottom" align="left" style="white-space: nowrap;"><br/><b>Load in (g) (<span id="box_count_filled"> </span>/<span id="uut_box_total"> </span>)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </b>
      	<input type="button" value=" + " class="std_add" />
  		<span class="std_values">
  			<span class="main_std main_std1"> 
                            <input type="button" value=" - " class="std_remove" /><input type="text" name="stdMeter[][]" class="stdMeter num_only" id="stdMeter_1" value="{$edit_reading_std_meter_1}"  size="4" align="right" title="stdMeter"  >
	  		</span>
  			<span class="main_std"> 
	      		<input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter" value="{$edit_reading_std_meter_2}" >
	  		</span>
  			<span class="main_std"> 
	      		<input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter" value="{$edit_reading_std_meter_3}" >
	  		</span>
  			<span class="main_std"> 
	      		<input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter" value="{$edit_reading_std_meter_4}"  >
	  		</span>
  			<span class="main_std"> 
                        <input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter" value="{$edit_reading_std_meter_5}" >
	  		</span>
  			<span class="main_std"> 
                        <input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter" value="{$edit_reading_std_meter_6}" >
	  		</span>
  			<span class="main_std"> 
                        <input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter" value="{$edit_reading_std_meter_7}" >
	  		</span>
  			<span class="main_std"> 
                        <input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter" value="{$edit_reading_std_meter_8}" >
	  		</span>
  			<span class="main_std"> 
                        <input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter" value="{$edit_reading_std_meter_9}" >
	  		</span>
  			<span class="main_std"> 
                        <input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter" value="{$edit_reading_std_meter_10}" >
	  		</span>
                        {if !empty($edit_reading_std_meter)}
                            {foreach from=$edit_reading_std_meter key=k item=det}
                                <span class="main_std"> 
                                    <input type="text" name="stdMeter[][]" class="stdMeter num_only"  size="4" align="right" title="stdMeter" value="{$det}" >
                                </span>
                            {/foreach}
                        {/if}
  		</span>
            <br/>
            <br/>
            <b>Location	(<span id="test_box_count_filled"> </span>/<span id="test_box_total"> </span>) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </b>
            <input type="button" value=" + " class="testMeter_add" />
            <span class="testMeter_values">
                <span class="main_testMeter1 main_testMeter"> 
                <input type="button" value=" - " class="testMeter_remove" /><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{$edit_reading_test_meter1_1}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{$edit_reading_test_meter1_2}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{$edit_reading_test_meter1_3}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{$edit_reading_test_meter1_4}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{$edit_reading_test_meter1_5}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{$edit_reading_test_meter1_6}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{$edit_reading_test_meter1_7}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{$edit_reading_test_meter1_8}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{$edit_reading_test_meter1_9}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{$edit_reading_test_meter1_10}"  >
                </span>
                {if !empty($edit_reading_test_meter1)}
                    {foreach from=$edit_reading_test_meter1 key=k item=det}
                        <span class="main_testMeter"> 
                            <input type="text" name="testMeter[][]" class="testMeter" size="4" align="right" title="testMeter"   value="{$det}">
                        </span>
                    {/foreach}
                {/if}
            </span>
            <br/>
            <br/>
            <b>Balance Reading in (g) (<span id="test_box_count_filled2"> </span>/<span id="test_box_total2"> </span>) </b>
            <input type="button" value=" + " class="testMeter2_add" />
            <span class="testMeter2_values">
                <span class="main_testMeter21 main_testMeter2"> 
                <input type="button" value=" - " class="testMeter2_remove" /><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{$edit_reading_test_meter2_1}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{$edit_reading_test_meter2_2}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{$edit_reading_test_meter2_3}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{$edit_reading_test_meter2_4}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{$edit_reading_test_meter2_5}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{$edit_reading_test_meter2_6}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{$edit_reading_test_meter2_7}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{$edit_reading_test_meter2_8}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{$edit_reading_test_meter2_9}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{$edit_reading_test_meter2_10}"  >
                </span>
                {if !empty($edit_reading_test_meter2)}
                    {foreach from=$edit_reading_test_meter2 key=k item=det}
                        <span class="main_testMeter2"> 
                            <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                        </span>
                    {/foreach}
                {/if}
            </span>
            <br/>
            <br/>
            
            {*<input type="button" id="view_calculation" value="View / Refresh Calculation" class="button" />
            <input type="button" id="hide_calculation" value="Hide Calculation" class="button" />*}
            <br/>
            <br/>
            
    </td>
  </tr>
  </tbody>
  <tr>
    <td align="center" colspan="28"><input type="submit" name="insertBtn" value="Submit" class="button" /></td>
  </tr>
</table>
  <span id="image_span"><img style="float: right" width="400px"  src="./images/table_info/table_16.png" /></span>
<div id="rangedisplay"></div> 
