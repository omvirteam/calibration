{include file="./headStart.tpl"}
<title>List Of Certificate</title>
<script type="text/javascript">
$(document).ready(function(){
  $(".link").click(function(){
    $("#certiList").attr("action","listOfCertificatePrint.php");
    $("#certiList").submit();
    
  });
});
function justClick()
{
	document.certiList.submit();
}
</script>
{include file="./headEnd.tpl"}
<form name="certiList" action="{$smarty.server.PHP_SELF}" id="certiList" method="post">
<table border="0" align="center">
<tr>
  <td>Customer :
    <select name="customerId" id="customerId" onchange="justClick();">
      <option value='0'>Select Name</option>
      {html_options values=$customerId output=$custName selected=$selectedCustomerId}
    </select>
  </td>
  <td>
    Status :
    <select name="status" onChange="justClick();">
    	{html_options values=$statusValue output=$statusName selected=$selectedStatusFired}
    </select>
  </td>
  <td><input type="hidden" name="getCertiData" value=" Search " /></td>
</tr>
</table>
</form>
<br />
{if $certiArray != 0}
  <table border='0' cellpadding='1' cellspacing='2' align='center'>
  <tr>
    <td class="table1" colspan="6">Company : {$selectedCustomer}</td>
    <td class="table1" colspan="3">Status : {$statusFired}</td>
  </tr>
  <tr>
    <td class="table1" align="center">Sr no.</td>
    <td class="table1" align="center">GRN</td>
    <td class="table1" align="center">Company</td>
    <td class="table1" align="center">Instrument</td>
    <td class="table1" align="center">Sr. No.</td>
    <td class="table1" align="center">Make</td>
    <td class="table1" align="center">Range</td>
    <td class="table1" align="center">Report No.</td>
	</tr>
  {section name="sec" loop=$certiArray}
  <tr>
    <td class="table2" align="center">{$smarty.section.sec.rownum}</td>
    <td class="table2">{$certiArray[sec].grnNo}</td>
    <td class="table2">{$certiArray[sec].custName}</td>
    <td class="table2">{$certiArray[sec].itemName}</td>
    <td class="table2">{$certiArray[sec].itemCode}</td>
    <td class="table2">{$certiArray[sec].makeModel}</td>
    <td class="table2">{$certiArray[sec].range}</td>
    <td class="table2" align="center">{$certiArray[sec].selfCertiNo}</td>
    </tr>
  {sectionelse}
     <tr><td colspan="9" align="center"><h1><font color="red"><b>No Record Found...!</b></font></h1></td></tr>
  {/section}
  {if $certiArray != 0}
    <tr><td align="center" colspan="22"><h4><a href="#" class="link" target="_blank"><font color="red"><b>Print</b></a></h4></font></td></tr>
  {/if}
  </table>
{/if}
{include file="footer.tpl"}