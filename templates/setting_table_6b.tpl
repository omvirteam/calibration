{include file="./headStart.tpl"}
<title>C.T.Class Setting</title>
{include file="./headEnd.tpl"}
<form name="classData" id="classData" method="post">

<table border='0' cellpadding='1' cellspacing='2' align='center' width="650px">
<tr width="650px">
  <td class="table1" colspan='11' align="center">Table 6B C.T.Class Setting</td>
</tr>
<tr >
  <td align="center" bgcolor="lightblue">C.T. Class</td>
  <td align="center" bgcolor="lightblue">1 Ratio Error</td>
  <td align="center" bgcolor="lightblue">1 Phase Error</td>
  <td align="center" bgcolor="lightblue">5 Ratio Error</td>
  <td align="center" bgcolor="lightblue">5 Phase Error</td>
  <td align="center" bgcolor="lightblue">20 Ratio Error</td>
  <td align="center" bgcolor="lightblue">20 Phase Error</td>
  <td align="center" bgcolor="lightblue">100 Ratio Error</td>
  <td align="center" bgcolor="lightblue">100 Phase Error</td>
  <td align="center" bgcolor="lightblue">120 Ratio Error</td>
  <td align="center" bgcolor="lightblue">120 Phase Error</td>
</tr>
{foreach $class_arr as $item}
<tr >
  <td align="center" ><input type="text" name="class_data[{$item.id}][ct_class]" size="10"        value="{$item.ct_class}"  ></td>
  <td align="center" ><input type="text" name="class_data[{$item.id}][1_ratio_error]" size="10"   value="{$item.1_ratio_error}"  ></td>
  <td align="center" ><input type="text" name="class_data[{$item.id}][1_phase_error]" size="10"   value="{$item.1_phase_error}"  ></td>
  <td align="center" ><input type="text" name="class_data[{$item.id}][5_ratio_error]" size="10"   value="{$item.5_ratio_error}"  ></td>
  <td align="center" ><input type="text" name="class_data[{$item.id}][5_phase_error]" size="10"   value="{$item.5_phase_error}"  ></td>
  <td align="center" ><input type="text" name="class_data[{$item.id}][20_ratio_error]" size="10"  value="{$item.20_ratio_error}"  ></td>
  <td align="center" ><input type="text" name="class_data[{$item.id}][20_phase_error]" size="10"  value="{$item.20_phase_error}"  ></td>
  <td align="center" ><input type="text" name="class_data[{$item.id}][100_ratio_error]" size="10" value="{$item.100_ratio_error}"  ></td>
  <td align="center" ><input type="text" name="class_data[{$item.id}][100_phase_error]" size="10" value="{$item.100_phase_error}"  ></td>
  <td align="center" ><input type="text" name="class_data[{$item.id}][120_ratio_error]" size="10" value="{$item.120_ratio_error}"  ></td>
  <td align="center" ><input type="text" name="class_data[{$item.id}][120_phase_error]" size="10" value="{$item.120_phase_error}"  ></td>
</tr>
{/foreach}
<tr>
  <td>
    <input type="submit" name="insertBtn" value="Update" class="button" />
  </td>
</tr>
</table>
<br/>
</form>