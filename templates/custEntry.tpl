{include file="./headStart.tpl"}
<title>Customer</title>
<form name="form1" action="{$smarty.server.PHP_SELF}" id="form1" method="post">
  <input type="hidden" name="customerIdSelected" value="{$customerIdSelected}">
<script type="text/javascript">
  $(document).ready(function() 
  {
    $('#custName').focus();
  });
function customerName(){
  if(document.form1.custName.value == '')
  {
    alert ("Please Enter your Name");
    return false;
  }
    return true;
  }
</script>
{include file="./headEnd.tpl"}
{if (have_access_role($smarty.const.MASTER_CUSTOMER_MODULE_ID,"add") && {!isset($smarty.get.customerId)}) || (have_access_role($smarty.const.MASTER_CUSTOMER_MODULE_ID,"edit") && {isset($smarty.get.customerId)})}
<table border = "1" align="center" cellpadding="1" cellspacing="1">
<tr>
  <td align="center" class="table1" colspan="2">Customer Entry</td>
</tr>
<tr>
  <td align="right">Customer Name</td>
  <td><input type="text" value="{$custNameSelected}" name="custName" id="custName"></td>
</tr>
<tr>
  <td align="right">Customer Code</td>
  <td><input type="text" value="{$custCodeSelected}" name="custCode" id="custCode"></td>
</tr>
<tr>
  <td align="right">Customer Reference Name</td>
  <td><input type="text" value="{$custRefNameSelected}" name="custRefName" id="custRefName"></td>
</tr>
<tr>
  <td align="right">Address</td>
  <td><textarea rows="3" cols="25" name="address" id="address">{$addressSelected}</textarea></td>
</tr>
<tr>
  <td align="right">City</td>
  <td><input type="text" value="{$citySelected}" name="city"></td>
</tr>
<tr>
  <td align="right">State</td>
  <td><input type="text" value="{$stateSelected}" name="state"></td>
</tr>
<tr>
  <td align="right">Country</td>
  <td><input type="text" value="{$countrySelected}" name="country"></td>
</tr>
<tr>
  <td align="right">Fax</td>
  <td><input type="text" value="{$faxSelected}" name="fax"></td>
</tr>
<tr>
  <td align="right">Phone 1</td>
  <td><input type="text" value="{$phone1Selected}" name="phone1"></td>
</tr>
<tr>
  <td align="right">Phone 2</td>
  <td><input type="text" value="{$phone2Selected}" name="phone2"></td>
</tr>
<tr>
  <td align="right">Email</td>
  <td><input type="text" value="{$emailSelected}" name="email"></td>
</tr>
<tr>
  <td align="right">Web</td>
  <td><input type="text" value="{$webSelected}" name="web"></td>
</tr>
<tr>
  <td align="right">Contact Person</td>
  <td><input type="text" value="{$contPersonSelected}" name="contPerson"></td>
</tr>
<tr>
  <td align="center" colspan="2"><input type="submit" name="insertBtn" value="Submit" class="button" Onclick="return customerName();" />
                                 <input type="submit" value="SEARCH" name="searchBtn" class="button"  />
                                 <input type="submit" value="CANCEL" name="cancelBtn" class="button" />
  </td>
</tr>
</table>
{/if} 
</form>
{include file="footer.tpl"}