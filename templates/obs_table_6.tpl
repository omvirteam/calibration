<script type="text/javascript">
$(document).ready(function(){
    
        $(document).on('change',"#ct_class",function(){
            if($(this).val() == '1' || $(this).val() == '2' || $(this).val() == '3' || $(this).val() == '4'){
                $('.1_per_entry').hide();
            } else {
                $('.1_per_entry').show();
            }
            
        });
        
        $(document).on('click',"#view_calculation",function(){
            testMeterAvg();
            meanReading();
            $('#calculation_table').show();
        });
        $(document).on('click',"#hide_calculation",function(){
            $('#calculation_table').hide();
        });
        $(document).on('change',"#table_info_id",function(){
            var image_url = $(this).find(':selected').data('image_url');
            if(typeof(image_url) !== 'undefined'){
                $('#image_span').html('<img style="float: right" width="400px"  src=".'+image_url+'" />');
            } else {
                $('#image_span').html('');
            }
        });
	

    $(document).on('click',".add",function(){
            var tr_obj = $(this).closest('.tableRow');
            var row_template = $("#row_template").clone(true)
            row_template.find('input[type="text"]').val('');
            row_template.removeAttr("id");
            row_template.removeAttr("style");
            row_template.insertAfter(tr_obj);
    });
        
        $(document).on('click',".delete",function(){
            $(this).parent().parent().parent().remove();
            return false;
	});
         
        $(document).on('click',".testMeter_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter:last').hasClass("main_testMeter1")) {
            } else {
                td_obj.find('.main_testMeter:last').remove();
            }
            master_box_total_count();
	});
        
  	$(".testMeter_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html = '<span class="main_testMeter"> <input type="text" name="testMeter[][]" class="testMeter" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter_values").append(main_testMeter_html);
            master_box_total_count();
  	});
        
  	$(document).on('keydown',".testMeter",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter:last').val() != '') {
                            var last_main_testMeter = td_obj.find('.main_testMeter:last');
                            var main_testMeter_html = '<span class="main_testMeter">&nbsp;<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   ></span>';
                            $(main_testMeter_html).insertAfter(last_main_testMeter);
                    }
                }
            }
            master_box_total_count();
  	});
        
        $(document).on('click',".testMeter2_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter2:last').hasClass("main_testMeter21")) {
            } else {
                td_obj.find('.main_testMeter2:last').remove();
            }
            master_box_total_count2();
	});
        
  	$(".testMeter2_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html2 = '<span class="main_testMeter2"> <input type="text" name="testMeter2[][]" class="testMeter2" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter2_values").append(main_testMeter_html2);
            master_box_total_count2();
  	});
        
  	$(document).on('keydown',".testMeter2",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter2:last').val() != '') {
                            var last_main_testMeter2 = td_obj.find('.main_testMeter2:last');
                            var main_testMeter_html2 = '<span class="main_testMeter2">&nbsp;<input type="text" name="testMeter2[][]" class="testMeter2 num_only" size="4" align="right" title="testMeter"   ></span>';
                            $(main_testMeter_html2).insertAfter(last_main_testMeter2);
                    }
                }
            }
            master_box_total_count2();
  	});
        
        $(document).on('click',".testMeter3_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter3:last').hasClass("main_testMeter31")) {
            } else {
                td_obj.find('.main_testMeter3:last').remove();
            }
            master_box_total_count3();
	});
        
  	$(".testMeter3_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html3 = '<span class="main_testMeter3"> <input type="text" name="testMeter3[][]" class="testMeter3" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter3_values").append(main_testMeter_html3);
            master_box_total_count3();
  	});
        
  	$(document).on('keydown',".testMeter3",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter3:last').val() != '') {
                            var last_main_testMeter3 = td_obj.find('.main_testMeter3:last');
                            var main_testMeter_html3 = '<span class="main_testMeter3">&nbsp;<input type="text" name="testMeter3[][]" class="testMeter3 num_only" size="4" align="right" title="testMeter"   ></span>';
                            $(main_testMeter_html3).insertAfter(last_main_testMeter3);
                    }
                }
            }
            master_box_total_count3();
  	});
        
        $(document).on('click',".testMeter4_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter4:last').hasClass("main_testMeter41")) {
            } else {
                td_obj.find('.main_testMeter4:last').remove();
            }
            master_box_total_count4();
	});
        
  	$(".testMeter4_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html4 = '<span class="main_testMeter4"> <input type="text" name="testMeter4[][]" class="testMeter4" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter4_values").append(main_testMeter_html4);
            master_box_total_count4();
  	});
        
  	$(document).on('keydown',".testMeter4",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter4:last').val() != '') {
                            var last_main_testMeter4 = td_obj.find('.main_testMeter4:last');
                            var main_testMeter_html4 = '<span class="main_testMeter4">&nbsp;<input type="text" name="testMeter4[][]" class="testMeter4 num_only" size="4" align="right" title="testMeter"   ></span>';
                            $(main_testMeter_html4).insertAfter(last_main_testMeter4);
                    }
                }
            }
            master_box_total_count4();
  	});
        
        $(document).on('click',".testMeter5_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter5:last').hasClass("main_testMeter51")) {
            } else {
                td_obj.find('.main_testMeter5:last').remove();
            }
            master_box_total_count5();
	});
        
  	$(".testMeter5_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html5 = '<span class="main_testMeter5"> <input type="text" name="testMeter5[][]" class="testMeter5" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter5_values").append(main_testMeter_html5);
            master_box_total_count5();
  	});
        
  	$(document).on('keydown',".testMeter5",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter5:last').val() != '') {
                            var last_main_testMeter5 = td_obj.find('.main_testMeter5:last');
                            var main_testMeter_html5 = '<span class="main_testMeter5">&nbsp;<input type="text" name="testMeter5[][]" class="testMeter5 num_only" size="4" align="right" title="testMeter"   ></span>';
                            $(main_testMeter_html5).insertAfter(last_main_testMeter5);
                    }
                }
            }
            master_box_total_count5();
  	});
        
        $(document).on('click',".testMeter6_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter6:last').hasClass("main_testMeter61")) {
            } else {
                td_obj.find('.main_testMeter6:last').remove();
            }
            master_box_total_count6();
	});
        
  	$(".testMeter6_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html6 = '<span class="main_testMeter6"> <input type="text" name="testMeter6[][]" class="testMeter6" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter6_values").append(main_testMeter_html6);
            master_box_total_count6();
  	});
        
  	$(document).on('keydown',".testMeter6",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter6:last').val() != '') {
                            var last_main_testMeter6 = td_obj.find('.main_testMeter6:last');
                            var main_testMeter_html6 = '<span class="main_testMeter6">&nbsp;<input type="text" name="testMeter6[][]" class="testMeter6 num_only" size="4" align="right" title="testMeter"   ></span>';
                            $(main_testMeter_html6).insertAfter(last_main_testMeter6);
                    }
                }
            }
            master_box_total_count6();
  	});
        
        $(document).on('click',".testMeter7_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter7:last').hasClass("main_testMeter71")) {
            } else {
                td_obj.find('.main_testMeter7:last').remove();
            }
            master_box_total_count7();
	});
        
  	$(".testMeter7_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html7 = '<span class="main_testMeter7"> <input type="text" name="testMeter7[][]" class="testMeter7" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter7_values").append(main_testMeter_html7);
            master_box_total_count7();
  	});
        
  	$(document).on('keydown',".testMeter7",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter7:last').val() != '') {
                            var last_main_testMeter7 = td_obj.find('.main_testMeter7:last');
                            var main_testMeter_html7 = '<span class="main_testMeter7">&nbsp;<input type="text" name="testMeter7[][]" class="testMeter7 num_only" size="4" align="right" title="testMeter"   ></span>';
                            $(main_testMeter_html7).insertAfter(last_main_testMeter7);
                    }
                }
            }
            master_box_total_count7();
  	});
        
        $(document).on('click',".testMeter8_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter8:last').hasClass("main_testMeter81")) {
            } else {
                td_obj.find('.main_testMeter8:last').remove();
            }
            master_box_total_count8();
	});
        
  	$(".testMeter8_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html8 = '<span class="main_testMeter8"> <input type="text" name="testMeter8[][]" class="testMeter8" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter8_values").append(main_testMeter_html8);
            master_box_total_count8();
  	});
        
  	$(document).on('keydown',".testMeter8",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter8:last').val() != '') {
                            var last_main_testMeter8 = td_obj.find('.main_testMeter8:last');
                            var main_testMeter_html8 = '<span class="main_testMeter8">&nbsp;<input type="text" name="testMeter8[][]" class="testMeter8 num_only" size="4" align="right" title="testMeter"   ></span>';
                            $(main_testMeter_html8).insertAfter(last_main_testMeter8);
                    }
                }
            }
            master_box_total_count8();
  	});
        
        $(document).on('click',".testMeter9_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter9:last').hasClass("main_testMeter91")) {
            } else {
                td_obj.find('.main_testMeter9:last').remove();
            }
            master_box_total_count9();
	});
        
  	$(".testMeter9_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html9 = '<span class="main_testMeter9"> <input type="text" name="testMeter9[][]" class="testMeter9" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter9_values").append(main_testMeter_html9);
            master_box_total_count9();
  	});
        
  	$(document).on('keydown',".testMeter9",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 9) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter9:last').val() != '') {
                            var last_main_testMeter9 = td_obj.find('.main_testMeter9:last');
                            var main_testMeter_html9 = '<span class="main_testMeter9">&nbsp;<input type="text" name="testMeter9[][]" class="testMeter9 num_only" size="4" align="right" title="testMeter"   ></span>';
                            $(main_testMeter_html9).insertAfter(last_main_testMeter9);
                    }
                }
            }
            master_box_total_count9();
  	});
        
        $(document).on('click',".testMeter10_remove",function(){
            var td_obj = $(this).closest('td');
            if (td_obj.find('.main_testMeter10:last').hasClass("main_testMeter101")) {
            } else {
                td_obj.find('.main_testMeter10:last').remove();
            }
            master_box_total_count10();
	});
        
  	$(".testMeter10_add").on('click',function(){
            var td_obj = $(this).closest('td');
            var main_testMeter_html10 = '<span class="main_testMeter10"> <input type="text" name="testMeter10[][]" class="testMeter10" size="4" align="right" title="testMeter"   ></span>';
            td_obj.find(".testMeter10_values").append(main_testMeter_html10);
            master_box_total_count10();
  	});
        
  	$(document).on('keydown',".testMeter10",function(e){
            var keyCode = e.keyCode || e.which; 
            if (keyCode == 10) {
                if($(this).val() != ''){
                    var td_obj = $(this).closest('td');
                    if(td_obj.find('.testMeter10:last').val() != '') {
                            var last_main_testMeter10 = td_obj.find('.main_testMeter10:last');
                            var main_testMeter_html10 = '<span class="main_testMeter10">&nbsp;<input type="text" name="testMeter10[][]" class="testMeter10 num_only" size="4" align="right" title="testMeter"   ></span>';
                            $(main_testMeter_html10).insertAfter(last_main_testMeter10);
                    }
                }
            }
            master_box_total_count10();
  	});

  	$(document).on('input', ".num_only", function () {
        this.value = this.value.replace(/[^\d\.\-]/g, '');
    });
    master_box_total_count();
    master_box_total_count2();
    master_box_total_count3();
    master_box_total_count4();
    master_box_total_count5();
    master_box_total_count6();
    master_box_total_count7();
    master_box_total_count8();
    master_box_total_count9();
    master_box_total_count10();
});


function master_box_total_count(){
    var test_box_count = 0;
    var test_box_count_filled = 0;
    $('tr.tableRow:visible').find('.testMeter').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled++;
        }
        test_box_count++;
    });
    $('#test_box_total').html(test_box_count);
    $('#test_box_count_filled').html(test_box_count_filled);
}

function master_box_total_count2(){
    var test_box_count2 = 0;
    var test_box_count_filled2 = 0;
    $('tr.tableRow:visible').find('.testMeter2').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled2++;
        }
        test_box_count2++;
    });
    $('#test_box_total2').html(test_box_count2);
    $('#test_box_count_filled2').html(test_box_count_filled2);
}

function master_box_total_count3(){
    var test_box_count3 = 0;
    var test_box_count_filled3 = 0;
    $('tr.tableRow:visible').find('.testMeter3').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled3++;
        }
        test_box_count3++;
    });
    $('#test_box_total3').html(test_box_count3);
    $('#test_box_count_filled3').html(test_box_count_filled3);
}

function master_box_total_count4(){
    var test_box_count4 = 0;
    var test_box_count_filled4 = 0;
    $('tr.tableRow:visible').find('.testMeter4').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled4++;
        }
        test_box_count4++;
    });
    $('#test_box_total4').html(test_box_count4);
    $('#test_box_count_filled4').html(test_box_count_filled4);
}

function master_box_total_count5(){
    var test_box_count5 = 0;
    var test_box_count_filled5 = 0;
    $('tr.tableRow:visible').find('.testMeter5').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled5++;
        }
        test_box_count5++;
    });
    $('#test_box_total5').html(test_box_count5);
    $('#test_box_count_filled5').html(test_box_count_filled5);
}
function master_box_total_count6(){
    var test_box_count6 = 0;
    var test_box_count_filled6 = 0;
    $('tr.tableRow:visible').find('.testMeter6').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled6++;
        }
        test_box_count6++;
    });
    $('#test_box_total6').html(test_box_count6);
    $('#test_box_count_filled6').html(test_box_count_filled6);
}
function master_box_total_count7(){
    var test_box_count7 = 0;
    var test_box_count_filled7 = 0;
    $('tr.tableRow:visible').find('.testMeter7').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled7++;
        }
        test_box_count7++;
    });
    $('#test_box_total7').html(test_box_count7);
    $('#test_box_count_filled7').html(test_box_count_filled7);
}
function master_box_total_count8(){
    var test_box_count8 = 0;
    var test_box_count_filled8 = 0;
    $('tr.tableRow:visible').find('.testMeter8').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled8++;
        }
        test_box_count8++;
    });
    $('#test_box_total8').html(test_box_count8);
    $('#test_box_count_filled8').html(test_box_count_filled8);
}
function master_box_total_count9(){
    var test_box_count9 = 0;
    var test_box_count_filled9 = 0;
    $('tr.tableRow:visible').find('.testMeter9').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled9++;
        }
        test_box_count9++;
    });
    $('#test_box_total9').html(test_box_count9);
    $('#test_box_count_filled9').html(test_box_count_filled9);
}
function master_box_total_count10(){
    var test_box_count10 = 0;
    var test_box_count_filled10 = 0;
    $('tr.tableRow:visible').find('.testMeter10').each(function(ind,ele){
        if($(this).val() != '') {
            test_box_count_filled10++;
        }
        test_box_count10++;
    });
    $('#test_box_total10').html(test_box_count10);
    $('#test_box_count_filled10').html(test_box_count_filled10);
}

function get_table_title(){
    var datastring = "grnDetailId=" + $("#grnDetailId").val() + "&table_no=" + $("#table_no").val();
    $.ajax({
        url: "./obsSheetAjGrnDetailChange.php",
        data: datastring,
        datatype:'json',
        success: function(data){
            var json = $.parseJSON(data);
            $('#table_title').val('');
            $('#table_title').val(json);
        }
    });
}
function getMasterMeterData()
{
	var dataString = "masterMeterId=" + $("#masterMeterId").val();
	$.ajax({
	  type: "GET",
	  url:  "masterMeterJq.php",
	  data: dataString,
	  success: function(data){ 
	    $("#masterpara").html(data);
	    $("#masterMeterSubId").focus();
	    
	    // Ajax Call At Range :Start 
	    $("#masterMeterSubId").change(function(){ 
	      var dataString = "masterMeterSubId=" + $("#masterMeterSubId").val();
	      $.ajax({
	        type: "GET",
	        url:  "masterMeterJqSub.php",
	        data: dataString,
	        success: function(data){
	           $("#masterparasub").html(data);
	           $("#masterMeterSubSubId").focus();
	          	// Get accuracysub With Ajax  :Start 
	          	$("#masterMeterSubSubId").change(function(){ 
	         	  var dataString = "masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
	         	  var dataString2 = "masterMeterSubSubId_2=" + $("#masterMeterSubSubId").val();
	       	    $.ajax({
		       	    type: "GET",
		       	    url:  "masterMeterJqSubAccuracy.php",
		       	    data: dataString,
		       	    success: function(data1){ 
		       	    	$("#rangedisplay").html(data1);
	       	      	}
	       	    });
	       	    $.ajax({
		       	    type: "GET",
		       	    url:  "masterMeterJqSubAccuracy.php",
		       	    data: dataString2,
		       	    success: function(data1){ 
                                var json = $.parseJSON(data1);
                                $("#accuracy_of_rdg").val(json.accuracySubSub);
                                $("#accuracy_of_fs").val(json.accuracySubSubFS);
                                $("#master_stability").val(json.stability);
                                $("#degree_of_freedom").val(json.degreeOfFreedom);
                                $("#uncertinty_from_tracebility").val(json.uncertinty);
                                $("#resolution_of_master").val(json.resolution);
                                $("#accuracy_taking_account").val(json.accuracy_main);
	       	      	}
	       	    });
	          });
	        }	
	      });
	    });
	    // Get accuracysub With Ajax: End
	  }
	});
        
}

function get_stability_of_source(){
    var master_stability_val_s = $('#master_stability').val();
    var stdMeterAverage_val_s = $('.stdMeterAverage').val();
    if(master_stability_val_s == ''){
        master_stability_val_s = 0;
    }
    var master_stability_val_ss = parseFloat(master_stability_val_s).toFixed(4);

    if(stdMeterAverage_val_s == ''){
        stdMeterAverage_val_s = 0;
    }
    var stdMeterAverage_val_ss = parseFloat(stdMeterAverage_val_s).toFixed(4);

    var stability_of_source = (master_stability_val_ss * stdMeterAverage_val_ss)*0.01/Math.sqrt(3);
    if(isNaN(stability_of_source)){
        stability_of_source = 0;
    }
    var stability_of_source_s = parseFloat(stability_of_source).toFixed(4);
    $('.stability_of_source').val(stability_of_source_s);
}
function meanReading(){
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
	var valuesAvailable = 0;
	
	var stdMeterValTotal = 0;
	var stdMeterValArr = [];
	row.find('.stdMeter').each(function(ind,ele){
		if($(this).val() != '') {
			var tmp_val1 = parseFloat($(this).val()) || 0;
			var tmp_val = parseFloat(tmp_val1).toFixed(4);
			stdMeterValTotal = parseFloat(stdMeterValTotal) + parseFloat(tmp_val);
			valuesAvailable++;
			stdMeterValArr.push(tmp_val);
		}
	});

	var stdMeterValTotal1 = parseFloat(stdMeterValTotal).toFixed(4);
	stdMeterAverageObj = stdMeterValTotal1 / valuesAvailable;
	if(isNaN(stdMeterAverageObj)){
            stdMeterAverageObj = 0;
        }
	row.find(".stdMeterAverage").val(stdMeterAverageObj.toFixed(4));
        var accuracyTaken_val = ((stdMeterAverageObj*($("#masterMeterAccuracyArray").val())+($("#masterMeterAccuracyRangeArray").val())*($("#masterMeterAccuracyFSArray").val()))/stdMeterAverageObj);
	if(isNaN(accuracyTaken_val)) {
            accuracyTaken_val = 0;
        }
        accuracyTaken_val = parseFloat(accuracyTaken_val).toFixed(4);
        row.find(".accuracyTaken").val(accuracyTaken_val);
	stdMeterAverageDev = stdMeterAverageObj;
	
	var stdMeterValDeviationTotal = 0;
	$.each(stdMeterValArr,function(ind,tmp_value){
		var tmpDeviation = (parseFloat(tmp_value) - parseFloat(stdMeterAverageDev)) * (parseFloat(tmp_value) - parseFloat(stdMeterAverageDev));
		stdMeterValDeviationTotal = parseFloat(stdMeterValDeviationTotal) + parseFloat(tmpDeviation);
	});
        var standardDeviation_val = (Math.sqrt(stdMeterValDeviationTotal / (valuesAvailable - 1)));
	if(isNaN(standardDeviation_val)) {
            standardDeviation_val = 0;
        }
        standardDeviation_val = parseFloat(standardDeviation_val).toFixed(4);
	row.find(".standardDeviation").val(standardDeviation_val);
	
	standardUncertinity = Math.sqrt(stdMeterValDeviationTotal / (valuesAvailable));
        var valuesAvailable = 0;
	row.find('.stdMeter').each(function(ind,ele){
		if($(this).val() != '') {
			valuesAvailable++;
		}
	});
        var standardUncertinity_val = (standardDeviation_val/Math.sqrt(valuesAvailable));
        if(isNaN(standardUncertinity_val)) {
            standardUncertinity_val = 0;
        }
        standardUncertinity_val = parseFloat(standardUncertinity_val).toFixed(4);
	row.find(".standardUncertinity").val(standardUncertinity_val);
        var standardUncertinityperc_val = (row.find(".standardUncertinity").val()*100/row.find(".stdMeterAverage").val());
        if(isNaN(standardUncertinityperc_val)) {
            standardUncertinityperc_val = 0;
        }
        standardUncertinityperc_val = parseFloat(standardUncertinityperc_val).toFixed(4);
	row.find(".standardUncertinityperc").val(standardUncertinityperc_val);
	row.find(".degreeOfFreedom").val(0);
        var uncertinty_from_tracebility = $('#uncertinty_from_tracebility').val();
        if(uncertinty_from_tracebility == ''){
            uncertinty_from_tracebility = 0;
        }
        var uncertinty_from_tracebility_val = parseFloat(uncertinty_from_tracebility).toFixed(4);
        var uncertinityForTypeB_val = ((uncertinty_from_tracebility_val / 2)*row.find(".stdMeterAverage").val()*0.01);
        if(isNaN(uncertinityForTypeB_val)) {
            uncertinityForTypeB_val = 0;
        }
        uncertinityForTypeB_val = parseFloat(uncertinityForTypeB_val).toFixed(4);
	row.find(".uncertinityForTypeB").val(uncertinityForTypeB_val);
	row.find(".uncertinityInPercentage").val(0);
        get_accuracy_taking_account();
        var accuracyForTypeB_val = ($("#accuracy_taking_account").val() * stdMeterAverageObj * 0.01 / Math.sqrt(3));
        if(isNaN(accuracyForTypeB_val)) {
            accuracyForTypeB_val = 0;
        }
        accuracyForTypeB_val = parseFloat(accuracyForTypeB_val).toFixed(4);
	row.find(".accuracyForTypeB").val(accuracyForTypeB_val);
	row.find(".acuuracyForTypeBPerc").val(0);
        var resolution_of_master = $("#resolution_of_master").val();
        if(resolution_of_master == ''){
            resolution_of_master = 0;
        }
        var resolution_of_master_val = parseFloat(resolution_of_master).toFixed(4);
        var resolutionTypeB_val  = ((resolution_of_master_val / 2)/Math.sqrt(3));
        if(isNaN(resolutionTypeB_val)) {
            resolutionTypeB_val = 0;
        }
        resolutionTypeB_val  = parseFloat(resolutionTypeB_val).toFixed(4);
	row.find(".resolutionTypeB").val(resolutionTypeB_val);
	
	row.find(".resolutionForTypeBPerc").val(0);
	row.find(".stabilityForTypeB").val(0);
	row.find(".stabilityForTypeBInPerc").val(0);
	row.find(".combinedUncertinityInPerc").val(0);
	row.find(".effectiveUncertinityInPer").val(0);
	row.find(".effectiveDegreeOfFreed").val(0);
	row.find(".masterMeterReading").val(0);
	row.find(".effectiveUncertinity").val(0);
	get_stability_of_source();
	var  standardUncertinityComined = row.find(".standardUncertinity").val();
	var  accuracyForTypeBCombined = row.find(".accuracyForTypeB").val();
	var  uncertinityForTypeBcombined = row.find(".uncertinityForTypeB").val();
	var  resolutionTypeBCombined = row.find(".resolutionTypeB").val();
	var  stability_of_source_mas = row.find(".stability_of_source").val();
	var combinedUncertinity_val = (Math.sqrt((standardUncertinityComined * standardUncertinityComined) + (accuracyForTypeBCombined * accuracyForTypeBCombined) + (uncertinityForTypeBcombined * uncertinityForTypeBcombined) + (resolutionTypeBCombined * resolutionTypeBCombined) + (stability_of_source_mas * stability_of_source_mas)));
        if(isNaN(combinedUncertinity_val)) {
            combinedUncertinity_val = 0;
        }
        combinedUncertinity_val = parseFloat(combinedUncertinity_val).toFixed(5);
	row.find(".combinedUncertinity").val(combinedUncertinity_val);
        var expandedUncertinity_val = 2*row.find(".combinedUncertinity").val();
        if(isNaN(expandedUncertinity_val)) {
            expandedUncertinity_val = 0;
        }
        expandedUncertinity_val = parseFloat(expandedUncertinity_val).toFixed(4);
	row.find(".expandedUncertinity").val(expandedUncertinity_val);
        var expandedUncertinityInPre_val = (2*row.find(".combinedUncertinity").val()).toFixed(4) * 100 / stdMeterAverageObj;
        if(isNaN(expandedUncertinityInPre_val)) {
            var expandedUncertinityInPre_val = 0;
        }
	row.find(".expandedUncertinityInPre").val(parseFloat(expandedUncertinityInPre_val).toFixed(4));
        var meanReading_val = row.find(".stdMeterAverage").val();
        if(isNaN(meanReading_val)) {
            meanReading_val = 0;
        }
        meanReading_val = parseFloat(meanReading_val).toFixed(4);
	row.find(".meanReading").val(meanReading_val);
        });
        
	// additional change by girish - partial end end
}

    function testMeterAvg(){
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            var row = $(this);
            var valuesAvailable = 0;

            var testMeterValTotal = 0;
            var testMeterValArr = [];
            row.find('.testMeter').each(function(ind,ele){
                    if($(this).val() != '') {
                            var tmp_val = parseFloat($(this).val()) || 0;
                            testMeterValTotal = testMeterValTotal + tmp_val;
                            valuesAvailable++;
                            testMeterValArr.push(tmp_val);
                    }
            });
            testAverageObj = testMeterValTotal / valuesAvailable;
            if(isNaN(testAverageObj)) {
                testAverageObj = 0;
            }
            testAverageObj = parseFloat(testAverageObj).toFixed(4);
            row.find(".testMeterAverage").val(testAverageObj);
        });
    }
    
    function get_accuracy_taking_account(){
        var accuracy_taking_account_val1 = $('#accuracy_taking_account').val();
        var accuracy_taking_account_val =  parseFloat(accuracy_taking_account_val1).toFixed(4);
        if(isNaN(accuracy_taking_account_val)) {
            accuracy_taking_account_val = 0;
        }
        if(accuracy_taking_account_val <= 0){
            var s_range1 = $('#masterMeterSubSubId').find(':selected').data('range');
            var s_range = parseFloat(s_range1);
            var accuracy_taking_account_val_new = (((($('.stdMeterAverage').val()*$('#accuracy_of_rdg').val())+(s_range*$('#accuracy_of_fs').val()))/100)*100)/$('.stdMeterAverage').val();
            var accuracy_taking_account_val_new1 =  parseFloat(accuracy_taking_account_val_new).toFixed(4);
            if(accuracy_taking_account_val_new == 'Infinity'){
                accuracy_taking_account_val_new1 = 0;
            }
            if(isNaN(accuracy_taking_account_val_new1)) {
                accuracy_taking_account_val_new1 = 0;
            }
{*            $('#accuracy_taking_account').val(accuracy_taking_account_val_new1);*}
            
            var accuracyForTypeB_val = (accuracy_taking_account_val_new1 * $('.stdMeterAverage').val() * 0.01 / Math.sqrt(3));
            if(isNaN(accuracyForTypeB_val)) {
                accuracyForTypeB_val = 0;
            }
            accuracyForTypeB_val = parseFloat(accuracyForTypeB_val).toFixed(4);
            $(".accuracyForTypeB").val(accuracyForTypeB_val);
        } 
    }
</script>
<table align="left" border='1' cellpadding='5' cellspacing='0'>
  <tbody id="mainDiv">
  <tr class="tableRow">
      <td valign="bottom" align="left" style="white-space: nowrap;">
          <b>120% Ratio (<span id="test_box_count_filled5"> </span>/<span id="test_box_total5"> </span>)</b>
            <input type="button" value=" + " class="testMeter5_add" />
            <span class="testMeter5_values">
                <span class="main_testMeter51 main_testMeter5"> 
                <input type="button" value=" - " class="testMeter5_remove" /><input type="text" name="testMeter5[][]" id="testMeter5_1" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter5.0)}{$edit_test_meter5.0}{/if}"  >
                </span>
                <span class="main_testMeter5"> 
                <input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter5.1)}{$edit_test_meter5.1}{/if}"  >
                </span>
                <span class="main_testMeter5"> 
                <input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter5.2)}{$edit_test_meter5.2}{/if}"  >
                </span>
                <span class="main_testMeter5"> 
                <input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter5.3)}{$edit_test_meter5.3}{/if}"  >
                </span>
                <span class="main_testMeter5"> 
                <input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter5.4)}{$edit_test_meter5.4}{/if}"  >
                </span>
                {if isset($edit_test_meter5.0)}{{$edit_test_meter5=$edit_test_meter5|array_diff_key:(['0']|array_flip)}}{/if}
                {if isset($edit_test_meter5.1)}{{$edit_test_meter5=$edit_test_meter5|array_diff_key:(['1']|array_flip)}}{/if}
                {if isset($edit_test_meter5.2)}{{$edit_test_meter5=$edit_test_meter5|array_diff_key:(['2']|array_flip)}}{/if}
                {if isset($edit_test_meter5.3)}{{$edit_test_meter5=$edit_test_meter5|array_diff_key:(['3']|array_flip)}}{/if}
                {if isset($edit_test_meter5.4)}{{$edit_test_meter5=$edit_test_meter5|array_diff_key:(['4']|array_flip)}}{/if}
                {if !empty($edit_test_meter5)}
                    {foreach from=$edit_test_meter5 key=k item=det}
                        <span class="main_testMeter5"> 
                            <input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                        </span>
                    {/foreach}
                {/if}
            </span>
             <br/>
             <br/>
          <b>100% Ratio (<span id="test_box_count_filled4"> </span>/<span id="test_box_total4"> </span>)</b>
            <input type="button" value=" + " class="testMeter4_add" />
            <span class="testMeter4_values">
                <span class="main_testMeter41 main_testMeter4"> 
                <input type="button" value=" - " class="testMeter4_remove" /><input type="text" name="testMeter4[][]" id="testMeter4_1" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter4.0)}{$edit_test_meter4.0}{/if}"  >
                </span>
                <span class="main_testMeter4"> 
                <input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter4.1)}{$edit_test_meter4.1}{/if}"  >
                </span>
                <span class="main_testMeter4"> 
                <input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter4.2)}{$edit_test_meter4.2}{/if}"  >
                </span>
                <span class="main_testMeter4"> 
                <input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter4.3)}{$edit_test_meter4.3}{/if}"  >
                </span>
                <span class="main_testMeter4"> 
                <input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter4.4)}{$edit_test_meter4.4}{/if}"  >
                </span>
                {if isset($edit_test_meter4.0)}{{$edit_test_meter4=$edit_test_meter4|array_diff_key:(['0']|array_flip)}}{/if}
                {if isset($edit_test_meter4.1)}{{$edit_test_meter4=$edit_test_meter4|array_diff_key:(['1']|array_flip)}}{/if}
                {if isset($edit_test_meter4.2)}{{$edit_test_meter4=$edit_test_meter4|array_diff_key:(['2']|array_flip)}}{/if}
                {if isset($edit_test_meter4.3)}{{$edit_test_meter4=$edit_test_meter4|array_diff_key:(['3']|array_flip)}}{/if}
                {if isset($edit_test_meter4.4)}{{$edit_test_meter4=$edit_test_meter4|array_diff_key:(['4']|array_flip)}}{/if}
                {if !empty($edit_test_meter4)}
                    {foreach from=$edit_test_meter4 key=k item=det}
                        <span class="main_testMeter4"> 
                            <input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                        </span>
                    {/foreach}
                {/if}
            </span>
             <br/>
             <br/>
          <b> &nbsp;&nbsp;20% Ratio (<span id="test_box_count_filled3"> </span>/<span id="test_box_total3"> </span>)</b>
            <input type="button" value=" + " class="testMeter3_add" />
            <span class="testMeter3_values">
                <span class="main_testMeter31 main_testMeter3"> 
                <input type="button" value=" - " class="testMeter3_remove" /><input type="text" name="testMeter3[][]" id="testMeter3_1" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter3.0)}{$edit_test_meter3.0}{/if}"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter3.1)}{$edit_test_meter3.1}{/if}"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter3.2)}{$edit_test_meter3.2}{/if}"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter3.3)}{$edit_test_meter3.3}{/if}"  >
                </span>
                <span class="main_testMeter3"> 
                <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter3.4)}{$edit_test_meter3.4}{/if}"  >
                </span>
                {if isset($edit_test_meter3.0)}{{$edit_test_meter3=$edit_test_meter3|array_diff_key:(['0']|array_flip)}}{/if}
                {if isset($edit_test_meter3.1)}{{$edit_test_meter3=$edit_test_meter3|array_diff_key:(['1']|array_flip)}}{/if}
                {if isset($edit_test_meter3.2)}{{$edit_test_meter3=$edit_test_meter3|array_diff_key:(['2']|array_flip)}}{/if}
                {if isset($edit_test_meter3.3)}{{$edit_test_meter3=$edit_test_meter3|array_diff_key:(['3']|array_flip)}}{/if}
                {if isset($edit_test_meter3.4)}{{$edit_test_meter3=$edit_test_meter3|array_diff_key:(['4']|array_flip)}}{/if}
                {if !empty($edit_test_meter3)}
                    {foreach from=$edit_test_meter3 key=k item=det}
                        <span class="main_testMeter3"> 
                            <input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                        </span>
                    {/foreach}
                {/if}
            </span>
             <br/>
             <br/>
            <b> &nbsp;&nbsp;&nbsp;&nbsp;5% Ratio (<span id="test_box_count_filled2"> </span>/<span id="test_box_total2"> </span>)</b>
            <input type="button" value=" + " class="testMeter2_add" />
            <span class="testMeter2_values">
                <span class="main_testMeter21 main_testMeter2"> 
                <input type="button" value=" - " class="testMeter2_remove" /><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter2.0)}{$edit_test_meter2.0}{/if}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter2.1)}{$edit_test_meter2.1}{/if}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter2.2)}{$edit_test_meter2.2}{/if}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter2.3)}{$edit_test_meter2.3}{/if}"  >
                </span>
                <span class="main_testMeter2"> 
                <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter2.4)}{$edit_test_meter2.4}{/if}"  >
                </span>
                {if isset($edit_test_meter2.0)}{{$edit_test_meter2=$edit_test_meter2|array_diff_key:(['0']|array_flip)}}{/if}
                {if isset($edit_test_meter2.1)}{{$edit_test_meter2=$edit_test_meter2|array_diff_key:(['1']|array_flip)}}{/if}
                {if isset($edit_test_meter2.2)}{{$edit_test_meter2=$edit_test_meter2|array_diff_key:(['2']|array_flip)}}{/if}
                {if isset($edit_test_meter2.3)}{{$edit_test_meter2=$edit_test_meter2|array_diff_key:(['3']|array_flip)}}{/if}
                {if isset($edit_test_meter2.4)}{{$edit_test_meter2=$edit_test_meter2|array_diff_key:(['4']|array_flip)}}{/if}
                {if !empty($edit_test_meter2)}
                    {foreach from=$edit_test_meter2 key=k item=det}
                        <span class="main_testMeter2"> 
                            <input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                        </span>
                    {/foreach}
                {/if}
            </span>
            <br/>
            <br/> 
          <span class="1_per_entry" >
            <b> &nbsp;&nbsp;&nbsp;&nbsp;1% Ratio (<span id="test_box_count_filled"> </span>/<span id="test_box_total"> </span>)</b>
            <input type="button" value=" + " class="testMeter_add" />
            <span class="testMeter_values">
                <span class="main_testMeter1 main_testMeter"> 
                <input type="button" value=" - " class="testMeter_remove" /><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only"  size="4" align="right" title="testMeter" value="{if isset($edit_test_meter1.0)}{$edit_test_meter1.0}{/if}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter1.1)}{$edit_test_meter1.1}{/if}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter1.2)}{$edit_test_meter1.2}{/if}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter1.3)}{$edit_test_meter1.3}{/if}"  >
                </span>
                <span class="main_testMeter"> 
                <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter1.4)}{$edit_test_meter1.4}{/if}"  >
                </span>
                {if isset($edit_test_meter1.0)}{{$edit_test_meter1=$edit_test_meter1|array_diff_key:(['0']|array_flip)}}{/if}
                {if isset($edit_test_meter1.1)}{{$edit_test_meter1=$edit_test_meter1|array_diff_key:(['1']|array_flip)}}{/if}
                {if isset($edit_test_meter1.2)}{{$edit_test_meter1=$edit_test_meter1|array_diff_key:(['2']|array_flip)}}{/if}
                {if isset($edit_test_meter1.3)}{{$edit_test_meter1=$edit_test_meter1|array_diff_key:(['3']|array_flip)}}{/if}
                {if isset($edit_test_meter1.4)}{{$edit_test_meter1=$edit_test_meter1|array_diff_key:(['4']|array_flip)}}{/if}
                {if !empty($edit_test_meter1)}
                    {foreach from=$edit_test_meter1 key=k item=det}
                        <span class="main_testMeter"> 
                            <input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                        </span>
                    {/foreach}
                {/if}
            </span>
            <br/>
            <br/>
          </span>
            <br/>
            <br/>
            
            <b> 120% Phase (<span id="test_box_count_filled10"> </span>/<span id="test_box_total10"> </span>)</b>
            <input type="button" value=" + " class="testMeter10_add" />
            <span class="testMeter10_values">
                <span class="main_testMeter101 main_testMeter10"> 
                <input type="button" value=" - " class="testMeter10_remove" /><input type="text" name="testMeter10[][]" id="testMeter10_1" class="testMeter10 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter10.0)}{$edit_test_meter10.0}{/if}"  >
                </span>
                <span class="main_testMeter10"> 
                <input type="text" name="testMeter10[][]" class="testMeter10 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter10.1)}{$edit_test_meter10.1}{/if}"  >
                </span>
                <span class="main_testMeter10"> 
                <input type="text" name="testMeter10[][]" class="testMeter10 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter10.2)}{$edit_test_meter10.2}{/if}"  >
                </span>
                <span class="main_testMeter10"> 
                <input type="text" name="testMeter10[][]" class="testMeter10 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter10.3)}{$edit_test_meter10.3}{/if}"  >
                </span>
                <span class="main_testMeter10"> 
                <input type="text" name="testMeter10[][]" class="testMeter10 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter10.4)}{$edit_test_meter10.4}{/if}"  >
                </span>
                {if isset($edit_test_meter10.0)}{{$edit_test_meter10=$edit_test_meter10|array_diff_key:(['0']|array_flip)}}{/if}
                {if isset($edit_test_meter10.1)}{{$edit_test_meter10=$edit_test_meter10|array_diff_key:(['1']|array_flip)}}{/if}
                {if isset($edit_test_meter10.2)}{{$edit_test_meter10=$edit_test_meter10|array_diff_key:(['2']|array_flip)}}{/if}
                {if isset($edit_test_meter10.3)}{{$edit_test_meter10=$edit_test_meter10|array_diff_key:(['3']|array_flip)}}{/if}
                {if isset($edit_test_meter10.4)}{{$edit_test_meter10=$edit_test_meter10|array_diff_key:(['4']|array_flip)}}{/if}
                {if !empty($edit_test_meter10)}
                    {foreach from=$edit_test_meter10 key=k item=det}
                        <span class="main_testMeter10"> 
                            <input type="text" name="testMeter10[][]" class="testMeter10 num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                        </span>
                    {/foreach}
                {/if}
            </span>
            <br/>
            <br/>
            <b> 100% Phase (<span id="test_box_count_filled9"> </span>/<span id="test_box_total9"> </span>)</b>
            <input type="button" value=" + " class="testMeter9_add" />
            <span class="testMeter9_values">
                <span class="main_testMeter91 main_testMeter9"> 
                <input type="button" value=" - " class="testMeter9_remove" /><input type="text" name="testMeter9[][]" id="testMeter9_1" class="testMeter9 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter9.0)}{$edit_test_meter9.0}{/if}"  >
                </span>
                <span class="main_testMeter9"> 
                <input type="text" name="testMeter9[][]" class="testMeter9 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter9.1)}{$edit_test_meter9.1}{/if}"  >
                </span>
                <span class="main_testMeter9"> 
                <input type="text" name="testMeter9[][]" class="testMeter9 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter9.2)}{$edit_test_meter9.2}{/if}"  >
                </span>
                <span class="main_testMeter9"> 
                <input type="text" name="testMeter9[][]" class="testMeter9 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter9.3)}{$edit_test_meter9.3}{/if}"  >
                </span>
                <span class="main_testMeter9"> 
                <input type="text" name="testMeter9[][]" class="testMeter9 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter9.4)}{$edit_test_meter9.4}{/if}"  >
                </span>
                    {if isset($edit_test_meter9.0)}{{$edit_test_meter9=$edit_test_meter9|array_diff_key:(['0']|array_flip)}}{/if}
                    {if isset($edit_test_meter9.1)}{{$edit_test_meter9=$edit_test_meter9|array_diff_key:(['1']|array_flip)}}{/if}
                    {if isset($edit_test_meter9.2)}{{$edit_test_meter9=$edit_test_meter9|array_diff_key:(['2']|array_flip)}}{/if}
                    {if isset($edit_test_meter9.3)}{{$edit_test_meter9=$edit_test_meter9|array_diff_key:(['3']|array_flip)}}{/if}
                    {if isset($edit_test_meter9.4)}{{$edit_test_meter9=$edit_test_meter9|array_diff_key:(['4']|array_flip)}}{/if}
                    {if !empty($edit_test_meter9)}
                        {foreach from=$edit_test_meter9 key=k item=det}
                            <span class="main_testMeter9"> 
                                <input type="text" name="testMeter9[][]" class="testMeter9 num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                            </span>
                        {/foreach}
                    {/if}
            </span>
            <br/>
            <br/>
            
            <b> &nbsp;&nbsp;20% Phase (<span id="test_box_count_filled8"> </span>/<span id="test_box_total8"> </span>)</b>
            <input type="button" value=" + " class="testMeter8_add" />
            <span class="testMeter8_values">
                <span class="main_testMeter81 main_testMeter8"> 
                <input type="button" value=" - " class="testMeter8_remove" /><input type="text" name="testMeter8[][]" id="testMeter8_1" class="testMeter8 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter8.0)}{$edit_test_meter8.0}{/if}"  >
                </span>
                <span class="main_testMeter8"> 
                <input type="text" name="testMeter8[][]" class="testMeter8 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter8.1)}{$edit_test_meter8.1}{/if}"  >
                </span>
                <span class="main_testMeter8"> 
                <input type="text" name="testMeter8[][]" class="testMeter8 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter8.2)}{$edit_test_meter8.2}{/if}"  >
                </span>
                <span class="main_testMeter8"> 
                <input type="text" name="testMeter8[][]" class="testMeter8 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter8.3)}{$edit_test_meter8.3}{/if}"  >
                </span>
                <span class="main_testMeter8"> 
                <input type="text" name="testMeter8[][]" class="testMeter8 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter8.4)}{$edit_test_meter8.4}{/if}"  >
                </span>
                    {if isset($edit_test_meter8.0)}{{$edit_test_meter8=$edit_test_meter8|array_diff_key:(['0']|array_flip)}}{/if}
                    {if isset($edit_test_meter8.1)}{{$edit_test_meter8=$edit_test_meter8|array_diff_key:(['1']|array_flip)}}{/if}
                    {if isset($edit_test_meter8.2)}{{$edit_test_meter8=$edit_test_meter8|array_diff_key:(['2']|array_flip)}}{/if}
                    {if isset($edit_test_meter8.3)}{{$edit_test_meter8=$edit_test_meter8|array_diff_key:(['3']|array_flip)}}{/if}
                    {if isset($edit_test_meter8.4)}{{$edit_test_meter8=$edit_test_meter8|array_diff_key:(['4']|array_flip)}}{/if}
                    {if !empty($edit_test_meter8)}
                        {foreach from=$edit_test_meter8 key=k item=det}
                            <span class="main_testMeter8"> 
                                <input type="text" name="testMeter8[][]" class="testMeter8 num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                            </span>
                        {/foreach}
                    {/if}
            </span>
            <br/>
            <br/>
            
            <b> &nbsp;&nbsp;&nbsp;&nbsp;5% Phase (<span id="test_box_count_filled7"> </span>/<span id="test_box_total7"> </span>)</b>
            <input type="button" value=" + " class="testMeter7_add" />
            <span class="testMeter7_values">
                <span class="main_testMeter71 main_testMeter7"> 
                <input type="button" value=" - " class="testMeter7_remove" /><input type="text" name="testMeter7[][]" id="testMeter7_1" class="testMeter7 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter7.0)}{$edit_test_meter7.0}{/if}"  >
                </span>
                <span class="main_testMeter7"> 
                <input type="text" name="testMeter7[][]" class="testMeter7 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter7.1)}{$edit_test_meter7.1}{/if}"  >
                </span>
                <span class="main_testMeter7"> 
                <input type="text" name="testMeter7[][]" class="testMeter7 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter7.2)}{$edit_test_meter7.2}{/if}"  >
                </span>
                <span class="main_testMeter7"> 
                <input type="text" name="testMeter7[][]" class="testMeter7 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter7.3)}{$edit_test_meter7.3}{/if}"  >
                </span>
                <span class="main_testMeter7"> 
                <input type="text" name="testMeter7[][]" class="testMeter7 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter7.4)}{$edit_test_meter7.4}{/if}"  >
                </span>
                    {if isset($edit_test_meter7.0)}{{$edit_test_meter7=$edit_test_meter7|array_diff_key:(['0']|array_flip)}}{/if}
                    {if isset($edit_test_meter7.1)}{{$edit_test_meter7=$edit_test_meter7|array_diff_key:(['1']|array_flip)}}{/if}
                    {if isset($edit_test_meter7.2)}{{$edit_test_meter7=$edit_test_meter7|array_diff_key:(['2']|array_flip)}}{/if}
                    {if isset($edit_test_meter7.3)}{{$edit_test_meter7=$edit_test_meter7|array_diff_key:(['3']|array_flip)}}{/if}
                    {if isset($edit_test_meter7.4)}{{$edit_test_meter7=$edit_test_meter7|array_diff_key:(['4']|array_flip)}}{/if}
                    {if !empty($edit_test_meter7)}
                        {foreach from=$edit_test_meter7 key=k item=det}
                            <span class="main_testMeter7"> 
                                <input type="text" name="testMeter7[][]" class="testMeter7 num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                            </span>
                        {/foreach}
                    {/if}
            </span>
            <br/>
            <br/>
            
             <span class="1_per_entry" >
                <b> &nbsp;&nbsp;&nbsp;&nbsp;1% Phase (<span id="test_box_count_filled6"> </span>/<span id="test_box_total6"> </span>)</b>
                <input type="button" value=" + " class="testMeter6_add" />
                <span class="testMeter6_values">
                    <span class="main_testMeter61 main_testMeter6"> 
                    <input type="button" value=" - " class="testMeter6_remove" /><input type="text" name="testMeter6[][]" id="testMeter6_1" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter6.0)}{$edit_test_meter6.0}{/if}"  >
                    </span>
                    <span class="main_testMeter6"> 
                    <input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter6.1)}{$edit_test_meter6.1}{/if}"  >
                    </span>
                    <span class="main_testMeter6"> 
                    <input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter6.2)}{$edit_test_meter6.2}{/if}"  >
                    </span>
                    <span class="main_testMeter6"> 
                    <input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter6.3)}{$edit_test_meter6.3}{/if}"  >
                    </span>
                    <span class="main_testMeter6"> 
                    <input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  value="{if isset($edit_test_meter6.4)}{$edit_test_meter6.4}{/if}"  >
                    </span>
                    {if isset($edit_test_meter6.0)}{{$edit_test_meter6=$edit_test_meter6|array_diff_key:(['0']|array_flip)}}{/if}
                    {if isset($edit_test_meter6.1)}{{$edit_test_meter6=$edit_test_meter6|array_diff_key:(['1']|array_flip)}}{/if}
                    {if isset($edit_test_meter6.2)}{{$edit_test_meter6=$edit_test_meter6|array_diff_key:(['2']|array_flip)}}{/if}
                    {if isset($edit_test_meter6.3)}{{$edit_test_meter6=$edit_test_meter6|array_diff_key:(['3']|array_flip)}}{/if}
                    {if isset($edit_test_meter6.4)}{{$edit_test_meter6=$edit_test_meter6|array_diff_key:(['4']|array_flip)}}{/if}
                    {if !empty($edit_test_meter6)}
                        {foreach from=$edit_test_meter6 key=k item=det}
                            <span class="main_testMeter6"> 
                                <input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"   value="{$det}">
                            </span>
                        {/foreach}
                    {/if}
                </span>
                <br/>
                <br/>
          </span>
            
            
            
            
            {*<input type="button" id="view_calculation" value="View / Refresh Calculation" class="button" />
            <input type="button" id="hide_calculation" value="Hide Calculation" class="button" />*}
            <br/>
            <br/>
            
    </td>
  </tr>
  </tbody>
  <tr>
    <td align="center" colspan="28"><input type="submit" name="insertBtn" value="Submit" class="button" /></td>
  </tr>
</table>
  <span id="image_span"><img style="float: right" width="400px"  src="./images/table_info/table_16.png" /></span>
<div id="rangedisplay"></div> 
