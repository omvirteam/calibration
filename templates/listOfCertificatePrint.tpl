<center><font size="5">List of Certificate</font></center>
{if $certiArray != 0}
  <table border='1' cellpadding='2' cellspacing='0' align='center'>
  <tr>
    <td class="table2" colspan="5">Company : {$selectedCustomer}</td>
    <td class="table2" colspan="3">Status : {$statusFired}</td>
  </tr>
  <tr>
    <td class="table1" align="center">Sr no.</td>
    <td class="table1" align="center">GRN</td>
    <td class="table1" align="center">Company</td>
    <td class="table1" align="center">Instrument</td>
    <td class="table1" align="center">Sr. No.</td>
    <td class="table1" align="center">Make</td>
    <td class="table1" align="center">Range</td>
    <td class="table1" align="center">Report No.</td>
	</tr>
  {section name="sec" loop=$certiArray}
  <tr>
    <td class="table2" align="center">{$smarty.section.sec.rownum}</td>
    <td class="table2">{$certiArray[sec].grnNo}</td>
    <td class="table2">{$certiArray[sec].custName}</td>
    <td class="table2">{$certiArray[sec].itemName}</td>
    <td class="table2">{$certiArray[sec].itemCode}</td>
    <td class="table2">{$certiArray[sec].makeModel}</td>
    <td class="table2">{$certiArray[sec].range}</td>
    <td class="table2" align="center">{$certiArray[sec].selfCertiNo}</td>
    </tr>
  {sectionelse}
  <tr><td colspan="8" align="center"><h1><font color="red"><b>No Record Found...!</b></font></h1></td></tr>
  {/section}
  </table>
{/if}
{include file="footer.tpl"}