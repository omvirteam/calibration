{include file="./headStartPrint.tpl"}
{include file="./headEndPrint.tpl"}
<form name="PrintForm" id="printForm" method="POST" action="{$smarty.server.PHP_SELF}">
<table border="1" cellSpacing="0" cellpadding="2">
    <tr>
      <th align="right" colspan="2">
        <a href="grnList.php"><img src="./images/logo_newaddr.jpg" border="0" width="300" height="130" /></a>
      </th>
    </tr>
    <tr>
      <td>
      <table border="0" cellSpacing="0" cellpadding="2">
      <tr>
        <td> GRN No. :</td>
        <td class="table2" align="left">{$grnPrefix}-{$grnNo}</td>
        <td></td>
        <td align="right"> Date :</td>
        <td class="table2" align="left">{$grnDate}</td>
        <td></td>
        <td> P.O /Letter No :</td>
        <td class="table2" align="left">{$poNo}</td>
        <td> Date :</td>
        <td class="table2" align="left">{$poDate}</td>
      </tr>
      <tr>
        <td colspan="2"> Customer / Supplier Name :</td>
        <td class="table2" align="left" colspan="2  ">{$custName}</td>
        <td align="right">CODE :</td>
        <td class="table2" align="left">{$custCode}</td>
      </tr>
      <tr>
        <td> Contact Person :</td>
        <td class="table2" align="left" colspan="3">{$mrAndMrs}.  {$contPerson}</td>
        <td colspan="2"align="right"> PH. No. :</td>
        <td class="table2" align="left">{$phNo}</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td align="center" colspan="2">
    <table border="1" cellSpacing="0" cellpadding="2">
      <tr><th colspan="11">FOLLOWING MATERIALS RECEIVED FOR - CALIBRATION /VERIFICATION</th> </tr>
      <tr>
      <th rowspan="2">SR NO.</th>
      <th rowspan="2">NAME OF INSTRUMENT</th>
      <th rowspan="2">ITEM ID Code</th>
      <th rowspan="2">PARAMETERS</th>
      <th rowspan="2">Range</th>
      <th colspan="3" align="center">Q U A N T I T Y</th>
      <th rowspan="2">Cali Due Date Requested By Cust.</th>
      <th rowspan="2">Expected Delivery Date</th>
      <th rowspan="2">Notes</th>
      </tr>
      <tr>
      <th>CHALLAN</th>
      <th>RECEIVED</th>
      <th>CONDITION</th>
      </tr>
      {section name="sec" loop=$grnDetail}
       <tr>
         <td class="table2" align="left">{$smarty.section.sec.rownum}</td>
         <td class="table2" align="left">{$grnDetail[sec].itemName}</td>
         <td class="table2" align="left">{$grnDetail[sec].itemCode}</td>
         <td class="table2" align="left">{$grnDetail[sec].parameterName}</td>
         <td class="table2" align="left">{$grnDetail[sec].range}</td>
         <td class="table2" align="center">{$grnDetail[sec].challan}</td>
         <td class="table2" align="center">{$grnDetail[sec].received}</td>
         <td class="table2" align="center">{$grnDetail[sec].grnCondition}</td>
         <td class="table2" align="center">{$grnDetail[sec].custReqDate}</td>
         <td class="table2" align="center">{$grnDetail[sec].expDelivDate}</td>
         <td class="table2" align="center">{$grnDetail[sec].description}</td>
         <tr class="table2" align="left">
       {sectionelse}
       {/section}
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="2"> Remarks : {$remarks}</td>
  </tr>
  <tr>
    <td> Received By : {$userName}</td>
    <td> Store HOD : </td>
  </tr>
  <tr><td> For delivery of materials this goods Receipt required,otherwise material will not be returned.</td></tr>
</table>

</form>
{include file="./footerPrint.tpl"}