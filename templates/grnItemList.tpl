{include file="./headStart.tpl"}
<title>G.R.N. Item List</title>
<script type="text/javascript">
$(document).ready(function(){
  $(".link").click(function(){
    $("#grnList").attr("action","grnItemListPrint.php");
    $("#grnList").submit();
    
  });
});
</script>
{include file="./headEnd.tpl"}
<form id="grnList" action="{$smarty.server.PHP_SELF}" name='grnList' method='POST'>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
<center><h2>G.R.N. Item List</h2></center><br>
<tr>
<td class="table1" align="center" colspan="9">
    <input type="submit" name="smsBtn" value=" Send SMS " class="button" style="background-color: #77F2D3;" />
	Select From : {html_select_date prefix="fromDate" start_year="-5" end_year="+2" field_order="DMY" time=$fromDate day_value_format="%02d"}
	To : {html_select_date prefix="toDate" start_year="-2" end_year="+1" field_order="DMY" time=$toDate day_value_format="%02d"}
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	Status : 
	<select name="dispatch">
		{if $dispatchValue == 0}
			<option value="0" SELECTED >All</option>
			<option value="1">Dispatched</option>
			<option value="2">Pending</option>
		{else if $dispatchValue == 1}
			<option value="0">All</option>
			<option value="1" SELECTED >Dispatched</option>
			<option value="2">Pending</option>
		{else if $dispatchValue == 2}
			<option value="0">All</option>
			<option value="1">Dispatched</option>
			<option value="2" SELECTED>Pending</option>
		{/if}
	</select>
         &nbsp; &nbsp; &nbsp;Prefix : <select type="text" name="prefix" style="margin: 5px;" >
        <option value="N" {if $prefix == 'N'} selected {else}{/if}>NABL</option>
        <option value="R" {if $prefix == 'R'} selected {else}{/if}>Regular</option>
        <option value="all" {if $prefix == 'all'} selected {else}{/if}>All</option>
        </select>&nbsp; 
<input type="submit" name="submitDateBtn" value="GO..." class="button"/>

</td>
</tr>
<tr>
  <td class="table1" align="center">Check For SMS</td>
  <td class="table1" align="center">GRN NO</td>
  <td class="table1" align="center">Date of Receipt</td>
  <td class="table1" align="center">Customer Name</td>
  <td class="table1" align="center">Customer Code</td>
  <td class="table1" align="center">Instrument Name</td>
  <td class="table1" align="center">Instrument Sr. No.</td>
  <td class="table1" align="center">ULR No.</td>
  <td class="table1" align="center">Location</td>
  <td class="table1" align="center">Total</td>
</tr>
 {assign var="srNo" value="1"}
 {section name="sec" loop=$grnCount}
<tr>
    <td class="table2" align="center" NOWRAP><input style="width:20px; height: 20px;" type="checkbox" name="mylist[]" value="{$grnArray[sec].customerId}"></td>
  {if $grnArray[sec].display == 1}
    {assign var="srNo" value="1"}
          <td class="table2" style="background-color: #DEEDF2;" align="center">{$grnArray[sec].grnPrefix}-{$grnArray[sec].grnNo}</td>
	  <td class="table2"   style="background-color: #DEEDF2;" align="center" NOWRAP>{$grnArray[sec].grnDate}</td>
	  <td class="table2"   style="background-color: #DEEDF2;" align="center" >{$grnArray[sec].custName}</td>
	  <td class="table2"   style="background-color: #DEEDF2;" align="center" >{$grnArray[sec].custCode}</td>
	  <td class="table2"   style="background-color: #DEEDF2;" align="center" >{$grnArray[sec].itemName}</td>
          <td class="table2"   style="background-color: #DEEDF2;" align="center">{$grnArray[sec].itemCode}</td>	
	  <td class="table2"   style="background-color: #DEEDF2;" align="center" >{$grnArray[sec].ulr_no}</td>
	  <td class="table2"   style="background-color: #DEEDF2;" align="center" >{$grnArray[sec].uuc_location}</td>
	  <td class="table2"   style="background-color: #DEEDF2;" align="center" >{$grnArray[sec].count_ulr}</td>
	{else}
	  <td class="table3">&nbsp;</td>
	  <td class="table3">&nbsp;</td>	
	  <td class="table3">&nbsp;</td>	
	  <td class="table3">&nbsp;</td>	
	  <td class="table2" align="center" >{$grnArray[sec].itemName}</td>
          <td class="table2" align="center">{$grnArray[sec].itemCode}</td>	
	  <td class="table2" align="right">{$grnArray[sec].ulr_no}</td>	
	  <td class="table2" align="center" >{$grnArray[sec].uuc_location}</td>	
	  <td class="table2">&nbsp;</td>	
	{/if}  
</tr>
{sectionelse}
<tr><td align="center" colspan="22"> <h1><font color="red"><b>Record Not Found...!</b></h1></font></td></tr>
{/section}
<tr><td align="center" colspan="22"> <h4><a href="#" class="link"><font color="red"><b>Print</b></a></h4></font></td></tr>
</table>
</form>
{include file="footer.tpl"}