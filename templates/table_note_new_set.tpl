{include file="./headStart.tpl"}
<title>Set Table Note</title>
<script type="text/javascript">
$(document).ready(function() {
  $('#table_info_id').focus();
  $("#table_info_id").change(function(){
    window.location.href = "table_note_new_set.php?table_info_id=" + $('#table_info_id').val();
  });
});
</script>
{include file="./headEnd.tpl"}
<form name="noteData" id="noteData" method="post">

<table border='0' cellpadding='1' cellspacing='2' align='center' width="650px">
<tr width="650px">
  <td class="table1" width="650px" align="center">Set Table Info Note for print</td>
</tr>
<tr width="650px">
  <td width="650px">
      <select name="table_info_id" id="table_info_id" style="width: 650px;" required="">
      <option value="">Select</option>
      {html_options values=$table_info_id output=$table_name selected=$selected_table_info_id}
    </select>
  </td>
</tr>

<tr>
  <td>
    <br/>
  </td>
</tr>
<tr>
  <td>
    Note :
  </td>
</tr>
    {foreach from=$note_arr key=rok item=note_d}
        <tr>
            <td>
                <input type="checkbox" style="height:17px; width: 17px;" {if in_array($note_d.note_id, $table_note_arr)}checked="checked"{else}{/if}   value="{$note_d.note_id}" name="note_ids[]" /> {$note_d.note}
            </td>
        </tr>
    {/foreach}
<tr>
  <td>
      <br/>
    <input type="submit" name="insertBtn" value="Save" class="button" />
  </td>
</tr>
</table>
<br/>
</form>