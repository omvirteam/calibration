<?php
session_start();

@ini_set('display_errors', 'on');
@ini_set('date.timezone', 'Asia/Kolkata');
define('_PS_DEBUG_SQL_', true);
//Connection Settings :Start
$host = "localhost"; //database location
$user = "root"; //database username
$pass = ""; //database password
$dbName = "krishna"; //database name

$link = mysql_connect($host, $user, $pass);//database connection
mysql_select_db($dbName);
// Connection Settings :End

// Smarty Settings :Start
require('./include/smarty/libs/Smarty.class.php');
$smarty = new Smarty();

$smarty->setTemplateDir('./templates');
$smarty->setCompileDir('./include/smarty/templates_c');
$smarty->setCacheDir('./include/smarty/cache');
$smarty->setConfigDir('./include/smarty/configs');
$smarty->allow_php_tag = true;
// Smarty Settings :End

$cfgRecPerGrnPage = 8;

function getModuleRoles(){
        $array = array();
        $seluser = "SELECT * FROM website_modules ORDER BY INET_ATON(REPLACE(TRIM(RPAD(main_module,8,' 0')),' ','.')) ";
        $selResult = mysql_query($seluser) or die(mysql_error());
        $rows = array();
        while($row1 = mysql_fetch_assoc($selResult)) { 
          $rows[] = $row1;
        }
        $i = 0;
        foreach ($rows as $row) {
            $sql = "SELECT * FROM module_roles WHERE website_module_id='" . $row['website_module_id'] . "' ";
            $sqlResult = mysql_query($sql) or die(mysql_error());
            $roles = array();
            while($row1 = mysql_fetch_assoc($sqlResult)) { 
              $roles[] = $row1;
            }
            $array[$i]['id'] = $row['website_module_id'];
            $array[$i]['title'] = $row['title'];
            $array[$i]['main_module'] = $row['main_module'];
            $array[$i]['roles'] = $roles;
            $i++;
        }
        return $array;
    }
    
    function getUserRoleIDS($role_type_id){
        $array = array();
        $sql = " SELECT * FROM user_roles WHERE user_id='" . $role_type_id . "' ";
        $sqlResult = mysql_query($sql) or die(mysql_error());
        $rows = array();
        while($row1 = mysql_fetch_assoc($sqlResult)) { 
          $rows[] = $row1;
        }
        $i = 0;
        foreach ($rows as $row) {
            $array[$i] = $row['role_type_id'];
            $i++;
        }
        return $array;
    }
    
    function have_access_role($module, $role){
        $status = 0;
        $user_roles = $_SESSION['user_roles'];
        if(isset($user_roles[$module]) && in_array($role, $user_roles[$module]))
        {
            $status = 1;
        }
        return $status;
    }

// Constant //
define("MASTER_MODULE_ID", 1); //1
define("MASTER_GRN_PREFIX_MODULE_ID", 2); //1.1
define("MASTER_ITEM_MODULE_ID", 3); //1.2
define("MASTER_PARAMETER_MODULE_ID", 4); //1.3
define("MASTER_METER_MODULE_ID", 5); //1.4
define("MASTER_METER_SUB_MODULE_ID", 6); //1.5
define("MASTER_CUSTOMER_MODULE_ID", 7); //1.6
define("MASTER_SYMBOL_MODULE_ID", 8); //1.7
define("MASTER_RIGHTS_MODULE_ID", 9); //1.8
define("GRN_MODULE_ID", 10); //2
define("STAFF_MODULE_ID", 11); //3
define("OBS_MODULE_ID", 12); //4

?>
