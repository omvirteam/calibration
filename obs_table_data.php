<?php
include("include/omConfig.php");
//echo "<pre>"; print_r($_POST); exit;
$obs_type_id = '';
$table_info_no = trim($_POST['table_info_no']);
$table_info_no_real = trim($_POST['table_info_no']);
if($table_info_no == '3') {
    $table_info_no = 1;
}
if($table_info_no == 'regular_1') {
    $table_info_no = 1;
}
if($table_info_no == '4') {
    $table_info_no = 1;
}
if($table_info_no == '2') {
    $table_info_no = 1;
}
if($table_info_no == '6A') {
    $table_info_no = 1;
}
if($table_info_no == '7') {
    $table_info_no = 1;
}
if($table_info_no == '8') {
    $table_info_no = 1;
}
if($table_info_no == '9') {
    $table_info_no = 1;
}
if($table_info_no == '15') {
    $table_info_no = 1;
}
if($table_info_no == '10') {
    $table_info_no = 1;
}

if($table_info_no == '6B') {
    $table_info_no = 6;
}
if($table_info_no == '11') {
    $table_info_no = 11;
}
if($table_info_no == '13_and_14') {
    $table_info_no = 1;
}
if($table_info_no == '17') {
    $table_info_no = 17;
}
if($table_info_no == '18') {
    $table_info_no = 1;
}
if($table_info_no == '5_and_12') {
    $table_info_no = 5;
}
if($table_info_no == '19') {
    $table_info_no = 19;
    if(isset($_POST['obs_type_id']) && !empty($_POST['obs_type_id'])){
        $obs_type_id = $_POST['obs_type_id'];
    } else {
        exit;
    }
}

$edit_reading_json_data = array();
$edit_uuc_json_data = array();

$edit_test_meter1 = array();
$edit_test_meter2 = array();
$edit_test_meter3 = array();
$edit_test_meter4 = array();
$edit_test_meter5 = array();
$edit_test_meter6 = array();
$edit_test_meter7 = array();
$edit_test_meter8 = array();
$edit_test_meter9 = array();
$edit_test_meter10 = array();
$edit_std_meter = array();
$edit_uuc_slip_reading = '';
$edit_uuc_rpm_reading = '';

if(isset($_POST['edit_reading_json_data']) && !empty($_POST['edit_reading_json_data'])){
    $edit_reading_json_data = json_decode(stripslashes($_POST['edit_reading_json_data']));
    $edit_reading_json_data = json_decode(json_encode($edit_reading_json_data), true);
    $edit_test_meter1 = isset($edit_reading_json_data['testMeter']) ? $edit_reading_json_data['testMeter'] : '';
    $edit_test_meter2 = isset($edit_reading_json_data['testMeter2']) ? $edit_reading_json_data['testMeter2'] : '';
    $edit_test_meter3 = isset($edit_reading_json_data['testMeter3']) ? $edit_reading_json_data['testMeter3'] : '';
    $edit_test_meter4 = isset($edit_reading_json_data['testMeter4']) ? $edit_reading_json_data['testMeter4'] : '';
    $edit_test_meter5 = isset($edit_reading_json_data['testMeter5']) ? $edit_reading_json_data['testMeter5'] : '';
    $edit_test_meter6 = isset($edit_reading_json_data['testMeter6']) ? $edit_reading_json_data['testMeter6'] : '';
    $edit_test_meter7 = isset($edit_reading_json_data['testMeter7']) ? $edit_reading_json_data['testMeter7'] : '';
    $edit_test_meter8 = isset($edit_reading_json_data['testMeter8']) ? $edit_reading_json_data['testMeter8'] : '';
    $edit_test_meter9 = isset($edit_reading_json_data['testMeter8']) ? $edit_reading_json_data['testMeter9'] : '';
    $edit_test_meter10 = isset($edit_reading_json_data['testMeter10']) ? $edit_reading_json_data['testMeter10'] : '';
    $edit_std_meter = $edit_reading_json_data['stdMeter'];
    $edit_uuc_json_data = json_decode(stripslashes($_POST['edit_uuc_json_data']));
    $edit_uuc_json_data = json_decode(json_encode($edit_uuc_json_data), true);
//    echo "<pre>"; print_r($_POST); exit;
}

$edit_reading_std_meter = array();
$edit_reading_std_meter_1 = ''; 
$edit_reading_std_meter_2 = ''; 
$edit_reading_std_meter_3 = ''; 
$edit_reading_std_meter_4 = ''; 
$edit_reading_std_meter_5 = ''; 
$edit_reading_std_meter_6 = ''; 
$edit_reading_std_meter_7 = ''; 
$edit_reading_std_meter_8 = ''; 
$edit_reading_std_meter_9 = ''; 
$edit_reading_std_meter_10 = ''; 

$edit_reading_test_meter = array();
$edit_reading_test_meter_1 = '';

$edit_reading_test_meter1 = array();
$edit_reading_test_meter1_1  = '';
$edit_reading_test_meter1_2  = '';
$edit_reading_test_meter1_3  = '';
$edit_reading_test_meter1_4  = '';
$edit_reading_test_meter1_5  = '';
$edit_reading_test_meter1_6  = '';
$edit_reading_test_meter1_7  = '';
$edit_reading_test_meter1_8  = '';
$edit_reading_test_meter1_9  = '';
$edit_reading_test_meter1_10 = '';

$edit_reading_test_meter2 = array();
$edit_reading_test_meter2_1  = '';
$edit_reading_test_meter2_2  = '';
$edit_reading_test_meter2_3  = '';
$edit_reading_test_meter2_4  = '';
$edit_reading_test_meter2_5  = '';
$edit_reading_test_meter2_6  = '';
$edit_reading_test_meter2_7  = '';
$edit_reading_test_meter2_8  = '';
$edit_reading_test_meter2_9  = '';
$edit_reading_test_meter2_10 = '';

$edit_reading_test_meter3 = array();
$edit_reading_test_meter3_1  = '';
$edit_reading_test_meter3_2  = '';
$edit_reading_test_meter3_3  = '';
$edit_reading_test_meter3_4  = '';
$edit_reading_test_meter3_5  = '';
$edit_reading_test_meter3_6  = '';
$edit_reading_test_meter3_7  = '';
$edit_reading_test_meter3_8  = '';
$edit_reading_test_meter3_9  = '';
$edit_reading_test_meter3_10 = '';

$edit_reading_test_meter4 = array();
$edit_reading_test_meter4_1  = '';
$edit_reading_test_meter4_2  = '';
$edit_reading_test_meter4_3  = '';
$edit_reading_test_meter4_4  = '';
$edit_reading_test_meter4_5  = '';
$edit_reading_test_meter4_6  = '';
$edit_reading_test_meter4_7  = '';
$edit_reading_test_meter4_8  = '';
$edit_reading_test_meter4_9  = '';
$edit_reading_test_meter4_10 = '';

$edit_reading_test_meter5 = array();
$edit_reading_test_meter5_1  = '';
$edit_reading_test_meter5_2  = '';
$edit_reading_test_meter5_3  = '';
$edit_reading_test_meter5_4  = '';
$edit_reading_test_meter5_5  = '';
$edit_reading_test_meter5_6  = '';
$edit_reading_test_meter5_7  = '';
$edit_reading_test_meter5_8  = '';
$edit_reading_test_meter5_9  = '';
$edit_reading_test_meter5_10 = '';

$edit_reading_test_meter6 = array();
$edit_reading_test_meter6_1  = '';
$edit_reading_test_meter6_2  = '';
$edit_reading_test_meter6_3  = '';
$edit_reading_test_meter6_4  = '';
$edit_reading_test_meter6_5  = '';
$edit_reading_test_meter6_6  = '';
$edit_reading_test_meter6_7  = '';
$edit_reading_test_meter6_8  = '';
$edit_reading_test_meter6_9  = '';
$edit_reading_test_meter6_10 = '';



if(isset($_POST['edit_reading_json_data']) && !empty($_POST['edit_reading_json_data'])){
    $edit_reading_json_data = json_decode(stripslashes($_POST['edit_reading_json_data']));
    $edit_reading_json_data = json_decode(json_encode($edit_reading_json_data), true);
    $edit_reading_std_meter = $edit_reading_json_data['stdMeter'];
    $edit_reading_std_meter_1 = isset($edit_reading_std_meter[0]) ? $edit_reading_std_meter[0] : ''; 
    $edit_reading_std_meter_2 = isset($edit_reading_std_meter[1]) ? $edit_reading_std_meter[1] : ''; 
    $edit_reading_std_meter_3 = isset($edit_reading_std_meter[2]) ? $edit_reading_std_meter[2] : ''; 
    $edit_reading_std_meter_4 = isset($edit_reading_std_meter[3]) ? $edit_reading_std_meter[3] : ''; 
    $edit_reading_std_meter_5 = isset($edit_reading_std_meter[4]) ? $edit_reading_std_meter[4] : ''; 
    if(isset($edit_reading_std_meter[0])){unset($edit_reading_std_meter[0]);}
    if(isset($edit_reading_std_meter[1])){unset($edit_reading_std_meter[1]);}
    if(isset($edit_reading_std_meter[2])){unset($edit_reading_std_meter[2]);}
    if(isset($edit_reading_std_meter[3])){unset($edit_reading_std_meter[3]);}
    if(isset($edit_reading_std_meter[4])){unset($edit_reading_std_meter[4]);}
    
    $edit_reading_test_meter = isset($edit_reading_json_data['testMeter']) ? $edit_reading_json_data['testMeter'] : '';
    $edit_reading_test_meter_1 = isset($edit_reading_test_meter[0]) ? $edit_reading_test_meter[0] : ''; 
    if(isset($edit_reading_test_meter[0])){unset($edit_reading_test_meter[0]);}
    
    $edit_reading_test_meter2 = isset($edit_reading_json_data['testMeter2']) ? $edit_reading_json_data['testMeter2'] : '';
    if(!empty($edit_reading_test_meter2)){
        $edit_reading_test_meter2_1 = isset($edit_reading_test_meter2[0]) ? $edit_reading_test_meter2[0] : ''; 
        $edit_reading_test_meter2_2 = isset($edit_reading_test_meter2[1]) ? $edit_reading_test_meter2[1] : ''; 
        $edit_reading_test_meter2_3 = isset($edit_reading_test_meter2[2]) ? $edit_reading_test_meter2[2] : ''; 
        $edit_reading_test_meter2_4 = isset($edit_reading_test_meter2[3]) ? $edit_reading_test_meter2[3] : ''; 
        $edit_reading_test_meter2_5 = isset($edit_reading_test_meter2[4]) ? $edit_reading_test_meter2[4] : ''; 
        $edit_reading_test_meter2_6 = isset($edit_reading_test_meter2[5]) ? $edit_reading_test_meter2[5] : ''; 
        $edit_reading_test_meter2_7 = isset($edit_reading_test_meter2[6]) ? $edit_reading_test_meter2[6] : ''; 
        $edit_reading_test_meter2_8 = isset($edit_reading_test_meter2[7]) ? $edit_reading_test_meter2[7] : ''; 
        $edit_reading_test_meter2_9 = isset($edit_reading_test_meter2[8]) ? $edit_reading_test_meter2[8] : ''; 
        $edit_reading_test_meter2_10 = isset($edit_reading_test_meter2[9]) ? $edit_reading_test_meter2[9] : ''; 
        if(isset($edit_reading_test_meter2[0])){unset($edit_reading_test_meter2[0]);}
        if(isset($edit_reading_test_meter2[1])){unset($edit_reading_test_meter2[1]);}
        if(isset($edit_reading_test_meter2[2])){unset($edit_reading_test_meter2[2]);}
        if(isset($edit_reading_test_meter2[3])){unset($edit_reading_test_meter2[3]);}
        if(isset($edit_reading_test_meter2[4])){unset($edit_reading_test_meter2[4]);}
        if(isset($edit_reading_test_meter2[5])){unset($edit_reading_test_meter2[5]);}
        if(isset($edit_reading_test_meter2[6])){unset($edit_reading_test_meter2[6]);}
        if(isset($edit_reading_test_meter2[7])){unset($edit_reading_test_meter2[7]);}
        if(isset($edit_reading_test_meter2[8])){unset($edit_reading_test_meter2[8]);}
        if(isset($edit_reading_test_meter2[9])){unset($edit_reading_test_meter2[9]);}
    }
    
    $edit_reading_test_meter3 = isset($edit_reading_json_data['testMeter3']) ? $edit_reading_json_data['testMeter3'] : '';
    if(!empty($edit_reading_test_meter3)) {
        $edit_reading_test_meter3_1 = isset($edit_reading_test_meter3[0]) ? $edit_reading_test_meter3[0] : ''; 
        $edit_reading_test_meter3_2 = isset($edit_reading_test_meter3[1]) ? $edit_reading_test_meter3[1] : ''; 
        $edit_reading_test_meter3_3 = isset($edit_reading_test_meter3[2]) ? $edit_reading_test_meter3[2] : ''; 
        $edit_reading_test_meter3_4 = isset($edit_reading_test_meter3[3]) ? $edit_reading_test_meter3[3] : ''; 
        $edit_reading_test_meter3_5 = isset($edit_reading_test_meter3[4]) ? $edit_reading_test_meter3[4] : ''; 
        $edit_reading_test_meter3_6 = isset($edit_reading_test_meter3[5]) ? $edit_reading_test_meter3[5] : ''; 
        $edit_reading_test_meter3_7 = isset($edit_reading_test_meter3[6]) ? $edit_reading_test_meter3[6] : ''; 
        $edit_reading_test_meter3_8 = isset($edit_reading_test_meter3[7]) ? $edit_reading_test_meter3[7] : ''; 
        $edit_reading_test_meter3_9 = isset($edit_reading_test_meter3[8]) ? $edit_reading_test_meter3[8] : ''; 
        $edit_reading_test_meter3_10 = isset($edit_reading_test_meter3[9]) ? $edit_reading_test_meter3[9] : ''; 
        if(isset($edit_reading_test_meter3[0])){unset($edit_reading_test_meter3[0]);}
        if(isset($edit_reading_test_meter3[1])){unset($edit_reading_test_meter3[1]);}
        if(isset($edit_reading_test_meter3[2])){unset($edit_reading_test_meter3[2]);}
        if(isset($edit_reading_test_meter3[3])){unset($edit_reading_test_meter3[3]);}
        if(isset($edit_reading_test_meter3[4])){unset($edit_reading_test_meter3[4]);}
        if(isset($edit_reading_test_meter3[5])){unset($edit_reading_test_meter3[5]);}
        if(isset($edit_reading_test_meter3[6])){unset($edit_reading_test_meter3[6]);}
        if(isset($edit_reading_test_meter3[7])){unset($edit_reading_test_meter3[7]);}
        if(isset($edit_reading_test_meter3[8])){unset($edit_reading_test_meter3[8]);}
        if(isset($edit_reading_test_meter3[9])){unset($edit_reading_test_meter3[9]);}
    }
    
    $edit_reading_test_meter4 = isset($edit_reading_json_data['testMeter4']) ? $edit_reading_json_data['testMeter4'] : '';
    if(!empty($edit_reading_test_meter4)) {
        $edit_reading_test_meter4_1 = isset($edit_reading_test_meter4[0]) ? $edit_reading_test_meter4[0] : ''; 
        $edit_reading_test_meter4_2 = isset($edit_reading_test_meter4[1]) ? $edit_reading_test_meter4[1] : ''; 
        $edit_reading_test_meter4_3 = isset($edit_reading_test_meter4[2]) ? $edit_reading_test_meter4[2] : ''; 
        $edit_reading_test_meter4_4 = isset($edit_reading_test_meter4[3]) ? $edit_reading_test_meter4[3] : ''; 
        $edit_reading_test_meter4_5 = isset($edit_reading_test_meter4[4]) ? $edit_reading_test_meter4[4] : ''; 
        $edit_reading_test_meter4_6 = isset($edit_reading_test_meter4[5]) ? $edit_reading_test_meter4[5] : ''; 
        $edit_reading_test_meter4_7 = isset($edit_reading_test_meter4[6]) ? $edit_reading_test_meter4[6] : ''; 
        $edit_reading_test_meter4_8 = isset($edit_reading_test_meter4[7]) ? $edit_reading_test_meter4[7] : ''; 
        $edit_reading_test_meter4_9 = isset($edit_reading_test_meter4[8]) ? $edit_reading_test_meter4[8] : ''; 
        $edit_reading_test_meter4_10 = isset($edit_reading_test_meter4[9]) ? $edit_reading_test_meter4[9] : ''; 
        if(isset($edit_reading_test_meter4[0])){unset($edit_reading_test_meter4[0]);}
        if(isset($edit_reading_test_meter4[1])){unset($edit_reading_test_meter4[1]);}
        if(isset($edit_reading_test_meter4[2])){unset($edit_reading_test_meter4[2]);}
        if(isset($edit_reading_test_meter4[3])){unset($edit_reading_test_meter4[3]);}
        if(isset($edit_reading_test_meter4[4])){unset($edit_reading_test_meter4[4]);}
        if(isset($edit_reading_test_meter4[5])){unset($edit_reading_test_meter4[5]);}
        if(isset($edit_reading_test_meter4[6])){unset($edit_reading_test_meter4[6]);}
        if(isset($edit_reading_test_meter4[7])){unset($edit_reading_test_meter4[7]);}
        if(isset($edit_reading_test_meter4[8])){unset($edit_reading_test_meter4[8]);}
        if(isset($edit_reading_test_meter4[9])){unset($edit_reading_test_meter4[9]);}
    }
    
    $edit_reading_test_meter5 = isset($edit_reading_json_data['testMeter5']) ? $edit_reading_json_data['testMeter5'] : '';
    if(!empty($edit_reading_test_meter5)) {
        $edit_reading_test_meter5_1 = isset($edit_reading_test_meter5[0]) ? $edit_reading_test_meter5[0] : ''; 
        $edit_reading_test_meter5_2 = isset($edit_reading_test_meter5[1]) ? $edit_reading_test_meter5[1] : ''; 
        $edit_reading_test_meter5_3 = isset($edit_reading_test_meter5[2]) ? $edit_reading_test_meter5[2] : ''; 
        $edit_reading_test_meter5_4 = isset($edit_reading_test_meter5[3]) ? $edit_reading_test_meter5[3] : ''; 
        $edit_reading_test_meter5_5 = isset($edit_reading_test_meter5[4]) ? $edit_reading_test_meter5[4] : ''; 
        $edit_reading_test_meter5_6 = isset($edit_reading_test_meter5[5]) ? $edit_reading_test_meter5[5] : ''; 
        $edit_reading_test_meter5_7 = isset($edit_reading_test_meter5[6]) ? $edit_reading_test_meter5[6] : ''; 
        $edit_reading_test_meter5_8 = isset($edit_reading_test_meter5[7]) ? $edit_reading_test_meter5[7] : ''; 
        $edit_reading_test_meter5_9 = isset($edit_reading_test_meter5[8]) ? $edit_reading_test_meter5[8] : ''; 
        $edit_reading_test_meter5_10 = isset($edit_reading_test_meter5[9]) ? $edit_reading_test_meter5[9] : ''; 
        if(isset($edit_reading_test_meter5[0])){unset($edit_reading_test_meter5[0]);}
        if(isset($edit_reading_test_meter5[1])){unset($edit_reading_test_meter5[1]);}
        if(isset($edit_reading_test_meter5[2])){unset($edit_reading_test_meter5[2]);}
        if(isset($edit_reading_test_meter5[3])){unset($edit_reading_test_meter5[3]);}
        if(isset($edit_reading_test_meter5[4])){unset($edit_reading_test_meter5[4]);}
        if(isset($edit_reading_test_meter5[5])){unset($edit_reading_test_meter5[5]);}
        if(isset($edit_reading_test_meter5[6])){unset($edit_reading_test_meter5[6]);}
        if(isset($edit_reading_test_meter5[7])){unset($edit_reading_test_meter5[7]);}
        if(isset($edit_reading_test_meter5[8])){unset($edit_reading_test_meter5[8]);}
        if(isset($edit_reading_test_meter5[9])){unset($edit_reading_test_meter5[9]);}
    }
    
    $edit_reading_test_meter6 = isset($edit_reading_json_data['testMeter6']) ? $edit_reading_json_data['testMeter6'] : '';
    if(!empty($edit_reading_test_meter6)) {
        $edit_reading_test_meter6_1 = isset($edit_reading_test_meter6[0]) ? $edit_reading_test_meter6[0] : ''; 
        $edit_reading_test_meter6_2 = isset($edit_reading_test_meter6[1]) ? $edit_reading_test_meter6[1] : ''; 
        $edit_reading_test_meter6_3 = isset($edit_reading_test_meter6[2]) ? $edit_reading_test_meter6[2] : ''; 
        $edit_reading_test_meter6_4 = isset($edit_reading_test_meter6[3]) ? $edit_reading_test_meter6[3] : ''; 
        $edit_reading_test_meter6_5 = isset($edit_reading_test_meter6[4]) ? $edit_reading_test_meter6[4] : ''; 
        $edit_reading_test_meter6_6 = isset($edit_reading_test_meter6[5]) ? $edit_reading_test_meter6[5] : ''; 
        $edit_reading_test_meter6_7 = isset($edit_reading_test_meter6[6]) ? $edit_reading_test_meter6[6] : ''; 
        $edit_reading_test_meter6_8 = isset($edit_reading_test_meter6[7]) ? $edit_reading_test_meter6[7] : ''; 
        $edit_reading_test_meter6_9 = isset($edit_reading_test_meter6[8]) ? $edit_reading_test_meter6[8] : ''; 
        $edit_reading_test_meter6_10 = isset($edit_reading_test_meter6[9]) ? $edit_reading_test_meter6[9] : ''; 
        if(isset($edit_reading_test_meter6[0])){unset($edit_reading_test_meter6[0]);}
        if(isset($edit_reading_test_meter6[1])){unset($edit_reading_test_meter6[1]);}
        if(isset($edit_reading_test_meter6[2])){unset($edit_reading_test_meter6[2]);}
        if(isset($edit_reading_test_meter6[3])){unset($edit_reading_test_meter6[3]);}
        if(isset($edit_reading_test_meter6[4])){unset($edit_reading_test_meter6[4]);}
        if(isset($edit_reading_test_meter6[5])){unset($edit_reading_test_meter6[5]);}
        if(isset($edit_reading_test_meter6[6])){unset($edit_reading_test_meter6[6]);}
        if(isset($edit_reading_test_meter6[7])){unset($edit_reading_test_meter6[7]);}
        if(isset($edit_reading_test_meter6[8])){unset($edit_reading_test_meter6[8]);}
        if(isset($edit_reading_test_meter6[9])){unset($edit_reading_test_meter6[9]);}
    }


    
    if($table_info_no == 19 && ($obs_type_id == 1 || $obs_type_id == 3)){
        $edit_reading_json_data = json_decode(stripslashes($_POST['edit_reading_json_data']));
        $edit_reading_json_data = json_decode(json_encode($edit_reading_json_data), true);
        $edit_reading_std_meter = $edit_reading_json_data['stdMeter'];
        $edit_reading_std_meter_1 = isset($edit_reading_std_meter[0]) ? $edit_reading_std_meter[0] : ''; 
        $edit_reading_std_meter_2 = isset($edit_reading_std_meter[1]) ? $edit_reading_std_meter[1] : ''; 
        $edit_reading_std_meter_3 = isset($edit_reading_std_meter[2]) ? $edit_reading_std_meter[2] : ''; 
        $edit_reading_std_meter_4 = isset($edit_reading_std_meter[3]) ? $edit_reading_std_meter[3] : ''; 
        $edit_reading_std_meter_5 = isset($edit_reading_std_meter[4]) ? $edit_reading_std_meter[4] : ''; 
        $edit_reading_std_meter_6 = isset($edit_reading_std_meter[5]) ? $edit_reading_std_meter[5] : ''; 
        $edit_reading_std_meter_7 = isset($edit_reading_std_meter[6]) ? $edit_reading_std_meter[6] : ''; 
        $edit_reading_std_meter_8 = isset($edit_reading_std_meter[7]) ? $edit_reading_std_meter[7] : ''; 
        $edit_reading_std_meter_9 = isset($edit_reading_std_meter[8]) ? $edit_reading_std_meter[8] : ''; 
        $edit_reading_std_meter_10 = isset($edit_reading_std_meter[9]) ? $edit_reading_std_meter[9] : ''; 
        if(isset($edit_reading_std_meter[0])){unset($edit_reading_std_meter[0]);}
        if(isset($edit_reading_std_meter[1])){unset($edit_reading_std_meter[1]);}
        if(isset($edit_reading_std_meter[2])){unset($edit_reading_std_meter[2]);}
        if(isset($edit_reading_std_meter[3])){unset($edit_reading_std_meter[3]);}
        if(isset($edit_reading_std_meter[4])){unset($edit_reading_std_meter[4]);}
        if(isset($edit_reading_std_meter[5])){unset($edit_reading_std_meter[5]);}
        if(isset($edit_reading_std_meter[6])){unset($edit_reading_std_meter[6]);}
        if(isset($edit_reading_std_meter[7])){unset($edit_reading_std_meter[7]);}
        if(isset($edit_reading_std_meter[8])){unset($edit_reading_std_meter[8]);}
        if(isset($edit_reading_std_meter[9])){unset($edit_reading_std_meter[9]);}
        
        $edit_reading_test_meter1 = isset($edit_reading_json_data['testMeter']) ? $edit_reading_json_data['testMeter'] : '';
        if(!empty($edit_reading_test_meter1)){
            $edit_reading_test_meter1_1 = isset($edit_reading_test_meter1[0]) ? $edit_reading_test_meter1[0] : ''; 
            $edit_reading_test_meter1_2 = isset($edit_reading_test_meter1[1]) ? $edit_reading_test_meter1[1] : ''; 
            $edit_reading_test_meter1_3 = isset($edit_reading_test_meter1[2]) ? $edit_reading_test_meter1[2] : ''; 
            $edit_reading_test_meter1_4 = isset($edit_reading_test_meter1[3]) ? $edit_reading_test_meter1[3] : ''; 
            $edit_reading_test_meter1_5 = isset($edit_reading_test_meter1[4]) ? $edit_reading_test_meter1[4] : ''; 
            $edit_reading_test_meter1_6 = isset($edit_reading_test_meter1[5]) ? $edit_reading_test_meter1[5] : ''; 
            $edit_reading_test_meter1_7 = isset($edit_reading_test_meter1[6]) ? $edit_reading_test_meter1[6] : ''; 
            $edit_reading_test_meter1_8 = isset($edit_reading_test_meter1[7]) ? $edit_reading_test_meter1[7] : ''; 
            $edit_reading_test_meter1_9 = isset($edit_reading_test_meter1[8]) ? $edit_reading_test_meter1[8] : ''; 
            $edit_reading_test_meter1_10 = isset($edit_reading_test_meter1[9]) ? $edit_reading_test_meter1[9] : ''; 
            if(isset($edit_reading_test_meter1[0])){unset($edit_reading_test_meter1[0]);}
            if(isset($edit_reading_test_meter1[1])){unset($edit_reading_test_meter1[1]);}
            if(isset($edit_reading_test_meter1[2])){unset($edit_reading_test_meter1[2]);}
            if(isset($edit_reading_test_meter1[3])){unset($edit_reading_test_meter1[3]);}
            if(isset($edit_reading_test_meter1[4])){unset($edit_reading_test_meter1[4]);}
            if(isset($edit_reading_test_meter1[5])){unset($edit_reading_test_meter1[5]);}
            if(isset($edit_reading_test_meter1[6])){unset($edit_reading_test_meter1[6]);}
            if(isset($edit_reading_test_meter1[7])){unset($edit_reading_test_meter1[7]);}
            if(isset($edit_reading_test_meter1[8])){unset($edit_reading_test_meter1[8]);}
            if(isset($edit_reading_test_meter1[9])){unset($edit_reading_test_meter1[9]);}
        }
        
    }
    if($table_info_no == 19 && $obs_type_id == 2){
        $edit_reading_json_data = json_decode(stripslashes($_POST['edit_reading_json_data']));
        $edit_reading_json_data = json_decode(json_encode($edit_reading_json_data), true);
        $edit_reading_std_meter = $edit_reading_json_data['stdMeter'];
        $edit_reading_std_meter_1 = isset($edit_reading_std_meter[0]) ? $edit_reading_std_meter[0] : ''; 
        $edit_reading_std_meter_2 = isset($edit_reading_std_meter[1]) ? $edit_reading_std_meter[1] : ''; 
        $edit_reading_std_meter_3 = isset($edit_reading_std_meter[2]) ? $edit_reading_std_meter[2] : ''; 
        $edit_reading_std_meter_4 = isset($edit_reading_std_meter[3]) ? $edit_reading_std_meter[3] : ''; 
        $edit_reading_std_meter_5 = isset($edit_reading_std_meter[4]) ? $edit_reading_std_meter[4] : ''; 
        if(isset($edit_reading_std_meter[0])){unset($edit_reading_std_meter[0]);}
        if(isset($edit_reading_std_meter[1])){unset($edit_reading_std_meter[1]);}
        if(isset($edit_reading_std_meter[2])){unset($edit_reading_std_meter[2]);}
        if(isset($edit_reading_std_meter[3])){unset($edit_reading_std_meter[3]);}
        if(isset($edit_reading_std_meter[4])){unset($edit_reading_std_meter[4]);}
        
        $edit_reading_test_meter1 = isset($edit_reading_json_data['testMeter']) ? $edit_reading_json_data['testMeter'] : '';
        if(!empty($edit_reading_test_meter1)){
            $edit_reading_test_meter1_1 = isset($edit_reading_test_meter1[0]) ? $edit_reading_test_meter1[0] : ''; 
            $edit_reading_test_meter1_2 = isset($edit_reading_test_meter1[1]) ? $edit_reading_test_meter1[1] : ''; 
            $edit_reading_test_meter1_3 = isset($edit_reading_test_meter1[2]) ? $edit_reading_test_meter1[2] : ''; 
            $edit_reading_test_meter1_4 = isset($edit_reading_test_meter1[3]) ? $edit_reading_test_meter1[3] : ''; 
            $edit_reading_test_meter1_5 = isset($edit_reading_test_meter1[4]) ? $edit_reading_test_meter1[4] : ''; 
            if(isset($edit_reading_test_meter1[0])){unset($edit_reading_test_meter1[0]);}
            if(isset($edit_reading_test_meter1[1])){unset($edit_reading_test_meter1[1]);}
            if(isset($edit_reading_test_meter1[2])){unset($edit_reading_test_meter1[2]);}
            if(isset($edit_reading_test_meter1[3])){unset($edit_reading_test_meter1[3]);}
            if(isset($edit_reading_test_meter1[4])){unset($edit_reading_test_meter1[4]);}
        }
        
    }
    
}
//echo "<pre>"; print_r($edit_test_meter1); exit;
$smarty->assign("edit_reading_json_data", $edit_reading_json_data);
$smarty->assign("edit_reading_std_meter", $edit_reading_std_meter);
$smarty->assign("edit_reading_test_meter", $edit_reading_test_meter);
$smarty->assign("edit_reading_std_meter_1", $edit_reading_std_meter_1);
$smarty->assign("edit_reading_std_meter_2", $edit_reading_std_meter_2);
$smarty->assign("edit_reading_std_meter_3", $edit_reading_std_meter_3);
$smarty->assign("edit_reading_std_meter_4", $edit_reading_std_meter_4);
$smarty->assign("edit_reading_std_meter_5", $edit_reading_std_meter_5);
if($table_info_no == 19 && ($obs_type_id == 1 || $obs_type_id == 3)){
    $smarty->assign("edit_reading_std_meter_6", $edit_reading_std_meter_6);
    $smarty->assign("edit_reading_std_meter_7", $edit_reading_std_meter_7);
    $smarty->assign("edit_reading_std_meter_8", $edit_reading_std_meter_8);
    $smarty->assign("edit_reading_std_meter_9", $edit_reading_std_meter_9);
    $smarty->assign("edit_reading_std_meter_10", $edit_reading_std_meter_10);
}

$smarty->assign("edit_reading_test_meter_1", $edit_reading_test_meter_1);

$smarty->assign("edit_reading_test_meter1", $edit_reading_test_meter1);
$smarty->assign("edit_reading_test_meter1_1", $edit_reading_test_meter1_1);
$smarty->assign("edit_reading_test_meter1_2", $edit_reading_test_meter1_2);
$smarty->assign("edit_reading_test_meter1_3", $edit_reading_test_meter1_3);
$smarty->assign("edit_reading_test_meter1_4", $edit_reading_test_meter1_4);
$smarty->assign("edit_reading_test_meter1_5", $edit_reading_test_meter1_5);
$smarty->assign("edit_reading_test_meter1_6", $edit_reading_test_meter1_6);
$smarty->assign("edit_reading_test_meter1_7", $edit_reading_test_meter1_7);
$smarty->assign("edit_reading_test_meter1_8", $edit_reading_test_meter1_8);
$smarty->assign("edit_reading_test_meter1_9", $edit_reading_test_meter1_9);
$smarty->assign("edit_reading_test_meter1_10", $edit_reading_test_meter1_10);

$smarty->assign("edit_reading_test_meter2", $edit_reading_test_meter2);
$smarty->assign("edit_reading_test_meter2_1", $edit_reading_test_meter2_1);
$smarty->assign("edit_reading_test_meter2_2", $edit_reading_test_meter2_2);
$smarty->assign("edit_reading_test_meter2_3", $edit_reading_test_meter2_3);
$smarty->assign("edit_reading_test_meter2_4", $edit_reading_test_meter2_4);
$smarty->assign("edit_reading_test_meter2_5", $edit_reading_test_meter2_5);
$smarty->assign("edit_reading_test_meter2_6", $edit_reading_test_meter2_6);
$smarty->assign("edit_reading_test_meter2_7", $edit_reading_test_meter2_7);
$smarty->assign("edit_reading_test_meter2_8", $edit_reading_test_meter2_8);
$smarty->assign("edit_reading_test_meter2_9", $edit_reading_test_meter2_9);
$smarty->assign("edit_reading_test_meter2_10", $edit_reading_test_meter2_10);

$smarty->assign("edit_reading_test_meter3", $edit_reading_test_meter3);
$smarty->assign("edit_reading_test_meter3_1", $edit_reading_test_meter3_1);
$smarty->assign("edit_reading_test_meter3_2", $edit_reading_test_meter3_2);
$smarty->assign("edit_reading_test_meter3_3", $edit_reading_test_meter3_3);
$smarty->assign("edit_reading_test_meter3_4", $edit_reading_test_meter3_4);
$smarty->assign("edit_reading_test_meter3_5", $edit_reading_test_meter3_5);
$smarty->assign("edit_reading_test_meter3_6", $edit_reading_test_meter3_6);
$smarty->assign("edit_reading_test_meter3_7", $edit_reading_test_meter3_7);
$smarty->assign("edit_reading_test_meter3_8", $edit_reading_test_meter3_8);
$smarty->assign("edit_reading_test_meter3_9", $edit_reading_test_meter3_9);
$smarty->assign("edit_reading_test_meter3_10", $edit_reading_test_meter3_10);

$smarty->assign("edit_reading_test_meter4", $edit_reading_test_meter4);
$smarty->assign("edit_reading_test_meter4_1", $edit_reading_test_meter4_1);
$smarty->assign("edit_reading_test_meter4_2", $edit_reading_test_meter4_2);
$smarty->assign("edit_reading_test_meter4_3", $edit_reading_test_meter4_3);
$smarty->assign("edit_reading_test_meter4_4", $edit_reading_test_meter4_4);
$smarty->assign("edit_reading_test_meter4_5", $edit_reading_test_meter4_5);
$smarty->assign("edit_reading_test_meter4_6", $edit_reading_test_meter4_6);
$smarty->assign("edit_reading_test_meter4_7", $edit_reading_test_meter4_7);
$smarty->assign("edit_reading_test_meter4_8", $edit_reading_test_meter4_8);
$smarty->assign("edit_reading_test_meter4_9", $edit_reading_test_meter4_9);
$smarty->assign("edit_reading_test_meter4_10", $edit_reading_test_meter4_10);

$smarty->assign("edit_reading_test_meter5", $edit_reading_test_meter5);
$smarty->assign("edit_reading_test_meter5_1", $edit_reading_test_meter5_1);
$smarty->assign("edit_reading_test_meter5_2", $edit_reading_test_meter5_2);
$smarty->assign("edit_reading_test_meter5_3", $edit_reading_test_meter5_3);
$smarty->assign("edit_reading_test_meter5_4", $edit_reading_test_meter5_4);
$smarty->assign("edit_reading_test_meter5_5", $edit_reading_test_meter5_5);
$smarty->assign("edit_reading_test_meter5_6", $edit_reading_test_meter5_6);
$smarty->assign("edit_reading_test_meter5_7", $edit_reading_test_meter5_7);
$smarty->assign("edit_reading_test_meter5_8", $edit_reading_test_meter5_8);
$smarty->assign("edit_reading_test_meter5_9", $edit_reading_test_meter5_9);
$smarty->assign("edit_reading_test_meter5_10", $edit_reading_test_meter5_10);

$smarty->assign("edit_reading_test_meter6", $edit_reading_test_meter6);
$smarty->assign("edit_reading_test_meter6_1", $edit_reading_test_meter6_1);
$smarty->assign("edit_reading_test_meter6_2", $edit_reading_test_meter6_2);
$smarty->assign("edit_reading_test_meter6_3", $edit_reading_test_meter6_3);
$smarty->assign("edit_reading_test_meter6_4", $edit_reading_test_meter6_4);
$smarty->assign("edit_reading_test_meter6_5", $edit_reading_test_meter6_5);
$smarty->assign("edit_reading_test_meter6_6", $edit_reading_test_meter6_6);
$smarty->assign("edit_reading_test_meter6_7", $edit_reading_test_meter6_7);
$smarty->assign("edit_reading_test_meter6_8", $edit_reading_test_meter6_8);
$smarty->assign("edit_reading_test_meter6_9", $edit_reading_test_meter6_9);
$smarty->assign("edit_reading_test_meter6_10", $edit_reading_test_meter6_10);

$smarty->assign("edit_uuc_json_data", $edit_uuc_json_data);
$smarty->assign("edit_std_meter", $edit_std_meter);
$smarty->assign("edit_test_meter1", $edit_test_meter1);
$smarty->assign("edit_test_meter2", $edit_test_meter2);
$smarty->assign("edit_test_meter3", $edit_test_meter3);
$smarty->assign("edit_test_meter4", $edit_test_meter4);
$smarty->assign("edit_test_meter5", $edit_test_meter5);
$smarty->assign("edit_test_meter6", $edit_test_meter6);
$smarty->assign("edit_test_meter7", $edit_test_meter7);
$smarty->assign("edit_test_meter8", $edit_test_meter8);
$smarty->assign("edit_test_meter9", $edit_test_meter9);
$smarty->assign("edit_test_meter10", $edit_test_meter10);
$smarty->assign("table_info_no_real", $table_info_no_real);
//echo $table_info_no; exit;
if($table_info_no == 19){
    $smarty->display("obs_table_".$table_info_no."_".$obs_type_id.".tpl");
} else {
    $smarty->display("obs_table_".$table_info_no.".tpl");
}
?>