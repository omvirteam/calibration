<?php
include("include/omConfig.php");
$username = "";
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{  
	$s_username         = $_SESSION['s_activId'];
	$username           = "";
  $a                  = 0;
  $gatePassId         = 0;
  $grnNo              = "";
  $grnPrefix          = "";
  $infoSheetNo        = "";
  $srNo               = "";
  $grnDate            = "";
  $itemCode           = "";
  $description        = "";
  $challan            = "";
  $received           = "";
  $condition          = "";
  $useCustId          = "";
  $poNo               = "";
  $poDate             = "";
  $contPerson         = "";
  $phNo               = "";
  $poDate             = "";
  $remarks            = "";
  $expDelivDate       = "";
  $customerId         = "";
  $custName           = "";
  $address            = "";
  $range              = "";
  $insertvar          = "";
  $grnId              = "";
  $gatePassMaxId      = "";
  $grnPrefixSelected  = "";
  $grnIdSelected      = "";
  $grnNoSelected      = "";
  $grnDateSelected    = "";
  $poNoSelected       = "";
  $poDateSelected     = "";
  $custIdSelected     = "";
  $custCodeSelected   = "";
  $contPersonSelected = "";
  $phNoSelected       = "";
  $userNameSelected   = "";
  $remarksSelected    = "";
  $customerIdSelected = "";
  $grnDetailMode      = "";
  $parameterId        = array();
  $parameterName      = array();
  $parameterhidden    = array();
  $grnDetail          = array();
  $gatePassId = $_GET['gatePassId'];
  //Cancel:Start
  if(isset($_POST['cancelBtn']))
  {
      header("Location:grnList.php");
      exit();
  }
  //Cancel:End
  if(isset($_POST['grnPrefix']))
  {
    $updategrnMasterQuery = "UPDATE  grnmaster 
                                SET  grnPrefix   = '".$_POST['grnPrefix']."',
			                               grnNo       = ".$_POST['grnNo'].",
			                               infoSheetNo = ".$_POST['infoSheetNo'].",
			                               grnDate     = '".$_POST['grnDateYear']."-".$_POST['grnDateMonth']."-".$_POST['grnDateDay']."',
			                               customerId  = ".$_POST['customerId'].",
			                               poNo				 = ".$_POST['poNo'].",
			                               poDate      = '".$_POST['poDateYear']."-".$_POST['poDateMonth']."-".$_POST['poDateDay']."',
			                               contPerson  = '".$_POST['contPerson']."',
			                               phNo        = ".$_POST['phNo'].",
			                               remarks     = '".$_POST['remarks']."'
                              WHERE  grnId = ".$_POST['grnId']." ";
    $updategrnMasterQueryRes = mysql_query($updategrnMasterQuery);
  }
  if(isset($_POST['submitBtn']))
  {
    $gatePassId = $_POST['gatePassId'];    
    $insertGateMatserQuery = "UPDATE gatepass
                                 SET deliveryMode = '".$_POST['mode']."'
                               WHERE gatePassId = ".$gatePassId." ";
    $insertGateMaterQueryRes = mysql_query($insertGateMatserQuery);
    $gatePassIdInserted = mysql_insert_Id();
    // Start code Dispach = 0 when all records display//
    $updateDispach = "UPDATE grndetail 
                         SET dispatch = 0, 
                             gatePassId = 0
                       WHERE gatePassId = ".$gatePassId." ";
    $updateDispachQuery = mysql_query($updateDispach);                              
    // End code Dispach = 0 when all records display//
    foreach ($_POST['dispatch'] as $key => $value)
    {
      $insertGateDetailQuery = "UPDATE grndetail 
	                                 SET reportNo = '".$_POST['reportNo'][$key]."',
	                                     gatePassId = ".$gatePassId.",
	                                     dispatch = ".$value."
	                               WHERE grndetailId = ".$key;
	    $insertGateDetailQueryRes = mysql_query($insertGateDetailQuery);
    }
    header("Location: deliveryChallan.php"); 
    exit();
  }
  /////////////////  Customer Name:Start
  $selectName = "SELECT customerId,custName,custCode,address
                   FROM customer";
  $selectNameResult = mysql_query($selectName);
  while($nameRow = mysql_fetch_array($selectNameResult))
  {
    $customerId [$a] = $nameRow['customerId'];
    $custName [$a]   = $nameRow['custName'];
    $custCode[$a]    = $nameRow['custCode'];
    $address[$a]     = $nameRow['address'];
    $a++;
  }
  ///////////////// GRN Prefix Combo : Starts
  $b = 0;
  $selectgrnPrefix = "SELECT grnPrefixId,grnPrefix
                        FROM grnprefix
                       ORDER BY grnPrefix";
  $selectgrnPrefixResult = mysql_query($selectgrnPrefix);
  while($grnPrefixRow = mysql_fetch_array($selectgrnPrefixResult))
  {
    $grnPrefixId [$b]  = $grnPrefixRow['grnPrefixId'];
    $grnPrefix [$b]    = $grnPrefixRow['grnPrefix'];
    $b++;
  }
  ///////////////// GRN Prefix Combo : Ends
  ///////////////// Item Name Combo : Starts
  $itemId = "";
  $itemName = "";
  $d = 0;

  $selectItem = "SELECT itemId,itemName
                   FROM item
                  ORDER BY itemName";
  $selectItemResult = mysql_query($selectItem);


  while($itemRow = mysql_fetch_array($selectItemResult))
  {
    $itemId [$d]  = $itemRow['itemId'];
    $itemName [$d]  = $itemRow['itemName'];
    $d++;
  }
  ///////////////// Item Name Combo : Ends
  ///////////////// parameter Name Combo : Starts
  $parameterId = "";
  $parameterName = "";
  $p = 0;

  $selectparameter = "SELECT parameterId,parameterName
                        FROM parameterentry
                       ORDER BY parameterName";
  $selectparameterResult = mysql_query($selectparameter);


  while($parameterRow = mysql_fetch_array($selectparameterResult))
  {
    $parameterId [$p]  = $parameterRow['parameterId'];
    $parameterName [$p]  = $parameterRow['parameterName'];
    $p++;
  }
  ///////////////// parameter Name Combo : Ends
  //////////////// Selection Of Gate Pass:Start
  //////////////// Selection Of Gate Pass:End
  ///////////////// Listing of GRN Master :  Starts
  $msg       = "";
  $selectGrnId = "SELECT grnId,userName,gatePassId,deliveryMode
                    FROM gatepass
                   WHERE gatePassId =".$_GET['gatePassId'];
  $selectGrnIdRes = mysql_query($selectGrnId);
  if($grnIdRow = mysql_fetch_assoc($selectGrnIdRes))
     $grnId = $grnIdRow['grnId'];
     $userNameSelected= $grnIdRow['userName'];
     $grnDetailMode = $grnIdRow['deliveryMode'];
  $grnmasterQuery = "SELECT grnId,grnPrefix,grnNo,infoSheetNo,grnmaster.grnDate,poNo,
                            poDate,custName,custCode,customer.address,grnmaster.contPerson,phNo,
                            remarks,grnmaster.customerId
                       FROM grnmaster
                       JOIN customer
                      WHERE grnmaster.customerId = customer.customerId
                        AND grnId =".$grnId;
  $grnmasterQueryResult = mysql_query($grnmasterQuery);
  //code for user edit security///
  if($s_username == $userNameSelected || $_SESSION['s_userType'] == "Admin")
 	{
 	}
 	else
 	{
    header("location:index.php");
 	}
  //End code for user edit security///
  while($grnListRow = mysql_fetch_array($grnmasterQueryResult))
  {
    $grnPrefixSelected  = $grnListRow['grnPrefix'];
    $grnIdSelected      = $grnListRow['grnId'];
    $grnNoSelected      = $grnListRow['grnNo'];
    $infoSheetNo        = $grnListRow['infoSheetNo'];
    $grnDateSelected    = $grnListRow['grnDate'];
    $poNoSelected       = $grnListRow['poNo'];
    $poDateSelected     = $grnListRow['poDate'];
    $custIdSelected     = $grnListRow['customerId'];
    $custCodeSelected   = $grnListRow['custCode'];
    $addressSelected    = $grnListRow['address'];
    $contPersonSelected = $grnListRow['contPerson'];
    $phNoSelected       = $grnListRow['phNo'];
    $remarksSelected    = $grnListRow['remarks'];
    $customerIdSelected = $grnListRow['customerId'];
  }
  ///////////////// Listing of GRN Master : Ends
  ///////////////// Listing of GRN Detail : Starts
  $selectGrnEntry  = "SELECT grndetail.itemCode,grndetail.description,grndetail.grnDetailId AS grnDetailId,
                             grndetail.grnId,grndetail.itemId,item.itemName,grndetail.rangeValue,            
                             grndetail.reportNo,grndetail.challan,grndetail.received,grndetail.gatePassId,
                             grndetail.grnCondition,custReqDate,expDelivDate,grndetail.parameterId,grndetail.dispatch,
                             parameterentry.parameterName, grndetail.itemId AS grnDetailItemId
                        FROM grndetail
                        JOIN item ON grndetail.itemId = item.itemId
                        JOIN parameterentry ON grndetail.parameterId = parameterentry.parameterId
                       WHERE grndetail.grnId = ".$grnId."
                         AND grndetail.gatePassId = ".$_GET['gatePassId']."
                       UNION
                      SELECT grndetail.itemCode,grndetail.description,grndetail.grnDetailId  AS grnDetailId,
                             grndetail.grnId,grndetail.itemId,item.itemName,grndetail.rangeValue,            
                             grndetail.reportNo,grndetail.challan,grndetail.received,grndetail.gatePassId,
                             grndetail.grnCondition,custReqDate,expDelivDate,grndetail.parameterId,grndetail.dispatch,
                             parameterentry.parameterName, grndetail.itemId AS grnDetailItemId
                        FROM grndetail
                        JOIN item ON grndetail.itemId = item.itemId
                        JOIN parameterentry ON grndetail.parameterId = parameterentry.parameterId
                       WHERE grndetail.grnId = ".$grnId."
                         AND grndetail.dispatch = 0
                       ORDER BY grnDetailId";

  $selectGrnEntryRes   = mysql_query($selectGrnEntry);
  $prevItemId = 0;
  $a  = 0;
  while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
  {
    $prevItemId = $selectGrnEntryResRow['grnDetailItemId'];
    $grnDetail[$a]['grnDetailId']  = $selectGrnEntryResRow['grnDetailId'];
    $grnDetail[$a]['grnId']        = $selectGrnEntryResRow['grnId'];
    $grnDetail[$a]['itemId']       = $selectGrnEntryResRow['itemId'];
    $grnDetail[$a]['itemName']     = $selectGrnEntryResRow['itemName'];
    $grnDetail[$a]['itemCode']     = $selectGrnEntryResRow['itemCode'];
    $grnDetail[$a]['range']        = $selectGrnEntryResRow['rangeValue'];
    $grnDetail[$a]['description']  = $selectGrnEntryResRow['description'];
    $grnDetail[$a]['reportNo']     = $selectGrnEntryResRow['reportNo'];
    $grnDetail[$a]['challan']      = $selectGrnEntryResRow['challan'];
    $grnDetail[$a]['received']     = $selectGrnEntryResRow['received'];
    $grnDetail[$a]['grnCondition'] = $selectGrnEntryResRow['grnCondition'];
    $grnDetail[$a]['custReqDate']  = $selectGrnEntryResRow['custReqDate'];
    $grnDetail[$a]['expDelivDate'] = $selectGrnEntryResRow['expDelivDate'];
    $grnDetail[$a]['parameterId']  = $selectGrnEntryResRow['parameterId'];
    $grnDetail[$a]['parameterId']  = $selectGrnEntryResRow['parameterId'];
    $grnDetail[$a]['description']  = $selectGrnEntryResRow['description'];
    $grnDetail[$a]['grnDetailChecked'] = ($selectGrnEntryResRow['dispatch'] == 1) ? "CHECKED" : "";
    $a++;
  }
//SELECT OF GRN DETAIL :END
  //grnCondition
  $grnCondition[] = "Ok";
  $grnCondition[] = "Faulty";
  $grnCondition[] = "NotChecked";
  //grnCondition

  include("./bottom.php");
  $smarty->assign("msg",$msg);
  $smarty->assign("grnIdSelected",$grnIdSelected);
  $smarty->assign("grnPrefixSelected",$grnPrefixSelected);
  $smarty->assign("grnNoSelected",$grnNoSelected);
  $smarty->assign("infoSheetNo",$infoSheetNo);
  $smarty->assign("grnDateSelected",$grnDateSelected);
  $smarty->assign("poNoSelected",$poNoSelected);
  $smarty->assign("poDateSelected",$poDateSelected);
  $smarty->assign("custIdSelected",$custIdSelected);
  $smarty->assign("custCodeSelected",$custCodeSelected);
  $smarty->assign("addressSelected",$addressSelected);
  $smarty->assign("contPersonSelected",$contPersonSelected);
  $smarty->assign("phNoSelected",$phNoSelected);
  $smarty->assign("remarksSelected",$remarksSelected);
  $smarty->assign("customerIdSelected",$customerIdSelected);
   $smarty->assign("grnDetailMode",$grnDetailMode);
  $smarty->assign("grnPrefix",$grnPrefix);
  $smarty->assign("grnNo",$grnNo);
  $smarty->assign("grnDate",$grnDate);
  $smarty->assign("customerId",$customerId);
  $smarty->assign("custName",$custName);
  $smarty->assign("poNo",$poNo);
  $smarty->assign("poDate",$poDate);
  $smarty->assign("address",$address);
  $smarty->assign("contPerson",$contPerson);
  $smarty->assign("phNo",$phNo);
  $smarty->assign("remarks",$remarks);
  $smarty->assign("srNo",$srNo);
  $smarty->assign("itemCode",$itemCode);
  $smarty->assign("range",$range);
  $smarty->assign("itemId",$itemId);
  $smarty->assign("itemName",$itemName);
  $smarty->assign("parameterId",$parameterId);
  $smarty->assign("parameterName",$parameterName);
  $smarty->assign("description",$description);
  $smarty->assign("expDelivDate",$expDelivDate);
  $smarty->assign("challan",$challan);
  $smarty->assign("received",$received);
  $smarty->assign("condition",$condition);
  $smarty->assign("parameterhidden",$parameterhidden);
  $smarty->assign("grnDetail",$grnDetail);
  $smarty->assign("grnCondition",$grnCondition);
  $smarty->assign("gatePassId",$gatePassId);
  $smarty->display("deliveryChallanEdit.tpl");
}
?>