<?php
include("include/omConfig.php");
include ("include/functions.inc.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(GRN_MODULE_ID,"edit"))){
  $grnNo              = "";
  $grnPrefix          = "";
  $infoSheetNo        = "";
  $srNo               = "";
  $uuc_location       = "";
  $grnDate            = "";
  $itemCode           = "";
  $description        = "";
  $ulr_no        = "";
  $challan            = "";
  $received           = "";
  $condition          = "";
  $useCustId          = "";
  $poNo               = "";
  $poDate             = "";
  $contPerson         = "";
  $phNo               = "";
  $poDate             = "";
  $remarks            = "";
  $expDelivDate       = "";
  $customerId         = "";
  $custName           = "";
  $address            = "";
  $range              = "";
  $refGrnDetailId     = "";
  $a                  = 0;
  $insertvar          = "";
  $parameterId        = array();
  $parameterName      = array();
  $parameterhidden    = array();
  $grnDetail          = array();
  $grnPrefixSelected  = "";
  $grnIdSelected      = "";
  $grnNoSelected      = "";
  $grnDateSelected    = "";
  $poNoSelected       = "";
  $poDateSelected     = "";
  $custIdSelected     = "";
  $custCodeSelected   = "";
  $addressSelected    = "";
  $contPersonSelected = "";
  $phNoSelected       = "";
  $remarksSelected    = "";
  $customerIdSelected = "";
  $item_type_id = '';
  $item_type_id_array  = array();
  $item_type_name_array = '';
  $item_type_id_selected = '';
  if(isset($_POST['grnDateDay']))
  {
    if(isset($_POST['cancelBtn']))
    {
      header("Location: index.php");
      exit();
    }
    else if(isset($_POST['searchBtn']))
    {
      header("Location: grnList.php");
      exit();
    }
   
    if(isset($_POST['custName']) && strlen($_POST['custName']) > 0)
    { 
      $insertCustQuery = "INSERT INTO customer(custName,custCode,address)
                           VALUE ('".$_POST['custName']."','".$_POST['custCode']."','".$_POST['address']."')";
      $insertCustQueryRes = mysql_query($insertCustQuery);
      if(!$insertCustQueryRes)
        die("Insert Query Not Inserted : ".mysql_error());

      $useCustId = mysql_insert_id();
    }
    else
    
    $useCustId      = $_POST['customerId'];
    $grnDate        = $_POST['grnDateYear']."-".$_POST['grnDateMonth']."-".$_POST['grnDateDay'];
    $poDate         = $_POST['poDateYear']."-".$_POST['poDateMonth']."-".$_POST['poDateDay'];
    if($poDate == '--'){
        $poDate = '';
    } else if($poDate == '-'){
        $poDate = '';
    }
    $date =date_create($grnDate);
    // if (date_format($date,"m") >= 4) {//On or After April (FY is current year - next year)
        // $financial_year_start = (date_format($date,"Y"));
        // $financial_year_end = (date_format($date,"Y")+1);
    // } else {//On or Before March (FY is previous year - current year)
        // $financial_year_start = (date_format($date,"Y")-1);
        // $financial_year_end = date_format($date,"Y");
    // }
	$financial_year_end = date_format($date,"Y");
    $max_ulr_no = '';
    $selectNo = "SELECT MAX(ulr_inc_no) as ulr_inc_no  FROM grndetail
                    WHERE grnId IN ( SELECT grnId FROM grnmaster WHERE grnDate >= '".$financial_year_end."-01-01' AND grnDate <= '".$financial_year_end."-12-31')  ";
    $selectNoResult = mysql_query($selectNo);
    while($noRow = mysql_fetch_array($selectNoResult)){
        $max_ulr_no = $noRow['ulr_inc_no'];
    }
    if(empty($max_ulr_no)){
        $max_ulr_no = 1;
    } else {
        $max_ulr_no = $max_ulr_no + 1;
    }
    $item_type_id   =  null;
//    $item_type_id   =  $_POST['item_type_id'];
//    $item_type_value = '';
//    $selectType = "SELECT short_name  FROM item_type
//                    WHERE id = ".$item_type_id." ";
////        echo $selectType; exit;
//    $selectTypeResult = mysql_query($selectType);
//    while($TypeRow = mysql_fetch_array($selectTypeResult)){
//        $item_type_value = $TypeRow['short_name'];
//    }
    $new_grn_int = $_POST['grnNo'];
//    $grn_no = $_POST['grnPrefix'].$item_type_value."-".$new_grn_int;
    $grn_no = $_POST['grnPrefix']."-".$new_grn_int;
//    -------------------------------;
    if(!empty($poDate)){
        $updateGrnEntry = "UPDATE grnmaster
                          SET grnPrefix = '".$_POST['grnPrefix']."', grnNo = '".$_POST['grnNo']."',grn_no = '".$grn_no."', infoSheetNo = '".$_POST['infoSheetNo']."', grnDate = '".$grnDate."',
                              poNo = '".$_POST['poNo']."', customerId = '".$useCustId."', poDate = '".$poDate."',
                              contPerson = '".$_POST['contPerson']."', phNo = '".$_POST['phNo']."', remarks = '".$_POST['remarks']."',
                              userName = '".$_SESSION['s_activId']."'
                        WHERE grnId = ".$_POST['grnId'];
    } else {
        $updateGrnEntry = "UPDATE grnmaster
                          SET grnPrefix = '".$_POST['grnPrefix']."', grnNo = '".$_POST['grnNo']."',grn_no = '".$grn_no."', infoSheetNo = '".$_POST['infoSheetNo']."', grnDate = '".$grnDate."',
                              poNo = '".$_POST['poNo']."', customerId = '".$useCustId."',
                              contPerson = '".$_POST['contPerson']."', phNo = '".$_POST['phNo']."', remarks = '".$_POST['remarks']."',
                              userName = '".$_SESSION['s_activId']."'
                        WHERE grnId = ".$_POST['grnId'];
    }
    $updateGrnEntryResult = mysql_query($updateGrnEntry);
    if(!$updateGrnEntryResult)
      die("Insert Query Not Inserted 1: ".mysql_error(). " : ".$updateGrnEntry);
    //--------------------------------
//    $deleteGrnDetail = "DELETE FROM grndetail
//                         WHERE grnId = ".$_POST['grnId'];
//    $deleteGrnDetailResult = mysql_query($deleteGrnDetail);
    
    $loopCount = 0;
    while($loopCount < count($_POST['itemId']))
    {
      $itemId         = ($_POST['itemId'][$loopCount] != '') ? $_POST['itemId'][$loopCount] : 0;
      $itemCode       = ($_POST['itemCode'][$loopCount] != '') ? $_POST['itemCode'][$loopCount] : 0;
      $parameterId    = ($_POST['parameterId'][$loopCount] != '') ? $_POST['parameterId'][$loopCount] : 0;
      $range          = ($_POST['range'][$loopCount] != '') ? $_POST['range'][$loopCount] : 0;
      $custReqDate    = ($_POST['custReqDate1'][$loopCount] != '') ? $_POST['custReqDate1'][$loopCount] : 0;
      $expDelivDate  =  ($_POST['expDelivDate'][$loopCount] != '') ?  $_POST['expDelivDate'][$loopCount*3+2]['Year']."-".$_POST['expDelivDate'][$loopCount*3+1]['Month']."-".$_POST['expDelivDate'][$loopCount*3]['Day'] : 0;
      $description    = ($_POST['description'][$loopCount] != '') ? $_POST['description'][$loopCount] : 0;
//      CC238120000008789F
      $challan        = ($_POST['challang'][$loopCount] != '') ? $_POST['challang'][$loopCount] : 0;
      $srNo           = ($_POST['srNo'][$loopCount] != '') ? $_POST['srNo'][$loopCount] : 0;
      $uuc_location   = ($_POST['uuc_location'][$loopCount] != '') ? $_POST['uuc_location'][$loopCount] : '';
      $received       = ($_POST['receivedg'][$loopCount] != '') ? $_POST['receivedg'][$loopCount] : 0;
      $condition      = ($_POST['condition'][$loopCount] != '') ? $_POST['condition'][$loopCount] : 0;
      
      if($_POST['itemId'][$loopCount] != "" || $_POST['itemCode'][$loopCount] != "" || $_POST['parameterId'][$loopCount] != "" || $_POST['range'][$loopCount] != "" || $_POST['description'][$loopCount] != "" ||
         $_POST['chllang'][$loopCount] != "" || $_POST['receivedg'][$loopCount] != "" || $_POST['condition'][$loopCount] != '')
      {
         
        if(isset($_POST['grnDetailId'][$loopCount]) && $_POST['grnDetailId'][$loopCount] > 0)
        {
           
          $updateGrnDetail = "UPDATE grndetail
                                 SET itemId = ".$itemId.",
                                     itemCode = '".$itemCode."',
                                     parameterId = '".$parameterId."',
                                     rangeValue = '".$range."',
                                     custReqDate = '".$custReqDate."',
                                     expDelivDate = '".$expDelivDate."',
                                     description = '".$description."',
                                     challan = '".$challan."',
                                     received = '".$received."',
                                     grnCondition = '".$condition."',
                                     sr_no = '".$srNo."',
                                     uuc_location = '".$uuc_location."'
                               WHERE grnDetailId = ".$_POST['grnDetailId'][$loopCount];
          $updateGrnDetailRes = mysql_query($updateGrnDetail);
        }
        else
        {
            if($_POST['grnPrefix'] == 'R'){
                $ulr_zeros_inc = null;
                $ulr_no  = null;
            } else {
                $ulr_zeros_inc = str_pad($max_ulr_no, 8, '0', STR_PAD_LEFT);
                $ulr_no  = "CC2381".substr($_POST['grnDateYear'],'-2')."0".$ulr_zeros_inc."F";
            }
          $insertQueryGrnDetail = "INSERT INTO grndetail(grnId,itemId,itemCode,parameterId,rangeValue,custReqDate,expDelivDate,description,challan,received,grnCondition,sr_no,uuc_location,ulr_inc_no,ulr_no)
                                   VALUES (".$_POST['grnId'].",".$itemId.",'".$itemCode."','".$parameterId."','".$range."','".$custReqDate."','".$expDelivDate."','".$description."','".$challan."','".$received."','".$condition."','".$srNo."','".$uuc_location."','".$ulr_zeros_inc."','".$ulr_no."')";
          $insertQueryGrnDetailResult = mysql_query($insertQueryGrnDetail);
          $grnDetailId = mysql_insert_id();
          $max_ulr_no = $max_ulr_no + 1;
        }
      }
      $loopCount++;
    }
    if(!empty($_POST['phNo'])){
        $updateGrnEntry = "UPDATE customer
                SET phone1 = '".$_POST['phNo']."'
              WHERE customerId = ".$useCustId;
            $updateGrnEntryResult = mysql_query($updateGrnEntry);
    }
    if(isset($_POST['send_sms'])){
        if(!empty($_POST['phNo']) && (strlen($_POST['phNo']) >= 10)){
            $sms = 'Thank you, your instrument has been received for calibration, we will update you, once it gets calibrated - Team Krishna Instruments 0281-2381444';
            send_sms($_POST['phNo'], $sms);
        }
    }
    if(isset($_POST['grnPrintBtn']))
      header("Location: grnPrintpdf.php?grnId=".$grnId);
    else if(isset($_POST['infoPrintBtn']))
      header("Location: grnPrintpdfInfo.php?grnId=".$grnId);
    else
      header("Location: grnEntry.php");
    exit();
  }

  /////////////////  Customer Name:Start
  $selectName = "SELECT customerId,custName,custCode,address
                   FROM customer
                  ORDER BY custName";
  $selectNameResult = mysql_query($selectName);
  while($nameRow = mysql_fetch_array($selectNameResult))
  {
    $customerId[$a] = $nameRow['customerId'];
    $custName[$a] = $nameRow['custName'];
    $custCode[$a] = $nameRow['custCode'];
    $address[$a] = $nameRow['address'];
    $a++;
  }
  ///////////////// GRN Prefix Combo : Starts
  $b = 0;
  $selectgrnPrefix = "SELECT grnPrefixId,grnPrefix
                        FROM grnprefix
                       ORDER BY grnPrefix";
  $selectgrnPrefixResult = mysql_query($selectgrnPrefix);


  while($grnPrefixRow = mysql_fetch_array($selectgrnPrefixResult))
  {
    $grnPrefixId[$b] = $grnPrefixRow['grnPrefixId'];
    $grnPrefix[$b] = $grnPrefixRow['grnPrefix'];
    $b++;
  }
  ///////////////// GRN Prefix Combo : Ends
  ///////////////// Item Name Combo : Starts
  $itemId = "";
  $itemName = "";
  $d = 0;

  $selectItem = "SELECT itemId,itemName
                   FROM item
                  ORDER BY itemName";
  $selectItemResult = mysql_query($selectItem);


  while($itemRow = mysql_fetch_array($selectItemResult))
  {
    $itemId [$d]  = $itemRow['itemId'];
    $itemName [$d]  = $itemRow['itemName'];
    $d++;
  }
  ///////////////// Item Name Combo : Ends
  ///////////////// parameter Name Combo : Starts
  $parameterId = "";
  $parameterName = "";
  $p = 0;

  $selectparameter = "SELECT parameterId,parameterName
                        FROM parameterentry
                       ORDER BY parameterName";
  $selectparameterResult = mysql_query($selectparameter);


  while($parameterRow = mysql_fetch_array($selectparameterResult))
  {
    $parameterId [$p]  = $parameterRow['parameterId'];
    $parameterName [$p]  = $parameterRow['parameterName'];
    $p++;
  }
  ///////////////// parameter Name Combo : Ends
  ///////////////// Listing of GRN Master :  Starts
  $msg = "";
  $grnmasterQuery = "SELECT grnId,grnPrefix,grnNo,infoSheetNo,grnmaster.grnDate,poNo,item_type_id,
                            poDate,custName,custCode,customer.address,grnmaster.contPerson,phNo,
                            remarks,grnmaster.customerId
                       FROM grnmaster
                       JOIN customer
                      WHERE grnmaster.customerId = customer.customerId
                        AND grnId =".$_GET['grnId'];
  $grnmasterQueryResult = mysql_query($grnmasterQuery);

  while($grnListRow = mysql_fetch_array($grnmasterQueryResult))
  {
    $grnPrefixSelected  = $grnListRow['grnPrefix'];
    $item_type_id_selected  = $grnListRow['item_type_id'];
    $grnIdSelected      = $_GET['grnId'];
    $grnNoSelected      = $grnListRow['grnNo'];
    $infoSheetNo        = $grnListRow['infoSheetNo'];
    $grnDateSelected    = $grnListRow['grnDate'];
    $poNoSelected       = $grnListRow['poNo'];
    $poDateSelected     = $grnListRow['poDate'];
    $custIdSelected     = $grnListRow['customerId'];
    $custCodeSelected   = $grnListRow['custCode'];
    $addressSelected    = $grnListRow['address'];
    $contPersonSelected = $grnListRow['contPerson'];
    $phNoSelected       = $grnListRow['phNo'];
    $remarksSelected    = $grnListRow['remarks'];
    $customerIdSelected = $grnListRow['customerId'];
  }
  ///////////////// Listing of GRN Master : Ends
  ///////////////// Listing of GRN Detail : Starts
  $selectGrnEntry  = "SELECT grnDetailId,grndetail.refGrnDetailId,grndetail.grnId,grndetail.itemId,item.itemName,grndetail.rangeValue,grndetail.itemCode,
                             grndetail.description,ulr_no,grndetail.uuc_location,grndetail.challan,grndetail.received,
                             grndetail.grnCondition,custReqDate,expDelivDate,grndetail.parameterId,
                             parameterentry.parameterName, grndetail.itemId AS grnDetailItemId
                        FROM grndetail
                        JOIN item
                        JOIN parameterentry
                       WHERE grnId = ".$_GET['grnId']."
                         AND grndetail.itemId = item.itemId
                         AND grndetail.parameterId = parameterentry.parameterId
                       ORDER BY grndetail.grnDetailId";
  $selectGrnEntryRes   = mysql_query($selectGrnEntry);

  $prevItemId = 0;
  $a  = 0;
  while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
  {
    $prevItemId = $selectGrnEntryResRow['grnDetailItemId'];
    $grnDetail[$a]['grnDetailId']    = $selectGrnEntryResRow['grnDetailId'];
    $grnDetail[$a]['refGrnDetailId'] = $selectGrnEntryResRow['refGrnDetailId'];
    $grnDetail[$a]['grnId']          = $selectGrnEntryResRow['grnId'];
    $grnDetail[$a]['itemId']         = $selectGrnEntryResRow['itemId'];
    $grnDetail[$a]['itemName']       = $selectGrnEntryResRow['itemName'];
    $grnDetail[$a]['itemCode']       = $selectGrnEntryResRow['itemCode'];
    $grnDetail[$a]['range']          = $selectGrnEntryResRow['rangeValue'];
    $grnDetail[$a]['description']    = $selectGrnEntryResRow['description'];
    $edit_ulr_no = '';
    if(!empty($selectGrnEntryResRow['ulr_no'])){
        $edit_ulr_no1 = substr($selectGrnEntryResRow['ulr_no'], 13); 
        $edit_ulr_no = substr_replace($edit_ulr_no1, "", -1);
    }
//    CC238120000008789F
    $grnDetail[$a]['ulr_no']    = $edit_ulr_no;
    $grnDetail[$a]['uuc_location']    = $selectGrnEntryResRow['uuc_location'];
    $grnDetail[$a]['challan']        = $selectGrnEntryResRow['challan'];
    $grnDetail[$a]['received']       = $selectGrnEntryResRow['received'];
    $grnDetail[$a]['grnCondition']   = $selectGrnEntryResRow['grnCondition'];
    $grnDetail[$a]['custReqDate']    = $selectGrnEntryResRow['custReqDate'];
    $grnDetail[$a]['expDelivDate']   = $selectGrnEntryResRow['expDelivDate'];
    $grnDetail[$a]['parameterId']    = $selectGrnEntryResRow['parameterId'];
    $grnDetail[$a]['parameterName']  = $selectGrnEntryResRow['parameterName'];
    $a++;
  }
//SELECT OF GRN DETAIL :END
  //grnCondition
  $grnCondition[] = "Ok";
  $grnCondition[] = "Faulty";
  $grnCondition[] = "NotChecked";
  //grnCondition
  
  $select_item = "SELECT * FROM item_type";
    $select_itemResult = mysql_query($select_item);
    $j = 0;
    while ($select_itemRow = mysql_fetch_array($select_itemResult))
    {
        $item_type_id_array[$j] = $select_itemRow['id'];
        $item_type_name_array[$j] = $select_itemRow['full_name'];
        $j++;
    }

  include("./bottom.php");
  $smarty->assign("msg",$msg);
  $smarty->assign("grnIdSelected",$grnIdSelected);
  $smarty->assign("grnPrefixSelected",$grnPrefixSelected);
  $smarty->assign("item_type_id_selected",$item_type_id_selected);
  $smarty->assign("grnNoSelected",$grnNoSelected);
  $smarty->assign("infoSheetNo",$infoSheetNo);
  $smarty->assign("grnDateSelected",$grnDateSelected);
  $smarty->assign("poNoSelected",$poNoSelected);
  $smarty->assign("poDateSelected",$poDateSelected);
  $smarty->assign("custIdSelected",$custIdSelected);
  $smarty->assign("custCodeSelected",$custCodeSelected);
  $smarty->assign("addressSelected",$addressSelected);
  $smarty->assign("contPersonSelected",$contPersonSelected);
  $smarty->assign("phNoSelected",$phNoSelected);
  $smarty->assign("remarksSelected",$remarksSelected);
  $smarty->assign("customerIdSelected",$customerIdSelected);
  $smarty->assign("grnPrefix",$grnPrefix);
  $smarty->assign("grnNo",$grnNo);
  $smarty->assign("grnDate",$grnDate);
  $smarty->assign("customerId",$customerId);
  $smarty->assign("custName",$custName);
  $smarty->assign("poNo",$poNo);
  $smarty->assign("poDate",$poDate);
  $smarty->assign("address",$address);
  $smarty->assign("contPerson",$contPerson);
  $smarty->assign("phNo",$phNo);
  $smarty->assign("remarks",$remarks);
  $smarty->assign("refGrnDetailId",$refGrnDetailId);
  $smarty->assign("srNo",$srNo);
  $smarty->assign("uuc_location",$uuc_location);
  $smarty->assign("itemCode",$itemCode);
  $smarty->assign("range",$range);
  $smarty->assign("itemId",$itemId);
  $smarty->assign("itemName",$itemName);
  $smarty->assign("parameterId",$parameterId);
  $smarty->assign("parameterName",$parameterName);
  $smarty->assign("description",$description);
  $smarty->assign("ulr_no",$ulr_no);
  $smarty->assign("expDelivDate",$expDelivDate);
  $smarty->assign("challan",$challan);
  $smarty->assign("received",$received);
  $smarty->assign("condition",$condition);
  $smarty->assign("parameterhidden",$parameterhidden);
  $smarty->assign("grnDetail",$grnDetail);
  $smarty->assign("grnCondition",$grnCondition);
  $smarty->assign("item_type_id_array",$item_type_id_array);
  $smarty->assign("item_type_name_array",$item_type_name_array);
  $smarty->display("grnEdit.tpl");
} else {
  header("Location:index.php");
}  
}

?>