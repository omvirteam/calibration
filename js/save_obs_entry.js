function save_table_1(new_entry = false)
{
    //+e.preventDefault();
    testMeterAvg();
    meanReading();
    var is_range = true;
    is_value_entered_uut = 0;
    is_value_entered_master = 0;
    var rangeIdSame1 =  $('#masterMeterSubSubId').find(':selected').data('range');
    var range_extra = parseFloat(rangeIdSame1) * 10 / 100;
    var rangeIdSame2 = parseFloat(rangeIdSame1) + parseFloat(range_extra);
    var rangeIdSame = parseFloat(rangeIdSame2).toFixed(4);
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.stdMeter').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','stdMeter['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter2').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter2['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter3').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter3['+row_ind+'][]');
            }
        });
    });
        
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.testMeter').each(function(ind,ele){
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
            }
        });

    });

    if(is_value_entered_uut == 0) {
        alert('Enter UUC Reading');
        $('#stdMeter_1').focus();
        return false;
    }
    var table_in_id =  $('#table_info_id').val();
    if($('#table_info_id').val() == '19'){
        
    } else {
        if(is_value_entered_master == 0) {
            alert('Enter Master Reading');
            $('#testMeter_1').focus();
            return false;
        }
    }
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        if(is_range == false) {
            return false;
        }
        $("tr.tableRow:visible").each(function(row_ind,row_ele) {
            $(this).find('.testMeter').each(function(ind,ele) {
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele) {
            if($(this).val() != '') {
                is_value_entered_master = 1;
//                var tmp_val = parseFloat($(this).val()) || 0;
//                if(tmp_val > parseFloat(rangeIdSame)) {
//                    is_range = false;
//                    return false;
//                }
            }
        });
        if(is_range == false) {
            return false;
        }
    });

    if(is_range == true) {
        var form_obj = $("#form1");
        console.log(form_obj.attr('action'));
        console.log(form_obj.serialize());
        $.ajax({
            type: "POST",
            url: form_obj.attr('action'),
            data: form_obj.serialize(), //serializes the form's elements.
            dataType:'json',
            async: false,
            success: function(data) {
                if(data.obs_id && new_entry){
                    window.location.href = "observationSheet.php?obs_id=" + data.obs_id + "&obs_value=" + data.obs_value;
                }
                if(data.is_refresh == 1) {
                    if(data.obs_id) {
                       window.location.href = "observationSheet.php?obs_id=" + data.obs_id + "&obs_value=" + data.obs_value;
                    } else {
                        window.location.href = "observationList.php";
                    }
                }
                $("#mainDiv input[type='text']").val('');
                 var std_values = '   <span class="main_std main_std1">   '  + 
                 '                   <input type="button" value=" - " class="std_remove"><input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" id="stdMeter_1" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                           <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '              </span>  ' ; 
                $(".std_values").html(std_values);
                if(table_in_id == '19'){
                    var testMeter_values =  '   <span class="main_testMeter1 main_testMeter">   '  + 
                     '                   <input type="hidden" name="testMeter[][]" id="testMeter_1" class="testMeter num_only" >  '  + 
                     '                  </span>  ' ; 
                } else {
                    var testMeter_values =  '   <span class="main_testMeter1 main_testMeter">   '  + 
                     '                   <input type="button" value=" - " class="testMeter_remove"><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                }
                
                $(".testMeter_values").html(testMeter_values);
                var testMeter2_values =  '   <span class="main_testMeter21 main_testMeter2">   '  + 
                     '                   <input type="button" value=" - " class="testMeter2_remove"><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                $(".testMeter2_values").html(testMeter2_values);
                var testMeter3_values =  '   <span class="main_testMeter31 main_testMeter3">   '  + 
                     '                   <input type="button" value=" - " class="testMeter3_remove"><input type="text" name="testMeter3[][]" id="testMeter3_1" class="testMeter3 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter3_values").html(testMeter3_values);
                uut_box_total_count();
                master_box_total_count();
                master_box_total_count2();
                master_box_total_count3();
                getDetails();
                get_table_no();
                $('#stdMeter_1').focus();
            }
        });
    } else {
        alert('Not a Range');
        return false;
    }
} 

function save_table_11(new_entry = false)
{
    //+e.preventDefault();
    meanReading();
    var is_range = true;
    is_value_entered_uut = 0;
    is_value_entered_master = 0;
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.stdMeter').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','stdMeter['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
    });

    if(is_value_entered_uut == 0) {
        alert('Enter UUC Reading');
        $('#stdMeter_1').focus();
        return false;
    }

    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        if(is_range == false) {
            return false;
        }
    });

    if(is_range == true) {
        var form_obj = $("#form1");
        console.log(form_obj.attr('action'));
        console.log(form_obj.serialize());
        $.ajax({
            type: "POST",
            url: form_obj.attr('action'),
            data: form_obj.serialize(), //serializes the form's elements.
            dataType:'json',
            async: false,
            success: function(data) {
                if(data.is_refresh == 1) {
                    if(data.obs_id) {
//                        window.location.href = "observationSheet.php?obs_id=" + data.obs_id + "&obs_value=" + data.obs_value;
                        window.location.href = "observationList.php";
                    } else {
                        window.location.href = "observationList.php";
                    }
                }
                $("#mainDiv input[type='text']").val('');
                 var std_values = '   <span class="main_std main_std1">   '  + 
                 '                   <input type="button" value=" - " class="std_remove"><input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" id="stdMeter_1" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                           <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '              </span>  ' ; 
                $(".std_values").html(std_values);
                $("#uuc_slip_reading").val('');
                $("#uuc_rpm_reading").val('');
                uut_box_total_count();
                getDetails();
                get_table_no();
                $('#stdMeter_1').focus();
            }
        });
    } else {
        alert('Not a Range');
        return false;
    }
} 
function save_table_6b(new_entry = false)
{
//    testMeterAvg();
//    meanReading();
    var is_range = true;
    is_value_entered_uut = 0;
    if($('#ct_class').val() == ''){
        alert('Please select C.T. Class');
        return false;
    }
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter2').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter2['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter3').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter3['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter4').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter4['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter5').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter5['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter6').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter6['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter7').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter7['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter8').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter8['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter9').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter9['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter10').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter10['+row_ind+'][]');
            }
        });
    });

    if(is_range == true) {
        var form_obj = $("#form1");
        console.log(form_obj.attr('action'));
        console.log(form_obj.serialize());
        $.ajax({
            type: "POST",
            url: form_obj.attr('action'),
            data: form_obj.serialize(), //serializes the form's elements.
            dataType:'json',
            async: false,
            success: function(data) {
                if(data.is_refresh == 1) {
                    if(data.obs_id) {
//                        window.location.href = "observationSheet.php?obs_id=" + data.obs_id + "&obs_value=" + data.obs_value;
                        window.location.href = "observationList.php";
                    } else {
                        window.location.href = "observationList.php";
                    }
                }
                $("#mainDiv input[type='text']").val('');
                var testMeter_values =  '   <span class="main_testMeter1 main_testMeter">   '  + 
                     '                   <input type="button" value=" - " class="testMeter_remove"><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                    testMeter_values +=  '   <span class="main_testMeter1">   '  + 
                     '                   <input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                    testMeter_values +=  '   <span class="main_testMeter1">   '  + 
                     '                   <input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                    testMeter_values +=  '   <span class="main_testMeter1">   '  + 
                     '                   <input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                    testMeter_values +=  '   <span class="main_testMeter1">   '  + 
                     '                   <input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                $(".testMeter_values").html(testMeter_values);
                var testMeter2_values =  '   <span class="main_testMeter21 main_testMeter2">   '  + 
                     '                   <input type="button" value=" - " class="testMeter2_remove"><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                                        '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                    '</span>  ' ; 
                $(".testMeter2_values").html(testMeter2_values);
                var testMeter3_values =  '   <span class="main_testMeter31 main_testMeter3">   '  + 
                     '                   <input type="button" value=" - " class="testMeter3_remove"><input type="text" name="testMeter3[][]" id="testMeter3_1" class="testMeter3 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter3_values").html(testMeter3_values);
                var testMeter4_values =  '   <span class="main_testMeter41 main_testMeter4">   '  + 
                     '                   <input type="button" value=" - " class="testMeter4_remove"><input type="text" name="testMeter4[][]" id="testMeter4_1" class="testMeter4 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter4_values").html(testMeter4_values);
                var testMeter5_values =  '   <span class="main_testMeter51 main_testMeter5">   '  + 
                     '                   <input type="button" value=" - " class="testMeter5_remove"><input type="text" name="testMeter5[][]" id="testMeter5_1" class="testMeter5 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter5_values").html(testMeter5_values);
                var testMeter6_values =  '   <span class="main_testMeter61 main_testMeter6">   '  + 
                     '                   <input type="button" value=" - " class="testMeter6_remove"><input type="text" name="testMeter6[][]" id="testMeter6_1" class="testMeter6 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter6_values").html(testMeter6_values);
                var testMeter7_values =  '   <span class="main_testMeter71 main_testMeter7">   '  + 
                     '                   <input type="button" value=" - " class="testMeter7_remove"><input type="text" name="testMeter7[][]" id="testMeter7_1" class="testMeter7 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter7_values +=  '   <span class="main_testMeter7"> ' +
                                        '<input type="text" name="testMeter7[][]" class="testMeter7 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter7_values +=  '   <span class="main_testMeter7"> ' +
                                        '<input type="text" name="testMeter7[][]" class="testMeter7 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter7_values +=  '   <span class="main_testMeter7"> ' +
                                        '<input type="text" name="testMeter7[][]" class="testMeter7 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter7_values +=  '   <span class="main_testMeter7"> ' +
                                        '<input type="text" name="testMeter7[][]" class="testMeter7 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter7_values").html(testMeter7_values);
                var testMeter8_values =  '   <span class="main_testMeter81 main_testMeter8">   '  + 
                     '                   <input type="button" value=" - " class="testMeter8_remove"><input type="text" name="testMeter8[][]" id="testMeter8_1" class="testMeter8 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter8_values +=  '   <span class="main_testMeter8"> ' +
                                        '<input type="text" name="testMeter8[][]" class="testMeter8 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter8_values +=  '   <span class="main_testMeter8"> ' +
                                        '<input type="text" name="testMeter8[][]" class="testMeter8 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter8_values +=  '   <span class="main_testMeter8"> ' +
                                        '<input type="text" name="testMeter8[][]" class="testMeter8 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter8_values +=  '   <span class="main_testMeter8"> ' +
                                        '<input type="text" name="testMeter8[][]" class="testMeter8 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter8_values").html(testMeter8_values);
                var testMeter9_values =  '   <span class="main_testMeter91 main_testMeter9">   '  + 
                     '                   <input type="button" value=" - " class="testMeter9_remove"><input type="text" name="testMeter9[][]" id="testMeter9_1" class="testMeter9 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter9_values +=  '   <span class="main_testMeter9"> ' +
                                        '<input type="text" name="testMeter9[][]" class="testMeter9 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter9_values +=  '   <span class="main_testMeter9"> ' +
                                        '<input type="text" name="testMeter9[][]" class="testMeter9 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter9_values +=  '   <span class="main_testMeter9"> ' +
                                        '<input type="text" name="testMeter9[][]" class="testMeter9 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter9_values +=  '   <span class="main_testMeter9"> ' +
                                        '<input type="text" name="testMeter9[][]" class="testMeter9 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter9_values").html(testMeter9_values);
                var testMeter10_values =  '   <span class="main_testMeter101 main_testMeter10">   '  + 
                     '                   <input type="button" value=" - " class="testMeter10_remove"><input type="text" name="testMeter10[][]" id="testMeter10_1" class="testMeter10 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter10_values +=  '   <span class="main_testMeter10"> ' +
                                        '<input type="text" name="testMeter10[][]" class="testMeter10 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter10_values +=  '   <span class="main_testMeter10"> ' +
                                        '<input type="text" name="testMeter10[][]" class="testMeter10 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter10_values +=  '   <span class="main_testMeter10"> ' +
                                        '<input type="text" name="testMeter10[][]" class="testMeter10 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter10_values +=  '   <span class="main_testMeter10"> ' +
                                        '<input type="text" name="testMeter10[][]" class="testMeter10 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter10_values").html(testMeter10_values);
                master_box_total_count();
                master_box_total_count2();
                master_box_total_count3();
                master_box_total_count4();
                master_box_total_count5();
                master_box_total_count6();
                master_box_total_count7();
                master_box_total_count8();
                master_box_total_count9();
                master_box_total_count10();
                getDetails();
                get_table_no();
                $('#testMeter_1').focus();
            }
        });
    } else {
        alert('Not a Range');
        return false;
    }
} 

function save_table_16(new_entry = false)
{
    testMeterAvg();
    meanReading();
    
    var is_range = true;
    is_value_entered_uut = 0;
    is_value_entered_master = 0;
    var rangeIdSame1 =  $('#masterMeterSubSubId').find(':selected').data('range');
    var range_extra = parseFloat(rangeIdSame1) * 10 / 100;
    var rangeIdSame2 = parseFloat(rangeIdSame1) + parseFloat(range_extra);
    var rangeIdSame = parseFloat(rangeIdSame2).toFixed(4);
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.stdMeter').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','stdMeter['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter2').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter2['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter3').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter3['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele) {
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele) {
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        
        $("tr.tableRow:visible").each(function(row_ind,row_ele) {
            $(this).find('.testMeter').each(function(ind,ele) {
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
            }
        });

    });
    
    if(is_value_entered_uut == 0) {
        alert('Enter UUC Reading');
        $('#stdMeter_1').focus();
        return false;
    }
    if(is_value_entered_master == 0) {
        alert('Enter Master Reading');
        $('#testMeter_1').focus();
        return false;
    }
    
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
//                var tmp_val = parseFloat($(this).val()) || 0;
//                if(tmp_val > parseFloat(rangeIdSame)) {
//                    is_range = false;
//                    return false;
//                }
            }
        });
        if(is_range == false) {
            return false;
        }
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
            }
        });
        if(is_range == false) {
            return false;
        }
    });

    if(is_range == true) {
        var form_obj = $("#form1");
        $.ajax({
            type: "POST",
            url: form_obj.attr('action'),
            data: form_obj.serialize(), //serializes the form's elements.
            dataType:'json',
            success: function(data)
            {
                if(data.is_refresh == 1) {
                    if(data.obs_id) {
//                        window.location.href = "observationSheet.php?obs_id=" + data.obs_id + "&obs_value=" + data.obs_value;
                        window.location.href = "observationList.php";
                    } else {
                        window.location.href = "observationList.php";
                    }
                }
                $("#mainDiv input[type='text']").val('');
                 var std_values = '   <span class="main_std main_std1">   '  + 
                 '                   <input type="button" value=" - " class="std_remove"><input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" id="stdMeter_1" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                           <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '              </span>  ' ; 
                $(".std_values").html(std_values);
                var testMeter_values =  '   <span class="main_testMeter1 main_testMeter">   '  + 
                     '                   <input type="button" value=" - " class="testMeter_remove"><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                $(".testMeter_values").html(testMeter_values);
                var testMeter2_values =  '   <span class="main_testMeter21 main_testMeter2">   '  + 
                     '                   <input type="button" value=" - " class="testMeter2_remove"><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter2_values").html(testMeter2_values);
                var testMeter3_values =  '   <span class="main_testMeter31 main_testMeter3">   '  + 
                     '                   <input type="button" value=" - " class="testMeter3_remove"><input type="text" name="testMeter3[][]" id="testMeter3_1" class="testMeter3 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter3_values").html(testMeter3_values);
                uut_box_total_count();
                master_box_total_count();
                master_box_total_count2();
                master_box_total_count3();
                getDetails();
                get_table_no();
                $('#stdMeter_1').focus();
            }
        });
    } else {
        alert('Not a Range');
        return false;
    }
}

function save_table_5(new_entry = false){
    testMeterAvg();
    meanReading();
    
    var is_range = true;
    is_value_entered_uut = 0;
    is_value_entered_master = 0;
    var rangeIdSame1 =  $('#masterMeterSubSubId').find(':selected').data('range');
    var range_extra = parseFloat(rangeIdSame1) * 10 / 100;
    var rangeIdSame2 = parseFloat(rangeIdSame1) + parseFloat(range_extra);
    var rangeIdSame = parseFloat(rangeIdSame2).toFixed(4);
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.stdMeter').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','stdMeter['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter2').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter2['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.testMeter').each(function(ind,ele){
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
            }
        });

    });
    
    if(is_value_entered_uut == 0) {
        alert('Enter UUC Reading');
        $('#stdMeter_1').focus();
        return false;
    }
    if(is_value_entered_master == 0) {
        alert('Enter Master Reading');
        $('#testMeter_1').focus();
        return false;
    }
    
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        if(is_range == false) {
            return false;
        }
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.testMeter').each(function(ind,ele){
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
//                var tmp_val = parseFloat($(this).val()) || 0;
//                if(tmp_val > parseFloat(rangeIdSame)) {
//                    is_range = false;
//                    return false;
//                }
            }
        });
        if(is_range == false) {
            return false;
        }
    });

    if(is_range == true) {
        var form_obj = $("#form1");
        $.ajax({
            type: "POST",
            url: form_obj.attr('action'),
            data: form_obj.serialize(), //serializes the form's elements.
            dataType:'json',
            success: function(data)
            {
                if(data.is_refresh == 1) {
                    if(data.obs_id) {
//                        window.location.href = "observationSheet.php?obs_id=" + data.obs_id + "&obs_value=" + data.obs_value;
                        window.location.href = "observationList.php";
                    } else {
                        window.location.href = "observationList.php";
                    }
                }
                $("#mainDiv input[type='text']").val('');
                 var std_values = '   <span class="main_std main_std1">   '  + 
                 '                   <input type="button" value=" - " class="std_remove"><input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" id="stdMeter_1" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                           <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '              </span>  ' ; 
                $(".std_values").html(std_values);
                var testMeter_values =  '   <span class="main_testMeter1 main_testMeter">   '  + 
                     '                   <input type="button" value=" - " class="testMeter_remove"><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                $(".testMeter_values").html(testMeter_values);
                var testMeter2_values =  '   <span class="main_testMeter21 main_testMeter2">   '  + 
                     '                   <input type="button" value=" - " class="testMeter2_remove"><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter2_values").html(testMeter2_values);
                uut_box_total_count();
                master_box_total_count();
                master_box_total_count2();
                getDetails();
                get_table_no();
                $('#stdMeter_1').focus();
            }
        });
    } else {
        alert('Not a Range');
        return false;
    }
}

function save_table_19(new_entry = false){
    testMeterAvg();
    meanReading();
    
    var is_range = true;
    is_value_entered_uut = 0;
    is_value_entered_master = 0;
    var rangeIdSame1 =  $('#masterMeterSubSubId').find(':selected').data('range');
    var range_extra = parseFloat(rangeIdSame1) * 10 / 100;
    var rangeIdSame2 = parseFloat(rangeIdSame1) + parseFloat(range_extra);
    var rangeIdSame = parseFloat(rangeIdSame2).toFixed(4);
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.stdMeter').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','stdMeter['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter2').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter2['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.testMeter').each(function(ind,ele){
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
            }
        });

    });
    
    if(is_value_entered_uut == 0) {
        alert('Enter UUC Reading');
        $('#stdMeter_1').focus();
        return false;
    }
    if(is_value_entered_master == 0) {
        alert('Enter Master Reading');
        $('#testMeter_1').focus();
        return false;
    }
    
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        if(is_range == false) {
            return false;
        }
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.testMeter').each(function(ind,ele){
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
//                var tmp_val = parseFloat($(this).val()) || 0;
//                if(tmp_val > parseFloat(rangeIdSame)) {
//                    is_range = false;
//                    return false;
//                }
            }
        });
        if(is_range == false) {
            return false;
        }
    });

    if(is_range == true) {
        var form_obj = $("#form1");
        $.ajax({
            type: "POST",
            url: form_obj.attr('action'),
            data: form_obj.serialize(), //serializes the form's elements.
            dataType:'json',
            success: function(data)
            {
                if(data.is_refresh == 1) {
                    if(data.obs_id) {
//                        window.location.href = "observationSheet.php?obs_id=" + data.obs_id + "&obs_value=" + data.obs_value;
                        window.location.href = "observationList.php";
                    } else {
                        window.location.href = "observationList.php";
                    }
                }
                $("#mainDiv input[type='text']").val('');
                 var std_values = '   <span class="main_std main_std1">   '  + 
                 '                   <input type="button" value=" - " class="std_remove"><input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" id="stdMeter_1" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                           <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '              </span>  ' ; 
                $(".std_values").html(std_values);
                var testMeter_values =  '   <span class="main_testMeter1 main_testMeter">   '  + 
                     '                   <input type="button" value=" - " class="testMeter_remove"><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' +
                                        '<span class="main_testMeter">   '  + 
                                        '<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                                        '</span>  ' +
                                        '<span class="main_testMeter">   '  + 
                                        '<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                                        '</span>  ' +
                                        '<span class="main_testMeter">   '  + 
                                        '<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                                        '</span>  ' +
                                        '<span class="main_testMeter">   '  + 
                                        '<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                                        '</span>  ' +
                                        '<span class="main_testMeter">   '  + 
                                        '<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                                        '</span>  ' +
                                        '<span class="main_testMeter">   '  + 
                                        '<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                                        '</span>  ' +
                                        '<span class="main_testMeter">   '  + 
                                        '<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                                        '</span>  ' +
                                        '<span class="main_testMeter">   '  + 
                                        '<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                                        '</span>  ' +
                                        '<span class="main_testMeter">   '  + 
                                        '<input type="text" name="testMeter[][]" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                                        '</span>  ' ; 
                $(".testMeter_values").html(testMeter_values);
                var testMeter2_values =  '   <span class="main_testMeter21 main_testMeter2">   '  + 
                     '                   <input type="button" value=" - " class="testMeter2_remove"><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter2_values").html(testMeter2_values);
                uut_box_total_count();
                master_box_total_count();
                master_box_total_count2();
                getDetails();
                get_table_no();
                $('#stdMeter_1').focus();
            }
        });
    } else {
        alert('Not a Range');
        return false;
    }
}

function save_table_regular_1(new_entry = false){
    testMeterAvg();
    meanReading();
    
    var is_range = true;
    is_value_entered_uut = 0;
    is_value_entered_master = 0;
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.stdMeter').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','stdMeter['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter2').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter2['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.testMeter').each(function(ind,ele){
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
            }
        });

    });
    
    if(is_value_entered_uut == 0) {
        alert('Enter RN Reading');
        $('#stdMeter_1').focus();
        return false;
    }
    if(is_value_entered_master == 0) {
        alert('Enter YN Reading');
        $('#testMeter_1').focus();
        return false;
    }
    
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        if(is_range == false) {
            return false;
        }
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.testMeter').each(function(ind,ele){
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
//                var tmp_val = parseFloat($(this).val()) || 0;
            }
        });
        if(is_range == false) {
            return false;
        }
    });

    if(is_range == true) {
        var form_obj = $("#form1");
        $.ajax({
            type: "POST",
            url: form_obj.attr('action'),
            data: form_obj.serialize(), //serializes the form's elements.
            dataType:'json',
            success: function(data)
            {
                if(data.is_refresh == 1) {
                    if(data.obs_id) {
//                        window.location.href = "observationSheet.php?obs_id=" + data.obs_id + "&obs_value=" + data.obs_value;
                        window.location.href = "observationList.php";
                    } else {
                        window.location.href = "observationList.php";
                    }
                }
                $("#mainDiv input[type='text']").val('');
                 var std_values = '   <span class="main_std main_std1">   '  + 
                 '                   <input type="button" value=" - " class="std_remove"><input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" id="stdMeter_1" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                           <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '              </span>  ' ; 
                $(".std_values").html(std_values);
                var testMeter_values =  '   <span class="main_testMeter1 main_testMeter">   '  + 
                     '                   <input type="button" value=" - " class="testMeter_remove"><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                    testMeter_values +=  '<span class="main_testMeter"> ' +
                                            '<input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                    testMeter_values +=  '<span class="main_testMeter"> ' +
                                            '<input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                    testMeter_values +=  '<span class="main_testMeter"> ' +
                                            '<input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                    testMeter_values +=  '<span class="main_testMeter"> ' +
                                            '<input type="text" name="testMeter[][]" class="testMeter num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter_values").html(testMeter_values);
                var testMeter2_values =  '   <span class="main_testMeter21 main_testMeter2">   '  + 
                     '                   <input type="button" value=" - " class="testMeter2_remove"><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter2_values").html(testMeter2_values);
                uut_box_total_count();
                master_box_total_count();
                master_box_total_count2();
                getDetails();
                get_table_no();
                $('#stdMeter_1').focus();
            }
        });
    } else {
        alert('Not a Range');
        return false;
    }
}

function save_table_17(new_entry = false)
{
    testMeterAvg();
    meanReading();
    
    var is_range = true;
    is_value_entered_uut = 0;
    is_value_entered_master = 0;
    var rangeIdSame1 =  $('#masterMeterSubSubId').find(':selected').data('range');
    var range_extra = parseFloat(rangeIdSame1) * 10 / 100;
    var rangeIdSame2 = parseFloat(rangeIdSame1) + parseFloat(range_extra);
    var rangeIdSame = parseFloat(rangeIdSame2).toFixed(4);
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.stdMeter').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','stdMeter['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter2').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter2['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter3').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter3['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter4').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter4['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter5').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter5['+row_ind+'][]');
            }
        });
    });
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        $(this).find('.testMeter6').each(function(ind,ele){
            if($(this).val() !== ''){
                $(this).attr('name','testMeter6['+row_ind+'][]');
            }
        });
    });
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele) {
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele) {
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        
        $("tr.tableRow:visible").each(function(row_ind,row_ele) {
            $(this).find('.testMeter').each(function(ind,ele) {
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
            }
        });

    });
    
    if(is_value_entered_uut == 0) {
        alert('Enter UUC Reading');
        $('#stdMeter_1').focus();
        return false;
    }
    if(is_value_entered_master == 0) {
        alert('Enter Master Reading');
        $('#testMeter_1').focus();
        return false;
    }
    
    
    $("tr.tableRow:visible").each(function(row_ind,row_ele){
        var row = $(this);
        row.find('.stdMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_uut = 1;
            }
        });
        if(is_range == false) {
            return false;
        }
        $("tr.tableRow:visible").each(function(row_ind,row_ele){
            $(this).find('.testMeter').each(function(ind,ele){
                if($(this).val() !== ''){
                    $(this).attr('name','testMeter['+row_ind+'][]');
                }
            });
        });
        row.find('.testMeter').each(function(ind,ele){
            if($(this).val() != '') {
                is_value_entered_master = 1;
//                var tmp_val = parseFloat($(this).val()) || 0;
//                if(tmp_val > parseFloat(rangeIdSame)) {
//                    is_range = false;
//                    return false;
//                }
            }
        });
        if(is_range == false) {
            return false;
        }
    });

    if(is_range == true) {
        var form_obj = $("#form1");
        $.ajax({
            type: "POST",
            url: form_obj.attr('action'),
            data: form_obj.serialize(), //serializes the form's elements.
            dataType:'json',
            success: function(data)
            {
                if(data.is_refresh == 1) {
                    if(data.obs_id) {
//                        window.location.href = "observationSheet.php?obs_id=" + data.obs_id + "&obs_value=" + data.obs_value;
                        window.location.href = "observationList.php";
                    } else {
                        window.location.href = "observationList.php";
                    }
                }
                $("#mainDiv input[type='text']").val('');
                 var std_values = '   <span class="main_std main_std1">   '  + 
                 '                   <input type="button" value=" - " class="std_remove"><input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" id="stdMeter_1" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                   <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '               </span>  '  + 
                 '               <span class="main_std">   '  + 
                 '                           <input type="text" name="stdMeter[][]" class="stdMeter num_only" size="4" align="right" title="stdMeter"  >  '  + 
                 '              </span>  ' ; 
                $(".std_values").html(std_values);
                var testMeter_values =  '   <span class="main_testMeter1 main_testMeter">   '  + 
                     '                   <input type="button" value=" - " class="testMeter_remove"><input type="text" name="testMeter[][]" id="testMeter_1" class="testMeter num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                $(".testMeter_values").html(testMeter_values);
                var testMeter2_values =  '   <span class="main_testMeter21 main_testMeter2">   '  + 
                     '                   <input type="button" value=" - " class="testMeter2_remove"><input type="text" name="testMeter2[][]" id="testMeter2_1" class="testMeter2 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                 testMeter2_values +=  '<span class="main_testMeter2"> ' +
                                            '<input type="text" name="testMeter2[][]" class="testMeter2 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter2_values").html(testMeter2_values);
                var testMeter3_values =  '   <span class="main_testMeter31 main_testMeter3">   '  + 
                     '                   <input type="button" value=" - " class="testMeter3_remove"><input type="text" name="testMeter3[][]" id="testMeter3_1" class="testMeter3 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter3_values +=  '   <span class="main_testMeter3"> ' +
                                        '<input type="text" name="testMeter3[][]" class="testMeter3 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter3_values").html(testMeter3_values);
                var testMeter4_values =  '   <span class="main_testMeter41 main_testMeter4">   '  + 
                     '                   <input type="button" value=" - " class="testMeter4_remove"><input type="text" name="testMeter4[][]" id="testMeter4_1" class="testMeter4 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter4_values +=  '   <span class="main_testMeter4"> ' +
                                        '<input type="text" name="testMeter4[][]" class="testMeter4 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter4_values").html(testMeter4_values);
                var testMeter5_values =  '   <span class="main_testMeter51 main_testMeter5">   '  + 
                     '                   <input type="button" value=" - " class="testMeter5_remove"><input type="text" name="testMeter5[][]" id="testMeter5_1" class="testMeter5 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter5_values +=  '   <span class="main_testMeter5"> ' +
                                        '<input type="text" name="testMeter5[][]" class="testMeter5 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter5_values").html(testMeter5_values);
                var testMeter6_values =  '   <span class="main_testMeter61 main_testMeter6">   '  + 
                     '                   <input type="button" value=" - " class="testMeter6_remove"><input type="text" name="testMeter6[][]" id="testMeter6_1" class="testMeter6 num_only" size="4" align="right" title="testMeter"   >  '  + 
                     '                  </span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                testMeter6_values +=  '   <span class="main_testMeter6"> ' +
                                        '<input type="text" name="testMeter6[][]" class="testMeter6 num_only"  size="4" align="right" title="testMeter"  > ' +
                                        '</span>  ' ; 
                $(".testMeter6_values").html(testMeter6_values);
                uut_box_total_count();
                master_box_total_count();
                master_box_total_count2();
                master_box_total_count3();
                master_box_total_count4();
                master_box_total_count5();
                master_box_total_count6();
                getDetails();
                get_table_no();
                $('#stdMeter_1').focus();
            }
        });
    } else {
        alert('Not a Range');
        return false;
    }
}
