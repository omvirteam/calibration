<?php
include("include/omConfig.php");

function getOhmsFromTemp($temp) {
  $query_temp_ohms = "SELECT `ohms` FROM `tmp_ohm_relation` WHERE `temp` = " . $temp;
  $query_result_temp_ohms = mysql_query($query_temp_ohms);
  $according_ohms_value = mysql_fetch_array($query_result_temp_ohms);
  return $according_ohms_value['ohms'];
}

function getAccuracyForTable6($ohms) {
  $accu_query_temp_ohms = "SELECT `a`, `b`, `to` FROM `8846A` WHERE `from` <= " . $ohms . " AND `to` > " . $ohms;
  $accu_query_result_temp_ohms = mysql_query($accu_query_temp_ohms);
  $accuracyTol = mysql_fetch_array($accu_query_result_temp_ohms);
  $calcAccuracy = ((((($ohms*$accuracyTol['a'])+(10000*$accuracyTol['b']))/100)*100)/$ohms);
  return $calcAccuracy;
}


$grnObsArray       = array(); //Main Array

/* Column Array Variables : Starts */
$columnArray1      = array(); 
$columnArray2      = array();
$columnArray3      = array();
$columnArray4      = array();
$columnArray5      = array();
/* Column Array Variables : Ends */

$grnId             = "";
$grnDetailId       = "";
$testMeter1        = 0;
$stdMeter2         = 0;
$stdMeter1         = 0;
$testMeter2        = 0;
$stdMeter3         = 0;
$testMeter3        = 0;
$stdMeter4         = 0;
$testMeter4        = 0;
$stdMeter5         = 0;
$testMeter5        = 0;
$stdMeterAverage   = 0;
$testMeterAverage  = 0;
$recordNotFound    = "";
$selectItem = "SELECT grnObservationId,grnmaster.grnNo,grnobservation.grnDetailId,uncertaintyCalibration,uncertaintyCalibrationInPer,resolutionTypeA,stdMeter1,testMeter1,stdMeter2,testMeter2,stdMeter3,testMeter3,stdMeter4,testMeter4,
                      stdMeter5,testMeter5,stdMeterAverage,testMeterAverage,accuracyTaken,standardDeviation,standardUncertinity,standardUncertinityperc,
                      uncertinityForTypeB,uncertinityInPercentage,accuracyForTypeB,acuuracyForTypeBPerc,resolutionTypeB,resolutionForTypeBPerc,
                      stabilityForTypeB,stabilityForTypeBInPerc,combinedUncertinity,combinedUncertinityInPerc,effectiveUncertinity,effectiveUncertinityInPer,
                      effectiveDegreeOfFreed,meanReading,masterMeterReading,error,percentageRdg,expandedUncertinity,mastermetersub.parameter, mastermetersubsub.resolution, mastermetersubsub.uncertinty, mastermetersubsub.degreeOfFreedom,
                      DATE_FORMAT(grndetail.callibrationDate,'%d-%m-%Y') AS callibrationDate, parameterentry.tableNo AS tableNo
                 FROM grnobservation
                 LEFT JOIN grndetail ON grnobservation.grnDetailId = grndetail.grnDetailId
                 LEFT JOIN grnmaster ON grndetail.grnId = grnmaster.grnId
                 LEFT JOIN mastermetersub ON grndetail.masterParameterId = mastermetersub.masterMeterSubId
                 LEFT JOIN mastermetersubsub ON grndetail.masterRangeId = mastermetersubsub.masterMeterSubSubId
                 LEFT JOIN parameterentry ON parameterentry.parameterId = grndetail.parameterId 
                WHERE grnobservation.grnId = ".$_GET['grnId']."
                  AND grnobservation.grnDetailId = ".$_GET['grnDetailId'];
$selectItemResult = mysql_query($selectItem);
$countNumber = mysql_num_rows($selectItemResult);
if($countNumber > 0)
{
  $mainArray = array();
}
else
{
	$mainArray = 0;
	$recordNotFound = "Record Not Found";
}
$ii = 0;
$tableNo = 1;
while($selectItemRow = mysql_fetch_array($selectItemResult))
{
  $tableNo = $selectItemRow['tableNo'];
  // echo var_dump($selectItemRow) . "<br/>"; continue;
  $mainArray['grnId']           = $selectItemRow['grnNo'];
  $mainArray['parameter']       = $selectItemRow['parameter'];
  $mainArray['callibrationDate']       = $selectItemRow['callibrationDate'];
  $mainArray['resolution'][$ii] = $selectItemRow['resolutionTypeA'];
  
  
  if(isset($mainArray['uncertaintyCalibration'][$ii-1]) && $mainArray['uncertaintyCalibration'][$ii-1] != $selectItemRow['uncertaintyCalibration'])
  {
    $mainArray['uncertaintyCalibration'][$ii] = $selectItemRow['uncertaintyCalibration'];
    $mainArray['newTr'][$ii] = 1;
  }
  else
  {
  	$mainArray['uncertaintyCalibration'][$ii] = $selectItemRow['uncertaintyCalibration'];
  	$mainArray['newTr'][$ii] = 0;
  }
  
  if(isset($mainArray['uncertaintyCalibrationInPer'][$ii-1]) && $mainArray['uncertaintyCalibrationInPer'][$ii-1] != $selectItemRow['uncertaintyCalibrationInPer'])
  {
    $mainArray['uncertaintyCalibrationInPer'][$ii] = $selectItemRow['uncertaintyCalibrationInPer'];
    $mainArray['newTr'][$ii] = 1;
  }
  else
  {
  	$mainArray['uncertaintyCalibrationInPer'][$ii] = $selectItemRow['uncertaintyCalibrationInPer'];
  	$mainArray['newTr'][$ii] = 0;
  }
  
  $mainArray['degreeOfFreedom'] = $selectItemRow['degreeOfFreedom'];
  
  $mainArray['accuracyTaken'][$ii] = $selectItemRow['accuracyTaken'];
  $mainArray['stdMtr1'][$ii]           = $selectItemRow['stdMeter1'];
  $mainArray['stdMtr2'][$ii]           = $selectItemRow['stdMeter2'];
  $mainArray['stdMtr3'][$ii]           = $selectItemRow['stdMeter3'];
  $mainArray['stdMtr4'][$ii]           = $selectItemRow['stdMeter4'];
  $mainArray['stdMtr5'][$ii]           = $selectItemRow['stdMeter5'];
  $mainArray['meanReading'][$ii]       = $selectItemRow['meanReading'];
  $mainArray['stdDeviation'][$ii]      = $selectItemRow['standardDeviation'];
  $mainArray['stdUncertinity'][$ii]    = $selectItemRow['standardUncertinity'];
  $mainArray['stdUncertinityPer'][$ii] = $selectItemRow['standardUncertinityperc'];
  $mainArray['uncertOfMaster'][$ii]    = $selectItemRow['uncertinityForTypeB'];
  $mainArray['accuOfCaliCerty'][$ii]   = $selectItemRow['accuracyForTypeB'];
  $mainArray['resolutionTypeB'][$ii]   = $selectItemRow['resolutionTypeB'];
  $mainArray['combinedUncert'][$ii]    = $selectItemRow['combinedUncertinity'];
  $mainArray['expanUncert'][$ii]       = $selectItemRow['expandedUncertinity'];
  $mainArray['expanUncertInPer'][$ii]  = sprintf('%0.4f',($selectItemRow['expandedUncertinity'] * 100) / $selectItemRow['meanReading']);
  if($tableNo == 6) {
    $ohms                                    = getOhmsFromTemp($selectItemRow['testMeter1']);
    $mainArray['stdMtrOhms1'][$ii]           = $ohms;
    $mainArray['stdMtrOhms2'][$ii]           = getOhmsFromTemp($selectItemRow['testMeter2']);
    $mainArray['stdMtrOhms3'][$ii]           = getOhmsFromTemp($selectItemRow['testMeter3']);
    $mainArray['stdMtrOhms4'][$ii]           = getOhmsFromTemp($selectItemRow['testMeter4']);
    $mainArray['stdMtrOhms5'][$ii]           = getOhmsFromTemp($selectItemRow['testMeter5']);
    $accuracy                                = getAccuracyForTable6($ohms);
    $mainArray['meanReadingOhms'][$ii]       = $ohms;
    $mainArray['accuracyTakenOhms'][$ii]     = number_format($accuracy, 4);
    $mainArray['uncertOfMasterOhms'][$ii]    = ($selectItemRow['uncertaintyCalibrationInPer']/2)*$ohms*0.01;
    $mainArray['accuOfCaliCertyOhms'][$ii]   = number_format(($ohms*$accuracy*0.01 / sqrt(3)), 4);
    $va                                      = $mainArray['stdUncertinity'][$ii] * $mainArray['stdUncertinity'][$ii];
    $vb                                      = $mainArray['accuOfCaliCertyOhms'][$ii] * $mainArray['accuOfCaliCertyOhms'][$ii];
    $vc                                      = $mainArray['uncertOfMasterOhms'][$ii] * $mainArray['uncertOfMasterOhms'][$ii];
    $vd                                      = $mainArray['resolutionTypeB'][$ii] * $mainArray['resolutionTypeB'][$ii];
    $combinedUncertOhms                      = sqrt($va+$vb+$vc+$vd);
    $mainArray['combinedUncertOhms'][$ii]    = number_format($combinedUncertOhms, 5);
    $mainArray['expanUncertOhms'][$ii]       = number_format(2*$combinedUncertOhms, 4);
    $mainArray['expanUncertInPerOhms'][$ii]  = number_format(200*$combinedUncertOhms/$ohms, 4);
    
  }
  $ii++;
}
// echo var_dump($mainArray);
// exit;
$selectItem = "SELECT grnObservationId,grnId,grnDetailId,stdMeter1,testMeter1,stdMeter2,testMeter2,stdMeter3,testMeter3,stdMeter4,testMeter4,
                      stdMeter5,testMeter5,stdMeterAverage,testMeterAverage,accuracyTaken,standardDeviation,standardUncertinity,standardUncertinityperc,
                      degreeOfFreedom,uncertinityForTypeB,uncertinityInPercentage,accuracyForTypeB,acuuracyForTypeBPerc,resolutionTypeB,resolutionForTypeBPerc,
                      stabilityForTypeB,stabilityForTypeBInPerc,combinedUncertinity,combinedUncertinityInPerc,effectiveUncertinity,effectiveUncertinityInPer,
                      effectiveDegreeOfFreed,meanReading,masterMeterReading,error,percentageRdg,expandedUncertinity
                 FROM grnobservation
                WHERE grnId = ".$_GET['grnId']."
                  AND grnDetailId = ".$_GET['grnDetailId'];
$selectItemResult = mysql_query($selectItem);
$grnCount = 0;
while($selectItemRow = mysql_fetch_array($selectItemResult))
{
  $grnObsArray[$grnCount]['grnObservationId']    = $selectItemRow['grnObservationId'];
  $grnObsArray[$grnCount]['grnId']               = $selectItemRow['grnId'];
  $grnObsArray[$grnCount]['grnDetailId']         = $selectItemRow['grnDetailId'];
  $grnObsArray[$grnCount]['stdMeter1']           = $selectItemRow['stdMeter1'];
  $grnObsArray[$grnCount]['testMeter1']          = $selectItemRow['testMeter1'];
  $grnObsArray[$grnCount]['stdMeter2']           = $selectItemRow['stdMeter2'];
  $grnObsArray[$grnCount]['testMeter2']          = $selectItemRow['testMeter2'];
  $grnObsArray[$grnCount]['stdMeter3']           = $selectItemRow['stdMeter3'];
  $grnObsArray[$grnCount]['testMeter3']          = $selectItemRow['testMeter3'];
  $grnObsArray[$grnCount]['stdMeter4']           = $selectItemRow['stdMeter4'];
  $grnObsArray[$grnCount]['testMeter4']          = $selectItemRow['testMeter4'];
  $grnObsArray[$grnCount]['stdMeter5']           = $selectItemRow['stdMeter5'];
  $grnObsArray[$grnCount]['testMeter5']          = $selectItemRow['testMeter5'];
  $grnObsArray[$grnCount]['stdMeterAverage']     = $selectItemRow['stdMeterAverage'];
  $grnObsArray[$grnCount]['testMeterAverage']    = $selectItemRow['testMeterAverage'];
  $grnCount++;
}

// echo var_dump($mainArray);
// exit;


$smarty->assign("mainArray",$mainArray);
$smarty->assign("tableNo",$tableNo);
$smarty->assign("grnId",$grnId);
$smarty->assign("grnDetailId",$grnDetailId);
$smarty->assign("grnObsArray",$grnObsArray);
//$smarty->assign("testMeter1Array",$testMeter1Array);
//$smarty->assign("testMeter2Array",$testMeter2Array);
//$smarty->assign("testMeter3Array",$testMeter3Array);
//$smarty->assign("testMeter4Array",$testMeter4Array);
//$smarty->assign("testMeter5Array",$testMeter5Array);
$smarty->assign("testMeter1",$testMeter1);
$smarty->assign("stdMeter2",$stdMeter2);
$smarty->assign("stdMeter1",$stdMeter1); 
$smarty->assign("testMeter2",$testMeter2);
$smarty->assign("stdMeter3",$stdMeter3);
$smarty->assign("testMeter3",$testMeter3);
$smarty->assign("stdMeter4",$stdMeter4);
$smarty->assign("testMeter4",$testMeter4);
$smarty->assign("stdMeter5",$stdMeter5);
$smarty->assign("testMeter5",$testMeter5);
$smarty->assign("stdMeterAverage",$stdMeterAverage);
$smarty->assign("testMeterAverage",$testMeterAverage);
$smarty->assign("recordNotFound",$recordNotFound);
$smarty->display("calUncerDetailAj.tpl");
?>