<?php
    $max_arr = array();
    $sum_arr = array();
    $available_arr = array();
    $reading_json = $reading_json[0];
    $max_arr[] = !empty($reading_json['testMeter']) ? max(array_keys($reading_json['testMeter'])) : '0';
    $sum_arr[] = !empty($reading_json['testMeter']) ? '1' : '0';
    $available_arr[] = !empty($reading_json['testMeter']) ? '1' : '';
    $max_arr[] = !empty($reading_json['testMeter2']) ? max(array_keys($reading_json['testMeter2'])) : '0';
    $sum_arr[] = !empty($reading_json['testMeter2']) ? '1' : '0';
    $available_arr[] = !empty($reading_json['testMeter2']) ? '2' : '';
    $max_arr[] = !empty($reading_json['testMeter3']) ? max(array_keys($reading_json['testMeter3'])) : '0';
    $sum_arr[] = !empty($reading_json['testMeter3']) ? '1' : '0';
    $available_arr[] = !empty($reading_json['testMeter3']) ? '3' : '';
    $max_arr[] = !empty($reading_json['testMeter4']) ? max(array_keys($reading_json['testMeter4'])) : '0';
    $sum_arr[] = !empty($reading_json['testMeter4']) ? '1' : '0';
    $available_arr[] = !empty($reading_json['testMeter4']) ? '4' : '';
    $max_arr[] = !empty($reading_json['testMeter5']) ? max(array_keys($reading_json['testMeter5'])) : '0';
    $sum_arr[] = !empty($reading_json['testMeter5']) ? '1' : '0';
    $available_arr[] = !empty($reading_json['testMeter5']) ? '5' : '';
    
    $available_arr = array_reverse($available_arr, true);
    
    $sum_arr[] = !empty($reading_json['testMeter10']) ? '1' : '0';
    $available_arr[] = !empty($reading_json['testMeter10']) ? '10' : '';
    $sum_arr[] = !empty($reading_json['testMeter9']) ? '1' : '0';
    $available_arr[] = !empty($reading_json['testMeter9']) ? '9' : '';
    $max_arr[] = !empty($reading_json['testMeter10']) ? max(array_keys($reading_json['testMeter10'])) : '0';
    $sum_arr[] = !empty($reading_json['testMeter8']) ? '1' : '0';
    $available_arr[] = !empty($reading_json['testMeter8']) ? '8' : '';
    $max_arr[] = !empty($reading_json['testMeter9']) ? max(array_keys($reading_json['testMeter9'])) : '0';
    $sum_arr[] = !empty($reading_json['testMeter7']) ? '1' : '0';
    $available_arr[] = !empty($reading_json['testMeter7']) ? '7' : '';
    $max_arr[] = !empty($reading_json['testMeter8']) ? max(array_keys($reading_json['testMeter8'])) : '0';
    $available_arr[] = !empty($reading_json['testMeter6']) ? '6' : '';
    $max_arr[] = !empty($reading_json['testMeter7']) ? max(array_keys($reading_json['testMeter7'])) : '0';
    $max_arr[] = !empty($reading_json['testMeter6']) ? max(array_keys($reading_json['testMeter6'])) : '0';
    $sum_arr[] = !empty($reading_json['testMeter6']) ? '1' : '0';
    
    $total_readings = array_sum($sum_arr); 
    $max_readings = max(array_values($max_arr)) + 1; 
    $available_arr = array_filter($available_arr);
    
    $master_data = $master_data[0];
    $uuc_data = $uuc_data[0];
    $type_a_type_b_json = $type_a_type_b_json[0];
    $reading_json = $reading_json;
    
//    echo "<pre>"; print_r($available_arr); exit;
    $ct_class_name = '';
    $qry13 = 'SELECT ct_class FROM accuracy_class
            WHERE id = '.$uuc_data['ct_class'].' ';
    $res13 = mysql_query($qry13) or die("Error :: Cannot select class details.<hr>".mysql_error());
    while($row13 = mysql_fetch_assoc($res13)) {
        $ct_class_name = $row13['ct_class'];
    }
?>
<table width="750" align="left" class="main_table print-friendly"  style="font-size:9.6px;">
    <tr>
        <td colspan="<?= $total_readings + 2; ?>" align="center"><strong>CALCULATION OF UNCERTAINTY</strong></td>
    </tr>
    <tr>
        <td width="" align="center" style="border-top: 1px solid black !important; border-right: none !important;"></td>
        <td width="" style="border-top: 1px solid black !important; border-right: none !important; border-left: none !important;"></td>
        <td colspan="<?= $total_readings; ?>" align="right" style="border-top: 1px solid black !important;"><?php echo $grn_data['certificate_no']; ?></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>GRN NO.</strong></td>
        <td colspan="<?= $total_readings; ?>" align="left"><?php echo $grn_data['grn_no']; ?></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Date</strong></td>
        <td colspan="<?= $total_readings; ?>" align="left"><?php echo $grn_data['callibrationDate']; ?></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Range of UUT</strong></td>
        <?php for ($x = 0; $x < $total_readings; $x++) { ?>
        <td valign="middle"><div style="word-break: break-all; width: 83px; text-align: center; font-family:dejavusans; font-size: 9.6px;" ><?php echo isset($uuc_data['grn_uuc_range']) ? $uuc_data['grn_uuc_range'] : ''; ?></div></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>UUC Accuracy class</strong></td>
        <?php for ($x = 0; $x < $total_readings; $x++) { ?>
            <td align="center"><?php echo isset($ct_class_name) ? $ct_class_name : ''; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Burden</strong></td>
        <?php for ($x = 0; $x < $total_readings; $x++) { ?>
            <td align="center"><?php echo isset($uuc_data['burden_6b']) ? $uuc_data['burden_6b'] : ''; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Ratio</strong></td>
        <?php for ($x = 0; $x < $total_readings; $x++) { ?>
            <td align="center"><?php echo isset($uuc_data['ratio_6b']) ? $uuc_data['ratio_6b'] : ''; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Degree of freedom</strong></td>
        <?php for ($x = 0; $x < $total_readings; $x++) { ?>
            <td align="center"><?php echo isset($master_data['degree_of_freedom']) ? $master_data['degree_of_freedom'] : ''; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Uncertinty of Standard CT in %</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($master_data['uncertinty_from_tracebility']) ? number_format((float)$master_data['uncertinty_from_tracebility'] , 4, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Uncertinty of Burden Box in %</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($master_data['uncertinty_from_tracebility_2']) ? number_format((float)$master_data['uncertinty_from_tracebility_2'] , 4, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Uncertinty of CT Test set in %</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($master_data['uncertinty_from_tracebility_3']) ? number_format((float)$master_data['uncertinty_from_tracebility_3'] , 4, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Accuracy of Standard CT in %</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($master_data['accuracy_taking_account']) ? number_format((float)$master_data['accuracy_taking_account'] , 4, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Accuracy of CT Test set in %</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($master_data['accuracy_taking_account_3']) ? number_format((float)$master_data['accuracy_taking_account_3'] , 4, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Confidance level(%)</strong></td>
        <?php for ($x = 0; $x < $total_readings; $x++) { ?>
            <td align="center"><?php echo isset($uuc_data['confidence_level']) ? $uuc_data['confidence_level'] : ''; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Covarage factor</strong></td>
        <?php for ($x = 0; $x < $total_readings; $x++) { ?>
            <td align="center"><?php echo isset($uuc_data['covarage_factor']) ? $uuc_data['covarage_factor'] : ''; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong></strong></td>
        <?php foreach($available_arr as $meter_no){?>
            <?php if($meter_no == '1' || $meter_no == '6'){?>
                        <td valign="middle" align="center" >Rated Amps.<br/>1% Load</td>
            <?php } else if($meter_no == '2' || $meter_no == '7'){?>
                    <td valign="middle" align="center">Rated Amps.<br/>5% Load</td>
            <?php } else if($meter_no == '3' || $meter_no == '8'){?>
                    <td valign="middle" align="center">Rated Amps.<br/> 20% Load</td>
            <?php } else if($meter_no == '4' || $meter_no == '9'){?>
                    <td valign="middle" align="center">Rated Amps.<br/> 100% Load</td>
            <?php } else if($meter_no == '5' || $meter_no == '10'){?>
                    <td valign="middle" align="center">Rated Amps.<br/> 120% Load</td>
            <?php } ?>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong></strong></td>
        <?php for ($x = 0; $x < $total_readings; $x++) { ?>
            <td align="center">% Ratio Error</td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>&nbsp;</strong></td>
        <td colspan="<?= $total_readings; ?>" align="center"><strong>Type A Uncertainty</strong></td>
    </tr>
    <?php for($i = 0; $i < $max_readings; $i++){ ?>
        <tr>
            <td colspan="2" align="center"><strong><?php echo $i + 1; ?></strong></td>
            <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = $meter_no; }?>
                <td align="center"><?php echo isset($reading_json['testMeter'.$meter_no_with][$i]) ? $reading_json['testMeter'.$meter_no_with][$i] : '0'; ?></td>
            <?php } ?>
        </tr>
    <?php } ?>
    <tr>
        <td align="center"><strong>Mean reading</strong></td>
        <td align="center"><strong>Avg.</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = $meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['testMeter_avg'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['testMeter_avg'.$meter_no_with] , 4, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>Standard deviation</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['std_dev'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['std_dev'.$meter_no_with] , 4, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Standard Uncertainty</strong></td>
        <td align="center"><strong>UA</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['std_uncer'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['std_uncer'.$meter_no_with] , 4, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Standard Uncertainty</strong></td>
        <td align="center"><strong>% U</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['std_uncer_per'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['std_uncer_per'.$meter_no_with] , 4, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td colspan="2" align="center"><strong>&nbsp;</strong></td>
        <td colspan="<?= $total_readings; ?>" align="center"><strong>Type B Uncertainty</strong></td>
    </tr>
    <tr>
        <td align="center"><strong>Degree of freedom</strong></td>
        <td align="center"><strong>µ</strong></td>
        <?php for ($x = 0; $x < $total_readings; $x++) { ?>
            <td align="center"><?php echo isset($master_data['degree_of_freedom']) ? $master_data['degree_of_freedom'] : ''; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Uncertinty of Standard CT in %</strong></td>
        <td align="center"><strong>UB1</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['uncertinty_of_std_ct'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['uncertinty_of_std_ct'.$meter_no_with] , 5, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Uncertinty of Burden Box in %</strong></td>
        <td align="center"><strong>UB2</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['uncertinty_of_burden_box'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['uncertinty_of_burden_box'.$meter_no_with] , 5, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Uncertinty of CT Test set in %</strong></td>
        <td align="center"><strong>UB3</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['uncertinty_of_ct_test'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['uncertinty_of_ct_test'.$meter_no_with] , 5, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Accuracy of Standard CT in %</strong></td>
        <td align="center"><strong>UB4</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['accuracy_of_std_ct'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['accuracy_of_std_ct'.$meter_no_with] , 5, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Accuracy of CT Test set in %</strong></td>
        <td align="center"><strong>UB5</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['accuracy_of_ct_test'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['accuracy_of_ct_test'.$meter_no_with] , 5, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Combined Uncertainty</strong></td>
        <td align="center"><strong>UC</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['com_uncer'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['com_uncer'.$meter_no_with] , 5, '.', '') : '0.0000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Expanded Uncertainty</strong></td>
        <td align="center"><strong>UE</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; if($meter_no == '1'){ } else { $meter_no_with = '_'.$meter_no; }?>
            <td align="center"><?php echo isset($type_a_type_b_json['exp_uncer'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['exp_uncer'.$meter_no_with] , 5, '.', '') : '0.00000'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Calculation</strong></td>
        <td align="center"><strong>%Ue</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; $meter_no_with = '_'.$meter_no; ?>
            <td align="center"><?php echo isset($type_a_type_b_json['exp_uncer_per_ue_without_cmc'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['exp_uncer_per_ue_without_cmc'.$meter_no_with] , 2, '.', '') : '0.00'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>CMC</strong></td>
        <td align="center"><strong></strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; $meter_no_with = '_'.$meter_no; ?>
            <td align="center"><?php echo isset($type_a_type_b_json['cmc']) ? number_format((float)$type_a_type_b_json['cmc'] , 2, '.', '') : '0.00'; ?></td>
        <?php } ?>
    </tr>
    <tr>
        <td align="center"><strong>Expanded Uncertainty</strong></td>
        <td align="center"><strong>%Ue</strong></td>
        <?php foreach($available_arr as $meter_no){ $meter_no_with = ''; $meter_no_with = '_'.$meter_no; ?>
            <td align="center"><?php echo isset($type_a_type_b_json['exp_uncer_per_ue'.$meter_no_with]) ? number_format((float)$type_a_type_b_json['exp_uncer_per_ue'.$meter_no_with] , 2, '.', '') : '0.00'; ?></td>
        <?php } ?>
    </tr>
    
</table>