<?php
include("include/omConfig.php");

function ohms_trim($amount, $perfection = 0) {
	$sign = "&Omega;";
	$postFix = "";
	$pfList = array("", "K", "M", "G");
	$i = 0;
	while($amount >= 1000) {
		$amount /= 1000;
		$i++;
	}	
	$postFix = $pfList[$i] . $sign;
	return number_format($amount, $perfection) . " " . $postFix;
}

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}

ob_start();
?>

<style>
table {font-family: "Times New Roman"; font-size: 12px;}
tr {line-height: 19px;}
.main_table tr td {border: 1px solid black;}
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 1px solid black;
    
}
.main_table td, .main_table th {
    padding: 4px;
    
}
.main_table{
    padding-bottom: 3px;
    
}
</style>

        <?php
        $qry1 = "SELECT * FROM observation_data WHERE id = ".$_GET['obs_data_id']."  ";
	$res1 = mysql_query($qry1) or die("Error :: Cannot select observation_data details.<hr>".mysql_error());
        $grnobsmaster_data = array();
        while($row1 = mysql_fetch_assoc($res1)) { 
            array_push($grnobsmaster_data,$row1);
	}
        $uuc_json_data = json_decode($grnobsmaster_data[0]['uuc_json']);
        $uuc_data_d = json_decode(json_encode($uuc_json_data), true);
        $uuc_data = $uuc_data_d[$_GET['key_value']];
        
        $master_json_data = json_decode($grnobsmaster_data[0]['master_json']);
        $master_data_d = json_decode(json_encode($master_json_data), true);
        $master_data = $master_data_d[$_GET['key_value']];
        
        $ab_json_data = json_decode($grnobsmaster_data[0]['type_a_type_b_json']);
        $ab_data_d = json_decode(json_encode($ab_json_data), true);
        $ab_data = $ab_data_d[$_GET['key_value']];
        
        $consider_ue = '';
        $ue_unit = '';
        if(isset($master_data['consider_ue']) && !empty($master_data['consider_ue'])){
            $consider_ue = '1';
            $ue_unit = isset($master_data['ue_unit']) ? $master_data['ue_unit'] : '';
        }
//        echo "<pre>"; print_r($master_data); exit;
        ?>
        
            <table width="100%" align="center" class="main_table print-friendly"  style="font-size:13px;">
                <tr>
                    <th colspan="9" align="center" valign="middle">Uncertainty Budget</th>
                </tr>
                <tr>
                    <th align="center" valign="middle">Source of uncertainty Xi</th>
                    <th align="center" valign="middle">Estimation Xi </th>
                    <th align="center" valign="middle">Limits<br/>%</th>
                    <th align="center" valign="middle">Probability <br/>Type A 0r<br/> B-factor</th>
                    <th align="center" valign="middle">Diviser</th>
                    <th align="center" valign="middle">Standard <br/>Uncertainty <br/>U(Xi) </th>
                    <th align="center" valign="middle">Sensitivity<br/> Coefficient</th>
                    <th align="center" valign="middle">Uncertainty <br/>Contribution<br/> u(yi)</th>
                    <th align="center" valign="middle">Deg. of<br/> Freedom <br/>v eff</th>
                </tr>
                <tr>
                    <td align="center" valign="middle">Uncertainty of master meter<br/> U1</td>
                    <td align="center">&nbsp;</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$master_data['uncertinty_from_tracebility'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">B <br/>normal</td>
                    <td align="center" valign="middle">2</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['uncer_master'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">1</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['uncer_master'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">infinity</td>
                </tr>
                <tr>
                    <td align="center" valign="middle">Accuracy of cali.certi.<br/> U2</td>
                    <td align="center">&nbsp;</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['r_phase_accuracy'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">B <br/>Rectangular</td>
                    <td align="center" valign="middle">sqrt(3)</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['acc_calibration'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">1</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['acc_calibration'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">infinity</td>
                </tr>
                <tr>
                    <td align="center" valign="middle" valign="middle">Resolution <br/> U3</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['uuc_least_count'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">B <br/>Rectangular</td>
                    <td align="center" valign="middle">sqrt(3)</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['resolution_new'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">1</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['resolution_new'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">infinity</td>
                </tr>
                <tr>
                    <td align="center" valign="middle">Standard <br/>Uncertainty</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['std_dev'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">A <br/>normal</td>
                    <td align="center" valign="middle">sqrt(5)</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['std_uncer'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">1</td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['std_uncer'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle"><?php echo $master_data['degree_of_freedom']; ?></td>
                </tr>
                <tr>
                    <td align="center" valign="middle">Combined <br/>Uncertainty</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle"></td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['com_uncer'], 4, '.', ''); ?></td>
                    <td align="center" valign="middle">infinity</td>
                </tr>
                <tr>
                    <td align="center" valign="middle">Expanded <br/>Uncertainty Ue</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle"></td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['exp_uncer'], 2, '.', ''); ?></td>
                    <td align="center" valign="middle">infinity</td>
                </tr>
                <tr>
                    <?php if(!empty($consider_ue)){ ?>
                        <td align="center" valign="middle">Expanded <br/>Uncertainty Ue <?php echo $ue_unit; ?></td>
                    <?php } else { ?>
                        <td align="center" valign="middle">Expanded <br/>Uncertainty Ue %</td>
                    <?php } ?>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle">&nbsp;</td>
                    <td align="center" valign="middle"></td>
                    <td align="center" valign="middle"><?php echo number_format((float)$ab_data['exp_uncer_per_ue_without_cmc'], 2, '.', ''); ?></td>
                    <td align="center" valign="middle">infinity</td>
                </tr>
            </table>
<br/>


<?php
// html - end //
//exit;
$content = ob_get_clean();
require_once 'html2pdf/html2pdf.class.php';
try {
    $html2pdf = new HTML2PDF('P', 'A4', 'fr',true,'UTF-8',array(7, 7, 7, 7));
    $html2pdf->pdf->SetTitle('Observation Sheet');
    $html2pdf->shrink_tables_to_fit = 1;    // Default: false
    $html2pdf->setDefaultFont('Times');
    $html2pdf->AddFont('dejavusans');
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output('certificate_print.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>