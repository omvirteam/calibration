<?php
include("include/omConfig.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = "";
  $grnDetailId         = "";
  $grnPrefix           = "";
  $grnNo               = "";
  $grnDate             = "";
  $poNo                = "";
  $poDate              = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $mrAndMrs            = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();

  //SELECT OF GRN MASTER :START
  $selectGrnMaster = "SELECT grnPrefix,grnNo,DATE_FORMAT(grnDate,'%d-%m-%Y') AS grnDate,poNo,DATE_FORMAT(poDate,'%d-%m-%Y') AS poDate,custName,custCode,remarks,grnmaster.mrAndMrs,grnmaster.contPerson,phNo,userName
                        FROM grnmaster
                        JOIN customer
                       WHERE grnId = ".$_GET['grnId']."
                         AND grnmaster.customerId = customer.customerId";
  $selectGrnMasterRes = mysql_query($selectGrnMaster);
  if($selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes))
  {
    $grnPrefix  = $selectGrnMasterRow['grnPrefix'];
    $grnNo      = $selectGrnMasterRow['grnNo'];
    $grnDate    = $selectGrnMasterRow['grnDate'];
    $poNo       = $selectGrnMasterRow['poNo'];
    $poDate     = $selectGrnMasterRow['poDate'];
    $custName   = $selectGrnMasterRow['custName'];
    $custCode   = $selectGrnMasterRow['custCode'];
    $remarks    = $selectGrnMasterRow['remarks'];
    $mrAndMrs   = $selectGrnMasterRow['mrAndMrs'];
    $contPerson = $selectGrnMasterRow['contPerson'];
    $phNo       = $selectGrnMasterRow['phNo'];
    $userName   = $selectGrnMasterRow['userName'];
  }
  //SELECT OF GRN MASTER :END
  //SELECT OF GRN DETAIL :START
  $selectGrnEntry  = "SELECT grndetail.grnId,item.itemName,grndetail.rangeValue,grndetail.itemCode,grndetail.description,grndetail.description,grndetail.challan,grndetail.received,
                             grndetail.grnCondition,custReqDate,expDelivDate,
                             parameterentry.parameterName, grndetail.itemId AS grnDetailItemId
                        FROM grndetail
                        JOIN item
                        JOIN parameterentry
                       WHERE grnId = ".$_GET['grnId']."
                         AND grndetail.itemId = item.itemId
                         AND grndetail.parameterId = parameterentry.parameterId
                       ORDER BY grndetail.grnDetailId";

  $selectGrnEntryRes   = mysql_query($selectGrnEntry);

  $prevItemId = 0;
  $a  = 0;
  while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
  {
    if($a > 0 && $prevItemId == $selectGrnEntryResRow['grnDetailItemId'])
    {
      $grnDetail[$a - 1]['parameterName'] .= ", ".$selectGrnEntryResRow['parameterName'];
    }
    else
    {
      $prevItemId = $selectGrnEntryResRow['grnDetailItemId'];
      $grnDetail[$a]['grnId']        = $selectGrnEntryResRow['grnId'];
      $grnDetail[$a]['itemName']     = $selectGrnEntryResRow['itemName'];
      $grnDetail[$a]['itemCode']     = $selectGrnEntryResRow['itemCode'];
      $grnDetail[$a]['range']        = $selectGrnEntryResRow['rangeValue'];
      $grnDetail[$a]['description']  = $selectGrnEntryResRow['description'];
      $grnDetail[$a]['challan']      = $selectGrnEntryResRow['challan'];
      $grnDetail[$a]['received']     = $selectGrnEntryResRow['received'];
      $grnDetail[$a]['grnCondition'] = $selectGrnEntryResRow['grnCondition'];
      $grnDetail[$a]['custReqDate']  = $selectGrnEntryResRow['custReqDate'];
      $grnDetail[$a]['expDelivDate'] = $selectGrnEntryResRow['expDelivDate'];
      $grnDetail[$a]['parameterName']= $selectGrnEntryResRow['parameterName'];
      $grnDetail[$a]['description']  = $selectGrnEntryResRow['description'];
      $a++;
    }
  }
//SELECT OF GRN DETAIL :END

  include("./bottom.php");
  $smarty->assign("grnId",$grnId);
  $smarty->assign("grnDetail",$grnDetail);
  $smarty->assign("grnDetailId",$grnDetailId);
  $smarty->assign("grnPrefix",$grnPrefix);
  $smarty->assign("grnNo",$grnNo);
  $smarty->assign("grnDate",$grnDate);
  $smarty->assign("poNo",$poNo);
  $smarty->assign("poDate",$poDate);
  $smarty->assign("custName",$custName);
  $smarty->assign("custCode",$custCode);
  $smarty->assign("mrAndMrs",$mrAndMrs);
  $smarty->assign("contPerson",$contPerson);
  $smarty->assign("phNo",$phNo);
  $smarty->assign("remarks",$remarks);
  $smarty->assign("userName",$userName);
  $smarty->assign("itemId",$itemId);
  $smarty->assign("a",$a);
  $smarty->assign("msg",$msg);
 
  if($_GET['display'] == 'grnDetail')
    $smarty->display("grnPrint_newaddr.tpl");
  else if($_GET['display'] == 'info')
    $smarty->display("infoSheetPrint_newaddr.tpl");
  else
    $smarty->display("allPrint_newaddr.tpl");
}
?>