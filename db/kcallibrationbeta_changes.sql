-- Shailesh : 2020_03_13 02:30 PM
CREATE TABLE IF NOT EXISTS `grnobservation_stdmeter` (
  `stdMeterId` int(11) NOT NULL AUTO_INCREMENT,
  `grnObservationId` int(11) DEFAULT NULL,
  `stdMeter` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`stdMeterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- Chirag : 2020_03_19 06:30 PM
CREATE TABLE IF NOT EXISTS `grnobservation_testMeter` (
  `testMeterId` int(11) NOT NULL AUTO_INCREMENT,
  `grnObservationId` int(11) DEFAULT NULL,
  `testMeter` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`testMeterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- Chirag : 2020_03_21 02:20 PM
ALTER TABLE `mastermetersubsub` ADD `range_unit` VARCHAR(255) NULL DEFAULT NULL AFTER `accuracySubSubFS`, ADD `frequency` VARCHAR(255) NULL DEFAULT NULL AFTER `range_unit`;

-- Chirag : 2020_03_24 06:40 PM
ALTER TABLE `grnobservation` ADD `accuracy_of_rdg` DOUBLE NULL DEFAULT NULL AFTER `expandedUncertinityInPre`, ADD `accuracy_of_fs` DOUBLE NULL DEFAULT NULL AFTER `accuracy_of_rdg`, ADD `master_stability` VARCHAR(255) NULL DEFAULT NULL AFTER `accuracy_of_fs`, ADD `degree_of_freedom` VARCHAR(255) NULL DEFAULT NULL AFTER `master_stability`, ADD `uncertinty_from_tracebility` VARCHAR(255) NULL DEFAULT NULL AFTER `degree_of_freedom`, ADD `accuracy_taking_account` VARCHAR(255) NULL DEFAULT NULL AFTER `uncertinty_from_tracebility`, ADD `resolution_of_master` VARCHAR(255) NULL DEFAULT NULL AFTER `accuracy_taking_account`, ADD `confidence_level` VARCHAR(255) NULL DEFAULT NULL AFTER `resolution_of_master`, ADD `covarage_factor` VARCHAR(255) NULL DEFAULT NULL AFTER `confidence_level`;

-- Chirag : 2020_03_25 03:15 PM
ALTER TABLE `grnobservation` ADD `degree_of_freedom_add` VARCHAR(255) NULL DEFAULT NULL AFTER `uncertinityForTypeB`;
ALTER TABLE `grnobservation` ADD `stability_of_source` DOUBLE NULL DEFAULT NULL AFTER `combinedUncertinity`;

-- Chirag : 2020_03_30 11:45 AM
ALTER TABLE `mastermetersubsub` ADD `accuracy_main` DOUBLE NULL DEFAULT NULL AFTER `accuracySubSubFS`;

-- Chirag : 2020_03_31 03:45 PM
CREATE TABLE `website_modules` (
  `website_module_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `main_module` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `website_modules`
  ADD PRIMARY KEY (`website_module_id`);

ALTER TABLE `website_modules`
  MODIFY `website_module_id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `module_roles` (
  `module_role_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `website_module_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `module_roles`
  ADD PRIMARY KEY (`module_role_id`);

ALTER TABLE `module_roles`
  MODIFY `module_role_id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `website_module_id` int(11) DEFAULT NULL,
  `role_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_role_id`);

ALTER TABLE `user_roles`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (1, 'Master', '1');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '1');
INSERT INTO `user_roles` (`user_role_id`, `user_id`, `website_module_id`, `role_type_id`) VALUES (1, 1, 1, 1);

-- Chirag : 2020_04_01 07:20 PM
ALTER TABLE `grnobservation` ADD `phase_type_id` INT(11) NULL DEFAULT NULL AFTER `covarage_factor`;
ALTER TABLE `grnobservation` ADD `uut_range` VARCHAR(255) NULL DEFAULT NULL AFTER `phase_type_id`;

-- Chirag : 2020_04_02 05:20 PM
INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (2, 'Master >> Grn Prefix', '1.1');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '2');

-- Chirag : 2020_04_03 02:20 PM
ALTER TABLE `grnobservation` ADD `table_no` INT(11) NULL DEFAULT NULL AFTER `uut_range`;
ALTER TABLE `grnobservation` ADD `table_title` VARCHAR(255) NULL DEFAULT NULL AFTER `table_no`;

-- Chirag : 2020_04_03 05:20 PM
INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (3, 'Master >> Item', '1.2');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '3');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'add', 'add', '3');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'edit', 'edit', '3');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'delete', 'delete', '3');

INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (4, 'Master >> Parameter', '1.3');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '4');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'add', 'add', '4');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'edit', 'edit', '4');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'delete', 'delete', '4');

INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (5, 'Master >> Master Meter', '1.4');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '5');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'add', 'add', '5');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'edit', 'edit', '5');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'delete', 'delete', '5');

INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (6, 'Master >> Master Meter Sub', '1.5');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '6');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'add', 'add', '6');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'edit', 'edit', '6');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'delete', 'delete', '6');

INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (7, 'Master >> Customer', '1.6');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '7');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'add', 'add', '7');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'edit', 'edit', '7');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'delete', 'delete', '7');

-- Chirag : 2020_04_04 05:00 PM
ALTER TABLE `grnobservation` ADD `leastCount` DOUBLE NULL DEFAULT NULL AFTER `table_title`;

-- Chirag : 2020_04_09 05:00 PM
INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (8, 'Master >> Symbol', '1.7');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '8');

INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (9, 'Master >> User Rights', '1.8');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '9');

INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (10, 'G.R.N', '2');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '10');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'item view', 'item view', '10');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'add', 'add', '10');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'edit', 'edit', '10');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'delete', 'delete', '10');

-- Chirag : 2020_04_11 11:45 AM
INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (11, 'Staff Entry', '3');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '11');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'add', 'add', '11');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'edit', 'edit', '11');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'delete', 'delete', '11');

INSERT INTO `website_modules` (`website_module_id`, `title`, `main_module`) VALUES (12, 'Observation Sheet', '4');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'view', 'view', '12');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'add', 'add', '12');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'edit', 'edit', '12');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'delete', 'delete', '12');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'cal uncer. view', 'cal uncer. view', '12');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'certi. view', 'certi. view', '12');
INSERT INTO `module_roles` (`module_role_id`, `title`, `role_name`, `website_module_id`) VALUES (NULL, 'inst. dues', 'inst. dues', '12');

-- Chirag : 2020_04_22 11:45 AM
ALTER TABLE `grndetail` ADD `cali_location` INT(11) NULL DEFAULT NULL AFTER `userName`;

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `setting_name` varchar(255) DEFAULT NULL,
  `setting_value` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `setting` (`id`, `setting_name`, `setting_value`) VALUES
(1, 'traceability_to', 'Equipments used for calibration are tracebled to National/International Standards.'),
(2, 'approved_by', 'D.R.SHAH, C.E.O.');

ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

-- Chirag : 2020_04_23 03:45 PM
ALTER TABLE `mastermeter` ADD `page_title` VARCHAR(255) NULL DEFAULT NULL AFTER `masterUncertaintyText`, ADD `master_temp` VARCHAR(255) NULL DEFAULT NULL AFTER `page_title`, ADD `master_humidity` VARCHAR(255) NULL DEFAULT NULL AFTER `master_temp`;
ALTER TABLE `grndetail` ADD `uuc_location` VARCHAR(255) NULL DEFAULT NULL AFTER `cali_location`;
INSERT INTO `setting` (`id`, `setting_name`, `setting_value`) VALUES (NULL, 'footer_note_for_page_1', '1 This report pertains to particular sample /Instrument submitted for test.<br/>\r\n2 This certificate may not be reproduced, except in full, without prior written permission of Krishna Instruments.<br/>\r\n3 This calibration results reported in the certificate are valid at the time of measurements and under stated condition.<br/>'), (NULL, 'footer_note_for_all', 'footer_note_for_all');

-- Chirag : 2020_04_25 11:00 PM
ALTER TABLE `grndetail` ADD `sr_no` VARCHAR(255) NULL DEFAULT NULL AFTER `uuc_location`;
ALTER TABLE `grndetail` ADD `approved_by` int(11) NULL DEFAULT NULL AFTER `sr_no`;

CREATE TABLE `approved_by` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `approved_by` (`id`, `name`, `designation`) VALUES
(1, 'D.R.SHAH', 'C.E.O'),
(2, 'SAMEER SHAH', 'C.E.O');

ALTER TABLE `approved_by`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `approved_by`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

-- Chirag : 2020_04_30 03:40 PM
INSERT INTO `setting` (`id`, `setting_name`, `setting_value`) VALUES (NULL, 'page_1_remark', 'page_1_remark');


CREATE TABLE `item_type` (
  `id` int(11) NOT NULL,
  `short_name` varchar(10) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `item_type` (`id`, `short_name`, `full_name`) VALUES
(1, 'E', 'E-Electirical'),
(2, 'M', 'M-Mechanical'),
(3, 'F', 'F-Fluid Flow');

ALTER TABLE `item_type`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `item_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `grndetail` ADD `ulr_no` VARCHAR(50) NULL DEFAULT NULL AFTER `approved_by`;
ALTER TABLE `grndetail` ADD `item_type_id` INT(11) NULL DEFAULT NULL AFTER `ulr_no`;

-- Chirag : 2020_05_01 04:40 PM
ALTER TABLE `grnmaster` ADD `item_type_id` INT(11) NULL DEFAULT NULL AFTER `userName`;
ALTER TABLE `grnmaster` ADD `grn_no_int` VARCHAR(255) NULL DEFAULT NULL AFTER `item_type_id`, ADD `grn_no` VARCHAR(255) NULL DEFAULT NULL AFTER `grn_no_int`;

CREATE TABLE `month_code` (
  `id` int(11) NOT NULL,
  `month_name` varchar(50) DEFAULT NULL,
  `month_code` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `month_code` (`id`, `month_name`, `month_code`) VALUES
(1, 'January', 'A'),
(2, 'February', 'B'),
(3, 'March', 'C'),
(4, 'April', 'D'),
(5, 'May', 'E'),
(6, 'June', 'F'),
(7, 'July', 'G'),
(8, 'August', 'H'),
(9, 'September', 'J'),
(10, 'October', 'K'),
(11, 'November', 'L'),
(12, 'December', 'M');

ALTER TABLE `month_code`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `month_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

CREATE TABLE `year_code` (
  `id` int(11) NOT NULL,
  `year_no` varchar(50) DEFAULT NULL,
  `year_code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `year_code` (`id`, `year_no`, `year_code`) VALUES
(1, '2019', 'M'),
(2, '2020', 'N'),
(3, '2021', 'P'),
(4, '2022', 'Q'),
(5, '2023', 'R');

ALTER TABLE `year_code`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `year_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

ALTER TABLE `grndetail` ADD `certificate_no` VARCHAR(50) NULL DEFAULT NULL AFTER `item_type_id`;

-- Chirag : 2020_05_09 07:10 PM
CREATE TABLE `grnobservation_testMeter2` (
  `testMeter2Id` int(11) NOT NULL,
  `grnObservationId` int(11) DEFAULT NULL,
  `testMeter2` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `grnobservation_testMeter2`
  ADD PRIMARY KEY (`testMeter2Id`);

ALTER TABLE `grnobservation_testMeter2`
  MODIFY `testMeter2Id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `grnobservation_testMeter3` (
  `testMeter3Id` int(11) NOT NULL,
  `grnObservationId` int(11) DEFAULT NULL,
  `testMeter3` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `grnobservation_testMeter3`
  ADD PRIMARY KEY (`testMeter3Id`);

ALTER TABLE `grnobservation_testMeter3`
  MODIFY `testMeter3Id` int(11) NOT NULL AUTO_INCREMENT;

-- Chirag : 2020_05_13 06:10 PM
ALTER TABLE `grnobservation` ADD `table_info_id` INT(11) NULL DEFAULT NULL AFTER `table_no`;

CREATE TABLE `table_info` (
  `table_info_id` int(11) NOT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`) VALUES
(1, '1', '/images/table_info/table_1.png'),
(2, '16', '/images/table_info/table_16.png');

ALTER TABLE `table_info`
  ADD PRIMARY KEY (`table_info_id`);

ALTER TABLE `table_info`
  MODIFY `table_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

-- Chirag : 2020_05_16 03:15 PM
ALTER TABLE `table_info` ADD `note` TEXT NULL DEFAULT NULL AFTER `image_url`, ADD `template_path` TEXT NULL DEFAULT NULL AFTER `note`, ADD `created_by` INT(11) NULL DEFAULT NULL AFTER `template_path`, ADD `created_at` DATETIME NULL DEFAULT NULL AFTER `created_by`, ADD `updated_by` INT(11) NULL DEFAULT NULL AFTER `created_at`, ADD `updated_at` DATETIME NULL DEFAULT NULL AFTER `updated_by`;

UPDATE `table_info` SET `note` = 'table info 1 note' WHERE `table_info_id` = 1;

UPDATE `table_info` SET `note` = 'table info 16 note' WHERE `table_info_id` = 2;

-- Chirag : 2020_05_18 03:45 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`) VALUES (3, '3', '/images/table_info/table_1.png');
UPDATE `table_info` SET `note` = 'table info 3 note' WHERE `table_info`.`table_info_id` = 3;

-- Chirag : 2020_05_21 02:30 PM
ALTER TABLE `mastermetersubsub` ADD `coverage_factor` DOUBLE NULL DEFAULT NULL AFTER `degreeOfFreedom`, ADD `cmc` DOUBLE NULL DEFAULT NULL AFTER `coverage_factor`;
UPDATE `mastermetersubsub` SET `coverage_factor`=2,`cmc`= 0.21;
ALTER TABLE `grndetail` ADD `resolution` DOUBLE NULL DEFAULT NULL AFTER `certificate_no`;

-- Chirag : 2020_05_22 12:00 PM
UPDATE `table_info` SET `note` = '<br/>1)The reported measurement uncertainty is estimated at a level of confidence of approximately 95 % with a coverage factor k = 2.<br/>\r\n2) Result are related only to the item calibrated.<br/>\r\n3) This certificate refers only to the particular items submitted for calibration.<br/>\r\n4) This certificate shall not be reproduced except in full without our prior permission in writing.<br/>\r\n5) The calibration results reported in this particular certificate are valid at the time of an under stated condition of measurement.<br/>\r\n6)Standard used for calibration were traceable to National / International standard.<br/>\r\n7) Readings given above are as on received condition of an instrument.<br/>\r\n8) Above calibration done is meant for scientific & industrial purpose only.' WHERE `table_info`.`table_info_id` = 2;

-- Chirag : 2020_05_30 07:00 PM
CREATE TABLE `observation_data` (
  `id` int(11) NOT NULL,
  `grn_id` int(11) DEFAULT NULL,
  `grn_detail_id` int(11) DEFAULT NULL,
  `table_info_id` int(11) DEFAULT NULL,
  `table_no` int(11) DEFAULT NULL,
  `master_json` longtext,
  `uuc_json` longtext,
  `reading_json` longtext,
  `type_a_type_b_json` longtext,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `observation_data`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `observation_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Chirag : 2020_06_12 07:00 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(4, '4', '/images/table_info/table_1.png', '<br/>(1) The reported expanded uncertainty in measurement is stated as the standard uncertainty<br/>\r\nin measurement multiplied by the coverage factor k=2, which for a normal distribution<br/>\r\ncorresponds to a coverage probability of approximately 95 %.<br/>', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_06_16 04:00 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(5, '2', '/images/table_info/table_1.png', 'table info 2 note', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_06_17 04:00 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(6, '5', '/images/table_info/table_1.png', 'table info 5 note', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_06_17 06:00 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(7, '6A', '/images/table_info/table_1.png', 'table info 6A note', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_06_17 07:45 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(8, '7', '/images/table_info/table_1.png', 'table info 7 note', NULL, NULL, NULL, NULL, NULL),
(9, '8', '/images/table_info/table_1.png', 'table info 8 note', NULL, NULL, NULL, NULL, NULL),
(10, '9', '/images/table_info/table_1.png', 'table info 9 note', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_07_01 08:00 PM
ALTER TABLE `grndetail` ADD `user_id` INT(11) NULL DEFAULT NULL AFTER `userName`;

-- Chirag : 2020_07_04 05:00 PM
ALTER TABLE `grndetail` DROP `masterMeterId`;
ALTER TABLE `grndetail` DROP `leastCount`;
ALTER TABLE `grndetail` DROP `extraFields`;
ALTER TABLE `grndetail` DROP `extraField1`;
ALTER TABLE `grndetail` DROP `extraField2`;
ALTER TABLE `grndetail` DROP `extraField3`;
ALTER TABLE `grndetail` DROP `extraField4`;
ALTER TABLE `grndetail` DROP `extraField5`;
ALTER TABLE `grndetail` DROP `extraField6`;
ALTER TABLE `grndetail` DROP `ratio1`;
ALTER TABLE `grndetail` DROP `ratio2`;
ALTER TABLE `grndetail` DROP `masterParameterId`;
ALTER TABLE `grndetail` DROP `masterRangeId`;

-- Chirag : 2020_07_16 03:00 PM
UPDATE `table_info` SET `image_url` = '/images/table_info/table_3.png' WHERE `table_info`.`table_info_id` = 3;
UPDATE `table_info` SET `image_url` = '/images/table_info/table_4.png' WHERE `table_info`.`table_info_id` = 4;
UPDATE `table_info` SET `image_url` = '/images/table_info/table_2.png' WHERE `table_info`.`table_info_id` = 5;
UPDATE `table_info` SET `image_url` = '/images/table_info/table_5.png' WHERE `table_info`.`table_info_id` = 6;
UPDATE `table_info` SET `image_url` = '/images/table_info/table_6A.png' WHERE `table_info`.`table_info_id` = 7;
UPDATE `table_info` SET `image_url` = '/images/table_info/table_7.png' WHERE `table_info`.`table_info_id` = 8;
UPDATE `table_info` SET `image_url` = '/images/table_info/table_8.png' WHERE `table_info`.`table_info_id` = 9;
UPDATE `table_info` SET `image_url` = '/images/table_info/table_9.png' WHERE `table_info`.`table_info_id` = 10;

-- Chirag : 2020_07_18 03:45 PM
ALTER TABLE `grndetail` CHANGE `temperature` `temperature` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- Chirag : 2020_07_23 07:30 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, '15', '/images/table_info/table_15.png', 'table info 15 note', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_07_25 12:30 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, '6B', '/images/table_info/table_15.png', 'table info 15 note', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_07_28 4:30 PM
CREATE TABLE `accuracy_class` (
  `id` int(11) NOT NULL,
  `ct_class` varchar(255) DEFAULT NULL,
  `1_ratio_error` double DEFAULT NULL,
  `1_phase_error` double DEFAULT NULL,
  `5_ratio_error` double DEFAULT NULL,
  `5_phase_error` double DEFAULT NULL,
  `20_ratio_error` double DEFAULT NULL,
  `20_phase_error` double DEFAULT NULL,
  `100_ratio_error` double DEFAULT NULL,
  `100_phase_error` double DEFAULT NULL,
  `120_ratio_error` double DEFAULT NULL,
  `120_phase_error` double DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `accuracy_class` (`id`, `ct_class`, `1_ratio_error`, `1_phase_error`, `5_ratio_error`, `5_phase_error`, `20_ratio_error`, `20_phase_error`, `100_ratio_error`, `100_phase_error`, `120_ratio_error`, `120_phase_error`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, '0.1', NULL, NULL, 0.4, 15, 0.2, 8, 0.1, 5, 0.1, 5, NULL, NULL, NULL, NULL),
(2, '0.2', NULL, NULL, 0.75, 30, 0.35, 15, 0.2, 10, 0.2, 10, NULL, NULL, NULL, NULL),
(3, '0.5', NULL, NULL, 0.051, 90, 0.021, 45, 0.101, 30, 0.121, 30, NULL, NULL, NULL, NULL),
(4, '1', NULL, NULL, 3, 180, 1.5, 90, 1, 60, 1, 60, NULL, NULL, NULL, NULL),
(5, '0.2s', 0.75, 30, 0.35, 15, 0.2, 10, 0.2, 10, 0.2, 10, NULL, NULL, NULL, NULL),
(6, '0.5s', 1.5, 90, 0.75, 45, 0.5, 30, 0.5, 30, 0.5, 30, NULL, NULL, NULL, NULL);

ALTER TABLE `accuracy_class`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `accuracy_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

-- Chirag : 2020_08_05 6:00 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, '11', '/images/table_info/table_11.png', 'table info 11 note', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_08_06 5:15 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, '13_and_14', '/images/table_info/table_13_and_14.png', 'table info 13_and_14 note', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_08_08 6:15 PM
INSERT INTO `grnprefix` (`grnPrefixId`, `grnPrefix`) VALUES (NULL, 'R');
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, 'regular_1', '/images/table_info/table_regular_1.png', '1) This report pertains to particular sample /Instrument submitted for test. This report<br/>may not be reproduced, except in full, without prior written permission.<br/>2) This calibration results reported in the certificate are valid at the time of measurements.', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_08_10 6:00 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, '17', '/images/table_info/table_17.png', 'table info 17 note', NULL, NULL, NULL, NULL, NULL);
UPDATE `table_info` SET `table_name` = '5_and_12' WHERE `table_info`.`table_info_id` = 6;

-- Chirag : 2020_08_20 7:30 PM
UPDATE `table_info` SET `image_url` = '/images/table_info/table_6B.png' WHERE `table_info`.`table_info_id` = 12;

-- Chirag : 2020_08_21 5:00 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, '10', '/images/table_info/table_10.png', 'table_10_note', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2020_09_09 8:10 PM
ALTER TABLE `grndetail` ADD `ulr_inc_no` VARCHAR(255) NULL DEFAULT NULL AFTER `ulr_no`;

-- Chirag : 2020_10_26 8:10 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES ('18', '19', '/images/table_info/table_19.png', 'table info 19 note', NULL, NULL, NULL, NULL, NULL);

CREATE TABLE `weight_unit` (
  `id` int(11) NOT NULL,
  `unit_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `weight_unit` (`id`, `unit_name`) VALUES
(1, 'g'),
(2, 'mg'),
(3, 'kg');

ALTER TABLE `weight_unit`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `weight_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

-- Chirag : 2020_11_14 01:05 AM

CREATE TABLE `table_notes` (
  `note_id` int(11) NOT NULL,
  `note` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `table_notes`
  ADD PRIMARY KEY (`note_id`);

ALTER TABLE `table_notes`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT;

-- Chirag : 2020_11_18 01:05 AM
CREATE TABLE `table_notes_detail` (
  `note_detail_id` int(11) NOT NULL,
  `table_info_id` int(11) DEFAULT NULL,
  `note_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `table_notes_detail`
  ADD PRIMARY KEY (`note_detail_id`);

ALTER TABLE `table_notes_detail`
  MODIFY `note_detail_id` int(11) NOT NULL AUTO_INCREMENT;

-- Chirag : 2020_11_25 07:50 PM
ALTER TABLE `mastermeter` CHANGE `masterMeterName` `masterMeterName` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `masterMeterIdNo` `masterMeterIdNo` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `masterMeterMake` `masterMeterMake` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `masterMeterModelNo` `masterMeterModelNo` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `masterMeterSerialNo` `masterMeterSerialNo` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `masterMeterCertificateNo` `masterMeterCertificateNo` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

-- Chirag : 2020_12_01 04:50 PM
ALTER TABLE `mastermetersubsub` ADD `range_disp` VARCHAR(255) NULL DEFAULT NULL AFTER `rangeValue`;
UPDATE `mastermetersubsub` SET range_disp = rangeValue;

-- Chirag : 2020_12_23 11:10 PM
INSERT INTO `table_info` (`table_info_id`, `table_name`, `image_url`, `note`, `template_path`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES ('19', '18', '/images/table_info/table_18.png', 'table info 18 note', NULL, NULL, NULL, NULL, NULL);

-- Chirag : 2021_04_12 06:45 PM
CREATE TABLE `user_roles_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  `website_module_id` int(11) DEFAULT NULL,
  `role_type_id` int(11) DEFAULT NULL,
  `change_type` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `user_roles_log`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user_roles_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
