<?php
require('./fpdf.php');
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = "";
  $grnDetailId         = "";
  $grnPrefix           = "";
  $grnNo               = "";
  $grnDate             = "";
  $poNo                = "";
  $poDate              = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();
  // Pdf Create :Start
  $pdf = new FPDF();
  $pdf->AliasNbPages();
  $pdf->AddPage();
    
  //SELECT OF GRN MASTER :START
  $selectGrnMaster = "SELECT grnPrefix,grnNo,DATE_FORMAT(grnDate,'%d-%m-%Y') AS grnDate,poNo,DATE_FORMAT(poDate,'%d-%m-%Y') AS poDate,custName,custCode,remarks,grnmaster.contPerson,phNo,userName
                        FROM grnmaster
                        JOIN customer
                       WHERE grnId = ".$_GET['grnId']."
                         AND grnmaster.customerId = customer.customerId";
  $selectGrnMasterRes = mysql_query($selectGrnMaster);
  if($selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes))
  {
   
  }
  //SELECT OF GRN MASTER :END
  //SELECT OF GRN DETAIL :START
  $a  = 0;
  $selectGrnEntry  = "SELECT grnDetail.grnDetailId,grndetail.grnId,item.itemName,grndetail.itemCode,grndetail.rangeValue,grndetail.description,grndetail.challan,grndetail.received,
                             grndetail.grnCondition,DATE_FORMAT(custReqDate,'%d-%m-%Y') AS custReqDate,DATE_FORMAT(expDelivDate,'%d-%m-%Y') AS expDelivDate,
                             parameterentry.parameterName
                        FROM grndetail
                        JOIN item
                        JOIN parameterentry
                       WHERE grnId = ".$_GET['grnId']."
                         AND grndetail.itemId = item.itemId
                         AND grndetail.parameterId = parameterentry.parameterId
                       ORDER BY grndetail.grnDetailId";
  $selectGrnEntryRes   = mysql_query($selectGrnEntry);
  while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
  {
    if(($a % $cfgRecPerGrnPage) == 0)
    {
      $yPosition = 75;
      if($a > 0)
      $pdf->AddPage();
      pageHeader();
      grnmasterFunc();
    }
    grnDetailFunc($yPosition,$a+1);
    $yPosition += 5;
    
  
    $a++;
  }


  $pdf->output();
  include("./bottom.php");
}

function grnmasterFunc()
{
    global $pdf;
    global $selectGrnMasterRow;
    $pdf->SetFont('Arial','',13);
    $pdf->SetXY(12,36);
    $pdf->Write(1,'GOODS RECEIPT NOTE');
    $pdf->SetXY(175,36);
    $pdf->Write(1,'ST F 01');
    $pdf->SetXY(12,42);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(5,'GrnNo : '.($selectGrnMasterRow['grnNo']));
    $pdf->SetXY(45,42);
    $pdf->Write(5,'Date  : '.($selectGrnMasterRow['grnDate']));
    $pdf->SetXY(162,47);
    $pdf->Write(5,'Code : '.($selectGrnMasterRow['custCode']));
    $pdf->Write(5,'Remarks : '.($selectGrnMasterRow['remarks']));
    $pdf->SetXY(10,135);
    $pdf->Write(5,'Received By : '.($selectGrnMasterRow['remarks']));
    $pdf->SetXY(80,135);
    $pdf->Write(5,'Store HOD :(SIGN.&DATE) : '.('_____________'));
    $pdf->SetXY(10,145);
    $pdf->Write(1,'For delivery of materials this goods Receipt required,otherwise material will not be returned.');

}

function pageHeader()
{
  global $pdf;
  $pdf->Image('./images/logo.jpg',122,2,70,30);
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(13,60);
  $pdf->cell(180,05,' FOLLOWING MATERIALS RECEIVED FOR - CALIBRATION / VERIFICATEION:','1', '0', 'C');

  $pdf->SetFont('Arial','',7);
  $pdf->SetXY(13,70);
  $pdf->cell(115,05,'SR.NO.','1', '0', 'L');
 
  $pdf->SetXY(28,70);
  $pdf->cell(16,05,'ITEM CODE','1', '0', 'L');
  
  $pdf->SetXY(44,70);
  $pdf->cell(20,05,'DESCRIPTION','', '0', 'L');
  // Quantity part :Start
  $pdf->SetXY(128,65);
  $pdf->cell(65,05,'QUANTITY','1', '0', 'C');
  $pdf->SetXY(13,65);
  $pdf->cell(180,05,'','1', '0', 'L');
  //Quantity part :Start
  $pdf->SetXY(128,70);
  $pdf->cell(20,05,'CHALLAN','1', '0', 'L');
  
  $pdf->SetXY(148,70);
  $pdf->cell(20,05,'RECEIVED','1', '0', 'L');

  $pdf->SetXY(168,70);
  $pdf->cell(25,05,'CONDITION','1', '0', 'L');
}
function grnDetailFunc($yPosition,$srNo)
{
  //$pdf->Line(10, 70, 10, 125);
  global $pdf;
  global $selectGrnEntryResRow;
  $pdf->SetXY(13,$yPosition);
  $pdf->cell(180 ,05,$srNo,'1', '0', 'L');
  $pdf->SetXY(28,$yPosition);
  $pdf->cell(16,05,$selectGrnEntryResRow['itemCode'],'1', '0', 'L');
  $pdf->SetXY(44,$yPosition);
  $pdf->Cell(84,05,$selectGrnEntryResRow['description'],1, "NOWRAP");
  $pdf->SetXY(128,$yPosition);
  $pdf->cell(20,05,$selectGrnEntryResRow['challan'],'1', '0', 'L');
  $pdf->SetXY(148,$yPosition);
  $pdf->cell(20,05,$selectGrnEntryResRow['received'],'1', '0', 'L');
  $pdf->SetXY(168,$yPosition);
  $pdf->cell(25,05,$selectGrnEntryResRow['grnCondition'],'1', '0', 'L');
}
?>