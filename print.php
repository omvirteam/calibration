<?php
// reference the Dompdf namespace
require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;
// instantiate and use the dompdf class
ob_start();
?>
<html>
    <head>
        <style>
            /** Define the margins of your page **/
            body {
              font-family: "Times New Roman";
              font-size: 13px;
            }

            header {
                position: fixed;
                top: 0px;
                left: 0px;
                right: 0px;
                height: auto;
                color: white;
                text-align: center;
            }

            footer {
                position: fixed;
                bottom: 0px;
                left: 0px;
                right: 0px;
                height: auto;
                color: white;
                text-align: center;
            }
            main {
              font-family: "Times New Roman";
              font-size: 13px;
            }
            table {font-family: "Times New Roman"; font-size: 13px; }
            .main-table {
              width: 100% !important;
              border: 1px solid red;
            }
            @page {
                margin: 0px;
            }

            tr {line-height:14.5px}
            .main-page {
              width: 630px;
              margin-top:62px;
              border: 1px solid black;
              border-radius: 10px;
              padding: 5px;
              display: inline-block;
            }

        </style>
    </head>
    <body>
        <!-- Define header and footer blocks before your content -->
        <header>
          <div style="height:100%; border:1px solid green;">
             <div style="text-align:right; padding-right: 25px;"><b>Ram Ram</b></div>
             <!--<div style="border: 1px solid black; border-radius: 10px; padding: 5px;">-->
             <table width="605px" class="main-table">
                <tr>
                   <td width="150px">CERTIFICATE NO.</td>
                   <td width="145px">: KNE0136GN03</td>
                   <td width="5px">&nbsp;</td>
                   <td width="200px">DATE OF CALIBRATION</td>
                   <td width="95px" align="right">: 03/08/2020</td>
                </tr>
                <tr>
                   <td>GRN NO.</td>
                   <td>: NE-0136</td>
                   <td>&nbsp;</td>
                   <td>NEXT RECOMMENDED DATE</td>
                   <td align="right">: 02/08/2021</td>
                </tr>
                <tr>
                   <td width="150px">PAGE</td>
                   <td width="145px">: 2 OF <span class="total_page_no">2</span></td>
                   <td width="5px">&nbsp;</td>
                   <td width="200px">CERTIFICATE ISSUE DATE</td>
                   <td width="95px" align="right">: 03/08/2020</td>
                </tr>
             </table>
          </div>
        </header>
        <footer>
          <table width="605px">
              <tr valign="top">
                  <td width="5px">&nbsp;</td>
                  <td width="600px">&nbsp;</td>
              </tr>
              <tr valign="top">
                  <hr />
                  <td width="5" style="height: 5px;"><strong></strong></td>
                  <td  style="height: 5px;" width="600px" style="font-size: 20px;"></td>
              </tr>
              <tr valign="bottom">
                  <td width="5px"><strong>&nbsp;</strong></td>
                  <td width="600px" style="font-size: 20px;"><img src="./images/for-krishna.png" /></td>
              </tr>
              <tr valign="top">
                  <td colspan="2">&nbsp;</td>
              </tr>
              <tr valign="top">
                  <td colspan="2">&nbsp;</td>
              </tr>
              <tr valign="top">
                  <td colspan="2">&nbsp;</td>
              </tr>
              <tr valign="top">
                  <td colspan="2">CALIBRATED BY: (Om) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; APPROVED BY: (D.R.SHAH, C.E.O) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; SEAL :</td>
              </tr>
              <tr valign="top">
                  <td width="5px"><strong>Note&nbsp;:</strong></td>
                  <td>&nbsp;</td>
              </tr>
              <tr valign="top">
                  <td colspan="2" style="font-size: 12px;"></td>
              </tr>
          </table>
        </footer>
        <!-- Wrap the content of your PDF inside a main tag -->
        <main >
          <div class="main-page" >
            <table width="100%" align="left" >
               <tr>
                  <th align="center" valign="middle">Title:</th>
                  <th colspan="6" align="left" valign="middle">&nbsp; Title 299 VAC</th>
               </tr>
               <tr>
                  <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                  <th colspan="2" align="center" valign="middle">Calibration Standard</th>
                  <th align="center" valign="middle" colspan="2">Unit Under Test</th>
                  <th align="center" colspan="2" valign="middle">Error</th>
               </tr>
               <tr>
                  <th align="center" width="80px" valign="middle">Range</th>
                  <th align="center" valign="middle">Reading in <br/>VAC</th>
                  <th align="center" valign="middle" width="80px">Range</th>
                  <th align="center" valign="middle">Reading 1 &Oslash; <br/>in VAC</th>
                  <th align="center" valign="middle">Unit(+/-)</th>
                  <th align="center" valign="middle">% of Rdg.</th>
               </tr>
               <tr>
                  <td align="center" valign="middle">1</td>
                  <td valign="middle" rowspan="4">
                     <div style="word-break: break-all; width: 80px;" >300 VAC @ 50Hz</div>
                  </td>
                  <td align="right">299.0000</td>
                  <td valign="middle" rowspan="4">
                     <div style="word-break: break-all; width: 80px;" >299</div>
                  </td>
                  <td align="right">299</td>
                  <td align="right">0.0000</td>
                  <td align="right">0.0000</td>
               </tr>
               <tr>
                  <td align="center" valign="middle">2</td>
                  <td align="right">299.0000</td>
                  <td align="right">299</td>
                  <td align="right">0.0000</td>
                  <td align="right">0.0000</td>
               </tr>
               <tr>
                  <td align="center" valign="middle">3</td>
                  <td align="right">300.0000</td>
                  <td align="right">300</td>
                  <td align="right">0.0000</td>
                  <td align="right">0.0000</td>
               </tr>
               <tr>
                  <td align="center" valign="middle">4</td>
                  <td align="right">300.0000</td>
                  <td align="right">295</td>
                  <td align="right">-5.0000</td>
                  <td align="right">-1.6667</td>
               </tr>
               <tr>
                  <td><b>&nbsp;</b></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
               </tr>
               <tr>
                  <td align="left" colspan="3"><b>Max Expanded Uncert. in %</b></td>
                  <td align="right"><b>12.7017</b></td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
               </tr>
            </table>
          </div>
        </main>
    </body>
</html>
<?php
$content = ob_get_clean();
// echo $content;
$dompdf = new Dompdf();
$dompdf->loadHtml($content);
$dompdf->setPaper('A4', 'portrait');
// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();
?>
