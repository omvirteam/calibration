<?php 
$col_span1 = '9';
if(!empty($view_uncertainity)){
    $col_span1 = $col_span1 + 1;
}
$stdmeter_data = isset($reading_json[0]['stdMeter']) ? $reading_json[0]['stdMeter'] : array();
$testmeter1_data = isset($reading_json[0]['testMeter']) ? $reading_json[0]['testMeter'] : array();
$testmeter2_data = isset($reading_json[0]['testMeter2']) ? $reading_json[0]['testMeter2'] : array();
$testmeter3_data = isset($reading_json[0]['testMeter3']) ? $reading_json[0]['testMeter3'] : array();
$testmeter4_data = isset($reading_json[0]['testMeter4']) ? $reading_json[0]['testMeter4'] : array();
$testmeter5_data = isset($reading_json[0]['testMeter5']) ? $reading_json[0]['testMeter5'] : array();
$testmeter6_data = isset($reading_json[0]['testMeter6']) ? $reading_json[0]['testMeter6'] : array();
$uuc_min_range = $uuc_data[0]['uut_range_min'];
$uuc_max_range = $uuc_data[0]['uut_range'];
$uuc_range = 0;
if($uuc_min_range < 0){
    $uuc_range = abs($uuc_min_range) + abs($uuc_max_range);
} else {
    if($uuc_min_range > $uuc_max_range){
        $uuc_range = abs($uuc_min_range);
    } else {
        $uuc_range = abs($uuc_max_range);
    }
}

$table_title = '';
foreach ($grn_data as $k => $grn_entry){ 
    if(!empty($grn_entry['table_title'])){
        $table_title = $grn_entry['table_title'];
        break;
    }
}
$table_title_right = '';
foreach ($grn_data as $k => $grn_entry){ 
    if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
        $table_title_right = $grn_entry['table_title_right'];
        break;
    }
}
//                echo "<pre>"; print_r($grn_data); exit;
$decimal_master = 0;
$resolution_of_master = $grn_data[0]['resolution_of_master'];
if(!empty($resolution_of_master)){
    $exp_val = explode('.', $resolution_of_master);
    if(isset($exp_val[1])){
        $decimal_master = strlen($exp_val[1]);  
    } else {
      $decimal_master = 0;  
    }
} else {
    $decimal_master = 0;
}
$decimal_least = 0;
$resolution_of_least = $grn_data[0]['leastCount'];
if(!empty($resolution_of_least)){
    $exp_least = explode('.', $resolution_of_least);
    if(isset($exp_least[1])){
        $decimal_least = strlen($exp_least[1]);  
    } else {
      $decimal_least = 0;  
    }
} else {
    $decimal_least = 0;
}
$qry13 = 'SELECT range_unit,coverage_factor,cmc FROM mastermetersubsub
WHERE masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
$res13 = mysql_query($qry13) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
$m_range_name = '';
$m_coverage_factor = '';
$m_cmc = '';
while($row13 = mysql_fetch_assoc($res13)) {
    $m_range_name = $row13['range_unit'];
    $m_coverage_factor = $row13['coverage_factor'];
    $m_cmc = $row13['cmc'];
}
$max_key1 = !empty($testmeter1_data) ? max(array_keys($testmeter1_data)) : '0';
$max_key2 = !empty($testmeter2_data) ? max(array_keys($testmeter2_data)) : '0';
$max_key3 = !empty($testmeter3_data) ? max(array_keys($testmeter3_data)) : '0';
$max_key4 = !empty($testmeter4_data) ? max(array_keys($testmeter4_data)) : '0';
$max_key5 = !empty($testmeter5_data) ? max(array_keys($testmeter5_data)) : '0';
$max_key6 = !empty($testmeter6_data) ? max(array_keys($testmeter6_data)) : '0';
$max_key7 = !empty($stdmeter_data) ? max(array_keys($stdmeter_data)) : '0';
$max_key = 0; 
if(($max_key1 >= $max_key2) && ($max_key1 >= $max_key3) && ($max_key1 >= $max_key4) && ($max_key1 >= $max_key5) && ($max_key1 >= $max_key6) && ($max_key1 >= $max_key7)){
    $max_key = $max_key1;
} else if(($max_key2 >= $max_key3) && ($max_key2 >= $max_key1) && ($max_key2 >= $max_key4) && ($max_key2 >= $max_key5) && ($max_key2 >= $max_key6) && ($max_key2 >= $max_key7)){
    $max_key = $max_key2;
} else if(($max_key3 >= $max_key1) && ($max_key3 >= $max_key2) && ($max_key3 >= $max_key4) && ($max_key3 >= $max_key5) && ($max_key3 >= $max_key6) && ($max_key3 >= $max_key7)){
    $max_key = $max_key3;
} else if(($max_key4 >= $max_key1) && ($max_key4 >= $max_key2) && ($max_key4 >= $max_key3) && ($max_key4 >= $max_key5) && ($max_key4 >= $max_key6) && ($max_key4 >= $max_key7)){
    $max_key = $max_key4;
} else if(($max_key5 >= $max_key1) && ($max_key5 >= $max_key2) && ($max_key5 >= $max_key3) && ($max_key5 >= $max_key4) && ($max_key5 >= $max_key6) && ($max_key5 >= $max_key7)){
    $max_key = $max_key5;
} else if(($max_key6 >= $max_key1) && ($max_key6 >= $max_key2) && ($max_key6 >= $max_key3) && ($max_key6 >= $max_key4) && ($max_key6 >= $max_key5) && ($max_key6 >= $max_key7)){
    $max_key = $max_key6;
} else if(($max_key7 >= $max_key1) && ($max_key7 >= $max_key2) && ($max_key7 >= $max_key3) && ($max_key7 >= $max_key4) && ($max_key7 >= $max_key5) && ($max_key7 >= $max_key6)){
    $max_key = $max_key7;
}
?>

<table width="605" align="left" class="main_table"  style="font-size:13px;">
<tr>
    <th align="center" valign="middle">Title:</th>
    <th  colspan="<?= $col_span1; ?>" align="left" valign="middle" style="font-family:dejavusansb; font-size: 11px;">&nbsp; <?php echo $table_title_right; ?></th>
</tr>
<tr>
    <th valign="middle" align="center" rowspan="3">Step</th>
    <th colspan="1" align="center" valign="middle">UUC Reading</th>
    <th align="center" valign="middle" colspan="6">Master Reading</th>
    <th align="center" colspan="1" valign="middle">Average Miw</th>
    <?php if(!empty($view_uncertainity)){ ?>
        <th align="center" rowspan="3" valign="middle">Max Expanded <br/>Uncertainty(%)</th>
    <?php } ?>
</tr>
<tr>
    <th valign="middle" align="center">Pind</th>
    <th valign="middle" align="center">M1</th>
    <th valign="middle" align="center">M2</th>
    <th valign="middle" align="center">M3</th>
    <th valign="middle" align="center">M4</th>
    <th valign="middle" align="center">M5</th>
    <th valign="middle" align="center">M6</th>
    <th valign="middle" align="center">((M1+M3)/2+</th>
</tr>
<tr>
    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
    <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
    <th valign="middle" align="center"> (M2+M4)2)/2</th>
</tr>
<?php
for($x = 0; $x <= $max_key; $x++) { ?>
    <tr>
        <td align="center" valign="middle"><?php echo $x + 1; ?></td>
        <?php $std_mter = (isset($stdmeter_data[$x]) ? number_format((float)$stdmeter_data[$x], $decimal_least, '.', '') : '0'); ?>
        <td align="center" valign="middle"><?= (isset($stdmeter_data[$x]) ? number_format((float)$stdmeter_data[$x], $decimal_least, '.', '') : ''); ?></td>
        <?php $m1 = (isset($testmeter1_data[$x]) ? number_format((float)$testmeter1_data[$x], $decimal_master, '.', '') : '0'); ?>
        <td align="center" valign="middle"><?= (isset($testmeter1_data[$x]) ? number_format((float)$testmeter1_data[$x], $decimal_master, '.', '') : '0'); ?></td>
        <?php $m2 = (isset($testmeter2_data[$x]) ? number_format((float)$testmeter2_data[$x], $decimal_master, '.', '') : '0'); ?>
        <td align="center" valign="middle"><?= (isset($testmeter2_data[$x]) ? number_format((float)$testmeter2_data[$x], $decimal_master, '.', '') : ''); ?></td>
        <?php $m3 = (isset($testmeter3_data[$x]) ? number_format((float)$testmeter3_data[$x], $decimal_master, '.', '') : '0'); ?>
        <td align="center" valign="middle"><?= (isset($testmeter3_data[$x]) ? number_format((float)$testmeter3_data[$x], $decimal_master, '.', '') : ''); ?></td>
        <?php $m4 = (isset($testmeter4_data[$x]) ? number_format((float)$testmeter4_data[$x], $decimal_master, '.', '') : '0'); ?>
        <td align="center" valign="middle"><?= (isset($testmeter4_data[$x]) ? number_format((float)$testmeter4_data[$x], $decimal_master, '.', '') : ''); ?></td>
        <?php $m5 = (isset($testmeter5_data[$x]) ? number_format((float)$testmeter5_data[$x], $decimal_master, '.', '') : '0'); ?>
        <td align="center" valign="middle"><?= (isset($testmeter5_data[$x]) ? number_format((float)$testmeter5_data[$x], $decimal_master, '.', '') : ''); ?></td>
        <?php $m6 = (isset($testmeter6_data[$x]) ? number_format((float)$testmeter6_data[$x], $decimal_master, '.', '') : '0'); ?>
        <td align="center" valign="middle"><?= (isset($testmeter6_data[$x]) ? number_format((float)$testmeter6_data[$x], $decimal_master, '.', '') : ''); ?></td>
        <?php $avg = ((($m1+$m3+$m5)/3+($m2+$m4+$m6)/3)/2); ?>
        <td align="center" valign="middle"><?= number_format((float)$avg, $decimal_master, '.', ''); ?></td>
        <?php $dev = (($std_mter - $avg)/$uuc_range)*100; ?>
        <?php $dev = number_format((float)$dev, 3, '.', ''); ?>
        <?php if(!empty($view_uncertainity)){ ?>
            <td valign="middle" align="center">
                <?php 
                //echo number_format((float)$max_ex_unce_p, 2, '.', ''); 
                // echo isset($type_a_type_b_json[$k]['exp_uncer_per_ue_without_cmc']) ? sprintf("%.2f",$type_a_type_b_json[$k]['exp_uncer_per_ue_without_cmc']) : '';
                echo isset($type_a_type_b_json[$k]['exp_uncer_per_ue']) ? sprintf("%.4f",$type_a_type_b_json[$k]['exp_uncer_per_ue']) : '';
                ?>
            </td>
        <?php } ?>
    </tr>
    <?php
} 
?>
<tr>
    <td colspan="3" align="center" valign="middle">Hysteresis (max)</td>
    <td colspan="1" align="center" valign="middle"><?php echo number_format((float)$grn_data[0]['uncertainty_hysteresis_max'], $decimal_master, '.', ''); ?> <?php echo $m_range_name; ?></td>
    <td colspan="2" align="center" valign="middle">&nbsp;</td>
    <td colspan="1" align="center" valign="middle">&nbsp;</td>
    <td colspan="1" align="center" valign="middle">&nbsp;</td>
    <td colspan="1" align="center" valign="middle">&nbsp;</td>
    <?php if(!empty($view_uncertainity)){ ?>
        <td valign="middle" align="center"></td>
    <?php } ?>
</tr>
<tr>
    <td colspan="3" align="center" valign="middle">Repetability</td>
    <td colspan="1" align="center" valign="middle"><?php echo number_format((float)$grn_data[0]['uncer_of_repeat_max'], $decimal_master, '.', ''); ?> <?php echo $m_range_name; ?></td>
    <?php if(!empty($view_uncertainity)){ ?>
        <td colspan="3" align="center" valign="middle"></td>
        <td colspan="2" align="center" valign="middle"></td>
        <td valign="middle" align="center"></td>
    <?php } else { ?>
        <td colspan="3" align="center" valign="middle">Exp. Uncertainty(max)</td>
        <td colspan="2" align="center" valign="middle"><?php echo number_format((float)$max_ex_unce_p, 2, '.', ''); ?> <?php echo $m_range_name; ?></td>
    <?php }  ?>
</tr>
<tr>
    <td colspan="2" align="center" valign="middle">Remark</td>
    <?php if(!empty($view_uncertainity)){ ?>
        <td colspan="8" align="left" valign="middle"><?php echo $instrumentDetails['certi_remarks']; ?></td>
    <?php } else { ?>
        <td colspan="7" align="left" valign="middle"><?php echo $instrumentDetails['certi_remarks']; ?></td>
    <?php }  ?>
    
</tr>
</table>