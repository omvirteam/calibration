<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else {
	$query1 = "SELECT * FROM grnmaster WHERE grnId = " . $_GET['grnId'];
	$result1 = mysql_query($query1);
	
	if(!$result1) { 
		echo strtoupper("selecting original grn master failed.");
	} else { 
		$grnRow = mysql_fetch_assoc($result1);
		
		$query2 = "SELECT * FROM grndetail WHERE grnId = " . $_GET['grnId'];
		$result2 = mysql_query($query2);
		
		if(!$result2) {
			echo strtoupper("selecting original grn detail failed.");
		} else { 
			// find next grn
			$query3 = "SELECT ( MAX( CONVERT(grnNo, SIGNED INTEGER) )+1 ) AS nextGrn FROM grnmaster";
			$result3 = mysql_query($query3);
			
			if(!$result3) { 
				echo strtoupper("cannot get next grn number");
			} else {
				$grnnoRow = mysql_fetch_assoc($result3);
				$nextGRN = $grnnoRow['nextGrn'];
				
				$selectgrnentry = "INSERT INTO grnmaster(grnPrefix,grnNo,infoSheetNo,grnDate,poNo,customerId,poDate,mrAndMrs,contPerson,phNo,remarks,userName)
									VALUE('".$grnRow['grnPrefix']."','".$nextGRN."','".$nextGRN."','".$grnRow['grnDate']."','".$grnRow['poNo']."','".$grnRow['customerId']."',
										  '".$grnRow['poDate']."','".$grnRow['mrAndMrs']."','".$grnRow['contPerson']."','".$grnRow['phNo']."','".$grnRow['remarks']."','".$_SESSION['s_activId']."')";
				$selectgrnentryResult = mysql_query($selectgrnentry);
				$grnId = mysql_insert_id();
				
				if(!$selectgrnentryResult) {
				  die("Insert Query Not Inserted 1: ".mysql_error(). " : ".$selectgrnentry  );
				}
				
				$grnDetailIds = array();
				$grnDetailCalibDates = array();
				$grnDetailCalibNDates = array();
				$grnDetailMakeModels = array();
				$grnDetailInstrumentIds = array();
				$grnDetailLeastCounts = array();
				$grnDetailAccuracies = array();
				$grnDetailTemperatures = array();
				$grnDetailHumidities = array();
				$grnDetailMasterParameterIds = array();
				$grnDetailMasterRangeIds = array();
				$grnDetailExtraField1 = array();
				$grnDetailExtraField2 = array();
				$grnDetailExtraField3 = array();
				$grnDetailExtraField4 = array();
				$grnDetailExtraField5 = array();
				$grnDetailExtraField6 = array();
				$grnDetailCertiRemarks = array();
				$grnDetailRatio1 = array();
				$grnDetailRatio2 = array();
				
				while($grndetailRow = mysql_fetch_assoc($result2)) {
					$itemId        = $grndetailRow['itemId'];
					$itemCode      = $grndetailRow['itemCode'];
					$parameterId   = $grndetailRow['parameterId'];
					$range         = $grndetailRow['rangeValue'];
					$custReqDate   = $grndetailRow['custReqDate'];
					$expDelivDate  = $grndetailRow['expDelivDate'];
					$description   = $grndetailRow['description'];
					$challan       = $grndetailRow['challan'];
					$received      = $grndetailRow['received'];
					$condition     = $grndetailRow['grnCondition'];
				  
					$insertQueryGrnDetail = "INSERT INTO grndetail(grnId,itemId,itemCode,parameterId,rangeValue,custReqDate,expDelivDate,description,challan,received,grnCondition)
											  VALUE (".$grnId.",".$itemId.",'".$itemCode."','".$parameterId."','".$range."','".$custReqDate."','".$expDelivDate."','".$description."','".$challan."','".$received."','".$condition."')";
					$insertQueryGrnDetailResult = mysql_query($insertQueryGrnDetail);
						
					$grnDetailIds[$grndetailRow['grnDetailId']] = mysql_insert_id();
					$grnDetailCalibDates[$grndetailRow['grnDetailId']] = $grndetailRow['callibrationDate'];
					$grnDetailCalibNDates[$grndetailRow['grnDetailId']] = $grndetailRow['nextYearDate'];
					$grnDetailMakeModels[$grndetailRow['grnDetailId']] = $grndetailRow['makeModel'];
					$grnDetailInstrumentIds[$grndetailRow['grnDetailId']] = $grndetailRow['instrumentId'];
					$grnDetailLeastCounts[$grndetailRow['grnDetailId']] = $grndetailRow['leastCount'];
					$grnDetailAccuracies[$grndetailRow['grnDetailId']] = $grndetailRow['accuracy'];
					$grnDetailTemperatures[$grndetailRow['grnDetailId']] = $grndetailRow['temperature'];
					$grnDetailHumidities[$grndetailRow['grnDetailId']] = $grndetailRow['humidity'];
					$grnDetailMasterParameterIds[$grndetailRow['grnDetailId']] = $grndetailRow['masterParameterId'];
					$grnDetailMasterRangeIds[$grndetailRow['grnDetailId']] = $grndetailRow['masterRangeId'];
					$grnDetailExtraField1[$grndetailRow['grnDetailId']] = $grndetailRow['extraField1'];
					$grnDetailExtraField2[$grndetailRow['grnDetailId']] = $grndetailRow['extraField2'];
					$grnDetailExtraField3[$grndetailRow['grnDetailId']] = $grndetailRow['extraField3'];
					$grnDetailExtraField4[$grndetailRow['grnDetailId']] = $grndetailRow['extraField4'];
					$grnDetailExtraField5[$grndetailRow['grnDetailId']] = $grndetailRow['extraField5'];
					$grnDetailExtraField6[$grndetailRow['grnDetailId']] = $grndetailRow['extraField6'];
					$grnDetailCertiRemarks[$grndetailRow['grnDetailId']] = $grndetailRow['certiRemarks'];
					$grnDetailRatio1[$grndetailRow['grnDetailId']] = $grndetailRow['ratio1'];
					$grnDetailRatio2[$grndetailRow['grnDetailId']] = $grndetailRow['ratio2'];
				}
				
				if($_GET['nabl'] == 1) { 
					$query4 = "SELECT * FROM grnobsmaster WHERE grnId = " . $_GET['grnId'];
					$result4 = mysql_query($query4);
					
					if(!$result4) { 
						echo strtoupper("cannot select from nabl master.");
					} else { 
						while($nablRow = mysql_fetch_assoc($result4)) {
							// Insert into observation master : Starts
							$insertMaster = "INSERT INTO grnobsmaster(grnId, grnDetailId, masterMeterId, mParameterId, mRangeId, grnObsDate, userName,selfCertiNo) 
								VALUES (".$grnId.",".$grnDetailIds[$nablRow['grnDetailId']].",".$nablRow['masterMeterId'].",".$nablRow['mParameterId'].",'".$nablRow['mRangeId']."','".$nablRow['grnObsDate']."', '".$_SESSION['s_activId']."','".$nablRow['selfCertiNo']."')";
							$insertMasterRes = mysql_query($insertMaster);
							
							if(!$insertMasterRes) {
							  die("Insert Query Not Inserted 1: ".mysql_error(). " : ".$insertMaster);
							} else {
								$grnObsMasterId = mysql_insert_id();
							}		
							// Insert into observation master : Ends
							
							// Update GRN Observation Detail : Starts
							$updateGrnEntry = "UPDATE grndetail
	                          SET refGrnDetailId = '0',masterMeterId = '".$nablRow['masterMeterId']."',callibrationDate ='".$grnDetailCalibDates[$nablRow['grnDetailId']]."',
	                              nextYearDate = '".$grnDetailCalibNDates[$nablRow['grnDetailId']]."', makeModel = '".$grnDetailMakeModels[$nablRow['grnDetailId']]."',
	                              instrumentId = '".$grnDetailInstrumentIds[$nablRow['grnDetailId']]."',leastCount = '".$grnDetailLeastCounts[$nablRow['grnDetailId']]."',
	                              accuracy = '".$grnDetailAccuracies[$nablRow['grnDetailId']]."',temperature = '".$grnDetailTemperatures[$nablRow['grnDetailId']]."',
	                              humidity = '".$grnDetailHumidities[$nablRow['grnDetailId']]."', masterParameterId ='".$grnDetailMasterParameterIds[$nablRow['grnDetailId']]."',
	                              masterRangeId = '".$grnDetailMasterRangeIds[$nablRow['grnDetailId']]."',userName = '".$_SESSION['s_activId']."',
								  extraField1 = '".$grnDetailExtraField1[$nablRow['grnDetailId']]."', 
								  extraField2 = '".$grnDetailExtraField2[$nablRow['grnDetailId']]."', 
								  extraField3 = '".$grnDetailExtraField3[$nablRow['grnDetailId']]."', 
								  extraField4 = '".$grnDetailExtraField4[$nablRow['grnDetailId']]."', 
								  extraField5 = '".$grnDetailExtraField5[$nablRow['grnDetailId']]."', 
								  extraField6 = '".$grnDetailExtraField6[$nablRow['grnDetailId']]."', 
								  certiRemarks = '".$grnDetailCertiRemarks[$nablRow['grnDetailId']]."',
	                              ratio1 = ".$grnDetailRatio1[$nablRow['grnDetailId']].", ratio2 = ".$grnDetailRatio2[$nablRow['grnDetailId']]."
	                        WHERE grnDetailId = ".$grnDetailIds[$nablRow['grnDetailId']];
							$updateGrnEntryResult = mysql_query($updateGrnEntry);
							if(!$updateGrnEntryResult) {
							  die("Insert Query Not Inserted 1: ".mysql_error(). " : ".$updateGrnEntry);
							}  
							// Update GRN Observation Detail : Ends
							
							$query5 = "SELECT * FROM grnobservation WHERE grnId = " . $_GET['grnId'] . " AND grnDetailId = " . $nablRow['grnDetailId'];
							$result5 = mysql_query($query5);
							
							if(!$result5) { 
								echo strtoupper("cannot select from nabl observation.");
							} else {
								while($nablobsvRow = mysql_fetch_assoc($result5)) {
									/////// Insert Query for GRN Observation Detail : Statrs
									$insertQueryGrnDetail = "INSERT INTO grnobservation (grnObsMasterId,grnId,grnDetailId,uncertaintyCalibration,uncertaintyCalibrationInPer,resolutionTypeA,stdMeter1AfterDecimal,resolutionTypeAAfterDecimal,stdMeter1,testMeter1,stdMeter2,testMeter2,stdMeter3,testMeter3,stdMeter4,
									testMeter4,stdMeter5,testMeter5,stdMeterAverage,testMeterAverage,accuracyTaken,standardDeviation,
									standardUncertinity,standardUncertinityperc,degreeOfFreedom,uncertinityForTypeB,uncertinityInPercentage,
									accuracyForTypeB,acuuracyForTypeBPerc,resolutionTypeB,resolutionForTypeBPerc,stabilityForTypeB,
									stabilityForTypeBInPerc,combinedUncertinity,combinedUncertinityInPerc,effectiveUncertinity,effectiveUncertinityInPer,
									effectiveDegreeOfFreed,meanReading,masterMeterReading,error,expandedUncertinity,expandedUncertinityInPre)
									VALUES ('".$grnObsMasterId."',".$grnId.",".$grnDetailIds[$nablRow['grnDetailId']].",".$nablobsvRow['uncertaintyCalibration'].",".$nablobsvRow['uncertaintyCalibrationInPer'].",".$nablobsvRow['resolutionTypeA'].",".$nablobsvRow['stdMeter1AfterDecimal'].",".$nablobsvRow['resolutionTypeAAfterDecimal'].",".$nablobsvRow['stdMeter1'].",'".$nablobsvRow['testMeter1']."','".$nablobsvRow['stdMeter2']."',
									'".$nablobsvRow['testMeter2']."','".$nablobsvRow['stdMeter3']."','".$nablobsvRow['testMeter3']."','".$nablobsvRow['stdMeter4']."',".$nablobsvRow['testMeter4'].",
									".$nablobsvRow['stdMeter5'].",'".$nablobsvRow['testMeter5']."','".$nablobsvRow['stdMeterAverage']."','".$nablobsvRow['testMeterAverage']."','".$nablobsvRow['accuracyTaken']."',
									'".$nablobsvRow['standardDeviation']."','".$nablobsvRow['standardUncertinity']."','".$nablobsvRow['standardUncertinityperc']."','".$nablobsvRow['degreeOfFreedom']."',
									'".$nablobsvRow['uncertinityForTypeB']."','".$nablobsvRow['uncertinityInPercentage']."','".$nablobsvRow['accuracyForTypeB']."','".$nablobsvRow['acuuracyForTypeBPerc']."',
									'".$nablobsvRow['resolutionTypeB']."','".$nablobsvRow['resolutionForTypeBPerc']."','".$nablobsvRow['stabilityForTypeB']."','".$nablobsvRow['stabilityForTypeBInPerc']."',
									'".$nablobsvRow['combinedUncertinity']."','".$nablobsvRow['combinedUncertinityInPerc']."','".$nablobsvRow['effectiveUncertinity']."','".$nablobsvRow['effectiveUncertinityInPer']."',
									'".$nablobsvRow['effectiveDegreeOfFreed']."','".$nablobsvRow['meanReading']."','".$nablobsvRow['masterMeterReading']."','".$nablobsvRow['error']."','".$nablobsvRow['expandedUncertinity']."','".$nablobsvRow['expandedUncertinityInPre']."')";
									$insertQueryGrnDetailResult = mysql_query($insertQueryGrnDetail);
									////// Insert Query for GRN Observation Detail : Ends
								}	
							}	
						}
					}	
				}
			}	
		}	
	}
		
    header("Location:grnEdit.php?grnId=".$grnId);
}
?>
