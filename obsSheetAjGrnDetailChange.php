<?php
include("include/omConfig.php");
$itemCode    = "";
$range       = "";
$instrumentId       = "";
$make_modal  = "";
$humidity    = "";
$temperature = "";
$uuc_location = "";
$approved_by  = "";
$grnDetailId  = "";
$is_table_selected_val  = "";
$edit_table_no = "";

if(isset($_GET['grn_detail_id']) && isset($_GET['table_info_id']) && isset($_GET['table_no'])) {
    $data_uuc = array();
    $data_uuc['min_range'] = '';
    $data_uuc['max_range'] = '';
    $data_uuc['table_title_right'] = '';
    $selectDetail = "SELECT * FROM observation_data
                    WHERE grn_detail_id = ".$_GET['grn_detail_id']." AND table_info_id = ".$_GET['table_info_id']." AND table_no = ".$_GET['table_no'];
      $selectDetailResult = mysql_query($selectDetail);
      while($selectDetailRow = mysql_fetch_array($selectDetailResult)) {
          $uuc_data = json_decode(json_encode(json_decode($selectDetailRow['uuc_json'])), true);
          $uuc_data = $uuc_data[max(array_keys($uuc_data))];
          $data_uuc['min_range'] = isset($uuc_data['uut_range_min']) ? $uuc_data['uut_range_min'] : '';
          $data_uuc['max_range'] = isset($uuc_data['uut_range']) ? $uuc_data['uut_range'] : '';
          $data_uuc['table_title_right'] = isset($uuc_data['table_title_right']) ? $uuc_data['table_title_right'] : '';
      }
      echo json_encode($data_uuc);
    exit;
    
} else if(isset($_GET['grnDetailId']) && isset($_GET['table_no'])) {
    $table_title = '';
    echo json_encode($table_title);
    exit;
} else {
    if(isset($_GET['grnDetailId']) && ($_GET['grnDetailId'] != 'Select Instrument')) {
        $selectDetail = "SELECT instrumentId,itemCode,rangeValue,temperature,humidity,uuc_location,approved_by,makeModel
                           FROM grndetail
                          WHERE grnDetailId = ".$_GET['grnDetailId'];
        $selectDetailResult = mysql_query($selectDetail);
        while($selectDetailRow = mysql_fetch_array($selectDetailResult)) {
            $itemCode = $selectDetailRow['itemCode'];
            $range    = $selectDetailRow['rangeValue'];
            $instrumentId    = $selectDetailRow['instrumentId'];
            $make_modal    = $selectDetailRow['makeModel'];
            $humidity = $selectDetailRow['humidity'];
            $temperature  = $selectDetailRow['temperature'];
            $uuc_location = $selectDetailRow['uuc_location'];
            $approved_by  = $selectDetailRow['approved_by'];
        }

        $grnDetail = "SELECT table_no
                      FROM observation_data
                      WHERE grn_detail_id = ".$_GET['grnDetailId'] ."
                         AND table_no IS NOT NULL GROUP BY table_no ORDER BY table_no ASC ";
        $grnDetailResult = mysql_query($grnDetail);
        $table_arr = array();
        $max_table_no = 0;
        $selected_table_no = 0;
        $selected_table_title = '';
        while($grnDetailRow = mysql_fetch_assoc($grnDetailResult)) {
            $table_arr[] = $grnDetailRow['table_no'];
            $max_table_no = $grnDetailRow['table_no'];
        }

        if(empty($table_arr)) {
            $table_arr[] = 1;
            $selected_table_no = 1;
            $selected_table_title = '';
        } else {
            $table_arr[] = $max_table_no + 1;
            $selected_table_no = $max_table_no;
        }
        $is_table = 0;
        if(isset($_GET['is_table'])) {
            $is_table = 1;
        }
        if(isset($_GET['edit_table_no'])) {
            $selected_table_no = $_GET['edit_table_no'];
        }
        if(isset($_GET['is_table_selected_val']) && !empty($_GET['is_table_selected_val'])) {
            $selected_table_no = $_GET['is_table_selected_val'];
        }
        $smarty->assign("is_table",$is_table);
        $smarty->assign("is_table_selected_val",$is_table_selected_val);
        $smarty->assign("itemCode",$itemCode);
        $smarty->assign("range",$range);
        $smarty->assign("instrumentId",$instrumentId);
        $smarty->assign("make_modal",$make_modal);
        $smarty->assign("humidity",$humidity);
        $smarty->assign("temperature",$temperature);
        $smarty->assign("uuc_location",$uuc_location);
        $smarty->assign("approved_by",$approved_by);
        $smarty->assign("grnDetailId",$grnDetailId);
        $smarty->assign("table_arr",$table_arr);
        $smarty->assign("selected_table_no",$selected_table_no);
        $smarty->assign("selected_table_title",$selected_table_title);
        $smarty->assign("edit_table_no",$edit_table_no);
        $smarty->display("obsSheetAjGrnDetailChange.tpl");
    }
}
?>