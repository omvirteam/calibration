<?php
include("include/omConfig.php");
$grnPrefixNo               = "";
$grnId                     = "";
$meterEntryNameArray       = "";
$meterEntryNameArray       = "";
$grnDetailId               = "";
$stdMeterAverage           = "";
$testMeterAverage          = "";
$error                     = "";
$masterMeterSubSubId       = "";
$callibrationDate          = 0;
if(isset($_REQUEST['grnDetailPassId']) && $_REQUEST['grnDetailPassId'] > 0)
  $grnDetailPassId = $_REQUEST['grnDetailPassId'] ;
else if(isset($_POST['grnDetailId'])) 
  $grnDetailPassId = $_POST['grnDetailId'];
else
  $grnDetailPassId = 0;
  
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  if(isset($_POST['grnDetailId']))
  {
    //  Update GRN Observation Detail : Start
    //  $obsDate = $_POST['obsDateYear']."-".$_POST['obsDateMonth']."-".$_POST['obsDateDay'];
    $callibrationDate          = ($_POST['callibrationDateYear']."-".$_POST['callibrationDateMonth']."-".$_POST['callibrationDateDay']);
    $nextYearDate              = ($_POST['nextYearDateYear']."-".$_POST['nextYearDateMonth']."-".$_POST['nextYearDateDay']);
    $updateGrnEntry = "UPDATE grndetail
                          SET refGrnDetailId = '".$grnDetailPassId."',masterMeterId = '".$_POST['masterMeterId']."',callibrationDate ='".$callibrationDate."',
                              nextYearDate = '".$nextYearDate."', makeModel = '".$_POST['makeModel']."',
                              instrumentId = '".$_POST['instrumentId']."',leastCount = '".$_POST['leastCount']."',
                              accuracy = '".$_POST['accuracy']."',temperature = '".$_POST['temperature']."',
                              humidity = '".$_POST['humidity']."', masterParameterId ='".$_POST['masterMeterSubId']."',
                              masterRangeId = '".$_POST['masterMeterSubSubId']."',userName = '".$_SESSION['s_activId']."'
                        WHERE grnDetailId = ".$_POST['grnDetailId'];
    $updateGrnEntryResult = mysql_query($updateGrnEntry);
    if(!$updateGrnEntryResult)
      die("Insert Query Not Inserted 1: ".mysql_error(). " : ".$updateGrnEntry);
    // Update GRN Observation Detail : Starts
    // Insert Query for GRN Observation Detail : Statrs
    $loopCount = 0;
    while($loopCount < count($_POST['stdMeterAverage']))
    {
      $stdMeterAverage           = ($_POST['stdMeterAverage'][$loopCount] != '') ? $_POST['stdMeterAverage'][$loopCount] : 0;
      $testMeterAverage          = ($_POST['testMeterAverage'][$loopCount] != '') ? $_POST['testMeterAverage'][$loopCount] : 0;
      $error                     = ($_POST['error'][$loopCount] != '') ? $_POST['error'][$loopCount] : 0;
      $percentageRdg             = ($_POST['percentageRdg'][$loopCount] != '') ? $_POST['percentageRdg'][$loopCount] : 0;

      if($_POST['stdMeterAverage'][$loopCount] != "" || $_POST['testMeterAverage'][$loopCount] != ""  )
      { 
        $insertQueryGrnDetail = "INSERT INTO grnobservation (grnId,grnDetailId,stdMeterAverage,testMeterAverage,error,
                                                            percentageRdg)
                                 VALUES (".$_POST['grnId'].",".$_POST['grnDetailId'].",'".$stdMeterAverage."','".$testMeterAverage."','".$error."','".$percentageRdg."')";
        $insertQueryGrnDetailResult = mysql_query($insertQueryGrnDetail);
        $grnDetailId = mysql_insert_id();
      }
      $loopCount++;
    }
  /////// Insert Query for GRN Observation Detail : Ends
}

  $selectGrn = "SELECT grnPrefix,grnNo,grnmaster.grnId
                  FROM grnmaster
                  WHERE grnPrefix = 'R'
                 ORDER BY grnPrefix,grnNo";
  $selectGrnResult = mysql_query($selectGrn);
  $i = 0;
  while($selectGrnRow = mysql_fetch_array($selectGrnResult))
  {
      $grnId[$i]       = $selectGrnRow['grnId'];
      $grnPrefixNo[$i] = $selectGrnRow['grnPrefix']."-".$selectGrnRow['grnNo'];
      $i++;
  }

  $selectGrn = "SELECT masterMeterId,masterMeterName
                  FROM mastermeter
                 ORDER BY masterMeterName";
  $selectGrnResult = mysql_query($selectGrn);
  $j = 0;
  while($selectGrnRow = mysql_fetch_array($selectGrnResult))
  {
      $meterEntryIdArray[$j]   = $selectGrnRow['masterMeterId'];
      $meterEntryNameArray[$j] = $selectGrnRow['masterMeterName'];
      $j++;
  }
    $nextYearDate = date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y")+1));

  include("./bottom.php");
  $smarty->assign("nextYearDate",$nextYearDate);
  $smarty->assign("meterEntryIdArray",$meterEntryIdArray);
  $smarty->assign("meterEntryNameArray",$meterEntryNameArray);
  $smarty->assign("grnId",$grnId);
  $smarty->assign("grnDetailId",$grnDetailId);
  $smarty->assign("stdMeterAverage",$stdMeterAverage);
  $smarty->assign("testMeterAverage",$testMeterAverage);
  $smarty->assign("error",$error);
  $smarty->assign("grnPrefixNo",$grnPrefixNo);
  $smarty->assign("grnDetailPassId",$grnDetailPassId);
  $smarty->assign("callibrationDate",$callibrationDate);
  $smarty->display("observationSheetRegular.tpl");
}
?>