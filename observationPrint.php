<?php
require('./fpdf.php');
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = isset($_POST['grnId']) ? $_POST['grnId'] : 0;
  $grnDetailId         = "";
  $grnPrefix           = "";
  $grnNo               = "";
  $grnDate             = "";
  $poNo                = "";
  $poDate              = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();
  // Pdf Create :Start
  $pdf = new FPDF('P','mm','A4');
  $pdf->AliasNbPages();
  $pdf->AddPage();

  //SELECT OF GRN MASTER :START
$selectGrnMaster = "SELECT grnPrefix,grnNo,DATE_FORMAT(grnDate,'%d-%m-%Y') AS grnDate,poNo,DATE_FORMAT(poDate,'%d-%m-%Y') AS poDate,
                             custName,custCode,remarks,grnmaster.contPerson,phNo,userName
                        FROM grnmaster
                        JOIN customer
                       WHERE grnId = ".$grnId."
                         AND grnmaster.customerId = customer.customerId";
  $selectGrnMasterRes    = mysql_query($selectGrnMaster);
  if($selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes))
  {
  }
  //SELECT OF GRN MASTER :END
  //SELECT OF GRN DETAIL :START
  $a  = 0;
  $countGrn = 0;
 $selectGrnEntry  = "SELECT grnDetail.grnDetailId,grndetail.grnId,item.itemName,grndetail.itemCode,grndetail.rangeValue,grnDetail.leastCount,grnDetail.accuracy,
                             parameterentry.parameterName
                        FROM grndetail
                        JOIN item
                        JOIN parameterentry
                       WHERE grnId =  ".$grnId."
                         AND grndetail.itemId = item.itemId
                         AND grndetail.parameterId = parameterentry.parameterId
                       ORDER BY grndetail.grnDetailId";
  $selectGrnEntryRes   = mysql_query($selectGrnEntry);
  while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
  {
  	if($a != 0 && !omIsOdd($a))  // 0 means first record, so we do not want to add new record for 0. when $a == 2 / 4 / ... Then we want new row
  	{
    	$yPositiondetail  +=  100;
      $xpositiondetail   = 45;
    }
    else{
      $yPositiondetail =  25;	
    }
    $countGrn++;

    if(($a % $cfgRecPerGrnPage) == 0)
    {
      $xpositiondetail  = 45;
      if($a > 0)
        $pdf->AddPage();
        grnmasterFunc();
    }
    grnDetailFunc($xpositiondetail,$yPositiondetail);
    
    $xpositiondetail += 100;
    $a++;
  }
  $b = 0;
  $selectObservationQuery = "SELECT grnObservationId,grnId,grnDetailId,stdMeter1,testMeter1,stdMeter2,testMeter2,
                                    stdMeter3,testMeter3,stdMeter4,testMeter4,stdMeter5,testMeter5
                               FROM grnobservation";
  $selectObservationQueryRes = mysql_query($selectObservationQuery);
  while($selectObservationRow = mysql_fetch_array($selectObservationQueryRes))
  {
  	 if(($b % $cfgRecPerGrnPage) == 0)
  	 {
  	   $xPosition = 70;
  	   if($b > 0)
  	   $pdf->AddPage();
  	   pageheader();
  	   pageHeaderSecond();
  	   pageHeaderThird();
  	   pageHeaderFourth();
  	   grnmasterFunc();	
  	 }
  	 grnObservationFunc($xPosition,$b+1);
  	 $xPosition += 20;
  	 $b++;
  }
                             
  $pdf->output();
  include("./bottom.php");
}

function omIsOdd($checkNumber)
{
	if($checkNumber % 2 == 1) 
	  return true;
	else
	  return false;
}
function grnmasterFunc()
{
  global $pdf;
  global $selectGrnMasterRow;
    $pdf->SetFont('Arial','',13);
    $pdf->SetXY(80,10);
    $pdf->Write(1,'KRISHNA INSTRUMENT');
    $pdf->SetXY(60,15);
    $pdf->Write(1,'CALLIBRATION OBSERVATION SHEET');
    $pdf->SetXY(162,20);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(5,'GRN No : '.$selectGrnMasterRow['grnPrefix']."-".$selectGrnMasterRow['grnNo']);
    $pdf->SetXY(20,20);
    $pdf->Write(5,'Code : '.($selectGrnMasterRow['custCode']));
}
//First grn Detail Display  Here :Start
function pageHeader()
{
  global $pdf;
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(15,25);
  $pdf->cell(25,05,'Instrument Name:','0', '0', 'L');
  $pdf->SetXY(15,30);
  $pdf->cell(25,05,'Make/Model:','', '0', 'L');
  $pdf->SetXY(15,35);
  $pdf->cell(25,05,'SrNo:','0', '0', 'L');
  $pdf->SetXY(15,40);
  $pdf->cell(180,05,'Instrument ID No:','0', '0', 'L');
  $pdf->SetXY(15,45);
  $pdf->cell(20,05,'Range','0', '0', 'L');
  $pdf->SetXY(15,50);
  $pdf->cell(20,05,'Least Count','0', '0', 'L');
  $pdf->SetXY(15,55);
  $pdf->cell(25,05,'Accuracy','0', '0', 'L');
}
//First grn Detail Display  Here :End

function pageHeaderSecond()
{
  global $pdf;
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(100,25);
  $pdf->cell(25,05,'Instrument Name:','0', '0', 'L');
  $pdf->SetXY(100,30);
  $pdf->cell(25,05,'Make/Model:','', '0', 'L');
  $pdf->SetXY(100,35);
  $pdf->cell(25,05,'SrNo:','0', '0', 'L');
  $pdf->SetXY(100,40);
  $pdf->cell(180,05,'Instrument ID No:','0', '0', 'L');
  $pdf->SetXY(100,45);
  $pdf->cell(20,05,'Range','0', '0', 'L');
  $pdf->SetXY(100,50);
  $pdf->cell(20,05,'Least Count','0', '0', 'L');
  $pdf->SetXY(100,55);
  $pdf->cell(25,05,'Accuracy','0', '0', 'L');
}
//First grn Detail Display  Here :End
//First grn Detail Display  Here :End

function pageHeaderThird()
{
  global $pdf;
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(15,125);
  $pdf->cell(25,05,'Instrument Name:','0', '0', 'L');
  $pdf->SetXY(15,130);
  $pdf->cell(25,05,'Make/Model:','', '0', 'L');
  $pdf->SetXY(15,135);
  $pdf->cell(25,05,'SrNo:','0', '0', 'L');
  $pdf->SetXY(15,140);
  $pdf->cell(180,05,'Instrument ID No:','0', '0', 'L');
  $pdf->SetXY(15,145);
  $pdf->cell(20,05,'Range','0', '0', 'L');
  $pdf->SetXY(15,150);
  $pdf->cell(20,05,'Least Count','0', '0', 'L');
  $pdf->SetXY(15	,155);
  $pdf->cell(25,05,'Accuracy','0', '0', 'L');
}
//First grn Detail Display  Here :End


function pageHeaderFourth()
{
  global $pdf;
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(100,125);
  $pdf->cell(25,05,'Instrument Name:','0', '0', 'L');
  $pdf->SetXY(100,130);
  $pdf->cell(25,05,'Make/Model:','', '0', 'L');
  $pdf->SetXY(100,135);
  $pdf->cell(25,05,'SrNo:','0', '0', 'L');
  $pdf->SetXY(100,140);
  $pdf->cell(180,05,'Instrument ID No:','0', '0', 'L');
  $pdf->SetXY(100,145);
  $pdf->cell(20,05,'Range','0', '0', 'L');
  $pdf->SetXY(100,150);
  $pdf->cell(20,05,'Least Count','0', '0', 'L');
  $pdf->SetXY(100,155);
  $pdf->cell(25,05,'Accuracy','0', '0', 'L');
}
//First grn Detail Display  Here :End
function grnDetailFunc($xpositiondetail,$yPositiondetail)
{
  //$pdf->Line(10, 70, 10, 125);
  global $pdf;
  global $selectGrnEntryResRow;
  $pdf->SetXY($xpositiondetail,$yPositiondetail+5);
   $pdf->cell(25,05,$selectGrnEntryResRow['itemCode'],'0', '0', 'L');
  $pdf->SetXY($xpositiondetail,$yPositiondetail);
  $pdf->cell(25,05,$selectGrnEntryResRow['itemName'],'0', '0', 'L');
  $pdf->SetXY($xpositiondetail,$yPositiondetail+20);
  $pdf->cell(20,05,$selectGrnEntryResRow['rangeValue'],'0', '0', 'L');
  $pdf->SetXY($xpositiondetail,$yPositiondetail+25);
  $pdf->cell(20,05,$selectGrnEntryResRow['leastCount'],'0', '0', 'L');
  $pdf->SetXY($xpositiondetail,$yPositiondetail+30);
  $pdf->cell(25,05,$selectGrnEntryResRow['accuracy'],'0', '0', 'L'); 

}
function grnObservationFunc($xPosition)
{
	global $pdf;
	global $selectObservationRow;
	$pdf->SetXY($xPosition,100);
  $pdf->cell(10,05,$selectObservationRow['stdMeter1'],'1', '0', 'L');
  $pdf->SetXY($xPosition,110);
  $pdf->cell(30,05,$selectObservationRow['stdMeter2'],'1', '0', 'L');
  $pdf->SetXY($xPosition,120);
  $pdf->cell(30,05,$selectObservationRow['testMeter2'],'1', '0', 'L');
}	
?>