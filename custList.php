<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(MASTER_CUSTOMER_MODULE_ID,"view")) || (have_access_role(MASTER_CUSTOMER_MODULE_ID,"edit")) ||  (have_access_role(MASTER_CUSTOMER_MODULE_ID,"delete"))){
  $custList = array();
  $customerId = "";
  $msg = "";

  $selectcustlist = "SELECT customerId,custName,custCode,address,city,state,country,fax,phone1,phone2,email,web,contPerson
                       FROM customer";
  $selectcustlistresult = mysql_query($selectcustlist);
  $custlistrow = mysql_num_rows($selectcustlistresult);
  if($custlistrow > 0)
  {
    $i = 0;
    while ($custlistrow = mysql_fetch_array($selectcustlistresult))
    {
      $custList[$i]['customerId'] = $custlistrow['customerId'];
      $custList[$i]['custName']   = $custlistrow['custName'];
      $custList[$i]['custCode']   = $custlistrow['custCode'];
      $custList[$i]['address']    = $custlistrow['address'];
      $custList[$i]['city']       = $custlistrow['city'];
      $custList[$i]['state']      = $custlistrow['state'];
      $custList[$i]['country']    = $custlistrow['country'];
      $custList[$i]['fax']        = $custlistrow['fax'];
      $custList[$i]['phone1']     = $custlistrow['phone1'];
      $custList[$i]['phone2']     = $custlistrow['phone2'];
      $custList[$i]['email']      = $custlistrow['email'];
      $custList[$i]['web']        = $custlistrow['web'];
      $custList[$i]['contPerson'] = $custlistrow['contPerson'];
      $i++;
    }
  }
  else
  {
    $msg = '<tr><td align="center" colspan="22"> <h1><font color="red"><b>Record Not Found...!</b></h1></font></td></tr>';
  }

  include("./bottom.php");
  $smarty->assign("msg",$msg);
  $smarty->assign("custList",$custList);
  $smarty->assign("customerId",$customerId);
  $smarty->display("custList.tpl");
} else {
  header("Location:index.php");
}  
}

?>