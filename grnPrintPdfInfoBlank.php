<?php
require('./fpdf.php');
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = "";
  $grnDetailId         = "";
  $grnNo               = "";
  $grnDate             = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();
  // Pdf Create :Start
  
  $pdf = new FPDF();
  $pdf->AliasNbPages();
  $pdf->AddPage();
    
  //SELECT OF GRN MASTER :START
  $selectGrnMaster = "SELECT grnPrefix,grnNo,infoSheetNo,DATE_FORMAT(grnDate,'%d-%m-%Y') AS grnDate,poNo,DATE_FORMAT(poDate,'%d-%m-%Y') AS poDate,custName,custCode,remarks,grnmaster.contPerson,phNo,userName
                        FROM grnmaster
                        JOIN customer
                       WHERE grnId = ".$_GET['grnId']."
                         AND grnmaster.customerId = customer.customerId";
  $selectGrnMasterRes = mysql_query($selectGrnMaster);
  $selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes);
  //SELECT OF GRN MASTER :END
  //SELECT OF GRN DETAIL :START
  $prevInstrument = 0;
  $a  = 0;
  $selectGrnEntry  = "SELECT grnDetail.grnDetailId,grndetail.grnId,grndetail.itemId AS grnDetailItemId,item.itemName,grndetail.itemCode,grndetail.rangeValue,
                             grndetail.description,grndetail.challan,grndetail.received,
                             grndetail.grnCondition,custReqDate,DATE_FORMAT(expDelivDate,'%d-%m-%Y') AS expDelivDate,
                             parameterentry.parameterName
                        FROM grndetail
                        JOIN item
                        JOIN parameterentry
                       WHERE grnId = ".$_GET['grnId']."
                         AND grndetail.itemId = item.itemId
                         AND grndetail.parameterId = parameterentry.parameterId
                       ORDER BY grndetail.grnDetailId";
  $selectGrnEntryRes   = mysql_query($selectGrnEntry);
  while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
  {
    if(($a % $cfgRecPerGrnPage) == 0)
    {
      $yPosition = 80;
      if($a > 0)
      {
        pageFooter($yPosition);
        $pdf->AddPage();
      }
      pageHeader();
      grnmasterFunc();
    }

    if($a > 0 && $prevInstrument == $selectGrnEntryResRow['grnDetailItemId'].$selectGrnEntryResRow['itemCode'] )
      grnDetailOnlyParamFunc($yPosition);
    else
    {
      $prevInstrument = $selectGrnEntryResRow['grnDetailItemId'].$selectGrnEntryResRow['itemCode'];
      grnDetailFunc($yPosition,$a+1);
      $a++;
    }

    $yPosition += 5;
  }
  pageFooter($yPosition);
  $pdf->output();
  include("./bottom.php"); 
}

function grnmasterFunc()
{
    global $pdf;
    global $selectGrnMasterRow;
    $pdf->SetFont('Arial','',13);
    $pdf->SetXY(12,36);
    $pdf->Write(1,'CUSTOMER ORDER INFO SHEET');
    $pdf->SetXY(175,36);
    $pdf->Write(1,'SM F 01');
    $pdf->SetXY(20,45);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(5,'Sheet No.: ');
    $pdf->SetXY(60,45);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(5,'GRN No.: ');
    $pdf->SetXY(105,45);
    $pdf->Write(5,'Date  : ');
    $pdf->SetXY(150,45);
    $pdf->Write(5,'Customer Code : ');
}

function pageFooter($yPosition)
{
  global $pdf, $selectGrnMasterRow;
  $yPosition += 30;
  $pdf->SetXY(10,$yPosition);
  $pdf->Write(5,'ANY OTHER INFO & REMARKS: ');
  $pdf->SetXY(10,$yPosition+5);
  $pdf->Write(5,'Sign(Customer\'s Rep.):');
  $pdf->SetXY(120,$yPosition+5);
  $pdf->Write(5,'Sign (KI): '.('_____________'));
  $pdf->SetXY(30,$yPosition+15);
  $pdf->Write(1,'For delivery of instruments this receipt is required, otherwise material will not be returned.');
}

function pageHeader()
{
  global $pdf;
  $pdf->Image('./images/logo.jpg',122,2,70,30);
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(13,55);
  $pdf->cell(180,05,' FOLLOWING INSTRUMENTS RECEIVED FOR - CALIBRATION','1', '0', 'C');

  $pdf->SetFont('Arial','',8);
  $pdf->SetXY(13,70);
  $pdf->cell(12,10,'SR.NO.','1', '0', 'C');
 
  $pdf->SetFont('Arial','',9);
  $pdf->SetXY(25,70);
  $pdf->cell(32,10,'','1', '0', 'C');
  $pdf->SetXY(32,73);
  $pdf->Write(1,'NAME OF');
  $pdf->SetXY(30,77);
  $pdf->Write(1,'INSTRUMENT');

  $pdf->SetFont('Arial','',9);
  $pdf->SetXY(57,70);
  $pdf->cell(25,10,'','1', '0', 'C');
  $pdf->SetXY(59,73);
  $pdf->Write(1,'INSTRUMENT');
  $pdf->SetXY(65,77);
  $pdf->Write(1,'ID No.');

  $pdf->SetXY(82,70);
  $pdf->SetFont('Arial','',8);
  $pdf->cell(22,10,'PARAMETERS	','1', '0', 'C');
  $pdf->SetXY(104,70);
  $pdf->cell(15,10,'RANGE','1', '0', 'C');

  $pdf->SetXY(119,70);
  $pdf->cell(30,10,'','1', '0', 'C');
  $pdf->SetXY(124,72);
  $pdf->Write(1,'Cali Due Date');
  $pdf->SetXY(121,76);
  $pdf->Write(1,'Requested By Cust.');

  $pdf->SetXY(149,70);
  $pdf->cell(25,10,'','1', '0', 'C');
  $pdf->SetXY(154,72);
  $pdf->Write(1,'Expected');
  $pdf->SetXY(152,76);
  $pdf->Write(1,'Delivery Date');

  $pdf->SetXY(174,70);
  $pdf->cell(19,10,'','1', '0', 'C');
  $pdf->SetXY(175,72);
  $pdf->Write(1,'Condition of');
  $pdf->SetXY(176,76);
  $pdf->Write(1,'Instrument');
}

function grnDetailFunc($yPosition,$srNo)
{
  //$pdf->Line(10, 70, 10, 125);
  global $pdf;
  global $selectGrnEntryResRow;
  $pdf->SetXY(13,$yPosition);
  $pdf->cell(12 ,05," ",'1', '0', 'C');
  $pdf->SetXY(25,$yPosition);
  $pdf->cell(32,05," ",'1', '0', 'C');
  $pdf->SetXY(57,$yPosition);
  $pdf->Cell(25,05," ",'1', '0', 'C');
  $pdf->SetXY(82,$yPosition);
  $pdf->cell(22,05," ",'1', '0', 'C');
  $pdf->SetXY(104,$yPosition);
  $pdf->cell(15,05," ",'1', '0', 'C');
  $pdf->SetXY(119,$yPosition);
  $pdf->cell(30,05," ",'1', '0', 'C');
  $pdf->SetXY(149,$yPosition);
  $pdf->cell(25,05," ",'1', '0', 'C');

  $pdf->SetFont('Arial','',8);
  $pdf->SetXY(174,$yPosition);
  $pdf->cell(19,05," ",'1', '0', 'C');

  $pdf->SetFont('Arial','',10);
}

function grnDetailOnlyParamFunc($yPosition)
{
  //$pdf->Line(10, 70, 10, 125);
  global $pdf;
  global $selectGrnEntryResRow;
  $pdf->SetXY(13,$yPosition);
  $pdf->cell(12 ,05,'','1', '0', 'C');
  $pdf->SetXY(25,$yPosition);
  $pdf->cell(32,05,'','1', '0', 'C');
  $pdf->SetXY(57,$yPosition);
  $pdf->Cell(25,05,'','1', '0', 'C');
  $pdf->SetXY(82,$yPosition);
  $pdf->cell(22,05," ",'1', '0', 'C');
  $pdf->SetXY(104,$yPosition);
  $pdf->cell(15,05,'','1', '0', 'C');
  $pdf->SetXY(119,$yPosition);
  $pdf->cell(30,05,'','1', '0', 'C');
  $pdf->SetXY(149,$yPosition);
  $pdf->cell(25,05,'','1', '0', 'C');

  $pdf->SetFont('Arial','',8);
  $pdf->SetXY(174,$yPosition);
  $pdf->cell(19,05,'','1', '0', 'C');

  $pdf->SetFont('Arial','',10);
}
?>