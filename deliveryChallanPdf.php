<?php
require('./fpdf.php');
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = "";
  $grnDetailId         = "";
  $grnPrefix           = "";
  $grnNo               = "";
  $grnDate             = "";
  $poNo                = "";
  $poDate              = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();
  // Pdf Create :Start
  $pdf = new FPDF('P','mm','A4');
  $pdf->AliasNbPages();
  $pdf->AddPage();
    
  //SELECT OF GRN MASTER :START
  $selectGrnMaster = "SELECT grnPrefix,grnNo,DATE_FORMAT(grnDate,'%d-%m-%Y') AS grnDate,poNo,DATE_FORMAT(poDate,'%d-%m-%Y') AS poDate,
                             custName,custCode,remarks,gatepass.userName,gatepass.deliveryMode,DATE_FORMAT(gatepass.gatePassDate,'%d-%m-%Y') AS gatePassDate,gatepass.gatePassId
                        FROM grnmaster
                        JOIN customer
                        JOIN gatepass
                       WHERE gatepass.gatePassId = ".$_GET['gatePassId']."
                         AND grnmaster.grnId = gatepass.grnId
                         AND grnmaster.customerId = customer.customerId";
  $selectGrnMasterRes = mysql_query($selectGrnMaster);
  if($selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes))
  {
   
  }
  //SELECT OF GRN MASTER :END
  //SELECT OF GRN DETAIL :START
  for($z=0; $z < 2; $z++)
  {
    $a  = 0;
    $selectGrnEntry  = "SELECT grnDetail.grnDetailId,grndetail.grnId,item.itemName,grndetail.itemCode,grndetail.rangeValue,grndetail.description,grndetail.challan,grndetail.received,
                               grndetail.grnCondition,grndetail.gatePassId,DATE_FORMAT(custReqDate,'%d-%m-%Y') AS custReqDate,DATE_FORMAT(expDelivDate,'%d-%m-%Y') AS expDelivDate,
                               parameterentry.parameterName
                          FROM grndetail 
                          JOIN item           ON grndetail.itemId      = item.itemId 
                          JOIN parameterentry ON grndetail.parameterId = parameterentry.parameterId
                         WHERE grndetail.gatePassId = ".$_GET['gatePassId']."
                         ORDER BY grndetail.grnDetailId";
    $selectGrnEntryRes   = mysql_query($selectGrnEntry);
    while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
    {
    	if(mysql_num_rows($selectGrnEntryRes) > 8)
    	{
        if(($a % 34) == 0)
        {
          $yPosition = 60;
          if($a > 0){
          	pageFooter1($lastYposition);
            $pdf->AddPage();
          }
          if($z == 0){
            pageHeader('office');
            grnmasterFunc();
          }
          else
          {
            pageHeader('customer');
            grnmasterFunc();
          }
        }
    		$lastYposition = gatePassDetailFunc($yPosition,$a+1);
        $yPosition += 5;
      }
      else
      {
        if(($a % $cfgRecPerGrnPage) == 0)
        {
          $yPosition = 60;
          $yPositionSecondPart = 205;
          if($a > 0)
            $pdf->AddPage();
          pageHeader('office');
          pageHeaderSecondPart();
          grnmasterFunc();
          pageFooter1(105);
          grnmasterFuncSecondPart();
          pageFooter2();
        }
        gatePassDetailFunc($yPosition,$a+1);
        $yPosition += 5;
        
        gatePassFuncSecondPart($yPositionSecondPart,$a+1);
        $yPositionSecondPart += 5;
      }
      $a++;
    }
    if(mysql_num_rows($selectGrnEntryRes) > 8)
      pageFooter1($lastYposition);
    
    if($z == 0 && mysql_num_rows($selectGrnEntryRes) > 8)
      $pdf->AddPage();
  }
  $pdf->output();
  include("./bottom.php");
}
// First Part Of grn headerEnd And Footer part: Start

function grnmasterFunc()
{
    global $pdf;
    global $selectGrnMasterRow;
    $pdf->SetFont('Arial','',10);
    $pdf->SetXY(12,12);
    $pdf->Write(1,'DOC. NO.: ST F 03');
    $pdf->SetFont('Arial','',10);
    $pdf->SetXY(12,22);
    $pdf->Write(1,'Grn No.   :  '.$selectGrnMasterRow['grnPrefix']."-".$selectGrnMasterRow['grnNo']);
    $pdf->SetXY(12,17);
    if($selectGrnMasterRow['grnPrefix'] == "N")
    {
      $pdf->cell(43,20,'No.          :  D-'.($selectGrnMasterRow['grnNo']),'1', '0', 'L');
    }
    else
    {
      $pdf->cell(43,20,'No.          :  '.($selectGrnMasterRow['grnNo']),'1', '0', 'L');
    }
    $pdf->SetXY(12,32);
    $pdf->Write(1,'Date        :  '.($selectGrnMasterRow['gatePassDate']));
    $pdf->SetXY(30,25);
    $pdf->Write(5,''.('__________'));
    $pdf->SetXY(30,30);
    $pdf->Write(5,''.('__________'));
        
    $pdf->SetXY(12,42);
    $pdf->Write(5,'M/S : '.($selectGrnMasterRow['custName']));
    $pdf->SetXY(21,42);
    $pdf->Write(5,''.('_______________________________________________________________________________________'));
    $pdf->SetXY(12,48);
    $pdf->Write(5,''.('____________________________________________________________________________________________'));
}
function pageFooter1($lastYposition)
{
    // master Meter Footer : Start 
    global $pdf;
    global $selectGrnMasterRow;
    $pdf->SetFont('Arial','',10);
    $pdf->Write(5,'');
    $pdf->SetXY(10,$lastYposition + 10);
    $pdf->Write(5,''.('Mode Of Delivery: '.$selectGrnMasterRow['deliveryMode']));
    $pdf->SetXY(10,$lastYposition + 17);
    $pdf->Write(5,''.('_______________'));
    $pdf->SetXY(13,$lastYposition + 24);
    $pdf->Write(5,'Received  By: ');
    $pdf->SetXY(50,$lastYposition + 15);
    $pdf->Write(5,'');
    $pdf->SetXY(50,$lastYposition + 17);
    $pdf->Write(5,''.('_______________'));
    $pdf->SetXY(53,$lastYposition + 24);
    $pdf->Write(5,'Receiver Name: ');
    $pdf->SetXY(155,$lastYposition + 16);
    $pdf->Write(5,''.($selectGrnMasterRow['userName']));
    $pdf->SetXY(150,$lastYposition + 17);
    $pdf->Write(5,''.('_______________'));
    $pdf->SetXY(153,$lastYposition + 24);
    $pdf->Write(5,'Prepared By: ');
    $pdf->SetXY(130,$lastYposition + 30);
    $pdf->Write(5,'For & On behalf of Krishna Instrumensts. ');
    $pdf->SetXY(10,$lastYposition + 32);
    $pdf->Write(5,'Note: Receive the Above Goods In Order And Good Condtions. ');
    // master Meter Footer : End
}
// Second Part Of grn headerEnd And Footer part: Start

function grnmasterFuncSecondPart()
{
    global $pdf;
    global $selectGrnMasterRow;
    $pdf->SetFont('Arial','',13);
    $pdf->SetXY(80,155);
    $pdf->Write(1,'Delivery Challan');
    $pdf->SetXY(80,160);
    $pdf->Write(1,'Customer Copy');
    
    $pdf->SetFont('Arial','',10);
    $pdf->SetXY(12,155);
    $pdf->Write(1,'DOC. NO.: ST F 03');
    $pdf->SetFont('Arial','',10);
    $pdf->SetXY(12,164);
    $pdf->Write(1,'Grn No.   : '.$selectGrnMasterRow['grnPrefix']."-".$selectGrnMasterRow['grnNo']);
    $pdf->SetXY(12,160);
    if($selectGrnMasterRow['grnPrefix'] == "N")
    {
      $pdf->cell(43,20,'No.          : D-'.($selectGrnMasterRow['grnNo']),'1', '0', 'L');
    }
    else
    {
      $pdf->cell(43,20,'No.          : '.($selectGrnMasterRow['grnNo']),'1', '0', 'L');
    }
    $pdf->SetXY(12,175);
    $pdf->Write(1,'Date        : '.($selectGrnMasterRow['gatePassDate']));
    $pdf->SetXY(29,167);
    $pdf->Write(5,''.('____________'));
    $pdf->SetXY(29,173);
    $pdf->Write(5,''.('____________'));

    $pdf->SetXY(12,187);
    $pdf->Write(5,'M/S : '.($selectGrnMasterRow['custName']));
    $pdf->SetXY(21,187);
    $pdf->Write(5,''.('_______________________________________________________________________________________'));
    $pdf->SetXY(12,193);
    $pdf->Write(5,''.('____________________________________________________________________________________________'));
}
function pageFooter2()
{
  // master Meter Footer : Start
  global $pdf;
  global $selectGrnMasterRow;
  $pdf->SetFont('Arial','',10);
  $pdf->Write(5,'');
  $pdf->SetXY(10,255);
  $pdf->Write(5,''.('Mode Of Delivery: '.$selectGrnMasterRow['deliveryMode']));
  $pdf->SetXY(10,260);
  $pdf->Write(5,''.('_______________'));
  $pdf->SetXY(13,265);
  $pdf->Write(5,'Received  By: ');
  $pdf->SetXY(50,260);
  $pdf->Write(5,'');
  $pdf->SetXY(50,260);
  $pdf->Write(5,''.('_______________'));
  $pdf->SetXY(53,265);
  $pdf->Write(5,'Receiver Name: ');
  $pdf->SetXY(155,259);
  $pdf->Write(5,''.($selectGrnMasterRow['userName']));
  $pdf->SetXY(150,260);
  $pdf->Write(5,''.('_______________'));
  $pdf->SetXY(153,265);
  $pdf->Write(5,'Prepared By: ');
  $pdf->SetXY(130,270);
  $pdf->Write(5,'For & On behalf of Krishna Instrumensts. ');
  $pdf->SetXY(10,271);
  $pdf->Write(5,'Note: Receive the Above Goods In Order And Good Condtions. ');
  // master Meter Footer : End
}
// First Part Of grn headerEnd And Footer part: End 
// Header Part Of grn Print pdf:Start 
function pageHeader($head)
{
  global $pdf;
  $pdf->Image('./images/logo.jpg',122,2,70,30,'','http://localhost/kcallibration/deliveryChallan.php');

  $pdf->SetFont('Arial','',13);
  $pdf->SetXY(80,5);
  $pdf->Write(1,'Delivery Challan');
  
  if($head == 'office'){
  	$pdf->SetXY(83,10);
    $pdf->Write(1,'Office Copy');
  }else{
  	$pdf->SetXY(80,10);
    $pdf->Write(1,'Customer Copy');
  }

  $pdf->SetFont('Arial','',7);
  $pdf->SetXY(13,55);
  $pdf->cell(15,05,'SR.NO.','1', '0', 'L');

  $pdf->SetXY(28,55);
  $pdf->cell(165,05,'PARTICULARS','1', '0', 'L');
  
  $pdf->SetXY(173,55);
  $pdf->cell(20,05,'QUANTITY','1', '0', 'L');

}
//First Part Of Detail Of Grn :Start
//second part page Header :Start 
function pageHeaderSecondPart()
{
  global $pdf;
  $pdf->Image('./images/logo.jpg',122,155,70,30);

  $pdf->SetFont('Arial','',7);
  $pdf->SetXY(13,200);
  $pdf->cell(15,05,'SR.NO.','1', '0', 'L');

  $pdf->SetXY(28,200);
  $pdf->cell(165,05,'PARTICULARS','1', '0', 'L');
  
  $pdf->SetXY(173,200);
  $pdf->cell(20,05,'QUANTITY','1', '0', 'L');
}
//second part page Header :Start 
function gatePassDetailFunc($yPosition,$srNo)
{
  global $pdf;
  global $selectGrnEntryResRow;
  $pdf->SetXY(13,$yPosition);
  $pdf->cell(180 ,05,$srNo,'1', '0', 'L');
  $pdf->SetXY(28,$yPosition);
  $pdf->Cell(165,05,$selectGrnEntryResRow['itemName']." ".$selectGrnEntryResRow['description'],1, "NOWRAP");
  $pdf->SetXY(173,$yPosition);
  $pdf->cell(20,05,$selectGrnEntryResRow['received'],'1', '0', 'L');
  return $yPosition;
}

function gatePassFuncSecondPart($yPositionSecondPart,$srNo)
{
  global $pdf;
  global $selectGrnEntryResRow;
  $pdf->SetXY(13,$yPositionSecondPart);
  $pdf->cell(180 ,05,$srNo,'1', '0', 'L');
  $pdf->SetXY(28,$yPositionSecondPart);
  $pdf->Cell(165,05,$selectGrnEntryResRow['itemName']." ".$selectGrnEntryResRow['description'],1, "NOWRAP");
  $pdf->SetXY(173,$yPositionSecondPart);
  $pdf->cell(20,05,$selectGrnEntryResRow['received'],'1', '0', 'L');
}
//First Part Of Detail Of Grn :End
?>