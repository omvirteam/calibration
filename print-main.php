<?php
// reference the Dompdf namespace
require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;
// instantiate and use the dompdf class
ob_start();
?>
<html>
    <head>
        <style>
            /** Define the margins of your page **/
            main {
              font-family: "Times New Roman";
              font-size: 13px;
            }
            table {font-family: "Times New Roman"; font-size: 13px; }
            .main-table {
              width: 100% !important;
            }
            @page {
                margin: 0px;
            }

            tr {line-height:14.5px}
            .main-page {
              width: 630px;
              margin-top:162px;
              margin-right: 38px;
              margin-left: 113px;
              margin-bottom: 50px;
              border: 1px solid black;
              border-radius: 10px;
              padding: 5px;
            }
        </style>
    </head>
    <body>
        <!-- Define header and footer blocks before your content -->
        <header>
        </header>

        <footer>

        </footer>

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
          <div class="main-page" >
             <table>
                <tr>
                   <td width="150px">CERTIFICATE NO.</td>
                   <td width="145px">: KNE0136GN03</td>
                   <td width="5px">&nbsp;</td>
                   <td width="200px">DATE OF CALIBRATION</td>
                   <td width="95px" align="right">: 03/08/2020</td>
                </tr>
                <tr>
                   <td>GRN NO.</td>
                   <td>: NE-0136</td>
                   <td>&nbsp;</td>
                   <td>NEXT RECOMMENDED DATE</td>
                   <td align="right">: 02/08/2021</td>
                </tr>
                <tr>
                   <td width="150px">PAGE</td>
                   <td width="145px">: 1 OF <span class="total_page_no">2</span></td>
                   <td width="5px">&nbsp;</td>
                   <td width="200px">CERTIFICATE ISSUE DATE</td>
                   <td width="95px" align="right">: 03/08/2020</td>
                </tr>
             </table>
             <hr />
             <table >
                <tr valign="top">
                   <td width="5px"><strong>1.</strong></td>
                   <td width="235px"><strong>CUSTOMER NAME & ADDRESS</strong></td>
                   <td width="5px">:</td>
                   <td width="360px" style="font-family: Agency FB;font-size:18px; font-weight:bold;"> NewAge Fire Fighting Co.Ltd.</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>&nbsp;</td>
                   <td>&nbsp;</td>
                   <td style="font-size:14px;" width="360">
                      Plot No.: 93,94,
                      GIDC, Phase-I
                      Wadhwan-363030
                   </td>
                </tr>
                <tr valign="top">
                   <td><strong>2.</strong></td>
                   <td><strong>CUSTOMER REFERENCE NO.</strong></td>
                   <td>:</td>
                   <td>NF/Cali/879</td>
                </tr>
                <tr valign="top">
                   <td><strong>3.</strong></td>
                   <td><strong>INSTRUMENT RECEIVED ON</strong></td>
                   <td>:</td>
                   <td>23/07/2020</td>
                </tr>
                <tr valign="top">
                   <td><strong>4.</strong></td>
                   <td><strong>ULR NO.</strong></td>
                   <td>:</td>
                   <td>CC238120000001555F</td>
                </tr>
                <tr valign="top">
                   <td><strong>5.</strong></td>
                   <td><strong>DESCRIPTION OF INSTRUMENT</strong></td>
                   <td>:</td>
                   <td>&nbsp;</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>Name</td>
                   <td>:</td>
                   <td>Turbine Flowmeter</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>Make/Model</td>
                   <td>:</td>
                   <td>225455</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>Sr. No.</td>
                   <td>:</td>
                   <td>26587</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>Instrument ID No.</td>
                   <td>:</td>
                   <td>123900</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>Range</td>
                   <td>:</td>
                   <td>13.5-135 m3/hr</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>Least Count</td>
                   <td>:</td>
                   <td>99</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>Accuracy</td>
                   <td>:</td>
                   <td>99</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>Condition on receipt</td>
                   <td>:</td>
                   <td>Ok</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>UUC Location</td>
                   <td>:</td>
                   <td>AT SITE</td>
                </tr>
                <tr valign="top">
                   <td>&nbsp;</td>
                   <td>Remark</td>
                   <td>:</td>
                   <td></td>
                </tr>
                <tr valign="top">
                   <td><strong>6.</strong></td>
                   <td><strong>AMBIENT TEMPERATURE</strong></td>
                   <td>:</td>
                   <td style="font-family:dejavusans; font-size: 12px;">299</td>
                </tr>
                <tr valign="top">
                   <td><strong></strong></td>
                   <td><strong>RELATIVE HUMIDITY</strong></td>
                   <td>:</td>
                   <td>199</td>
                </tr>
             </table>
             <table >
                <tr valign="top">
                   <td width="5px"><strong>7.</strong></td>
                   <td width="600px"><strong>DETAILS OF REFERENCE STANDARD & MAJOR INSTRUMENTS USED</strong></td>
                </tr>
                <tr valign="top">
                   <td width="5px">&nbsp;</td>
                   <td width="600px">
                      <table cellpadding="0" cellspacing="0" border="1" style="font-size:14px;" width="100%">
                         <tr>
                            <td align="left" style="padding: 2px;" valign="center"><b>Name</b></td>
                            <td align="left" style="padding: 2px;" valign="center">Ac Multifunction Calibrator	</td>
                         </tr>
                         <tr>
                            <td align="left" style="padding: 2px;" valign="center"><b>I.D. No.</b></td>
                            <td align="left" style="padding: 2px;" valign="center">009</td>
                         </tr>
                         <tr>
                            <td align="left" style="padding: 2px;" valign="center"><b>Make</b></td>
                            <td align="left" style="padding: 2px;" valign="center">MECO</td>
                         </tr>
                         <tr>
                            <td align="left" style="padding: 2px;" valign="center"><b>Model No.</b></td>
                            <td align="left" style="padding: 2px;" valign="center">MECO 90P</td>
                         </tr>
                         <tr>
                            <td align="left" style="padding: 2px;" valign="center"><b>Serial no.</b></td>
                            <td align="left" style="padding: 2px;" valign="center">20091107</td>
                         </tr>
                         <tr>
                            <td align="left" style="padding: 2px;" valign="center"><b>Certificate No.</b></td>
                            <td align="left" style="padding: 2px;" valign="center">CC/ECL/1257/12-13</td>
                         </tr>
                         <tr>
                            <td align="left" style="padding: 2px;" valign="center"><b>Calibration Valid Up to</b></td>
                            <td align="left" style="padding: 2px;" valign="center">05/12/2017</td>
                         </tr>
                         <tr>
                            <td align="left" style="padding: 2px;" valign="center"><b>Traceability To </b></td>
                            <td align="left" style="padding: 2px;" valign="center" colspan="1">Equipments used for calibration are tracebled to National/International Standards.</td>
                         </tr>
                      </table>
                   </td>
                </tr>
             </table>
             <table width="605px">
                <tr valign="top">
                   <td width="5px"><strong>8.</strong></td>
                   <td width="235px"><strong>PROCEDURE</strong></td>
                   <td width="5px">:</td>
                   <td width="360px">TRP02/TRW12</td>
                </tr>
                <tr valign="top">
                   <td width="5px"><strong>9.</strong></td>
                   <td width="235px"><strong>CALIBRATION LOCATION</strong></td>
                   <td width="5px">:</td>
                   <td width="360px">At Lab</td>
                </tr>
                <tr valign="bottom">
                   <hr />
                   <td width="5px" style="height: 5px;"><strong></strong></td>
                   <td colspan="3" style="height: 5px;" width="600px" style="font-size: 20px;"></td>
                </tr>
                <tr valign="bottom">
                   <td colspan="4"><hr /></td>
                </tr>
                <tr valign="bottom">
                   <td width="5px"><strong>&nbsp;</strong></td>
                   <td colspan="3" width="600px" style="font-size: 20px;"><img src="./images/for-krishna.png" /></td>
                </tr>
                <tr valign="top">
                   <td colspan="4">&nbsp;</td>
                </tr>
                <tr valign="top">
                   <td colspan="4">&nbsp;</td>
                </tr>
                <tr valign="top">
                   <td colspan="4">&nbsp;</td>
                </tr>
                <tr valign="top">
                   <td colspan="4">CALIBRATED BY: (Om) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; APPROVED BY: (D.R.SHAH, C.E.O) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; SEAL :</td>
                </tr>
                <tr valign="top">
                   <td width="5px"><strong>Note&nbsp;:</strong></td>
                   <td>&nbsp;</td>
                   <td>&nbsp;</td>
                   <td>&nbsp;</td>
                </tr>
                <tr valign="top">
                   <td colspan="4" style="font-size: 12px;">1 This report pertains to particular sample /Instrument submitted for test.<br/>
                      2 This certificate may not be reproduced, except in full, without prior written permission of Krishna Instruments.<br/>
                      3 This calibration results reported in the certificate are valid at the time of measurements and under stated condition.<br/>
                   </td>
                </tr>
             </table>
          </div>
        </main>
    </body>
</html>
<?php
$content = ob_get_clean();
//echo $content;
$dompdf = new Dompdf();
$dompdf->loadHtml($content);
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();
?>
