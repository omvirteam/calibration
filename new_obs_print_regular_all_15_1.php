<table width="605" align="left" class="main_table"  style="font-size:13px;">
                <?php
                $is_3_phase = 0;
                foreach ($grn_data as $k => $grn_entry){ 
                    if($grn_entry['phase_type_id'] == 2){
                        $is_3_phase = 1;
                        break;
                    }
                }
                $table_title = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(!empty($grn_entry['table_title'])){
                        $table_title = $grn_entry['table_title'];
                        break;
                    }
                }
                $table_title_right = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                        $table_title_right = $grn_entry['table_title_right'];
                        break;
                    }
                }
                $pre_range = '';
                $pre_range_key = '';
                $pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(empty($pre_range)){
                        $pre_range = $grn_entry['mRangeId'];
                        $pre_range_key = $k;
                        $pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span'] = 1;
                        }
                    } else {
                        if($pre_range == $grn_entry['mRangeId']){
                            $pre_range_total = $pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        } else {
                            $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                $t_pre_range = '';
                $t_pre_range_key = '';
                $t_pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    $uuc_range_print_table = isset($grn_entry['uuc_range_print_table']) ? $grn_entry['uuc_range_print_table'] : '';
                    if($k == 0){
                        
                        $t_pre_range = $uuc_range_print_table;
                        $t_pre_range_key = $k;
                        $t_pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span_test'] = 1;
                        }
                    } else {
                        if($t_pre_range == $uuc_range_print_table){
                            $t_pre_range_total = $t_pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        } else {
                            $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            $t_pre_range = $uuc_range_print_table;
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                if(!empty($is_3_phase)){
                    $col_span1 = '9';
                } else {
                    $col_span1 = '6';
                }
                if(!empty($view_uncertainity)){
                    $col_span1 = $col_span1 + 1;
                }
                ?>
                <tr>
                    <th valign="middle" colspan="7" align="center" >OBSERVATION</th>
                </tr>
                <tr>
                    <th valign="middle" align="center">Sr. No.</th>
                    <th align="center" valign="middle">Std.Meter Value</th>
                    <th align="center" valign="middle">Test Meter Value</th>
                    <th align="center" colspan="2" valign="middle">Error</th>
                </tr>
                <?php 
                    $total_test_meter_val = 0;
                    $total_std_meter_val = 0;
                    foreach ($grn_data as $k => $grn_entry){ 
                    $i = $k + 1;
                    
                    $master_hz = '';
                    $master_unit_type = '';
                    $master_range = '';
                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                            WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    while($row1 = mysql_fetch_assoc($res1)) { 
                        $master_hz = $row1['frequency'];
                        $master_unit_type = $row1['range_unit'];
                        $master_range = $row1['range_disp'];
                    }
                    
                    ?>
                    <tr>
                        <td align="center" valign="middle"><?php echo $i; ?></td>
                        <?php 
                            $test_meter_val = $grn_entry['testMeterAverage'];
                            $std_meter_val = $grn_entry['stdMeterAverage'];
                           $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                           $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                           $decimal_master = 0;
                           $resolution_of_master = $grn_entry['resolution_of_master'];
                           
                           if(!empty($resolution_of_master)){
                               $exp_val = explode('.', $resolution_of_master);
                               if(isset($exp_val[1])){
                                   $decimal_master = strlen($exp_val[1]);  
                               } else {
                                 $decimal_master = 0;  
                               }
                           } else {
                               $decimal_master = 0;
                           }
                           $decimal_least = 0;
                           $resolution_of_least = $grn_entry['leastCount'];
                           if(!empty($resolution_of_least)){
                               $exp_least = explode('.', $resolution_of_least);
                               if(isset($exp_least[1])){
                                   $decimal_least = strlen($exp_least[1]);  
                               } else {
                                 $decimal_least = 0;  
                               }
                           } else {
                               $decimal_least = 0;
                           }
                        ?>
                        <td valign="middle" align="center"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                        <td valign="middle" align="center"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                        <?php $per = 100 * ($test_meter_val - $std_meter_val) / $test_meter_val;  ?>
                        <td valign="middle" align="center"><?php echo number_format((float)$per, 3, '.', ''); ?></td>
                    </tr>
                <?php } ?>
            </table>