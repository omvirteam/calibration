<?php
include("include/omConfig.php");

//error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
	if($haystack == null){
		return false;
	}else{
 foreach($haystack as $item) {
 	      if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
}
    return false;
}
// general functions - start //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// variable list - start //
$certificateNo 			= '';
$certificate_no			= '';
$grnNo 					= '';
$grn_no 					= '';
$grnDate				= ''; 
$totalPages 			= 0;
$callibrationDate		= '';
$callibrationDateNext 	= '';
$certificateIssue		= '';
$certificateIssueDate	= '';
$customerName			= '';
$customerAddress		= '';
$customerCity		= '';
$customerRefNo			= '';
$ambientTemperature		= '';
$relativeHumidity		= '';
$remarks				= '';

$counter 				= 0;
$grnDetailIds 			= array();
$tempArr 				= array() ;

$instrumentDetails					= array();
$instrumentDetails['item_name']		= '';
$instrumentDetails['make']			= '';
$instrumentDetails['item_code']		= '';
$instrumentDetails['id_no']			= '';
$instrumentDetails['range_value']	= '';
$instrumentDetails['accuracy']		= '';
$instrumentDetails['grn_condition']	= '';
$instrumentDetails['certi_remarks']	= '';
$instrumentDetails['cali_location']	= '';
$instrumentDetails['uuc_location']	= '';
$instrumentDetails['ulr_no']	= '';
$instrumentDetails['userName']	= '';
$instrumentDetails['approved_by']	= '';
$instrumentDetails['designation']	= '';
$instrumentDetails['sr_no']	= '';

$masterMeters						= array();
$masterMeters['masterMeterName']			= array();
$masterMeters['masterMeterIdNo']				= array();
$masterMeters['masterMeterMake']			= array();
$masterMeters['masterMeterModelNo']				= array();
$masterMeters['masterMeterSerialNo']			= array();
$masterMeters['masterMeterCertificateNo']		= array();
$masterMeters['due_date']	= array();

$observations						= array();
/*$observations['parameters']			= array();
$observations['table_no']			= array();
$observations['range1']				= array();
$observations['reading1']			= array();
$observations['range2']				= array();
$observations['reading2']			= array();
$observations['units']				= array();
$observations['rdg']				= array();
$observations['expanded']			= array();*/
// variable list - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// fetch data - start //
$qry = 'SELECT DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate,
	DATE_FORMAT(grndetail.nextYearDate,"%d/%m/%Y") AS nextYearDate,
	certificateIssue,DATE_FORMAT(grndetail.certificateIssueDate,"%d/%m/%Y") AS certificateIssueDate,
	grndetail.grnId,grndetail.certificate_no,grndetail.grnDetailId,
	grndetail.temperature,grndetail.humidity,itemCode,
	certiRemarks,
	CONCAT("N-",grnmaster.grnNo) AS grnNo,grnmaster.grn_no,
	DATE_FORMAT(grnmaster.grnDate,"%d/%m/%Y") AS grnDate,
	customer.custName,customer.address,customer.city,
	customer.custCode AS customerRef 
	FROM grndetail 
	LEFT JOIN grnmaster ON grnmaster.grnId = grndetail.grnId 
	LEFT JOIN customer ON customer.customerId = grnmaster.customerId 
	WHERE grndetail.grnDetailId = '.$_GET['grnDetailId'];
$res = mysql_query($qry) or die("Error :: Cannot select basic details.<hr>".mysql_error());
if($row = mysql_fetch_assoc($res)) {
	$grn_no 					= $row['grn_no'];
	$certificate_no 					= $row['certificate_no'];
	$grnDate				= $row['grnDate'];
	$callibrationDate		= $row['callibrationDate'];
	$callibrationDateNext 	= $row['nextYearDate'];
	$certificateIssue		= $row['certificateIssue'];
	$certificateIssueDate	= $row['certificateIssueDate'];
	$customerName			= $row['custName'];
	$customerAddress		= $row['address'];
	$customerCity		= $row['city'];
        $ref_no = '';
        $words = preg_split("/\s+/", trim($row['custName']));
//        echo "<pre>"; print_r($words); exit;
        $acronym = "";
        foreach ($words as $w) {
            if(strlen($acronym) == 2){
                break;
            } else {
                $acronym .= $w[0];
            }
        }
        $cust_sn = Strtoupper($acronym);
	$customerRefNo			= $cust_sn.'/Cali/'.$row['customerRef'];
	$ambientTemperature		= $row['temperature'];
	$relativeHumidity		= $row['humidity'];
	
	$tempArr = $instrumentDetails;
	// instrument details query //
	$qry1 = 'SELECT grndetail.grnDetailId,grndetail.makeModel AS make,grndetail.itemCode AS item_code,
		grndetail.instrumentId AS id_no,grndetail.rangeValue AS range_value,
		grndetail.accuracy,grndetail.grnCondition AS grn_condition,item.itemName AS item_name,
		grndetail.certiRemarks AS certi_remarks, grndetail.cali_location, grndetail.uuc_location,grndetail.ulr_no, staff.staffName as userName, grndetail.sr_no, approved_by.name as approved_by, approved_by.designation
		FROM grndetail 
		LEFT JOIN item ON item.itemId = grndetail.itemId 
		LEFT JOIN approved_by ON approved_by.id = grndetail.approved_by 
                LEFT JOIN staff ON staff.staffId = grndetail.user_id 
		WHERE grndetail.callibrationDate IS NOT NULL AND grndetail.grnId = '.$row['grnId'].' AND grndetail.grnDetailId = "'.$row['grnDetailId'].'"';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        while($row1 = mysql_fetch_assoc($res1)) { 
		array_push($grnDetailIds,$row1['grnDetailId']);
		foreach($tempArr as $key=>$val) {
			if(!in_array_r($row1[$key],$tempArr[$key])) {
				$tempArr[$key][$counter] = $row1[$key];
				$instrumentDetails[$key] .= $row1[$key].' | ';
			}
		}
		$counter++;
	}
	
	foreach($instrumentDetails as $key=>$val) {
		if($val != '')
			$instrumentDetails[$key] = substr($val,0,-3);
	}
	mysql_free_result($res1);
	
	$tempArr = $masterMeters;
	// master meters query //
        $all_least = '';
        $all_uut = '';
        $qry2 = 'SELECT master_json,uuc_json FROM observation_data as grn
		WHERE grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].'  ';
	$res2 = mysql_query($qry2) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $master_data = array();
//        echo "<pre>"; print_r(mysql_fetch_assoc($res1)); exit;
        while($row2 = mysql_fetch_assoc($res2)) {
                array_push($master_data,$row2);
        }
//        echo "<pre>"; print_r($master_data); exit;
        $master_meter_ids = array();
        $uuc_ranges = array();
        $all_least_arr = array();
        $max_count_page = count($master_data) + 1;
        foreach ($master_data as $m_key => $ms_data){ 
           
            $m_data = json_decode(json_encode(json_decode($ms_data['master_json'])), true);
            $u_data = json_decode(json_encode(json_decode($ms_data['uuc_json'])), true);
//             echo "<pre>"; print_r($m_data); exit;
//            $all_uut = $u_data[0]['uut_range'];
//            isset($uuc_data['uut_range_min']) ? $uuc_data['uut_range_min'] : '';
            foreach ($u_data as $uu_data){ 
                if($uu_data['uut_range_min'] != ''){
                    $uuu_name = $uu_data['uut_range_min'] ." to ". $uu_data['uut_range'];
                } else {
                    $uuu_name = $uu_data['uut_range'];
                }
                if(isset($uu_data['leastCount_print']) &&  ($uu_data['leastCount_print'] != '')){
                    $all_least_arr[] = $uu_data['leastCount_print'];
                }
                $uuc_ranges[] = $uuu_name;
            }
            foreach ($m_data as $s_data){ 
                $master_meter_ids[] = $s_data['masterMeterId'];
            }
            
        }
        $uni_uut = array_unique($uuc_ranges);
        $all_uut = implode(",",$uni_uut);
        $all_least_arr1 = array_unique($all_least_arr);
        $all_least = implode(",",$all_least_arr1);
	$qry2 = 'SELECT masterMeterName as "0",masterMeterIdNo as "1",masterMeterMake as "2",masterMeterModelNo as "3",masterMeterSerialNo as "4",masterMeterCertificateNo as "5",
		 DATE_FORMAT(masterMeterExp,"%d/%m/%Y") AS "6"
                 FROM mastermeter 
		 WHERE masterMeterId IN ('.implode(",",$master_meter_ids).')'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res2 = mysql_query($qry2) or die("Error :: Cannot select master meter details1.<hr>".mysql_error());
	$master_m_data = array();
        while($row2 = mysql_fetch_assoc($res2)) { 
            $master_m_data[] = $row2;
	}
//        echo "<pre>"; print_r($master_m_data); exit;
	$qry21 = 'SELECT GROUP_CONCAT(DISTINCT procedureText) as procedureText, GROUP_CONCAT(DISTINCT page_title) as page_title
                 FROM mastermeter 
		 WHERE masterMeterId IN ('.implode(",",$master_meter_ids).')'; 
//	 echo "DEBUG QUERY: " . $qry2;
	$res21 = mysql_query($qry21) or die("Error :: Cannot select master meter details2.<hr>".mysql_error());
	$master_procedureText = '';
	$page_title = '';
        while($row21 = mysql_fetch_assoc($res21)) { 
            $master_procedureText = $row21['procedureText'];
            $page_title = $row21['page_title'];
	}
//        echo "<pre>"; print_r($master_procedureText); exit;
//print '<pre>'; print_r($masterMeters);
	mysql_free_result($res2);
}
mysql_free_result($res);
// fetch data - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// html - start //
ob_start();
?>
<?php 
$qry112 = 'SELECT * FROM setting ';
$res112 = mysql_query($qry112) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
$s_trac = '';
$s_approve = '';
$s_note = '';
$s_all_note = '';
$s_page_1_note = '';
while($row112 = mysql_fetch_assoc($res112)) { 
    if($row112['setting_name'] == 'traceability_to'){
        $s_trac = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'approved_by'){
        $s_approve = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'footer_note_for_page_1'){
        $s_note = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'footer_note_for_all'){
        $s_all_note = $row112['setting_value'];
    }
    if($row112['setting_name'] == 'page_1_remark'){
        $s_page_1_note = $row112['setting_value'];
    }
}
?>
<style>
table {font-family: "Times New Roman"; font-size: 13px;}
tr {line-height: 21px;}
</style>

<div style="border: 1px solid black; border-radius: 10px; padding: 5px;">
    <table width="605">
        <tr valign="top">
            <td width="605"><strong>To,</strong></td>
        </tr>
        <tr valign="top">
            <td width="605" style="font-family: Agency FB;font-size:18px; font-weight:bold;"><?php echo $customerName; ?></td>
        </tr>
        <tr valign="top" >
            <td style="font-size:13px; height:81px;" width="605">
                <?php echo nl2br($customerAddress); ?>
                <?php if (!empty($customerCity)) { ?>
                    <br/><b><?php echo $customerCity; ?></b>
                <?php } ?>
            </td>
        </tr>
    </table>
    <table width="605">
        <tr valign="top" allign="center">
            <td width="605" allign="center" style="text-align: center;"><b>CALIBRATION CERTIFICATE</b></td>
        </tr>
    </table>
    <table width="605">
        <tr valign="top">
            <td width="110">Report No.</td>
            <td width="5">:</td>
            <td width="185"><?php echo $certificate_no; ?></td>
            <td width="115">Date of Calibration</td>
            <td width="5">:</td>
            <td width="185"><?php echo $callibrationDate; ?></td>
        </tr>
        <tr valign="top">
            <td width="110">Customer Ref. No.</td>
            <td width="5">:</td>
            <td width="185"><?php echo $customerRefNo; ?></td>
            <td width="115">GRN No.</td>
            <td width="5">:</td>
            <td width="185"><?php echo $grn_no; ?></td>
        </tr>
    </table>
    <table width="605">
        <tr valign="top">
            <td width="110">Name of Inst.</td>
            <td width="5">:</td>
            <td width="490"><?php echo $instrumentDetails['item_name']; ?></td>
        </tr>
        <tr valign="top">
            <td>Make/Model</td>
            <td>:</td>
            <td><?php echo $instrumentDetails['make']; ?></td>
        </tr>
        <tr valign="top">
            <td>Sr. No./ID No.</td>
            <td>:</td>
            <td><?php echo $instrumentDetails['item_code']; ?>,<?php echo!empty($instrumentDetails['id_no']) ? $instrumentDetails['id_no'] : '---'; ?></td>
        </tr>
        <tr valign="top">
            <td>Instrument ID No.</td>
            <td>:</td>
            <td><?php echo!empty($instrumentDetails['id_no']) ? $instrumentDetails['id_no'] : '---'; ?></td>
        </tr>
    </table>
    <?php
        $qry1 = 'SELECT *,ti.note as table_info_note FROM observation_data as grn
                LEFT JOIN table_info as ti ON ti.table_info_id = grn.table_info_id
		WHERE grn.table_info_id = "15" AND grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].'  ORDER BY grn.table_no ASC ';
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
        $grnobsmaster_data = array();
//        echo "<pre>"; print_r(mysql_fetch_assoc($res1)); exit;
        while($row1 = mysql_fetch_assoc($res1)) {
                array_push($grnobsmaster_data,$row1);
        }
//        echo "<pre>"; print_r($grnobsmaster_data); exit;
        ?>
        
        <?php foreach ($grnobsmaster_data as $main_key => $grn_data){ 
        $master_data = json_decode(json_encode(json_decode($grn_data['master_json'])), true);
        $uuc_data = json_decode(json_encode(json_decode($grn_data['uuc_json'])), true);
        $type_a_type_b_json = json_decode(json_encode(json_decode($grn_data['type_a_type_b_json'])), true);
        unset($grn_data['master_json']);
        unset($grn_data['uuc_json']);
        unset($grn_data['type_a_type_b_json']);
        unset($grn_data['reading_json']);
//        $grn_data[] = $type_a_type_b_json;
        $main_arr = array();
        foreach ($master_data as $key => $master_dat){
            $t_arr = array_merge($master_dat,$uuc_data[$key],$type_a_type_b_json[$key],$grn_data);
            $main_arr[] = $t_arr;
        }
        $grn_data = $main_arr;
//        echo "<pre>"; print_r($grn_data); exit;
        ?>
    <table width="605">
        <tr valign="top">
            <td width="110">Range</td>
            <td width="5">:</td>
            <td width="185"><?php echo $instrumentDetails['range_value']; ?></td>
            <td width="115">CTR</td>
            <td width="5">:</td>
            <td width="185"><?php echo $grn_data[0]['ctr']; ?></td>
        </tr>
        <tr valign="top">
            <td width="110">Class</td>
            <td width="5">:</td>
            <td width="185"><?php echo $grn_data[0]['class']; ?></td>
            <td width="115">Inst. Location</td>
            <td width="5">:</td>
            <td width="185"><?php echo $instrumentDetails['uuc_location']; ?></td>
        </tr>
        <tr valign="top">
            <td width="110">Next Cali. Date</td>
            <td width="5">:</td>
            <td width="185"><?php echo $callibrationDateNext; ?></td>
            <td width="115">&nbsp;</td>
            <td width="5">&nbsp;</td>
            <td width="185">&nbsp;</td>
        </tr>
    </table>
<style>
    tr {line-height: 21px;}
.main_table tr td {border: 1px solid black;}
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 1px solid black;
    
}
.main_table td, .main_table th {
    padding: 1px;
    
}
.main_table{
    padding-bottom: 2px;
    
}
</style>

        <table width="605" align="left" class="main_table"  style="font-size:13px;">
                <?php
                $is_3_phase = 0;
                foreach ($grn_data as $k => $grn_entry){ 
                    if($grn_entry['phase_type_id'] == 2){
                        $is_3_phase = 1;
                        break;
                    }
                }
                $table_title = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(!empty($grn_entry['table_title'])){
                        $table_title = $grn_entry['table_title'];
                        break;
                    }
                }
                $table_title_right = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                        $table_title_right = $grn_entry['table_title_right'];
                        break;
                    }
                }
                $pre_range = '';
                $pre_range_key = '';
                $pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(empty($pre_range)){
                        $pre_range = $grn_entry['mRangeId'];
                        $pre_range_key = $k;
                        $pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span'] = 1;
                        }
                    } else {
                        if($pre_range == $grn_entry['mRangeId']){
                            $pre_range_total = $pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        } else {
                            $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                $t_pre_range = '';
                $t_pre_range_key = '';
                $t_pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if($k == 0){
                        
                        $t_pre_range = $grn_entry['uut_range'];
                        $t_pre_range_key = $k;
                        $t_pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span_test'] = 1;
                        }
                    } else {
                        if($t_pre_range == $grn_entry['uut_range']){
                            $t_pre_range_total = $t_pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        } else {
                            $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            $t_pre_range = $grn_entry['uut_range'];
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                ?>
                <tr>
                    <th align="center" valign="middle">Title:</th>
                    <th colspan="6" align="left" valign="middle">&nbsp; <?php echo $table_title_right; ?></th>
                </tr>
                <tr>
                    <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                    <th rowspan="2" align="center" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UUC Range&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th rowspan="2" align="center" valign="middle">&nbsp;&nbsp;&nbsp;Cos&nbsp;&nbsp;&nbsp;</th>
                    <th align="center" colspan="3" valign="middle">% Error Observed</th>
                </tr>
                <tr>
                    <th align="center" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th align="center" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;YN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th align="center" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                </tr>
                <?php 
                    $master_unit_type1 = '';
                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                            WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    while($row1 = mysql_fetch_assoc($res1)) { 
                        $master_unit_type1 = $row1['range_unit'];
                    }
                    
                    ?>
                <?php 
                    $total_test_meter_val = 0;
                    $total_std_meter_val = 0;
                    $max_ex_unce = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                    $i = $k + 1;
                    
                    $master_hz = '';
                    $master_unit_type = '';
                    $master_range = '';
                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                            WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    while($row1 = mysql_fetch_assoc($res1)) { 
                        $master_hz = $row1['frequency'];
                        $master_unit_type = $row1['range_unit'];
                        $master_range = $row1['rangeValue'];
                    }
                    
                    ?>
                    <tr>
                        <td align="center" valign="middle"><?php echo $i; ?></td>
                        <?php if(isset($grn_entry['row_span'])){ ?>
                        <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; text-align: center; width: 170px;" ><?php echo $master_range.' '.$master_unit_type.' @ '.$master_hz ?></div></td>
                        <?php } ?>
                        <?php 
                            $test_meter_val = $grn_entry['testMeterAverage'];
                            $std_meter_val = $grn_entry['stdMeterAverage'];
                            $test2_meter_val = $grn_entry['testMeter2Average'];
                           $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                           $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                           $decimal_master = 4;
                           $resolution_of_master = $grn_entry['resolution_of_master'];
                           
                           $ex_unce = $grn_entry['expandedUncertinityInPre'];
                           if($max_ex_unce == ''){
                               $max_ex_unce = $ex_unce;
                           } else {
                               if($ex_unce >= $max_ex_unce){
                                   $max_ex_unce = $ex_unce;
                               }
                           }
                           
                           if(!empty($resolution_of_master)){
                               $exp_val = explode('.', $resolution_of_master);
                               if(isset($exp_val[1])){
                                   $decimal_master = strlen($exp_val[1]);  
                               } else {
                                 $decimal_master = 0;  
                               }
                           } else {
                               $decimal_master = 4;
                           }
                           $decimal_least = 4;
                           $resolution_of_least = $grn_entry['leastCount'];
                           if(!empty($resolution_of_least)){
                               $exp_least = explode('.', $resolution_of_least);
                               if(isset($exp_least[1])){
                                   $decimal_least = strlen($exp_least[1]);  
                               } else {
                                 $decimal_least = 0;  
                               }
                           } else {
                               $decimal_least = 4;
                           }
                        ?>
                        <td align="center" valign="middle"><?php echo isset($grn_entry['cos']) ? $grn_entry['cos'] : ''; ?></td>
                        <td align="center" valign="middle"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                        <td align="center" valign="middle"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                        <td align="center" valign="middle"><?php echo number_format((float)$test2_meter_val, $decimal_master, '.', ''); ?></td>
                    </tr>
                <?php } ?>
                    
            </table>
	<?php } ?>
	<?php
	?>
<table >	
    <tr valign="top">
        <td width="605"><strong>DETAILS OF REFERENCE STANDARD & MAJOR INSTRUMENTS USED</strong></td>
    </tr>
    <br/>
    <tr valign="top">
        <td width="605">
            <?php
            $max_rows = count($master_m_data);
            $name_arr = array();
            $name_arr[] = 'Name';
            $name_arr[] = 'I.D. No.';
            $name_arr[] = 'Make';
            $name_arr[] = 'Model No.';
            $name_arr[] = 'Serial no.';
            $name_arr[] = 'Certificate No.';
            $name_arr[] = 'Calibration Valid Up to';
            if ($max_rows <= 2) {
                $font_s = 'font-size:13px;';
            } else {
                $font_s = 'font-size:10px;';
            }
//                    echo "<pre>"; print_r($master_m_data); exit;
            ?>
            <table cellpadding="0" cellspacing="0" border="1" style="<?php echo $font_s; ?>" width="100%">
                <?php for ($x = 0; $x < 7; $x++) { ?>
                    <tr>
                        <td align="left" style="padding: 2px;" valign="center"><b><?php echo $name_arr[$x]; ?></b></td>
                        <?php foreach ($master_m_data as $master_m) { ?>
                            <td align="left" style="padding: 2px;" valign="center"><?php echo $master_m[$x]; ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                <tr>
                    <td align="left" style="padding: 2px;" valign="center"><b>Traceability To </b></td>
                    <td align="left" style="padding: 2px;" valign="center" colspan="<?php echo $max_rows; ?>"><?php echo $s_trac; ?></td>
                </tr>
            </table>
        </td>
    </tr>

</table>
<table width="605">	
    <tr valign="bottom">
    <hr />
    <td style="height: 5px;" width="605" style="font-size: 20px;"></td>
    </tr>
    <tr valign="bottom">
        <td width="605" style="font-size: 20px;"><img src="./images/for-krishna.png" /></td>
    </tr>
    <tr valign="top">
        <td >&nbsp;</td>
    </tr>
    <tr valign="top">
        <td >&nbsp;</td>
    </tr>
    <tr valign="top">
        <td >&nbsp;</td>
    </tr>
    <tr valign="top">
        <td >CALIBRATED BY: (<?php echo $instrumentDetails['userName']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; APPROVED BY: (<?php echo $instrumentDetails['approved_by'] . ', ' . $instrumentDetails['designation']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; SEAL :</td>
    </tr>
    <tr valign="top">
        <td width="605"><strong>Note&nbsp;:</strong></td>
    </tr>
    <tr valign="top">
        <td style="font-size: 12px;"><?php echo $s_note; ?></td>
    </tr>
</table>

</div>

<?php

// html - end //
$content = ob_get_clean();

// convert in PDF
require_once 'html2pdf/html2pdf.class.php';
try {
//	$html2pdf = new HTML2PDF('P', 'A4', 'fr');
	$html2pdf = new HTML2PDF('P', 'A4', 'fr',true,'UTF-8',array(30, 43, 10, 14));
	// $html2pdf->setModeDebug();
	$html2pdf->setDefaultFont('Times');
        $html2pdf->AddFont('dejavusans');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output('example.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>