<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(OBS_MODULE_ID,"inst. dues"))){
	$msg		= "";
	$grnList	= array();
	$fromDate	= 0;
	$toDate		= 0;
	
	/////////////////  grnList date View:Start
	if(isset($_REQUEST['fromDateYear']))
		$fromDate  = $_REQUEST['fromDateYear']."-".$_REQUEST['fromDateMonth']."-".$_REQUEST['fromDateDay'];
	else
		$fromDate  = date("Y-m-01");
	
	if(isset($_REQUEST['toDateYear']))
		$toDate    = $_REQUEST['toDateYear']."-".$_REQUEST['toDateMonth']."-".$_REQUEST['toDateDay'];
	else
		$toDate    = date("Y-m-t");
		
	if(isset($_POST['dispatch']) && $_POST['dispatch'] != 0){
		if($_POST['dispatch'] == 1){
		  $dispatchQry = " AND grndetail.dispatch = 1 ";
		  $dispatchValue = 1;
		}else{
		  $dispatchQry = " AND grndetail.dispatch = 0 ";
		  $dispatchValue = 2;
		}
	}else{
		$dispatchQry = NULL;
		$dispatchValue = 0;
	}
	/////////////////  grnList date View:Stop
	
	$query 		= "SELECT gm.grnId, gm.grnPrefix, gm.grnNo, gm.customerId, 
		gd.grnDetailId, gd.itemCode, gd.itemId, gd.parameterId, DATE_FORMAT(gd.nextYearDate, '%d/%m/%Y') AS expDate,  DATEDIFF(gd.nextYearDate, NOW()) AS remainingDays, 
		i.itemName, p.parameterName, 
		c.custName 
		FROM grnmaster gm 
		LEFT JOIN grndetail gd ON gd.grnId = gm.grnId 
		LEFT JOIN item i ON i.itemId = gd.itemId 
		LEFT JOIN parameterentry p ON p.parameterId = gd.ParameterId 
		LEFT JOIN customer c ON c.customerId = gm.customerId 
		WHERE gd.nextYearDate BETWEEN '".$fromDate."' AND '".$toDate."' 
		ORDER BY remainingDays ASC";
	$result 	= mysql_query($query) or die("Cannot select instruments detail.<hr>".mysql_error());
	
	if( mysql_num_rows($result) > 0 ) { 
		$i = 0;
		while( $arow = mysql_fetch_assoc($result) ) {
			array_push($grnList, $arow);
		}
	} else { 
		$msg = '<tr><td align="center" colspan="22"> <h1><font color="red"><b>Record Not Found...!</b></h1></font></td></tr>';
	}
	mysql_free_result($result);

	include("./bottom.php");
	$smarty->assign("msg",$msg);
	$smarty->assign("grnList",$grnList);
	$smarty->assign("fromDate",$fromDate);
	$smarty->assign("toDate",$toDate);  
	$smarty->display("instrumentsDues.tpl");
} else {
  header("Location:index.php");
}  
}

?>