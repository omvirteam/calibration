<table width="605" align="left" class="main_table"  style="font-size:13px;">
                <?php
                $is_3_phase = 0;
                foreach ($grn_data as $k => $grn_entry){ 
                    if($grn_entry['phase_type_id'] == 2){
                        $is_3_phase = 1;
                        break;
                    }
                }
                $table_title = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(!empty($grn_entry['table_title'])){
                        $table_title = $grn_entry['table_title'];
                        break;
                    }
                }
                $table_title_right = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['table_title_right']) && !empty($grn_entry['table_title_right'])){
                        $table_title_right = $grn_entry['table_title_right'];
                        break;
                    }
                }
                $start_reading_of_kwh = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['start_reading_of_kwh']) && !empty($grn_entry['start_reading_of_kwh'])){
                        $start_reading_of_kwh = $grn_entry['start_reading_of_kwh'];
                        break;
                    }
                }
                $end_reading_of_kwh = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(isset($grn_entry['end_reading_of_kwh']) && !empty($grn_entry['end_reading_of_kwh'])){
                        $end_reading_of_kwh = $grn_entry['end_reading_of_kwh'];
                        break;
                    }
                }
                $pre_range = '';
                $pre_range_key = '';
                $pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if(empty($pre_range)){
                        $pre_range = $grn_entry['mRangeId'];
                        $pre_range_key = $k;
                        $pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span'] = 1;
                        }
                    } else {
                        if($pre_range == $grn_entry['mRangeId']){
                            $pre_range_total = $pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        } else {
                            $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                $t_pre_range = '';
                $t_pre_range_key = '';
                $t_pre_range_total = '';
                foreach ($grn_data as $k => $grn_entry){ 
                    if($k == 0){
                        
                        $t_pre_range = $grn_entry['uut_range'];
                        $t_pre_range_key = $k;
                        $t_pre_range_total = 1;
                        if(isset($grn_data[$k+1])){} else {
                            $grn_data[$k]['row_span_test'] = 1;
                        }
                    } else {
                        if($t_pre_range == $grn_entry['uut_range']){
                            $t_pre_range_total = $t_pre_range_total + 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        } else {
                            $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            $t_pre_range = $grn_entry['uut_range'];
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                            }
                        }
                    }
                }
//                echo "<pre>"; print_r($grn_data); exit; 
                $col_span1 = '5';
                $col_span2 = '1';
                if(!empty($view_uncertainity)){
                    $col_span1 = $col_span1 + 1;
                    $col_span2 = $col_span2 + 1;
                }
                ?>
                <tr>
                    <th align="center" valign="middle">Title:</th>
                    <th  colspan="<?= $col_span1; ?>" align="left" valign="middle" style="font-family:dejavusansb; font-size: 11px;">&nbsp; <?php echo $table_title_right; ?></th>
                </tr>
                <?php if(!empty($start_reading_of_kwh)){ ?>
                    <tr>
                        <th align="center" valign="middle"></th>
                        <th align="center" valign="middle"></th>
                        <th align="center" valign="middle"></th>
                        <th align="center" valign="middle" colspan="2">Start reading of kwh</th>
                        <th  colspan="<?= $col_span2; ?>" align="center" valign="middle" style="font-family:dejavusansb; font-size: 11px;">&nbsp; <?php echo $start_reading_of_kwh; ?></th>
                    </tr>
                <?php } ?>
                <tr>
                    <th align="center" valign="middle">Sr. No.</th>
                    <th align="center" valign="middle">AC Voltage</th>
                    <th align="center" valign="middle">AC Current</th>
                    <th align="center" valign="middle" width="80px">Power Factor</th>
                    <th align="center" valign="middle">% Error</th>
                    <th align="center" valign="middle">Expanded Uncertainty in%(±)</th>
                </tr>
                <?php 
                    $total_test_meter_val = 0;
                    $total_std_meter_val = 0;
                    foreach ($grn_data as $k => $grn_entry){ 
//                    echo "<pre>"; print_r($reading_json); exit; 
                    $i = $k + 1;
                    
                    $master_hz = '';
                    $master_unit_type = '';
                    $master_range = '';
                    $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                            WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    while($row1 = mysql_fetch_assoc($res1)) { 
                        $master_hz = $row1['frequency'];
                        $master_unit_type = $row1['range_unit'];
                        $master_range = $row1['range_disp'];
                    }
                    
                    ?>
                    <tr>
                        <td align="center" valign="middle"><?php echo $i; ?></td>
                        <?php 
                            $test_meter_val = $grn_entry['testMeterAverage'];
                            $std_meter_val = $grn_entry['stdMeterAverage'];
                           $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                           $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                           $decimal_master = 0;
                           $resolution_of_master = $grn_entry['resolution_of_master'];
                           
                           if(!empty($resolution_of_master)){
                               $exp_val = explode('.', $resolution_of_master);
                               if(isset($exp_val[1])){
                                   $decimal_master = strlen($exp_val[1]);  
                               } else {
                                 $decimal_master = 0;  
                               }
                           } else {
                               $decimal_master = 0;
                           }
                           $decimal_least = 0;
                           $resolution_of_least = $grn_entry['leastCount'];
                           if(!empty($resolution_of_least)){
                               $exp_least = explode('.', $resolution_of_least);
                               if(isset($exp_least[1])){
                                   $decimal_least = strlen($exp_least[1]);  
                               } else {
                                 $decimal_least = 0;  
                               }
                           } else {
                               $decimal_least = 0;
                           }
                           $decimal_least_as_reading = 0;
                           $resolution_of_least_e = $reading_json[$k]['stdMeter'][0];
                           if(!empty($resolution_of_least_e)){
                               $exp_least_e = explode('.', $resolution_of_least_e);
                               if(isset($exp_least_e[1])){
                                   $decimal_least_as_reading = strlen($exp_least_e[1]);  
                               } else {
                                 $decimal_least_as_reading = 0;  
                               }
                           } else {
                               $decimal_least_as_reading = 0;
                           }
                        ?>
                        <td valign="middle" align="center"><?php echo $grn_entry['ac_voltage']; ?></td>
                        <td valign="middle" align="center"><?php echo $grn_entry['ac_current']; ?></td>
                        <td valign="middle" align="center"><?php echo $grn_entry['power_factor']; ?></td>
                        <td valign="middle" align="center"><?php echo number_format((float)$std_meter_val, $decimal_least_as_reading, '.', ''); ?></td>
                        <td valign="middle" align="center"><?php echo number_format((float)$grn_entry['exp_uncer_per_ue'], 2, '.', ''); ?></td>
                    </tr>
                <?php } ?>
                    <?php if(!empty($end_reading_of_kwh)){ ?>
                    <tr>
                        <th align="center" valign="middle"></th>
                        <th align="center" valign="middle"></th>
                        <th align="center" valign="middle"></th>
                        <th align="center" valign="middle" colspan="2">End reading of kwh</th>
                        <th  colspan="<?= $col_span2; ?>" align="center" valign="middle" style="font-family:dejavusansb; font-size: 11px;">&nbsp; <?php echo $end_reading_of_kwh; ?></th>
                    </tr>
                <?php } ?>
                    
            </table>