<?php
require('./fpdf.php');
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = "";
  $grnDetailId         = "";
  $grnPrefix           = "";
  $grnNo               = "";
  $grnDate             = "";
  $poNo                = "";
  $poDate              = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();
  // Pdf Create :Start
  $pdf = new FPDF('P','mm','A4');
  $pdf->AliasNbPages();
  $pdf->AddPage();
    
  //SELECT OF GRN MASTER :START
  $selectGrnMaster = "SELECT grnmaster.grnId,grnmaster.grnNo,grnmaster.infoSheetNo,grnmaster.customerId,customer.customerId,customer.custName,
                             customer.address,customer.city,grnDetail.grnId,grnDetail.masterMeterId,DATE_FORMAT(grndetail.callibrationDate,'%d/%m/%Y') AS callibrationDate,
                             DATE_FORMAT(grndetail.nextYearDate,'%d/%m/%Y') AS nextYearDate,grnDetail.grnDetailId,mastermeter.masterMeterId,
                             mastermeter.masterMeterName,mastermeter.masterMeterIdNo,mastermeter.masterMeterMake,mastermeter.masterMeterSerialNo,mastermetersub.masterMeterSubId,
                             mastermetersub.masterMeterId,mastermetersubsub.masterMeterSubId,mastermetersubsub.masterMeterSubSubId,
                             mastermetersubsub.certiAccuracy,grnDetail.accuracy,grnDetail.itemId,grnDetail.rangeValue,grndetail.makeModel,grndetail.instrumentId,grnDetail.leastCount,
                             grndetail.accuracy,grndetail.temperature,grndetail.humidity,custReqDate,item.itemId,item.itemName,customer.custCode,mastermeter.procedureText
                        FROM grnmaster
                        JOIN customer
                        JOIN grndetail
                        JOIN mastermeter
                        JOIN mastermetersub
                        JOIN mastermetersubsub
                        JOIN item
                       WHERE grnmaster.grnId = ".$_GET['grnId']."
                         AND grndetail.grnDetailId = ".$_GET['grnDetailId']."
                         AND grnmaster.customerId = customer.customerId
                         AND grnmaster.grnId = grndetail.grnId
                         AND mastermeter.masterMeterId = mastermetersub.masterMeterId
                         AND mastermetersub.masterMeterSubId = mastermetersub.masterMeterSubId
                         AND grndetail.itemId = item.itemId";
  $selectGrnMasterRes = mysql_query($selectGrnMaster);
  if($selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes))
  {
    if(isset($selectGrnMasterRow['custCode']))
    {
      $custCodeLength = strlen(($selectGrnMasterRow['custCode']));
      if($custCodeLength == "1")
        $custCodeLengthId = '00'.$selectGrnMasterRow['custCode'];
      else if($custCodeLength == "2")
        $custCodeLengthId = '0'.$selectGrnMasterRow['custCode'];
      else if($custCodeLength == "3")
        $custCodeLengthId = $selectGrnMasterRow['custCode'];
      else if($custCodeLength == "4")
        $custCodeLengthId = '000'; 
    }
    if(isset($selectGrnMasterRow['grnDetailId']))
    {
      $grnDetailLength = strlen(($selectGrnMasterRow['grnDetailId']));
      if($grnDetailLength == "1")
        $grnDetailLengthId = '00'.$selectGrnMasterRow['grnDetailId'];
      else if($grnDetailLength == "2")
        $grnDetailLengthId = '0'.$selectGrnMasterRow['grnDetailId'];
      else if($grnDetailLength == "3")
        $grnDetailLengthId = $selectGrnMasterRow['grnDetailId'];
      else if($grnDetailLength == "0")
        $grnDetailLengthId = '000'; 
    }
    if(isset($selectGrnMasterRow['callibrationDate']))
    {
      if(substr($selectGrnMasterRow['callibrationDate'],6,4));
      {
        $year = substr($selectGrnMasterRow['callibrationDate'],6,4);
        if($year == 2010)
          $yearWord = "A";
        else if ($year == 2011)
          $yearWord = "B";
        else if($year == 2012)
          $yearWord = "C";
        else if($year == 2013)
          $yearWord = "D";
        else if($year == 2014)
          $yearWord = "E";
      } 
      if(substr($selectGrnMasterRow['callibrationDate'],3,2))
      {
        $month = substr($selectGrnMasterRow['callibrationDate'],3,2);
        if($month == "01")
          $monthWord = "A";
        else if($month == "02")
          $monthWord = "B";
        else if($month == "03")
          $monthWord = "C";
        else if($month == "04")
          $monthWord = "D";
        else if($month == "05")
          $monthWord = "E";
         else if($month == "06")
          $monthWord = "F";
         else if($month == "07")
          $monthWord = "G";
         else if($month == "08")
          $monthWord = "H";
         else if($month == "09")
          $monthWord = "I";
         else if($month == "10")
          $monthWord = "J";
         else if($month == "11")
          $monthWord = "K";
         else if($month == "12")
          $monthWord = "L";
      } 
    }
  }  
  pageHeader();
  pageHeaderCustomerDetail();
  footer();
  
  $selectGrnDetailQuery = "SELECT grnDetailId,grndetail.parameterId,parameterentry.parameterId,parameterentry.parameterName,masterMeterId
                             FROM grndetail
                             JOIN parameterentry
                            WHERE (grnDetailId       = ".$_GET['grnDetailId']."
                                   OR refGrnDetailId = ".$_GET['grnDetailId'].")
                              AND grndetail.parameterId = parameterentry.parameterId";
  $selectGrnDetailQueryRes = mysql_query($selectGrnDetailQuery);
  $grnDetailCount = 0;
  $masterMeterId = 1;
  $yPosition = 150;
  $leftMasterMeterId    = 0;
  while($grnDetailrow = mysql_fetch_array($selectGrnDetailQueryRes))
  {
    $selectDetailMasterQuery = "SELECT grndetail.grnId,grndetail.masterMeterId,grndetail.grnDetailId,mastermeter.masterMeterId,
                                       mastermeter.masterMeterName,mastermeter.masterMeterIdNo,mastermeter.masterMeterMake,mastermeter.masterMeterModelNo,mastermeter.masterMeterSerialNo,mastermeter.masterMeterCertificateNo,
                                       DATE_FORMAT(mastermeter.masterMeterExp,'%d/%m/%Y') as masterMeterExp,mastermeter.masterMeterTraceabilityTo,mastermetersub.masterMeterSubId,mastermetersub.masterMeterId,mastermetersubsub.masterMeterSubId,mastermetersubsub.masterMeterSubSubId,
                                       mastermetersubsub.certiAccuracy,grndetail.itemId,grndetail.rangeValue,grndetail.makeModel,grndetail.instrumentId,grndetail.leastCount,
                                       grndetail.accuracy,grndetail.userName
                                  FROM grndetail
                                  JOIN mastermeter
                                  JOIN mastermetersub
                                  JOIN mastermetersubsub
                                 WHERE grndetail.grnDetailId = ".$grnDetailrow['grnDetailId']."
                                   AND grndetail.masterMeterId = mastermeter.masterMeterId
                                   AND mastermeter.masterMeterId = mastermetersub.masterMeterId
                                   AND mastermetersub.masterMeterSubId = mastermetersub.masterMeterSubId";
    $selectDetailMasterQueryRes = mysql_query($selectDetailMasterQuery);
    $masterMeterDetailRow = mysql_fetch_array($selectDetailMasterQueryRes);
    ///
//    if($grnDetailCount % 2 == 0)
//    {
//      if($grnDetailCount > 0 && ($grnDetailCount % 2 == 0))
//      {
//        $pdf->AddPage();
//        pageHeader();
//        pageHeaderCustomerDetail();
//      }
//      $pdf->AddPage();
//      
//    }
//    else
//      rightTableHeader($yPositionRightHeader);

    $a = 0;
    $total = 0;
    $selectDetailQuery = "SELECT grnObservationId,grnId,grnDetailId,stdMeterAverage,testMeterAverage,percentageRdg
                            FROM grnobservation
                           WHERE grnDetailId = ".$grnDetailrow['grnDetailId'];
    $selectDetailQueryRes = mysql_Query($selectDetailQuery);
    while($detailRow = mysql_fetch_array($selectDetailQueryRes))
    {
      $total = $detailRow['percentageRdg'];
      // $refGrnDetailId = $detailRow['refGrnDetailId'];
      if(($a % $cfgNablObservPerPage) == 0)
      {
        $yPosition = 211;
        if($a > 0)
        {
          $pdf->AddPage();
          pageHeader();
        }
      }
         
      if($grnDetailCount % 2 == 0)
      {
        $leftMasterMeterId = $grnDetailrow['masterMeterId'];
        masterMeterDetailLeft();
      }
      else
      {
        if($grnDetailrow['masterMeterId'] != $leftMasterMeterId)
        masterMeterDetailRight();
      }
      $yPosition += 5;
      $a++;
    }
    $grnDetailCount++;
  }
////////////////////////////////////////////////////////////
  
  $pdf->AddPage();
  pageHeader();
  $selectGrnDetailQuery = "SELECT grnDetailId,grndetail.parameterId,parameterentry.parameterId,parameterentry.parameterName,masterMeterId
                             FROM grndetail
                             JOIN parameterentry
                            WHERE (grnDetailId       = ".$_GET['grnDetailId']."
                                   OR refGrnDetailId = ".$_GET['grnDetailId'].")
                              AND grndetail.parameterId = parameterentry.parameterId";
  $selectGrnDetailQueryRes = mysql_query($selectGrnDetailQuery);
  $grnDetailCount = 0;
  $masterMeterId = 1;
  $yPosition = 150;
  $leftMasterMeterId    = 0;
  while($grnDetailrow = mysql_fetch_array($selectGrnDetailQueryRes))
  {
    $selectDetailMasterQuery = "SELECT grndetail.grnId,grndetail.masterMeterId,grndetail.grnDetailId,mastermeter.masterMeterId,
                                       mastermeter.masterMeterName,mastermeter.masterMeterIdNo,mastermeter.masterMeterMake,mastermeter.masterMeterModelNo,mastermeter.masterMeterSerialNo,mastermeter.masterMeterCertificateNo,
                                       DATE_FORMAT(mastermeter.masterMeterExp,'%d/%m/%Y') as masterMeterExp,mastermeter.masterMeterTraceabilityTo,mastermetersub.masterMeterSubId,mastermetersub.masterMeterId,mastermetersubsub.masterMeterSubId,mastermetersubsub.masterMeterSubSubId,
                                       mastermetersubsub.certiAccuracy,grndetail.itemId,grndetail.rangeValue,grndetail.makeModel,grndetail.instrumentId,grndetail.leastCount,
                                       grndetail.accuracy,grndetail.userName
                                  FROM grndetail
                                  JOIN mastermeter
                                  JOIN mastermetersub
                                  JOIN mastermetersubsub
                                 WHERE grndetail.grnDetailId = ".$grnDetailrow['grnDetailId']."
                                   AND grndetail.masterMeterId = mastermeter.masterMeterId
                                   AND mastermeter.masterMeterId = mastermetersub.masterMeterId
                                   AND mastermetersub.masterMeterSubId = mastermetersub.masterMeterSubId";
    $selectDetailMasterQueryRes = mysql_query($selectDetailMasterQuery);
    $masterMeterDetailRow = mysql_fetch_array($selectDetailMasterQueryRes);
    $a = 0;
    $total = 0;
    $Errorunits = 0;
    $selectDetailQuery = "SELECT grnObservationId,grnId,grnDetailId,stdMeterAverage,testMeterAverage,percentageRdg,masterMeterReading,expandedUncertinity
                            FROM grnobservation
                           WHERE grnDetailId = ".$grnDetailrow['grnDetailId'];
    $selectDetailQueryRes = mysql_Query($selectDetailQuery);
    $yPositionHeader = 80;
    $yPositionRightHeader = 130;
     $yPosition = 90;
      $yPositionRight = 140;
    while($detailRow = mysql_fetch_array($selectDetailQueryRes))
    {
     echo $total       = $detailRow['percentageRdg'];
      $Errorunits  = $detailRow['stdMeterAverage'] - $detailRow['masterMeterReading'];
      $ErrorRdg    = 100*$Errorunits/($detailRow['stdMeterAverage']);
      // $refGrnDetailId = $detailRow['refGrnDetailId'];
      if(($a % $cfgNablObservPerPage) == 0)
      {
        if($a > 0)
        {
          $pdf->AddPage();
          pageHeader();
          $pdf->SetXY(80,50);
        }
      }
      if($grnDetailCount % 2 == 0)
      {
        $leftMasterMeterId = $grnDetailrow['masterMeterId'];
        leftTableHeader($yPositionHeader);
        leftTableData($yPosition,$a+1,$total);
      }
      else
      {
        rightTableHeader($yPositionRightHeader);
        rightTableData($yPositionRight,$a+1,$total);
      }
      $yPosition += 5;
      $yPositionRight += 5;
      $a++;
    }
    $grnDetailCount++;
  }
////////////////////////////////////////////////////////////
  $pdf->output();
  include("./bottom.php");
}

// Header Part Of grn Print pdf:Start 
function pageHeader()
{
  global $pdf;
  global $selectGrnMasterRow;
  global $yearWord;
  global $monthWord;
  global $grnDetailLengthId;
  global $custCodeLengthId;
  $pdf->SetFont('Arial','',10);
  // Header part Of nabl Certificate :Start 
  $pdf->SetXY(80,23);
  $pdf->Write(5,' CALIBRATION CERTIFICATE');
  $pdf->SetXY(81,23);
  $pdf->Write(5,''.('________________________'));
  $pdf->SetXY(25,30);
  $pdf->Write(5,'CERTIFICATE NO.   :  KN'.$custCodeLengthId.''.$yearWord.''.$monthWord.''.$grnDetailLengthId);
  $pdf->SetXY(25,35);
  $pdf->Write(5,'NO OF SHEETS.      :');
  $pdf->SetXY(61,35);
  $pdf->Write(5,'1 Of 2');
  $pdf->SetXY(25,40);
  $pdf->Write(5,'GRN No                    :  N'.($selectGrnMasterRow['grnNo']));
  $pdf->SetXY(110,30);
  $pdf->Write(5,'DATE OF CALLIBRATION                 :  '.($selectGrnMasterRow['callibrationDate']));
  $pdf->SetXY(110,35);
  $pdf->Write(5,'RECOMMONDED CALLIBRATION.  :  '.($selectGrnMasterRow['nextYearDate']));
  $pdf->SetXY(15,36);
  $pdf->Write(5,''.('_______________________________________________________________________________________________'));
  // Header part Of nabl Certificate :End
}

function pageHeaderCustomerDetail()
{
  global $pdf;
  global $selectGrnMasterRow;
  // Deail part Of nabl Certificate :Start
  $pdf->SetXY(25,50);
  $pdf->Write(5,'1.CUSTOMERS NAME & ADDRESS   ');
  $pdf->SetXY(95,50);
  $pdf->Write(5,':');
  $pdf->SetXY(100 ,50);
  $pdf->Write(5,''.($selectGrnMasterRow['custName']));
  $pdf->SetXY(100,55);
  $pdf->Write(5,''.($selectGrnMasterRow['address']));
  $pdf->SetXY(25,62);
  $pdf->Write(5,'2.CUSTOMERS REFERANCE NO.                    KI/cali/NABL ');
  $pdf->SetXY(95,62);
  $pdf->Write(5,':');
  $pdf->SetXY(25,68);
  $pdf->Write(5,'3.INSTRUMENT RECEIVED ON');
  $pdf->SetXY(95,68);
  $pdf->Write(5,':');
  $pdf->SetXY(25,73);
  $pdf->Write(5,'4.DESCRIPTION OF INSTRUMENT'); 
  $pdf->SetXY(28,80);
  $pdf->Write(5,'Name');
  $pdf->SetXY(100,80);
  $pdf->Write(5,''.($selectGrnMasterRow['itemName']));
  $pdf->SetXY(95,80);
  $pdf->Write(5,':');
  $pdf->SetXY(28,85);
  $pdf->Write(5,'Make/Model.');
  $pdf->SetXY(100,85);
  $pdf->Write(5,''.($selectGrnMasterRow['makeModel']));
  $pdf->SetXY(95,85);
  $pdf->Write(5,':');
  $pdf->SetXY(28,90);
  $pdf->Write(5,'Sr No.');
  $pdf->SetXY(100,90);
  $pdf->Write(5,''.($selectGrnMasterRow['masterMeterSerialNo']));
  $pdf->SetXY(95,90);
  $pdf->Write(5,':');
  $pdf->SetXY(28,95);
  $pdf->Write(5,'Instrument. ID. No.');
  $pdf->SetXY(100,95);
  $pdf->Write(5,''.($selectGrnMasterRow['instrumentId']));
  $pdf->SetXY(95,95);
  $pdf->Write(5,':');
  $pdf->SetXY(95,100);
  $pdf->Write(5,':');
  $pdf->SetXY(28,100);
  $pdf->Write(5,'Range.');
  $pdf->SetXY(126,100);
  $pdf->Write(5,''.($selectGrnMasterRow['rangeValue']));
  $pdf->SetXY(100,100);
  $pdf->Write(5,'AC Voltage : 0 -        V');
  $pdf->SetXY(95,105);
  $pdf->Write(5,':');
  $pdf->SetXY(28,105);
  $pdf->Write(5,'Resolution.');
  $pdf->SetXY(100,105);
  $pdf->Write(5,'AC Voltage :       V');
  $pdf->SetXY(120,105);
  $pdf->Write(5,' '.($selectGrnMasterRow['rangeValue']));
  $pdf->SetXY(95,110);
  $pdf->Write(5,':');
  $pdf->SetXY(28,110);
  $pdf->Write(5,'Accuracy.');
  $pdf->SetXY(100,110);
  $pdf->Write(5,'+/- '.($selectGrnMasterRow['accuracy']));
  $pdf->SetXY(113,110);
  $pdf->Write(5,'%');
  $pdf->SetXY(95,120);
  $pdf->Write(5,':');
  $pdf->SetXY(25,120);
  $pdf->Write(5,'5.AMBIENT TEMPERATURE');
  $pdf->SetXY(102,120);
  $pdf->Write(5,''.($selectGrnMasterRow['temperature']));
  $pdf->SetXY(100,120);
  $pdf->Write(5,'(            )*C');
  $pdf->SetXY(95,110);
  $pdf->Write(5,':');
  $pdf->SetXY(28,125);
  $pdf->Write(5,'RELATIVE HUMIDITY');
  $pdf->SetXY(100,125);
  $pdf->Write(5,''.($selectGrnMasterRow['humidity']));
  $pdf->SetXY(110,125);
  $pdf->Write(5,'%');
  $pdf->SetXY(95,125);
  $pdf->Write(5,':');
}
// Header Part Of grn Print pdf:End

function leftTableHeader($yPositionHeader)
{
  global $pdf;
  global $grnDetailrow;
  $pdf->SetXY(20,80);
  $pdf->cell(12,10,'SR No','1', '0', 'C');
  $pdf->SetXY(32,$yPositionHeader);
  $pdf->cell(50,05,'Callibration Standard','1', '0', 'C');
  $pdf->SetXY(32,85);
  $pdf->cell(25,05,'Range','1', '0', 'C');
  $pdf->SetXY(57,85);
  $pdf->cell(25,05,'Reading','1', '0', 'C');
  /////
  $pdf->SetXY(82,$yPositionHeader);
  $pdf->cell(50,05,'Unit Under Test','1', '0', 'C');
  $pdf->SetXY(82,85);
  $pdf->cell(25,05,'Range','1', '0', 'C');
  $pdf->SetXY(107,85);
  $pdf->cell(25,05,'Reading','1', '0', 'C');
  /////
  /////
  $pdf->SetXY(132,$yPositionHeader);
  $pdf->cell(50,05,'Error','1', '0', 'C');
  $pdf->SetXY(132,85);
  $pdf->cell(25,05,'Units','1', '0', 'C');
  $pdf->SetXY(157,85);
  $pdf->cell(25,05,'% of Rdg.','1', '0', 'C');
  /////
  $pdf->SetXY(182,80);
  $pdf->cell(20,10,'%Expanded.','1', '0', 'C');
  /////
}

function rightTableHeader($yPositionRightHeader)
{
  global $pdf;
  global $grnDetailrow;
  $pdf->SetXY(20,130);
  $pdf->cell(12,10,'SR No','1', '0', 'C');
  $pdf->SetXY(32,$yPositionRightHeader);
  $pdf->cell(50,05,'Callibration Standard','1', '0', 'C');
  $pdf->SetXY(32,135);
  $pdf->cell(25,05,'Range','1', '0', 'C');
  $pdf->SetXY(57,135);
  $pdf->cell(25,05,'Reading','1', '0', 'C');
  /////
  $pdf->SetXY(82,$yPositionRightHeader);
  $pdf->cell(50,05,'Unit Under Test','1', '0', 'C');
  $pdf->SetXY(82,135);
  $pdf->cell(25,05,'Range','1', '0', 'C');
  $pdf->SetXY(107,135);
  $pdf->cell(25,05,'Reading','1', '0', 'C');
  /////
  /////
  $pdf->SetXY(132,$yPositionRightHeader);
  $pdf->cell(50,05,'Error','1', '0', 'C');
  $pdf->SetXY(132,135);
  $pdf->cell(25,05,'Units','1', '0', 'C');
  $pdf->SetXY(157,135);
  $pdf->cell(25,05,'% of Rdg.','1', '0', 'C');
  /////
  $pdf->SetXY(182,130);
  $pdf->cell(20,10,'%Expanded.','1', '0', 'C');
  /////
}
//$grnDetailrow['parameterName']
function leftTableData($yPosition,$srNo,$total)
{
  global $pdf;
  global $detailRow;
  global $grnDetailrow;
  global $Errorunits;
  global $ErrorRdg;
  $pdf->SetXY(25,60);
  $pdf->Write(5,'8.OBSERVATION');
  $pdf->SetXY(20,$yPosition);
  $pdf->cell(12,05,$srNo,'1', '0', 'C');
  $pdf->SetXY(32,$yPosition);
  $pdf->cell(25,05,$grnDetailrow['parameterName'],'1', '0', 'C');
  $pdf->SetXY(57,$yPosition);
  $pdf->cell(25,05,$detailRow['masterMeterReading'],'1', '0', 'C');
  $pdf->SetXY(82,$yPosition);
  $pdf->cell(25,05,'600V','1', '0', 'C');
  $pdf->SetXY(107,$yPosition);
  $pdf->cell(25,05,$detailRow['stdMeterAverage'],'1', '0', 'C');
  $pdf->SetXY(132,$yPosition);
  $pdf->cell(25,05,$Errorunits,'1', '0', 'C');
  $pdf->SetXY(157,$yPosition);
  $pdf->cell(25,05,round($ErrorRdg,4),'1', '0', 'C');
  $pdf->SetXY(182,$yPosition);
  $pdf->cell(20,05,round($detailRow['expandedUncertinity'],4),'1', '0', 'C');
  $pdf->SetXY(40,190);
  //  $pdf->Write(5,'NOTE');
  //  $pdf->SetXY(40,195);
  //  $pdf->Write(5,'1. Measurement Uncertinity is = 0.0676% For Ac Volt');
  //  $pdf->SetXY(40,200);
  //  $pdf->Write(5,'2. The Reported expanded Uncertinity In Measurement Is started as the standard Uncertinity');
  //  $pdf->SetXY(44,205);
  //  $pdf->Write(5,'in measurement multiplied by the coverage factor k=2 Which For a Normal distribution ');
  //  $pdf->SetXY(44,210);
  //  $pdf->Write(5,'corresponds to a coverage Probability Of approximately 95%');
}


function rightTableData($yPositionRight,$srNo,$total)
{
  global $pdf;
  global $detailRow;
  global $grnDetailrow;
  global $Errorunits;
  global $ErrorRdg;
  $pdf->SetXY(25,120);
  $pdf->Write(5,'9.OBSERVATION');
  $pdf->SetXY(20,$yPositionRight);
  $pdf->cell(12,05,$srNo,'1', '0', 'C');
  $pdf->SetXY(32,$yPositionRight);
  $pdf->cell(25,05,$grnDetailrow['parameterName'],'1', '0', 'C');
  $pdf->SetXY(57,$yPositionRight);
  $pdf->cell(25,05,$detailRow['masterMeterReading'],'1', '0', 'C');
  $pdf->SetXY(82,$yPositionRight);
  $pdf->cell(25,05,'600V','1', '0', 'C');
  $pdf->SetXY(107,$yPositionRight);
  $pdf->cell(25,05,$detailRow['stdMeterAverage'],'1', '0', 'C');
  $pdf->SetXY(132,$yPositionRight);
  $pdf->cell(25,05,$Errorunits,'1', '0', 'C');
  $pdf->SetXY(157,$yPositionRight);
  $pdf->cell(25,05,round($ErrorRdg,4),'1', '0', 'C');
  $pdf->SetXY(182,$yPositionRight);
  $pdf->cell(20,05,round($detailRow['expandedUncertinity'],4),'1', '0', 'C');
}


//function observationTableNoteDoubleTable()
//{
//  global $pdf;
//  $pdf->SetXY(40,130);
//  $pdf->Write(5,'NOTE');
//  $pdf->SetXY(40,135);
//  $pdf->Write(5,'1. Measurement Uncertinity is = 0.0676% For Ac Volt');
//  $pdf->SetXY(40,140);
//  $pdf->Write(5,'2. The Reported expanded Uncertinity In Measurement Is started as the standard Uncertinity');
//  $pdf->SetXY(44,145);
//  $pdf->Write(5,'in measurement multiplied by the coverage factor k=2 Which For a Normal distribution ');
//  $pdf->SetXY(44,150);
//  $pdf->Write(5,'corresponds to a coverage Probability Of approximately 95%');
//}
//function observationDateRes($yPosition,$totalRdg)
//{
//  global $pdf;
//  global $detailRow;
//  global $totalRdg;
//  $pdf->SetXY(122,$yPosition);
//  $pdf->cell(30,05,$detailRow['stdMeterAverageRes'],'1', '0', 'C');
// 
//  $pdf->SetXY(152,$yPosition);
//  $pdf->cell(30,05,$detailRow['testMeterAverageRes'],'1', '0', 'C');
//  $pdf->SetXY(182,$yPosition);
//  $pdf->cell(20,05,number_format($totalRdg,3,".",""),'1', '0', 'C');
//}

function masterMeterDetailLeft()
{
  global $pdf;
  global $masterMeterDetailRow;
  global $selectGrnMasterRow;
  global $s_activId;
  $pdf->SetXY(25,140);
  $pdf->write(5,'6.DETAILS OF REFERANCE STANDARD & MAJOR INSTRUMENT USED.');
  $pdf->SetXY(28,145);
  $pdf->write(5,'Name');
  $pdf->SetXY(100,145);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterName']);
  $pdf->SetXY(95,145);
  $pdf->Write(5,':');
  $pdf->SetXY(28,150);
  $pdf->write(5,'I.D No');
  $pdf->SetXY(100,150);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterIdNo']);
  $pdf->SetXY(95,150);
  $pdf->Write(5,':');
  $pdf->SetXY(28,155);
  $pdf->write(5,'Make');
  $pdf->SetXY(100,155);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterMake']);
  $pdf->SetXY(95,155);
  $pdf->Write(5,':');
  $pdf->SetXY(28,160);
  $pdf->write(5,'Model No');
  $pdf->SetXY(100,160);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterModelNo']);
  $pdf->SetXY(95,160);
  $pdf->Write(5,':');
  $pdf->SetXY(28,165);
  $pdf->write(5,'Serial No: ');
  $pdf->SetXY(100,165);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterSerialNo']);
  $pdf->SetXY(95,165);
  $pdf->Write(5,':');
  $pdf->SetXY(28,170);
  $pdf->write(5,'Certificate No');
  $pdf->SetXY(100,170);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterCertificateNo']);
  $pdf->SetXY(95,170);
  $pdf->Write(5,':');
  $pdf->SetXY(28,175);
  $pdf->write(5,'Calibration Valid Up To');
  $pdf->SetXY(100,175);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterExp']);
  $pdf->SetXY(95,175);
  $pdf->Write(5,':');
  $pdf->SetXY(28,180);
  $pdf->write(5,'Traceability To : ');
  $pdf->SetXY(100,180);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterTraceabilityTo']);
  $pdf->SetXY(95,180);
  $pdf->Write(5,':');
  $pdf->SetXY(25,187);
  $pdf->write(5,'7.PROCEDURE');
  $pdf->SetXY(99,187);
  $pdf->write(5,''.$selectGrnMasterRow['procedureText']);
  $pdf->SetXY(95,187);
  $pdf->Write(5,':');
  $pdf->SetXY(15,185);
}

function masterMeterDetailRight()
{
  global $pdf;
  global $masterMeterDetailRow;
  global $selectGrnMasterRow;
  $pdf->SetXY(150,145);
  $pdf->Write(5,':');
  $pdf->SetXY(155,145);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterName']);
  $pdf->SetXY(150,150);
  $pdf->Write(5,':');
  $pdf->SetXY(155,150);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterIdNo']);
  $pdf->SetXY(150,155);
  $pdf->Write(5,':');
  $pdf->SetXY(155,155);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterMake']);
  $pdf->SetXY(150,160);
  $pdf->Write(5,':');
  $pdf->SetXY(155,160);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterModelNo']);
  $pdf->SetXY(150,165);
  $pdf->Write(5,':');
  $pdf->SetXY(155,165);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterSerialNo']);
  $pdf->SetXY(150,170);
  $pdf->Write(5,':');
  $pdf->SetXY(155,170);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterCertificateNo']);
  $pdf->SetXY(150,175);
  $pdf->Write(5,':');
  $pdf->SetXY(155,175);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterExp']);
  $pdf->SetXY(150,180);
  $pdf->Write(5,':');
  $pdf->SetXY(155,180);
  $pdf->write(5,''.$masterMeterDetailRow['masterMeterTraceabilityTo']);
  $pdf->SetXY(150,187);
  $pdf->Write(5,':');
  $pdf->SetXY(154,187);
  $pdf->write(5,''.$selectGrnMasterRow['procedureText']);
}
function footer()
{
  global $pdf;
  global $masterMeterDetailRow;
  $pdf->Write(5,''.('_______________________________________________________________________________________________'));
  $pdf->SetXY(28,195);
  $pdf->write(5,'For  Krishna Instruments ');
  $pdf->SetXY(50,210);
  $pdf->write(5,' '.$masterMeterDetailRow['userName']);
  $pdf->SetXY(20,210);
  $pdf->write(5,'CALIBRATED BY:'); 
  $pdf->SetXY(90,210);
  $pdf->write(5,'APPROVED BY:'); 
  $pdf->SetXY(160,210);
  $pdf->write(5,'SEAL:'); 
  $pdf->SetXY(25,220);
  $pdf->write(5,'1.This report pertains To Particulars sample/Instruments submitted for test');
  $pdf->SetXY(25,225);
  $pdf->write(5,'2.This certificate may not be reproduced except in full,without prior written permission of Krishna Instruments.');
}
?>