<?php
include ("include/omConfig.php");
include ("include/functions.inc.php");
$grnPrefixNo = "";
$grnId = "";
$meterEntryNameArray = "";
$meterEntryNameArray = "";
$grnDetailId = "";
$stdMeter1 = "";
$testMeter1 = "";
$stdMeter2 = "";
$testMeter2 = "";
$stdMeter3 = "";
$testMeter3 = "";
$stdMeter4 = "";
$testMeter4 = "";
$stdMeter5 = "";
$testMeter5 = "";
$stdMeterAverage = "";
$testMeterAverage = "";
$accuracyTaken = "";
$standardDeviation = "";
$standardUncertinity = "";
$standardUncertinityperc = "";
$degreeOfFreedom = "";
$uncertinityForTypeB = "";
$degree_of_freedom_add = "";
$uncertinityInPercentage = "";
$accuracyForTypeB = "";
$acuuracyForTypeBPerc = "";
$resolutionTypeB = "";
$resolutionForTypeBPerc = "";
$stabilityForTypeB = "";
$stabilityForTypeBInPerc = "";
$stability_of_source = "";
$combinedUncertinity = "";
$combinedUncertinityInPerc = "";
$effectiveUncertinity = "";
$effectiveUncertinityInPer = "";
$effectiveDegreeOfFreed = "";
$meanReading = "";
$masterMeterReading = "";
$error = "";
$expandedUncertinity = "";
$humidity = "";
$masterMeterSubSubId = "";
$callibrationDate = "";
$meterEntryIdArray = array();
$approveArray = array();
$approveNameArray = '';
$userArray = array();
$userNameArray = '';
$tableArray = array();
$tableNameArray = '';
$weightArray = array();
$weightNameArray = '';
$ctArray = array();
$ctNameArray = '';
$nextYearDate = "";
$certificateIssue = "";
$certificateIssueDate = "";
$grnObsMasterId = 0;

$accuracy_fs      = '';
$accuracy_rdg     = '';
$master_range     = '';
$master_range_2     = '';
$master_range_3     = '';
$un_cali_certi    = '';
$un_cali_certi_2    = '';
$stdmeter_avg_new = '';
$r_phase_accuracy = '';
$uuc_least_count  = '';
$std_dev          = '';
$std_uncer        = '';
$std_uncer_per    = '';
$uncer_master     = '';
$acc_calibration  = '';
$resolution_new   = '';
$com_uncer        = '';
$exp_uncer        = '';
$exp_uncer_per_ue = '';
$uncer_of_install_error = '';
$uncer_of_repeat = '';
$cmc              = '';
$eccentric_loading = '';
$reference_weight = '';
$uncertainty_hysteresis = '';
$uncertainty_hysteresis_max = '';
$uncer_of_repeat_max = '';
$u1     = '';
$u1_val = '';
$u2     = '';
$u2_val = '';
$u3     = '';
$u3_val = '';
$u4     = '';
$u4_val = '';
$u5     = '';
$u5_val = '';
$u6     = '';
$u6_val = '';
$r_phase_accuracy_2 = '';
$uncer_master_2 = '';
$acc_calibration_2 = '';

if (isset($_GET['grnDetailPassId']) && $_GET['grnDetailPassId'] > 0){
    $grnDetailPassId = $_POST['grnDetailPassId'];
} else {
    $grnDetailPassId = 0;   
}

if (!isset($_SESSION['s_activId'])) {
    $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
    header("Location:checkLogin.php");
} else {
    if ((have_access_role(OBS_MODULE_ID,"add"))){
        if (isset($_POST['custCode'])) {
//            echo "<pre>"; print_r($_POST); exit;
            if(isset($_POST['accuracy_taking_account'])){$_POST['accuracy_taking_account'] = trim($_POST['accuracy_taking_account']," ");}
            if(isset($_POST['accuracy_taking_account_2'])){$_POST['accuracy_taking_account_2'] = trim($_POST['accuracy_taking_account_2']," ");}
            if(isset($_POST['accuracy_taking_account_3'])){$_POST['accuracy_taking_account_3'] = trim($_POST['accuracy_taking_account_3']," ");}
            if(isset($_POST['accuracy_of_rdg'])){$_POST['accuracy_of_rdg'] = trim($_POST['accuracy_of_rdg']," ");}
            if(isset($_POST['accuracy_of_rdg_2'])){$_POST['accuracy_of_rdg_2'] = trim($_POST['accuracy_of_rdg_2']," ");}
            if(isset($_POST['accuracy_of_rdg_3'])){$_POST['accuracy_of_rdg_3'] = trim($_POST['accuracy_of_rdg_3']," ");}
            if(isset($_POST['accuracy_of_fs'])){$_POST['accuracy_of_fs'] = trim($_POST['accuracy_of_fs']," ");}
            if(isset($_POST['accuracy_of_fs_2'])){$_POST['accuracy_of_fs_2'] = trim($_POST['accuracy_of_fs_2']," ");}
            if(isset($_POST['accuracy_of_fs_3'])){$_POST['accuracy_of_fs_3'] = trim($_POST['accuracy_of_fs_3']," ");}
            if(isset($_POST['master_stability'])){$_POST['master_stability'] = trim($_POST['master_stability']," ");}
            if(isset($_POST['master_stability_2'])){$_POST['master_stability_2'] = trim($_POST['master_stability_2']," ");}
            if(isset($_POST['master_stability_3'])){$_POST['master_stability_3'] = trim($_POST['master_stability_3']," ");}
            if(isset($_POST['resolution_of_master'])){$_POST['resolution_of_master'] = trim($_POST['resolution_of_master']," ");}
            if(isset($_POST['resolution_of_master_2'])){$_POST['resolution_of_master_2'] = trim($_POST['resolution_of_master_2']," ");}
            if(isset($_POST['resolution_of_master_3'])){$_POST['resolution_of_master_3'] = trim($_POST['resolution_of_master_3']," ");}
            if(isset($_POST['degree_of_freedom'])){$_POST['degree_of_freedom'] = trim($_POST['degree_of_freedom']," ");}
            if(isset($_POST['degree_of_freedom_2'])){$_POST['degree_of_freedom_2'] = trim($_POST['degree_of_freedom_2']," ");}
            if(isset($_POST['degree_of_freedom_3'])){$_POST['degree_of_freedom_3'] = trim($_POST['degree_of_freedom_3']," ");}
            if(isset($_POST['uncertinty_from_tracebility'])){$_POST['uncertinty_from_tracebility'] = trim($_POST['uncertinty_from_tracebility']," ");}
            if(isset($_POST['uncertinty_from_tracebility_2'])){$_POST['uncertinty_from_tracebility_2'] = trim($_POST['uncertinty_from_tracebility_2']," ");}
            if(isset($_POST['uncertinty_from_tracebility_3'])){$_POST['uncertinty_from_tracebility_3'] = trim($_POST['uncertinty_from_tracebility_3']," ");}
            
            if($_POST['table_info_id'] == '12'){
                
            } else if($_POST['table_info_id'] == '4'){
                if(isset($_POST['masterMeterId_3'])){ unset($_POST['masterMeterId_3']); }
                if(isset($_POST['masterMeterSubId_3'])){ unset($_POST['masterMeterSubId_3']); }
                if(isset($_POST['masterMeterSubSubId_3'])){ unset($_POST['masterMeterSubSubId_3']); }
            } else {
                if(isset($_POST['masterMeterId_2'])){ unset($_POST['masterMeterId_2']); }
                if(isset($_POST['masterMeterSubId_2'])){ unset($_POST['masterMeterSubId_2']); }
                if(isset($_POST['masterMeterSubSubId_2'])){ unset($_POST['masterMeterSubSubId_2']); }
                if(isset($_POST['masterMeterId_3'])){ unset($_POST['masterMeterId_3']); }
                if(isset($_POST['masterMeterSubId_3'])){ unset($_POST['masterMeterSubId_3']); }
                if(isset($_POST['masterMeterSubSubId_3'])){ unset($_POST['masterMeterSubSubId_3']); }
            }
            $type_ab_json = array();
            $_POST['resolution_of_master'] = trim($_POST['resolution_of_master']," ");
            $resolution_of_master = $_POST['resolution_of_master'];
            if(!empty($resolution_of_master)) {
                $exp_val = explode('.', $resolution_of_master);
                if(isset($exp_val[1])){
                    $decimal_master = strlen($exp_val[1]);  
                } else {
                  $decimal_master = 0;
                }
            } else {
                $decimal_master = 0;
            }
            $least_count = $_POST['leastCount'];
            if(!empty($least_count)) {
                $exp_val = explode('.', $least_count);
                if(isset($exp_val[1])){
                    $decimal_least = strlen($exp_val[1]);  
                } else {
                  $decimal_least = 0;
                }
            } else {
                $decimal_least = 0;
            }
            
            // NEW CALCULATION OF UNCERTAINTY : Start : //
            $m_cmc = '';
            $exp_uncer_per_ue = '';
            $qry13 = 'SELECT range_unit,coverage_factor,cmc FROM mastermetersubsub
                          WHERE masterMeterSubSubId = '.$_POST['masterMeterSubSubId'].' ';
            $res13 = mysql_query($qry13) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
            $m_cmc = '';
            while($row13 = mysql_fetch_assoc($res13)) {
                $m_cmc = $row13['cmc'];
            }
//            not for table info : 6B,16,17,19
            $selectm = "SELECT * FROM mastermetersubsub WHERE masterMeterSubSubId = ".$_POST['masterMeterSubSubId']."  ";
            $selectmResult = mysql_query($selectm);
            $master_range = '';
            while ($selectmRow = mysql_fetch_array($selectmResult)){
                $master_range = $selectmRow['rangeValue'];
            }
            if(isset($_POST['masterMeterSubSubId_2'])){
                $selectm = "SELECT * FROM mastermetersubsub WHERE masterMeterSubSubId = ".$_POST['masterMeterSubSubId_2']."  ";
                $selectmResult = mysql_query($selectm);
                $master_range_2 = '';
                while ($selectmRow = mysql_fetch_array($selectmResult)){
                    $master_range_2 = $selectmRow['rangeValue'];
                }
            }
            if(isset($_POST['masterMeterSubSubId_3'])){
                $selectm = "SELECT * FROM mastermetersubsub WHERE masterMeterSubSubId = ".$_POST['masterMeterSubSubId_3']."  ";
                $selectmResult = mysql_query($selectm);
                $master_range_3 = '';
                while ($selectmRow = mysql_fetch_array($selectmResult)){
                    $master_range_3 = $selectmRow['rangeValue'];
                }
            }
            if($_POST['table_info_id'] == '12'){
//                echo "<pre>"; print_r($_POST); exit;
                
                $testMeter_avg = isset($_POST['testMeter']) && !empty($_POST['testMeter']) ? stdmeter_avg($_POST['testMeter'][0]) : 0;
                $testMeter_avg2 = stdmeter_avg($_POST['testMeter2'][0]);
                $testMeter_avg3 = stdmeter_avg($_POST['testMeter3'][0]);
                $testMeter_avg4 = stdmeter_avg($_POST['testMeter4'][0]);
                $testMeter_avg5 = stdmeter_avg($_POST['testMeter5'][0]);
                $testMeter_avg6 = isset($_POST['testMeter6']) && !empty($_POST['testMeter6']) ? stdmeter_avg($_POST['testMeter6'][0]) : 0;
                $testMeter_avg7 = stdmeter_avg($_POST['testMeter7'][0]);
                $testMeter_avg8 = stdmeter_avg($_POST['testMeter8'][0]);
                $testMeter_avg9 = stdmeter_avg($_POST['testMeter9'][0]);
                $testMeter_avg10 = stdmeter_avg($_POST['testMeter10'][0]);
                $uncer_arr = array();
                
                if(!empty($testMeter_avg)){
                    $std_dev          = standardDeviation_val($_POST['testMeter'][0], $testMeter_avg);
                    $std_dev          = number_format((float)$std_dev, 4, '.', '');
                    
                    $std_uncer        = standardUncertinity_val($_POST['testMeter'][0], $std_dev);
                    $std_uncer        = number_format((float)$std_uncer, 4, '.', '');
                    
                    $std_uncer_per    = $std_uncer*100/$testMeter_avg;
                    $std_uncer_per    = number_format((float)$std_uncer_per, 4, '.', '');
                    
                    $uncertinty_of_std_ct = ($_POST['uncertinty_from_tracebility']/2)*$testMeter_avg*0.01;
                    $uncertinty_of_std_ct = number_format((float)$uncertinty_of_std_ct, 5, '.', '');
                    
                    $uncertinty_of_burden_box = ($_POST['uncertinty_from_tracebility_2']/2)*$testMeter_avg*0.01;
                    $uncertinty_of_burden_box = number_format((float)$uncertinty_of_burden_box, 5, '.', '');
                    
                    $uncertinty_of_ct_test = ($_POST['uncertinty_from_tracebility_3']/2)*$testMeter_avg*0.01;
                    $uncertinty_of_ct_test = number_format((float)$uncertinty_of_ct_test, 5, '.', '');
                    
                    $accuracy_of_std_ct = $_POST['accuracy_taking_account']*$testMeter_avg*0.01/sqrt(3);
                    $accuracy_of_std_ct = number_format((float)$accuracy_of_std_ct, 5, '.', '');
                    
                    $accuracy_of_ct_test = $_POST['accuracy_taking_account_3']*$testMeter_avg*0.01/sqrt(3);
                    $accuracy_of_ct_test = number_format((float)$accuracy_of_ct_test, 5, '.', '');
                    
                    $com_uncer        = sqrt(($std_uncer*$std_uncer)+($uncertinty_of_std_ct*$uncertinty_of_std_ct)+($uncertinty_of_burden_box*$uncertinty_of_burden_box)+($uncertinty_of_ct_test*$uncertinty_of_ct_test)+($accuracy_of_std_ct*$accuracy_of_std_ct)+($accuracy_of_ct_test*$accuracy_of_ct_test));
                    $com_uncer = number_format((float)$com_uncer, 5, '.', '');
                    
                    $exp_uncer        = 2*$com_uncer;
                    $exp_uncer = number_format((float)$exp_uncer, 5, '.', '');
                    
                    $exp_uncer_per_ue_1 = $exp_uncer*100/$testMeter_avg;
                    $exp_uncer_per_ue_1 = number_format((float)$exp_uncer_per_ue_1, 2, '.', '');
                    
                    $uncer_arr[] = $exp_uncer_per_ue_1;
                    $exp_uncer_per_ue_without_cmc_1 = $exp_uncer_per_ue_1;
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue_1){
                        $exp_uncer_per_ue_1 = $m_cmc;
                    }
                    $type_ab_json['std_dev'] = $std_dev;
                    $type_ab_json['std_uncer'] = $std_uncer;
                    $type_ab_json['std_uncer_per'] = $std_uncer_per;
                    $type_ab_json['uncertinty_of_std_ct'] = $uncertinty_of_std_ct;
                    $type_ab_json['uncertinty_of_burden_box'] = $uncertinty_of_burden_box;
                    $type_ab_json['uncertinty_of_ct_test'] = $uncertinty_of_ct_test;
                    $type_ab_json['accuracy_of_std_ct'] = $accuracy_of_std_ct;
                    $type_ab_json['accuracy_of_ct_test'] = $accuracy_of_ct_test;
                    $type_ab_json['com_uncer'] = $com_uncer;
                    $type_ab_json['exp_uncer'] = $exp_uncer;
                    $type_ab_json['exp_uncer_per_ue_without_cmc_1'] = $exp_uncer_per_ue_without_cmc_1;
                    $type_ab_json['exp_uncer_per_ue_1'] = $exp_uncer_per_ue_1;
                }
                if(!empty($testMeter_avg2)){
                    $std_dev_2          = standardDeviation_val($_POST['testMeter2'][0], $testMeter_avg2);
                    $std_dev_2          = number_format((float)$std_dev_2, 4, '.', '');
                    $std_uncer_2        = standardUncertinity_val($_POST['testMeter2'][0], $std_dev_2);
                    $std_uncer_2        = number_format((float)$std_uncer_2, 4, '.', '');
                    $std_uncer_per_2    = $std_uncer_2*100/$testMeter_avg2;
                    $std_uncer_per_2    = number_format((float)$std_uncer_per_2, 4, '.', '');
                    $uncertinty_of_std_ct_2 = ($_POST['uncertinty_from_tracebility']/2)*$testMeter_avg2*0.01;
                    $uncertinty_of_std_ct_2 = number_format((float)$uncertinty_of_std_ct_2, 5, '.', '');
                    $uncertinty_of_burden_box_2 = ($_POST['uncertinty_from_tracebility_2']/2)*$testMeter_avg2*0.01;
                    $uncertinty_of_burden_box_2 = number_format((float)$uncertinty_of_burden_box_2, 5, '.', '');
                    $uncertinty_of_ct_test_2 = ($_POST['uncertinty_from_tracebility_3']/2)*$testMeter_avg2*0.01;
                    $uncertinty_of_ct_test_2 = number_format((float)$uncertinty_of_ct_test_2, 5, '.', '');
                    $accuracy_of_std_ct_2 = $_POST['accuracy_taking_account']*$testMeter_avg2*0.01/sqrt(3);
                    $accuracy_of_std_ct_2 = number_format((float)$accuracy_of_std_ct_2, 5, '.', '');
                    $accuracy_of_ct_test_2 = $_POST['accuracy_taking_account_3']*$testMeter_avg2*0.01/sqrt(3);
                    $accuracy_of_ct_test_2 = number_format((float)$accuracy_of_ct_test_2, 5, '.', '');
                    $com_uncer_2        = sqrt(($std_uncer_2*$std_uncer_2)+($uncertinty_of_std_ct_2*$uncertinty_of_std_ct_2)+($uncertinty_of_burden_box_2*$uncertinty_of_burden_box_2)+($uncertinty_of_ct_test_2*$uncertinty_of_ct_test_2)+($accuracy_of_std_ct_2*$accuracy_of_std_ct_2)+($accuracy_of_ct_test_2*$accuracy_of_ct_test_2));
                    $com_uncer_2 = number_format((float)$com_uncer_2, 5, '.', '');
                    $exp_uncer_2        = 2*$com_uncer_2;
                    $exp_uncer_2 = number_format((float)$exp_uncer_2, 5, '.', '');
                    $exp_uncer_per_ue_2 = $exp_uncer_2*100/$testMeter_avg2;
                    $exp_uncer_per_ue_2 = number_format((float)$exp_uncer_per_ue_2, 2, '.', '');
                    $uncer_arr[] = $exp_uncer_per_ue_2;
                    $exp_uncer_per_ue_without_cmc_2 = $exp_uncer_per_ue_2;
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue_2){
                        $exp_uncer_per_ue_2 = $m_cmc;
                    }
                    $type_ab_json['std_dev_2'] = $std_dev_2;
                    $type_ab_json['std_uncer_2'] = $std_uncer_2;
                    $type_ab_json['std_uncer_per_2'] = $std_uncer_per_2;
                    $type_ab_json['uncertinty_of_std_ct_2'] = $uncertinty_of_std_ct_2;
                    $type_ab_json['uncertinty_of_burden_box_2'] = $uncertinty_of_burden_box_2;
                    $type_ab_json['uncertinty_of_ct_test_2'] = $uncertinty_of_ct_test_2;
                    $type_ab_json['accuracy_of_std_ct_2'] = $accuracy_of_std_ct_2;
                    $type_ab_json['accuracy_of_ct_test_2'] = $accuracy_of_ct_test_2;
                    $type_ab_json['com_uncer_2'] = $com_uncer_2;
                    $type_ab_json['exp_uncer_2'] = $exp_uncer_2;
                    $type_ab_json['exp_uncer_per_ue_without_cmc_2'] = $exp_uncer_per_ue_without_cmc_2;
                    $type_ab_json['exp_uncer_per_ue_2'] = $exp_uncer_per_ue_2;
                }
                if(!empty($testMeter_avg3)){
                    $std_dev_3          = standardDeviation_val($_POST['testMeter3'][0], $testMeter_avg3);
                    $std_dev_3          = number_format((float)$std_dev_3, 4, '.', '');
                    $std_uncer_3        = standardUncertinity_val($_POST['testMeter3'][0], $std_dev_3);
                    $std_uncer_3        = number_format((float)$std_uncer_3, 4, '.', '');
                    $std_uncer_per_3    = $std_uncer_3*100/$testMeter_avg3;
                    $std_uncer_per_3    = number_format((float)$std_uncer_per_3, 4, '.', '');
                    $uncertinty_of_std_ct_3 = ($_POST['uncertinty_from_tracebility']/2)*$testMeter_avg3*0.01;
                    $uncertinty_of_std_ct_3 = number_format((float)$uncertinty_of_std_ct_3, 5, '.', '');
                    $uncertinty_of_burden_box_3 = ($_POST['uncertinty_from_tracebility_2']/2)*$testMeter_avg3*0.01;
                    $uncertinty_of_burden_box_3 = number_format((float)$uncertinty_of_burden_box_3, 5, '.', '');
                    $uncertinty_of_ct_test_3 = ($_POST['uncertinty_from_tracebility_3']/2)*$testMeter_avg3*0.01;
                    $uncertinty_of_ct_test_3 = number_format((float)$uncertinty_of_ct_test_3, 5, '.', '');
                    $accuracy_of_std_ct_3 = $_POST['accuracy_taking_account']*$testMeter_avg3*0.01/sqrt(3);
                    $accuracy_of_std_ct_3 = number_format((float)$accuracy_of_std_ct_3, 5, '.', '');
                    $accuracy_of_ct_test_3 = $_POST['accuracy_taking_account_3']*$testMeter_avg3*0.01/sqrt(3);
                    $accuracy_of_ct_test_3 = number_format((float)$accuracy_of_ct_test_3, 5, '.', '');
                    $com_uncer_3        = sqrt(($std_uncer_3*$std_uncer_3)+($uncertinty_of_std_ct_3*$uncertinty_of_std_ct_3)+($uncertinty_of_burden_box_3*$uncertinty_of_burden_box_3)+($uncertinty_of_ct_test_3*$uncertinty_of_ct_test_3)+($accuracy_of_std_ct_3*$accuracy_of_std_ct_3)+($accuracy_of_ct_test_3*$accuracy_of_ct_test_3));
                    $com_uncer_3 = number_format((float)$com_uncer_3, 5, '.', '');
                    $exp_uncer_3        = 2*$com_uncer_3;
                    $exp_uncer_3 = number_format((float)$exp_uncer_3, 5, '.', '');
                    $exp_uncer_per_ue_3 = $exp_uncer_3*100/$testMeter_avg3;
                    $exp_uncer_per_ue_3 = number_format((float)$exp_uncer_per_ue_3, 2, '.', '');
                    $uncer_arr[] = $exp_uncer_per_ue_3;
                    $exp_uncer_per_ue_without_cmc_3 = $exp_uncer_per_ue_3;
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue_3){
                        $exp_uncer_per_ue_3 = $m_cmc;
                    }
                    $type_ab_json['std_dev_3'] = $std_dev_3;
                    $type_ab_json['std_uncer_3'] = $std_uncer_3;
                    $type_ab_json['std_uncer_per_3'] = $std_uncer_per_3;
                    $type_ab_json['uncertinty_of_std_ct_3'] = $uncertinty_of_std_ct_3;
                    $type_ab_json['uncertinty_of_burden_box_3'] = $uncertinty_of_burden_box_3;
                    $type_ab_json['uncertinty_of_ct_test_3'] = $uncertinty_of_ct_test_3;
                    $type_ab_json['accuracy_of_std_ct_3'] = $accuracy_of_std_ct_3;
                    $type_ab_json['accuracy_of_ct_test_3'] = $accuracy_of_ct_test_3;
                    $type_ab_json['com_uncer_3'] = $com_uncer_3;
                    $type_ab_json['exp_uncer_3'] = $exp_uncer_3;
                    $type_ab_json['exp_uncer_per_ue_without_cmc_3'] = $exp_uncer_per_ue_without_cmc_3;
                    $type_ab_json['exp_uncer_per_ue_3'] = $exp_uncer_per_ue_3;
                }
                if(!empty($testMeter_avg4)){
                    $std_dev_4          = standardDeviation_val($_POST['testMeter4'][0], $testMeter_avg4);
                    $std_dev_4          = number_format((float)$std_dev_4, 4, '.', '');
                    $std_uncer_4        = standardUncertinity_val($_POST['testMeter4'][0], $std_dev_4);
                    $std_uncer_4        = number_format((float)$std_uncer_4, 4, '.', '');
                    $std_uncer_per_4    = $std_uncer_4*100/$testMeter_avg4;
                    $std_uncer_per_4    = number_format((float)$std_uncer_per_4, 4, '.', '');
                    $uncertinty_of_std_ct_4 = ($_POST['uncertinty_from_tracebility']/2)*$testMeter_avg4*0.01;
                    $uncertinty_of_std_ct_4 = number_format((float)$uncertinty_of_std_ct_4, 5, '.', '');
                    $uncertinty_of_burden_box_4 = ($_POST['uncertinty_from_tracebility_2']/2)*$testMeter_avg4*0.01;
                    $uncertinty_of_burden_box_4 = number_format((float)$uncertinty_of_burden_box_4, 5, '.', '');
                    $uncertinty_of_ct_test_4 = ($_POST['uncertinty_from_tracebility_3']/2)*$testMeter_avg4*0.01;
                    $uncertinty_of_ct_test_4 = number_format((float)$uncertinty_of_ct_test_4, 5, '.', '');
                    $accuracy_of_std_ct_4 = $_POST['accuracy_taking_account']*$testMeter_avg4*0.01/sqrt(3);
                    $accuracy_of_std_ct_4 = number_format((float)$accuracy_of_std_ct_4, 5, '.', '');
                    $accuracy_of_ct_test_4 = $_POST['accuracy_taking_account_3']*$testMeter_avg4*0.01/sqrt(3);
                    $accuracy_of_ct_test_4 = number_format((float)$accuracy_of_ct_test_4, 5, '.', '');
                    $com_uncer_4        = sqrt(($std_uncer_4*$std_uncer_4)+($uncertinty_of_std_ct_4*$uncertinty_of_std_ct_4)+($uncertinty_of_burden_box_4*$uncertinty_of_burden_box_4)+($uncertinty_of_ct_test_4*$uncertinty_of_ct_test_4)+($accuracy_of_std_ct_4*$accuracy_of_std_ct_4)+($accuracy_of_ct_test_4*$accuracy_of_ct_test_4));
                    $com_uncer_4 = number_format((float)$com_uncer_4, 5, '.', '');
                    $exp_uncer_4        = 2*$com_uncer_4;
                    $exp_uncer_4 = number_format((float)$exp_uncer_4, 5, '.', '');
                    $exp_uncer_per_ue_4 = $exp_uncer_4*100/$testMeter_avg4;
                    $exp_uncer_per_ue_4 = number_format((float)$exp_uncer_per_ue_4, 2, '.', '');
                    $uncer_arr[] = $exp_uncer_per_ue_4;
                    $exp_uncer_per_ue_without_cmc_4 = $exp_uncer_per_ue_4;
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue_4){
                        $exp_uncer_per_ue_4 = $m_cmc;
                    }
                    $type_ab_json['std_dev_4'] = $std_dev_4;
                    $type_ab_json['std_uncer_4'] = $std_uncer_4;
                    $type_ab_json['std_uncer_per_4'] = $std_uncer_per_4;
                    $type_ab_json['uncertinty_of_std_ct_4'] = $uncertinty_of_std_ct_4;
                    $type_ab_json['uncertinty_of_burden_box_4'] = $uncertinty_of_burden_box_4;
                    $type_ab_json['uncertinty_of_ct_test_4'] = $uncertinty_of_ct_test_4;
                    $type_ab_json['accuracy_of_std_ct_4'] = $accuracy_of_std_ct_4;
                    $type_ab_json['accuracy_of_ct_test_4'] = $accuracy_of_ct_test_4;
                    $type_ab_json['com_uncer_4'] = $com_uncer_4;
                    $type_ab_json['exp_uncer_4'] = $exp_uncer_4;
                    $type_ab_json['exp_uncer_per_ue_without_cmc_4'] = $exp_uncer_per_ue_without_cmc_4;
                    $type_ab_json['exp_uncer_per_ue_4'] = $exp_uncer_per_ue_4;
                }
                if(!empty($testMeter_avg5)){
                    $std_dev_5          = standardDeviation_val($_POST['testMeter5'][0], $testMeter_avg5);
                    $std_dev_5          = number_format((float)$std_dev_5, 4, '.', '');
                    $std_uncer_5        = standardUncertinity_val($_POST['testMeter5'][0], $std_dev_5);
                    $std_uncer_5        = number_format((float)$std_uncer_5, 4, '.', '');
                    $std_uncer_per_5    = $std_uncer_5*100/$testMeter_avg5;
                    $std_uncer_per_5    = number_format((float)$std_uncer_per_5, 4, '.', '');
                    $uncertinty_of_std_ct_5 = ($_POST['uncertinty_from_tracebility']/2)*$testMeter_avg5*0.01;
                    $uncertinty_of_std_ct_5 = number_format((float)$uncertinty_of_std_ct_5, 5, '.', '');
                    $uncertinty_of_burden_box_5 = ($_POST['uncertinty_from_tracebility_2']/2)*$testMeter_avg5*0.01;
                    $uncertinty_of_burden_box_5 = number_format((float)$uncertinty_of_burden_box_5, 5, '.', '');
                    $uncertinty_of_ct_test_5 = ($_POST['uncertinty_from_tracebility_3']/2)*$testMeter_avg5*0.01;
                    $uncertinty_of_ct_test_5 = number_format((float)$uncertinty_of_ct_test_5, 5, '.', '');
                    $accuracy_of_std_ct_5 = $_POST['accuracy_taking_account']*$testMeter_avg5*0.01/sqrt(3);
                    $accuracy_of_std_ct_5 = number_format((float)$accuracy_of_std_ct_5, 5, '.', '');
                    $accuracy_of_ct_test_5 = $_POST['accuracy_taking_account_3']*$testMeter_avg5*0.01/sqrt(3);
                    $accuracy_of_ct_test_5 = number_format((float)$accuracy_of_ct_test_5, 5, '.', '');
                    $com_uncer_5        = sqrt(($std_uncer_5*$std_uncer_5)+($uncertinty_of_std_ct_5*$uncertinty_of_std_ct_5)+($uncertinty_of_burden_box_5*$uncertinty_of_burden_box_5)+($uncertinty_of_ct_test_5*$uncertinty_of_ct_test_5)+($accuracy_of_std_ct_5*$accuracy_of_std_ct_5)+($accuracy_of_ct_test_5*$accuracy_of_ct_test_5));
                    $com_uncer_5 = number_format((float)$com_uncer_5, 5, '.', '');
                    $exp_uncer_5        = 2*$com_uncer_5;
                    $exp_uncer_5 = number_format((float)$exp_uncer_5, 5, '.', '');
                    $exp_uncer_per_ue_5 = $exp_uncer_5*100/$testMeter_avg5;
                    $exp_uncer_per_ue_5 = number_format((float)$exp_uncer_per_ue_5, 2, '.', '');
                    $uncer_arr[] = $exp_uncer_per_ue_5;
                    $exp_uncer_per_ue_without_cmc_5 = $exp_uncer_per_ue_5;
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue_5){
                        $exp_uncer_per_ue_5 = $m_cmc;
                    }
                    $type_ab_json['std_dev_5'] = $std_dev_5;
                    $type_ab_json['std_uncer_5'] = $std_uncer_5;
                    $type_ab_json['std_uncer_per_5'] = $std_uncer_per_5;
                    $type_ab_json['uncertinty_of_std_ct_5'] = $uncertinty_of_std_ct_5;
                    $type_ab_json['uncertinty_of_burden_box_5'] = $uncertinty_of_burden_box_5;
                    $type_ab_json['uncertinty_of_ct_test_5'] = $uncertinty_of_ct_test_5;
                    $type_ab_json['accuracy_of_std_ct_5'] = $accuracy_of_std_ct_5;
                    $type_ab_json['accuracy_of_ct_test_5'] = $accuracy_of_ct_test_5;
                    $type_ab_json['com_uncer_5'] = $com_uncer_5;
                    $type_ab_json['exp_uncer_5'] = $exp_uncer_5;
                    $type_ab_json['exp_uncer_per_ue_without_cmc_5'] = $exp_uncer_per_ue_without_cmc_5;
                    $type_ab_json['exp_uncer_per_ue_5'] = $exp_uncer_per_ue_5;
                }
                if(!empty($testMeter_avg6)){
                    $std_dev_6          = standardDeviation_val($_POST['testMeter6'][0], $testMeter_avg6);
                    $std_dev_6          = number_format((float)$std_dev_6, 4, '.', '');
                    $std_uncer_6        = standardUncertinity_val($_POST['testMeter6'][0], $std_dev_6);
                    $std_uncer_6        = number_format((float)$std_uncer_6, 4, '.', '');
                    $std_uncer_per_6    = $std_uncer_6*100/$testMeter_avg6;
                    $std_uncer_per_6    = number_format((float)$std_uncer_per_6, 4, '.', '');
                    $uncertinty_of_std_ct_6 = ($_POST['uncertinty_from_tracebility']/2)*$testMeter_avg6*0.01;
                    $uncertinty_of_std_ct_6 = number_format((float)$uncertinty_of_std_ct_6, 5, '.', '');
                    $uncertinty_of_burden_box_6 = ($_POST['uncertinty_from_tracebility_2']/2)*$testMeter_avg6*0.01;
                    $uncertinty_of_burden_box_6 = number_format((float)$uncertinty_of_burden_box_6, 5, '.', '');
                    $uncertinty_of_ct_test_6 = ($_POST['uncertinty_from_tracebility_3']/2)*$testMeter_avg6*0.01;
                    $uncertinty_of_ct_test_6 = number_format((float)$uncertinty_of_ct_test_6, 5, '.', '');
                    $accuracy_of_std_ct_6 = $_POST['accuracy_taking_account']*$testMeter_avg6*0.01/sqrt(3);
                    $accuracy_of_std_ct_6 = number_format((float)$accuracy_of_std_ct_6, 5, '.', '');
                    $accuracy_of_ct_test_6 = $_POST['accuracy_taking_account_3']*$testMeter_avg6*0.01/sqrt(3);
                    $accuracy_of_ct_test_6 = number_format((float)$accuracy_of_ct_test_6, 5, '.', '');
                    $com_uncer_6        = sqrt(($std_uncer_6*$std_uncer_6)+($uncertinty_of_std_ct_6*$uncertinty_of_std_ct_6)+($uncertinty_of_burden_box_6*$uncertinty_of_burden_box_6)+($uncertinty_of_ct_test_6*$uncertinty_of_ct_test_6)+($accuracy_of_std_ct_6*$accuracy_of_std_ct_6)+($accuracy_of_ct_test_6*$accuracy_of_ct_test_6));
                    $com_uncer_6 = number_format((float)$com_uncer_6, 5, '.', '');
                    $exp_uncer_6        = 2*$com_uncer_6;
                    $exp_uncer_6 = number_format((float)$exp_uncer_6, 5, '.', '');
                    $exp_uncer_per_ue_6 = $exp_uncer_6*100/$testMeter_avg6;
                    $exp_uncer_per_ue_6 = number_format((float)$exp_uncer_per_ue_6, 2, '.', '');
                    $uncer_arr[] = $exp_uncer_per_ue_6;
                    $exp_uncer_per_ue_without_cmc_6 = $exp_uncer_per_ue_6;
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue_6){
                        $exp_uncer_per_ue_6 = $m_cmc;
                    }
                    $type_ab_json['std_dev_6'] = $std_dev_6;
                    $type_ab_json['std_uncer_6'] = $std_uncer_6;
                    $type_ab_json['std_uncer_per_6'] = $std_uncer_per_6;
                    $type_ab_json['uncertinty_of_std_ct_6'] = $uncertinty_of_std_ct_6;
                    $type_ab_json['uncertinty_of_burden_box_6'] = $uncertinty_of_burden_box_6;
                    $type_ab_json['uncertinty_of_ct_test_6'] = $uncertinty_of_ct_test_6;
                    $type_ab_json['accuracy_of_std_ct_6'] = $accuracy_of_std_ct_6;
                    $type_ab_json['accuracy_of_ct_test_6'] = $accuracy_of_ct_test_6;
                    $type_ab_json['com_uncer_6'] = $com_uncer_6;
                    $type_ab_json['exp_uncer_6'] = $exp_uncer_6;
                    $type_ab_json['exp_uncer_per_ue_without_cmc_6'] = $exp_uncer_per_ue_without_cmc_6;
                    $type_ab_json['exp_uncer_per_ue_6'] = $exp_uncer_per_ue_6;
                }
                if(!empty($testMeter_avg7)){
                    $std_dev_7          = standardDeviation_val($_POST['testMeter7'][0], $testMeter_avg7);
                    $std_dev_7          = number_format((float)$std_dev_7, 4, '.', '');
                    $std_uncer_7        = standardUncertinity_val($_POST['testMeter7'][0], $std_dev_7);
                    $std_uncer_7        = number_format((float)$std_uncer_7, 4, '.', '');
                    $std_uncer_per_7    = $std_uncer_7*100/$testMeter_avg7;
                    $std_uncer_per_7    = number_format((float)$std_uncer_per_7, 4, '.', '');
                    $uncertinty_of_std_ct_7 = ($_POST['uncertinty_from_tracebility']/2)*$testMeter_avg7*0.01;
                    $uncertinty_of_std_ct_7 = number_format((float)$uncertinty_of_std_ct_7, 5, '.', '');
                    $uncertinty_of_burden_box_7 = ($_POST['uncertinty_from_tracebility_2']/2)*$testMeter_avg7*0.01;
                    $uncertinty_of_burden_box_7 = number_format((float)$uncertinty_of_burden_box_7, 5, '.', '');
                    $uncertinty_of_ct_test_7 = ($_POST['uncertinty_from_tracebility_3']/2)*$testMeter_avg7*0.01;
                    $uncertinty_of_ct_test_7 = number_format((float)$uncertinty_of_ct_test_7, 5, '.', '');
                    $accuracy_of_std_ct_7 = $_POST['accuracy_taking_account']*$testMeter_avg7*0.01/sqrt(3);
                    $accuracy_of_std_ct_7 = number_format((float)$accuracy_of_std_ct_7, 5, '.', '');
                    $accuracy_of_ct_test_7 = $_POST['accuracy_taking_account_3']*$testMeter_avg7*0.01/sqrt(3);
                    $accuracy_of_ct_test_7 = number_format((float)$accuracy_of_ct_test_7, 5, '.', '');
                    $com_uncer_7        = sqrt(($std_uncer_7*$std_uncer_7)+($uncertinty_of_std_ct_7*$uncertinty_of_std_ct_7)+($uncertinty_of_burden_box_7*$uncertinty_of_burden_box_7)+($uncertinty_of_ct_test_7*$uncertinty_of_ct_test_7)+($accuracy_of_std_ct_7*$accuracy_of_std_ct_7)+($accuracy_of_ct_test_7*$accuracy_of_ct_test_7));
                    $com_uncer_7 = number_format((float)$com_uncer_7, 5, '.', '');
                    $exp_uncer_7        = 2*$com_uncer_7;
                    $exp_uncer_7 = number_format((float)$exp_uncer_7, 5, '.', '');
                    $exp_uncer_per_ue_7 = $exp_uncer_7*100/$testMeter_avg7;
                    $exp_uncer_per_ue_7 = number_format((float)$exp_uncer_per_ue_7, 2, '.', '');
                    $uncer_arr[] = $exp_uncer_per_ue_7;
                    $exp_uncer_per_ue_without_cmc_7 = $exp_uncer_per_ue_7;
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue_7){
                        $exp_uncer_per_ue_7 = $m_cmc;
                    }
                    $type_ab_json['std_dev_7'] = $std_dev_7;
                    $type_ab_json['std_uncer_7'] = $std_uncer_7;
                    $type_ab_json['std_uncer_per_7'] = $std_uncer_per_7;
                    $type_ab_json['uncertinty_of_std_ct_7'] = $uncertinty_of_std_ct_7;
                    $type_ab_json['uncertinty_of_burden_box_7'] = $uncertinty_of_burden_box_7;
                    $type_ab_json['uncertinty_of_ct_test_7'] = $uncertinty_of_ct_test_7;
                    $type_ab_json['accuracy_of_std_ct_7'] = $accuracy_of_std_ct_7;
                    $type_ab_json['accuracy_of_ct_test_7'] = $accuracy_of_ct_test_7;
                    $type_ab_json['com_uncer_7'] = $com_uncer_7;
                    $type_ab_json['exp_uncer_7'] = $exp_uncer_7;
                    $type_ab_json['exp_uncer_per_ue_without_cmc_7'] = $exp_uncer_per_ue_without_cmc_7;
                    $type_ab_json['exp_uncer_per_ue_7'] = $exp_uncer_per_ue_7;
                }
                if(!empty($testMeter_avg8)){
                    $std_dev_8          = standardDeviation_val($_POST['testMeter8'][0], $testMeter_avg8);
                    $std_dev_8          = number_format((float)$std_dev_8, 4, '.', '');
                    $std_uncer_8        = standardUncertinity_val($_POST['testMeter8'][0], $std_dev_8);
                    $std_uncer_8        = number_format((float)$std_uncer_8, 4, '.', '');
                    $std_uncer_per_8    = $std_uncer_8*100/$testMeter_avg8;
                    $std_uncer_per_8    = number_format((float)$std_uncer_per_8, 4, '.', '');
                    $uncertinty_of_std_ct_8 = ($_POST['uncertinty_from_tracebility']/2)*$testMeter_avg8*0.01;
                    $uncertinty_of_std_ct_8 = number_format((float)$uncertinty_of_std_ct_8, 5, '.', '');
                    $uncertinty_of_burden_box_8 = ($_POST['uncertinty_from_tracebility_2']/2)*$testMeter_avg8*0.01;
                    $uncertinty_of_burden_box_8 = number_format((float)$uncertinty_of_burden_box_8, 5, '.', '');
                    $uncertinty_of_ct_test_8 = ($_POST['uncertinty_from_tracebility_3']/2)*$testMeter_avg8*0.01;
                    $uncertinty_of_ct_test_8 = number_format((float)$uncertinty_of_ct_test_8, 5, '.', '');
                    $accuracy_of_std_ct_8 = $_POST['accuracy_taking_account']*$testMeter_avg8*0.01/sqrt(3);
                    $accuracy_of_std_ct_8 = number_format((float)$accuracy_of_std_ct_8, 5, '.', '');
                    $accuracy_of_ct_test_8 = $_POST['accuracy_taking_account_3']*$testMeter_avg8*0.01/sqrt(3);
                    $accuracy_of_ct_test_8 = number_format((float)$accuracy_of_ct_test_8, 5, '.', '');
                    $com_uncer_8        = sqrt(($std_uncer_8*$std_uncer_8)+($uncertinty_of_std_ct_8*$uncertinty_of_std_ct_8)+($uncertinty_of_burden_box_8*$uncertinty_of_burden_box_8)+($uncertinty_of_ct_test_8*$uncertinty_of_ct_test_8)+($accuracy_of_std_ct_8*$accuracy_of_std_ct_8)+($accuracy_of_ct_test_8*$accuracy_of_ct_test_8));
                    $com_uncer_8 = number_format((float)$com_uncer_8, 5, '.', '');
                    $exp_uncer_8        = 2*$com_uncer_8;
                    $exp_uncer_8 = number_format((float)$exp_uncer_8, 5, '.', '');
                    $exp_uncer_per_ue_8 = $exp_uncer_8*100/$testMeter_avg8;
                    $exp_uncer_per_ue_8 = number_format((float)$exp_uncer_per_ue_8, 2, '.', '');
                    $uncer_arr[] = $exp_uncer_per_ue_8;
                    $exp_uncer_per_ue_without_cmc_8 = $exp_uncer_per_ue_8;
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue_8){
                        $exp_uncer_per_ue_8 = $m_cmc;
                    }
                    $type_ab_json['std_dev_8'] = $std_dev_8;
                    $type_ab_json['std_uncer_8'] = $std_uncer_8;
                    $type_ab_json['std_uncer_per_8'] = $std_uncer_per_8;
                    $type_ab_json['uncertinty_of_std_ct_8'] = $uncertinty_of_std_ct_8;
                    $type_ab_json['uncertinty_of_burden_box_8'] = $uncertinty_of_burden_box_8;
                    $type_ab_json['uncertinty_of_ct_test_8'] = $uncertinty_of_ct_test_8;
                    $type_ab_json['accuracy_of_std_ct_8'] = $accuracy_of_std_ct_8;
                    $type_ab_json['accuracy_of_ct_test_8'] = $accuracy_of_ct_test_8;
                    $type_ab_json['com_uncer_8'] = $com_uncer_8;
                    $type_ab_json['exp_uncer_8'] = $exp_uncer_8;
                    $type_ab_json['exp_uncer_per_ue_without_cmc_8'] = $exp_uncer_per_ue_without_cmc_8;
                    $type_ab_json['exp_uncer_per_ue_8'] = $exp_uncer_per_ue_8;
                }
                if(!empty($testMeter_avg9)){
                    $std_dev_9          = standardDeviation_val($_POST['testMeter9'][0], $testMeter_avg9);
                    $std_dev_9          = number_format((float)$std_dev_9, 4, '.', '');
                    $std_uncer_9        = standardUncertinity_val($_POST['testMeter9'][0], $std_dev_9);
                    $std_uncer_9        = number_format((float)$std_uncer_9, 4, '.', '');
                    $std_uncer_per_9    = $std_uncer_9*100/$testMeter_avg9;
                    $std_uncer_per_9    = number_format((float)$std_uncer_per_9, 4, '.', '');
                    $uncertinty_of_std_ct_9 = ($_POST['uncertinty_from_tracebility']/2)*$testMeter_avg9*0.01;
                    $uncertinty_of_std_ct_9 = number_format((float)$uncertinty_of_std_ct_9, 5, '.', '');
                    $uncertinty_of_burden_box_9 = ($_POST['uncertinty_from_tracebility_2']/2)*$testMeter_avg9*0.01;
                    $uncertinty_of_burden_box_9 = number_format((float)$uncertinty_of_burden_box_9, 5, '.', '');
                    $uncertinty_of_ct_test_9 = ($_POST['uncertinty_from_tracebility_3']/2)*$testMeter_avg9*0.01;
                    $uncertinty_of_ct_test_9 = number_format((float)$uncertinty_of_ct_test_9, 5, '.', '');
                    $accuracy_of_std_ct_9 = $_POST['accuracy_taking_account']*$testMeter_avg9*0.01/sqrt(3);
                    $accuracy_of_std_ct_9 = number_format((float)$accuracy_of_std_ct_9, 5, '.', '');
                    $accuracy_of_ct_test_9 = $_POST['accuracy_taking_account_3']*$testMeter_avg9*0.01/sqrt(3);
                    $accuracy_of_ct_test_9 = number_format((float)$accuracy_of_ct_test_9, 5, '.', '');
                    $com_uncer_9        = sqrt(($std_uncer_9*$std_uncer_9)+($uncertinty_of_std_ct_9*$uncertinty_of_std_ct_9)+($uncertinty_of_burden_box_9*$uncertinty_of_burden_box_9)+($uncertinty_of_ct_test_9*$uncertinty_of_ct_test_9)+($accuracy_of_std_ct_9*$accuracy_of_std_ct_9)+($accuracy_of_ct_test_9*$accuracy_of_ct_test_9));
                    $com_uncer_9 = number_format((float)$com_uncer_9, 5, '.', '');
                    $exp_uncer_9        = 2*$com_uncer_9;
                    $exp_uncer_9 = number_format((float)$exp_uncer_9, 5, '.', '');
                    $exp_uncer_per_ue_9 = $exp_uncer_9*100/$testMeter_avg9;
                    $exp_uncer_per_ue_9 = number_format((float)$exp_uncer_per_ue_9, 2, '.', '');
                    $exp_uncer_per_ue_without_cmc_9 = $exp_uncer_per_ue_9;
                    $uncer_arr[] = $exp_uncer_per_ue_9;
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue_9){
                        $exp_uncer_per_ue_9 = $m_cmc;
                    }
                    $type_ab_json['std_dev_9'] = $std_dev_9;
                    $type_ab_json['std_uncer_9'] = $std_uncer_9;
                    $type_ab_json['std_uncer_per_9'] = $std_uncer_per_9;
                    $type_ab_json['uncertinty_of_std_ct_9'] = $uncertinty_of_std_ct_9;
                    $type_ab_json['uncertinty_of_burden_box_9'] = $uncertinty_of_burden_box_9;
                    $type_ab_json['uncertinty_of_ct_test_9'] = $uncertinty_of_ct_test_9;
                    $type_ab_json['accuracy_of_std_ct_9'] = $accuracy_of_std_ct_9;
                    $type_ab_json['accuracy_of_ct_test_9'] = $accuracy_of_ct_test_9;
                    $type_ab_json['com_uncer_9'] = $com_uncer_9;
                    $type_ab_json['exp_uncer_9'] = $exp_uncer_9;
                    $type_ab_json['exp_uncer_per_ue_without_cmc_9'] = $exp_uncer_per_ue_without_cmc_9;
                    $type_ab_json['exp_uncer_per_ue_9'] = $exp_uncer_per_ue_9;
                }
                if(!empty($testMeter_avg10)){
                    $std_dev_10          = standardDeviation_val($_POST['testMeter10'][0], $testMeter_avg10);
                    $std_dev_10          = number_format((float)$std_dev_10, 4, '.', '');
                    $std_uncer_10        = standardUncertinity_val($_POST['testMeter10'][0], $std_dev_10);
                    $std_uncer_10        = number_format((float)$std_uncer_10, 4, '.', '');
                    $std_uncer_per_10    = $std_uncer_10*100/$testMeter_avg10;
                    $std_uncer_per_10    = number_format((float)$std_uncer_per_10, 4, '.', '');
                    $uncertinty_of_std_ct_10 = ($_POST['uncertinty_from_tracebility']/2)*$testMeter_avg10*0.01;
                    $uncertinty_of_std_ct_10 = number_format((float)$uncertinty_of_std_ct_10, 5, '.', '');
                    $uncertinty_of_burden_box_10 = ($_POST['uncertinty_from_tracebility_2']/2)*$testMeter_avg10*0.01;
                    $uncertinty_of_burden_box_10 = number_format((float)$uncertinty_of_burden_box_10, 5, '.', '');
                    $uncertinty_of_ct_test_10 = ($_POST['uncertinty_from_tracebility_3']/2)*$testMeter_avg10*0.01;
                    $uncertinty_of_ct_test_10 = number_format((float)$uncertinty_of_ct_test_10, 5, '.', '');
                    $accuracy_of_std_ct_10 = $_POST['accuracy_taking_account']*$testMeter_avg10*0.01/sqrt(3);
                    $accuracy_of_std_ct_10 = number_format((float)$accuracy_of_std_ct_10, 5, '.', '');
                    $accuracy_of_ct_test_10 = $_POST['accuracy_taking_account_3']*$testMeter_avg10*0.01/sqrt(3);
                    $accuracy_of_ct_test_10 = number_format((float)$accuracy_of_ct_test_10, 5, '.', '');
                    $com_uncer_10        = sqrt(($std_uncer_10*$std_uncer_10)+($uncertinty_of_std_ct_10*$uncertinty_of_std_ct_10)+($uncertinty_of_burden_box_10*$uncertinty_of_burden_box_10)+($uncertinty_of_ct_test_10*$uncertinty_of_ct_test_10)+($accuracy_of_std_ct_10*$accuracy_of_std_ct_10)+($accuracy_of_ct_test_10*$accuracy_of_ct_test_10));
                    $com_uncer_10 = number_format((float)$com_uncer_10, 5, '.', '');
                    $exp_uncer_10        = 2*$com_uncer_10;
                    $exp_uncer_10 = number_format((float)$exp_uncer_10, 5, '.', '');
                    $exp_uncer_per_ue_10 = $exp_uncer_10*100/$testMeter_avg10;
                    $exp_uncer_per_ue_10 = number_format((float)$exp_uncer_per_ue_10, 2, '.', '');
                    $uncer_arr[] = $exp_uncer_per_ue_10;
                    $exp_uncer_per_ue_without_cmc_10 = $exp_uncer_per_ue_10;
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue_10){
                        $exp_uncer_per_ue_10 = $m_cmc;
                    }
                    $type_ab_json['std_dev_10'] = $std_dev_10;
                    $type_ab_json['std_uncer_10'] = $std_uncer_10;
                    $type_ab_json['std_uncer_per_10'] = $std_uncer_per_10;
                    $type_ab_json['uncertinty_of_std_ct_10'] = $uncertinty_of_std_ct_10;
                    $type_ab_json['uncertinty_of_burden_box_10'] = $uncertinty_of_burden_box_10;
                    $type_ab_json['uncertinty_of_ct_test_10'] = $uncertinty_of_ct_test_10;
                    $type_ab_json['accuracy_of_std_ct_10'] = $accuracy_of_std_ct_10;
                    $type_ab_json['accuracy_of_ct_test_10'] = $accuracy_of_ct_test_10;
                    $type_ab_json['com_uncer_10'] = $com_uncer_10;
                    $type_ab_json['exp_uncer_10'] = $exp_uncer_10;
                    $type_ab_json['exp_uncer_per_ue_without_cmc_10'] = $exp_uncer_per_ue_without_cmc_10;
                    $type_ab_json['exp_uncer_per_ue_10'] = $exp_uncer_per_ue_10;
                }
                $exp_uncer_per_ue = max($uncer_arr);
//                echo "<pre>"; print_r($_POST); exit;
                
            } else if($_POST['table_info_id'] == '2'){
                $max_key1 = max(array_keys($_POST['testMeter'][0]));
                $max_key2 = !empty($_POST['testMeter2'][0]) ? max(array_keys($_POST['testMeter2'][0])) : '0';
                $max_key3 = !empty($_POST['testMeter3'][0]) ? max(array_keys($_POST['testMeter3'][0])) : '0';
                $max_key4 = max(array_keys($_POST['stdMeter'][0]));
                $stdmeter_data = $_POST['stdMeter'][0];
                $testmeter1_data = $_POST['testMeter'][0];
                $testmeter2_data = $_POST['testMeter2'][0];
                $testmeter3_data = $_POST['testMeter3'][0];
                $max_key = 0; 
                if(($max_key1 >= $max_key2) && ($max_key1 >= $max_key3) && ($max_key1 >= $max_key4)){
                    $max_key = $max_key1;
                } else if(($max_key2 >= $max_key3) && ($max_key2 >= $max_key1) && ($max_key2 >= $max_key4)){
                    $max_key = $max_key2;
                } else if(($max_key3 >= $max_key1) && ($max_key3 >= $max_key2) && ($max_key3 >= $max_key4)){
                    $max_key = $max_key3;
                } else if(($max_key4 >= $max_key1) && ($max_key4 >= $max_key2) && ($max_key4 >= $max_key3)){
                    $max_key = $max_key4;
                }
                
                $m1_total = 0;
                $m2_total = 0;
                $max_m2_m1_h = 0;
                $max_m1_m3_h = 0;
                $hysteresis_val = '';
                $repetability_val = '';
                $one_avg = '';
                $max_avg = '';
                for($x = 0; $x <= $max_key; $x++) { 
                    $std_mter = (isset($stdmeter_data[$x]) ? number_format((float)$stdmeter_data[$x], $decimal_least, '.', '') : '0');
                    $m1 = (isset($testmeter1_data[$x]) ? number_format((float)$testmeter1_data[$x], $decimal_master, '.', '') : '0');
                    $m2 = (isset($testmeter2_data[$x]) ? number_format((float)$testmeter2_data[$x], $decimal_master, '.', '') : '0');
                    $m3 = (isset($testmeter3_data[$x]) ? number_format((float)$testmeter3_data[$x], $decimal_master, '.', '') : '0');
                    $avg = (($m1 + $m2)/2);
                    if($x == 0){
                        $one_avg = $avg;
                        $max_avg = $avg;
                    }
                    $m1_total = $m1_total + $m1;
                    $m2_total = $m2_total + $m2;
                    $new_max_m2_m1_h = number_format((float)abs($m2 - $m1), $decimal_master, '.', '');
                    if($new_max_m2_m1_h > $max_m2_m1_h){
                        $max_m2_m1_h = $new_max_m2_m1_h;
                    }
                    $new_max_m1_m3_h = number_format((float)$m1 - $m3, $decimal_master, '.', '');
                    if($new_max_m1_m3_h > $max_m1_m3_h){
                        $max_m1_m3_h = $new_max_m1_m3_h;
                    }
                    if($avg > $max_avg){
                        $max_avg = $avg;
                    }
                    $max_m2_m1 = number_format((float)abs($m2 - $m1), 3, '.', '');
                    if($hysteresis_val == ''){
                        $hysteresis_val = $max_m2_m1;
                    } else {
                        if($max_m2_m1 > $hysteresis_val){
                            $hysteresis_val = $max_m2_m1;
                        }
                    }
                    $max_m1_m3 = number_format((float)$m1 - $m3, 3, '.', '');
                    if($repetability_val == ''){
                        $repetability_val = $max_m1_m3;
                    } else {
                        if($max_m1_m3 > $repetability_val){
                            $repetability_val = $max_m1_m3;
                        }
                    }
                } 
                // calculation for Uncertainty (max) : Start //
                    $Q6 = number_format((float)$_POST['uncertinty_from_tracebility'], 4, '.', '');
                    $U5 = number_format((float)$_POST['uncertinty_from_tracebility'] / 2, 4, '.', '');
                    $U5 = number_format((float)$U5, 4, '.', '');
                    
                    $Q8 = number_format((float)$_POST['accuracy_taking_account'], 4, '.', '');
                    $U7 = number_format((float)$_POST['accuracy_taking_account'] / sqrt(3), 4, '.', '');
                    $U7 = number_format((float)$U7, 4, '.', '');
                    
                    $Q10 = number_format((float)$max_m2_m1_h / 2, 4, '.', '');
                    $U9 = number_format((float)($max_m2_m1_h / 2) / sqrt(3), 4, '.', '');
                    $U9 = number_format((float)$U9, 4, '.', '');
                    
                    $Q12 = number_format((float)$max_m1_m3_h / 2, 4, '.', '');
                    $U11 = number_format((float)($max_m1_m3_h / 2) / sqrt(3), 4, '.', '');
                    $U11 = number_format((float)$U11, 4, '.', '');
                    
                    $Q14 = number_format((float)$one_avg / 2, 4, '.', '');
                    $U13 = number_format((float)($one_avg / 2) / sqrt(3), 4, '.', '');
                    $U13 = number_format((float)$U13, 4, '.', '');
                    
                    $Q16 = number_format((float)$_POST['leastCount'] / 2, 4, '.', '');
                    $U15 = number_format((float)($_POST['leastCount'] / 2) / sqrt(3), 4, '.', '');
                    $U15 = number_format((float)$U15, 4, '.', '');
                    
                    $U17 = sqrt(pow($U5,2)+pow($U7,2)+pow($U9,2)+pow($U11,2)+pow($U13,2)+pow($U15,2)); // =SQRT(U5^2+U7^2+U9^2+U11^2+U13^2+U15^2)
                    $U17 = number_format((float)$U17, 4, '.', '');
                    $exp_uncer = number_format((float)$U17, 4, '.', '') * $_POST['covarage_factor']; // =U17*M22
                    $exp_uncer = number_format((float)$exp_uncer, 2, '.', '');
                    $exp_uncer_per_ue = number_format((float)$exp_uncer, 4, '.', '') * 100 / number_format((float)$max_avg, 4, '.', ''); // =U18*100/MAX(G29:G38)
                    $exp_uncer_per_ue = number_format((float)$exp_uncer_per_ue, 2, '.', '');
                    
                    $uncer_master = $U5;
                    $acc_calibration  = $U7;
                    $resolution_new   = $U15;
                    $uncer_of_repeat = $U11;
                    $uncertainty_hysteresis = $U9;
                    $com_uncer        = $U17;
//                    echo $com_uncer; exit;
                    $u1     = $Q6;
                    $u1_val = $U5;
                    $u2     = $Q8;
                    $u2_val = $U7;
                    $u3     = $Q10;
                    $u3_val = $U9;
                    $u4     = $Q12;
                    $u4_val = $U11;
                    $u5     = $Q14;
                    $u5_val = $U13;
                    $u6     = $Q16;
                    $u6_val = $U15;
                    
//                    echo $u1;     echo '<br/>';
//                    echo $u1_val; echo '<br/>';
//                    echo $u2;     echo '<br/>';
//                    echo $u2_val; echo '<br/>';
//                    echo $u3;     echo '<br/>';
//                    echo $u3_val; echo '<br/>';
//                    echo $u4;     echo '<br/>';
//                    echo $u4_val; echo '<br/>';
//                    echo $u5;     echo '<br/>';
//                    echo $u5_val; echo '<br/>';
//                    echo $u6;     echo '<br/>';
//                    echo $u6_val; echo '<br/>';
//                    echo $U17; echo '<br/>'; 
//                    echo $exp_uncer; echo '<br/>';
//                    echo $exp_uncer_per_ue; echo '<br/>'; exit;
                    
                // calculation for Uncertainty (max) : End //
                    
                    
                $accuracy_fs      = $_POST['accuracy_of_fs'];
                $accuracy_rdg     = $_POST['accuracy_of_rdg'];
                $master_range     = $master_range;
//                $un_cali_certi    = $_POST['uncertinty_from_tracebility'];
//                $stdmeter_avg_new = $max_avg;
//                $r_phase_accuracy = '0';
//                $uuc_least_count  = $_POST['leastCount'];
//                $std_dev          = '0';
//                $std_uncer        = '0';
//                $std_uncer_per    = '0';
//                $uncer_master     = $U5;
//                $acc_calibration  = $U7;
//                $resolution_new   = ($uuc_least_count/2)/sqrt(3);
//                $com_uncer        = sqrt(($std_uncer*$std_uncer)+($acc_calibration*$acc_calibration)+($uncer_master*$uncer_master)+($resolution_new*$resolution_new));
//                $exp_uncer        = 2*$com_uncer;
//                $exp_uncer_per_ue = $exp_uncer*100/$stdmeter_avg_new;
                
            } else if($_POST['table_info_id'] == '16'){
                $stdmeter_data = $_POST['stdMeter'][0];
                $testmeter1_data = $_POST['testMeter'][0];
                $testmeter2_data = isset($_POST['testMeter2'][0]) ? $_POST['testMeter2'][0] : array();
                $testmeter3_data = isset($_POST['testMeter3'][0]) ? $_POST['testMeter3'][0] : array();
                $testmeter4_data = isset($_POST['testMeter4'][0]) ? $_POST['testMeter4'][0] : array();
                $testmeter5_data = isset($_POST['testMeter5'][0]) ? $_POST['testMeter5'][0] : array();
                $testmeter6_data = isset($_POST['testMeter6'][0]) ? $_POST['testMeter6'][0] : array();
                $max_key1 = max(array_keys($testmeter1_data));
                $max_key2 = !empty($testmeter2_data) ? max(array_keys($testmeter2_data)) : '0';
                $max_key3 = !empty($testmeter3_data) ? max(array_keys($testmeter3_data)) : '0';
                $max_key4 = !empty($testmeter4_data) ? max(array_keys($testmeter4_data)) : '0';
                $max_key5 = !empty($testmeter5_data) ? max(array_keys($testmeter5_data)) : '0';
                $max_key6 = !empty($testmeter6_data) ? max(array_keys($testmeter6_data)) : '0';
                $max_key7 = max(array_keys($stdmeter_data));
                $max_key = 0; 
                if(($max_key1 >= $max_key2) && ($max_key1 >= $max_key3) && ($max_key1 >= $max_key4) && ($max_key1 >= $max_key5) && ($max_key1 >= $max_key6) && ($max_key1 >= $max_key7)){
                    $max_key = $max_key1;
                } else if(($max_key2 >= $max_key3) && ($max_key2 >= $max_key1) && ($max_key2 >= $max_key4) && ($max_key2 >= $max_key5) && ($max_key2 >= $max_key6) && ($max_key2 >= $max_key7)){
                    $max_key = $max_key2;
                } else if(($max_key3 >= $max_key1) && ($max_key3 >= $max_key2) && ($max_key3 >= $max_key4) && ($max_key3 >= $max_key5) && ($max_key3 >= $max_key6) && ($max_key3 >= $max_key7)){
                    $max_key = $max_key3;
                } else if(($max_key4 >= $max_key1) && ($max_key4 >= $max_key2) && ($max_key4 >= $max_key3) && ($max_key4 >= $max_key5) && ($max_key4 >= $max_key6) && ($max_key4 >= $max_key7)){
                    $max_key = $max_key4;
                } else if(($max_key5 >= $max_key1) && ($max_key5 >= $max_key2) && ($max_key5 >= $max_key3) && ($max_key5 >= $max_key4) && ($max_key5 >= $max_key6) && ($max_key5 >= $max_key7)){
                    $max_key = $max_key5;
                } else if(($max_key6 >= $max_key1) && ($max_key6 >= $max_key2) && ($max_key6 >= $max_key3) && ($max_key6 >= $max_key4) && ($max_key6 >= $max_key5) && ($max_key6 >= $max_key7)){
                    $max_key = $max_key6;
                } else if(($max_key7 >= $max_key1) && ($max_key7 >= $max_key2) && ($max_key7 >= $max_key3) && ($max_key7 >= $max_key4) && ($max_key7 >= $max_key5) && ($max_key7 >= $max_key6)){
                    $max_key = $max_key7;
                }
                $max_hyp = '';
                $max_repeat = '';
                $first_avg = 0;
                $max_avg = '';
                for($x = 0; $x <= $max_key; $x++) { 
                    $std_mter = (isset($stdmeter_data[$x]) ? number_format((float)$stdmeter_data[$x], $decimal_least, '.', '') : '0'); 
                    $m1 = (isset($testmeter1_data[$x]) ? number_format((float)$testmeter1_data[$x], $decimal_master, '.', '') : '0'); 
                    $m2 = (isset($testmeter2_data[$x]) ? number_format((float)$testmeter2_data[$x], $decimal_master, '.', '') : '0'); 
                    $m3 = (isset($testmeter3_data[$x]) ? number_format((float)$testmeter3_data[$x], $decimal_master, '.', '') : '0'); 
                    $m4 = (isset($testmeter4_data[$x]) ? number_format((float)$testmeter4_data[$x], $decimal_master, '.', '') : '0'); 
                    $m5 = (isset($testmeter5_data[$x]) ? number_format((float)$testmeter5_data[$x], $decimal_master, '.', '') : '0'); 
                    $m6 = (isset($testmeter6_data[$x]) ? number_format((float)$testmeter6_data[$x], $decimal_master, '.', '') : '0'); 
                    if($x == 0){
                        $first_avg = ((($m1+$m3+$m5)/3+($m2+$m4+$m6)/3)/2);
                        $hyp = ((abs(($m2-$m1)-($m1-$m1)))+(abs(($m4-$m3)-($m3-$m3)))+(abs(($m6-$m5)-($m5-$m5)))/3);
                    }
                    $av = ((($m1+$m3+$m5)/3+($m2+$m4+$m6)/3)/2);
                    if($max_avg == ''){
                        $max_avg = $av;
                    } else {
                        if($av > $max_avg){
                            $max_avg = $av;
                        }
                    }
//                    echo "M ".$max_avg; echo '<br/>';
//                    if($x != 0){
                        $m1_old = (isset($testmeter1_data[0]) ? number_format((float)$testmeter1_data[0], $decimal_master, '.', '') : '0'); 
                        $m2_old = (isset($testmeter2_data[0]) ? number_format((float)$testmeter2_data[0], $decimal_master, '.', '') : '0'); 
                        $m3_old = (isset($testmeter3_data[0]) ? number_format((float)$testmeter3_data[0], $decimal_master, '.', '') : '0'); 
                        $m4_old = (isset($testmeter4_data[0]) ? number_format((float)$testmeter4_data[0], $decimal_master, '.', '') : '0'); 
                        $m5_old = (isset($testmeter5_data[0]) ? number_format((float)$testmeter5_data[0], $decimal_master, '.', '') : '0'); 
                        $m6_old = (isset($testmeter6_data[0]) ? number_format((float)$testmeter6_data[0], $decimal_master, '.', '') : '0'); 
                        $avg = ((($m1+$m3+$m5)/3+($m2+$m4+$m6)/3)/2); 
                        $hyp = ((abs(($m2-$m1_old)-($m1-$m1_old)))+(abs(($m4-$m3_old)-($m3-$m3_old)))+(abs(($m6-$m5_old)-($m5-$m5_old)))/3);
                        $array_1 = array();
                        $array_1[] = number_format((float)(abs(($m3 - $m3_old)) - ($m1 - $m1_old)), 3, '.', '');
                        $array_1[] = number_format((float)(abs(($m4 - $m4_old)) - ($m2 - $m2_old)), 3, '.', '');
                        $array_1[] = number_format((float)(abs(($m5 - $m5_old)) - ($m1 - $m1_old)), 3, '.', '');
                        $array_1[] = number_format((float)(abs(($m6 - $m6_old) - ($m2 - $m2_old))), 3, '.', '');
//                        echo $m6."**".$m6_old."**".$m2."**".$m2_old."**<br/>";
//                        echo "<pre>"; print_r($array_1);
//                        =(MAX((IMABS(E37-$E$29)-(C37-$C$29))));
//                        =(MAX((IMABS((F37-$F$29)-(D37-$D$29))));
//                        =(MAX((IMABS((G37-$G$29)-(C37-$C$29)))));
//                        =(MAX((IMABS((H37-$H$29)-(D37-$D$29)))));
//                        =(MAX((IMABS(E37-$E$29)-(C37-$C$29)),IMABS((F37-$F$29)-(D37-$D$29)),(IMABS(G37-$G$29)-(C37-$C$29)),(IMABS((H37-$H$29)-(D37-$D$29)))))
                        $max_arr = max($array_1);
                        $max_arr = number_format((float)$max_arr, 3, '.', '');
//                        echo $max_arr;
                        if($max_repeat == ''){
                            $max_repeat = $max_arr;
                        } else {
                            if($max_arr > $max_repeat){
                                $max_repeat = $max_arr;
                            }
                        }
//                    }
                    if($max_hyp == ''){
                        $max_hyp = $hyp;
                    } else {
                        if($hyp > $max_hyp){
                            $max_hyp = $hyp;
                        }
                    }
                    
                }
                if(!empty($max_hyp)){
                    $max_hyp = number_format((float)$max_hyp, 4, '.', '');
                }
                if(!empty($max_repeat)){
                    $max_repeat = number_format((float)$max_repeat, 4, '.', '');
                }
                if(!empty($first_avg)){
                    $first_avg = number_format((float)$first_avg, $decimal_master, '.', '');
                }
                if(!empty($max_avg)){
                    $max_avg = number_format((float)$max_avg, $decimal_master, '.', '');
                }
                
                $u1     = number_format((float)$_POST['uncertinty_from_tracebility'], 4, '.', '');
                $u1_val = $u1/2;
                $u1_val = number_format((float)$u1_val, 4, '.', '');
                
                $u2     = number_format((float)$_POST['accuracy_taking_account'], 4, '.', '');
                $u2_val = $u2/sqrt(3);
                $u2_val = number_format((float)$u2_val, 4, '.', '');
                
                $u3     = number_format((float)$max_hyp/2, 4, '.', '');
                $u3_val = $u3/sqrt(3);
                $u3_val = number_format((float)$u3_val, 4, '.', '');
                
                $u4     = number_format((float)$max_repeat/2, 4, '.', '');
                $u4_val = $u4/sqrt(3);
                $u4_val = number_format((float)$u4_val, 4, '.', '');
                
                $u5     = 0;
                if($first_avg != 0){
                    $u5 = $first_avg/2;
                }
                $u5 = number_format((float)$u5, 4, '.', '');
                $u5_val = 0;
                if($first_avg != 0){
                    $u5_val = $u5/sqrt(3);
                }
                $u5_val = number_format((float)$u5_val, 4, '.', '');
                
                $uuc_least_count  = $_POST['leastCount'];
                
                $u6        = number_format((float)$uuc_least_count/2, 4, '.', '');
                $u6_val    = $u6/sqrt(3);
                $u6_val = number_format((float)$u6_val, 4, '.', '');
                
                $com_uncer = sqrt(pow($u1_val,2)+pow($u2_val,2)+pow($u3_val,2)+pow($u4_val,2)+pow($u5_val,2)+pow($u6_val,2));
                $com_uncer = number_format((float)$com_uncer, 4, '.', '');
                
                $exp_uncer = $com_uncer*$_POST['covarage_factor'];
                $exp_uncer = number_format((float)$exp_uncer, 2, '.', '');
                if(!empty($max_avg)){
                    $exp_uncer_per_ue = $exp_uncer*100/$max_avg;
                } else {
                    $exp_uncer_per_ue = $exp_uncer*100;
                }
                $exp_uncer_per_ue           = number_format((float)$exp_uncer_per_ue, 2, '.', '');
                $uncer_master               = $u1_val;
                $acc_calibration            = $u2_val;
                $resolution_new             = $u6_val;
                $uncer_of_repeat            = $u4_val;
                $uncertainty_hysteresis     = $u3_val;
                $uncer_of_repeat_max        = $max_repeat;
                $uncertainty_hysteresis_max = $max_hyp;
//                echo $u1;     echo ' **** ';
//                echo $u1_val; echo '<br/>';
//                echo $u2;     echo ' **** ';
//                echo $u2_val; echo '<br/>';
//                echo $u3;     echo ' **** ';
//                echo $u3_val; echo '<br/>';
//                echo $u4;     echo ' **** ';
//                echo $u4_val; echo '<br/>';
//                echo $u5;     echo ' **** ';
//                echo $u5_val; echo '<br/>';
//                echo $u6;     echo ' **** ';
//                echo $u6_val; echo '<br/>';
//                echo $com_uncer; echo '<br/>';
//                echo $exp_uncer; echo '<br/>';
//                echo $exp_uncer_per_ue; echo '<br/>';
//                echo $uncertainty_hysteresis_max; echo '<br/>';
//                echo $uncer_of_repeat_max; echo '<br/>'; exit;
                
            } else if($_POST['table_info_id'] == '11'){
                $accuracy_fs      = $_POST['accuracy_of_fs'];
                $accuracy_rdg     = $_POST['accuracy_of_rdg'];
                $master_range     = $master_range;
                $un_cali_certi    = $_POST['uncertinty_from_tracebility'];
                $stdmeter_avg_new = isset($_POST['stdMeter']) && !empty($_POST['stdMeter']) ? stdmeter_avg($_POST['stdMeter'][0]) : 0;
                if(!empty($_POST['accuracy_taking_account'])){
                    $r_phase_accuracy = $_POST['accuracy_taking_account'];
                } else {
                    $r_phase_accuracy = (((($stdmeter_avg_new*$accuracy_rdg)+($master_range*$accuracy_fs))/100)*100)/$stdmeter_avg_new;
//                    $r_phase_accuracy = (((($stdmeter_avg_new*$accuracy_rdg)+($master_range*$accuracy_fs))/100));
                }
//                $r_phase_accuracy = $r_phase_accuracy_cal*100/$stdmeter_avg_new;
                $r_phase_accuracy = number_format((float)$r_phase_accuracy, 4, '.', '');
                $uuc_least_count  = $_POST['leastCount'];
                $std_dev          = standardDeviation_val($_POST['stdMeter'][0], $stdmeter_avg_new);
                $std_uncer        = $std_dev/sqrt(5);
                $std_uncer_per    = $std_uncer*100/$stdmeter_avg_new;
                
                $uncer_master     = ($un_cali_certi/2)*$stdmeter_avg_new*0.01;
                $acc_calibration  = (($r_phase_accuracy*$stdmeter_avg_new*0.01/sqrt(3)));
                $resolution_new   = ($uuc_least_count/2)/sqrt(3);
                $uncer_of_repeat  = $_POST['repeatibility']*$stdmeter_avg_new*0.01/sqrt(3);
                $uncer_of_install_error = (($_POST['sensor']*$stdmeter_avg_new*0.01/sqrt(3)));
                $com_uncer        = sqrt(($std_uncer*$std_uncer)+($acc_calibration*$acc_calibration)+($uncer_master*$uncer_master)+($resolution_new*$resolution_new)+($uncer_of_repeat*$uncer_of_repeat)+($uncer_of_install_error*$uncer_of_install_error));
                $exp_uncer        = 2*$com_uncer;
                $exp_uncer_per_ue = $exp_uncer*100/$stdmeter_avg_new;
                
            } else if($_POST['table_info_id'] == '4'){
                $accuracy_fs      = $_POST['accuracy_of_fs'];
                $accuracy_rdg     = $_POST['accuracy_of_rdg'];
                $accuracy_fs_2      = $_POST['accuracy_of_fs_2'];
                $accuracy_rdg_2     = $_POST['accuracy_of_rdg_2'];
                $master_range     = $master_range;
                $master_range_2     = $master_range_2;
                $un_cali_certi    = $_POST['uncertinty_from_tracebility'];
                $un_cali_certi_2    = $_POST['uncertinty_from_tracebility_2'];
                
                $stdmeter_avg_new = isset($_POST['stdMeter']) && !empty($_POST['stdMeter']) ? stdmeter_avg($_POST['stdMeter'][0]) : 0;
                if(!empty($_POST['accuracy_taking_account'])){
                    $r_phase_accuracy = $_POST['accuracy_taking_account'];
                } else {
                    $r_phase_accuracy = (((($stdmeter_avg_new*$accuracy_rdg)+($master_range*$accuracy_fs))/100)*100)/$stdmeter_avg_new;
//                    $r_phase_accuracy = (((($stdmeter_avg_new*$accuracy_rdg)+($master_range*$accuracy_fs))/100));
                }
                $r_phase_accuracy = number_format((float)$r_phase_accuracy, 4, '.', '');
                if(!empty($_POST['accuracy_taking_account_2'])){
                    $r_phase_accuracy_2 = $_POST['accuracy_taking_account_2'];
                } else {
                    $r_phase_accuracy_2 = (((($stdmeter_avg_new*$accuracy_rdg_2)+($master_range_2*$accuracy_fs_2))/100)*100)/$stdmeter_avg_new;
//                    $r_phase_accuracy_2 = (((($stdmeter_avg_new*$accuracy_rdg_2)+($master_range_2*$accuracy_fs_2))/100));
                }
                $r_phase_accuracy_2 = number_format((float)$r_phase_accuracy_2, 4, '.', '');
                $uuc_least_count  = $_POST['leastCount'];
                $std_dev          = standardDeviation_val($_POST['stdMeter'][0], $stdmeter_avg_new);
//                print_r($std_dev); exit;
                $std_dev = number_format((float)$std_dev, 4, '.', '');
                
                $std_uncer        = standardUncertinity_val($_POST['stdMeter'][0], $std_dev);
                $std_uncer        = number_format((float)$std_uncer, 4, '.', '');
                
                $std_uncer_per    = $std_uncer*100/$stdmeter_avg_new;
                $std_uncer_per    = number_format((float)$std_uncer_per, 4, '.', '');
                
                $uncer_master     = ($un_cali_certi/2)*$stdmeter_avg_new*0.01;
                $uncer_master_2     = ($un_cali_certi_2/2)*$stdmeter_avg_new*0.01;
                
                $acc_calibration  = (($r_phase_accuracy*$stdmeter_avg_new*0.01/sqrt(3)));
                $acc_calibration_2  = (($r_phase_accuracy_2*$stdmeter_avg_new*0.01/sqrt(3)));
                
                $resolution_new   = ($uuc_least_count/2)/sqrt(3);
                
                $com_uncer        = sqrt(($std_uncer*$std_uncer)+($acc_calibration*$acc_calibration)+($uncer_master*$uncer_master)+($uncer_master_2*$uncer_master_2)+($acc_calibration_2*$acc_calibration_2));
                
                $exp_uncer        = 2*$com_uncer;
                $exp_uncer_per_ue = $exp_uncer*100/$stdmeter_avg_new;
                
            } else {
                
                $accuracy_fs      = $_POST['accuracy_of_fs'];
                $accuracy_rdg     = $_POST['accuracy_of_rdg'];
                $master_range     = $master_range;
                $un_cali_certi    = $_POST['uncertinty_from_tracebility'];
                $stdmeter_avg_new = isset($_POST['stdMeter']) && !empty($_POST['stdMeter']) ? stdmeter_avg($_POST['stdMeter'][0]) : 0;
                if(!empty($_POST['accuracy_taking_account'])){
                    $r_phase_accuracy = $_POST['accuracy_taking_account'];
                } else {
                    $r_phase_accuracy = (((($stdmeter_avg_new*$accuracy_rdg)+($master_range*$accuracy_fs))/100)*100)/$stdmeter_avg_new;
//                    $r_phase_accuracy = (((($stdmeter_avg_new*$accuracy_rdg)+($master_range*$accuracy_fs))/100));
                }
                $r_phase_accuracy = number_format((float)$r_phase_accuracy, 4, '.', '');
//                $r_phase_accuracy = $r_phase_accuracy_cal*100/$stdmeter_avg_new;
                $uuc_least_count  = $_POST['leastCount'];
//                print_r($_POST['stdMeter'][0]); exit;
                if($_POST['table_info_id'] == '19'){
                    $std_dev          = standardDeviation_val_for_table_18($_POST['stdMeter'][0], $stdmeter_avg_new);
                } else {
                    $std_dev          = standardDeviation_val($_POST['stdMeter'][0], $stdmeter_avg_new);
                }
//                print_r($std_dev); exit;
                $std_dev = number_format((float)$std_dev, 4, '.', '');
                $std_uncer        = standardUncertinity_val($_POST['stdMeter'][0], $std_dev);
                $std_uncer        = number_format((float)$std_uncer, 4, '.', '');
                $std_uncer_per    = $std_uncer*100/$stdmeter_avg_new;
                $std_uncer_per    = number_format((float)$std_uncer_per, 4, '.', '');
                if($_POST['table_info_id'] == '19'){
                    $uncer_master     = ($un_cali_certi/2);
                } else if(($_POST['table_info_id'] == '5') && isset($_POST['consider_ue'])){
                    $uncer_master     = ($un_cali_certi/2);
                } else {
                    $uncer_master     = ($un_cali_certi/2)*$stdmeter_avg_new*0.01;
                }
                if($_POST['table_info_id'] == '19'){
                    $acc_calibration  = (($r_phase_accuracy/sqrt(3)));
                } else {
                    $acc_calibration  = (($r_phase_accuracy*$stdmeter_avg_new*0.01/sqrt(3)));
                }
                $resolution_new   = ($uuc_least_count/2)/sqrt(3);
                
                if($_POST['table_info_id'] == '19'){
                    $com_uncer        = sqrt(($std_uncer*$std_uncer)+($uncer_master*$uncer_master)+($acc_calibration*$acc_calibration)+($resolution_new*$resolution_new));
                } else {
                    $com_uncer        = sqrt(($std_uncer*$std_uncer)+($acc_calibration*$acc_calibration)+($uncer_master*$uncer_master)+($resolution_new*$resolution_new));
                }
//                echo $std_uncer."**".$acc_calibration."**".$uncer_master."**".$resolution_new."**<br/>"; exit;
                $exp_uncer        = 2*$com_uncer;
                if(($_POST['table_info_id'] == '5') && isset($_POST['consider_ue'])){
                    $exp_uncer_per_ue = $exp_uncer;
                } else {
                    $exp_uncer_per_ue = $exp_uncer*100/$stdmeter_avg_new;
                }
            
            }
            $un_cali_certi    = $_POST['uncertinty_from_tracebility'];
            $exp_uncer_per_ue_without_cmc = $exp_uncer_per_ue;
            if($_POST['table_info_id'] == '19'){
                $exp_uncer_per_ue = $exp_uncer_per_ue;
            } else {
                $cmc = $m_cmc;
                if($m_cmc > $exp_uncer_per_ue){
                    $exp_uncer_per_ue = $m_cmc;
                }
            }
            // NEW CALCULATION OF UNCERTAINTY : End : // 
            
            
            
            
            $testmeter_avg = 0;
            $testmeter2_avg = 0;
            $testmeter3_avg = 0;
            if(isset($_POST['testMeter'])) {
                if(isset($_POST['testMeter'][0])) {
                    $testmeter_avg = testmeter_avg($_POST['testMeter'][0]);
                }
            }
            if(isset($_POST['testMeter2'])) {
                if(isset($_POST['testMeter2'][0])) {
                    $testmeter2_avg = testmeter_avg($_POST['testMeter2'][0]);
                }
            }
            if(isset($_POST['testMeter3'])) {
                if(isset($_POST['testMeter3'][0])) {
                    $testmeter3_avg = testmeter_avg($_POST['testMeter3'][0]);
                }
            }
            if($_POST['table_info_id'] == '12'){
                $testMeter_avg = isset($_POST['testMeter']) && !empty($_POST['testMeter']) ? stdmeter_avg($_POST['testMeter'][0]) : 0;
                $testMeter_avg2 = stdmeter_avg($_POST['testMeter2'][0]);
                $testMeter_avg3 = stdmeter_avg($_POST['testMeter3'][0]);
                $testMeter_avg4 = stdmeter_avg($_POST['testMeter4'][0]);
                $testMeter_avg5 = stdmeter_avg($_POST['testMeter5'][0]);
                $testMeter_avg6 = isset($_POST['testMeter6']) && !empty($_POST['testMeter6']) ? stdmeter_avg($_POST['testMeter6'][0]) : 0;
                $testMeter_avg7 = stdmeter_avg($_POST['testMeter7'][0]);
                $testMeter_avg8 = stdmeter_avg($_POST['testMeter8'][0]);
                $testMeter_avg9 = stdmeter_avg($_POST['testMeter9'][0]);
                $testMeter_avg10 = stdmeter_avg($_POST['testMeter10'][0]);
            } else {
                $stdmeter_avg = stdmeter_avg($_POST['stdMeter'][0]);
                $standardDeviation = standardDeviation_val($_POST['stdMeter'][0], $stdmeter_avg);
                $standardUncertinity = standardUncertinity_val($_POST['stdMeter'][0], $standardDeviation);
                $standardUncertinityperc = ($standardUncertinity*100/$stdmeter_avg);
                $uncertinityForTypeB = (($_POST['uncertinty_from_tracebility'] / 2)*$stdmeter_avg*0.01);
                $accuracy_taking_account = $_POST['accuracy_taking_account'];
                $accuracyForTypeB = ($accuracy_taking_account * $stdmeter_avg * 0.01 / sqrt(3));
                $resolutionTypeB  = (($_POST['resolution_of_master'] / 2)/sqrt(3));
                $stability_of_source  = ($_POST['master_stability'] * $stdmeter_avg)*0.01/sqrt(3);
                if($_POST['table_info_id'] == '19'){
                    $uncer_of_repeat = $stability_of_source;
                }
                
                if($_POST['table_info_id'] == '11'){
                    $unc_of_repeat = ($_POST['repeatibility'] * $stdmeter_avg * 0.01 / sqrt(3));
                    $unc_of_install = ($_POST['sensor'] * $stdmeter_avg * 0.01 / sqrt(3));
                    $uncertainty_of_master = ($_POST['uncertainty_calibration'] / 2);
                    $combinedUncertinity = (sqrt(($standardUncertinity * $standardUncertinity) + ($accuracyForTypeB * $accuracyForTypeB) + ($uncertainty_of_master * $uncertainty_of_master) + ($resolutionTypeB * $resolutionTypeB) + ($unc_of_repeat * $unc_of_repeat) + ($unc_of_install * $unc_of_install)));
                    $expandedUncertinity = (2*$combinedUncertinity);
                    $expandedUncertinityInPre = ($expandedUncertinity * 100 / $stdmeter_avg);
                } else if($_POST['table_info_id'] == '13'){
                    $uncertainty_of_master = ($_POST['uncertainty_calibration'] / 2);
                    $reslution = (($_POST['resolution_of_master'] / 2)/sqrt(3));
                    $combinedUncertinity = (sqrt(($standardUncertinity * $standardUncertinity) + ($accuracyForTypeB * $accuracyForTypeB) + ($uncertainty_of_master * $uncertainty_of_master) + ($reslution * $reslution)));
                    $expandedUncertinity = (2*$combinedUncertinity);
                    $expandedUncertinityInPre = ($expandedUncertinity * 100 / $stdmeter_avg);
                } else {
                    $combinedUncertinity = (sqrt(($standardUncertinity * $standardUncertinity) + ($accuracyForTypeB * $accuracyForTypeB) + ($uncertinityForTypeB * $uncertinityForTypeB) + ($resolutionTypeB * $resolutionTypeB)));
                    $expandedUncertinity = (2*$combinedUncertinity);
                    $expandedUncertinityInPre = ($expandedUncertinity * 100 / $stdmeter_avg);
                }
//                echo "<pre>"; print_r($_POST); exit;
                $meanReading = $stdmeter_avg;
            }
            // INSERT INTO JSON DATA : START
            if(isset($_POST['obs_data_id']) && !empty($_POST['obs_data_id'])){
                $selectobs = "SELECT * FROM observation_data
                              WHERE id = ".$_POST['obs_data_id']."  ";
                $selectobsResult = mysql_query($selectobs);
            } else {
                $selectobs = "SELECT * FROM observation_data
                              WHERE grn_id = ".$_POST['grnId']."
                              AND grn_detail_id = ".$_POST['grnDetailId']."
                              AND table_no = ".$_POST['table_no']."
                              AND table_info_id = ".$_POST['table_info_id']." ";
                $selectobsResult = mysql_query($selectobs);
            }
            $old_table_id = '';
            while ($selectobsRow = mysql_fetch_array($selectobsResult)) {
                $old_table_id = $selectobsRow['id'];
                $old_master_json = $selectobsRow['master_json'];
                $old_uuc_json = $selectobsRow['uuc_json'];
                $old_reading_json = $selectobsRow['reading_json'];
                $old_type_a_type_b_json = $selectobsRow['type_a_type_b_json'];
            }
//             echo "<pre>"; print_r($old_table_id); exit;
            $master_json = array();
            $master_json['consider_ue'] = isset($_POST['consider_ue']) ? '1' : '';
            $master_json['ue_unit'] = isset($_POST['ue_unit']) ? $_POST['ue_unit'] : '';
            $master_json['masterMeterId'] = $_POST['masterMeterId'];
            $master_json['masterMeterSubId'] = $_POST['masterMeterSubId'];
            $master_json['masterMeterSubSubId'] = $_POST['masterMeterSubSubId'];
            $master_json['mParameterId'] = $_POST['masterMeterSubId'];
            $master_json['mRangeId'] = $_POST['masterMeterSubSubId'];
            
            $master_json['masterMeterId_2'] = isset($_POST['masterMeterId_2']) ? $_POST['masterMeterId_2'] : '';
            $master_json['masterMeterSubId_2'] = isset($_POST['masterMeterSubId_2']) ? $_POST['masterMeterSubId_2'] : '';
            $master_json['masterMeterSubSubId_2'] = isset($_POST['masterMeterSubSubId_2']) ? $_POST['masterMeterSubSubId_2'] : '';
            $master_json['mParameterId_2'] = isset($_POST['masterMeterSubId_2']) ? $_POST['masterMeterSubId_2'] : '';
            $master_json['mRangeId_2'] = isset($_POST['masterMeterSubSubId_2']) ? $_POST['masterMeterSubSubId_2'] : '';
            
            $master_json['masterMeterId_3'] = isset($_POST['masterMeterId_3']) ? $_POST['masterMeterId_3'] : '';
            $master_json['masterMeterSubId_3'] = isset($_POST['masterMeterSubId_3']) ? $_POST['masterMeterSubId_3'] : '';
            $master_json['masterMeterSubSubId_3'] = isset($_POST['masterMeterSubSubId_3']) ? $_POST['masterMeterSubSubId_3'] : '';
            $master_json['mParameterId_3'] = isset($_POST['masterMeterSubId_3']) ? $_POST['masterMeterSubId_3'] : '';
            $master_json['mRangeId_3'] = isset($_POST['masterMeterSubSubId_3']) ? $_POST['masterMeterSubSubId_3'] : '';
            
            $master_json['callibrationDate'] = ($_POST['callibrationDateYear'] . "-" . $_POST['callibrationDateMonth'] . "-" . $_POST['callibrationDateDay']);
            $master_json['nextYearDate'] = ($_POST['nextYearDateYear'] . "-" . $_POST['nextYearDateMonth'] . "-" . $_POST['nextYearDateDay']);
            $master_json['certificateIssue'] = isset($_POST['certificateIssue']) ? "Y" : "N";
            $master_json['certificateIssueDate'] = ($_POST['certificateIssueDateYear'] . "-" . $_POST['certificateIssueDateMonth'] . "-" . $_POST['certificateIssueDateDay']);
            $master_json['accuracy_of_rdg'] = $_POST['accuracy_of_rdg'];
            $master_json['accuracy_of_fs'] = $_POST['accuracy_of_fs'];
            $master_json['master_stability'] = $_POST['master_stability'];
            $master_json['resolution_of_master'] = $_POST['resolution_of_master'];
            $master_json['degree_of_freedom'] = $_POST['degree_of_freedom'];
            $master_json['uncertinty_from_tracebility'] = $_POST['uncertinty_from_tracebility'];
            $master_json['accuracy_taking_account'] = $_POST['accuracy_taking_account'];
            $master_json['temperature'] = $_POST['temperature'];
            $master_json['humidity'] = $_POST['humidity'];
            
            $master_json['accuracy_of_rdg_2'] = isset($_POST['accuracy_of_rdg_2']) ? $_POST['accuracy_of_rdg_2'] : '';
            $master_json['accuracy_of_fs_2'] = isset($_POST['accuracy_of_fs_2']) ? $_POST['accuracy_of_fs_2'] : '';
            $master_json['master_stability_2'] = isset($_POST['master_stability_2']) ? $_POST['master_stability_2'] : '';
            $master_json['resolution_of_master_2'] = isset($_POST['resolution_of_master_2']) ? $_POST['resolution_of_master_2'] : '';
            $master_json['degree_of_freedom_2'] = isset($_POST['degree_of_freedom_2']) ? $_POST['degree_of_freedom_2'] : '';
            $master_json['uncertinty_from_tracebility_2'] = isset($_POST['uncertinty_from_tracebility_2']) ? $_POST['uncertinty_from_tracebility_2'] : '';
            $master_json['accuracy_taking_account_2'] = isset($_POST['accuracy_taking_account_2']) ? $_POST['accuracy_taking_account_2'] : '';
            
            $master_json['accuracy_of_rdg_3'] = isset($_POST['accuracy_of_rdg_3']) ? $_POST['accuracy_of_rdg_3'] : '';
            $master_json['accuracy_of_fs_3'] = isset($_POST['accuracy_of_fs_3']) ? $_POST['accuracy_of_fs_3'] : '';
            $master_json['master_stability_3'] = isset($_POST['master_stability_3']) ? $_POST['master_stability_3'] : '';
            $master_json['resolution_of_master_3'] = isset($_POST['resolution_of_master_3']) ? $_POST['resolution_of_master_3'] : '';
            $master_json['degree_of_freedom_3'] = isset($_POST['degree_of_freedom_3']) ? $_POST['degree_of_freedom_3'] : '';
            $master_json['uncertinty_from_tracebility_3'] = isset($_POST['uncertinty_from_tracebility_3']) ? $_POST['uncertinty_from_tracebility_3'] : '';
            $master_json['accuracy_taking_account_3'] = isset($_POST['accuracy_taking_account_3']) ? $_POST['accuracy_taking_account_3'] : '';
            
            if(!empty($old_table_id)) {
                $decode_old_master_json = json_decode($old_master_json);
                $decode_old_master_json = json_decode(json_encode($decode_old_master_json), true);
                if(isset($_POST['obs_data_id']) && !empty($_POST['obs_data_id'])){
                    $decode_old_master_json[$_POST['obs_data_key']] = $master_json;
                } else {
                    $decode_old_master_json[] = $master_json;
                }
//                $master_json_data = json_encode($decode_old_master_json, JSON_UNESCAPED_UNICODE);
                $master_json_data =  preg_replace('/\\\\u([a-f0-9]{4})/e', "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($decode_old_master_json));
//                $master_json_data = json_encode($decode_old_master_json);
            } else {
                $master_json_arr = array();
                $master_json_arr[0] = $master_json;
                $master_json_data = preg_replace('/\\\\u([a-f0-9]{4})/e', "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($master_json_arr));
            }

//            $selectm = "SELECT * FROM mastermetersubsub
//                         WHERE masterMeterSubSubId = ".$_POST['masterMeterSubSubId']."  ";
//            $selectmResult = mysql_query($selectm);
            $table_title_new = '';

//            while ($selectmRow = mysql_fetch_array($selectmResult)){
//                $table_title_new = $_POST['range'].' '.$selectmRow['range_unit'];
//            }
            $uuc_json = array();
            $uuc_json['custCode'] = $_POST['custCode'];
            $uuc_json['itemCode'] = $_POST['itemCode'];
            $uuc_json['phase_type_id'] = $_POST['phase_type_id'];
            $uuc_json['table_no'] = $_POST['table_no'];
            $uuc_json['table_title'] = $table_title_new;
            $uuc_json['table_title_right'] = $_POST['table_title_right'];
            $uuc_json['selfCertiNo'] = $_POST['selfCertiNo'];
            $uuc_json['confidence_level'] = $_POST['confidence_level'];
            $uuc_json['covarage_factor'] = $_POST['covarage_factor'];
            $uuc_json['leastCount'] = $_POST['leastCount'];
            $uuc_json['leastCount_print'] = $_POST['leastCount_print'];
            $uuc_json['accuracy'] = $_POST['accuracy'];
            $uuc_json['certiRemarks'] = $_POST['certiRemarks'];
            $uuc_json['makeModel'] = $_POST['makeModel'];
            $uuc_json['instrumentId'] = $_POST['instrumentId'];
            $uuc_json['obs_type_id'] = isset($_POST['obs_type_id']) ? $_POST['obs_type_id'] : '';
            $uuc_json['weight_id'] = isset($_POST['weight_id']) ? $_POST['weight_id'] : '';
            $uuc_json['d1'] = isset($_POST['d1']) ? $_POST['d1'] : '';
            $uuc_json['d2'] = isset($_POST['d2']) ? $_POST['d2'] : '';
            $uuc_json['frequency'] = isset($_POST['frequency']) ? $_POST['frequency'] : '';
            $uuc_json['rpm'] = isset($_POST['rpm']) ? $_POST['rpm'] : '';
            $uuc_json['ocillation'] = isset($_POST['ocillation']) ? $_POST['ocillation'] : '';
            $uuc_json['col_head_1_least_count'] = isset($_POST['col_head_1_least_count']) ? $_POST['col_head_1_least_count'] : '';
            $uuc_json['least_count_for_rpm'] = isset($_POST['least_count_for_rpm']) ? $_POST['least_count_for_rpm'] : '';
            $uuc_json['uuc_rpm_reading'] = isset($_POST['uuc_rpm_reading']) ? $_POST['uuc_rpm_reading'] : '';
            $uuc_json['uuc_slip_reading'] = isset($_POST['uuc_slip_reading']) ? $_POST['uuc_slip_reading'] : '';
            $uuc_json['uuc_range_2'] = isset($_POST['uuc_range_2']) ? $_POST['uuc_range_2'] : '';
            $uuc_json['uuc_range_print_table'] = isset($_POST['uuc_range_print_table']) ? $_POST['uuc_range_print_table'] : '';
            $uuc_json['uut_range_min'] = isset($_POST['range_min']) ? $_POST['range_min'] : '';
            $uuc_json['temp_of_water'] = isset($_POST['temp_of_water']) ? $_POST['temp_of_water'] : '';
            $uuc_json['ct_class'] = isset($_POST['ct_class']) ? $_POST['ct_class'] : '';
            $uuc_json['repeatibility'] = isset($_POST['repeatibility']) ? $_POST['repeatibility'] : '';
            $uuc_json['sensor'] = isset($_POST['sensor']) ? $_POST['sensor'] : '';
            $uuc_json['uncertainty_calibration'] = isset($_POST['uncertainty_calibration']) ? $_POST['uncertainty_calibration'] : '';
            $uuc_json['grn_uuc_range'] = isset($_POST['grn_uuc_range']) ? $_POST['grn_uuc_range'] : '';
            $uuc_json['class'] = isset($_POST['class']) ? $_POST['class'] : '';
            $uuc_json['cos'] = isset($_POST['cos']) ? $_POST['cos'] : '';
            $uuc_json['ctr'] = isset($_POST['ctr']) ? $_POST['ctr'] : '';
            $uuc_json['col_head_1'] = isset($_POST['col_head_1']) ? $_POST['col_head_1'] : '';
            $uuc_json['span_in'] = isset($_POST['span_in']) ? $_POST['span_in'] : '';
            $uuc_json['start_time'] = isset($_POST['start_time']) ? $_POST['start_time'] : '';
            $uuc_json['stop_time'] = isset($_POST['stop_time']) ? $_POST['stop_time'] : '';
            $uuc_json['flow_meter_size'] = isset($_POST['flow_meter_size']) ? $_POST['flow_meter_size'] : '';
            $uuc_json['line_size'] = isset($_POST['line_size']) ? $_POST['line_size'] : '';
            $uuc_json['pipe_mate_thick'] = isset($_POST['pipe_mate_thick']) ? $_POST['pipe_mate_thick'] : '';
            $uuc_json['cali_fluid'] = isset($_POST['cali_fluid']) ? $_POST['cali_fluid'] : '';
            $uuc_json['cal_reading_range_1'] = isset($_POST['cal_reading_range_1']) ? $_POST['cal_reading_range_1'] : '';
            $uuc_json['cal_reading_range_2'] = isset($_POST['cal_reading_range_2']) ? $_POST['cal_reading_range_2'] : '';
            $uuc_json['power_factor'] = isset($_POST['power_factor']) ? $_POST['power_factor'] : '';
            $uuc_json['start_reading_of_kwh'] = isset($_POST['start_reading_of_kwh']) ? $_POST['start_reading_of_kwh'] : '';
            $uuc_json['end_reading_of_kwh'] = isset($_POST['end_reading_of_kwh']) ? $_POST['end_reading_of_kwh'] : '';
            $uuc_json['ac_current'] = isset($_POST['ac_current']) ? $_POST['ac_current'] : '';
            $uuc_json['ac_voltage'] = isset($_POST['ac_voltage']) ? $_POST['ac_voltage'] : '';
            $uuc_json['uut_range'] = $_POST['range'];
            $uuc_json['uut_range_range'] = isset($_POST['uuc_range_range']) ? $_POST['uuc_range_range'] : '';
            $uuc_json['burden_6b'] = isset($_POST['burden_6b']) ? $_POST['burden_6b'] : '';
            $uuc_json['ratio_6b'] = isset($_POST['ratio_6b']) ? $_POST['ratio_6b'] : '';
            $uuc_json['uut_range_sec'] = $_POST['range_sec'];
            $uuc_json['ratio_bottom_part'] = $_POST['ratio_bottom_part'];
            $uuc_json['ratio_bottom_part_range'] = $_POST['ratio_bottom_part_range'];
            $uuc_json['voltage'] = $_POST['voltage'];
            $uuc_json['cali_location'] = $_POST['cali_location'];
            $uuc_json['uuc_location'] = $_POST['uuc_location'];
            $uuc_json['approved_by'] = isset($_POST['approved_by']) ? $_POST['approved_by'] : '';
            $uuc_json['user_id'] = isset($_POST['user_id']) ? $_POST['user_id'] : '';

            if(!empty($old_table_id)){
                $decode_old_uuc_json = json_decode($old_uuc_json);
                $decode_old_uuc_json = json_decode(json_encode($decode_old_uuc_json), true);
                if(isset($_POST['obs_data_id']) && !empty($_POST['obs_data_id'])){
                    $decode_old_uuc_json[$_POST['obs_data_key']] = $uuc_json;
                } else {
                    $decode_old_uuc_json[] = $uuc_json;
                }
                $uuc_json_data =  preg_replace('/\\\\u([a-f0-9]{4})/e', "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($decode_old_uuc_json));
            } else {
                $uuc_json_arr = array();
                $uuc_json_arr[0] = $uuc_json;
                $uuc_json_data = preg_replace('/\\\\u([a-f0-9]{4})/e', "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($uuc_json_arr));
            }
//            echo "<pre>"; print_r($uuc_json_data); exit;
            $meter_reading_arr = array();
            $std_meter_json = array();
            if(!empty($_POST['stdMeter'][0])) {
                foreach ($_POST['stdMeter'][0] as $stdMeter_val) {
                    if($stdMeter_val != ''){
                        $std_meter_json[] = $stdMeter_val;
                    }
                }
            }
            $meter_reading_arr['stdMeter'] = $std_meter_json;
            if(isset($_POST['testMeter'])) {
                $test_meter_json = array();
                if(!empty($_POST['testMeter'][0])) {
                    foreach ($_POST['testMeter'][0] as $testMeter_val) {
                        if($testMeter_val != '') {
                            $test_meter_json[] = $testMeter_val;
                        }
                    }
                }
                $meter_reading_arr['testMeter'] = $test_meter_json;
            }
            
            if(isset($_POST['testMeter2'])) {
                $test_meter2_json = array();
                if(!empty($_POST['testMeter2'][0])) {
                    foreach ($_POST['testMeter2'][0] as $testMeter2_val){
                        if($testMeter2_val != '') {
                            $test_meter2_json[] = $testMeter2_val;
                        }
                    }
                }
                $meter_reading_arr['testMeter2'] = $test_meter2_json;
            }
            
            if(isset($_POST['testMeter3'])) {
                $test_meter3_json = array();
                if(!empty($_POST['testMeter3'][0])) {
                    foreach ($_POST['testMeter3'][0] as $testMeter3_val) {
                        if($testMeter3_val != '') {
                            $test_meter3_json[] = $testMeter3_val;
                        }
                    }
                }
                $meter_reading_arr['testMeter3'] = $test_meter3_json;
            }
            if(isset($_POST['testMeter4'])) {
                $test_meter4_json = array();
                if(!empty($_POST['testMeter4'][0])) {
                    foreach ($_POST['testMeter4'][0] as $testMeter4_val) {
                        if($testMeter4_val != '') {
                            $test_meter4_json[] = $testMeter4_val;
                        }
                    }
                }
                $meter_reading_arr['testMeter4'] = $test_meter4_json;
            }
            if(isset($_POST['testMeter5'])) {
                $test_meter5_json = array();
                if(!empty($_POST['testMeter5'][0])) {
                    foreach ($_POST['testMeter5'][0] as $testMeter5_val) {
                        if($testMeter5_val != '') {
                            $test_meter5_json[] = $testMeter5_val;
                        }
                    }
                }
                $meter_reading_arr['testMeter5'] = $test_meter5_json;
            }
            if(isset($_POST['testMeter6'])) {
                $test_meter6_json = array();
                if(!empty($_POST['testMeter6'][0])) {
                    foreach ($_POST['testMeter6'][0] as $testMeter6_val) {
                        if($testMeter6_val != '') {
                            $test_meter6_json[] = $testMeter6_val;
                        }
                    }
                }
                $meter_reading_arr['testMeter6'] = $test_meter6_json;
            }
            if(isset($_POST['testMeter7'])) {
                $test_meter7_json = array();
                if(!empty($_POST['testMeter7'][0])) {
                    foreach ($_POST['testMeter7'][0] as $testMeter7_val) {
                        if($testMeter7_val != '') {
                            $test_meter7_json[] = $testMeter7_val;
                        }
                    }
                }
                $meter_reading_arr['testMeter7'] = $test_meter7_json;
            }
            if(isset($_POST['testMeter8'])) {
                $test_meter8_json = array();
                if(!empty($_POST['testMeter8'][0])) {
                    foreach ($_POST['testMeter8'][0] as $testMeter8_val) {
                        if($testMeter8_val != '') {
                            $test_meter8_json[] = $testMeter8_val;
                        }
                    }
                }
                $meter_reading_arr['testMeter8'] = $test_meter8_json;
            }
            if(isset($_POST['testMeter9'])) {
                $test_meter9_json = array();
                if(!empty($_POST['testMeter9'][0])) {
                    foreach ($_POST['testMeter9'][0] as $testMeter9_val) {
                        if($testMeter9_val != '') {
                            $test_meter9_json[] = $testMeter9_val;
                        }
                    }
                }
                $meter_reading_arr['testMeter9'] = $test_meter9_json;
            }
            if(isset($_POST['testMeter10'])) {
                $test_meter10_json = array();
                if(!empty($_POST['testMeter10'][0])) {
                    foreach ($_POST['testMeter10'][0] as $testMeter10_val) {
                        if($testMeter10_val != '') {
                            $test_meter10_json[] = $testMeter10_val;
                        }
                    }
                }
                $meter_reading_arr['testMeter10'] = $test_meter10_json;
            }
//            echo "<pre>"; print_r($uuc_json_data); exit;
            if(!empty($old_table_id)) {
                $decode_old_reading_json = json_decode($old_reading_json);
                $decode_old_reading_json = json_decode(json_encode($decode_old_reading_json), true);
                if(isset($_POST['obs_data_id']) && !empty($_POST['obs_data_id'])) {
                    $decode_old_reading_json[$_POST['obs_data_key']] = $meter_reading_arr;
                } else {
                    $decode_old_reading_json[] = $meter_reading_arr;
                }
                $meter_reading_json_data = json_encode($decode_old_reading_json);
            } else {
                $meter_reading_data_arr = array();
                $meter_reading_data_arr[0] = $meter_reading_arr;
                $meter_reading_json_data = json_encode($meter_reading_data_arr);
            }
            
            
            $type_ab_json['accuracy_fs'] = $accuracy_fs;
            $type_ab_json['accuracy_rdg'] = $accuracy_rdg;
            $type_ab_json['master_range'] = $master_range;
            $type_ab_json['un_cali_certi'] = $un_cali_certi;
            $type_ab_json['un_cali_certi_2'] = $un_cali_certi_2;
            $type_ab_json['stdmeter_avg_new'] = $stdmeter_avg_new;
            $type_ab_json['r_phase_accuracy'] = $r_phase_accuracy;
            $type_ab_json['uuc_least_count'] = $uuc_least_count;
            $type_ab_json['std_dev'] = $std_dev;
            $type_ab_json['std_uncer'] = $std_uncer;
            $type_ab_json['std_uncer_per'] = $std_uncer_per;
            $type_ab_json['uncer_master'] = $uncer_master;
            $type_ab_json['acc_calibration'] = $acc_calibration;
            $type_ab_json['resolution_new'] = $resolution_new;
            $type_ab_json['com_uncer'] = $com_uncer;
            $type_ab_json['exp_uncer'] = $exp_uncer;
            $type_ab_json['exp_uncer_per_ue'] = $exp_uncer_per_ue;
            $type_ab_json['exp_uncer_per_ue_without_cmc'] = $exp_uncer_per_ue_without_cmc;
            $type_ab_json['cmc'] = $cmc;
            $type_ab_json['uncer_of_install_error'] = $uncer_of_install_error;
            $type_ab_json['uncer_of_repeat'] = $uncer_of_repeat;
            $type_ab_json['uncertainty_hysteresis'] = $uncertainty_hysteresis;
            $type_ab_json['reference_weight'] = $reference_weight;
            $type_ab_json['eccentric_loading'] = $eccentric_loading;
            $type_ab_json['stability_of_source'] = $stability_of_source;
            $type_ab_json['u1'] = $u1;
            $type_ab_json['u1_val'] = $u1_val;
            $type_ab_json['u2'] = $u2;
            $type_ab_json['u2_val'] = $u2_val;
            $type_ab_json['u3'] = $u3;
            $type_ab_json['u3_val'] = $u3_val;
            $type_ab_json['u4'] = $u4;
            $type_ab_json['u4_val'] = $u4_val;
            $type_ab_json['u5'] = $u5;
            $type_ab_json['u5_val'] = $u5_val;
            $type_ab_json['u6'] = $u6;
            $type_ab_json['u6_val'] = $u6_val;
            $type_ab_json['uncertainty_hysteresis_max'] = $uncertainty_hysteresis_max;
            $type_ab_json['uncer_of_repeat_max'] = $uncer_of_repeat_max;
            
            if($_POST['table_info_id'] == '12'){
                $type_ab_json['testMeter_avg'] = $testMeter_avg;
                $type_ab_json['testMeter_avg2'] = $testMeter_avg2;
                $type_ab_json['testMeter_avg3'] = $testMeter_avg3;
                $type_ab_json['testMeter_avg4'] = $testMeter_avg4;
                $type_ab_json['testMeter_avg5'] = $testMeter_avg5;
                $type_ab_json['testMeter_avg6'] = $testMeter_avg6;
                $type_ab_json['testMeter_avg7'] = $testMeter_avg7;
                $type_ab_json['testMeter_avg8'] = $testMeter_avg8;
                $type_ab_json['testMeter_avg9'] = $testMeter_avg9;
                $type_ab_json['testMeter_avg10'] = $testMeter_avg10;
                
                $type_ab_json['stdMeterAverage'] = 0;
                $type_ab_json['standardDeviation'] = 0;
                $type_ab_json['standardUncertinity'] = 0;
                $type_ab_json['standardUncertinityperc'] = 0;
                $type_ab_json['uncertinityForTypeB'] = 0;
                $type_ab_json['accuracyForTypeB'] = 0;
                $type_ab_json['resolutionTypeB'] = 0;
                $type_ab_json['meanReading'] = 0;
                $type_ab_json['stability_of_source'] = 0;
                $type_ab_json['combinedUncertinity'] = 0;
                $type_ab_json['expandedUncertinity'] = 0;
                $type_ab_json['expandedUncertinityInPre'] = 0;
                $type_ab_json['testMeterAverage'] = 0;
                $type_ab_json['testMeter2Average'] = 0;
                $type_ab_json['testMeter3Average'] = 0;
                $type_ab_json['uncertainty_hysteresis_max'] = $uncertainty_hysteresis_max;
                $type_ab_json['uncer_of_repeat_max'] = $uncer_of_repeat_max;
                
            } else if($_POST['table_info_id'] == '18'){
                $type_ab_json['stdMeterAverage'] = $stdmeter_avg;
                $type_ab_json['standardDeviation'] = 0;
                $type_ab_json['standardUncertinity'] = 0;
                $type_ab_json['standardUncertinityperc'] = 0;
                $type_ab_json['uncertinityForTypeB'] = 0;
                $type_ab_json['accuracyForTypeB'] = 0;
                $type_ab_json['resolutionTypeB'] = 0;
                $type_ab_json['meanReading'] = 0;
                $type_ab_json['stability_of_source'] = $stability_of_source;
                $type_ab_json['combinedUncertinity'] = 0;
                $type_ab_json['expandedUncertinity'] = 0;
                $type_ab_json['expandedUncertinityInPre'] = 0;
                $type_ab_json['testMeterAverage'] = 0;
                $type_ab_json['testMeter2Average'] = $testmeter2_avg;
                $type_ab_json['testMeter3Average'] = 0;
                
                $type_ab_json['accuracy_fs'] = $accuracy_fs;
                $type_ab_json['accuracy_rdg'] = $accuracy_rdg;
                $type_ab_json['master_range'] = $master_range;
                $type_ab_json['un_cali_certi'] = 0;
                $type_ab_json['stdmeter_avg_new'] = $stdmeter_avg_new;
                $type_ab_json['r_phase_accuracy'] = 0;
                $type_ab_json['uuc_least_count'] = $uuc_least_count;
                $type_ab_json['std_dev'] = $std_dev;
                $type_ab_json['std_uncer'] = 0;
                $type_ab_json['std_uncer_per'] = 0;
                $type_ab_json['uncer_master'] = 0;
                $type_ab_json['acc_calibration'] = 0;
                $type_ab_json['resolution_new'] = 0;
                $type_ab_json['com_uncer'] = 0;
                $type_ab_json['exp_uncer'] = 0;
                $type_ab_json['exp_uncer_per_ue'] = 0;
                $type_ab_json['uncer_of_install_error'] = 0;
                $type_ab_json['uncer_of_repeat'] = 0;
                $type_ab_json['uncertainty_hysteresis_max'] = $uncertainty_hysteresis_max;
                $type_ab_json['uncer_of_repeat_max'] = $uncer_of_repeat_max;
                
            } else {
                $type_ab_json['stdMeterAverage'] = $stdmeter_avg;
                $type_ab_json['standardDeviation'] = $standardDeviation;
                $type_ab_json['standardUncertinity'] = $standardUncertinity;
                $type_ab_json['standardUncertinityperc'] = $standardUncertinityperc;
                $type_ab_json['uncertinityForTypeB'] = $uncertinityForTypeB;
                $type_ab_json['accuracyForTypeB'] = $accuracyForTypeB;
                $type_ab_json['resolutionTypeB'] = $resolutionTypeB;
                $type_ab_json['meanReading'] = $meanReading;
                $type_ab_json['stability_of_source'] = $stability_of_source;
                $type_ab_json['combinedUncertinity'] = $combinedUncertinity;
                $type_ab_json['expandedUncertinity'] = $expandedUncertinity;
                $type_ab_json['expandedUncertinityInPre'] = $expandedUncertinityInPre;
                $type_ab_json['testMeterAverage'] = $testmeter_avg;
                $type_ab_json['testMeter2Average'] = $testmeter2_avg;
                $type_ab_json['testMeter3Average'] = $testmeter3_avg;
                $type_ab_json['uncertainty_hysteresis_max'] = $uncertainty_hysteresis_max;
                $type_ab_json['uncer_of_repeat_max'] = $uncer_of_repeat_max;
            }
                $type_ab_json['r_phase_accuracy_2'] = $r_phase_accuracy_2;
                $type_ab_json['uncer_master_2'] = $uncer_master_2;
                $type_ab_json['acc_calibration_2'] = $acc_calibration_2;
                
                
            if(!empty($old_table_id)) {
                $decode_old_type_a_type_b_json = json_decode($old_type_a_type_b_json);
                $decode_old_type_a_type_b_json = json_decode(json_encode($decode_old_type_a_type_b_json), true);
                if(isset($_POST['obs_data_id']) && !empty($_POST['obs_data_id'])){
                    $decode_old_type_a_type_b_json[$_POST['obs_data_key']] = $type_ab_json;
                } else {
                    $decode_old_type_a_type_b_json[] = $type_ab_json;
                }
                $type_ab_json_data = json_encode($decode_old_type_a_type_b_json);
            } else {
                $type_ab_json_arr = array();
                $type_ab_json_arr[0] = $type_ab_json;
                $type_ab_json_data = json_encode($type_ab_json_arr);
            }

            if(!empty($old_table_id)) {
                $insert_obs = "UPDATE `observation_data` SET
                                master_json = '" . $master_json_data . "',
                                uuc_json = '" . mysql_real_escape_string($uuc_json_data) . "',
                                reading_json ='" . $meter_reading_json_data . "',
                                type_a_type_b_json = '" . $type_ab_json_data . "'
                            WHERE id = " . $old_table_id;
                $insert_obs_Res = mysql_query($insert_obs);
                if (!$insert_obs_Res) {
                    die("Insert Query Not Inserted 1: " . mysql_error() . " : " . $insert_obs);    
                }
            } else {
                $created_by = $_SESSION['user_id'];
                $created_at = date('Y-m-d H:i:s');
                $insert_obs = "INSERT INTO observation_data (grn_id, grn_detail_id, table_info_id,
                            table_no, master_json, uuc_json, reading_json,type_a_type_b_json,created_by,created_at)
                          VALUES (" . $_POST['grnId'] . ",
                                  " . $_POST['grnDetailId'] . ",
                                  " . $_POST['table_info_id'] . ",
                                  " . $_POST['table_no'] . ",
                                  '" . $master_json_data . "',
                                  '" . mysql_real_escape_string($uuc_json_data) . "',
                                  '" . $meter_reading_json_data . "',
                                  '" . $type_ab_json_data . "',
                                  '" . $created_by . "',
                                  '" . $created_at . "')";
                $insert_obs_Res = mysql_query($insert_obs);
                if (!$insert_obs_Res) {
                    die("Insert Query Not Inserted 1: " . mysql_error() . " : " . $insert_obs);
                }
            }
//            //INSERT INTO JSON DATA : END
//
            $callibrationDate = ($_POST['callibrationDateYear'] . "-" . $_POST['callibrationDateMonth'] . "-" . $_POST['callibrationDateDay']);
            $nextYearDate = ($_POST['nextYearDateYear'] . "-" . $_POST['nextYearDateMonth'] . "-" . $_POST['nextYearDateDay']);
            $certificateIssue = isset($_POST['certificateIssue']) ? "Y" : "N";
            $certificateIssueDate = ($_POST['certificateIssueDateYear'] . "-" . $_POST['certificateIssueDateMonth'] . "-" . $_POST['certificateIssueDateDay']);
        


            $updateGrnEntry = "UPDATE grndetail SET
                                refGrnDetailId = '" . $grnDetailPassId . "',
                                callibrationDate ='" . $callibrationDate . "',
                                nextYearDate = '" . $nextYearDate . "',
                                makeModel = '" . str_replace("'","\'",$_POST['makeModel']) . "',
                                certificateIssue = '" . $certificateIssue . "',
                                certificateIssueDate = '" . $certificateIssueDate . "',
                                certificate_no = '" . $_POST['selfCertiNo'] . "',
                                instrumentId = '" . $_POST['instrumentId'] . "',
                                rangeValue = '" . $_POST['grn_uuc_range'] . "',
                                temperature = '" . $_POST['temperature'] . "',
                                humidity = '" . $_POST['humidity'] . "',
                                accuracy = '" . $_POST['accuracy'] . "',
                                userName = '" . $_SESSION['s_activId'] . "',
                                user_id = '" . $_POST['user_id'] . "',
                                certiRemarks = '" . $_POST['certiRemarks'] . "',
                                cali_location = " . $_POST['cali_location'] . ",
                                uuc_location = '" . $_POST['uuc_location'] . "',
                                approved_by = " . $_POST['approved_by'] . "
                            WHERE grnDetailId = " . $_POST['grnDetailId'];
            $updateGrnEntryResult = mysql_query($updateGrnEntry);
            if (!$updateGrnEntryResult) {
                die("Insert Query Not Inserted 1: " . mysql_error() . " : " . $updateGrnEntry);
            }
            
            if($_POST['table_info_id'] == '18'){
                $master_data1 = '';
                $uuc_data1 = '';
                $reading_data1 = '';
                        
                $master_data2 = '';
                $uuc_data2 = '';
                $reading_data3 = '';
                
                $master_data3 = '';
                $uuc_data3 = '';
                $reading_data3 = '';
                $old_type_a_type_b_json1 = '';
                $selectobsa = "SELECT * FROM observation_data WHERE grn_id = ".$_POST['grnId']." AND grn_detail_id = ".$_POST['grnDetailId']."  ";
                $selectobsaResult = mysql_query($selectobsa);
                while ($selectobsRow = mysql_fetch_array($selectobsaResult)) {
                    $master_json_data1 = json_decode($selectobsRow['master_json']);
                    $master_json_data1 = json_decode(json_encode($master_json_data1), true);
                    $uuc_json_data1 = json_decode($selectobsRow['uuc_json']);
                    $uuc_json_data1 = json_decode(json_encode($uuc_json_data1), true);
                    $reading_json_data1 = json_decode($selectobsRow['reading_json']);
                    $reading_json_data1 = json_decode(json_encode($reading_json_data1), true);
                    $type_a_b_json_data1 = json_decode($selectobsRow['type_a_type_b_json']);
                    $old_type_a_type_b_json1 = json_decode(json_encode($type_a_b_json_data1), true);
                    if($uuc_json_data1[0]['obs_type_id'] == 1){
                        $master_data1 = $master_json_data1[0];
                        $uuc_data1 = $uuc_json_data1[0];
                        $reading_data1 = $reading_json_data1[0];
                    }
                    if($uuc_json_data1[0]['obs_type_id'] == 2){
                        $master_data2 = $master_json_data1[0];
                        $uuc_data2 = $uuc_json_data1[0];
                        $reading_data2 = $reading_json_data1[0];
                    }
                    if($uuc_json_data1[0]['obs_type_id'] == 3){
                        $master_data3 = $master_json_data1[0];
                        $uuc_data3 = $uuc_json_data1[0];
                        $reading_data3 = $reading_json_data1[0];
                    }
                }
//                echo "<pre>"; print_r($reading_data3); exit;
                if(!empty($master_data1) && !empty($master_data2) && !empty($master_data3)){
                    $u1     = ((($_POST['uncertinty_from_tracebility']/$_POST['covarage_factor'])*0.1));
                    $u1     = number_format((float)$u1, 6, '.', '');
                    $u1_val = $u1/sqrt(3);
                    $u1_val = number_format((float)$u1_val, 6, '.', '');
                    
                    $stdmeter_data3   = $reading_data3['stdMeter'];
                    $testmeter1_data3 = $reading_data3['testMeter'];
                    $testmeter2_data3 = $reading_data3['testMeter2'];
                    $testmeter2_avg3 = testmeter_avg($testmeter2_data3);
                    $max_key = max(array_keys($testmeter2_data3));
                    $max_testmeter2_val = '';
                    foreach ($testmeter2_data3 as $testmeter2_read){
                        if($max_testmeter2_val == ''){
                            $max_testmeter2_val = $testmeter2_read;
                        } else {
                            if($testmeter2_read > $max_testmeter2_val){
                                $max_testmeter2_val = $testmeter2_read;
                            }
                        }
                    }
                    $std_dev = standardDeviation_val($testmeter2_data3, $testmeter2_avg3);
                    $std_dev = number_format((float)$std_dev, 6, '.', '');
                    $max_error = '';
                    foreach ($testmeter2_data3 as $key => $testmeter2_read){
                        $std_m_val = isset($stdmeter_data3[$key]) ? $stdmeter_data3[$key] : '0';
                        $error = $std_m_val - $testmeter2_read;
                        if($max_error == ''){
                            $max_error = $error;
                        } else {
                            if($error > $max_error){
                                $max_error = $error;
                            }
                        }
                    }
                    $repetability = $max_error;
                    $u2     =  $repetability/2;
                    $u2     =  number_format((float)$u2, 6, '.', '');
                    $u2_val = $u2/sqrt(3);
                    $u2_val = number_format((float)$u2_val, 6, '.', '');
                    
                    $u3     = $_POST['uncertinty_from_tracebility']/2;
                    $u3     = number_format((float)$u3, 6, '.', '');
                    $u3_val = $u3/sqrt(3);
                    
                    $u4     = $_POST['leastCount']/2/sqrt(3);
                    $u4     = number_format((float)$u4, 6, '.', '');
                    $u4_val = $u4/sqrt(3); 
                    $u4_val = number_format((float)$u4_val, 6, '.', '');
//                    echo $u4;     echo '<br/>';
//                    echo $u4_val;     echo '<br/>'; exit;
                    $stdmeter_data = $reading_data2['stdMeter'];
                    $testmeter1_data = $reading_data2['testMeter'];
                    $c_min = '';
                    $c_max = '';
                    $a_min = '';
                    $a_max = '';
                    $e_min = '';
                    $e_max = '';
                    for ($x = 0; $x <= max(array_keys($stdmeter_data)); $x++) {
                    $i = $x + 1;
                    if($c_min == ''){
                        $c_min = $stdmeter_data[$x];
                    } else {
                        if($stdmeter_data[$x] < $c_min){
                            $c_min = $stdmeter_data[$x];
                        }
                    }
                    if($c_max == ''){
                        $c_max = $stdmeter_data[$x];
                    } else {
                        if($stdmeter_data[$x] > $c_max){
                            $c_max = $stdmeter_data[$x];
                        }
                    }
                    if($a_min == ''){
                        $a_min = $testmeter1_data[$x];
                    } else {
                        if($testmeter1_data[$x] < $a_min){
                            $a_min = $testmeter1_data[$x];
                        }
                    }
                    if($a_max == ''){
                        $a_max = $testmeter1_data[$x];
                    } else {
                        if($testmeter1_data[$x] > $a_max){
                            $a_max = $testmeter1_data[$x];
                        }
                    }
                    } 
                    if($c_min < $a_min){
                        $e_min = $c_min;
                    } else {
                        $e_min = $a_min;
                    }
                    if($c_max > $a_max){
                        $e_max = $c_max;
                    } else {
                        $e_max = $a_max;
                    }
                    $d = $e_max - $e_min;
                    $d = number_format((float)$d, 6, '.', '');
                    $u5     = (($uuc_data2['d1']/$uuc_data2['d2'])*$d)/2;
                    $u5     = number_format((float)$u5, 6, '.', '');
                    $u5_val = $u5/sqrt(3);
                    $u5_val = number_format((float)$u5_val, 6, '.', '');
                    $com_uncer = sqrt(pow($u1_val,2)+pow($u2_val,2)+pow($u3_val,2)+pow($u4_val,2)+pow($u5_val,2));
                    $com_uncer = number_format((float)$com_uncer, 6, '.', '');
                    $exp_uncer = $com_uncer*$_POST['covarage_factor'];
                    $exp_uncer = number_format((float)$exp_uncer, 6, '.', '');
                    $exp_uncer_per_ue = $exp_uncer*100/$max_testmeter2_val;
                    $exp_uncer_per_ue = number_format((float)$exp_uncer_per_ue, 6, '.', '');
                    $exp_uncer_per_ue_without_cmc = $exp_uncer_per_ue;
                    $exp_uncer_per_ue_without_cmc = number_format((float)$exp_uncer_per_ue_without_cmc, 6, '.', '');
                    $cmc = $m_cmc;
                    if($m_cmc > $exp_uncer_per_ue){
                        $exp_uncer_per_ue = $m_cmc;
                    }
                    $uncer_master = $u1_val;
                    $acc_calibration = '';
                    $resolution_new = $u4_val;
                    $uncer_of_repeat = $u2_val;
                    $un_cali_certi    = $_POST['uncertinty_from_tracebility'];
                    
                    $old_type_a_type_b_json1[0]['com_uncer'] = $com_uncer;
                    $old_type_a_type_b_json1[0]['exp_uncer'] = $exp_uncer;
                    $old_type_a_type_b_json1[0]['exp_uncer_per_ue'] = $exp_uncer_per_ue;
                    $old_type_a_type_b_json1[0]['exp_uncer_per_ue_without_cmc'] = $exp_uncer_per_ue_without_cmc;
                    $old_type_a_type_b_json1[0]['cmc'] = $cmc;
                    $old_type_a_type_b_json1[0]['uncer_master'] = $uncer_master;
                    $old_type_a_type_b_json1[0]['acc_calibration'] = $acc_calibration;
                    $old_type_a_type_b_json1[0]['resolution_new'] = $resolution_new;
                    $old_type_a_type_b_json1[0]['uncer_of_repeat'] = $uncer_of_repeat;
                    $old_type_a_type_b_json1[0]['un_cali_certi'] = $un_cali_certi;
                    $old_type_a_type_b_json1[0]['eccentric_loading'] = $u5_val;
                    $old_type_a_type_b_json1[0]['reference_weight'] = $u3_val;
                    
                    $old_type_a_type_b_json1[0]['u1'] = $u1;
                    $old_type_a_type_b_json1[0]['u1_val'] = $u1_val;
                    $old_type_a_type_b_json1[0]['u2'] = $u2;
                    $old_type_a_type_b_json1[0]['u2_val'] = $u2_val;
                    $old_type_a_type_b_json1[0]['u3'] = $u3;
                    $old_type_a_type_b_json1[0]['u3_val'] = $u3_val;
                    $old_type_a_type_b_json1[0]['u4'] = $u4;
                    $old_type_a_type_b_json1[0]['u4_val'] = $u4_val;
                    $old_type_a_type_b_json1[0]['u5'] = $u5;
                    $old_type_a_type_b_json1[0]['u5_val'] = $u5_val;
                    $type_ab_json['uncertainty_hysteresis_max'] = $uncertainty_hysteresis_max;
                    $type_ab_json['uncer_of_repeat_max'] = $uncer_of_repeat_max;
                    
//                    echo $com_uncer;     echo '<br/>';
//                    echo $exp_uncer;     echo '<br/>';
//                    echo $exp_uncer_per_ue;     echo '<br/>'; 
//                    echo $exp_uncer_per_ue_without_cmc;     echo '<br/>'; exit;
//                    echo $u1;     echo '<br/>';
//                    echo $u1_val; echo '<br/>';
//                    echo $u2;     echo '<br/>';
//                    echo $u2_val; echo '<br/>';
//                    echo $u3;     echo '<br/>';
//                    echo $u3_val; echo '<br/>';
//                    echo $u4;     echo '<br/>';
//                    echo $u4_val; echo '<br/>';
//                    echo $u5;     echo '<br/>';
//                    echo $u5_val; echo '<br/>';
//                    exit;
                    
                    $type_ab_json_data = json_encode($old_type_a_type_b_json1);
//                    echo "<pre>"; print_r($old_type_a_type_b_json1); exit;
                    $insert_obs = "UPDATE `observation_data` SET
                                type_a_type_b_json = '" . $type_ab_json_data . "'
                            WHERE grn_id = ".$_POST['grnId']." AND grn_detail_id = ".$_POST['grnDetailId'];
                    $insert_obs_Res = mysql_query($insert_obs);
                    if (!$insert_obs_Res) {
                        die("Insert Query Not Inserted 99: " . mysql_error() . " : " . $insert_obs);    
                    }
                }
                
            }
            ////// Insert Query for GRN Observation Detail : Ends
            $is_refresh = 0;
            if(isset($_POST['obs_data_id']) && !empty($_POST['obs_data_id'])){
                $is_refresh = 1;
                echo json_encode(array("status" => "success", "is_refresh" => $is_refresh, "obs_id" => $_POST['obs_data_id'], "obs_value" => $_POST['obs_data_key']));
            } else if(isset($_POST['obs_id']) && !empty($_POST['obs_id'])){
                $is_refresh = 1;
                echo json_encode(array("status" => "success", "is_refresh" => $is_refresh, "obs_id" => $_POST['obs_id'], "obs_value" => $_POST['obs_key'] + 1));
            } else {
                echo json_encode(array("status" => "success", "is_refresh" => $is_refresh));
            }
            exit();

        }
        $edit_master_meter_id = '';
        $edit_master_meter_id_2 = '';
        $edit_master_meter_id_3 = '';
        $edit_master_meter_sub_id = '';
        $edit_master_meter_sub_id_2 = '';
        $edit_master_meter_sub_id_3 = '';
        $edit_master_meter_sub_sub_id = '';
        $edit_master_meter_sub_sub_id_2 = '';
        $edit_master_meter_sub_sub_id_3 = '';
        $edit_consider_ue = '';
        $edit_ue_unit = '';
        $edit_grn_id = '';
        $edit_grn_detail_id = '';
        $edit_table_info_id = '';
        $edit_uuc_data = array();
        $edit_leastCount = '';
        $edit_accuracy = '';
        $edit_certiRemarks = '';
        $edit_voltage = '';
        $edit_ocillation = '';
        $edit_col_head_1_least_count = '';
        $edit_least_count_for_rpm = '';
        $edit_uncertainty_calibration = '';
        $edit_frequency = '';
        $edit_rpm = '';
        $edit_makeModel = '';
        $edit_instrumentId = '';
        $edit_uut_range = '';
        $edit_uuc_range_print_table = '';
        $edit_uuc_location = '';
        $edit_table_no = '0';
        $edit_uut_range_min = '';
        $edit_user_id = '';
        $edit_table_title_right = '';
        $edit_ct_class = '';
        $edit_covarage_factor = '';
        $edit_confidence_level = '';
        $edit_sensor = '';
        $edit_humidity = '';
        $edit_obs_type_id = '';
        $edit_weight_id = '';
        $edit_d1 = '';
        $edit_d2 = '';
        $edit_temperature = '';
        $edit_repeatibility = '';
        $edit_temp_of_water = '';
        $edit_phase_type_id = '';
        $edit_cos = '';
        $edit_class = '';
        $edit_ctr = '';
        $edit_start_time = '';
        $edit_stop_time = '';
        $edit_flow_meter_size = '';
        $edit_line_size = '';
        $edit_pipe_mate_thick = '';
        $edit_cali_fluid = '';
        $edit_cal_reading_range_1 = '';
        $edit_cal_reading_range_2 = '';
        $edit_power_factor = '';
        $edit_start_reading_of_kwh = '';
        $edit_end_reading_of_kwh = '';
        $edit_ac_current = '';
        $edit_ac_voltage = '';
        $edit_uut_range_sec = '';
        $edit_ratio_bottom_part = '';
        $edit_ratio_bottom_part_range = '';
        $edit_uuc_range_range = '';
        $edit_burden_6b = '';
        $edit_ratio_6b = '';
        $edit_col_head_1 = '';
        $edit_span_in = '';
        $edit_cali_location = '';
        $edit_uuc_range_2 = '';
        $edit_master = array();
        $edit_leastCount_print = '';
        $edit_reading_json_data = array();
        $edit_uuc_json_data = array();
        $obs_data_id = '';
        $obs_id = '';
        $obs_key = '';
        $obs_data_key = '';
        if ((isset($_GET['obs_data_id']) && isset($_GET['key_value'])) || (isset($_GET['obs_id']) && isset($_GET['obs_value']))) {
            if (isset($_GET['obs_data_id']) && isset($_GET['key_value'])) {
                $obs_data_id = $_GET['obs_data_id'];
                $obs_data_key = $_GET['key_value'];
                $edit_master_json = '';
                $selectobs1 = "SELECT * FROM observation_data
                              WHERE id = ".$_GET['obs_data_id']."  ";
                $selectobs1Result = mysql_query($selectobs1);
            } else {
                $_GET['key_value'] = $_GET['obs_value'];
                $obs_id = $_GET['obs_id'];
                $obs_key = $_GET['obs_value'];
                $edit_master_json = '';
                $selectobs1 = "SELECT * FROM observation_data
                              WHERE id = ".$_GET['obs_id']."  ";
                $selectobs1Result = mysql_query($selectobs1);
            }
            while ($selectobsRow = mysql_fetch_array($selectobs1Result)) {
                $edit_id = $selectobsRow['id'];
                $edit_master_json = $selectobsRow['master_json'];
                $edit_uuc_json = $selectobsRow['uuc_json'];
                $edit_reading_json = $selectobsRow['reading_json'];
                $edit_type_a_type_b_json = $selectobsRow['type_a_type_b_json'];
                $edit_grn_id = $selectobsRow['grn_id'];
                $edit_grn_detail_id = $selectobsRow['grn_detail_id'];
                $edit_table_info_id = $selectobsRow['table_info_id'];
            }
            if(!empty($edit_master_json)) {
                $edit_master_json_data = json_decode($edit_master_json);
                $edit_master_json_data = json_decode(json_encode($edit_master_json_data), true);
                $edit_uuc_json_data1 = json_decode($edit_uuc_json);
                $edit_uuc_data2 = json_decode(json_encode($edit_uuc_json_data1), true);
                $edit_uuc_data = $edit_uuc_data2[$_GET['key_value']];
                $edit_leastCount = $edit_uuc_data2[$_GET['key_value']]['leastCount'];
                $edit_accuracy = $edit_uuc_data2[$_GET['key_value']]['accuracy'];
                $edit_certiRemarks = $edit_uuc_data2[$_GET['key_value']]['certiRemarks'];
                $edit_voltage = $edit_uuc_data2[$_GET['key_value']]['voltage'];
                $edit_frequency = $edit_uuc_data2[$_GET['key_value']]['frequency'];
                $edit_uncertainty_calibration = $edit_uuc_data2[$_GET['key_value']]['uncertainty_calibration'];
                $edit_ocillation = $edit_uuc_data2[$_GET['key_value']]['ocillation'];
                $edit_makeModel = $edit_uuc_data2[$_GET['key_value']]['makeModel'];
                $edit_instrumentId = $edit_uuc_data2[$_GET['key_value']]['instrumentId'];
                $edit_uut_range = $edit_uuc_data2[$_GET['key_value']]['uut_range'];
                $edit_uuc_location = $edit_uuc_data2[$_GET['key_value']]['uuc_location'];
                $edit_table_no = $edit_uuc_data2[$_GET['key_value']]['table_no'];
                $edit_table_title_right = $edit_uuc_data2[$_GET['key_value']]['table_title_right'];
                $edit_uut_range_sec = $edit_uuc_data2[$_GET['key_value']]['uut_range_sec'];
                $edit_ratio_bottom_part = $edit_uuc_data2[$_GET['key_value']]['ratio_bottom_part'];
                $edit_ratio_bottom_part_range = isset($edit_uuc_data2[$_GET['key_value']]['ratio_bottom_part_range']) ? $edit_uuc_data2[$_GET['key_value']]['ratio_bottom_part_range'] : '';
                $edit_uuc_range_print_table = isset($edit_uuc_data2[$_GET['key_value']]['uuc_range_print_table']) ? $edit_uuc_data2[$_GET['key_value']]['uuc_range_print_table'] : '';
                $edit_uuc_range_range = isset($edit_uuc_data2[$_GET['key_value']]['uut_range_range']) ? $edit_uuc_data2[$_GET['key_value']]['uut_range_range'] : '';
                $edit_burden_6b = isset($edit_uuc_data2[$_GET['key_value']]['burden_6b']) ? $edit_uuc_data2[$_GET['key_value']]['burden_6b'] : '';
                $edit_ratio_6b = isset($edit_uuc_data2[$_GET['key_value']]['ratio_6b']) ? $edit_uuc_data2[$_GET['key_value']]['ratio_6b'] : '';
                $edit_rpm = isset($edit_uuc_data2[$_GET['key_value']]['rpm']) ? $edit_uuc_data2[$_GET['key_value']]['rpm'] : '';
                $edit_uuc_range_2 = isset($edit_uuc_data2[$_GET['key_value']]['uuc_range_2']) ? $edit_uuc_data2[$_GET['key_value']]['uuc_range_2'] : '';
                $edit_col_head_1 = isset($edit_uuc_data2[$_GET['key_value']]['col_head_1']) ? $edit_uuc_data2[$_GET['key_value']]['col_head_1'] : '';
                $edit_span_in = isset($edit_uuc_data2[$_GET['key_value']]['span_in']) ? $edit_uuc_data2[$_GET['key_value']]['span_in'] : '';
                $edit_col_head_1_least_count = isset($edit_uuc_data2[$_GET['key_value']]['col_head_1_least_count']) ? $edit_uuc_data2[$_GET['key_value']]['col_head_1_least_count'] : '';
                $edit_least_count_for_rpm = isset($edit_uuc_data2[$_GET['key_value']]['least_count_for_rpm']) ? $edit_uuc_data2[$_GET['key_value']]['least_count_for_rpm'] : '';
                $edit_cali_location = $edit_uuc_data2[$_GET['key_value']]['cali_location'];
                $edit_ct_class = $edit_uuc_data2[$_GET['key_value']]['ct_class'];
                $edit_covarage_factor = $edit_uuc_data2[$_GET['key_value']]['covarage_factor'];
                $edit_confidence_level = $edit_uuc_data2[$_GET['key_value']]['confidence_level'];
                $edit_sensor = $edit_uuc_data2[$_GET['key_value']]['sensor'];
                $edit_repeatibility = $edit_uuc_data2[$_GET['key_value']]['repeatibility'];
                $edit_temp_of_water = $edit_uuc_data2[$_GET['key_value']]['temp_of_water'];
                $edit_phase_type_id = $edit_uuc_data2[$_GET['key_value']]['phase_type_id'];
                $edit_cos = $edit_uuc_data2[$_GET['key_value']]['cos'];
                $edit_class = $edit_uuc_data2[$_GET['key_value']]['class'];
                $edit_ctr = $edit_uuc_data2[$_GET['key_value']]['ctr'];
                $edit_master = $edit_master_json_data[$_GET['key_value']];
                $edit_uut_range_min = $edit_uuc_data2[$_GET['key_value']]['uut_range_min'];
                $edit_user_id = isset($edit_uuc_data2[$_GET['key_value']]['user_id']) ? $edit_uuc_data2[$_GET['key_value']]['user_id'] : '';
                $edit_start_time = isset($edit_uuc_data2[$_GET['key_value']]['start_time']) ? $edit_uuc_data2[$_GET['key_value']]['start_time'] : '';
                $edit_stop_time = isset($edit_uuc_data2[$_GET['key_value']]['stop_time']) ? $edit_uuc_data2[$_GET['key_value']]['stop_time'] : '';
                $edit_flow_meter_size = isset($edit_uuc_data2[$_GET['key_value']]['flow_meter_size']) ? $edit_uuc_data2[$_GET['key_value']]['flow_meter_size'] : '';
                $edit_pipe_mate_thick = isset($edit_uuc_data2[$_GET['key_value']]['pipe_mate_thick']) ? $edit_uuc_data2[$_GET['key_value']]['pipe_mate_thick'] : '';
                $edit_line_size = isset($edit_uuc_data2[$_GET['key_value']]['line_size']) ? $edit_uuc_data2[$_GET['key_value']]['line_size'] : '';
                $edit_cali_fluid = isset($edit_uuc_data2[$_GET['key_value']]['cali_fluid']) ? $edit_uuc_data2[$_GET['key_value']]['cali_fluid'] : '';
                $edit_cal_reading_range_1 = isset($edit_uuc_data2[$_GET['key_value']]['cal_reading_range_1']) ? $edit_uuc_data2[$_GET['key_value']]['cal_reading_range_1'] : '';
                $edit_cal_reading_range_2 = isset($edit_uuc_data2[$_GET['key_value']]['cal_reading_range_2']) ? $edit_uuc_data2[$_GET['key_value']]['cal_reading_range_2'] : '';
                $edit_obs_type_id = isset($edit_uuc_data2[$_GET['key_value']]['obs_type_id']) ? $edit_uuc_data2[$_GET['key_value']]['obs_type_id'] : '';
                $edit_weight_id = isset($edit_uuc_data2[$_GET['key_value']]['weight_id']) ? $edit_uuc_data2[$_GET['key_value']]['weight_id'] : '';
                $edit_d1 = isset($edit_uuc_data2[$_GET['key_value']]['d1']) ? $edit_uuc_data2[$_GET['key_value']]['d1'] : '';
                $edit_d2 = isset($edit_uuc_data2[$_GET['key_value']]['d2']) ? $edit_uuc_data2[$_GET['key_value']]['d2'] : '';
                $edit_power_factor = isset($edit_uuc_data2[$_GET['key_value']]['power_factor']) ? $edit_uuc_data2[$_GET['key_value']]['power_factor'] : '';
                $edit_start_reading_of_kwh = isset($edit_uuc_data2[$_GET['key_value']]['start_reading_of_kwh']) ? $edit_uuc_data2[$_GET['key_value']]['start_reading_of_kwh'] : '';
                $edit_end_reading_of_kwh = isset($edit_uuc_data2[$_GET['key_value']]['end_reading_of_kwh']) ? $edit_uuc_data2[$_GET['key_value']]['end_reading_of_kwh'] : '';
                $edit_ac_current = isset($edit_uuc_data2[$_GET['key_value']]['ac_current']) ? $edit_uuc_data2[$_GET['key_value']]['ac_current'] : '';
                $edit_ac_voltage = isset($edit_uuc_data2[$_GET['key_value']]['ac_voltage']) ? $edit_uuc_data2[$_GET['key_value']]['ac_voltage'] : '';
                $edit_leastCount_print = $edit_uuc_data2[$_GET['key_value']]['leastCount_print'];
//                 echo "<pre>"; print_r($edit_master_json_data); exit;
                $edit_master_meter_id = $edit_master_json_data[$_GET['key_value']]['masterMeterId'];
                $edit_master_meter_id_2 = isset($edit_master_json_data[$_GET['key_value']]['masterMeterId_2']) ? $edit_master_json_data[$_GET['key_value']]['masterMeterId_2'] : '';
                $edit_master_meter_id_3 = isset($edit_master_json_data[$_GET['key_value']]['masterMeterId_3']) ? $edit_master_json_data[$_GET['key_value']]['masterMeterId_3'] : '';
                $edit_master_meter_sub_id = $edit_master_json_data[$_GET['key_value']]['masterMeterSubId'];
                $edit_master_meter_sub_id_2 = isset($edit_master_json_data[$_GET['key_value']]['masterMeterSubId_2']) ? $edit_master_json_data[$_GET['key_value']]['masterMeterSubId_2'] : '';
                $edit_master_meter_sub_id_3 = isset($edit_master_json_data[$_GET['key_value']]['masterMeterSubId_3']) ? $edit_master_json_data[$_GET['key_value']]['masterMeterSubId_3'] : '';
                $edit_master_meter_sub_sub_id = $edit_master_json_data[$_GET['key_value']]['masterMeterSubSubId'];
                $edit_master_meter_sub_sub_id_2 = isset($edit_master_json_data[$_GET['key_value']]['masterMeterSubSubId_2']) ? $edit_master_json_data[$_GET['key_value']]['masterMeterSubSubId_2'] : '';
                $edit_master_meter_sub_sub_id_3 = isset($edit_master_json_data[$_GET['key_value']]['masterMeterSubSubId_3']) ? $edit_master_json_data[$_GET['key_value']]['masterMeterSubSubId_3'] : '';
                $edit_consider_ue = isset($edit_master_json_data[$_GET['key_value']]['consider_ue']) ? $edit_master_json_data[$_GET['key_value']]['consider_ue'] : '';
                $edit_ue_unit = isset($edit_master_json_data[$_GET['key_value']]['ue_unit']) ? $edit_master_json_data[$_GET['key_value']]['ue_unit'] : '';
                $edit_humidity = isset($edit_master_json_data[$_GET['key_value']]['humidity']) ? $edit_master_json_data[$_GET['key_value']]['humidity'] : '';
                $edit_temperature = isset($edit_master_json_data[$_GET['key_value']]['temperature']) ? $edit_master_json_data[$_GET['key_value']]['temperature'] : '';
                $callibrationDate = isset($edit_master_json_data[$_GET['key_value']]['callibrationDate']) ? $edit_master_json_data[$_GET['key_value']]['callibrationDate'] : '';
                $certificateIssueDate = isset($edit_master_json_data[$_GET['key_value']]['certificateIssueDate']) ? $edit_master_json_data[$_GET['key_value']]['certificateIssueDate'] : '';
                $nextYearDate = isset($edit_master_json_data[$_GET['key_value']]['nextYearDate']) ? $edit_master_json_data[$_GET['key_value']]['nextYearDate'] : '';
                $edit_reading_json_dat = json_decode($edit_reading_json);
//                echo "<pre>"; print_r($edit_master_meter_id_2); exit;
                if (isset($_GET['obs_data_id']) && isset($_GET['key_value'])) {
                    $edit_reading_json_data1 = json_decode(json_encode($edit_reading_json_dat), true);
                    $edit_reading_json_data = json_encode($edit_reading_json_data1[$_GET['key_value']]);
                }
                $edit_uuc_json_dat = json_decode($edit_uuc_json);
                $edit_uuc_json_data1 = json_decode(json_encode($edit_uuc_json_dat), true);
                $edit_uuc_json_data = json_encode($edit_uuc_json_data1[$_GET['key_value']]);
            }
        }
        $g_where = "";
        if(!empty($edit_grn_id)){
            $g_where = " AND grnId = ".$edit_grn_id;
        }
        $selectGrn = "SELECT grnPrefix,grnNo,grnId,grnDate,grn_no FROM grnmaster WHERE grnPrefix IN ('N','R') ".$g_where." ORDER BY grn_no * 1 DESC";
        $selectGrnResult = mysql_query($selectGrn);
        $i = 0;
        while ($selectGrnRow = mysql_fetch_array($selectGrnResult)) {
            $grnId[$i] = $selectGrnRow['grnId'];
            $grnPrefixNo[$i] = $selectGrnRow['grn_no'];
            $i++;
        }
        $m_where = "";
//        if(!empty($edit_master_meter_id)){
//            $m_where = "WHERE masterMeterId = ".$edit_master_meter_id;
//        }
        $selectGrn = "SELECT masterMeterId,masterMeterModelNo,masterMeterName,procedureText
                       FROM mastermeter ".$m_where."
                      ORDER BY masterMeterName";
        $selectGrnResult = mysql_query($selectGrn);
        $j = 0;
        while ($selectGrnRow = mysql_fetch_array($selectGrnResult)) {
            $meterEntryIdArray[$j] = $selectGrnRow['masterMeterId'];
            $meterEntryNameArray[$j] = $selectGrnRow['masterMeterName'] . " : (".$selectGrnRow['masterMeterModelNo'].")";
            $j++;
        }
        $selectUser = "SELECT * FROM approved_by";
        $selectUserResult = mysql_query($selectUser);
        $j = 0;
        while ($selectUserRow = mysql_fetch_array($selectUserResult)) {
            $approveArray[$j] = $selectUserRow['id'];
            $approveNameArray[$j] = $selectUserRow['name'] . " : (".$selectUserRow['designation'].")";
            $j++;
        }
        $selectUser = "SELECT * FROM staff";
        $selectUserResult = mysql_query($selectUser);
        $j = 0;
        $userArray[$j] = '';
        $userNameArray[$j] = '';
        $j = 1;
        if($selectUserResult){
            if(mysql_num_rows($selectUserResult)>0){
                while ($selectUserRow = mysql_fetch_array($selectUserResult)) {
                    $userArray[$j] = $selectUserRow['staffId'];
                    $userNameArray[$j] = $selectUserRow['staffName'];
                    $j++;
                }
            }
        }
        $selecttb = "SELECT * FROM table_info ORDER BY cast(table_name as unsigned)";
        $selecttbResult = mysql_query($selecttb);
        $j = 0;
        if($selecttbResult && mysql_num_rows($selecttbResult)>0){
            while ($selecttbRow = mysql_fetch_array($selecttbResult)) {
                $tableArray[$j] = $selecttbRow;
                $tableNameArray[$j] = $selecttbRow['table_name'];
                $j++;
            }
        }
        $selecttb = "SELECT * FROM weight_unit";
        $selecttbResult = mysql_query($selecttb);
        $j = 0;
        if($selecttbResult && mysql_num_rows($selecttbResult)>0){
            while ($selecttbRow = mysql_fetch_array($selecttbResult)) {
                $weightArray[$j] = $selecttbRow;
                $weightNameArray[$j] = $selecttbRow['unit_name'];
                $j++;
            }
        }
        $selecttb = "SELECT * FROM accuracy_class";
        $selecttbResult = mysql_query($selecttb);
        $j = 0;
        while ($selecttbRow = mysql_fetch_array($selecttbResult)) {
            $ctArray[$j] = $selecttbRow;
            $j++;
        }
//echo "<pre>"; print_r($tableArray); exit;
        if(empty($nextYearDate)){
            $nextYearDate = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d") - 1 , date("Y") + 1));
        }
        $certificateIssue = "N";
        if(empty($certificateIssueDate)){
            $certificateIssueDate = date("Y-m-d");
        }

        include ("./bottom.php");
        $smarty->assign("meterEntryIdArray", $meterEntryIdArray);
        $smarty->assign("meterEntryNameArray", $meterEntryNameArray);
        $smarty->assign("obs_data_id", $obs_data_id);
        $smarty->assign("obs_id", $obs_id);
        $smarty->assign("obs_id2", $obs_id);
        $smarty->assign("obs_key", $obs_key);
        $smarty->assign("obs_data_key", $obs_data_key);
        $smarty->assign("edit_master_meter_id", $edit_master_meter_id);
        $smarty->assign("edit_master_meter_id_2", $edit_master_meter_id_2);
        $smarty->assign("edit_master_meter_id_3", $edit_master_meter_id_3);
        $smarty->assign("edit_master_meter_sub_id", $edit_master_meter_sub_id);
        $smarty->assign("edit_master_meter_sub_id_2", $edit_master_meter_sub_id_2);
        $smarty->assign("edit_master_meter_sub_id_3", $edit_master_meter_sub_id_3);
        $smarty->assign("edit_master_meter_sub_sub_id", $edit_master_meter_sub_sub_id);
        $smarty->assign("edit_master_meter_sub_sub_id_2", $edit_master_meter_sub_sub_id_2);
        $smarty->assign("edit_master_meter_sub_sub_id_3", $edit_master_meter_sub_sub_id_3);
        $smarty->assign("edit_consider_ue", $edit_consider_ue);
        $smarty->assign("edit_ue_unit", $edit_ue_unit);
        $smarty->assign("edit_grn_id", $edit_grn_id);
        $smarty->assign("edit_grn_detail_id", $edit_grn_detail_id);
        $smarty->assign("edit_uuc_data", $edit_uuc_data);
        $smarty->assign("edit_leastCount", $edit_leastCount);
        $smarty->assign("edit_accuracy", $edit_accuracy);
        $smarty->assign("edit_certiRemarks", $edit_certiRemarks);
        $smarty->assign("edit_voltage", $edit_voltage);
        $smarty->assign("edit_ocillation", $edit_ocillation);
        $smarty->assign("edit_least_count_for_rpm", $edit_least_count_for_rpm);
        $smarty->assign("edit_col_head_1_least_count", $edit_col_head_1_least_count);
        $smarty->assign("edit_uncertainty_calibration", $edit_uncertainty_calibration);
        $smarty->assign("edit_frequency", $edit_frequency);
        $smarty->assign("edit_rpm", $edit_rpm);
        $smarty->assign("edit_makeModel", $edit_makeModel);
        $smarty->assign("edit_instrumentId", $edit_instrumentId);
        $smarty->assign("edit_uut_range", $edit_uut_range);
        $smarty->assign("edit_uuc_range_print_table", $edit_uuc_range_print_table);
        $smarty->assign("edit_leastCount_print", $edit_leastCount_print);
        $smarty->assign("edit_uuc_location", $edit_uuc_location);
        $smarty->assign("edit_table_no", $edit_table_no);
        $smarty->assign("edit_uut_range_min", $edit_uut_range_min);
        $smarty->assign("edit_table_title_right", $edit_table_title_right);
        $smarty->assign("edit_uut_range_sec", $edit_uut_range_sec);
        $smarty->assign("edit_ratio_bottom_part", $edit_ratio_bottom_part);
        $smarty->assign("edit_ratio_bottom_part_range", $edit_ratio_bottom_part_range);
        $smarty->assign("edit_uuc_range_range", $edit_uuc_range_range);
        $smarty->assign("edit_burden_6b", $edit_burden_6b);
        $smarty->assign("edit_ratio_6b", $edit_ratio_6b);
        $smarty->assign("edit_col_head_1", $edit_col_head_1);
        $smarty->assign("edit_span_in", $edit_span_in);
        $smarty->assign("edit_uuc_range_2", $edit_uuc_range_2);
        $smarty->assign("edit_cali_location", $edit_cali_location);
        $smarty->assign("edit_ct_class", $edit_ct_class);
        $smarty->assign("edit_confidence_level", $edit_confidence_level);
        $smarty->assign("edit_covarage_factor", $edit_covarage_factor);
        $smarty->assign("edit_sensor", $edit_sensor);
        $smarty->assign("edit_humidity", $edit_humidity);
        $smarty->assign("edit_temperature", $edit_temperature);
        $smarty->assign("edit_repeatibility", $edit_repeatibility);
        $smarty->assign("edit_temp_of_water", $edit_temp_of_water);
        $smarty->assign("edit_phase_type_id", $edit_phase_type_id);
        $smarty->assign("edit_ctr", $edit_ctr);
        $smarty->assign("edit_start_time", $edit_start_time);
        $smarty->assign("edit_stop_time", $edit_stop_time);
        $smarty->assign("edit_flow_meter_size", $edit_flow_meter_size);
        $smarty->assign("edit_line_size", $edit_line_size);
        $smarty->assign("edit_pipe_mate_thick", $edit_pipe_mate_thick);
        $smarty->assign("edit_cali_fluid", $edit_cali_fluid);
        $smarty->assign("edit_cal_reading_range_1", $edit_cal_reading_range_1);
        $smarty->assign("edit_cal_reading_range_2", $edit_cal_reading_range_2);
        $smarty->assign("edit_class", $edit_class);
        $smarty->assign("edit_power_factor", $edit_power_factor);
        $smarty->assign("edit_start_reading_of_kwh", $edit_start_reading_of_kwh);
        $smarty->assign("edit_end_reading_of_kwh", $edit_end_reading_of_kwh);
        $smarty->assign("edit_ac_current", $edit_ac_current);
        $smarty->assign("edit_ac_voltage", $edit_ac_voltage);
        $smarty->assign("edit_cos", $edit_cos);
        $smarty->assign("edit_obs_type_id", $edit_obs_type_id);
        $smarty->assign("edit_weight_id", $edit_weight_id);
        $smarty->assign("edit_d1", $edit_d1);
        $smarty->assign("edit_d2", $edit_d2);
        $smarty->assign("edit_master", $edit_master);
        $smarty->assign("edit_table_info_id", $edit_table_info_id);
        $smarty->assign("edit_user_id", $edit_user_id);
        $smarty->assign("edit_reading_json_data", $edit_reading_json_data);
        $smarty->assign("edit_uuc_json_data", $edit_uuc_json_data);
        $smarty->assign("approveArray", $approveArray);
        $smarty->assign("approveNameArray", $approveNameArray);
        $smarty->assign("userArray", $userArray);
        $smarty->assign("userNameArray", $userNameArray);
        $smarty->assign("tableArray", $tableArray);
        $smarty->assign("tableNameArray", $tableNameArray);
        $smarty->assign("weightArray", $weightArray);
        $smarty->assign("weightNameArray", $weightNameArray);
        $smarty->assign("ctArray", $ctArray);
        $smarty->assign("ctNameArray", $ctNameArray);
        $smarty->assign("grnId", $grnId);
        $smarty->assign("grnDetailId", $grnDetailId);
        $smarty->assign("stdMeter1", $stdMeter1);
        $smarty->assign("testMeter1", $testMeter1);
        $smarty->assign("stdMeter2", $stdMeter2);
        $smarty->assign("testMeter2", $testMeter2);
        $smarty->assign("stdMeter3", $stdMeter3);
        $smarty->assign("testMeter3", $testMeter3);
        $smarty->assign("stdMeter4", $stdMeter4);
        $smarty->assign("testMeter4", $testMeter4);
        $smarty->assign("stdMeter5", $stdMeter5);
        $smarty->assign("testMeter5", $testMeter5);
        $smarty->assign("stdMeterAverage", $stdMeterAverage);
        $smarty->assign("testMeterAverage", $testMeterAverage);
        $smarty->assign("accuracyTaken", $accuracyTaken);
        $smarty->assign("standardDeviation", $standardDeviation);
        $smarty->assign("standardUncertinity", $standardUncertinity);
        $smarty->assign("standardUncertinityperc", $standardUncertinityperc);
        $smarty->assign("degreeOfFreedom", $degreeOfFreedom);
        $smarty->assign("uncertinityForTypeB", $uncertinityForTypeB);
        $smarty->assign("degree_of_freedom_add", $degree_of_freedom_add);
        $smarty->assign("uncertinityInPercentage", $uncertinityInPercentage);
        $smarty->assign("accuracyForTypeB", $accuracyForTypeB);
        $smarty->assign("acuuracyForTypeBPerc", $acuuracyForTypeBPerc);
        $smarty->assign("resolutionTypeB", $resolutionTypeB);
        $smarty->assign("resolutionForTypeBPerc", $resolutionForTypeBPerc);
        $smarty->assign("stabilityForTypeB", $stabilityForTypeB);
        $smarty->assign("stabilityForTypeBInPerc", $stabilityForTypeBInPerc);
        $smarty->assign("combinedUncertinity", $combinedUncertinity);
        $smarty->assign("stability_of_source", $stability_of_source);
        $smarty->assign("combinedUncertinityInPerc", $combinedUncertinityInPerc);
        $smarty->assign("effectiveUncertinity", $effectiveUncertinity);
        $smarty->assign("effectiveUncertinityInPer", $effectiveUncertinityInPer);
        $smarty->assign("effectiveDegreeOfFreed", $effectiveDegreeOfFreed);
        $smarty->assign("meanReading", $meanReading);
        $smarty->assign("masterMeterReading", $masterMeterReading);
        $smarty->assign("error", $error);
        $smarty->assign("expandedUncertinity", $expandedUncertinity);
        $smarty->assign("grnPrefixNo", $grnPrefixNo);
        $smarty->assign("grnDetailPassId", $grnDetailPassId);
        $smarty->assign("callibrationDate", $callibrationDate);
        $smarty->assign("nextYearDate", $nextYearDate);
        $smarty->assign("certificateIssue", $certificateIssue);
        $smarty->assign("certificateIssueDate", $certificateIssueDate);
        $smarty->assign("humidity", $humidity);
        //$smarty->display("observationSheet.tpl");
        $smarty->display("observationSheet2_new.tpl");
    } else {
        header("Location:index.php");
    }
}

?>