<?php
include("include/omConfig.php");
$grnPrefixNo               = "";
$grnId                     = "";
$meterEntryNameArray       = "";
$meterEntryNameArray       = "";
$grnDetailId               = "";
$stdMeter1                 = "";
$testMeter1                = "";
$stdMeter2                 = "";
$testMeter2                = "";
$stdMeter3                 = "";
$testMeter3                = "";
$stdMeter4                 = "";
$testMeter4                = "";
$stdMeter5                 = "";
$testMeter5                = "";
$stdMeterAverage           = "";
$testMeterAverage          = "";
$accuracyTaken             = "";
$standardDeviation         = "";
$standardUncertinity       = "";
$standardUncertinityperc   = "";
$degreeOfFreedom           = "";
$uncertinityForTypeB       = "";
$uncertinityInPercentage   = "";
$accuracyForTypeB          = "";
$acuuracyForTypeBPerc      = "";
$resolutionTypeB           = "";
$resolutionForTypeBPerc    = "";
$stabilityForTypeB         = "";
$stabilityForTypeBInPerc   = "";
$combinedUncertinity       = "";
$combinedUncertinityInPerc = "";
$effectiveUncertinity      = "";
$effectiveUncertinityInPer = "";
$effectiveDegreeOfFreed    = "";
$meanReading               = "";
$masterMeterReading        = "";
$error                     = "";
$expandedUncertinity       = "";
$humidity                  = "";
$masterMeterSubSubId       = "";
$callibrationDate          = "";
$meterEntryIdArray         = array();
$nextYearDate              = "";
$grnObsMasterId            = 0;
if(isset($_GET['grnDetailPassId']) && $_GET['grnDetailPassId'] > 0)
  $grnDetailPassId = $_POST['grnDetailPassId'] ;
//else if(isset($_POST['grnDetailId'])) 
//  $grnDetailPassId = $_POST['grnDetailId'];
else
  $grnDetailPassId = 0;
  
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['insertBtn']))
	{
    $callibrationDate  = ($_POST['callibrationDateYear']."-".$_POST['callibrationDateMonth']."-".$_POST['callibrationDateDay']);
	  $nextYearDate      = ($_POST['nextYearDateYear']."-".$_POST['nextYearDateMonth']."-".$_POST['nextYearDateDay']);
	  
	  //Check whether range is already present : starts
	  $selRange = "SELECT grnObsMasterId 
	                 FROM grnobsmaster 
	                WHERE grnId = ".$_POST['grnId']."
	                  AND grnDetailId = ".$_POST['grnDetailId']."
	                  AND mParameterId = ".$_POST['masterMeterSubId'];
	  $selRangeRes = mysql_query($selRange);
	  if(mysql_num_rows($selRangeRes) > 0)
    {
    	if($mId = mysql_fetch_array($selRangeRes))
      {
      	$grnObsMasterId = $mId['grnObsMasterId'];
      }
	  }
	  else
	  {
  	  // Insert into observation master : Starts
  	  $insertMaster = "INSERT INTO grnobsmaster(grnId, grnDetailId, masterMeterId, mParameterId, mRangeId, grnObsDate, userName,selfCertiNo)
  	                   VALUES (".$_POST['grnId'].",".$_POST['grnDetailId'].",".$_POST['masterMeterId'].",".$_POST['masterMeterSubId'].",'".$_POST['masterMeterSubSubId']."','".$callibrationDate."', '".$_SESSION['s_activId']."','".$_POST['selfCertiNo']."')";
  	  $insertMasterRes = mysql_query($insertMaster);
  	  if(!$insertMasterRes)
  	      die("Insert Query Not Inserted 1: ".mysql_error(). " : ".$insertMaster);
  	  else
  	    $grnObsMasterId = mysql_insert_id();
  	  // Insert into observation master : Ends
  	}
	  //Check whether range is already present : ends
  	 
	    // Update GRN Observation Detail : Starts
	    // $obsDate        = $_POST['obsDateYear']."-".$_POST['obsDateMonth']."-".$_POST['obsDateDay'];
	    if(isset($_POST['extraFields']) && $_POST['extraFields'] == 1)
	    {
	    	$extraFields = $_POST['extraFields'];
	    	$extraField1 = isset($_POST['extraField1']) && $_POST['extraField1'] != "" ? $_POST['extraField1'] : NULL;
	    	$extraField2 = isset($_POST['extraField2']) && $_POST['extraField2'] != "" ? $_POST['extraField2'] : NULL;
	    	$extraField3 = isset($_POST['extraField3']) && $_POST['extraField3'] != "" ? $_POST['extraField3'] : NULL;
	    	$extraField4 = isset($_POST['extraField4']) && $_POST['extraField4'] != "" ? $_POST['extraField4'] : NULL;
	    	$extraField5 = isset($_POST['extraField5']) && $_POST['extraField5'] != "" ? $_POST['extraField5'] : NULL;
	    	$extraField6 = isset($_POST['extraField6']) && $_POST['extraField6'] != "" ? $_POST['extraField6'] : NULL;
	    	
	    	$extraQuery = "  , extraFields = ".$extraFields.", extraField1 = '".$extraField1."', extraField2 = '".$extraField2."', extraField3 = '".$extraField3."', extraField4 = '".$extraField4."', extraField5 = '".$extraField5."', extraField6 = '".$extraField6."' ";
	    }
	    else
	    {
	    	$extraQuery = "";
	    }
	    
	    $ratio1 = ($_POST['ratio1'] == "") ? 1 : $_POST['ratio1'];
	    $ratio2 = ($_POST['ratio2'] == "") ? 1 : $_POST['ratio2'];
	    
	    $updateGrnEntry = "UPDATE grndetail
	                          SET refGrnDetailId = '".$grnDetailPassId."',masterMeterId = '".$_POST['masterMeterId']."',callibrationDate ='".$callibrationDate."',
	                              nextYearDate = '".$nextYearDate."', makeModel = '".$_POST['makeModel']."',
	                              instrumentId = '".$_POST['instrumentId']."',leastCount = '".$_POST['leastCount']."',
	                              accuracy = '".$_POST['accuracy']."',temperature = '".$_POST['temperature']."',
	                              humidity = '".$_POST['humidity']."', masterParameterId ='".$_POST['masterMeterSubId']."',
	                              masterRangeId = '".$_POST['masterMeterSubSubId']."',userName = '".$_SESSION['s_activId']."' ".
	                              $extraQuery.",certiRemarks = '".$_POST['certiRemarks']."',
	                              ratio1 = ".$ratio1.", ratio2 = ".$ratio2."
	                        WHERE grnDetailId = ".$_POST['grnDetailId'];
	    $updateGrnEntryResult = mysql_query($updateGrnEntry);
	    if(!$updateGrnEntryResult)
	      die("Insert Query Not Inserted 1: ".mysql_error(). " : ".$updateGrnEntry);
	    // Update GRN Observation Detail : Starts
	
	    /////// Insert Query for GRN Observation Detail : Statrs
	    $loopCount = 0;
	    while($loopCount < count($_POST['stdMeter1']))
	    {
	      $uncertaintyCalibration    = ($_POST['uncertaintyCalibration'][$loopCount] != '') ? $_POST['uncertaintyCalibration'][$loopCount]   : 0;
	      $uncertaintyCalibrationInPer = ($_POST['uncertaintyCalibrationInPer'][$loopCount] != '') ? $_POST['uncertaintyCalibrationInPer'][$loopCount]   : 0;
	      $resolutionTypeA           = ($_POST['resolutionTypeA'][$loopCount] != '') ? $_POST['resolutionTypeA'][$loopCount]   : 0;
	      
	      $stdMeter1                 = ($_POST['stdMeter1'][$loopCount] != '') ? $_POST['stdMeter1'][$loopCount]   : 0;
	      $testMeter1                = ($_POST['testMeter1'][$loopCount] != '') ? $_POST['testMeter1'][$loopCount] : 0;
	      $stdMeter2                 = ($_POST['stdMeter2'][$loopCount] != '') ? $_POST['stdMeter2'][$loopCount]   : 0;
	      $testMeter2                = ($_POST['testMeter2'][$loopCount] != '') ? $_POST['testMeter2'][$loopCount] : 0;
	      $stdMeter3                 = ($_POST['stdMeter3'][$loopCount] != '') ? $_POST['stdMeter3'][$loopCount]   : 0;
	      $testMeter3                = ($_POST['testMeter3'][$loopCount] != '') ? $_POST['testMeter3'][$loopCount] : 0;
	      $stdMeter4                 = ($_POST['stdMeter4'][$loopCount] != '') ? $_POST['stdMeter4'][$loopCount]   : 0;
	      $testMeter4                = ($_POST['testMeter4'][$loopCount] != '') ? $_POST['testMeter4'][$loopCount] : 0;
	      $stdMeter5                 = ($_POST['stdMeter5'][$loopCount] != '') ? $_POST['stdMeter5'][$loopCount]   : 0;
	      $testMeter5                = ($_POST['testMeter5'][$loopCount] != '') ? $_POST['testMeter5'][$loopCount]   : 0;
	      $stdMeterAverage           = ($_POST['stdMeterAverage'][$loopCount] != '') ? $_POST['stdMeterAverage'][$loopCount] : 0;
	      $testMeterAverage          = ($_POST['testMeterAverage'][$loopCount] != '') ? $_POST['testMeterAverage'][$loopCount] : 0;
	      $accuracyTaken             = ($_POST['accuracyTaken'][$loopCount] != '') ? $_POST['accuracyTaken'][$loopCount] : 0;
	      $standardDeviation         = ($_POST['standardDeviation'][$loopCount] != '') ? $_POST['standardDeviation'][$loopCount] : 0;
	      $standardUncertinity       = ($_POST['standardUncertinity'][$loopCount] != '') ? $_POST['standardUncertinity'][$loopCount] : 0;
	      $standardUncertinityperc   = ($_POST['standardUncertinityperc'][$loopCount] != '') ? $_POST['standardUncertinityperc'][$loopCount] : 0;
	      $degreeOfFreedom           = ($_POST['degreeOfFreedom'][$loopCount] != '') ? $_POST['degreeOfFreedom'][$loopCount] : 0;
	      $uncertinityForTypeB       = ($_POST['uncertinityForTypeB'][$loopCount] != '') ? $_POST['uncertinityForTypeB'][$loopCount] : 0;
	      $uncertinityInPercentage   = ($_POST['uncertinityInPercentage'][$loopCount] != '') ? $_POST['uncertinityInPercentage'][$loopCount] : 0;
	      $accuracyForTypeB          = ($_POST['accuracyForTypeB'][$loopCount] != '') ? $_POST['accuracyForTypeB'][$loopCount] : 0;
	      $acuuracyForTypeBPerc      = ($_POST['acuuracyForTypeBPerc'][$loopCount] != '') ? $_POST['acuuracyForTypeBPerc'][$loopCount] : 0;
	      $resolutionTypeB           = ($_POST['resolutionTypeB'][$loopCount] != '') ? $_POST['resolutionTypeB'][$loopCount] : 0;
	      $resolutionForTypeBPerc    = ($_POST['resolutionForTypeBPerc'][$loopCount] != '') ? $_POST['resolutionForTypeBPerc'][$loopCount] : 0;
	      $stabilityForTypeB         = ($_POST['stabilityForTypeB'][$loopCount] != '') ? $_POST['stabilityForTypeB'][$loopCount] : 0;
	      $stabilityForTypeBInPerc   = ($_POST['stabilityForTypeBInPerc'][$loopCount] != '') ? $_POST['stabilityForTypeBInPerc'][$loopCount] : 0;
	      $combinedUncertinity       = ($_POST['combinedUncertinity'][$loopCount] != '') ? $_POST['combinedUncertinity'][$loopCount] : 0;
	      $combinedUncertinityInPerc = ($_POST['combinedUncertinityInPerc'][$loopCount] != '') ? $_POST['combinedUncertinityInPerc'][$loopCount] : 0;
	      $effectiveUncertinity      = ($_POST['effectiveUncertinity'][$loopCount] != '') ? $_POST['effectiveUncertinity'][$loopCount] : 0;
	      $effectiveUncertinityInPer = ($_POST['effectiveUncertinityInPer'][$loopCount] != '') ? $_POST['effectiveUncertinityInPer'][$loopCount] : 0;
	      $effectiveDegreeOfFreed    = ($_POST['effectiveDegreeOfFreed'][$loopCount] != '') ? $_POST['effectiveDegreeOfFreed'][$loopCount] : 0;
	      $meanReading               = ($_POST['meanReading'][$loopCount] != '') ? $_POST['meanReading'][$loopCount] : 0;
	      $masterMeterReading        = ($_POST['masterMeterReading'][$loopCount] != '') ? $_POST['masterMeterReading'][$loopCount] : 0;
	      $error                     = ($_POST['error'][$loopCount] != '') ? $_POST['error'][$loopCount] : 0;
	      $expandedUncertinity       = ($_POST['expandedUncertinity'][$loopCount] != '') ? $_POST['expandedUncertinity'][$loopCount] : 0;
	      $expandedUncertinityInPre  = ($_POST['expandedUncertinityInPre'][$loopCount] != '') ? $_POST['expandedUncertinityInPre'][$loopCount] : 0;
	      $humidity                  = ($_POST['humidity'] != '') ? $_POST['humidity'] : 0;
	      $measurementUncertaintyIs  = isset($_POST['measurementUncertaintyIs']) ? $_POST['measurementUncertaintyIs'] : "";
	      
        ////////////////////
        $testMeter1Len = strlen($testMeter1);
        $decimalPointLen    = 1;//decimalPoint has strlen is 1 
        $testMeter1BeforeDecimalLen = strlen(substr($testMeter1,0,strpos($testMeter1,".")));
        //if 0.0001 ==>> $testMeter1Len = 1, $testMeter1BeforeDecimalLen = 2
        $stdMeter1AfterDecimal = ($testMeter1Len - $decimalPointLen - $testMeter1BeforeDecimalLen);
        ////////////////////
        ////////////////////
        $resolutionTypeALen = strlen($resolutionTypeA);
        $decimalPointLen    = 1;//decimalPoint has strlen is 1 
        $resolutionTypeABeforeDecimalLen = strlen(substr($resolutionTypeA,0,strpos($resolutionTypeA,".")));
        //if 0.0001 ==>> $stdMeter1Len = 1, $stdMeter1BeforeDecimalLen = 2
        $resolutionTypeAAfterDecimal = ($resolutionTypeALen - $decimalPointLen - $resolutionTypeABeforeDecimalLen);
        ////////////////////
        
	      if($_POST['stdMeter1'][$loopCount] != "" || $_POST['testMeter1'][$loopCount] != "" ||
	         $_POST['stdMeter2'][$loopCount] != "" || $_POST['testMeter2'][$loopCount] != "" ||
	         $_POST['stdMeter3'][$loopCount] != "" || $_POST['testMeter3'][$loopCount] != "" ||
	         $_POST['stdMeter4'][$loopCount] != "" || $_POST['testMeter4'][$loopCount] != "" ||
	         $_POST['stdMeter5'][$loopCount] != "" || $_POST['testMeter5'][$loopCount] != "" ||
	         $_POST['stdMeterAverage'][$loopCount] != "" || $_POST['testMeterAverage'][$loopCount] != ""  )
	      { 
	        $callibrationDate     = ($_POST['callibrationDateYear']."-".$_POST['callibrationDateMonth']."-".$_POST['callibrationDateDay']);
	        $nextYearDate         = ($_POST['nextYearDateYear']."-".$_POST['nextYearDateMonth']."-".$_POST['nextYearDateDay']);	      	
	        $insertQueryGrnDetail = "INSERT INTO grnobservation (grnObsMasterId,grnId,grnDetailId,uncertaintyCalibration,uncertaintyCalibrationInPer,resolutionTypeA,stdMeter1AfterDecimal,resolutionTypeAAfterDecimal,stdMeter1,testMeter1,stdMeter2,testMeter2,stdMeter3,testMeter3,stdMeter4,
	                                                             testMeter4,stdMeter5,testMeter5,stdMeterAverage,testMeterAverage,accuracyTaken,standardDeviation,
	                                                             standardUncertinity,standardUncertinityperc,degreeOfFreedom,uncertinityForTypeB,uncertinityInPercentage,
	                                                             accuracyForTypeB,acuuracyForTypeBPerc,resolutionTypeB,resolutionForTypeBPerc,stabilityForTypeB,
	                                                             stabilityForTypeBInPerc,combinedUncertinity,combinedUncertinityInPerc,effectiveUncertinity,effectiveUncertinityInPer,
	                                                             effectiveDegreeOfFreed,meanReading,masterMeterReading,error,expandedUncertinity,expandedUncertinityInPre)
	                                 VALUES ('".$grnObsMasterId."',".$_POST['grnId'].",".$_POST['grnDetailId'].",".$uncertaintyCalibration.",".$uncertaintyCalibrationInPer.",".$resolutionTypeA.",".$stdMeter1AfterDecimal.",".$resolutionTypeAAfterDecimal.",".$stdMeter1.",'".$testMeter1."','".$stdMeter2."',
	                                        '".$testMeter2."','".$stdMeter3."','".$testMeter3."','".$stdMeter4."',".$testMeter4.",
	                                         ".$stdMeter5.",'".$testMeter5."','".$stdMeterAverage."','".$testMeterAverage."','".$accuracyTaken."',
	                                         '".$standardDeviation."','".$standardUncertinity."','".$standardUncertinityperc."','".$degreeOfFreedom."',
	                                         '".$uncertinityForTypeB."','".$uncertinityInPercentage."','".$accuracyForTypeB."','".$acuuracyForTypeBPerc."',
	                                         '".$resolutionTypeB."','".$resolutionForTypeBPerc."','".$stabilityForTypeB."','".$stabilityForTypeBInPerc."',
	                                         '".$combinedUncertinity."','".$combinedUncertinityInPerc."','".$effectiveUncertinity."','".$effectiveUncertinityInPer."',
	                                         '".$effectiveDegreeOfFreed."','".$meanReading."','".$masterMeterReading."','".$error."','".$expandedUncertinity."','".$expandedUncertinityInPre."')";
	        $insertQueryGrnDetailResult = mysql_query($insertQueryGrnDetail);
	        $grnDetailId = mysql_insert_id();
	      }
	      $loopCount++;
	    }
	    ////// Insert Query for GRN Observation Detail : Ends
	  
	}
	$selectGrn = "SELECT grnPrefix,grnNo,grnId,grnDate
                  FROM grnmaster
                 WHERE grnPrefix = 'N'
              ORDER BY grnNo * 1 DESC";
  $selectGrnResult = mysql_query($selectGrn);
  $i = 0;
  while($selectGrnRow = mysql_fetch_array($selectGrnResult))
  {
      $grnId[$i]       = $selectGrnRow['grnId'];
      $grnPrefixNo[$i] = $selectGrnRow['grnNo'];
      $i++;
  }


  $selectGrn = "SELECT masterMeterId,masterMeterName,procedureText
                  FROM mastermeter
                 ORDER BY masterMeterName";
  $selectGrnResult = mysql_query($selectGrn);
  $j = 0;
  while($selectGrnRow = mysql_fetch_array($selectGrnResult))
  {
      $meterEntryIdArray[$j]   = $selectGrnRow['masterMeterId'];
      $meterEntryNameArray[$j] = $selectGrnRow['masterMeterName']." : ".$selectGrnRow['procedureText'];
      $j++;
  }
	  ////
	  $nextYearDate = date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y")+1));
	  ////
	  include("./bottom.php");
	  $smarty->assign("meterEntryIdArray",$meterEntryIdArray);
	  $smarty->assign("meterEntryNameArray",$meterEntryNameArray);
	  $smarty->assign("grnId",$grnId);
	  $smarty->assign("grnDetailId",$grnDetailId);
	  $smarty->assign("stdMeter1",$stdMeter1);
	  $smarty->assign("testMeter1",$testMeter1);
	  $smarty->assign("stdMeter2",$stdMeter2);
	  $smarty->assign("testMeter2",$testMeter2);
	  $smarty->assign("stdMeter3",$stdMeter3);
	  $smarty->assign("testMeter3",$testMeter3);
	  $smarty->assign("stdMeter4",$stdMeter4);
	  $smarty->assign("testMeter4",$testMeter4);
	  $smarty->assign("stdMeter5",$stdMeter5);
	  $smarty->assign("testMeter5",$testMeter5);
	  $smarty->assign("stdMeterAverage",$stdMeterAverage);
	  $smarty->assign("testMeterAverage",$testMeterAverage);
	  $smarty->assign("accuracyTaken",$accuracyTaken);
	  $smarty->assign("standardDeviation",$standardDeviation);
	  $smarty->assign("standardUncertinity",$standardUncertinity);
	  $smarty->assign("standardUncertinityperc",$standardUncertinityperc);
	  $smarty->assign("degreeOfFreedom",$degreeOfFreedom);
	  $smarty->assign("uncertinityForTypeB",$uncertinityForTypeB);
	  $smarty->assign("uncertinityInPercentage",$uncertinityInPercentage);
	  $smarty->assign("accuracyForTypeB",$accuracyForTypeB);
	  $smarty->assign("acuuracyForTypeBPerc",$acuuracyForTypeBPerc);
	  $smarty->assign("resolutionTypeB",$resolutionTypeB);
	  $smarty->assign("resolutionForTypeBPerc",$resolutionForTypeBPerc);
	  $smarty->assign("stabilityForTypeB",$stabilityForTypeB);
	  $smarty->assign("stabilityForTypeBInPerc",$stabilityForTypeBInPerc);
	  $smarty->assign("combinedUncertinity",$combinedUncertinity);
	  $smarty->assign("combinedUncertinityInPerc",$combinedUncertinityInPerc);
	  $smarty->assign("effectiveUncertinity",$effectiveUncertinity);
	  $smarty->assign("effectiveUncertinityInPer",$effectiveUncertinityInPer);
	  $smarty->assign("effectiveDegreeOfFreed",$effectiveDegreeOfFreed);
	  $smarty->assign("meanReading",$meanReading);
	  $smarty->assign("masterMeterReading",$masterMeterReading);
	  $smarty->assign("error",$error);
	  $smarty->assign("expandedUncertinity",$expandedUncertinity);
	  $smarty->assign("grnPrefixNo",$grnPrefixNo);
	  $smarty->assign("grnDetailPassId",$grnDetailPassId);
	  $smarty->assign("callibrationDate",$callibrationDate);
	  $smarty->assign("nextYearDate",$nextYearDate);
	  $smarty->assign("humidity",$humidity);
	  //$smarty->display("observationSheet.tpl");
	  $smarty->display("observationSheet2.tpl");
}
?>