<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(MASTER_METER_SUB_MODULE_ID,"view"))){
	$isEdit              = 0;
	$editArray           = 0;
	//$editArray['masterMeterSubId'] = 0;
  $masterMeterId       = "";
  $masterMeterName     = "";
  $masterMeterExpDate  = "";
  $parameter           = "";
  $range               = "";
  $range_disp               = "";
  $accuracy            = 0;
  $accuracySubSub      = 0;
  $accuracySubSubFS    = 0;
  $accuracy_main    = 0;
  $range_unit          = "";
  $frequency           = "";
  $uncertinity         = 0;
  $certiAccuracy       = 0;
  $degreeOfFreedom     = 0;
  $accuracy            = 0;
  $resolution          = 0;
  $coverage_factor          = 0;
  $cmc          = 0;
  $stability           = 0;
  $masterMeterExp      = "";
  $meterEntry          = array();
  $meterEntryNameArray = array();
  $meterEntryIdArray   = array();
  $meterEntryExpArray  = isset($_REQUEST['masterMeterId']) ? $_REQUEST['masterMeterId'] : 0;
  // Master Meter Name Entry : Starts
//echo "<pre>"; print_r($_POST); exit;
  if(isset($_POST['masterMeterSubSubId']))
  { 
//      echo "<pre>"; print_r($_POST); exit;
      $masterMeterSubId = $_POST['masterMeterSubId'];
      $loopCountSubSub = 0;
		  while($loopCountSubSub < count($_POST['range']))
		  {
		    $range            =  ($_POST['range'][$loopCountSubSub] != '') ? $_POST['range'][$loopCountSubSub] : 0;
		    $range_disp            =  ($_POST['range_disp'][$loopCountSubSub] != '') ? $_POST['range_disp'][$loopCountSubSub] : 0;
		    $accuracySubSub   =  ($_POST['accuracySubSub'][$loopCountSubSub] != '') ? $_POST['accuracySubSub'][$loopCountSubSub] : 0;
		    $accuracySubSubFS =  ($_POST['accuracySubSubFS'][$loopCountSubSub] != '') ? $_POST['accuracySubSubFS'][$loopCountSubSub] : 0;
		    $accuracy_main =  ($_POST['accuracy_main'][$loopCountSubSub] != '') ? $_POST['accuracy_main'][$loopCountSubSub] : 0;
		    $range_unit       =  ($_POST['range_unit'][$loopCountSubSub] != '') ? $_POST['range_unit'][$loopCountSubSub] : '';
		    $frequency       =  ($_POST['frequency'][$loopCountSubSub] != '') ? $_POST['frequency'][$loopCountSubSub] : '';
		    $resolution       =  ($_POST['resolution'][$loopCountSubSub] != '') ? $_POST['resolution'][$loopCountSubSub] : 0;
		    $coverage_factor       =  ($_POST['coverage_factor'][$loopCountSubSub] != '') ? $_POST['coverage_factor'][$loopCountSubSub] : 0;
		    $cmc       =  ($_POST['cmc'][$loopCountSubSub] != '') ? $_POST['cmc'][$loopCountSubSub] : 0;
		    $stability        =  ($_POST['stability'][$loopCountSubSub] != '') ? $_POST['stability'][$loopCountSubSub] : 0;
		    $uncertinty       =  ($_POST['uncertinty'][$loopCountSubSub] != '') ? $_POST['uncertinty'][$loopCountSubSub] : 0;
		    $certiAccuracy    =  0;
		    $degreeOfFreedom  =  ($_POST['degreeOfFreedom'][$loopCountSubSub] != '') ? $_POST['degreeOfFreedom'][$loopCountSubSub] : 0;
		    
		    if(isset($_POST['masterMeterSubSubId']) && $_POST['masterMeterSubSubId'] > 0)
		    {
		    	$updateSubSub = "UPDATE mastermetersubsub
		    	                    SET rangeValue            = '".$range."',
		    	                        range_disp   = '".$range_disp."',
		    	                        accuracySubSub   = '".$accuracySubSub."',
  		                            accuracySubSubFS = '".$accuracySubSubFS."',
  		                            accuracy_main = '".$accuracy_main."',
  		                            range_unit = '".$range_unit."',
  		                            frequency = '".$frequency."',
  		                            resolution       = '".$resolution."',
  		                            coverage_factor       = '".$coverage_factor."',
  		                            cmc       = '".$cmc."',
  		                            stability        = '".$stability."',
  		                            uncertinty       = '".$uncertinty."',
  		                            certiAccuracy    = '".$certiAccuracy."',
  		                            degreeOfFreedom  = '".$degreeOfFreedom."'
  		                      WHERE masterMeterSubSubId = " .$_POST['masterMeterSubSubId'];
  		    $updateSubSubRes = mysql_query($updateSubSub);
  		    if(!$updateSubSubRes)
  		    {
  		    	die("Insert Query Not Inserted : ".mysql_error());
          }
		    }
		    else
		    {
	  	    if($_POST['range'][$loopCountSubSub] != "" || $_POST['accuracySubSub'][$loopCountSubSub] != "" || $_POST['accuracySubSubFS'][$loopCountSubSub] != "" || $_POST['resolution'][$loopCountSubSub] != "" || $_POST['stability'][$loopCountSubSub] != ""|| $_POST['uncertinty'][$loopCountSubSub] != "" || $_POST['degreeOfFreedom'][$loopCountSubSub] != "" )
	  	    {
	  	      $insertMeterSubPara       = "INSERT INTO mastermetersubsub(masterMeterSubId,rangeValue,range_disp,accuracySubSub,accuracySubSubFS,accuracy_main,range_unit,frequency,resolution,stability,uncertinty,certiAccuracy,degreeOfFreedom,coverage_factor,cmc)
	  	                                    VALUES(".$masterMeterSubId.",'".$range."','".$range_disp."','".$accuracySubSub."','".$accuracySubSubFS."','".$accuracy_main."','".$range_unit."','".$frequency."','".$resolution."','".$stability."','".$uncertinty."','".$certiAccuracy."','".$degreeOfFreedom."','".$coverage_factor."','".$cmc."')";
	  	      $insertMeterSubParaResult = mysql_query($insertMeterSubPara);
	  	    }
	  	  }
        $loopCountSubSub++;
      }
      if(isset($_POST['masterMeterSubSubId']) && $_POST['masterMeterSubSubId'] > 0){
        echo json_encode(array("status" => "reload"));
      } else {
          echo json_encode(array("status" => ""));
      }
      exit();
    //header("Location:masterMeterSub.php");
  }
  
  if(isset($_GET['masterMeterSubSubId']) && $_GET['masterMeterSubSubId'] > 0)
  {
  	$isEdit = 1;
  	
  	$editArray = array();
  	$selEditData = "SELECT mastermetersubsub.masterMeterSubSubId,	mastermetersubsub.masterMeterSubId,mastermetersub.masterMeterId,	
  	                       rangeValue,range_disp,	accuracySubSub,	accuracySubSubFS,accuracy_main, range_unit, frequency, resolution, stability,	uncertinty,	certiAccuracy,
  	                       degreeOfFreedom, coverage_factor, cmc
  	                  FROM mastermetersubsub
  	                  JOIN mastermetersub ON mastermetersubsub.masterMeterSubId = mastermetersub.masterMeterSubId
  	                 WHERE masterMeterSubSubId = ".$_GET['masterMeterSubSubId'];
    $selEditDataRes = mysql_query($selEditData);
    if($editRow = mysql_fetch_array($selEditDataRes))
    {
    	$editArray['masterMeterSubSubId'] = $editRow['masterMeterSubSubId'];
    	$editArray['masterMeterSubId']    = $editRow['masterMeterSubId'];
    	$editArray['masterMeterId']       = $editRow['masterMeterId'];
    	$editArray['range']               = $editRow['rangeValue'];
    	$editArray['range_disp']          = $editRow['range_disp'];
    	$editArray['accuracySubSub']      = $editRow['accuracySubSub'];
    	$editArray['accuracySubSubFS']    = $editRow['accuracySubSubFS'];
    	$editArray['accuracy_main']       = $editRow['accuracy_main'];
    	$editArray['range_unit']          = $editRow['range_unit'];
    	$editArray['frequency']           = $editRow['frequency'];
    	$editArray['resolution']          = $editRow['resolution'];
    	$editArray['coverage_factor']          = $editRow['coverage_factor'];
    	$editArray['cmc']          = $editRow['cmc'];
    	$editArray['stability']           = $editRow['stability'];
    	$editArray['uncertinty']          = $editRow['uncertinty'];
    	$editArray['certiAccuracy']       = $editRow['certiAccuracy'];
    	$editArray['degreeOfFreedom']     = $editRow['degreeOfFreedom'];
    }
  }
  
  // Master Meter Name Listing : Starts
  $meterNameList          = "SELECT masterMeterId,masterMeterName,masterMeterExp,masterMeterModelNo
                               FROM mastermeter
                              ORDER BY masterMeterName";
  $meterNameListResult    = mysql_query($meterNameList);
  $meterListRow           = mysql_num_rows($meterNameListResult);
  $i = 0;
  while($meterListRow = mysql_fetch_array($meterNameListResult))
  {
    $meterEntryIdArray[$i]       = $meterListRow['masterMeterId'];
    $meterEntryNameArray[$i]     = $meterListRow['masterMeterName'].'('.$meterListRow['masterMeterModelNo'].')';
    $i++;
  }
  
  include("./bottom.php");
  $smarty->assign("masterMeterExpDate",$masterMeterExpDate);
  $smarty->assign("parameter",$parameter);
  $smarty->assign("accuracy",$accuracy);
  $smarty->assign("range",$range);
  $smarty->assign("range_disp",$range_disp);
  $smarty->assign("accuracySubSub",$accuracySubSub);
  $smarty->assign("accuracySubSubFS",$accuracySubSubFS);
  $smarty->assign("accuracy_main",$accuracy_main);
  $smarty->assign("range_unit",$range_unit);
  $smarty->assign("frequency",$frequency);
  $smarty->assign("resolution",$resolution);
  $smarty->assign("coverage_factor",$coverage_factor);
  $smarty->assign("cmc",$cmc);
  $smarty->assign("stability",$stability);
  $smarty->assign("uncertinity",$uncertinity);
  $smarty->assign("certiAccuracy",$certiAccuracy);
  $smarty->assign("degreeOfFreedom",$degreeOfFreedom);
  $smarty->assign("masterMeterId",$masterMeterId);
  $smarty->assign("meterEntry",$meterEntry);
  $smarty->assign("meterEntryIdArray",$meterEntryIdArray);
  $smarty->assign("meterEntryNameArray",$meterEntryNameArray);
  $smarty->assign("meterEntryExpArray",$meterEntryExpArray);
  $smarty->assign("isEdit",$isEdit);
  $smarty->assign("editArray",$editArray);
  $smarty->display("masterMeterSub.tpl");
} else {
  header("Location:index.php");
}  
}

?>