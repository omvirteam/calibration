<?php
require('./fpdf.php');
include("include/omConfig.php");
class ConductPDF extends FPDF {
    function vcell($c_width, $c_height, $x_axis,$yPosition, $text,$lengthToSplit) {
        $w_w = $c_height / 3;
        $w_w_1 = $w_w + 2;
        $w_w1 = $w_w + $w_w + $w_w + 3;
        $len = strlen($text); // check the length of the cell and splits the text into 7 character each and saves in a array 

        if ($len > $lengthToSplit) {
            $w_text = str_split($text, $lengthToSplit);
            $this->SetXY($x_axis,$yPosition);
            $this->Cell($c_width, $w_w_1, $w_text[0], '', '', 'C');
            if (isset($w_text[1])) {
                $this->SetXY($x_axis,$yPosition);
                $this->Cell($c_width, $w_w1, $w_text[1], '', '', 'C');
            }
            $this->SetXY($x_axis,$yPosition);
            $this->Cell($c_width, $c_height, '', 'LTRB', 0, 'L', 0);
        } else {
            $this->SetXY($x_axis,$yPosition);
            $this->Cell($c_width, $c_height, $text, 'LTRB', 0, 'C', 0);
        }
    }

}
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = "";
  $grnDetailId         = "";
  $grnNo               = "";
  $grnDate             = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();
  $cfgRecPerGrnPage    = 8;
  // Pdf Create :Start
  
  $pdf = new ConductPDF();
  $pdf->AliasNbPages();
  $pdf->AddPage();
    
  //SELECT OF GRN MASTER :START
  $selectGrnMaster = "SELECT grnPrefix,grnNo,infoSheetNo,DATE_FORMAT(grnDate,'%d-%m-%Y') AS grnDate,poNo,DATE_FORMAT(poDate,'%d-%m-%Y') AS poDate,custName,custCode,remarks,grnmaster.contPerson,phNo,userName
                        FROM grnmaster
                        JOIN customer
                       WHERE grnId = ".$_GET['grnId']."
                         AND grnmaster.customerId = customer.customerId";
  $selectGrnMasterRes = mysql_query($selectGrnMaster);
  $selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes);
  //SELECT OF GRN MASTER :END
  //SELECT OF GRN DETAIL :START
  $prevInstrument = 0;
  $a  = 0;
  $selectGrnEntry  = "SELECT grndetail.grnId,grndetail.itemId AS grnDetailItemId,item.itemName,grndetail.itemCode,grndetail.description,
                             grndetail.rangeValue,grndetail.description,grndetail.challan,grndetail.received,
                             grndetail.grnCondition,custReqDate,DATE_FORMAT(expDelivDate,'%d-%m-%y') AS expDelivDate,
                             parameterentry.parameterName
                        FROM grndetail
                        JOIN item
                        JOIN parameterentry
                       WHERE grnId = ".$_GET['grnId']."
                         AND grndetail.itemId = item.itemId
                         AND grndetail.parameterId = parameterentry.parameterId
                       ORDER BY grndetail.grnDetailId";
  $selectGrnEntryRes   = mysql_query($selectGrnEntry);
  while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
  {
    if(($a % 32) == 0)
    {
      $yPosition = 82;
      if($a > 0)
      {
        pageFooter($yPosition);
        $pdf->AddPage();
      }
      pageHeader();
      grnmasterFunc();
    }

    if($a > 0 && $prevInstrument == $selectGrnEntryResRow['grnDetailItemId'].$selectGrnEntryResRow['itemCode'] )
      grnDetailFunc($yPosition,$a+1);
    else
    {
      $prevInstrument = $selectGrnEntryResRow['grnDetailItemId'].$selectGrnEntryResRow['itemCode'];
      grnDetailFunc($yPosition,$a+1);
    }
    $a++;
    $yPosition += 5;
  }
  pageFooter($yPosition);
  $pdf->output();
  include("./bottom.php"); 
}

function grnmasterFunc()
{
    global $pdf;
    global $selectGrnMasterRow;
    $pdf->SetFont('Arial','',13);
    $pdf->SetXY(12,36);
    $pdf->Write(1,'CUSTOMER ORDER INFO SHEET');
    $pdf->SetXY(180,36);
    $pdf->Write(1,'SM F 01');
    $pdf->SetXY(20,45);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(5,'Sheet No.: '.($selectGrnMasterRow['infoSheetNo']));
    $pdf->SetXY(60,45);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(5,'GRN No.: '.$selectGrnMasterRow['grnPrefix']."-".$selectGrnMasterRow['grnNo']);
    $pdf->SetXY(105,45);
    $pdf->Write(5,'Date  : '.($selectGrnMasterRow['grnDate']));
    $pdf->SetXY(150,45);
    $pdf->Write(5,'Customer Code : '.($selectGrnMasterRow['custCode']));
}

function pageFooter($yPosition)
{
  global $pdf, $selectGrnMasterRow;
  if($yPosition == 82)
  {
  	$yPosition = 242;
  }
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(10,$yPosition);
  $pdf->Write(8,'Note :');
  $pdf->SetXY(21,$yPosition+4);
  $pdf->Write(8,'(1) Callibration Standared method to be used.');
  $pdf->SetXY(21,$yPosition+8);
  $pdf->Write(8,'(2) Review of record (if any) to be done before sent to customer.');
  $pdf->SetXY(10,$yPosition+10);
  $pdf->Write(18,'ANY OTHER INFO & REMARKS: '.($selectGrnMasterRow['remarks']));
  $pdf->SetXY(10,$yPosition+16);
  /* additional change by girish - start */
  /*
  $pdf->Write(15,'Sign(Customer\'s Rep.):');
  $pdf->SetXY(120,$yPosition+20);
  $pdf->Write(5,'Sign (KI): '.('_____________'));
  $pdf->SetXY(30,$yPosition+30);
  */
  $pdf->Write(15,'Sign (KI): '.('_____________'));
  $pdf->SetXY(120,$yPosition+20);
  $pdf->Write(5,'Sign(Customer\'s Rep.):');
  $pdf->SetXY(30,$yPosition+30);
  /* additional change by girish - end */
  $pdf->Write(1,'For delivery of instruments this receipt is required, otherwise material will not be returned.');
}

function pageHeader()
{
  global $pdf;
  $pdf->Image('./images/logo_newaddr.jpg',128,3,70,30);
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(13,55);
  $pdf->cell(190,05,' FOLLOWING INSTRUMENTS RECEIVED FOR - CALIBRATION','1', '0', 'C');

 
  $pdf->SetFont('Arial','',9);
  $pdf->SetXY(10,69);
  $pdf->cell(7,13,'','1', '0', 'C');
  $pdf->SetXY(10,73);
  $pdf->Write(1,'SR.');
  $pdf->SetXY(10,77);
  $pdf->Write(1,'No.');
  
  $pdf->SetFont('Arial','',9);
  $pdf->SetXY(17,69);
  $pdf->cell(37,13,'','1', '0', 'C');
  $pdf->SetXY(25,73);
  $pdf->Write(1,'NAME OF');
  $pdf->SetXY(23,77);
  $pdf->Write(1,'INSTRUMENT');

  $pdf->SetFont('Arial','',9);
  $pdf->SetXY(54,69);
  $pdf->cell(22,13,'','1', '0', 'C');
  $pdf->SetXY(56,73);
  $pdf->Write(1,'Instrument');
  $pdf->SetXY(60,77);
  $pdf->Write(1,'ID No.');

  $pdf->SetXY(76,69);
  $pdf->SetFont('Arial','',8);
  $pdf->cell(24,13,'PARAMETERS	','1', '0', 'C');
  $pdf->SetXY(100,69);
  $pdf->cell(28,13,'RANGE','1', '0', 'C');

  $pdf->SetXY(128,69);
  $pdf->SetFont('Arial','',10);
  $pdf->cell(25,13,'','1', '0', 'C');
  $pdf->SetXY(135,76);
  $pdf->Write(1,'Notes');
  
  $pdf->SetXY(153,69);
  $pdf->SetFont('Arial','',8);
  $pdf->cell(18,13,'','1', '0', 'C');
  $pdf->SetXY(153,72);
  $pdf->Write(1,'Cali Due Dt');
  $pdf->SetXY(154,76);
  $pdf->Write(1,'Requested');
  $pdf->SetXY(155,80);
  $pdf->Write(1,'By Cust.');

  $pdf->SetXY(171,69);
  $pdf->cell(18,13,'','1', '0', 'C');
  $pdf->SetXY(173,72);
  $pdf->Write(1,'Expected');
  $pdf->SetXY(173,76);
  $pdf->Write(1,'Delivery');
  $pdf->SetXY(173,80);
  $pdf->Write(1,'Date');

  $pdf->SetXY(189,69);
  $pdf->cell(15,13,'','1', '0', 'C');
  $pdf->SetXY(189,72);
  $pdf->Write(1,'Condi.');
  $pdf->SetXY(192,76);
  $pdf->Write(1,'of');
  $pdf->SetXY(190,80);
  $pdf->Write(1,'Instr.');
}

function grnDetailFunc($yPosition,$srNo)
{
  //$pdf->Line(10, 70, 10, 125);
  global $pdf;
  global $selectGrnEntryResRow;
  
  
  $pdf->vcell(7,05,10,$yPosition,$srNo,'2');
  $pdf->SetFont('Arial','',7);
  $pdf->vcell(37,05,17,$yPosition,$selectGrnEntryResRow['itemName'],'27');
  $pdf->vcell(22,05,54,$yPosition,$selectGrnEntryResRow['itemCode'],'15');
  $pdf->vcell(24,05,76,$yPosition,$selectGrnEntryResRow['parameterName'],'14');
  $pdf->SetFont('Arial','',5);
  $pdf->vcell(28,05,100,$yPosition,$selectGrnEntryResRow['rangeValue'],'22');
  $pdf->SetFont('Arial','',5);
  $pdf->vcell(25,05,128,$yPosition,$selectGrnEntryResRow['description'],'22');
  $pdf->SetFont('Arial','',8);
  $pdf->vcell(18,05,153,$yPosition,$selectGrnEntryResRow['custReqDate'],'8');
  $pdf->vcell(18,05,171,$yPosition,$selectGrnEntryResRow['expDelivDate'],'9');
  $pdf->SetFont('Arial','',8);
  $pdf->vcell(15,05,189,$yPosition,$selectGrnEntryResRow['grnCondition'],'4');
  $pdf->SetFont('Arial','',10);
}

//function grnDetailOnlyParamFunc($yPosition)
//{
//  //$pdf->Line(10, 70, 10, 125);
//  global $pdf;
//  global $selectGrnEntryResRow;
////  $pdf->SetXY(10,$yPosition);
////  $pdf->cell(7,05,$srNo,'1', '0', 'C');
//  $pdf->SetFont('Arial','',7);
//  $pdf->SetXY(17,$yPosition);
//  $pdf->cell(40,05,$selectGrnEntryResRow['itemName'],'1', '0', 'C');
//  $pdf->SetFont('Arial','',10);
//  $pdf->SetXY(57,$yPosition);
//  $pdf->Cell(22,05,$selectGrnEntryResRow['itemCode'],'1', '0', 'C');
//  $pdf->SetXY(79,$yPosition);
//  $pdf->cell(24,05,$selectGrnEntryResRow['parameterName'],'1', '0', 'C');
//  $pdf->SetXY(103,$yPosition);
//  $pdf->cell(22,05,$selectGrnEntryResRow['rangeValue'],'1', '0', 'C');
//  $pdf->SetFont('Arial','',6);
//  $pdf->SetXY(125,$yPosition);
//  $pdf->cell(25,05,$selectGrnEntryResRow['description'],'1', '0', 'C');
//  $pdf->SetFont('Arial','',8);
//  $pdf->SetXY(150,$yPosition);
//  $pdf->cell(18,05,$selectGrnEntryResRow['custReqDate'],'1', '0', 'C');
//  $pdf->SetXY(168,$yPosition);
//  $pdf->cell(18,05,$selectGrnEntryResRow['expDelivDate'],'1', '0', 'C');
//
//  $pdf->SetFont('Arial','',8);
//  $pdf->SetXY(186,$yPosition);
//  $pdf->cell(18,05,$selectGrnEntryResRow['grnCondition'],'1', '0', 'C');
//
//  $pdf->SetFont('Arial','',10);
//  $pdf->SetXY(15,$yPosition);
//  $pdf->cell(7,05,'','1', '0', 'C');
//  $pdf->SetXY(22,$yPosition);
//  $pdf->cell(40,05,'','1', '0', 'C');
//  $pdf->SetXY(62,$yPosition);
//  $pdf->Cell(22,05,'','1', '0', 'C');
//  $pdf->SetXY(84,$yPosition);
//  $pdf->cell(32,05,$selectGrnEntryResRow['parameterName'],'1', '0', 'C');
//  $pdf->SetXY(116,$yPosition);
//  $pdf->cell(27,05,'','1', '0', 'C');
//  $pdf->SetXY(143,$yPosition);
//  $pdf->cell(21,05,'','1', '0', 'C');
//  $pdf->SetXY(164,$yPosition);
//  $pdf->cell(20,05,'','1', '0', 'C');
//
//  $pdf->SetFont('Arial','',8);
//  $pdf->SetXY(184,$yPosition);
//  $pdf->cell(18,05,'','1', '0', 'C');
//
//  $pdf->SetFont('Arial','',10);
//}
?>