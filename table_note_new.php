<?php
include("include/omConfig.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(OBS_MODULE_ID,"view"))){
  $msg                  = "";
  $note_id          = (isset($_REQUEST['note_id'])) ? $_REQUEST['note_id'] : 0;
  $current_note = "";
  $note_arr         = array();

  //note Insert : Start
  if(isset($_POST['note']))
  {
    if(isset($_POST['cancelBtn']))
    {
      header("Location: index.php"); 
      exit();
    }
    if($note_id > 0)
    {
      $updatenote = "UPDATE table_notes
                             SET note = '".$_POST['note']."'
                           WHERE note_id = ".$_POST['note_id'];
      $updatenoteResult = mysql_query($updatenote);
      $note_id = 0; //We don't want user to remain in Edit part after Update query done.
    }
    else
    {
      $insertnote = "INSERT INTO table_notes(note)
                               VALUE('".$_POST['note']."')";
      $insertnoteResult = mysql_query($insertnote);

      if(!$insertnoteResult)
        die("Insert Query Not Inserted : ".mysql_error()." : ".$insertnote);
      else
        header("Location:table_note_new.php");
    }
  }
  //note Insert : End

  //Note Listing : Start
  $noteToDisplay = "SELECT note_id,note
                               FROM table_notes
                              ORDER BY note";
  $selectnoteResult = mysql_query($noteToDisplay);
  $i = 0;
  while($noteInRow = mysql_fetch_array($selectnoteResult))
  {
    $note_arr[$i]['note_id']   = $noteInRow['note_id'];
    $note_arr[$i]['note'] = $noteInRow['note'];

    if($noteInRow['note_id'] == $note_id)
       $current_note = $noteInRow['note'];
    $i++;
  }
  //note Listing : End
  
  include("./bottom.php");
  $smarty->assign("msg",$msg);
  $smarty->assign("note_id",$note_id);
  $smarty->assign("note_arr",$note_arr);
  $smarty->assign("current_note",$current_note);
  $smarty->display("table_note_new.tpl");
} else {
  header("Location:index.php");
}  
}

?>