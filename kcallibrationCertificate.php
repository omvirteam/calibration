<?php
require('./fpdf.php');
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = "";
  $grnDetailId         = "";
  $grnPrefix           = "";
  $grnNo               = "";
  $grnDate             = "";
  $poNo                = "";
  $poDate              = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();
  // Pdf Create :Start
  $pdf = new FPDF('P','mm','A4');
  $pdf->AliasNbPages();
  $pdf->AddPage();
    
  //SELECT OF GRN MASTER :START
  $selectGrnMaster = "SELECT grnmaster.grnId,grnmaster.grnNo,grnmaster.infoSheetNo,grnmaster.customerId,customer.customerId,customer.custName,
                             customer.address,customer.city,grnDetail.grnId,grnDetail.masterMeterId,DATE_FORMAT(grndetail.callibrationDate,'%d/%m/%Y') AS callibrationDate,
                             DATE_FORMAT(grndetail.nextYearDate,'%d/%m/%Y') AS nextYearDate,grnDetail.grnDetailId,mastermeter.masterMeterId,
                             mastermeter.masterMeterName,mastermeter.masterMeterIdNo,mastermeter.masterMeterMake,mastermeter.masterMeterSerialNo,mastermetersub.masterMeterSubId,
                             mastermetersub.masterMeterId,mastermetersubsub.masterMeterSubId,mastermetersubsub.masterMeterSubSubId,
                             mastermetersubsub.certiAccuracy,grnDetail.accuracy,grnDetail.itemId,grnDetail.rangeValue,grndetail.makeModel,grndetail.instrumentId,grnDetail.leastCount,
                             grndetail.accuracy,custReqDate,item.itemId,item.itemName
                        FROM grnmaster
                        JOIN customer
                        JOIN grndetail
                        JOIN mastermeter
                        JOIN mastermetersub
                        JOIN mastermetersubsub
                        JOIN item
                       WHERE grnmaster.grnId = ".$_GET['grnId']."
                         AND grndetail.grnDetailId = ".$_GET['grnDetailId']."
                         AND grnmaster.customerId = customer.customerId
                         AND grnmaster.grnId = grndetail.grnId
                         AND mastermeter.masterMeterId = mastermetersub.masterMeterId
                         AND mastermetersub.masterMeterSubId = mastermetersub.masterMeterSubId
                         AND grndetail.itemId = item.itemId";
  $selectGrnMasterRes = mysql_query($selectGrnMaster);
  if($selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes))
  {
    pageHeader();
    $pdf->SetXY(80,50);
    $pdf->Write(5,' CALIBRATION CERTIFICATE');
  }
  
  $selectGrnDetailQuery = "SELECT grnDetailId,grndetail.parameterId,parameterentry.parameterId,parameterentry.parameterName,masterMeterId
                             FROM grndetail
                             JOIN parameterentry
                            WHERE (grnDetailId       = ".$_GET['grnDetailId']."
                                   OR refGrnDetailId = ".$_GET['grnDetailId'].")
                              AND grndetail.parameterId = parameterentry.parameterId";
  $selectGrnDetailQueryRes = mysql_query($selectGrnDetailQuery);
  $grnDetailCount = 0;
  $masterMeterId = 1;
  $yPosition = 150;
  $yPositionHeader = 116;
  $yPositionRightHeader = 116;
  $leftMasterMeterId    = 0;
  while($grnDetailrow = mysql_fetch_array($selectGrnDetailQueryRes))
  {
////////////////////
    $selectDetailMasterQuery = "SELECT grndetail.grnId,grndetail.masterMeterId,grndetail.grnDetailId,mastermeter.masterMeterId,
                                mastermeter.masterMeterName,mastermeter.masterMeterIdNo,mastermeter.masterMeterMake,mastermeter.masterMeterModelNo,mastermeter.masterMeterSerialNo,mastermeter.masterMeterCertificateNo,
                                DATE_FORMAT(mastermeter.masterMeterExp,'%d/%m/%Y') as masterMeterExp,mastermeter.masterMeterTraceabilityTo,mastermetersub.masterMeterSubId,mastermetersub.masterMeterId,mastermetersubsub.masterMeterSubId,mastermetersubsub.masterMeterSubSubId,
                                mastermetersubsub.certiAccuracy,grndetail.itemId,grndetail.rangeValue,grndetail.makeModel,grndetail.instrumentId,grndetail.leastCount,
                                grndetail.accuracy,grndetail.userName
                           FROM grndetail
                           JOIN mastermeter
                           JOIN mastermetersub
                           JOIN mastermetersubsub
                          WHERE grndetail.grnDetailId = ".$grnDetailrow['grnDetailId']."
                            AND grndetail.masterMeterId = mastermeter.masterMeterId
                            AND mastermeter.masterMeterId = mastermetersub.masterMeterId
                            AND mastermetersub.masterMeterSubId = mastermetersub.masterMeterSubId";
    $selectDetailMasterQueryRes = mysql_query($selectDetailMasterQuery);
    $masterMeterDetailRow = mysql_fetch_array($selectDetailMasterQueryRes);
////////////////////
    if($grnDetailCount % 2 == 0)
    {
      if($grnDetailCount > 0 && ($grnDetailCount % 2 == 0))
      {
        $pdf->AddPage();
        $pdf->SetXY(80,50);
        $pdf->Write(5,' CALIBRATION CERTIFICATE');
        pageHeader();
      }
      
      leftTableHeader($yPositionHeader);
    }
    else
      rightTableHeader($yPositionRightHeader);

    $a = 0;
    $total = 0;
    $selectDetailQuery = "SELECT grnObservationId,grnId,grnDetailId,stdMeterAverage,testMeterAverage,percentageRdg
                            FROM grnobservation
                           WHERE grnDetailId = ".$grnDetailrow['grnDetailId'];
    $selectDetailQueryRes = mysql_Query($selectDetailQuery);
    while($detailRow = mysql_fetch_array($selectDetailQueryRes))
    {
      $total = $detailRow['percentageRdg'];
      // $refGrnDetailId = $detailRow['refGrnDetailId'];
      if(($a % $cfgRecPerGrnPage) == 0)
      {
        $yPosition = 141;
        if($a > 0)
        {
          $pdf->AddPage();
          pageHeader();
        }
      }

      if($grnDetailCount % 2 == 0)
      {
        $leftMasterMeterId = $grnDetailrow['masterMeterId'];
        leftTableData($yPosition,$a+1,$total);
        masterMeterDetailLeft();
      }
      else
      {
        rightTableData($yPosition,$a+1,$total);
        
        if($grnDetailrow['masterMeterId'] != $leftMasterMeterId)
          masterMeterDetailRight();
      }
      
      $yPosition += 5;
      $a++;
    }
    $grnDetailCount++;
  }
  
  $pdf->output();
  include("./bottom.php");
}
// First Part Of grn headerEnd And Footer part: Start


// First Part Of grn headerEnd And Footer part: End 
// Header Part Of grn Print pdf:Start 
function pageHeader()
{
  global $pdf;
  global $selectGrnMasterRow;
  $pdf->Image('./images/logo.jpg',122,2,70,30);
  $pdf->SetFont('Arial','',10);
  $pdf->SetXY(10,16);
  $pdf->Write(5,''.('___________________________________________________________'));
  $pdf->SetXY(20,25);
  $pdf->Write(5,'To');
  $pdf->SetXY(20,30);
  $pdf->Write(5,''.($selectGrnMasterRow['custName']));
  $pdf->SetXY(20,35);
  $pdf->Write(5,''.($selectGrnMasterRow['address']));
  $pdf->SetXY(81,50);
  $pdf->Write(5,''.('________________________'));
  $pdf->SetXY(20,60);
  $pdf->Write(5,'Report No');
  $pdf->SetXY(65,60);
  $pdf->Write(5,':');
  $pdf->SetXY(70,60);
  $pdf->Write(5,''.($selectGrnMasterRow['address']));
  $pdf->SetXY(120,60);
  $pdf->Write(5,'Date Of Callibration');
  $pdf->SetXY(155,60);
  $pdf->Write(5,':');
  $pdf->SetXY(160,60);
  $pdf->Write(5,''.($selectGrnMasterRow['callibrationDate']));
  $pdf->SetXY(20,65);
  $pdf->Write(5,'Name Of The Instrument');
  $pdf->SetXY(65,65);
  $pdf->Write(5,':');
  $pdf->SetXY(70,65);
  $pdf->Write(5,''.($selectGrnMasterRow['itemName']));
  $pdf->SetXY(120,65);
  $pdf->Write(5,'GRN No');
  $pdf->SetXY(155,65);
  $pdf->Write(5,':');
  $pdf->SetXY(160,65);
  $pdf->Write(5,''.($selectGrnMasterRow['grnNo']));
  $pdf->SetXY(20,70);
  $pdf->Write(5,'Make/Model.');
  $pdf->SetXY(65,70);
  $pdf->Write(5,':');
  $pdf->SetXY(70,70);
  $pdf->Write(5,''.($selectGrnMasterRow['makeModel']));
  $pdf->SetXY(20,75);
  $pdf->Write(5,'Sr No.');
  $pdf->SetXY(65,75);
  $pdf->Write(5,':');
  $pdf->SetXY(70,75);
  $pdf->Write(5,''.($selectGrnMasterRow['masterMeterSerialNo']));
  $pdf->SetXY(20,80);
  $pdf->Write(5,'Instrument. ID. No.');
  $pdf->SetXY(65,80);
  $pdf->Write(5,':');
  $pdf->SetXY(70,80);
  $pdf->Write(5,''.($selectGrnMasterRow['instrumentId']));
  $pdf->SetXY(20,85);
  $pdf->Write(5,'Range.');
  $pdf->SetXY(65,85);
  $pdf->Write(5,':');
  $pdf->SetXY(70,85);
  $pdf->Write(5,''.($selectGrnMasterRow['rangeValue']));
  $pdf->SetXY(20,90);
  $pdf->Write(5,'Least Count.');
  $pdf->SetXY(65,90);
  $pdf->Write(5,':');
  $pdf->SetXY(70,90);
  $pdf->Write(5,''.($selectGrnMasterRow['leastCount']));
  $pdf->SetXY(20,95);
  $pdf->Write(5,'Accuracy. +/- ');
  $pdf->SetXY(65,95);
  $pdf->Write(5,':');
  $pdf->SetXY(70,95);
  $pdf->Write(5,''.($selectGrnMasterRow['accuracy']));
  $pdf->SetXY(75,95);
  $pdf->Write(5,'%');
  $pdf->SetXY(20,100);
  $pdf->Write(5,'Next Calibration Due .');
  $pdf->SetXY(65,100);
  $pdf->Write(5,':');
  $pdf->SetXY(70,100);
  $pdf->Write(5,''.($selectGrnMasterRow['nextYearDate']));
  $pdf->SetXY(80,110);
  $pdf->Write(5,'OBSERVATION');
  $pdf->SetXY(80,110);
  $pdf->Write(5,''.('_____________'));
}

function leftTableHeader($yPositionHeader)
{
  global $pdf;
  global $grnDetailrow;
  $pdf->SetXY(32,$yPositionHeader);
  $pdf->cell(90,05,$grnDetailrow['parameterName'],'1', '0', 'C');
  $pdf->SetXY(62,$yPositionHeader);
  $pdf->Write(5,'For');
  $pdf->SetXY(20,116);
  $pdf->cell(12,25,'SR No','1', '0', 'C');
  $pdf->SetXY(32,121);
  $pdf->cell(30,20,'Std meter Values','1', '0', 'C');
  $pdf->SetXY(62,121);
  $pdf->cell(30,20,'Test Meter Values','1', '0', 'C');
  $pdf->SetXY(92,121);
  $pdf->cell(30,20,'Error in %','1', '0', 'C');
}

function rightTableHeader($yPositionRightHeader)
{
  global $pdf;
  global $grnDetailrow;
  $pdf->SetXY(122,$yPositionRightHeader);
  $pdf->cell(80,05,$grnDetailrow['parameterName'],'1', '0', 'C');
  $pdf->SetXY(142,$yPositionRightHeader);
  $pdf->Write(5,'For');
  $pdf->SetXY(122,121);
  $pdf->cell(30,20,'Std meter Values','1', '0', 'C');
  $pdf->SetXY(152,121);
  $pdf->cell(30,20,'Test Meter Values','1', '0', 'C');
  $pdf->SetXY(182,121);
  $pdf->cell(20,20,'Error in %','1', '0', 'C');
}

function leftTableData($yPosition,$srNo,$total)
{
  global $pdf;
  global $detailRow;
  global $grnDetailrow;
  $pdf->SetXY(20,$yPosition);
  $pdf->cell(12,05,$srNo,'1', '0', 'C');
  $pdf->SetXY(32,$yPosition);
  $pdf->cell(30,05,$detailRow['stdMeterAverage'],'1', '0', 'C');
 
  $pdf->SetXY(62,$yPosition);
  $pdf->cell(30,05,$detailRow['testMeterAverage'],'1', '0', 'C');
  $pdf->SetXY(92,$yPosition);
  $pdf->cell(30,05,number_format($total,3,".",""),'1', '0', 'C');
}

function rightTableData($yPosition,$srNo,$total)
{
  global $pdf;
  global $detailRow;
  $pdf->SetXY(122,$yPosition);
  $pdf->cell(30,05,$detailRow['stdMeterAverage'],'1', '0', 'C');
 
  $pdf->SetXY(152,$yPosition);
  $pdf->cell(30,05,$detailRow['testMeterAverage'],'1', '0', 'C');
  $pdf->SetXY(182,$yPosition);
  $pdf->cell(20,05,number_format($total,3,".",""),'1', '0', 'C');
}


//function observationDateRes($yPosition,$totalRdg)
//{
//  global $pdf;
//  global $detailRow;
//  global $totalRdg;
//  $pdf->SetXY(122,$yPosition);
//  $pdf->cell(30,05,$detailRow['stdMeterAverageRes'],'1', '0', 'C');
// 
//  $pdf->SetXY(152,$yPosition);
//  $pdf->cell(30,05,$detailRow['testMeterAverageRes'],'1', '0', 'C');
//  $pdf->SetXY(182,$yPosition);
//  $pdf->cell(20,05,number_format($totalRdg,3,".",""),'1', '0', 'C');
//}

function masterMeterDetailLeft()
{
  global $pdf;
  global $masterMeterDetailRow;
  global $s_activId;
  $pdf->SetXY(20,180);
  $pdf->write(5,'DETAILS OF REFERANCE STANDARD & MAJOR INSTRUMENT USED.');
  $pdf->SetXY(20,180);
  $pdf->Write(5,''.('___________________________________________________________'));
  $pdf->SetXY(20,190);
  $pdf->write(5,'Name');
  $pdf->SetXY(62,190);
  $pdf->Write(5,':');
  $pdf->SetXY(65,190);
  $pdf->Write(5,''.($masterMeterDetailRow['masterMeterName']));
  $pdf->SetXY(20,195);
  $pdf->write(5,'I.D No');
  $pdf->SetXY(62,195);
  $pdf->Write(5,':');
  $pdf->SetXY(65,195);
  $pdf->Write(5,''.($masterMeterDetailRow['masterMeterIdNo']));
  $pdf->SetXY(20,200);
  $pdf->write(5,'Make');
  $pdf->SetXY(62,200);
  $pdf->Write(5,':');
  $pdf->SetXY(65,200);
  $pdf->Write(5,''.($masterMeterDetailRow['masterMeterMake']));
  $pdf->SetXY(62,200);
  $pdf->Write(5,':');
  $pdf->SetXY(20,205);
  $pdf->write(5,'Certificate No');
  $pdf->SetXY(62,205);
  $pdf->Write(5,':');
  $pdf->SetXY(65,205);
  $pdf->Write(5,''.($masterMeterDetailRow['masterMeterCertificateNo']));
  $pdf->SetXY(20,210);
  $pdf->write(5,'Serial No');
  $pdf->SetXY(62,210);
  $pdf->Write(5,':');
  $pdf->SetXY(65,210);
  $pdf->Write(5,''.($masterMeterDetailRow['masterMeterSerialNo']));
  $pdf->SetXY(20,215);
  $pdf->write(5,'Model No');
  $pdf->SetXY(62,215);
  $pdf->Write(5,':');
  $pdf->SetXY(65,215);
  $pdf->Write(5,''.($masterMeterDetailRow['masterMeterModelNo']));
  $pdf->SetXY(20,220);
  $pdf->write(5,'Calibration Valid Up To');
  $pdf->SetXY(62,220);
  $pdf->Write(5,':');
  $pdf->SetXY(65,220);
  $pdf->Write(5,''.($masterMeterDetailRow['masterMeterExp']));
  $pdf->SetXY(20,225);
  $pdf->write(5,'Traceability To');
  $pdf->SetXY(62,225);
  $pdf->Write(5,':');
  $pdf->SetXY(65,225);
  $pdf->Write(5,''.($masterMeterDetailRow['masterMeterTraceabilityTo']));
  $pdf->SetXY(20,230);
  $pdf->write(5,'Remarks :This Instrument above Observation  is whithin its accuracy limit and is accepted');
  $pdf->SetXY(20,235);
  $pdf->write(5,'For  Krishna Instruments ');
  $pdf->SetXY(20,245);
  $pdf->write(5,' '.$masterMeterDetailRow['userName']);
  $pdf->SetXY(20,250);
  $pdf->write(5,'TESTED BY:'); 
  $pdf->SetXY(90,250);
  $pdf->write(5,'APPROVED BY:'); 
  $pdf->SetXY(160,250);
  $pdf->write(5,'SEAL:'); 
  $pdf->SetXY(40,255);
  $pdf->write(5,'Note: 1) This Report pertains to particular sample/Instrument submitted for test.This report');
  $pdf->SetXY(55,260);
  $pdf->write(5,'may not bereproduced,except in full,without prior written permission ');
  $pdf->SetXY(50,265);
  $pdf->write(5,'2) This calibration results reported in the certificate are valid at the time of measurements');
  $pdf->SetXY(50,270);
  $pdf->write(5,'Email : info@krishnainstruments.com        Web : www.krishnainstruments.com');
}


function masterMeterDetailRight()
{
  global $pdf;
  global $masterMeterDetailRow;
  $pdf->SetXY(110,190);
  $pdf->write(5,': '.$masterMeterDetailRow['masterMeterName']);
  $pdf->SetXY(110,195);
  $pdf->write(5,': '.$masterMeterDetailRow['masterMeterIdNo']);
  $pdf->SetXY(110,200);
  $pdf->write(5,': '.$masterMeterDetailRow['masterMeterMake']);
  $pdf->SetXY(110,205);
  $pdf->write(5,': '.$masterMeterDetailRow['masterMeterModelNo']);
  $pdf->SetXY(110,210);
  $pdf->write(5,': '.$masterMeterDetailRow['masterMeterSerialNo']);
  $pdf->SetXY(110,215);
  $pdf->write(5,': '.$masterMeterDetailRow['masterMeterCertificateNo']);
  $pdf->SetXY(110,220);
  $pdf->write(5,': '.$masterMeterDetailRow['masterMeterExp']);
  $pdf->SetXY(110,225);
  $pdf->write(5,': '.$masterMeterDetailRow['masterMeterTraceabilityTo']);
}
// Header Part Of grn Print pdf:End
?>
