<?php
include("include/omConfig.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(MASTER_ITEM_MODULE_ID,"view"))){
  $msg       = "";
  $itemId    = (isset($_REQUEST['itemId'])) ? $_REQUEST['itemId'] : 0;
  $currentItemName = "";
  $itemArr   = array();

  //Item Insert : Start
  if(isset($_POST['itemName']))
  {
    if(isset($_POST['cancelBtn']))
    {
      header("Location: index.php"); 
      exit();
    }
    if($itemId > 0)
    {
      $updateItem = "UPDATE item
                        SET itemName = '".$_POST['itemName']."'
                      WHERE itemId = ".$_POST['itemId'];
      $updateItemResult = mysql_query($updateItem);
      $itemId = 0; //We don't want user to remain in Edit part after Update query done.
    }
    else
    {
      $insertItemName = "INSERT INTO item(itemName)
                          VALUE('".$_POST['itemName']."')";
      $insertItemNameResult = mysql_query($insertItemName);

      if(!$insertItemNameResult)
        die("Insert Query Not Inserted : ".mysql_error()." : ".$insertItemName);
      else
        header("Location:item.php");
    }
  }
  //Item Insert : End

  //Item Listing : Start
  $itemNameToDisplay = "SELECT itemId,itemName
                          FROM item
                         ORDER BY itemName";
  $selectItemResult = mysql_query($itemNameToDisplay);
  $i = 0;
  while($itemInRow = mysql_fetch_array($selectItemResult))
  {
    $itemArr[$i]['itemId']   = $itemInRow['itemId'];
    $itemArr[$i]['itemName'] = $itemInRow['itemName'];

    if($itemInRow['itemId'] == $itemId)
      $currentItemName = $itemInRow['itemName'];
    $i++;
  }
  //Item Listing : End
  
  include("./bottom.php");
  $smarty->assign("msg",$msg);
  $smarty->assign("itemId",$itemId);
  $smarty->assign("itemArr",$itemArr);
  $smarty->assign("currentItemName",$currentItemName);
  $smarty->display("item.tpl");

} else {
  header("Location:index.php");
}  
}

?>