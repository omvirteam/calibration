<?php
include("include/omConfig.php");

//error_reporting(0);
if (!isset($_SESSION['s_activId'])) {
    $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
    header("Location:checkLogin.php");
    exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
    if ($haystack == null) {
        return false;
    } else {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
    }
    return false;
}

// general functions - start //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// variable list - start //
$certificate_no = '';
$grn_no = '';
$grnDate = '';
$callibrationDate = '';
$callibrationDateNext = '';
$certificateIssue = '';
$certificateIssueDate = '';
$customerName = '';
$customerAddress = '';
$customerCity = '';
$customerRefNo = '';
$ambientTemperature = '';
$relativeHumidity = '';

$counter = 0;
$grnDetailIds = array();
$tempArr = array();

$instrumentDetails = array();
$instrumentDetails['item_name'] = '';
$instrumentDetails['make'] = '';
$instrumentDetails['item_code'] = '';
$instrumentDetails['id_no'] = '';
$instrumentDetails['range_value'] = '';
$instrumentDetails['accuracy'] = '';
$instrumentDetails['grn_condition'] = '';
$instrumentDetails['certi_remarks'] = '';
$instrumentDetails['cali_location'] = '';
$instrumentDetails['uuc_location'] = '';
$instrumentDetails['ulr_no'] = '';
$instrumentDetails['userName'] = '';
$instrumentDetails['approved_by'] = '';
$instrumentDetails['designation'] = '';
$instrumentDetails['sr_no'] = '';

$masterMeters = array();
$masterMeters['masterMeterName'] = array();
$masterMeters['masterMeterIdNo'] = array();
$masterMeters['masterMeterMake'] = array();
$masterMeters['masterMeterModelNo'] = array();
$masterMeters['masterMeterSerialNo'] = array();
$masterMeters['masterMeterCertificateNo'] = array();
$masterMeters['due_date'] = array();

$observations = array();

// variable list - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// fetch data - start //
$qry = 'SELECT DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate,
	DATE_FORMAT(grndetail.nextYearDate,"%d/%m/%Y") AS nextYearDate,
	certificateIssue,DATE_FORMAT(grndetail.certificateIssueDate,"%d/%m/%Y") AS certificateIssueDate,
	grndetail.grnId,grndetail.certificate_no,
	grndetail.temperature,grndetail.humidity,itemCode,
	CONCAT("N-",grnmaster.grnNo) AS grnNo,grnmaster.grn_no,
	DATE_FORMAT(grnmaster.grnDate,"%d/%m/%Y") AS grnDate,
	customer.custName,customer.address,customer.city,
	customer.custCode AS customerRef 
	FROM grndetail 
	LEFT JOIN grnmaster ON grnmaster.grnId = grndetail.grnId 
	LEFT JOIN customer ON customer.customerId = grnmaster.customerId 
	WHERE grndetail.grnDetailId = ' . $_GET['grnDetailId'];
$res = mysql_query($qry) or die("Error :: Cannot select basic details.<hr>" . mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $grn_no = $row['grn_no'];
    $certificate_no = $row['certificate_no'];
    $grnDate = $row['grnDate'];
    $callibrationDate = $row['callibrationDate'];
    $callibrationDateNext = $row['nextYearDate'];
    $certificateIssue = $row['certificateIssue'];
    $certificateIssueDate = $row['certificateIssueDate'];
    $customerName = $row['custName'];
    $customerAddress = $row['address'];
    $customerCity = $row['city'];
    $ref_no = '';
    $words = preg_split("/\s+/", trim($row['custName']));
//        echo "<pre>"; print_r($words); exit;
    $acronym = "";
    foreach ($words as $w) {
        if (strlen($acronym) == 2) {
            break;
        } else {
            $acronym .= $w[0];
        }
    }
    $cust_sn = Strtoupper($acronym);
    $customerRefNo = $cust_sn . '/Cali/' . $row['customerRef'];
    $ambientTemperature = $row['temperature'];
    $relativeHumidity = $row['humidity'];

    $tempArr = $instrumentDetails;
    // instrument details query //
    $qry1 = 'SELECT grndetail.grnDetailId,grndetail.makeModel AS make,grndetail.itemCode AS item_code,
		grndetail.instrumentId AS id_no,grndetail.rangeValue AS range_value,
		grndetail.accuracy,grndetail.grnCondition AS grn_condition,item.itemName AS item_name,
		grndetail.certiRemarks AS certi_remarks, grndetail.cali_location, grndetail.uuc_location,grndetail.ulr_no, staff.staffName as userName, grndetail.sr_no, approved_by.name as approved_by, approved_by.designation
		FROM grndetail 
		LEFT JOIN item ON item.itemId = grndetail.itemId 
		LEFT JOIN approved_by ON approved_by.id = grndetail.approved_by 
                LEFT JOIN staff ON staff.staffId = grndetail.user_id 
		WHERE grndetail.callibrationDate IS NOT NULL AND grndetail.grnId = ' . $row['grnId'] . ' AND grndetail.itemCode = "' . $row['itemCode'] . '"';
    $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>" . mysql_error());
    while ($row1 = mysql_fetch_assoc($res1)) {
        array_push($grnDetailIds, $row1['grnDetailId']);
        foreach ($tempArr as $key => $val) {
            if (!in_array_r($row1[$key], $tempArr[$key])) {
                $tempArr[$key][$counter] = $row1[$key];
                $instrumentDetails[$key] .= $row1[$key] . ' | ';
            }
        }
        $counter++;
    }

    foreach ($instrumentDetails as $key => $val) {
        if ($val != '')
            $instrumentDetails[$key] = substr($val, 0, -3);
    }
    mysql_free_result($res1);

    $tempArr = $masterMeters;
    // master meters query //
    $all_least = '';
    $all_uut = '';
    $qry2 = 'SELECT master_json,uuc_json FROM observation_data as grn
		WHERE grn.grn_id = ' . $_GET['grnId'] . ' AND grn.grn_detail_id = ' . $_GET['grnDetailId'] . '  ';
    $res2 = mysql_query($qry2) or die("Error :: Cannot select customer and instrument details.<hr>" . mysql_error());
    $master_data = array();
//        echo "<pre>"; print_r(mysql_fetch_assoc($res1)); exit;
    while ($row2 = mysql_fetch_assoc($res2)) {
        array_push($master_data, $row2);
    }
//        echo "<pre>"; print_r($master_data); exit;
        $master_meter_ids = array();
        $uuc_ranges = array();
        $max_count_page = count($master_data) + 1;
        foreach ($master_data as $m_key => $ms_data){ 
           
            $m_data = json_decode(json_encode(json_decode($ms_data['master_json'])), true);
            $u_data = json_decode(json_encode(json_decode($ms_data['uuc_json'])), true);
//             echo "<pre>"; print_r($m_data); exit;
            $all_least = $u_data[0]['leastCount'];
            $all_uut = $u_data[0]['uut_range'];
//            isset($uuc_data['uut_range_min']) ? $uuc_data['uut_range_min'] : '';
            foreach ($u_data as $uu_data){ 
                if($uu_data['uut_range_min'] != ''){
                    $uuu_name = $uu_data['uut_range_min'] ." to ". $uu_data['uut_range'];
                } else {
                    $uuu_name = $uu_data['uut_range'];
                }
                $uuc_ranges[] = $uuu_name;
            }
            foreach ($m_data as $s_data){ 
                $master_meter_ids[] = $s_data['masterMeterId'];
            }
            
        }
        $uni_uut = array_unique($uuc_ranges);
        $all_uut = implode(",",$uni_uut);
//        echo "<pre>"; print_r($master_meter_ids); exit;
    $qry2 = 'SELECT masterMeterName as "0",masterMeterIdNo as "1",masterMeterMake as "2",masterMeterModelNo as "3",masterMeterSerialNo as "4",masterMeterCertificateNo as "5",
		 DATE_FORMAT(masterMeterExp,"%d/%m/%Y") AS "6"
                 FROM mastermeter 
		 WHERE masterMeterId IN (' . implode(",", $master_meter_ids) . ')';
//	 echo "DEBUG QUERY: " . $qry2;
    $res2 = mysql_query($qry2) or die("Error :: Cannot select master meter details1.<hr>" . mysql_error());
    $master_m_data = array();
    while ($row2 = mysql_fetch_assoc($res2)) {
        $master_m_data[] = $row2;
    }
//        echo "<pre>"; print_r($master_m_data); exit;
    $qry21 = 'SELECT GROUP_CONCAT(DISTINCT procedureText) as procedureText, GROUP_CONCAT(DISTINCT page_title) as page_title
                 FROM mastermeter 
		 WHERE masterMeterId IN (' . implode(",", $master_meter_ids) . ')';
//	 echo "DEBUG QUERY: " . $qry2;
    $res21 = mysql_query($qry21) or die("Error :: Cannot select master meter details2.<hr>" . mysql_error());
    $master_procedureText = '';
    $page_title = '';
    while ($row21 = mysql_fetch_assoc($res21)) {
        $master_procedureText = $row21['procedureText'];
        $page_title = $row21['page_title'];
    }
    mysql_free_result($res2);
}
mysql_free_result($res);
// fetch data - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// html - start //
ob_start();
?>
<page backtop="0" footer="page" >

<style>
table {font-family: "Times New Roman"; font-size: 13px; page-break-inside:auto !important; page-break-before:always;}
tr {line-height: 21px;page-break-inside: avoid !important;page-break-after:auto;  page-break-before: always;}
td    { page-break-inside:avoid; page-break-after:auto; page-break-before: always; }
thead { display:table-header-group }
tfoot { display:table-footer-group }
@media print
{
  table { page-break-after:auto; page-break-before: always; }
  tr    { page-break-inside:avoid; page-break-after:auto; page-break-before: always; }
  td    { page-break-inside:avoid; page-break-after:auto;  page-break-before: always; }
  thead { display:table-header-group;  page-break-before: always; }
  tfoot { display:table-footer-group;  page-break-before: always; }
}
</style>

<div style="text-align:right; padding-right: 25px;"><b><?php echo $page_title; ?></b></div>
<div style="border: 1px solid black; border-radius: 10px; padding: 5px;">
    <table width="605">
    	<tr>
    		<td width="150">CERTIFICATE NO.</td>
    		<td width="145">: <?php echo $certificate_no; ?></td>
    		<td width="5">&nbsp;</td>
    		<td width="200">DATE OF CALIBRATION</td>
    		<td width="95" align="right">: <?php echo $callibrationDate; ?></td>
    	</tr>
    	<tr>
    		<td>GRN NO.</td>
    		<td>: <?php echo $grn_no; ?></td>
    		<td>&nbsp;</td>
    		<td>NEXT RECOMMENDED DATE</td>
    		<td align="right">: <?php echo $callibrationDateNext; ?></td>
    	</tr>
    	<tr>
    		<td width="150">PAGE</td>
    		<td width="145">: 1 OF <span class="total_page_no">2</span></td>
    		<td width="5">&nbsp;</td>
    		<td width="200">CERTIFICATE ISSUE DATE</td>
    		<td width="95" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
    	</tr>
    </table>
    <hr />
    <table width="605">
    	<tr valign="top">
    		<td width="5"><strong>1.</strong></td>
    		<td width="235"><strong>CUSTOMER NAME & ADDRESS</strong></td>
    		<td width="5">:</td>
    		<td width="360" style="font-family: Agency FB;font-size:18px; font-weight:bold;"><?php echo $customerName; ?></td>
    	</tr>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>&nbsp;</td>
    		<td>&nbsp;</td>
    		<td style="font-size:14px;" width="360">
                <?php echo $customerAddress; ?>
                <?php if(!empty($customerCity)) { ?>
                    <br/><b><?php echo $customerCity; ?></b>
                <?php } ?>
    		</td>
    	</tr>
    	<tr valign="top">
    		<td><strong>2.</strong></td>
    		<td><strong>CUSTOMER REFERENCE NO.</strong></td>
    		<td>:</td>
    		<td><?php echo $customerRefNo; ?></td>
    	</tr>
    	<tr valign="top">
    		<td><strong>3.</strong></td>
    		<td><strong>INSTRUMENT RECEIVED ON</strong></td>
    		<td>:</td>
    		<td><?php echo $grnDate; ?></td>
    	</tr>
    	<tr valign="top">
    		<td><strong>4.</strong></td>
    		<td><strong>ULR NO.</strong></td>
    		<td>:</td>
    		<td><?php echo $instrumentDetails['ulr_no']; ?></td>
    	</tr>
    	<tr valign="top">
    		<td><strong>5.</strong></td>
    		<td><strong>DESCRIPTION OF INSTRUMENT</strong></td>
    		<td>:</td>
    		<td>&nbsp;</td>
    	</tr>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>Name</td>
    		<td>:</td>
    		<td><?php echo $instrumentDetails['item_name']; ?></td>
    	</tr>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>Make/Model</td>
    		<td>:</td>
    		<td><?php echo $instrumentDetails['make']; ?></td>
    	</tr>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>Sr. No.</td>
    		<td>:</td>
    		<td><?php echo $instrumentDetails['sr_no']; ?></td>
    	</tr>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>Instrument ID No.</td>
    		<td>:</td>
            <td><?php echo !empty($instrumentDetails['item_code']) ? $instrumentDetails['item_code'] : '---'; ?></td>
    	</tr>
        <?php 
            
            $qry112 = 'SELECT * FROM setting ';
    	    $res112 = mysql_query($qry112) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
            $s_trac = '';
            $s_approve = '';
            $s_note = '';
            $s_all_note = '';
            $s_page_1_note = '';
            while($row112 = mysql_fetch_assoc($res112)) { 
                if($row112['setting_name'] == 'traceability_to'){
                    $s_trac = $row112['setting_value'];
                }
                if($row112['setting_name'] == 'approved_by'){
                    $s_approve = $row112['setting_value'];
                }
                if($row112['setting_name'] == 'footer_note_for_page_1'){
                    $s_note = $row112['setting_value'];
                }
                if($row112['setting_name'] == 'footer_note_for_all'){
                    $s_all_note = $row112['setting_value'];
                }
                if($row112['setting_name'] == 'page_1_remark'){
                    $s_page_1_note = $row112['setting_value'];
                }
            }
        ?>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>Range</td>
    		<td>:</td>
    		<td><?php echo $all_uut; ?></td>
    	</tr>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>Least Count</td>
    		<td>:</td>
    		<td><?php echo $all_least; ?></td>
    	</tr>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>Accuracy</td>
    		<td>:</td>
    		<td><?php echo $instrumentDetails['accuracy']; ?></td>
    	</tr>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>Condition on receipt</td>
    		<td>:</td>
    		<td><?php echo $instrumentDetails['grn_condition']; ?></td>
    	</tr>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>UUC Location</td>
    		<td>:</td>
    		<td><?php echo $instrumentDetails['uuc_location']; ?></td>
    	</tr>
    	<tr valign="top">
    		<td>&nbsp;</td>
    		<td>Remark</td>
    		<td>:</td>
    		<td><?php echo $s_page_1_note; ?></td>
    	</tr>
    	<tr valign="top">
    		<td><strong>6.</strong></td>
    		<td><strong>AMBIENT TEMPERATURE</strong></td>
    		<td>:</td>
    		<td><?php echo $ambientTemperature; ?></td>
    	</tr>
    	<tr valign="top">
    		<td><strong></strong></td>
    		<td><strong>RELATIVE HUMIDITY</strong></td>
    		<td>:</td>
    		<td><?php echo $relativeHumidity; ?></td>
    	</tr>
    </table>
    <table >	
    	<tr valign="top">
    		<td width="5"><strong>7.</strong></td>
    		<td width="600"><strong>DETAILS OF REFERENCE STANDARD & MAJOR INSTRUMENTS USED</strong></td>
    	</tr>
    	<tr valign="top">
            <td width="5">&nbsp;</td>
            <td width="600">
                <?php
                    $max_rows = count($master_m_data);
                    $name_arr = array();
                    $name_arr[] = 'Name';
                    $name_arr[] = 'I.D. No.';
                    $name_arr[] = 'Make';
                    $name_arr[] = 'Model No.';
                    $name_arr[] = 'Serial no.';
                    $name_arr[] = 'Certificate No.';
                    $name_arr[] = 'Calibration Valid Up to';
                    if($max_rows <= 2){
                        $font_s = 'font-size:14px;';
                    } else {
                        $font_s = 'font-size:10px;';
                    }

                ?>
                <table cellpadding="0" cellspacing="0" border="1" style="<?php echo $font_s; ?>" width="100%">
                    <?php for($x = 0; $x < 7; $x++) { ?>
                    <tr>
                        <td align="left" style="padding: 2px;" valign="center"><b><?php echo $name_arr[$x]; ?></b></td>
                        <?php foreach ($master_m_data as $master_m){ ?>
                            <td align="left" style="padding: 2px;" valign="center"><?php echo $master_m[$x]; ?></td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td align="left" style="padding: 2px;" valign="center"><b>Traceability To </b></td>
                        <td align="left" style="padding: 2px;" valign="center" colspan="<?php echo $max_rows; ?>"><?php echo $s_trac; ?></td>
                    </tr>
                </table>
        	</td>
    	</tr>
    </table>
    <table width="605">	
    	<tr valign="top">
    		<td width="5"><strong>8.</strong></td>
    		<td width="235"><strong>PROCEDURE</strong></td>
    		<td width="5">:</td>
    		<td width="360"><?php echo $master_procedureText; ?></td>
    	</tr>
    	<tr valign="top">
    		<td width="5"><strong>9.</strong></td>
    		<td width="235"><strong>CALIBRATION LOCATION</strong></td>
    		<td width="5">:</td>
    		<td width="360"><?=  ($instrumentDetails['cali_location'] == '2') ? 'At Site' : 'At Lab';  ?></td>
    	</tr>
            <tr valign="bottom">
                <hr />
                <td width="5" style="height: 5px;"><strong></strong></td>
                <td colspan="3" style="height: 5px;" width="600" style="font-size: 20px;"></td>
    	</tr>
            <tr valign="bottom">
                <td width="5"><strong>&nbsp;</strong></td>
                <td colspan="3" width="600" style="font-size: 20px;"><img src="./images/for-krishna.png" /></td>
    	</tr>
            <tr valign="top">
                <td colspan="4">&nbsp;</td>
    	</tr>
            <tr valign="top">
                <td colspan="4">&nbsp;</td>
    	</tr>
            <tr valign="top">
                <td colspan="4">&nbsp;</td>
    	</tr>
            <tr valign="top">
                <td colspan="4">CALIBRATED BY: (<?php echo $instrumentDetails['userName']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; APPROVED BY: (<?php echo $instrumentDetails['approved_by'].', '.$instrumentDetails['designation']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; SEAL :</td>
    	</tr>
            <tr valign="top">
    		<td width="5"><strong>Note&nbsp;:</strong></td>
                    <td>&nbsp;</td>
    		<td>&nbsp;</td>
    		<td>&nbsp;</td>
    	</tr>
            <tr valign="top">
                <td colspan="4" style="font-size: 12px;"><?php echo $s_note; ?></td>
    	</tr>
    </table>
</div>

<div style="page-break-after: always;"></div>
<style>
    tr {line-height: 21px;}
.main_table tr td {border: 1px solid black; }
.main_table{
    border-collapse: collapse;
}
.main_table, .main_table td, .main_table th {
    border: 1px solid black;
    
}
.main_table td, .main_table th {
    padding: 3px;
    
}
table {
        page-break-inside: avoid;
    }
</style>
<div>
<div style="text-align:right; padding-right: 25px;"><b><?php echo $page_title; ?></b></div>
<!--<div style="border: 1px solid black; border-radius: 10px; padding: 5px;">-->
<table width="605">
	<tr>
		<td width="150">CERTIFICATE NO.</td>
		<td width="145">: <?php echo $certificate_no; ?></td>
		<td width="5">&nbsp;</td>
		<td width="200">DATE OF CALIBRATION</td>
		<td width="95" align="right">: <?php echo $callibrationDate; ?></td>
	</tr>
	<tr>
		<td>GRN NO.</td>
		<td>: <?php echo $grn_no; ?></td>
		<td>&nbsp;</td>
		<td>NEXT RECOMMENDED DATE</td>
		<td align="right">: <?php echo $callibrationDateNext; ?></td>
	</tr>
	<tr>
		<td width="150">PAGE</td>
		<td width="145">: 2 OF <span class="total_page_no">2</span></td>
		<td width="5">&nbsp;</td>
		<td width="200">CERTIFICATE ISSUE DATE</td>
		<td width="95" align="right">: <?php echo ($certificateIssue == "Y") ? $certificateIssueDate : $callibrationDate; ?></td>
	</tr>
</table>
</div>
<hr />
    <?php if(isset($_GET['grnDetailId']) && $_GET['grnDetailId'] != '') { ?>
            <br/>
            <table width="605">
                <tr valign="top">
                    <td width="10"><strong>10.</strong></td>
                    <td width="235"><strong> Observation</strong></td>
                    <td width="5"></td>
                    <td width="355"></td>
                </tr>
            </table>
            <?php
            $qry1 = 'SELECT *,ti.note as table_info_note FROM observation_data as grn
                    LEFT JOIN table_info as ti ON ti.table_info_id = grn.table_info_id
                    WHERE grn.grn_id = '.$_GET['grnId'].' AND grn.grn_detail_id = '.$_GET['grnDetailId'].'  ORDER BY grn.table_no ASC ';
            $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
            $grnobsmaster_data = array();
    //        echo "<pre>"; print_r(mysql_fetch_assoc($res1)); exit;
            while($row1 = mysql_fetch_assoc($res1)) {
                    array_push($grnobsmaster_data,$row1);
            }
    //        echo "<pre>"; print_r($grnobsmaster_data); exit;
            ?>
            <?php foreach ($grnobsmaster_data as $main_key => $grn_data){ 
                $table_info_id =  $grn_data['table_info_id'];
                $master_data = json_decode(json_encode(json_decode($grn_data['master_json'])), true);
                $uuc_data = json_decode(json_encode(json_decode($grn_data['uuc_json'])), true);
                $type_a_type_b_json = json_decode(json_encode(json_decode($grn_data['type_a_type_b_json'])), true);
                $reading_json = json_decode(json_encode(json_decode($grn_data['reading_json'])), true);
                $stdmeter_data = $reading_json[0]['stdMeter'];
                $testmeter1_data = $reading_json[0]['testMeter'];
                $testmeter2_data = isset($reading_json[0]['testMeter2']) ? $reading_json[0]['testMeter2'] : array();
                $testmeter3_data = isset($reading_json[0]['testMeter3']) ? $reading_json[0]['testMeter3'] : array();
                $uuc_min_range = $uuc_data[0]['uut_range_min'];
                $uuc_max_range = $uuc_data[0]['uut_range'];
                $uuc_range = 0;
                if($uuc_min_range < 0){
                    $uuc_range = abs($uuc_min_range) + abs($uuc_max_range);
                } else {
                    if($uuc_min_range > $uuc_max_range){
                        $uuc_range = abs($uuc_min_range);
                    } else {
                        $uuc_range = abs($uuc_max_range);
                    }
                }
                unset($grn_data['master_json']);
                unset($grn_data['uuc_json']);
                unset($grn_data['type_a_type_b_json']);
                unset($grn_data['reading_json']);
        //        $grn_data[] = $type_a_type_b_json;
                $main_arr = array();
                foreach ($master_data as $key => $master_dat){
                    $t_arr = array_merge($master_dat,$uuc_data[$key],$type_a_type_b_json[$key],$grn_data);
                    $main_arr[] = $t_arr;
                }
                $grn_data = $main_arr; 
    //            echo "<pre>"; print_r($grn_data); exit;
                ?>
                <!--Table Info 1 -->
               <?php  if($table_info_id == '1'){  ?>
                    <table width="100%" align="left" class="main_table"  style="font-size:14px;">
                        <?php
                        $is_3_phase = 0;
                        foreach ($grn_data as $k => $grn_entry){ 
                            if($grn_entry['phase_type_id'] == 2){
                                $is_3_phase = 1;
                                break;
                            }
                        }
                        $table_title = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                            if(!empty($grn_entry['table_title'])){
                                $table_title = $grn_entry['table_title'];
                                break;
                            }
                        }
                        $pre_range = '';
                        $pre_range_key = '';
                        $pre_range_total = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                            if(empty($pre_range)){
                                $pre_range = $grn_entry['mRangeId'];
                                $pre_range_key = $k;
                                $pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$k]['row_span'] = 1;
                                }
                            } else {
                                if($pre_range == $grn_entry['mRangeId']){
                                    $pre_range_total = $pre_range_total + 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                    }
                                } else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                    $pre_range = $grn_entry['mRangeId'];
                                    $pre_range_key = $k;
                                    $pre_range_total = 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                    }
                                }
                            }
                        }
        //                echo "<pre>"; print_r($grn_data); exit; 
                        $t_pre_range = '';
                        $t_pre_range_key = '';
                        $t_pre_range_total = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                            if($k == 0){

                                $t_pre_range = $grn_entry['uut_range'];
                                $t_pre_range_key = $k;
                                $t_pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$k]['row_span_test'] = 1;
                                }
                            } else {
                                if($t_pre_range == $grn_entry['uut_range']){
                                    $t_pre_range_total = $t_pre_range_total + 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                    }
                                } else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                    $t_pre_range = $grn_entry['uut_range'];
                                    $t_pre_range_key = $k;
                                    $t_pre_range_total = 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                    }
                                }
                            }
                        }
        //                echo "<pre>"; print_r($grn_data); exit; 
                        ?>
                        <tr>
                            <th align="center" valign="middle">Title:</th>
                            <th  <?= !empty($is_3_phase) ? 'colspan="7"' : 'colspan="6"'; ?> align="left" valign="middle">&nbsp; <?php echo $table_title; ?></th>
                        </tr>
                        <tr>
                            <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                            <th colspan="2" align="center" valign="middle">Calibration Standard</th>
                            <th align="center" valign="middle" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                            <th align="center" colspan="2" valign="middle">Error</th>
                        </tr>
                        <?php 
                            $master_unit_type1 = '';
                            $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                    WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                            $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                            while($row1 = mysql_fetch_assoc($res1)) { 
                                $master_unit_type1 = $row1['range_unit'];
                            }

                            ?>
                        <tr>
                            <th align="center" width="80px" valign="middle">Range</th>
                            <th align="center" valign="middle">Reading in <br/><?php echo $master_unit_type1; ?></th>
                            <th align="center" valign="middle" width="80px">Range</th>
                            <th align="center" valign="middle">Reading 1 <?php echo htmlentities('Ø') ?> <br/>in <?php echo $master_unit_type1; ?></th>
                            <?php if(!empty($is_3_phase)){ ?>
                            <th align="center" valign="middle">Reading 3 <?php echo htmlentities('Ø') ?> <br/>in <?php echo $master_unit_type1; ?></th>
                            <?php } ?>
                            <th align="center" valign="middle">Unit(+/-)</th>
                            <th align="center" valign="middle">% of Rdg.</th>
                        </tr>
                        <?php 
                            $total_test_meter_val = 0;
                            $total_std_meter_val = 0;
                            $max_ex_unce = '';
                            foreach ($grn_data as $k => $grn_entry){ 
                            $i = $k + 1;

                            $master_hz = '';
                            $master_unit_type = '';
                            $master_range = '';
                            $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                    WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                            $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                            while($row1 = mysql_fetch_assoc($res1)) { 
                                $master_hz = $row1['frequency'];
                                $master_unit_type = $row1['range_unit'];
                                $master_range = $row1['rangeValue'];
                            }

                            ?>
                            <tr>
                                <td align="center" valign="middle"><?php echo $i; ?></td>
                                <?php if(isset($grn_entry['row_span'])){ ?>
                                <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $master_range.' '.$master_unit_type.' @ '.$master_hz ?></div></td>
                                <?php } ?>
                                <?php 
                                    $test_meter_val = $grn_entry['testMeterAverage'];
                                    $std_meter_val = $grn_entry['stdMeterAverage'];
                                   $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                                   $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                                   $decimal_master = 4;
                                   $resolution_of_master = $grn_entry['resolution_of_master'];

                                   $ex_unce = $grn_entry['expandedUncertinityInPre'];
                                   if($max_ex_unce == ''){
                                       $max_ex_unce = $ex_unce;
                                   } else {
                                       if($ex_unce >= $max_ex_unce){
                                           $max_ex_unce = $ex_unce;
                                       }
                                   }

                                   if(!empty($resolution_of_master)){
                                       $exp_val = explode('.', $resolution_of_master);
                                       if(isset($exp_val[1])){
                                           $decimal_master = strlen($exp_val[1]);  
                                       } else {
                                         $decimal_master = 0;  
                                       }
                                   } else {
                                       $decimal_master = 4;
                                   }
                                   $decimal_least = 4;
                                   $resolution_of_least = $grn_entry['leastCount'];
                                   if(!empty($resolution_of_least)){
                                       $exp_least = explode('.', $resolution_of_least);
                                       if(isset($exp_least[1])){
                                           $decimal_least = strlen($exp_least[1]);  
                                       } else {
                                         $decimal_least = 0;  
                                       }
                                   } else {
                                       $decimal_least = 4;
                                   }
                                ?>
                                <td align="right"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                                <?php if(isset($grn_entry['row_span_test'])){ ?>
                                <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $grn_entry['uut_range'] ?></div></td>
                                <?php } ?>
                                <td align="right"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                                <?php if(!empty($is_3_phase)){ ?>
                                    <td align="right"><?php echo number_format((float)$std_meter_val * 1.732, $decimal_least, '.', ''); ?></td>
                                <?php } ?>
                                <td align="right"><?php echo number_format((float)$std_meter_val - $test_meter_val , $decimal_master, '.', ''); ?></td>
                                <?php $per = 100 * ($std_meter_val - $test_meter_val) / $test_meter_val;  ?>
                                <td align="right"><?php echo number_format((float)$per, $decimal_master, '.', ''); ?></td>
                            </tr>
                        <?php } ?>
                            <tr>
                                <td><b>&nbsp;</b></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <?php if(!empty($is_3_phase)){ ?>
                                    <td>&nbsp;</td>
                                <?php } ?>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" colspan="3"><b>Max Expanded Uncert. in %</b></td>
                                <td align="right"><b><?php echo number_format((float)$max_ex_unce, $decimal_master, '.', ''); ?></b></td>
                                <td>&nbsp;</td>
                                <?php if(!empty($is_3_phase)){ ?>
                                    <td>&nbsp;</td>
                                <?php } ?>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>

                    </table>
                    <br/>
                    <br/>
                <?php } ?>
                <!--Table Info 2 -->
               <?php  if($table_info_id == '5'){  ?>
                     <table width="100%" align="left" class="main_table"  style="font-size:14px;">
                        <?php
                        $is_3_phase = 0;
                        $table_title = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                            if(!empty($grn_entry['table_title'])){
                                $table_title = $grn_entry['table_title'];
                                break;
                            }
                        }
                        $pre_range = '';
                        $pre_range_key = '';
                        $pre_range_total = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                            if(empty($pre_range)){
                                $pre_range = $grn_entry['mRangeId'];
                                $pre_range_key = $k;
                                $pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$k]['row_span'] = 1;
                                }
                            } else {
                                if($pre_range == $grn_entry['mRangeId']){
                                    $pre_range_total = $pre_range_total + 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                    }
                                } else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                    $pre_range = $grn_entry['mRangeId'];
                                    $pre_range_key = $k;
                                    $pre_range_total = 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                    }
                                }
                            }
                        }
        //                echo "<pre>"; print_r($grn_data); exit; 
                        $t_pre_range = '';
                        $t_pre_range_key = '';
                        $t_pre_range_total = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                            if($k == 0){

                                $t_pre_range = $grn_entry['uut_range'];
                                $t_pre_range_key = $k;
                                $t_pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$k]['row_span_test'] = 1;
                                }
                            } else {
                                if($t_pre_range == $grn_entry['uut_range']){
                                    $t_pre_range_total = $t_pre_range_total + 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                    }
                                } else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                    $t_pre_range = $grn_entry['uut_range'];
                                    $t_pre_range_key = $k;
                                    $t_pre_range_total = 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                    }
                                }
                            }
                        }
        //                echo "<pre>"; print_r($grn_data); exit; 
                        ?>
                        <?php 
                            $master_unit_type1 = '';
                            $master_range1 = '';
                            $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                    WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                            $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                            while($row1 = mysql_fetch_assoc($res1)) { 
                                $master_unit_type1 = $row1['range_unit'];
                                $master_range1 = $row1['rangeValue'];
                            }
                            $uut_range1 = $grn_data[0]['uut_range'];
                        ?>
                        <tr>
                            <th align="center" valign="middle">Title:</th>
                            <th  <?= !empty($is_3_phase) ? 'colspan="7"' : 'colspan="6"'; ?> align="left" valign="middle">&nbsp; <?php echo $table_title; ?></th>
                        </tr>
                        <tr>
                            <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                            <th colspan="2" align="center" valign="middle">Calibration Standard</th>
                            <th align="center" valign="middle" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                            <th align="center" colspan="2" valign="middle">Error</th>
                        </tr>
                        <tr>
                            <th align="center" width="80px" valign="middle">Range</th>
                            <th align="center" valign="middle">Reading in <br/><?php echo $master_unit_type1; ?></th>
                            <th align="center" valign="middle" width="80px">Range</th>
                            <th align="center" valign="middle">Reading in <?php echo $master_unit_type1; ?></th>
                            <?php if(!empty($is_3_phase)){ ?>
                            <th align="center" valign="middle">Reading 3 <?php echo htmlentities('Ø') ?> <br/>in <?php echo $master_unit_type1; ?></th>
                            <?php } ?>
                            <th align="center" valign="middle">Unit(+/-)</th>
                            <th align="center" valign="middle">% of Rdg.</th>
                        </tr>
                        <?php 
                            $total_test_meter_val = 0;
                            $total_std_meter_val = 0;
                            $max_ex_unce = '';
                            foreach ($grn_data as $k => $grn_entry){ 
                            $i = $k + 1;

                            $master_hz = '';
                            $master_unit_type = '';
                            $master_range = '';
                            $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                    WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                            $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                            while($row1 = mysql_fetch_assoc($res1)) { 
                                $master_hz = $row1['frequency'];
                                $master_unit_type = $row1['range_unit'];
                                $master_range = $row1['rangeValue'];
                            }

                            ?>
                            <tr>
                                <td align="center" valign="middle"><?php echo $i; ?></td>
                                <?php if(isset($grn_entry['row_span'])){ ?>
                                <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $master_range.' '.$master_unit_type.' @ '.$master_hz ?></div></td>
                                <?php } ?>
                                <?php 
                                    $test_meter_val = $grn_entry['testMeterAverage'];
                                    $std_meter_val = $grn_entry['stdMeterAverage'];
                                   $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                                   $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                                   $decimal_master = 4;
                                   $resolution_of_master = $grn_entry['resolution_of_master'];

                                   $ex_unce = $grn_entry['expandedUncertinityInPre'];
                                   if($max_ex_unce == ''){
                                       $max_ex_unce = $ex_unce;
                                   } else {
                                       if($ex_unce >= $max_ex_unce){
                                           $max_ex_unce = $ex_unce;
                                       }
                                   }

                                   if(!empty($resolution_of_master)){
                                       $exp_val = explode('.', $resolution_of_master);
                                       if(isset($exp_val[1])){
                                           $decimal_master = strlen($exp_val[1]);  
                                       } else {
                                         $decimal_master = 0;  
                                       }
                                   } else {
                                       $decimal_master = 4;
                                   }
                                   $decimal_least = 4;
                                   $resolution_of_least = $grn_entry['leastCount'];
                                   if(!empty($resolution_of_least)){
                                       $exp_least = explode('.', $resolution_of_least);
                                       if(isset($exp_least[1])){
                                           $decimal_least = strlen($exp_least[1]);  
                                       } else {
                                         $decimal_least = 0;  
                                       }
                                   } else {
                                       $decimal_least = 4;
                                   }
                                ?>
                                <td align="right"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                                <?php if(isset($grn_entry['row_span_test'])){ ?>
                                <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $grn_entry['uut_range'] ." ".$master_unit_type ?></div></td>
                                <?php } ?>
                                <td align="right"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                                <?php if(!empty($is_3_phase)){ ?>
                                    <td align="right"><?php echo number_format((float)$std_meter_val * 1.732, $decimal_least, '.', ''); ?></td>
                                <?php } ?>
                                <?php $units = $std_meter_val - $test_meter_val;  ?>
                                <td align="right"><?php echo number_format((float)$units, $decimal_master, '.', ''); ?></td>
                                <?php $per = 100 * $units / $test_meter_val;  ?>
                                <td align="right"><?php echo number_format((float)$per, $decimal_master, '.', ''); ?></td>

                            </tr>
                        <?php } ?>
                            <tr>
                                <td><b>&nbsp;</b></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <?php if(!empty($is_3_phase)){ ?>
                                    <td>&nbsp;</td>
                                <?php } ?>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" colspan="3"><b>Max Expanded Uncert. in %</b></td>
                                <td align="right"><b><?php echo number_format((float)$max_ex_unce, $decimal_master, '.', ''); ?></b></td>
                                <td>&nbsp;</td>
                                <?php if(!empty($is_3_phase)){ ?>
                                    <td>&nbsp;</td>
                                <?php } ?>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                    </table>
                    <br/>
                    <br/>
                <?php } ?>
                <!--Table Info 3 -->
                <?php  if($table_info_id == '3'){  ?>
                    <table width="100%" align="left" class="main_table"  style="font-size:14px;">
                    <?php
                    $is_3_phase = 0;
                    foreach ($grn_data as $k => $grn_entry){ 
                        if($grn_entry['phase_type_id'] == 2){
                            $is_3_phase = 1;
                            break;
                        }
                    }
                    $table_title = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(!empty($grn_entry['table_title'])){
                            $table_title = $grn_entry['table_title'];
                            break;
                        }
                    }
                    $pre_range = '';
                    $pre_range_key = '';
                    $pre_range_total = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(empty($pre_range)){
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$k]['row_span'] = 1;
                            }
                        } else {
                            if($pre_range == $grn_entry['mRangeId']){
                                $pre_range_total = $pre_range_total + 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                }
                            } else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                $pre_range = $grn_entry['mRangeId'];
                                $pre_range_key = $k;
                                $pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                }
                            }
                        }
                    }
    //                echo "<pre>"; print_r($grn_data); exit; 
                    $t_pre_range = '';
                    $t_pre_range_key = '';
                    $t_pre_range_total = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if($k == 0){

                            $t_pre_range = $grn_entry['uut_range'];
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$k]['row_span_test'] = 1;
                            }
                        } else {
                            if($t_pre_range == $grn_entry['uut_range']){
                                $t_pre_range_total = $t_pre_range_total + 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                }
                            } else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                $t_pre_range = $grn_entry['uut_range'];
                                $t_pre_range_key = $k;
                                $t_pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                }
                            }
                        }
                    }
    //                echo "<pre>"; print_r($grn_data); exit; 
                    ?>
                    <?php 
                        $master_unit_type1 = '';
                        $master_range1 = '';
                        $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                        $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                        while($row1 = mysql_fetch_assoc($res1)) { 
                            $master_unit_type1 = $row1['range_unit'];
                            $master_range1 = $row1['rangeValue'];
                        }
                        $uut_range1 = $grn_data[0]['uut_range'];
                        $uut_range2 = $grn_data[0]['uut_range_sec'];
                        $ratio = $uut_range1 / $uut_range2;
                    ?>
                    <tr>
                        <th align="center" valign="middle">Title:</th>
                        <th  <?= !empty($is_3_phase) ? 'colspan="5"' : 'colspan="4"'; ?> align="left" valign="middle">&nbsp; <?php echo $table_title; ?></th>
                        <th  align="left" valign="middle">Ratio </th>
                        <th  align="left" valign="middle" colspan="2"><?php echo number_format((float)$ratio, 3, '.', ''); ?></th>
                    </tr>
                    <tr>
                        <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                        <th colspan="2" align="center" valign="middle">Calibration Standard</th>
                        <th align="center" valign="middle" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                        <th align="center" colspan="2" valign="middle">Error</th>
                        <th align="center" valign="middle">&nbsp;</th>
                    </tr>
                    <tr>
                        <th align="center" width="80px" valign="middle">Range</th>
                        <th align="center" valign="middle">Applied <br/><?php echo $master_unit_type1; ?></th>
                        <th align="center" valign="middle" width="80px">Range</th>
                        <th align="center" valign="middle">Reading in <?php echo $master_unit_type1; ?></th>
                        <?php if(!empty($is_3_phase)){ ?>
                        <th align="center" valign="middle">Reading 3 <?php echo htmlentities('Ø') ?> <br/>in <?php echo $master_unit_type1; ?></th>
                        <?php } ?>
                        <th align="center" valign="middle">Unit(+/-)</th>
                        <th align="center" valign="middle">% of Rdg.</th>
                        <th align="center" valign="middle" style="">Expan.<br/> Uncert.%</th>
                    </tr>
                    <?php 
                        $total_test_meter_val = 0;
                        $total_std_meter_val = 0;
                        $max_ex_unce = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                        $i = $k + 1;

                        $master_hz = '';
                        $master_unit_type = '';
                        $master_range = '';
                        $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                        $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                        while($row1 = mysql_fetch_assoc($res1)) { 
                            $master_hz = $row1['frequency'];
                            $master_unit_type = $row1['range_unit'];
                            $master_range = $row1['rangeValue'];
                        }

                        ?>
                        <tr>
                            <td align="center" valign="middle"><?php echo $i; ?></td>
                            <?php if(isset($grn_entry['row_span'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $master_range.' '.$master_unit_type.' @ '.$master_hz ?></div></td>
                            <?php } ?>
                            <?php 
                                $test_meter_val = $grn_entry['testMeterAverage'];
                                $std_meter_val = $grn_entry['stdMeterAverage'];
                               $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                               $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                               $decimal_master = 4;
                               $resolution_of_master = $grn_entry['resolution_of_master'];

                               $ex_unce = $grn_entry['expandedUncertinityInPre'];
                               if($max_ex_unce == ''){
                                   $max_ex_unce = $ex_unce;
                               } else {
                                   if($ex_unce >= $max_ex_unce){
                                       $max_ex_unce = $ex_unce;
                                   }
                               }

                               if(!empty($resolution_of_master)){
                                   $exp_val = explode('.', $resolution_of_master);
                                   if(isset($exp_val[1])){
                                       $decimal_master = strlen($exp_val[1]);  
                                   } else {
                                     $decimal_master = 0;  
                                   }
                               } else {
                                   $decimal_master = 4;
                               }
                               $decimal_least = 4;
                               $resolution_of_least = $grn_entry['leastCount'];
                               if(!empty($resolution_of_least)){
                                   $exp_least = explode('.', $resolution_of_least);
                                   if(isset($exp_least[1])){
                                       $decimal_least = strlen($exp_least[1]);  
                                   } else {
                                     $decimal_least = 0;  
                                   }
                               } else {
                                   $decimal_least = 4;
                               }
                            ?>
                            <td align="right"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                            <?php if(isset($grn_entry['row_span_test'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $grn_entry['uut_range'] ." ".$master_unit_type. "/".$grn_entry['uut_range_sec'] ." ".$master_unit_type ?></div></td>
                            <?php } ?>
                            <td align="right"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                            <?php if(!empty($is_3_phase)){ ?>
                                <td align="right"><?php echo number_format((float)$std_meter_val * 1.732, $decimal_least, '.', ''); ?></td>
                            <?php } ?>
                            <?php $units = ($std_meter_val * $ratio) - $test_meter_val;  ?>
                            <td align="right"><?php echo number_format((float)$units, $decimal_master, '.', ''); ?></td>
                            <?php $per = 100 * $units / $test_meter_val;  ?>
                            <td align="right"><?php echo number_format((float)$per, $decimal_master, '.', ''); ?></td>
                            <?php $expandedUncert_pre = $grn_entry['expandedUncertinity'] * 100 / $test_meter_val;  ?>
                            <td align="right"><?php echo number_format((float)$expandedUncert_pre, $decimal_master, '.', ''); ?></td>
                        </tr>
                    <?php } ?>
                        <tr>
                            <td><b>&nbsp;</b></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <?php if(!empty($is_3_phase)){ ?>
                                <td>&nbsp;</td>
                            <?php } ?>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                <?php } ?>
                <!--Table Info 5 -->
                <?php  if($table_info_id == '6'){  ?>
                    <table width="100%" align="left" class="main_table"  style="font-size:13px;">
                    <?php
                    $is_3_phase = 0;
                    $table_title = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(!empty($grn_entry['table_title'])){
                            $table_title = $grn_entry['table_title'];
                            break;
                        }
                    }
                    $pre_range = '';
                    $pre_range_key = '';
                    $pre_range_total = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(empty($pre_range)){
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$k]['row_span'] = 1;
                            }
                        } else {
                            if($pre_range == $grn_entry['mRangeId']){
                                $pre_range_total = $pre_range_total + 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                }
                            } else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                $pre_range = $grn_entry['mRangeId'];
                                $pre_range_key = $k;
                                $pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                }
                            }
                        }
                    }
    //                echo "<pre>"; print_r($grn_data); exit; 
                    $t_pre_range = '';
                    $t_pre_range_key = '';
                    $t_pre_range_total = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if($k == 0){

                            $t_pre_range = $grn_entry['uut_range'];
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$k]['row_span_test'] = 1;
                            }
                        } else {
                            if($t_pre_range == $grn_entry['uut_range']){
                                $t_pre_range_total = $t_pre_range_total + 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                }
                            } else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                $t_pre_range = $grn_entry['uut_range'];
                                $t_pre_range_key = $k;
                                $t_pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                }
                            }
                        }
                    }
    //                echo "<pre>"; print_r($grn_data); exit; 
                    ?>
                    <?php 
                        $master_unit_type1 = '';
                        $master_range1 = '';
                        $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                        $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                        while($row1 = mysql_fetch_assoc($res1)) { 
                            $master_unit_type1 = $row1['range_unit'];
                            $master_range1 = $row1['rangeValue'];
                        }
                        $uut_range1 = $grn_data[0]['uut_range'];
                    ?>
                    <tr>
                        <th align="center" valign="middle">Title:</th>
                        <th  <?= !empty($is_3_phase) ? 'colspan="9"' : 'colspan="8"'; ?> align="left" valign="middle">&nbsp; <?php echo $table_title; ?></th>
                    </tr>
                    <tr>
                        <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                        <th colspan="4" align="center" valign="middle">Calibration Standard</th>
                        <th align="center" valign="middle" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                        <th align="center" colspan="2" valign="middle">Error</th>
                    </tr>
                    <tr>
                        <th align="center" width="80px" valign="middle">Range</th>
                        <th align="center" valign="middle">Reading in <br/><?php echo $master_unit_type1; ?> <br/>Primary</th>
                        <th align="center" valign="middle">Reading in <br/><?php echo $master_unit_type1; ?> <br/>Secondary</th>
                        <th align="center" valign="middle">Calculated <br/>Ratio</th>
                        <th align="center" valign="middle" width="80px">Range</th>
                        <th align="center" valign="middle">Observed <br/>Ratio</th>
                        <?php if(!empty($is_3_phase)){ ?>
                        <th align="center" valign="middle">Reading 3 <?php echo htmlentities('Ø') ?> <br/>in <?php echo $master_unit_type1; ?></th>
                        <?php } ?>
                        <th align="center" valign="middle">Unit(+/-)</th>
                        <th align="center" valign="middle">% of Rdg.</th>
                    </tr>
                    <?php 
                        $total_test_meter_val = 0;
                        $total_std_meter_val = 0;
                        $max_ex_unce = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                        $i = $k + 1;

                        $master_hz = '';
                        $master_unit_type = '';
                        $master_range = '';
                        $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                        $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                        while($row1 = mysql_fetch_assoc($res1)) { 
                            $master_hz = $row1['frequency'];
                            $master_unit_type = $row1['range_unit'];
                            $master_range = $row1['rangeValue'];
                        }

                        ?>
                        <tr>
                            <td align="center" valign="middle"><?php echo $i; ?></td>
                            <?php if(isset($grn_entry['row_span'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $master_range.' '.$master_unit_type.' @ '.$master_hz ?></div></td>
                            <?php } ?>
                            <?php 
                                $test_meter_val = $grn_entry['testMeterAverage'];
                                $test_meter2_val = $grn_entry['testMeter2Average'];
                                $std_meter_val = $grn_entry['stdMeterAverage'];
                                $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                                $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                                $decimal_master = 4;
                                $resolution_of_master = $grn_entry['resolution_of_master'];

                               $ex_unce = $grn_entry['expandedUncertinityInPre'];
                               if($max_ex_unce == ''){
                                   $max_ex_unce = $ex_unce;
                               } else {
                                   if($ex_unce >= $max_ex_unce){
                                       $max_ex_unce = $ex_unce;
                                   }
                               }

                               if(!empty($resolution_of_master)){
                                   $exp_val = explode('.', $resolution_of_master);
                                   if(isset($exp_val[1])){
                                       $decimal_master = strlen($exp_val[1]);  
                                   } else {
                                     $decimal_master = 0;  
                                   }
                               } else {
                                   $decimal_master = 4;
                               }
                               $decimal_least = 4;
                               $resolution_of_least = $grn_entry['leastCount'];
                               if(!empty($resolution_of_least)){
                                   $exp_least = explode('.', $resolution_of_least);
                                   if(isset($exp_least[1])){
                                       $decimal_least = strlen($exp_least[1]);  
                                   } else {
                                     $decimal_least = 0;  
                                   }
                               } else {
                                   $decimal_least = 4;
                               }
                            ?>
                            <td align="right"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                            <td align="right"><?php echo number_format((float)$test_meter2_val, $decimal_master, '.', ''); ?></td>
                            <td align="right"><?php echo number_format((float)$test_meter_val / $test_meter2_val, $decimal_master, '.', ''); ?></td>
                            <?php if(isset($grn_entry['row_span_test'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $grn_entry['uut_range'] ." ".$master_unit_type ?></div></td>
                            <?php } ?>
                            <td align="right"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                            <?php if(!empty($is_3_phase)){ ?>
                                <td align="right"><?php echo number_format((float)$std_meter_val * 1.732, $decimal_least, '.', ''); ?></td>
                            <?php } ?>
                            <?php $units = $std_meter_val - ($test_meter_val / $test_meter2_val);  ?>
                            <td align="right"><?php echo number_format((float)$units, $decimal_master, '.', ''); ?></td>
                            <?php $per = 100 * ($units) / ($test_meter_val / $test_meter2_val);  ?>
                            <td align="right"><?php echo number_format((float)$per, $decimal_master, '.', ''); ?></td>

                        </tr>
                    <?php } ?>
                        <tr>
                            <td><b>&nbsp;</b></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <?php if(!empty($is_3_phase)){ ?>
                                <td>&nbsp;</td>
                            <?php } ?>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" colspan="3"><b>Max Expanded Uncert. in %</b></td>
                            <td align="right"><b><?php echo number_format((float)$max_ex_unce, $decimal_master, '.', ''); ?></b></td>
                            <td>&nbsp;</td>
                            <?php if(!empty($is_3_phase)){ ?>
                                <td>&nbsp;</td>
                            <?php } ?>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                <?php } ?>
                <!--Table Info 6A -->
                <?php  if($table_info_id == '7'){  ?>
                    <table width="100%" align="left" class="main_table"  style="font-size:13px;">
                        <?php
                        $is_3_phase = 0;
                        $table_title = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                            if(!empty($grn_entry['table_title'])){
                                $table_title = $grn_entry['table_title'];
                                break;
                            }
                        }
                        $pre_range = '';
                        $pre_range_key = '';
                        $pre_range_total = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                            if(empty($pre_range)){
                                $pre_range = $grn_entry['mRangeId'];
                                $pre_range_key = $k;
                                $pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$k]['row_span'] = 1;
                                }
                            } else {
                                if($pre_range == $grn_entry['mRangeId']){
                                    $pre_range_total = $pre_range_total + 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                    }
                                } else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                    $pre_range = $grn_entry['mRangeId'];
                                    $pre_range_key = $k;
                                    $pre_range_total = 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                    }
                                }
                            }
                        }
        //                echo "<pre>"; print_r($grn_data); exit; 
                        $t_pre_range = '';
                        $t_pre_range_key = '';
                        $t_pre_range_total = '';
                        foreach ($grn_data as $k => $grn_entry){ 
                            if($k == 0){

                                $t_pre_range = $grn_entry['uut_range'];
                                $t_pre_range_key = $k;
                                $t_pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$k]['row_span_test'] = 1;
                                }
                            } else {
                                if($t_pre_range == $grn_entry['uut_range']){
                                    $t_pre_range_total = $t_pre_range_total + 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                    }
                                } else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                    $t_pre_range = $grn_entry['uut_range'];
                                    $t_pre_range_key = $k;
                                    $t_pre_range_total = 1;
                                    if(isset($grn_data[$k+1])){} else {
                                        $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                    }
                                }
                            }
                        }
        //                echo "<pre>"; print_r($grn_data); exit; 
                        ?>
                        <?php 
                            $master_unit_type1 = '';
                            $master_range1 = '';
                            $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                    WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                            $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                            while($row1 = mysql_fetch_assoc($res1)) { 
                                $master_unit_type1 = $row1['range_unit'];
                                $master_range1 = $row1['rangeValue'];
                            }
                            $uut_range1 = $grn_data[0]['uut_range'];
                        ?>
                        <tr>
                            <th align="center" valign="middle">Title:</th>
                            <th  <?= !empty($is_3_phase) ? 'colspan="8"' : 'colspan="7"'; ?> align="left" valign="middle">&nbsp; <?php echo $table_title; ?></th>
                        </tr>
                        <tr>
                            <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                            <th colspan="2" align="center" valign="middle">Calibration Standard</th>
                            <th align="center" valign="middle" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                            <th align="center" colspan="3" valign="middle">Error</th>
                        </tr>
                        <tr>
                            <th align="center" width="80px" valign="middle">Range</th>
                            <th align="center" valign="middle">Applied <br/><?php echo $master_unit_type1; ?></th>
                            <th align="center" valign="middle" width="80px">Range</th>
                            <th align="center" valign="middle">Secondary <br/>Reading in <br/><?php echo $master_unit_type1; ?></th>
                            <th align="center" valign="middle">Secondary <br/> Calculated <br/>Reading in <br/><?php echo $master_unit_type1; ?></th>
                            <?php if(!empty($is_3_phase)){ ?>
                            <th align="center" valign="middle">Reading 3 <?php echo htmlentities('Ø') ?> <br/>in <?php echo $master_unit_type1; ?></th>
                            <?php } ?>
                            <th align="center" valign="middle">Unit(+/-)</th>
                            <th align="center" valign="middle">% of Rdg.</th>
                        </tr>
                        <?php 
                            $total_test_meter_val = 0;
                            $total_std_meter_val = 0;
                            $max_ex_unce = '';
                            foreach ($grn_data as $k => $grn_entry){ 
                            $i = $k + 1;

                            $master_hz = '';
                            $master_unit_type = '';
                            $master_range = '';
                            $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                    WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                            $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                            while($row1 = mysql_fetch_assoc($res1)) { 
                                $master_hz = $row1['frequency'];
                                $master_unit_type = $row1['range_unit'];
                                $master_range = $row1['rangeValue'];
                            }

                            ?>
                            <tr>
                                <td align="center" valign="middle"><?php echo $i; ?></td>
                                <?php if(isset($grn_entry['row_span'])){ ?>
                                <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $master_range.' '.$master_unit_type.' @ '.$master_hz ?></div></td>
                                <?php } ?>
                                <?php 
                                    $test_meter_val = $grn_entry['testMeterAverage'];
                                    $std_meter_val = $grn_entry['stdMeterAverage'];
                                    $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                                    $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                                    $decimal_master = 4;
                                    $resolution_of_master = $grn_entry['resolution_of_master'];

                                   $ex_unce = $grn_entry['expandedUncertinityInPre'];
                                   if($max_ex_unce == ''){
                                       $max_ex_unce = $ex_unce;
                                   } else {
                                       if($ex_unce >= $max_ex_unce){
                                           $max_ex_unce = $ex_unce;
                                       }
                                   }

                                   if(!empty($resolution_of_master)){
                                       $exp_val = explode('.', $resolution_of_master);
                                       if(isset($exp_val[1])){
                                           $decimal_master = strlen($exp_val[1]);  
                                       } else {
                                         $decimal_master = 0;  
                                       }
                                   } else {
                                       $decimal_master = 4;
                                   }
                                   $decimal_least = 4;
                                   $resolution_of_least = $grn_entry['leastCount'];
                                   if(!empty($resolution_of_least)){
                                       $exp_least = explode('.', $resolution_of_least);
                                       if(isset($exp_least[1])){
                                           $decimal_least = strlen($exp_least[1]);  
                                       } else {
                                         $decimal_least = 0;  
                                       }
                                   } else {
                                       $decimal_least = 4;
                                   }
                                ?>
                                <td align="right"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                                <?php if(isset($grn_entry['row_span_test'])){ ?>
                                <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $grn_entry['uut_range'] ." ".$master_unit_type ?></div></td>
                                <?php } ?>
                                <td align="right"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                                <?php $sec_cal_reading = $test_meter_val * 5 / 100;  ?>
                                <td align="right"><?php echo number_format((float)$sec_cal_reading, $decimal_master, '.', ''); ?></td>
                                <?php $units = $std_meter_val - $sec_cal_reading;  ?>
                                <td align="right"><?php echo number_format((float)$units, $decimal_master, '.', ''); ?></td>
                                <?php $per = (($units)/$sec_cal_reading)*100;  ?>
                                <td align="right"><?php echo number_format((float)$per, $decimal_master, '.', ''); ?></td>

                            </tr>
                        <?php } ?>
                            <tr>
                                <td><b>&nbsp;</b></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <?php if(!empty($is_3_phase)){ ?>
                                    <td>&nbsp;</td>
                                <?php } ?>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" colspan="3"><b>Max Expanded Uncert. in %</b></td>
                                <td align="right"><b><?php echo number_format((float)$max_ex_unce, $decimal_master, '.', ''); ?></b></td>
                                <td>&nbsp;</td>
                                <?php if(!empty($is_3_phase)){ ?>
                                    <td>&nbsp;</td>
                                <?php } ?>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                    </table>
                    <br/>
                    <br/>
                <?php } ?>
                <!--Table Info 7 -->
                <?php  if($table_info_id == '8'){  ?>
                    <table width="100%" align="left" class="main_table"  style="font-size:13px;">
                    <?php
                    $is_3_phase = 0;
                    $table_title = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(!empty($grn_entry['table_title'])){
                            $table_title = $grn_entry['table_title'];
                            break;
                        }
                    }
                    $pre_range = '';
                    $pre_range_key = '';
                    $pre_range_total = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(empty($pre_range)){
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$k]['row_span'] = 1;
                            }
                        } else {
                            if($pre_range == $grn_entry['mRangeId']){
                                $pre_range_total = $pre_range_total + 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                }
                            } else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                $pre_range = $grn_entry['mRangeId'];
                                $pre_range_key = $k;
                                $pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                }
                            }
                        }
                    }
    //                echo "<pre>"; print_r($grn_data); exit; 
                    $t_pre_range = '';
                    $t_pre_range_key = '';
                    $t_pre_range_total = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if($k == 0){

                            $t_pre_range = $grn_entry['uut_range'];
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$k]['row_span_test'] = 1;
                            }
                        } else {
                            if($t_pre_range == $grn_entry['uut_range']){
                                $t_pre_range_total = $t_pre_range_total + 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                }
                            } else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                $t_pre_range = $grn_entry['uut_range'];
                                $t_pre_range_key = $k;
                                $t_pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                }
                            }
                        }
                    }
    //                echo "<pre>"; print_r($grn_data); exit; 
                    ?>
                    <?php 
                        $master_unit_type1 = '';
                        $master_range1 = '';
                        $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                        $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                        while($row1 = mysql_fetch_assoc($res1)) { 
                            $master_unit_type1 = $row1['range_unit'];
                            $master_range1 = $row1['rangeValue'];
                        }
                        $uut_range1 = $grn_data[0]['uut_range'];
                    ?>
                    <tr>
                        <th align="center" valign="middle">Title:</th>
                        <th  <?= !empty($is_3_phase) ? 'colspan="8"' : 'colspan="7"'; ?> align="left" valign="middle">&nbsp; <?php echo $table_title; ?></th>
                    </tr>
                    <tr>
                        <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                        <th colspan="2" align="center" valign="middle">Calibration Standard</th>
                        <th align="center" valign="middle">&nbsp;</th>
                        <th align="center" valign="middle" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                        <th align="center" colspan="2" valign="middle">Error</th>
                    </tr>
                    <tr>
                        <th align="center" width="80px" valign="middle">Range</th>
                        <th align="center" valign="middle">Applied <br/><?php echo $master_unit_type1; ?></th>
                        <th align="center" valign="middle">Calculated <br/>Reading in <br/><?php echo $master_unit_type1; ?></th>
                        <th align="center" valign="middle" width="80px">Range</th>
                        <th align="center" valign="middle">Reading in <br/><?php echo $master_unit_type1; ?></th>
                        <th align="center" valign="middle">Unit(+/-)</th>
                        <th align="center" valign="middle">% of Rdg.</th>
                    </tr>
                    <?php 
                        $total_test_meter_val = 0;
                        $total_std_meter_val = 0;
                        $max_ex_unce = '';
                        $ratio_bottom_part = '';

                        foreach ($grn_data as $k => $grn_entry){

                        if($ratio_bottom_part == ''){
                            $ratio_bottom_part = $grn_entry['ratio_bottom_part'];
                        }

                        $i = $k + 1;

                        $master_hz = '';
                        $master_unit_type = '';
                        $master_range = '';
                        $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                        $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                        while($row1 = mysql_fetch_assoc($res1)) { 
                            $master_hz = $row1['frequency'];
                            $master_unit_type = $row1['range_unit'];
                            $master_range = $row1['rangeValue'];
                        }

                        ?>
                        <tr>
                            <td align="center" valign="middle"><?php echo $i; ?></td>
                            <?php if(isset($grn_entry['row_span'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $master_range.' '.$master_unit_type.' @ '.$master_hz ?></div></td>
                            <?php } ?>
                            <?php 
                                $test_meter_val = $grn_entry['testMeterAverage'];
                                $std_meter_val = $grn_entry['stdMeterAverage'];
                                $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                                $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                                $decimal_master = 4;
                                $resolution_of_master = $grn_entry['resolution_of_master'];

                               $ex_unce = $grn_entry['expandedUncertinityInPre'];
                               if($max_ex_unce == ''){
                                   $max_ex_unce = $ex_unce;
                               } else {
                                   if($ex_unce >= $max_ex_unce){
                                       $max_ex_unce = $ex_unce;
                                   }
                               }

                               if(!empty($resolution_of_master)){
                                   $exp_val = explode('.', $resolution_of_master);
                                   if(isset($exp_val[1])){
                                       $decimal_master = strlen($exp_val[1]);  
                                   } else {
                                     $decimal_master = 0;  
                                   }
                               } else {
                                   $decimal_master = 4;
                               }
                               $decimal_least = 4;
                               $resolution_of_least = $grn_entry['leastCount'];
                               if(!empty($resolution_of_least)){
                                   $exp_least = explode('.', $resolution_of_least);
                                   if(isset($exp_least[1])){
                                       $decimal_least = strlen($exp_least[1]);  
                                   } else {
                                     $decimal_least = 0;  
                                   }
                               } else {
                                   $decimal_least = 4;
                               }
                            ?>
                            <td align="right"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                            <?php $cal_reading = ($std_meter_val * $grn_entry['ratio_bottom_part'])  / $grn_entry['uut_range'];  ?>
                            <td align="right"><?php echo number_format((float)$cal_reading, $decimal_least, '.', ''); ?></td>
                            <?php if(isset($grn_entry['row_span_test'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $grn_entry['uut_range'] ." ".$master_unit_type ?></div></td>
                            <?php } ?>
                            <td align="right"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                            <?php $units = $cal_reading - $test_meter_val;  ?>
                            <td align="right"><?php echo number_format((float)$units, $decimal_master, '.', ''); ?></td>
                            <?php $per = ((($cal_reading*1-$test_meter_val)/($cal_reading*1)*100));  ?>
                            <td align="right"><?php echo number_format((float)$per, $decimal_master, '.', ''); ?></td>
                        </tr>
                    <?php }
                        $ratio_bottom_part = '';
                    ?>
                        <tr>
                            <td><b>&nbsp;</b></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <?php if(!empty($is_3_phase)){ ?>
                                <td>&nbsp;</td>
                            <?php } ?>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" colspan="3"><b>Max Expanded Uncert. in %</b></td>
                            <td align="right"><b><?php echo number_format((float)$max_ex_unce, $decimal_master, '.', ''); ?></b></td>
                            <td>&nbsp;</td>
                            <?php if(!empty($is_3_phase)){ ?>
                                <td>&nbsp;</td>
                            <?php } ?>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                <?php } ?>
                <!--Table Info 8 -->
                <?php  if($table_info_id == '9'){  ?>
                    <table width="100%" align="left" class="main_table"  style="font-size:13px;">
                    <?php
                    $is_3_phase = 0;
                    $table_title = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(!empty($grn_entry['table_title'])){
                            $table_title = $grn_entry['table_title'];
                            break;
                        }
                    }
                    $pre_range = '';
                    $pre_range_key = '';
                    $pre_range_total = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(empty($pre_range)){
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$k]['row_span'] = 1;
                            }
                        } else {
                            if($pre_range == $grn_entry['mRangeId']){
                                $pre_range_total = $pre_range_total + 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                }
                            } else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                $pre_range = $grn_entry['mRangeId'];
                                $pre_range_key = $k;
                                $pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                }
                            }
                        }
                    }
    //                echo "<pre>"; print_r($grn_data); exit; 
                    $t_pre_range = '';
                    $t_pre_range_key = '';
                    $t_pre_range_total = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if($k == 0){

                            $t_pre_range = $grn_entry['uut_range'];
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$k]['row_span_test'] = 1;
                            }
                        } else {
                            if($t_pre_range == $grn_entry['uut_range']){
                                $t_pre_range_total = $t_pre_range_total + 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                }
                            } else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                $t_pre_range = $grn_entry['uut_range'];
                                $t_pre_range_key = $k;
                                $t_pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                }
                            }
                        }
                    }
    //                echo "<pre>"; print_r($grn_data); exit; 
                    ?>
                    <?php 
                        $master_unit_type1 = '';
                        $master_range1 = '';
                        $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                        $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                        while($row1 = mysql_fetch_assoc($res1)) { 
                            $master_unit_type1 = $row1['range_unit'];
                            $master_range1 = $row1['rangeValue'];
                        }
                        $uut_range1 = $grn_data[0]['uut_range'];
                    ?>
                    <tr>
                        <th align="center" valign="middle">Title:</th>
                        <th  <?= !empty($is_3_phase) ? 'colspan="7"' : 'colspan="6"'; ?> align="left" valign="middle">&nbsp; <?php echo $table_title; ?></th>
                    </tr>
                    <tr>
                        <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                        <th colspan="2" align="center" valign="middle">Calibration Standard</th>
                        <th align="center" valign="middle" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                        <th align="center" colspan="2" valign="middle">Error</th>
                    </tr>
                    <tr>
                        <th align="center" width="80px" valign="middle">Range</th>
                        <th align="center" valign="middle">Reading in <br/><?php echo $master_unit_type1; ?></th>
                        <th align="center" valign="middle" width="80px">Range</th>
                        <th align="center" valign="middle">Reading in <br/><?php echo $master_unit_type1; ?></th>
                        <th align="center" valign="middle">Unit(+/-)</th>
                        <th align="center" valign="middle">% of Rdg.</th>
                    </tr>
                    <?php 
                        $total_test_meter_val = 0;
                        $total_std_meter_val = 0;
                        $max_ex_unce = '';

                        foreach ($grn_data as $k => $grn_entry){


                        $i = $k + 1;

                        $master_hz = '';
                        $master_unit_type = '';
                        $master_range = '';
                        $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                        $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                        while($row1 = mysql_fetch_assoc($res1)) { 
                            $master_hz = $row1['frequency'];
                            $master_unit_type = $row1['range_unit'];
                            $master_range = $row1['rangeValue'];
                        }

                        ?>
                        <tr>
                            <td align="center" valign="middle"><?php echo $i; ?></td>
                            <?php if(isset($grn_entry['row_span'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $master_range.' '.$master_unit_type.' @ '.$master_hz ?></div></td>
                            <?php } ?>
                            <?php 
                                $test_meter_val = $grn_entry['testMeterAverage'];
                                $std_meter_val = $grn_entry['stdMeterAverage'];
                                $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                                $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                                $decimal_master = 4;
                                $resolution_of_master = $grn_entry['resolution_of_master'];

                               $ex_unce = $grn_entry['expandedUncertinityInPre'];
                               if($max_ex_unce == ''){
                                   $max_ex_unce = $ex_unce;
                               } else {
                                   if($ex_unce >= $max_ex_unce){
                                       $max_ex_unce = $ex_unce;
                                   }
                               }

                               if(!empty($resolution_of_master)){
                                   $exp_val = explode('.', $resolution_of_master);
                                   if(isset($exp_val[1])){
                                       $decimal_master = strlen($exp_val[1]);  
                                   } else {
                                     $decimal_master = 0;  
                                   }
                               } else {
                                   $decimal_master = 4;
                               }
                               $decimal_least = 4;
                               $resolution_of_least = $grn_entry['leastCount'];
                               if(!empty($resolution_of_least)){
                                   $exp_least = explode('.', $resolution_of_least);
                                   if(isset($exp_least[1])){
                                       $decimal_least = strlen($exp_least[1]);  
                                   } else {
                                     $decimal_least = 0;  
                                   }
                               } else {
                                   $decimal_least = 4;
                               }
                            ?>
                            <td align="right"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                            <?php if(isset($grn_entry['row_span_test'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $grn_entry['uut_range'] ." ".$master_unit_type ?></div></td>
                            <?php } ?>
                            <td align="right"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                            <?php $units = $std_meter_val - $test_meter_val;  ?>
                            <td align="right"><?php echo number_format((float)$units, $decimal_master, '.', ''); ?></td>
                            <?php $per = 100*($units)/$test_meter_val;  ?>
                            <td align="right"><?php echo number_format((float)$per, $decimal_master, '.', ''); ?></td>
                        </tr>
                    <?php }
                    ?>
                        <tr>
                            <td><b>&nbsp;</b></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" colspan="3"><b>Max Expanded Uncert. in %</b></td>
                            <td align="right"><b><?php echo number_format((float)$max_ex_unce, $decimal_master, '.', ''); ?></b></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                <?php } ?>
                <!--Table Info 9 -->
                <?php  if($table_info_id == '10'){  ?>
                    <table width="100%" align="left" class="main_table"  style="font-size:13px;">
                    <?php
                    $is_3_phase = 0;
                    $table_title = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(!empty($grn_entry['table_title'])){
                            $table_title = $grn_entry['table_title'];
                            break;
                        }
                    }
                    $pre_range = '';
                    $pre_range_key = '';
                    $pre_range_total = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(empty($pre_range)){
                            $pre_range = $grn_entry['mRangeId'];
                            $pre_range_key = $k;
                            $pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$k]['row_span'] = 1;
                            }
                        } else {
                            if($pre_range == $grn_entry['mRangeId']){
                                $pre_range_total = $pre_range_total + 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                }
                            } else {
                                $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                $pre_range = $grn_entry['mRangeId'];
                                $pre_range_key = $k;
                                $pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$pre_range_key]['row_span'] = $pre_range_total;
                                }
                            }
                        }
                    }
    //                echo "<pre>"; print_r($grn_data); exit; 
                    $t_pre_range = '';
                    $t_pre_range_key = '';
                    $t_pre_range_total = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if($k == 0){

                            $t_pre_range = $grn_entry['uut_range'];
                            $t_pre_range_key = $k;
                            $t_pre_range_total = 1;
                            if(isset($grn_data[$k+1])){} else {
                                $grn_data[$k]['row_span_test'] = 1;
                            }
                        } else {
                            if($t_pre_range == $grn_entry['uut_range']){
                                $t_pre_range_total = $t_pre_range_total + 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                }
                            } else {
                                $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                $t_pre_range = $grn_entry['uut_range'];
                                $t_pre_range_key = $k;
                                $t_pre_range_total = 1;
                                if(isset($grn_data[$k+1])){} else {
                                    $grn_data[$t_pre_range_key]['row_span_test'] = $t_pre_range_total;
                                }
                            }
                        }
                    }
    //                echo "<pre>"; print_r($grn_data); exit; 
                    ?>
                    <?php 
                        $master_unit_type1 = '';
                        $master_range1 = '';
                        $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                WHERE grn.masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                        $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                        while($row1 = mysql_fetch_assoc($res1)) { 
                            $master_unit_type1 = $row1['range_unit'];
                            $master_range1 = $row1['rangeValue'];
                        }
                        $uut_range1 = $grn_data[0]['uut_range'];
                    ?>
                    <tr>
                        <th align="center" valign="middle">Title:</th>
                        <th  <?= !empty($is_3_phase) ? 'colspan="8"' : 'colspan="7"'; ?> align="left" valign="middle">&nbsp; <?php echo $table_title; ?></th>
                    </tr>
                    <tr>
                        <th valign="middle" align="center" rowspan="2">Sr. No.</th>
                        <th colspan="3" align="center" valign="middle">Calibration Standard</th>
                        <th align="center" valign="middle" <?= !empty($is_3_phase) ? 'colspan="3"' : 'colspan="2"'; ?>>Unit Under Test</th>
                        <th align="center" colspan="2" valign="middle">Error</th>
                    </tr>
                    <tr>
                        <th align="center" width="80px" valign="middle">Range</th>
                        <th align="center" valign="middle">Reading in <br/><?php echo $master_unit_type1; ?></th>
                        <th align="center" valign="middle">Voltage in <br/><?php echo $master_unit_type1; ?></th>
                        <th align="center" valign="middle" width="80px">Range</th>
                        <th align="center" valign="middle">Reading in <br/><?php echo $master_unit_type1; ?></th>
                        <th align="center" valign="middle">Unit(+/-)</th>
                        <th align="center" valign="middle">% of Rdg.</th>
                    </tr>
                    <?php 
                        $total_test_meter_val = 0;
                        $total_std_meter_val = 0;
                        $max_ex_unce = '';

                        foreach ($grn_data as $k => $grn_entry){


                        $i = $k + 1;

                        $master_hz = '';
                        $master_unit_type = '';
                        $master_range = '';
                        $qry1 = 'SELECT *  FROM mastermetersubsub as grn
                                WHERE grn.masterMeterSubSubId = '.$grn_entry['mRangeId'].' ';
                        $res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                        while($row1 = mysql_fetch_assoc($res1)) { 
                            $master_hz = $row1['frequency'];
                            $master_unit_type = $row1['range_unit'];
                            $master_range = $row1['rangeValue'];
                        }

                        ?>
                        <tr>
                            <td align="center" valign="middle"><?php echo $i; ?></td>
                            <?php if(isset($grn_entry['row_span'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span']) ? $grn_entry['row_span'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $master_range.' '.$master_unit_type.' @ '.$master_hz ?></div></td>
                            <?php } ?>
                            <?php 
                                $test_meter_val = $grn_entry['testMeterAverage'];
                                $std_meter_val = $grn_entry['stdMeterAverage'];
                                $total_test_meter_val = $total_test_meter_val + $test_meter_val;
                                $total_std_meter_val = $total_std_meter_val + $std_meter_val;
                                $decimal_master = 4;
                                $resolution_of_master = $grn_entry['resolution_of_master'];

                               $ex_unce = $grn_entry['expandedUncertinityInPre'];
                               if($max_ex_unce == ''){
                                   $max_ex_unce = $ex_unce;
                               } else {
                                   if($ex_unce >= $max_ex_unce){
                                       $max_ex_unce = $ex_unce;
                                   }
                               }

                               if(!empty($resolution_of_master)){
                                   $exp_val = explode('.', $resolution_of_master);
                                   if(isset($exp_val[1])){
                                       $decimal_master = strlen($exp_val[1]);  
                                   } else {
                                     $decimal_master = 0;  
                                   }
                               } else {
                                   $decimal_master = 4;
                               }
                               $decimal_least = 4;
                               $resolution_of_least = $grn_entry['leastCount'];
                               if(!empty($resolution_of_least)){
                                   $exp_least = explode('.', $resolution_of_least);
                                   if(isset($exp_least[1])){
                                       $decimal_least = strlen($exp_least[1]);  
                                   } else {
                                     $decimal_least = 0;  
                                   }
                               } else {
                                   $decimal_least = 4;
                               }
                            ?>
                            <td align="right"><?php echo number_format((float)$test_meter_val, $decimal_master, '.', ''); ?></td>
                            <?php if(isset($grn_entry['row_span_test'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $grn_entry['uut_range'] ." ".$master_unit_type ?></div></td>
                            <?php } ?>
                            <?php if(isset($grn_entry['row_span_test'])){ ?>
                            <td valign="middle" rowspan="<?= isset($grn_entry['row_span_test']) ? $grn_entry['row_span_test'] : ''; ?>"><div style="word-break: break-all; width: 80px;" ><?php echo $grn_entry['voltage']; ?></div></td>
                            <?php } ?>
                            <td align="right"><?php echo number_format((float)$std_meter_val, $decimal_least, '.', ''); ?></td>
                            <?php $units = $std_meter_val - $test_meter_val;  ?>
                            <td align="right"><?php echo number_format((float)$units, $decimal_master, '.', ''); ?></td>
                            <?php $per = 100*($units)/$test_meter_val;  ?>
                            <td align="right"><?php echo number_format((float)$per, $decimal_master, '.', ''); ?></td>
                        </tr>
                    <?php }
                    ?>
                        <tr>
                            <td><b>&nbsp;</b></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" colspan="3"><b>Max Expanded Uncert. in %</b></td>
                            <td align="right"><b><?php echo number_format((float)$max_ex_unce, $decimal_master, '.', ''); ?></b></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                <?php } ?>
                <!--Table Info 16 -->
                <?php  if($table_info_id == '2'){  ?>
                   <?php $table_title = '';
                    foreach ($grn_data as $k => $grn_entry){ 
                        if(!empty($grn_entry['table_title'])){
                            $table_title = $grn_entry['table_title'];
                            break;
                        }
                    }
    //                echo "<pre>"; print_r($reading_json); exit;
                    $decimal_master = 2;
                    $resolution_of_master = $grn_data[0]['resolution_of_master'];
                    if(!empty($resolution_of_master)){
                        $exp_val = explode('.', $resolution_of_master);
                        if(isset($exp_val[1])){
                            $decimal_master = strlen($exp_val[1]);  
                        } else {
                          $decimal_master = 0;  
                        }
                    } else {
                        $decimal_master = 2;
                    }
                    $decimal_least = 2;
                    $resolution_of_least = $grn_data[0]['leastCount'];
                    if(!empty($resolution_of_least)){
                        $exp_least = explode('.', $resolution_of_least);
                        if(isset($exp_least[1])){
                            $decimal_least = strlen($exp_least[1]);  
                        } else {
                          $decimal_least = 0;  
                        }
                    } else {
                        $decimal_least = 2;
                    }
                    $qry13 = 'SELECT range_unit,coverage_factor,cmc FROM mastermetersubsub
                    WHERE masterMeterSubSubId = '.$grn_data[0]['mRangeId'].' ';
                    $res13 = mysql_query($qry13) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
                    $m_range_name = '';
                    $m_coverage_factor = '';
                    $m_cmc = '';
                    while($row13 = mysql_fetch_assoc($res13)) {
                        $m_range_name = $row13['range_unit'];
                        $m_coverage_factor = $row13['coverage_factor'];
                        $m_cmc = $row13['cmc'];
                    }
                    $max_key1 = max(array_keys($testmeter1_data));
                    $max_key2 = !empty($testmeter2_data) ? max(array_keys($testmeter2_data)) : '0';
                    $max_key3 = !empty($testmeter3_data) ? max(array_keys($testmeter3_data)) : '0';
                    $max_key4 = max(array_keys($stdmeter_data));
                    $max_key = 0; 
                    if(($max_key1 >= $max_key2) && ($max_key1 >= $max_key3) && ($max_key1 >= $max_key4)){
                        $max_key = $max_key1;
                    } else if(($max_key2 >= $max_key3) && ($max_key2 >= $max_key1) && ($max_key2 >= $max_key4)){
                        $max_key = $max_key2;
                    } else if(($max_key3 >= $max_key1) && ($max_key3 >= $max_key2) && ($max_key3 >= $max_key4)){
                        $max_key = $max_key3;
                    } else if(($max_key4 >= $max_key1) && ($max_key4 >= $max_key2) && ($max_key4 >= $max_key3)){
                        $max_key = $max_key4;
                    }
                ?>
                <table width="100%" align="left" class="main_table"  style="font-size:14px;">
                    <tr>
                        <th align="center" valign="middle">Title:</th>
                        <th  colspan="7" align="left" valign="middle">&nbsp; <?php echo $table_title; ?></th>
                    </tr>
                    <tr>
                        <th valign="middle" align="center" rowspan="3">Step</th>
                        <th colspan="1" align="center" valign="middle">Master Reading</th>
                        <th align="center" valign="middle" colspan="3">UUC Reading</th>
                        <th align="center" colspan="1" valign="middle">Average</th>
                        <th align="center" colspan="1" valign="middle">Measurment</th>
                    </tr>
                    <tr>
                        <th valign="middle" align="center">Pind</th>
                        <th valign="middle" align="center">&nbsp;</th>
                        <th valign="middle" align="center">&nbsp;</th>
                        <th valign="middle" align="center">&nbsp;</th>
                        <th valign="middle" align="center"> Miw</th>
                        <th valign="middle" align="center">Deviation</th>
                    </tr>
                    <tr>
                        <th valign="middle" align="center"><?php echo $m_range_name; ?></th>
                        <th valign="middle" align="center">M1 <?php echo $m_range_name; ?></th>
                        <th valign="middle" align="center">M2 <?php echo $m_range_name; ?></th>
                        <th valign="middle" align="center">M3 <?php echo $m_range_name; ?></th>
                        <th valign="middle" align="center">((M1+M2)/2</th>
                        <th valign="middle" align="center">P % FS</th>
                    </tr>
                    <?php
                    $m1_total = 0;
                    $m2_total = 0;
                    $max_m2_m1_h = 0;
                    $max_m1_m3_h = 0;
                    $hysteresis_val = '';
                    $repetability_val = '';
                    $one_avg = '';
                    $max_avg = '';
                    for($x = 0; $x <= $max_key; $x++) { ?>
                        <tr>
                            <td align="center" valign="middle"><?php echo $x + 1; ?></td>
                            <?php $std_mter = (isset($stdmeter_data[$x]) ? number_format((float)$stdmeter_data[$x], $decimal_master, '.', '') : '0'); ?>
                            <td align="right" valign="middle"><?= (isset($stdmeter_data[$x]) ? number_format((float)$stdmeter_data[$x], $decimal_master, '.', '') : ''); ?></td>
                            <?php $m1 = (isset($testmeter1_data[$x]) ? number_format((float)$testmeter1_data[$x], $decimal_least, '.', '') : '0'); ?>
                            <td align="right" valign="middle"><?= (isset($testmeter1_data[$x]) ? number_format((float)$testmeter1_data[$x], $decimal_least, '.', '') : '0'); ?></td>
                            <?php $m2 = (isset($testmeter2_data[$x]) ? number_format((float)$testmeter2_data[$x], $decimal_least, '.', '') : '0'); ?>
                            <td align="right" valign="middle"><?= (isset($testmeter2_data[$x]) ? number_format((float)$testmeter2_data[$x], $decimal_least, '.', '') : ''); ?></td>
                            <?php $m3 = (isset($testmeter3_data[$x]) ? number_format((float)$testmeter3_data[$x], $decimal_least, '.', '') : '0'); ?>
                            <td align="right" valign="middle"><?= (isset($testmeter3_data[$x]) ? number_format((float)$testmeter3_data[$x], $decimal_least, '.', '') : ''); ?></td>
                            <?php $avg = (($m1 + $m2)/2); ?>
                            <?php $dev = (($std_mter - $avg)/$uuc_range)*100; ?>
                            <td align="right" valign="middle"><?= number_format((float)$avg, $decimal_master, '.', ''); ?></td>
                            <td align="right" valign="middle"><?= number_format((float)$dev, $decimal_master, '.', ''); ?></td>
                        </tr>
                        <?php
                        if($x == 1){
                            $one_avg = $avg;
                            $max_avg = $avg;
                        }
                        $m1_total = $m1_total + $m1;
                        $m2_total = $m2_total + $m2;
                        $new_max_m2_m1_h = $m2 - $m1;
                        if($new_max_m2_m1_h > $max_m2_m1_h){
                            $max_m2_m1_h = $new_max_m2_m1_h;
                        }
                        $new_max_m1_m3_h = $m1 - $m3;
                        if($new_max_m1_m3_h > $max_m1_m3_h){
                            $max_m1_m3_h = $new_max_m1_m3_h;
                        }
                        if($avg > $max_avg){
                            $max_avg = $avg;
                        }
                        $max_m2_m1 = abs($m2 - $m1);
                        if($hysteresis_val == ''){
                            $hysteresis_val = $max_m2_m1;
                        } else {
                            if($max_m2_m1 > $hysteresis_val){
                                $hysteresis_val = $max_m2_m1;
                            }
                        }
                        $max_m1_m3 = $m1 - $m3;
                        if($repetability_val == ''){
                            $repetability_val = $max_m1_m3;
                        } else {
                            if($max_m1_m3 > $repetability_val){
                                $repetability_val = $max_m1_m3;
                            }
                        }
                    } 
                    // calculation for Uncertainty (max) : Start //
                    $U5 = $grn_data[0]['uncertinty_from_tracebility'] / 2;
                    $U7 = $grn_data[0]['accuracy_taking_account'] / sqrt(3);
                    $U9 = ($new_max_m2_m1_h / 2) / sqrt(3);
                    $U11 = ($max_m1_m3_h / 2) / sqrt(3);
                    $U13 = ($one_avg / 2) / sqrt(3);
                    $U15 = ($decimal_least / 2) / sqrt(3);
                    $U17 = sqrt(pow($U5,2)+pow($U7,2)+pow($U9,2)+pow($U11,2)+pow($U13,2)+pow($U15,2)); // =SQRT(U5^2+U7^2+U9^2+U11^2+U13^2+U15^2)
                    $U18 = $U17 * $m_coverage_factor; // =U17*M22
                    $U19 = $U18 * 100 / $max_avg; // =U18*100/MAX(G29:G38)
    //                if(strtolower($m_range_name) == 'bar'){
                        if($U18 > $m_cmc){
                            $uncertainty_max = $U18;
                        } else {
                            $uncertainty_max = $m_cmc;
                        }
                    // calculation for Uncertainty (max) : End //
                    ?>
                    <tr>
                        <td colspan="3" align="center" valign="middle"><b>Hysteresis (max)</b></td>
                        <td colspan="1" align="right" valign="middle"><b><?php echo number_format((float)$hysteresis_val, $decimal_master, '.', ''); ?> <?php echo $m_range_name; ?></b></td>
                        <td colspan="2" align="center" valign="middle">&nbsp;</td>
                        <td colspan="1" align="right" valign="middle">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" valign="middle"><b>Repetability</b></td>
                        <td colspan="1" align="right" valign="middle"><b><?php echo number_format((float)$repetability_val, $decimal_master, '.', ''); ?></b></td>
                        <td colspan="2" align="center" valign="middle"><b>Exp. Uncertainty(max)</b></td>
                        <td colspan="1" align="right" valign="middle"><b><?php echo number_format((float)$uncertainty_max, $decimal_master, '.', ''); ?> <?php echo $m_range_name; ?></b></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" valign="middle">Remark</td>
                        <td colspan="5" align="left" valign="middle"><?php echo $instrumentDetails['certi_remarks']; ?></td>
                    </tr>
                </table>
                <div style="page-break-after: always;"></div>

                <page_footer>
                    <table width="605">
                        <tr valign="top">
                            <td width="5">&nbsp;</td>
                            <td width="600">&nbsp;</td>
                        </tr>
                            <tr valign="top">
                                <hr />
                                <td width="5" style="height: 5px;"><strong></strong></td>
                                <td  style="height: 5px;" width="600" style="font-size: 20px;"></td>
                        </tr>
                            <tr valign="bottom">
                                <td width="5"><strong>&nbsp;</strong></td>
                                <td width="600" style="font-size: 20px;"><img src="./images/for-krishna.png" /></td>
                        </tr>
                            <tr valign="top">
                                <td colspan="2">&nbsp;</td>
                        </tr>
                            <tr valign="top">
                                <td colspan="2">&nbsp;</td>
                        </tr>
                            <tr valign="top">
                                <td colspan="2">&nbsp;</td>
                        </tr>
                            <tr valign="top">
                                <td colspan="2">CALIBRATED BY: (<?php echo $instrumentDetails['userName']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; APPROVED BY: (<?php echo $instrumentDetails['approved_by'].', '.$instrumentDetails['designation']; ?>) &nbsp; &nbsp; &nbsp; || &nbsp; &nbsp; &nbsp; SEAL :</td>
                        </tr>
                            <tr valign="top">
                            <td width="5"><strong>Note&nbsp;:</strong></td>
                                    <td>&nbsp;</td>
                        </tr>
                            <tr valign="top">
                                <td colspan="2" style="font-size: 12px;"><?php echo $s_all_note; ?></td>
                        </tr>
                    </table>

                </page_footer>
                <?php } ?>
            <?php } ?>
	<?php } ?>
<!--</div>-->
</page>
<?php
//exit;
// html - end //
$content = ob_get_clean();

// convert in PDF
require_once 'html2pdf/html2pdf.class.php';
try {
//	$html2pdf = new HTML2PDF('P', 'A4', 'fr');
    $html2pdf = new HTML2PDF('P', 'A4', 'fr',true,'UTF-8',array(30, 43, 10, 14));
	// $html2pdf->setModeDebug();
    $html2pdf->shrink_tables_to_fit = 1;    // Default: false
    # $html2pdf->defaultPagebreakType = 'page-break-before';
    $html2pdf->setDefaultFont('Times');
    //$html2pdf->SetAutoPageBreak(TRUE, 10);
    $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
    $html2pdf->Output('example.pdf');
} catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>