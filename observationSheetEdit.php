<?php
include ("include/omConfig.php");
$grnPrefixNo = "";
$grnId = "";
$meterEntryNameArray = "";
$meterEntryNameArray = "";
$grnDetailId = "";
$stdMeter1 = "";
$testMeter1 = "";
$stdMeter2 = "";
$testMeter2 = "";
$stdMeter3 = "";
$testMeter3 = "";
$stdMeter4 = "";
$testMeter4 = "";
$stdMeter5 = "";
$testMeter5 = "";
$stdMeterAverage = "";
$testMeterAverage = "";
$accuracyTaken = "";
$standardDeviation = "";
$standardUncertinity = "";
$standardUncertinityperc = "";
$degreeOfFreedom = "";
$uncertinityForTypeB = "";
$uncertinityInPercentage = "";
$accuracyForTypeB = "";
$acuuracyForTypeBPerc = "";
$resolutionTypeB = "";
$resolutionForTypeBPerc = "";
$stabilityForTypeB = "";
$stabilityForTypeBInPerc = "";
$combinedUncertinity = "";
$combinedUncertinityInPerc = "";
$effectiveUncertinity = "";
$effectiveUncertinityInPer = "";
$effectiveDegreeOfFreed = "";
$meanReading = "";
$masterMeterReading = "";
$error = "";
$expandedUncertinity = "";
$humidity = "";
$masterMeterSubSubId = "";
$callibrationDate = "";
$meterEntryIdArray = array();
$nextYearDate = "";
$certificateIssue = "";
$certificateIssueDate = "";
$grnObsMasterId = 0;
if (isset($_GET['grnDetailPassId']) && $_GET['grnDetailPassId'] > 0) $grnDetailPassId = $_POST['grnDetailPassId'];
//else if(isset($_POST['grnDetailId']))
//  $grnDetailPassId = $_POST['grnDetailId'];
else $grnDetailPassId = 0;

if (!isset($_SESSION['s_activId']))
{
    $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
    header("Location:checkLogin.php");

} else {
    if((have_access_role(OBS_MODULE_ID,"edit"))){
    if (isset($_REQUEST['insertBtn']))
    {
        if (isset($_POST['grnObsMasterIdHidden']) && $_POST['grnObsMasterIdHidden'] > 0)
        {
            $callibrationDate = ($_POST['callibrationDateYear'] . "-" . $_POST['callibrationDateMonth'] . "-" . $_POST['callibrationDateDay']);
            $nextYearDate = ($_POST['nextYearDateYear'] . "-" . $_POST['nextYearDateMonth'] . "-" . $_POST['nextYearDateDay']);
            $certificateIssue = isset($_POST['certificateIssue']) ? "Y" : "N";
            $certificateIssueDate = ($_POST['certificateIssueDateYear'] . "-" . $_POST['certificateIssueDateMonth'] . "-" . $_POST['certificateIssueDateDay']);

            // Insert into observation master : Starts
            $updateMaster = "UPDATE grnobsmaster
  	                      SET masterMeterId  = " . $_POST['masterMeterId'] . ",
  	                          mParameterId   = " . $_POST['masterMeterSubId'] . ",
  	                          mRangeId       = '" . $_POST['masterMeterSubSubId'] . "',
  	                          grnObsDate     = '" . $callibrationDate . "',
  	                          selfCertiNo    = '" . $_POST['selfCertiNo'] . "'
  	                    WHERE grnObsMasterId = " . $_POST['grnObsMasterIdHidden'] . "";
            $updateMasterRes = mysql_query($updateMaster);
            if (!$updateMasterRes) die("Update Master Query Not Inserted 1: " . mysql_error() . " : " . $updateMaster);
            else

            // Update GRN Observation Detail : Starts
            // $obsDate        = $_POST['obsDateYear']."-".$_POST['obsDateMonth']."-".$_POST['obsDateDay'];
            if (isset($_POST['extraFields']) && $_POST['extraFields'] == 1)
            {
                $extraFields = $_POST['extraFields'];
                $extraField1 = isset($_POST['extraField1']) && $_POST['extraField1'] != "" ? $_POST['extraField1'] : NULL;
                $extraField2 = isset($_POST['extraField2']) && $_POST['extraField2'] != "" ? $_POST['extraField2'] : NULL;
                $extraField3 = isset($_POST['extraField3']) && $_POST['extraField3'] != "" ? $_POST['extraField3'] : NULL;
                $extraField4 = isset($_POST['extraField4']) && $_POST['extraField4'] != "" ? $_POST['extraField4'] : NULL;
                $extraField5 = isset($_POST['extraField5']) && $_POST['extraField5'] != "" ? $_POST['extraField5'] : NULL;
                $extraField6 = isset($_POST['extraField6']) && $_POST['extraField6'] != "" ? $_POST['extraField6'] : NULL;

                $extraQuery = "  , extraFields = " . $extraFields . ", extraField1 = '" . $extraField1 . "', extraField2 = '" . $extraField2 . "', extraField3 = '" . $extraField3 . "', extraField4 = '" . $extraField4 . "', extraField5 = '" . $extraField5 . "', extraField6 = '" . $extraField6 . "' ";
            }
            else
            {
                $extraFields = isset($_POST['extraFields']) && $_POST['extraFields'] != "" ? 0 : 0;
                $extraField1 = isset($_POST['extraField1']) && $_POST['extraField1'] != "" ? NULL : NULL;
                $extraField2 = isset($_POST['extraField2']) && $_POST['extraField2'] != "" ? NULL : NULL;
                $extraField3 = isset($_POST['extraField3']) && $_POST['extraField3'] != "" ? NULL : NULL;
                $extraField4 = isset($_POST['extraField4']) && $_POST['extraField4'] != "" ? NULL : NULL;
                $extraField5 = isset($_POST['extraField5']) && $_POST['extraField5'] != "" ? NULL : NULL;
                $extraField6 = isset($_POST['extraField6']) && $_POST['extraField6'] != "" ? NULL : NULL;

                $extraQuery = "  , extraFields = " . $extraFields . ", extraField1 = '" . $extraField1 . "', extraField2 = '" . $extraField2 . "', extraField3 = '" . $extraField3 . "', extraField4 = '" . $extraField4 . "', extraField5 = '" . $extraField5 . "', extraField6 = '" . $extraField6 . "' ";
            }

            $ratio1 = ($_POST['ratio1'] == "") ? 1 : $_POST['ratio1'];
            $ratio2 = ($_POST['ratio2'] == "") ? 1 : $_POST['ratio2'];

            $updateGrnEntry = "UPDATE grndetail
	                          SET refGrnDetailId = '" . $grnDetailPassId . "',
	                              masterMeterId = '" . $_POST['masterMeterId'] . "',
	                              callibrationDate ='" . $callibrationDate . "',
	                              nextYearDate = '" . $nextYearDate . "',
								  certificateIssue = '" . $certificateIssue . "',
								  certificateIssueDate = '" . $certificateIssueDate . "',
	                              makeModel = '" . $_POST['makeModel'] . "',
	                              instrumentId = '" . $_POST['instrumentId'] . "',
	                              leastCount = '" . $_POST['leastCount'] . "',
	                              accuracy = '" . $_POST['accuracy'] . "',
	                              temperature = '" . $_POST['temperature'] . "',
	                              humidity = '" . $_POST['humidity'] . "',
	                              masterParameterId ='" . $_POST['masterMeterSubId'] . "',
	                              masterRangeId = '" . $_POST['masterMeterSubSubId'] . "'
	                              " . $extraQuery . ",
	                              certiRemarks = '" . $_POST['certiRemarks'] . "',
	                              ratio1 = " . $ratio1 . ",
	                              ratio2 = " . $ratio2 . "
	                        WHERE grnDetailId = " . $_POST['grnDetailId'];
            $updateGrnEntryResult = mysql_query($updateGrnEntry);
            if (!$updateGrnEntryResult) die("Insert Query Not Inserted 1: " . mysql_error() . " : " . $updateGrnEntry);
            // Update GRN Observation Detail : Starts
            
        }

        if (isset($_REQUEST['grnObservationId']) && $_REQUEST['grnObservationId'] > 0)
        {
            /////// Insert Query for GRN Observation Detail : Statrs
            $loopCount = 0;
            while ($loopCount < count($_POST['stdMeter']))
            {
                $uncertaintyCalibration = ($_POST['uncertaintyCalibration'][$loopCount] != '') ? $_POST['uncertaintyCalibration'][$loopCount] : 0;
                $uncertaintyCalibrationInPer = ($_POST['uncertaintyCalibrationInPer'][$loopCount] != '') ? $_POST['uncertaintyCalibrationInPer'][$loopCount] : 0;
                $resolutionTypeA = ($_POST['resolutionTypeA'][$loopCount] != '') ? $_POST['resolutionTypeA'][$loopCount] : 0;

                $stdMeter1 = ($_POST['stdMeter1'][$loopCount] != '') ? $_POST['stdMeter1'][$loopCount] : 0;
                $testMeter1 = ($_POST['testMeter1'][$loopCount] != '') ? $_POST['testMeter1'][$loopCount] : 0;
                $stdMeter2 = ($_POST['stdMeter2'][$loopCount] != '') ? $_POST['stdMeter2'][$loopCount] : 0;
                $testMeter2 = ($_POST['testMeter2'][$loopCount] != '') ? $_POST['testMeter2'][$loopCount] : 0;
                $stdMeter3 = ($_POST['stdMeter3'][$loopCount] != '') ? $_POST['stdMeter3'][$loopCount] : 0;
                $testMeter3 = ($_POST['testMeter3'][$loopCount] != '') ? $_POST['testMeter3'][$loopCount] : 0;
                $stdMeter4 = ($_POST['stdMeter4'][$loopCount] != '') ? $_POST['stdMeter4'][$loopCount] : 0;
                $testMeter4 = ($_POST['testMeter4'][$loopCount] != '') ? $_POST['testMeter4'][$loopCount] : 0;
                $stdMeter5 = ($_POST['stdMeter5'][$loopCount] != '') ? $_POST['stdMeter5'][$loopCount] : 0;
                $testMeter5 = ($_POST['testMeter5'][$loopCount] != '') ? $_POST['testMeter5'][$loopCount] : 0;

                $stdMeter1 = 0;
	            $stdMeter2 = 0;
	            $stdMeter3 = 0;
	            $stdMeter4 = 0;
	            $stdMeter5 = 0;

                $stdMeterAverage = ($_POST['stdMeterAverage'][$loopCount] != '') ? $_POST['stdMeterAverage'][$loopCount] : 0;
                $testMeterAverage = ($_POST['testMeterAverage'][$loopCount] != '') ? $_POST['testMeterAverage'][$loopCount] : 0;
                $accuracyTaken = ($_POST['accuracyTaken'][$loopCount] != '') ? $_POST['accuracyTaken'][$loopCount] : 0;
                $standardDeviation = ($_POST['standardDeviation'][$loopCount] != '') ? $_POST['standardDeviation'][$loopCount] : 0;
                $standardUncertinity = ($_POST['standardUncertinity'][$loopCount] != '') ? $_POST['standardUncertinity'][$loopCount] : 0;
                $standardUncertinityperc = ($_POST['standardUncertinityperc'][$loopCount] != '') ? $_POST['standardUncertinityperc'][$loopCount] : 0;
                $degreeOfFreedom = ($_POST['degreeOfFreedom'][$loopCount] != '') ? $_POST['degreeOfFreedom'][$loopCount] : 0;
                $uncertinityForTypeB = ($_POST['uncertinityForTypeB'][$loopCount] != '') ? $_POST['uncertinityForTypeB'][$loopCount] : 0;
                $uncertinityInPercentage = ($_POST['uncertinityInPercentage'][$loopCount] != '') ? $_POST['uncertinityInPercentage'][$loopCount] : 0;
                $accuracyForTypeB = ($_POST['accuracyForTypeB'][$loopCount] != '') ? $_POST['accuracyForTypeB'][$loopCount] : 0;
                $acuuracyForTypeBPerc = ($_POST['acuuracyForTypeBPerc'][$loopCount] != '') ? $_POST['acuuracyForTypeBPerc'][$loopCount] : 0;
                $resolutionTypeB = ($_POST['resolutionTypeB'][$loopCount] != '') ? $_POST['resolutionTypeB'][$loopCount] : 0;
                $resolutionForTypeBPerc = ($_POST['resolutionForTypeBPerc'][$loopCount] != '') ? $_POST['resolutionForTypeBPerc'][$loopCount] : 0;
                $stabilityForTypeB = ($_POST['stabilityForTypeB'][$loopCount] != '') ? $_POST['stabilityForTypeB'][$loopCount] : 0;
                $stabilityForTypeBInPerc = ($_POST['stabilityForTypeBInPerc'][$loopCount] != '') ? $_POST['stabilityForTypeBInPerc'][$loopCount] : 0;
                $combinedUncertinity = ($_POST['combinedUncertinity'][$loopCount] != '') ? $_POST['combinedUncertinity'][$loopCount] : 0;
                $combinedUncertinityInPerc = ($_POST['combinedUncertinityInPerc'][$loopCount] != '') ? $_POST['combinedUncertinityInPerc'][$loopCount] : 0;
                $effectiveUncertinity = ($_POST['effectiveUncertinity'][$loopCount] != '') ? $_POST['effectiveUncertinity'][$loopCount] : 0;
                $effectiveUncertinityInPer = ($_POST['effectiveUncertinityInPer'][$loopCount] != '') ? $_POST['effectiveUncertinityInPer'][$loopCount] : 0;
                $effectiveDegreeOfFreed = ($_POST['effectiveDegreeOfFreed'][$loopCount] != '') ? $_POST['effectiveDegreeOfFreed'][$loopCount] : 0;
                $meanReading = ($_POST['meanReading'][$loopCount] != '') ? $_POST['meanReading'][$loopCount] : 0;
                $masterMeterReading = ($_POST['masterMeterReading'][$loopCount] != '') ? $_POST['masterMeterReading'][$loopCount] : 0;
                $error = ($_POST['error'][$loopCount] != '') ? $_POST['error'][$loopCount] : 0;
                $expandedUncertinity = ($_POST['expandedUncertinity'][$loopCount] != '') ? $_POST['expandedUncertinity'][$loopCount] : 0;
                $expandedUncertinityInPre = ($_POST['expandedUncertinityInPre'][$loopCount] != '') ? $_POST['expandedUncertinityInPre'][$loopCount] : 0;
                $humidity = ($_POST['humidity'] != '') ? $_POST['humidity'] : 0;
                $measurementUncertaintyIs = isset($_POST['measurementUncertaintyIs']) ? $_POST['measurementUncertaintyIs'] : "";

                ////////////////////
                $testMeter1Len = strlen($testMeter1);
                $decimalPointLen = 1; //decimalPoint has strlen is 1
                $testMeter1decimalPos = (strpos($testMeter1, ".") > 0) ? strpos($testMeter1, ".") : 0;
                if ($testMeter1decimalPos > 0)
                {
                    $testMeter1BeforeDecimalLen = strlen(substr($testMeter1, 0, $testMeter1decimalPos));
                    $stdMeter1AfterDecimal = ($testMeter1Len - $decimalPointLen - $testMeter1BeforeDecimalLen);
                }
                else
                {
                    $testMeter1BeforeDecimalLen = strlen($testMeter1);
                    $stdMeter1AfterDecimal = ($testMeter1Len - $testMeter1BeforeDecimalLen);
                }
                ////////////////////
                ////////////////////
                $resolutionTypeALen = strlen($resolutionTypeA);
                $decimalPointLen = 1; //decimalPoint has strlen is 1
                $resolutionTypeABeforeDecimalLen = strlen(substr($resolutionTypeA, 0, strpos($resolutionTypeA, ".")));
                //if 0.0001 ==>> $testMeter1Len = 1, $testMeter1BeforeDecimalLen = 2
                $resolutionTypeAAfterDecimal = ($resolutionTypeALen - $decimalPointLen - $resolutionTypeABeforeDecimalLen);
                ////////////////////
                if (count($_POST['stdMeter'][$loopCount]) > 0 && ($_POST['testMeter1'][$loopCount] != "" || $_POST['testMeter2'][$loopCount] != "" || $_POST['testMeter3'][$loopCount] != "" || $_POST['testMeter4'][$loopCount] != "" || $_POST['testMeter5'][$loopCount] != "" || $_POST['stdMeterAverage'][$loopCount] != "" || $_POST['testMeterAverage'][$loopCount] != ""))
                {
                    $updateQueryGrnDetail = "UPDATE grnobservation
	                                    SET uncertaintyCalibration      = " . $uncertaintyCalibration . ",
	                                        uncertaintyCalibrationInPer = " . $uncertaintyCalibrationInPer . ",
	                                        resolutionTypeA             = " . $resolutionTypeA . ",
	                                        stdMeter1AfterDecimal       = " . $stdMeter1AfterDecimal . ",
	                                        resolutionTypeAAfterDecimal = " . $resolutionTypeAAfterDecimal . ",
	                                        stdMeter1                   = " . $stdMeter1 . ",
	                                        testMeter1                  = '" . $testMeter1 . "',
	                                        stdMeter2                   = '" . $stdMeter2 . "',
	                                        testMeter2                  = '" . $testMeter2 . "',
	                                        stdMeter3                   = '" . $stdMeter3 . "',
	                                        testMeter3                  = '" . $testMeter3 . "',
	                                        stdMeter4                   = '" . $stdMeter4 . "',
	                                        testMeter4                  = " . $testMeter4 . ",
	                                        stdMeter5                   = " . $stdMeter5 . ",
	                                        testMeter5                  = '" . $testMeter5 . "',
	                                        stdMeterAverage             = '" . $stdMeterAverage . "',
	                                        testMeterAverage            = '" . $testMeterAverage . "',
	                                        accuracyTaken               = '" . $accuracyTaken . "',
	                                        standardDeviation           = '" . $standardDeviation . "',
	                                        standardUncertinity         = '" . $standardUncertinity . "',
	                                        standardUncertinityperc     = '" . $standardUncertinityperc . "',
	                                        degreeOfFreedom             = '" . $degreeOfFreedom . "',
	                                        uncertinityForTypeB         = '" . $uncertinityForTypeB . "',
	                                        uncertinityInPercentage     = '" . $uncertinityInPercentage . "',
	                                        accuracyForTypeB            = '" . $accuracyForTypeB . "',
	                                        acuuracyForTypeBPerc        = '" . $acuuracyForTypeBPerc . "',
	                                        resolutionTypeB             = '" . $resolutionTypeB . "',
	                                        resolutionForTypeBPerc      = '" . $resolutionForTypeBPerc . "',
	                                        stabilityForTypeB           = '" . $stabilityForTypeB . "',
	                                        stabilityForTypeBInPerc     = '" . $stabilityForTypeBInPerc . "',
	                                        combinedUncertinity         = '" . $combinedUncertinity . "',
	                                        combinedUncertinityInPerc   = '" . $combinedUncertinityInPerc . "',
	                                        effectiveUncertinity        = '" . $effectiveUncertinity . "',
	                                        effectiveUncertinityInPer   = '" . $effectiveUncertinityInPer . "',
	                                        effectiveDegreeOfFreed      = '" . $effectiveDegreeOfFreed . "',
	                                        meanReading                 = '" . $meanReading . "',
	                                        masterMeterReading          = '" . $masterMeterReading . "',
	                                        error                       = '" . $error . "',
	                                        expandedUncertinity         = '" . $expandedUncertinity . "',
	                                        expandedUncertinityInPre    = '" . $expandedUncertinityInPre . "'
	                                  WHERE grnObservationId            = " . $_REQUEST['grnObservationId'] . "";

                    $updateQueryGrnDetailResult = mysql_query($updateQueryGrnDetail) or die($updateQueryGrnDetail." <br/> SQL ERROR : ".mysql_error());
                    $grnDetailId = $_REQUEST['grnObservationId'];

                    $stdmeterDeleteQeury = "DELETE FROM grnobservation_stdmeter WHERE grnObservationId=".$grnDetailId;
                    mysql_query($stdmeterDeleteQeury);
                    foreach ($_POST['stdMeter'][$loopCount] as $stdMeter) {
	                	if($stdMeter != "") {
	                		$datetime = date("Y-m-d H:i:s");
		                	$login_user_id = $_SESSION['user_id'];
		                	$stdmeterInsertQeury = "INSERT INTO `grnobservation_stdmeter` (`grnObservationId`, `stdMeter`, `created_by`, `created_at`,`updated_by`, `updated_at`) VALUES ('".$grnDetailId."','".$stdMeter."','".$login_user_id."','".$datetime."','".$login_user_id."','".$datetime."')";	
		                	mysql_query($stdmeterInsertQeury);
	                	}
	                	
	                }
                }
                $loopCount++;
            }
        }
        header("Location:observationSheet.php");
        ////// Insert Query for GRN Observation Detail : Ends
    }

    $selectGrn = "SELECT grnPrefix,grnNo,grnId,grnDate
                  FROM grnmaster
                  WHERE grnPrefix = 'N'
                 ORDER BY grnDate DESC, grnId DESC
                 	";
    $selectGrnResult = mysql_query($selectGrn);
    $i = 0;
    while ($selectGrnRow = mysql_fetch_array($selectGrnResult))
    {
        $grnId[$i] = $selectGrnRow['grnId'];
        $grnPrefixNo[$i] = $selectGrnRow['grnNo'];
        $i++;
    }

    $selectGrn = "SELECT masterMeterId,masterMeterName,procedureText
                  FROM mastermeter
                 ORDER BY masterMeterName";
    $selectGrnResult = mysql_query($selectGrn);
    $j = 0;
    while ($selectGrnRow = mysql_fetch_array($selectGrnResult))
    {
        $meterEntryIdArray[$j] = $selectGrnRow['masterMeterId'];
        $meterEntryNameArray[$j] = $selectGrnRow['masterMeterName'] . " : " . $selectGrnRow['procedureText'];
        $j++;
    }
    ////
    $nextYearDate = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d") , date("Y") + 1));
    $certificateIssue = "N";
    $certificateIssueDate = date("Y-m-d");
    ////
    ////////////////////////  Edit Selection Master Start  //////////////////////////////////////////////////////////////////
    $selectedMMeter = "SELECT grnObsMasterId,masterMeterId,mParameterId,mRangeId,selfCertiNo,grnDetailId
                       FROM grnobsmaster
                      WHERE grnObsMasterId = '" . $_REQUEST['grnObsMasterId'] . "'";
    $selectedMMeterResult = mysql_query($selectedMMeter);
    if ($selectMRow = mysql_fetch_array($selectedMMeterResult))
    {
        $grnObsMasterIdHidden = $selectMRow['grnObsMasterId'];
        $selectedMasterMeter = $selectMRow['masterMeterId'];
        $selectedmParameterId = $selectMRow['mParameterId'];
        $selectedmRangeId = $selectMRow['mRangeId'];
        $selfCertiNo = $selectMRow['selfCertiNo'];
        $grnDetailId = $selectMRow['grnDetailId'];
    }

    $grnDtlArray = array();
    $selectedMasterDetail = "SELECT callibrationDate,nextYearDate,certificateIssue,certificateIssueDate,grnId,makeModel,instrumentId,leastCount,accuracy,temperature,
                                  humidity,extraFields,extraField1,extraField2,extraField3,extraField4,extraField5,extraField6,
                                  certiRemarks,ratio1,ratio2
                             FROM grndetail
                            WHERE grnDetailId = " . $grnDetailId . "";
    $selectedMasterDetailResult = mysql_query($selectedMasterDetail);
    if ($selectGrnDtlRow = mysql_fetch_array($selectedMasterDetailResult))
    {
        $callibrationDate = $selectGrnDtlRow['callibrationDate'];
        $nextYearDate = $selectGrnDtlRow['nextYearDate'];
        $certificateIssue = $selectGrnDtlRow['certificateIssue'];
        $certificateIssueDate = $selectGrnDtlRow['certificateIssueDate'];
        $grnIdSelected = $selectGrnDtlRow['grnId'];
        $grnDtlArray['makeModel'] = $selectGrnDtlRow['makeModel'];
        $grnDtlArray['instrumentId'] = $selectGrnDtlRow['instrumentId'];
        $grnDtlArray['leastCount'] = $selectGrnDtlRow['leastCount'];
        $grnDtlArray['accuracy'] = $selectGrnDtlRow['accuracy'];
        $grnDtlArray['temperature'] = $selectGrnDtlRow['temperature'];
        $grnDtlArray['humidity'] = $selectGrnDtlRow['humidity'];
        $grnDtlArray['extraFields'] = $selectGrnDtlRow['extraFields'];
        $grnDtlArray['extraField1'] = $selectGrnDtlRow['extraField1'];
        $grnDtlArray['extraField2'] = $selectGrnDtlRow['extraField2'];
        $grnDtlArray['extraField3'] = $selectGrnDtlRow['extraField3'];
        $grnDtlArray['extraField4'] = $selectGrnDtlRow['extraField4'];
        $grnDtlArray['extraField5'] = $selectGrnDtlRow['extraField5'];
        $grnDtlArray['extraField6'] = $selectGrnDtlRow['extraField6'];
        $grnDtlArray['certiRemarks'] = $selectGrnDtlRow['certiRemarks'];
        $grnDtlArray['ratio1'] = $selectGrnDtlRow['ratio1'];
        $grnDtlArray['ratio2'] = $selectGrnDtlRow['ratio2'];
    }

    ////////////////////////  Edit Selection Master End  //////////////////////////////////////////////////////////////////
    ////////////////////////  Edit Selection Start  //////////////////////////////////////////////////////////////////
    $editArray = array();
    $stdMeter = array();
    $selAllEdit = "SELECT grnObservationId,uncertaintyCalibration,uncertaintyCalibrationInPer,resolutionTypeA,stdMeter1AfterDecimal,stdMeter1,testMeter1,stdMeter2,testMeter2,
                        stdMeter3,testMeter3,stdMeter4,testMeter4,stdMeter5,testMeter5,stdMeterAverage,testMeterAverage,
                        accuracyTaken,standardDeviation,standardUncertinity,standardUncertinityperc,degreeOfFreedom,
                        uncertinityForTypeB,uncertinityInPercentage,accuracyForTypeB,acuuracyForTypeBPerc,resolutionTypeB,
                        resolutionForTypeBPerc,stabilityForTypeB,stabilityForTypeBInPerc,combinedUncertinity,
                        combinedUncertinityInPerc,effectiveUncertinity,effectiveUncertinityInPer,effectiveDegreeOfFreed,
                        meanReading,masterMeterReading,error,expandedUncertinity,expandedUncertinityInPre
                   FROM grnobservation
                  WHERE grnObservationId = " . $_REQUEST['grnObservationId'] . "";
    $selAllEditRes = mysql_query($selAllEdit);
    while ($editRow = mysql_fetch_array($selAllEditRes))
    {
        $editArray['grnObservationId'] = $editRow['grnObservationId'];
        $editArray['stdMeter1'] = $editRow['stdMeter1'];
        $editArray['stdMeter2'] = $editRow['stdMeter2'];
        $editArray['stdMeter3'] = $editRow['stdMeter3'];
        $editArray['stdMeter4'] = $editRow['stdMeter4'];
        $editArray['stdMeter5'] = $editRow['stdMeter5'];
        $editArray['stdMeterAverage'] = $editRow['stdMeterAverage'];
        $editArray['standardDeviation'] = $editRow['standardDeviation'];
        $editArray['standardUncertinity'] = $editRow['standardUncertinity'];
        $editArray['standardUncertinityperc'] = $editRow['standardUncertinityperc'];
        $editArray['degreeOfFreedom'] = $editRow['degreeOfFreedom'];
        $editArray['uncertaintyCalibration'] = $editRow['uncertaintyCalibration'];
        $editArray['uncertaintyCalibrationInPer'] = $editRow['uncertaintyCalibrationInPer'];
        $editArray['resolutionTypeA'] = $editRow['resolutionTypeA'];
        $editArray['accuracyTaken'] = $editRow['accuracyTaken'];
        $editArray['uncertinityForTypeB'] = $editRow['uncertinityForTypeB'];
        $editArray['uncertinityInPercentage'] = $editRow['uncertinityInPercentage'];
        $editArray['accuracyForTypeB'] = $editRow['accuracyForTypeB'];
        $editArray['acuuracyForTypeBPerc'] = $editRow['acuuracyForTypeBPerc'];
        $editArray['resolutionTypeB'] = $editRow['resolutionTypeB'];
        $editArray['resolutionForTypeBPerc'] = $editRow['resolutionForTypeBPerc'];
        $editArray['stabilityForTypeB'] = $editRow['stabilityForTypeB'];
        $editArray['stabilityForTypeBInPerc'] = $editRow['stabilityForTypeBInPerc'];
        $editArray['combinedUncertinity'] = $editRow['combinedUncertinity'];
        $editArray['combinedUncertinityInPerc'] = $editRow['combinedUncertinityInPerc'];
        $editArray['effectiveUncertinity'] = $editRow['effectiveUncertinity'];
        $editArray['effectiveUncertinityInPer'] = $editRow['effectiveUncertinityInPer'];
        $editArray['effectiveDegreeOfFreed'] = $editRow['effectiveDegreeOfFreed'];
        $editArray['meanReading'] = $editRow['meanReading'];
        $editArray['masterMeterReading'] = $editRow['masterMeterReading'];
        $editArray['error'] = $editRow['error'];
        $editArray['expandedUncertinity'] = $editRow['expandedUncertinity'];
        $editArray['expandedUncertinityInPre'] = $editRow['expandedUncertinityInPre'];
        $editArray['testMeter1'] = sprintf("%." . $editRow['stdMeter1AfterDecimal'] . "f", $editRow['testMeter1']);
        $editArray['testMeter2'] = $editRow['testMeter2'];
        $editArray['testMeter3'] = $editRow['testMeter3'];
        $editArray['testMeter4'] = $editRow['testMeter4'];
        $editArray['testMeter5'] = $editRow['testMeter5'];
        $editArray['testMeterAverage'] = $editRow['testMeterAverage'];

        $selectStdMeter = "SELECT stdMeter FROM grnobservation_stdmeter WHERE grnObservationId=".$editRow['grnObservationId'];
		$stdMeterResult = mysql_query($selectStdMeter);
		while($stdMeterRow = mysql_fetch_array($stdMeterResult))
		{
		  $stdMeter[] = $stdMeterRow['stdMeter'];
		}
		if(empty($stdMeter)) {
			$stdMeter[] = $editRow['stdMeter1'];
			$stdMeter[] = $editRow['stdMeter2'];
			$stdMeter[] = $editRow['stdMeter3'];
			$stdMeter[] = $editRow['stdMeter4'];
			$stdMeter[] = $editRow['stdMeter5'];
		}
    }
    /*echo "<pre>";
    print_r($stdMeter);
    die();*/
    ////////////////////////  Edit Selection End  //////////////////////////////////////////////////////////////////
    include ("./bottom.php");
    $smarty->assign("meterEntryIdArray", $meterEntryIdArray);
    $smarty->assign("meterEntryNameArray", $meterEntryNameArray);
    $smarty->assign("grnId", $grnId);
    $smarty->assign("grnDetailId", $grnDetailId);
    $smarty->assign("stdMeter", $stdMeter);
    $smarty->assign("stdMeter1", $stdMeter1);
    $smarty->assign("testMeter1", $testMeter1);
    $smarty->assign("stdMeter2", $stdMeter2);
    $smarty->assign("testMeter2", $testMeter2);
    $smarty->assign("stdMeter3", $stdMeter3);
    $smarty->assign("testMeter3", $testMeter3);
    $smarty->assign("stdMeter4", $stdMeter4);
    $smarty->assign("testMeter4", $testMeter4);
    $smarty->assign("stdMeter5", $stdMeter5);
    $smarty->assign("testMeter5", $testMeter5);
    $smarty->assign("stdMeterAverage", $stdMeterAverage);
    $smarty->assign("testMeterAverage", $testMeterAverage);
    $smarty->assign("accuracyTaken", $accuracyTaken);
    $smarty->assign("standardDeviation", $standardDeviation);
    $smarty->assign("standardUncertinity", $standardUncertinity);
    $smarty->assign("standardUncertinityperc", $standardUncertinityperc);
    $smarty->assign("degreeOfFreedom", $degreeOfFreedom);
    $smarty->assign("uncertinityForTypeB", $uncertinityForTypeB);
    $smarty->assign("uncertinityInPercentage", $uncertinityInPercentage);
    $smarty->assign("accuracyForTypeB", $accuracyForTypeB);
    $smarty->assign("acuuracyForTypeBPerc", $acuuracyForTypeBPerc);
    $smarty->assign("resolutionTypeB", $resolutionTypeB);
    $smarty->assign("resolutionForTypeBPerc", $resolutionForTypeBPerc);
    $smarty->assign("stabilityForTypeB", $stabilityForTypeB);
    $smarty->assign("stabilityForTypeBInPerc", $stabilityForTypeBInPerc);
    $smarty->assign("combinedUncertinity", $combinedUncertinity);
    $smarty->assign("combinedUncertinityInPerc", $combinedUncertinityInPerc);
    $smarty->assign("effectiveUncertinity", $effectiveUncertinity);
    $smarty->assign("effectiveUncertinityInPer", $effectiveUncertinityInPer);
    $smarty->assign("effectiveDegreeOfFreed", $effectiveDegreeOfFreed);
    $smarty->assign("meanReading", $meanReading);
    $smarty->assign("masterMeterReading", $masterMeterReading);
    $smarty->assign("error", $error);
    $smarty->assign("expandedUncertinity", $expandedUncertinity);
    $smarty->assign("grnPrefixNo", $grnPrefixNo);
    $smarty->assign("grnDetailPassId", $grnDetailPassId);
    $smarty->assign("callibrationDate", $callibrationDate);
    $smarty->assign("nextYearDate", $nextYearDate);
    $smarty->assign("certificateIssue", $certificateIssue);
    $smarty->assign("certificateIssueDate", $certificateIssueDate);
    $smarty->assign("humidity", $humidity);
    $smarty->assign("editArray", $editArray);
    $smarty->assign("selectedMasterMeter", $selectedMasterMeter);
    $smarty->assign("selectedmParameterId", $selectedmParameterId);
    $smarty->assign("selectedmRangeId", $selectedmRangeId);
    $smarty->assign("selfCertiNo", $selfCertiNo);
    $smarty->assign("grnIdSelected", $grnIdSelected);
    $smarty->assign("grnDtlArray", $grnDtlArray);
    $smarty->assign("grnObsMasterIdHidden", $grnObsMasterIdHidden);
    //$smarty->display("observationSheet.tpl");
    $smarty->display("observationSheetEdit.tpl");
} else {
  header("Location:index.php");
}  
}

?>