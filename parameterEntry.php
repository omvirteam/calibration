<?php
include("include/omConfig.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
if((have_access_role(MASTER_PARAMETER_MODULE_ID,"view"))){
  $msg                  = "";
  $parameterId          = (isset($_REQUEST['parameterId'])) ? $_REQUEST['parameterId'] : 0;
  $currentparameterName = "";
  $parameterArr         = array();

  //parameter Insert : Start
  if(isset($_POST['parameterName']))
  {
    if(isset($_POST['cancelBtn']))
    {
      header("Location: index.php"); 
      exit();
    }
    if($parameterId > 0)
    {
      $updateparameter = "UPDATE parameterentry
                             SET parameterName = '".$_POST['parameterName']."'
                           WHERE parameterId = ".$_POST['parameterId'];
      $updateparameterResult = mysql_query($updateparameter);
      $parameterId = 0; //We don't want user to remain in Edit part after Update query done.
    }
    else
    {
      $insertparameterName = "INSERT INTO parameterentry(parameterName)
                               VALUE('".$_POST['parameterName']."')";
      $insertparameterNameResult = mysql_query($insertparameterName);

      if(!$insertparameterNameResult)
        die("Insert Query Not Inserted : ".mysql_error()." : ".$insertparameterName);
      else
        header("Location:parameterEntry.php");
    }
  }
  //parameter Insert : End

  //parameter Listing : Start
  $parameterNameToDisplay = "SELECT parameterId,parameterName
                               FROM parameterentry
                              ORDER BY parameterName";
  $selectparameterResult = mysql_query($parameterNameToDisplay);
  $i = 0;
  while($parameterInRow = mysql_fetch_array($selectparameterResult))
  {
    $parameterArr[$i]['parameterId']   = $parameterInRow['parameterId'];
    $parameterArr[$i]['parameterName'] = $parameterInRow['parameterName'];

    if($parameterInRow['parameterId'] == $parameterId)
       $currentparameterName = $parameterInRow['parameterName'];
    $i++;
  }
  //parameter Listing : End
  
  include("./bottom.php");
  $smarty->assign("msg",$msg);
  $smarty->assign("parameterId",$parameterId);
  $smarty->assign("parameterArr",$parameterArr);
  $smarty->assign("currentparameterName",$currentparameterName);
  $smarty->display("parameterEntry.tpl");
} else {
  header("Location:index.php");
}  
}

?>