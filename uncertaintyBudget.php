<?php
include("include/omConfig.php");
 
  $grnId       = "";
  $grnPrefixNo = "";
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{    
  $range          = 0;
  $accuracySubSub = 0;
  $uncertinty     = 0;
  $resolution     = 0;
  $stability      = 0;

  //Grn ObserVation Value For The Uncertanity Budget :Start  
  $selectItem = "SELECT stdMeterAverage,testMeterAverage,grnObservationId,uncertinityInPercentage,acuuracyForTypeBPerc,acuuracyForTypeBPerc,resolutionForTypeBPerc,stabilityForTypeBInPerc,
                        combinedUncertinityInPerc,effectiveUncertinityInPer,standardUncertinityperc,degreeOfFreedom,accuracyTaken
                   FROM grnobservation
                  WHERE grnObservationId = ".$_GET['grnObservationId'];
  $selectItemResult = mysql_query($selectItem);
  while($selectItemRow = mysql_fetch_array($selectItemResult))
  {
    $grnObservationId          = $selectItemRow['grnObservationId'];
    $stdMeterAverage           = $selectItemRow['stdMeterAverage'];
    $testMeterAverage          = $selectItemRow['testMeterAverage'];
    $uncertinityInPercentage   = $selectItemRow['uncertinityInPercentage'];
    $acuuracyForTypeBPerc      = $selectItemRow['acuuracyForTypeBPerc'];
    $resolutionForTypeBPerc    = $selectItemRow['resolutionForTypeBPerc'];
    $stabilityForTypeBInPerc   = $selectItemRow['stabilityForTypeBInPerc'];
    $combinedUncertinityInPerc = $selectItemRow['combinedUncertinityInPerc'];
    $effectiveUncertinityInPer = $selectItemRow['effectiveUncertinityInPer'];
    $standardUncertinityperc   = $selectItemRow['standardUncertinityperc'];
    $degreeOfFreedom           = $selectItemRow['degreeOfFreedom'];
    $accuracyTaken             = $selectItemRow['accuracyTaken'];
  }

  //Grn ObserVation Value For The Uncertanity Budget :End  
  
  $selectGrn = "SELECT grnPrefix,grnNo,grnId
                  FROM grnmaster  
                 ORDER BY grnPrefix,grnNo";
  $selectGrnResult = mysql_query($selectGrn);
  $i = 0;
  while($selectGrnRow = mysql_fetch_array($selectGrnResult))
  {
    $grnId[$i]       = $selectGrnRow['grnId'];
    $grnPrefixNo[$i] = $selectGrnRow['grnPrefix']."-".$selectGrnRow['grnNo'];
    $i++;
  }

$selectGrn = "SELECT masterMeterId,masterMeterName
                 FROM mastermeter
                ORDER BY masterMeterName";
 $selectGrnResult = mysql_query($selectGrn);
 $j = 0;
while($selectGrnRow = mysql_fetch_array($selectGrnResult))
{
  $meterEntryIdArray[$j]   = $selectGrnRow['masterMeterId'];
  $meterEntryNameArray[$j] = $selectGrnRow['masterMeterName'];
  $j++;
}

$selectMasterQuery = "SELECT grnobservation.grnObservationId,grnobservation.grnDetailId,grndetail.grnDetailId,grndetail.masterMeterId,mastermetersub.masterMeterId,
                             mastermetersub.masterMeterSubId,mastermetersubsub.masterMeterSubId,mastermetersubsub.rangeValue,mastermetersubsub.accuracySubSub,mastermetersubsub.uncertinty,
                             mastermetersubsub.resolution,mastermetersubsub.stability
                        FROM grnobservation
                        JOIN grndetail
                        JOIN mastermetersub
                        JOIN mastermetersubsub
                       WHERE grnobservation.grnDetailId = grndetail.grnDetailId
                         AND grndetail.masterMeterId = mastermetersub.masterMeterId
                         AND mastermetersub.masterMeterSubId =  mastermetersubsub.masterMeterSubId
                         AND grnobservation.grnObservationId = ".$_GET['grnObservationId'];
$selectMasterQueryRes = mysql_Query($selectMasterQuery);
while($masterRow = mysql_fetch_array($selectMasterQueryRes))
{
  $range          = $masterRow['rangeValue'];
  $accuracySubSub = $masterRow['accuracySubSub'];
  $uncertinty     = $masterRow['uncertinty'];
  $resolution     = $masterRow['resolution']/2;
  $stability      = $masterRow['stability'];
}
}
include("./bottom.php");
  $smarty->assign("meterEntryIdArray",$meterEntryIdArray);
  $smarty->assign("meterEntryNameArray",$meterEntryNameArray);
  $smarty->assign("grnId",$grnId);
  $smarty->assign("grnObservationId",$grnObservationId);
  $smarty->assign("grnPrefixNo",$grnPrefixNo);
  $smarty->assign("testMeterAverage",$testMeterAverage);
  $smarty->assign("stdMeterAverage",$stdMeterAverage);
  $smarty->assign("uncertinityInPercentage",$uncertinityInPercentage);
  $smarty->assign("acuuracyForTypeBPerc",$acuuracyForTypeBPerc);
  $smarty->assign("resolutionForTypeBPerc",$resolutionForTypeBPerc);
  $smarty->assign("stabilityForTypeBInPerc",$stabilityForTypeBInPerc);
  $smarty->assign("combinedUncertinityInPerc",$combinedUncertinityInPerc);
  $smarty->assign("effectiveUncertinityInPer",$effectiveUncertinityInPer);
  $smarty->assign("standardUncertinityperc",$standardUncertinityperc);
  $smarty->assign("degreeOfFreedom",$degreeOfFreedom);
  $smarty->assign("range",$range);
  $smarty->assign("accuracySubSub",$accuracySubSub);
  $smarty->assign("uncertinty",$uncertinty);
  $smarty->assign("resolution",$resolution);
  $smarty->assign("stability",$stability);
  $smarty->assign("accuracyTaken",$accuracyTaken);
  $smarty->display("uncertaintybudget.tpl");
?>