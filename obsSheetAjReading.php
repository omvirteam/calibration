<?php
include("include/omConfig.php");

$val = isset($_GET['val']) ? trim($_GET['val']) : '';
if($val != "") { 
	$reading = array();
	$reading['10'] = '0.1';
	$reading['20'] = '0.2';
	$reading['30'] = '0.3';
	$reading['40'] = '0.4';
	$reading['50'] = '0.5';
	
	if( array_key_exists($val, $reading) ) { 
		print $reading[$val]; exit;
	} else {
		print $val;
	}
} else {
	print $val;
}