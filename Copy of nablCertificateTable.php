<?php
include("include/omConfig.php");
error_reporting(0);
if(!isset($_SESSION['s_activId'])) {
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
  exit;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// general functions - start //
function in_array_r($needle, $haystack, $strict = false) {
    foreach($haystack as $item) {
        if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}
// general functions - start //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// variable list - start //
$certificateNo 			= '';
$grnNo 					= '';
$grnDate				= ''; 
$totalPages 			= 0;
$callibrationDate		= '';
$callibrationDateNext 	= '';
$customerName			= '';
$customerAddress		= '';
$customerRefNo			= '';
$ambientTemperature		= '';
$relativeHumidity		= '';
$remarks				= '';

$counter 				= 0;
$grnDetailIds 			= array();
$tempArr 				= array() ;

$instrumentDetails					= array();
$instrumentDetails['item_name']		= '';
$instrumentDetails['make']			= '';
$instrumentDetails['item_code']		= '';
$instrumentDetails['id_no']			= '';
$instrumentDetails['range_value']	= '';
$instrumentDetails['least_count']	= '';
$instrumentDetails['accuracy']		= '';
$instrumentDetails['grn_condition']	= '';
$instrumentDetails['certi_remarks']	= '';

$masterMeters						= array();
$masterMeters['meter_name']			= array();
$masterMeters['sr_no']				= array();
$masterMeters['certi_no']			= array();
$masterMeters['id_no']				= array();
$masterMeters['due_date']			= array();
$masterMeters['tracebility']		= array();
$masterMeters['meter_procedure']	= array();
$masterMeters['masterMeterNote']	= array();

$observations						= array();
/*$observations['parameters']			= array();
$observations['table_no']			= array();
$observations['range1']				= array();
$observations['reading1']			= array();
$observations['range2']				= array();
$observations['reading2']			= array();
$observations['units']				= array();
$observations['rdg']				= array();
$observations['expanded']			= array();*/
// variable list - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// fetch data - start //
$qry = 'SELECT DATE_FORMAT(grndetail.callibrationDate,"%d/%m/%Y") AS callibrationDate,
	DATE_FORMAT(grndetail.nextYearDate,"%d/%m/%Y") AS nextYearDate,grndetail.grnId,
	grndetail.temperature,grndetail.humidity,
	CONCAT("N-",grnmaster.grnNo) AS grnNo,
	DATE_FORMAT(grnmaster.grnDate,"%d/%m/%Y") AS grnDate,
	grnobsmaster.selfCertiNo,
	customer.custName,customer.address,
	CONCAT(IF(customer.custRefName IS NOT NULL, customer.custRefName,""),"/cali/",customer.custCode) AS customerRef 
	FROM grndetail 
	LEFT JOIN grnmaster ON grnmaster.grnId = grndetail.grnId 
	LEFT JOIN grnobsmaster ON grnobsmaster.grnDetailId = grndetail.grnDetailId 
	LEFT JOIN customer ON customer.customerId = grnmaster.customerId 
	WHERE grndetail.grnDetailId = '.$_GET['grnDetailId'];
$res = mysql_query($qry) or die("Error :: Cannot select basic details.<hr>".mysql_error());
if($row = mysql_fetch_assoc($res)) { 
	$certificateNo 			= $row['selfCertiNo'];
	$grnNo 					= $row['grnNo'];
	$grnDate				= $row['grnDate'];
	$callibrationDate		= $row['callibrationDate'];
	$callibrationDateNext 	= $row['nextYearDate'];;
	$customerName			= $row['custName'];
	$customerAddress		= $row['address'];
	$customerRefNo			= $row['customerRef'];
	$ambientTemperature		= $row['temperature'];
	$relativeHumidity		= $row['humidity'];
	
	$tempArr = $instrumentDetails;
	// instrument details query //
	$qry1 = 'SELECT grndetail.grnDetailId,grndetail.makeModel AS make,grndetail.itemCode AS item_code,
		grndetail.instrumentId AS id_no,grndetail.rangeValue AS range_value,grndetail.leastCount AS least_count,
		grndetail.accuracy,grndetail.grnCondition AS grn_condition,item.itemName AS item_name,
		grndetail.certiRemarks AS certi_remarks 
		FROM grndetail 
		LEFT JOIN item ON item.itemId = grndetail.itemId 
		WHERE grndetail.callibrationDate IS NOT NULL AND grndetail.grnId = '.$row['grnId'];
	$res1 = mysql_query($qry1) or die("Error :: Cannot select customer and instrument details.<hr>".mysql_error());
	while($row1 = mysql_fetch_assoc($res1)) { 
		array_push($grnDetailIds,$row1['grnDetailId']);
		foreach($tempArr as $key=>$val) {
			if(!in_array_r($row1[$key],$tempArr[$key])) {
				$tempArr[$key][$counter] = $row1[$key];
				$instrumentDetails[$key] .= $row1[$key].' | ';
			}
		}
		$counter++;
	}
	
	foreach($instrumentDetails as $key=>$val) {
		if($val != '')
			$instrumentDetails[$key] = substr($val,0,-3);
	}
	mysql_free_result($res1);
	
	$tempArr = $masterMeters;
	// master meters query //
	$qry2 = 'SELECT masterMeterId,CONCAT(masterMeterName," (",masterMeterModelNo,")") AS meter_name,
		masterMeterSerialNo AS sr_no,masterMeterCertificateNo AS certi_no,
		masterMeterIdNo AS id_no,DATE_FORMAT(masterMeterExp,"%d/%m/%Y") AS due_date,
		masterMeterTraceabilityTo AS tracebility,procedureText AS meter_procedure,masterUncertaintyText AS masterMeterNote 
		FROM mastermeter 
		WHERE masterMeterId IN (SELECT masterMeterId 
			FROM grnobsmaster 
			WHERE grnDetailId IN ('.implode(",",$grnDetailIds).'))';
	$res2 = mysql_query($qry2) or die("Error :: Cannot select master meter details.<hr>".mysql_error());
	while($row2 = mysql_fetch_assoc($res2)) { 
		foreach($tempArr as $key=>$val) { 
			array_push($masterMeters[$key],$row2[$key]);
		}
	}
	mysql_free_result($res2);

	// observations/parameters query //
	/*
	$qry3 = 'SELECT grndetail.parameterId,grndetail.rangeValue AS range2,
		FORMAT((((grnobservation.stdMeterAverage / (grndetail.ratio1 / grndetail.ratio2)) - grnobservation.testMeterAverage) / grnobservation.testMeterAverage * 100),2) AS rdg,
		parameterentry.parameterName,parameterentry.tableNo,
		grnobservation.stdMeterAverage AS reading2,grnobservation.expandedUncertinityInPre AS expanded,
		mastermetersubsub.rangeValue AS range1,
		grnobservation.testMeterAverage AS reading1 
		FROM grndetail 
		LEFT JOIN parameterentry ON parameterentry.parameterId = grndetail.parameterId 
		LEFT JOIN grnobservation ON grnobservation.grnDetailId = grndetail.grnDetailId 
		LEFT JOIN grnobsmaster ON grnobsmaster.grnDetailId = grndetail.grnDetailId 
		LEFT JOIN mastermeter ON mastermeter.masterMeterId = grnobsmaster.masterMeterId 
		LEFT JOIN mastermetersub ON mastermetersub.masterMeterId = mastermeter.masterMeterId 
		LEFT JOIN mastermetersubsub ON mastermetersubsub.masterMeterSubId = mastermetersub.masterMeterSubId
		WHERE grndetail.grnDetailId IN ('.implode(",",$grnDetailIds).')';
	$res3 = mysql_query($qry3) or die("Error :: Cannot select master meters.<hr>".mysql_error());
	while($row3 = mysql_fetch_assoc($res3)) { 
		array_push($observations['parameters'], $row3['parameterName']);
		array_push($observations['table_no'], $row3['tableNo']);
		array_push($observations['range1'], $row3['range1']);
		array_push($observations['reading1'], $row3['reading1']);
		array_push($observations['range2'], $row3['range2']);
		array_push($observations['reading2'], $row3['reading2']);
		array_push($observations['units'], ($row3['reading2'] - $row3['reading1']));
		array_push($observations['rdg'], $row3['rdg']);
		array_push($observations['expanded'], abs(sprintf("%.3f",$row3['expanded'])));
	}
	mysql_free_result($res3);
	*/
}
mysql_free_result($res);
// fetch data - end //
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// html - start //
ob_start();
?>
<style>
table {font:12px arial,sans-serif;}
@media print
{
.pagebreak { page-break-after:always; }
}

</style>
<div style="display:block;border:1px solid black;">	
<!-- page header start -->
<table border=0 cellpadding=0 cellspacing=3 width="100%" align="center" >
	<tr>
		<td>
			<table width="100%">
				<tr>
					<td width="15%">CERTIFICATE NO.</td>
					<td width="20%">: <?php echo $certificateNo; ?></td>
					<td width="15%">&nbsp;</td>
					<td width="35%">DATE OF CALLIBRATION</td>
					<td width="15%">: <?php echo $callibrationDate; ?></td>
				</tr>
				<tr>
					<td>GRN NO.</td>
					<td>: <?php echo $grnNo; ?></td>
					<td>&nbsp;</td>
					<td>NEXT RECOMMENDED CALLIBRATION DATE</td>
					<td>: <?php echo $callibrationDateNext; ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><hr /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<tr valign="top">
					<td width="2%"><strong>1.</strong></td>
					<td width="46%"><strong>CUSTOMER NAME & ADDRESS</strong></td>
					<td width="2%">:</td>
					<td width="50%" style="font-family: Agency FB;font-size:18px; font-weight:bold;">M/s. <?php echo $customerName; ?></td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td style="font-size:x-small;"><?php echo $customerAddress; ?></td>
				</tr>
				<tr valign="top">
					<td><strong>2.</strong></td>
					<td><strong>CUSTOMER REFERENCE NO.</strong></td>
					<td>:</td>
					<td><?php echo $customerRefNo; ?></td>
				</tr>
				<tr valign="top">
					<td><strong>3.</strong></td>
					<td><strong>INSTRUMENT RECEIVED ON</strong></td>
					<td>:</td>
					<td><?php echo $grnDate; ?></td>
				</tr>
				<tr valign="top">
					<td><strong>4.</strong></td>
					<td><strong>DESCRIPTION OF INSTRUMENT</strong></td>
					<td>:</td>
					<td>&nbsp;</td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td>Name</td>
					<td>:</td>
					<td><?php echo $instrumentDetails['item_name']; ?></td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td>Make/Model</td>
					<td>:</td>
					<td><?php echo $instrumentDetails['make']; ?></td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td>Sr. No.</td>
					<td>:</td>
					<td><?php echo $instrumentDetails['item_code']; ?></td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td>Instrument ID No.</td>
					<td>:</td>
					<td><?php echo $instrumentDetails['id_no']; ?></td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td>Range</td>
					<td>:</td>
					<td><?php echo $instrumentDetails['range_value']; ?></td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td>Least Count</td>
					<td>:</td>
					<td><?php echo $instrumentDetails['least_count']; ?></td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td>Accuracy</td>
					<td>:</td>
					<td><?php echo $instrumentDetails['accuracy']; ?></td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td>Condition on receipt</td>
					<td>:</td>
					<td><?php echo $instrumentDetails['grn_condition']; ?></td>
				</tr>
				<tr valign="top">
					<td><strong>5.</strong></td>
					<td><strong>AMBIENT TEMPERATURE</strong></td>
					<td>:</td>
					<td><?php echo $ambientTemperature; ?><div style="float:right"><strong>RELATIVE HUMIDITY</strong> : <?php echo $relativeHumidity; ?></div></td>
				</tr>
				<tr valign="top">
					<td><strong>6.</strong></td>
					<td colspan="3"><strong>DETAILS OF REFERANCE STANDARD & MAJOR INSTRUMENT USED</strong></td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td colspan="3">
						<table width="100%" cellpadding="3" cellspacing="0" border="1" style="border:thin;">
							<tr valign="top" style="font-weight:bold;">
								<td width="2%">#</td>
								<td width="35">Name & Make</td>
								<td width="10%">Sr. No.</td>
								<td width="20%">Certi. No.</td>
								<td width="8%">ID No.</td>
								<td width="10%">Due Date</td>
								<td width="15%">Tracebility</td>
							</tr>
							<?php for($index = 0; $index < count($masterMeters['meter_name']); $index++) { ?>
							<tr valign="top">
								<td><?php echo ($index+1); ?></td>
								<td><?php echo $masterMeters['meter_name'][$index]; ?></td>
								<td><?php echo $masterMeters['sr_no'][$index]; ?></td>
								<td><?php echo $masterMeters['certi_no'][$index]; ?></td>
								<td><?php echo $masterMeters['id_no'][$index]; ?></td>
								<td><?php echo $masterMeters['due_date'][$index]; ?></td>
								<td><?php echo $masterMeters['tracebility'][$index]; ?></td>
							</tr>
							<?php } ?>
						</table>
					</td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td colspan="3"><strong>Note:</strong><br />
					<?php 
					for($index = 0; $index < count($masterMeters['masterMeterNote']); $index++) { 
						if(strlen(trim($masterMeters['masterMeterNote'][$index])) > 0)
							echo '- '.$masterMeters['masterMeterNote'][$index].'<br>';
					}
					?>
					</td>
				</tr>
				<tr valign="top">
					<td><strong>7.</strong></td>
					<td><strong>PROCEDURE</strong></td>
					<td>:</td>
					<td><?php echo implode(' | ',$masterMeters['meter_procedure']); ?></td>
				</tr>
				<tr valign="top">
					<td>&nbsp;</td>
					<td colspan="3"><strong>Remarks:</strong><br />
					<?php 
					$remarks = explode(' | ', $instrumentDetails['certi_remarks']);
					for($index = 0; $index < count($remarks); $index++) { 
						if(strlen(trim($remarks[$index])) > 0)
							echo '- '.$remarks[$index].'<br>';
					}
					?>
					</td>
				</tr>
				<tr valign="top">
					<td><strong>8.</strong></td>
					<td colspan="3"><strong>OBSERVATION</strong></td>
				</tr>	
				<tr>
					<td>&nbsp;</td>	
					<td colspan="3">
					<?php
					$qry3 = 'SELECT grndetail.grnDetailId, grndetail.parameterId, grndetail.rangeValue, ratio1, ratio2,  
						parameterentry.parameterName, parameterentry.tableNo 
						FROM grndetail 
						LEFT JOIN parameterentry ON parameterentry.parameterId = grndetail.parameterId 
						WHERE grndetail.grnDetailId IN ('.implode(",",$grnDetailIds).')';
					$res3 = mysql_query($qry3) or die("Error :: Cannot select parameters in GRN details.<hr>".mysql_error());
					while($row3 = mysql_fetch_assoc($res3)) {
						$range2 = $row3['rangeValue'];
						if($row3['ration2'] != 0)
							$mf	= ($row3['ratio1'] / $row3['ration2']);
						else 
							$mf = 1;
							
						if($mf != 1) 
							$displayCalculated = 1;
						else
							$displayCalculated = 0;
					?>
						<table width="100%">
							<tr>
								<td><?php echo $row3['parameterName']; ?></td>
							</tr>
						</table>
						<?php if($row3['tableNo'] == 1) { ?>
						<table width="100%" cellpadding="3" cellspacing="0" border="1" style="border:thin;">
							<tr valign="top" style="font-weight:bold;">
								<td rowspan="2" align="center">#</td>
								<td colspan="2" align="center">Callibration Standard</td>
								<?php if($displayCalculated == 1) { ?>
								<td rowspan="2" align="center">Calculated Reading</td>
								<?php } ?>
								<td colspan="2" align="center">Unit Under Test</td>
								<td colspan="2" align="center">Error</td>
								<td rowspan="2" align="center">Expanded Uncert. In %</td>
							</tr>
							<tr>
								<td align="center">Range</td>
								<td align="center">Reading</td>
								<td align="center">Range</td>
								<td align="center">Reading</td>
								<td align="center">Units</td>
								<td align="center">$ of Rdg.</td>
							</tr>
							<?php 
							$index = 1;
							$qry4 = 'SELECT stdMeterAverage, testMeterAverage, expandedUncertinityInPre 
								FROM grnobservation WHERE grnDetailId = '.$row3['grnDetailId'];
							$res4 = mysql_query($qry4) or die("Error :: Cannot select grn overstation details.<hr>".mysql_error());	
							while($row4 = mysql_fetch_assoc($res4)) {
								$reading1 = $row4['testMeterAverage']; 
								$reading2 = $row4['stdMeterAverage'];	
								$expanded = number_format($row4['expandedUncertinityInPre'], 2);
							
								$qry5 = 'SELECT rangeValue FROM mastermetersubsub WHERE masterMeterSubId IN (
									SELECT masterMeterSubId FROM mastermetersub WHERE masterMeterId IN (
										SELECT masterMeterId FROM grnobsmaster WHERE grnDetailId = '.$row3['grnDetailId'].'))';
								$res5 = mysql_query($qry5) or die("Error :: Cannot select range value.<hr>".mysql_error());
								$row5 = mysql_fetch_assoc($res5);
								
								$range1 = $row5['rangeValue'];
								$units	= ($reading2 / $mf - $reading1); 	
								$rdg	= number_format(($units / $reading1 * 100), 2);
							?>
							<tr valign="top">
								<td align="center" width="15px"><?php echo $index; ?></td>
								<td align="center" width="60px"><?php echo $range1; ?></td>
								<?php if($displayCalculated == 1) { ?>
								<td align="center" width="60px"><?php echo ($row4['stdMeterAverage'] / $mf); ?></td>
								<?php } ?>
								<td align="center" width="60px"><?php echo $reading1; ?></td>
								<td align="center" width="60px"><?php echo $range2; ?></td>
								<td align="center" width="60px"><?php echo $reading2; ?></td>
								<td align="center" width="60px"><?php echo $units; ?></td>
								<td align="center" width="60px"><?php echo $rdg; ?></td>
								<td align="center" width="60px"><?php echo $expanded; ?></td>
							</tr>
							<?php 
								$index++;
							}
							mysql_free_result($res4); 
							?>
						</table>
					<?php
						}
					}
					mysql_free_result($res3);
					?>
					</td>
				</tr>	
			</table>
		</td>
	</tr>
</table>
</div>
<div style="clear:both;"></div>
<?php
// html - end //
$content = ob_get_flush();
//////////////////////////////////////////////////////////////////////////////////////////////////////////
?>
